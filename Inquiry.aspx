﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Inquiry.aspx.cs" Inherits="Inquiry" Title="استعلام بليط" %>

<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tr>
            <td class="Title" colspan="6">
                استعلام بليط
            </td>
        </tr>
        <tr>
            <td>
                از تاريخ
            </td>
            <td>
                <uc1:datepicker id="sDateStart" runat="server" width="150px" />
            </td>
            <td>
                تا تاريخ
            </td>
            <td>
                <uc1:datepicker id="sDateEnd" runat="server" width="150px" />
            </td>
        </tr>
        <tr>
            <td>
                شماره بليط
            </td>
            <td>
                <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                <asp:Button ID="doSearch" runat="server" Text="جستجو" CssClass="CB" OnClick="doSearch_Click"
                    Width="80px" />
            </td>
            <td>
                نام مسافر
            </td>
            <td>
                <asp:TextBox ID="sFullname" runat="server" CssClass="T" Width="150px" />
                <asp:Button ID="doSearch2" runat="server" Text="جستجو" CssClass="CB" OnClick="doSearch2_Click"
                    Width="80px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                شماره بلیط بصورت کامل نوشته شود شامل شماره سرویس، خط تیره، شماره بلیط
                <br />
                8-1 : بلیط 1 از سرویس 8
            </td>
        </tr>
        <tr>
            <td class="Title" colspan="6">
                نتايج جستجو
            </td>
            <td class="Title">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ID" Visible="False" />
                        <asp:BoundField DataField="No" HeaderText="شماره بليط" />
                        <asp:BoundField DataField="Fullname" HeaderText="نام" />
                        <asp:BoundField DataField="Tel" HeaderText="تلفن" />
                        <asp:BoundField DataField="Price" HeaderText="کرايه" />
                        <asp:BoundField DataField="ID" HeaderText="تعداد" />
                        <asp:BoundField DataField="ID" HeaderText="جمع" />
                        <asp:BoundField DataField="CityID" HeaderText="مقصد" />
                        <asp:BoundField DataField="ID" HeaderText="ساعت" />
                        <asp:BoundField DataField="ID" HeaderText="راننده" />
                        <asp:BoundField DataField="ID" HeaderText="شماره صورت" />
                        <asp:BoundField DataField="ID" HeaderText="وضعيت سرويس" />
                        <asp:BoundField DataField="ID" HeaderText="تاريخ" />
                        <asp:HyperLinkField HeaderText=" استرداد بليط " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="PayBack.aspx?ticketid={0}"
                            Text="استرداد بليط" />
                        <asp:CommandField SelectText=" چاپ " ShowSelectButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        <br />
                        <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                    <HeaderStyle CssClass="GH" />
                    <RowStyle CssClass="GR" />
                    <AlternatingRowStyle CssClass="GAR" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
