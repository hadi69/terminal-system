﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;

public partial class getbarcode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        int val = Tool.GetInt(Request.QueryString["val"], 123);
        Response.ContentType = "image/jpeg";
        int w = 200, h = 20;
        using (Bitmap bit = new Bitmap(w, h))
        {
            using (Graphics gr = Graphics.FromImage(bit))
            {
                gr.FillRectangle(Brushes.Red, 0, 0, w, h);
                gr.DrawString(val.ToString(), SystemFonts.CaptionFont, Brushes.Black, 0, 0);
            }
            bit.Save(Response.OutputStream, ImageFormat.Jpeg);
        }
    }
}
