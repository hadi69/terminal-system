﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EditExclusive.aspx.cs" Inherits="EditExclusive" Title="صورت وضعيت سرويس دربستی" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tr>
            <td class="Title" colspan="8">
                صورت وضعيت سرويس دربستی
                <asp:Label ID="mExclusiveTitle" runat="server" />&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Exclusives.aspx'
                    runat="server" ID="doReturn">برگشت</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="9">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="BErr" colspan="9">
                <asp:Label ID="mWarnings" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                اتوکار
            </td>
            <td colspan="2">
                <asp:DropDownList runat="server" ID="mCarID" CssClass="DD" Width="250px" />
                <cc1:ListSearchExtender TargetControlID='mCarID' runat="server" ID="mCarIDS" QueryPattern="Contains"
                    PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseAuto" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseAuto_Click" />
                <asp:HiddenField runat="server" ID="mCar" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="9">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                راننده اول
            </td>
            <td>
                <asp:DropDownList ID="mDriverID1" runat="server" CssClass="DD" Width="180px" />
                <cc1:ListSearchExtender TargetControlID='mDriverID1' runat="server" ID="ListSearchExtender1"
                    QueryPattern="Contains" PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseDriver1" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseDriver1_Click" />
                <asp:HiddenField runat="server" ID="mDriver1" />
            </td>
            <td>
                راننده دوم
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mDriverID2" CssClass="DD" Width="180px" />
                <cc1:ListSearchExtender TargetControlID='mDriverID2' runat="server" ID="ListSearchExtender2"
                    QueryPattern="Contains" PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseDriver2" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseDriver2_Click" />
                <asp:HiddenField runat="server" ID="mDriver2" />
            </td>
            <td>
                راننده سوم
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mDriverID3" CssClass="DD" Width="180px" />
                <cc1:ListSearchExtender TargetControlID='mDriverID3' runat="server" ID="ListSearchExtender3"
                    QueryPattern="Contains" PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseDriver3" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseDriver3_Click" />
                <asp:HiddenField runat="server" ID="mDriver3" />
            </td>
            <td>
                &nbsp;استان
            </td>
            <td>
                <asp:TextBox ID="mProvince" runat="server" CssClass="T" Width="181px" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                مسير
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mPathID" CssClass="DD" Width="180px" />
            </td>
            <td colspan="3">
                و
                <asp:TextBox ID="mPath2" runat="server" CssClass="T" Width="315px" />
            </td>
            <td>
                و<asp:CheckBox ID="mViceVersa" runat="server" Text="بالعکس" />
            </td>
            <td>
                مدت سفر
            </td>
            <td>
                <asp:TextBox runat="server" ID="mDuration" CssClass="T" Width="120px" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                تاریخ:
            </td>
            <td>
                <uc1:DatePicker ID="mDepartureDate" runat="server" Width="150px" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                زمان حرکت:
            </td>
            <td>
                ساعت:
                <asp:TextBox ID="mDepartureHour" runat="server" Width="20" MaxLength="2" />
                <cc1:FilteredTextBoxExtender FilterMode="ValidChars" ValidChars="0123456789" runat="server"
                    TargetControlID="mDepartureHour" ID="x2" />
                دقیقه:
                <asp:TextBox ID="mDepartureMinute" runat="server" Width="20" MaxLength="2" />
                <cc1:FilteredTextBoxExtender ID="x1" FilterMode="ValidChars" ValidChars="0123456789"
                    runat="server" TargetControlID="mDepartureMinute" />
                <asp:RangeValidator ID="RangeValidator6" Type="Integer" runat="server" Display="Dynamic"
                    ValidationGroup="addGroup" ControlToValidate="mDepartureHour" ErrorMessage="ساعت بايد يک عدد بين 0 و 23 باشد"
                    MaximumValue="23" MinimumValue="0" />
                <asp:RangeValidator ID="RangeValidator7" Type="Integer" runat="server" Display="Dynamic"
                    ValidationGroup="addGroup" ControlToValidate="mDepartureMinute" ErrorMessage="ساعت بايد يک عدد بين 0 و 59 باشد"
                    MaximumValue="59" MinimumValue="0" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                گیرنده کرایه:
            </td>
            <td>
                <asp:RadioButton ID="mCarIsOwnerYes" runat="server" GroupName="carOwner" Text="اتوکار" />
                <asp:RadioButton ID="mCarIsOwnerNo" runat="server" GroupName="carOwner" Text="شرکت" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                ش قرارداد
            </td>
            <td>
                <asp:TextBox ID="mContractNo" runat="server" CssClass="T" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                تاریخ قرارداد
            </td>
            <td>
                <uc1:DatePicker ID="mContractDate" runat="server" Width="150px" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                طرف قرارداد
            </td>
            <td>
                <asp:TextBox runat="server" ID="mContractor" CssClass="T" Width="120px" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                تعداد مسافر
            </td>
            <td>
                <asp:TextBox ID="mNumPassangers" runat="server" CssClass="T" />
                <cc1:FilteredTextBoxExtender ID="f1" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="mNumPassangers" />
            </td>
            <td>
                <asp:RangeValidator ID="r1" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mNumPassangers" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="addGroup"
                    ControlToValidate="mNumPassangers" ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
            <td>
                کل کرايه
            </td>
            <td>

                <script language="javascript">
                    function addCommas(nStr) {
                        //alert('ddd');
                        nStr = nStr.replace(',', '');
                        nStr += '';
                        x = nStr.split('.');
                        x1 = x[0];
                        x2 = x.length > 1 ? '.' + x[1] : '';
                        var rgx = /(\d+)(\d{3})/;
                        while (rgx.test(x1)) {
                            x1 = x1.replace(rgx, '$1' + ',' + '$2');
                        }
                        return x1 + x2;
                    }

                </script>

                <asp:TextBox runat="server" ID="mTotalRent" CssClass="T" />
            </td>
            <td>
                <cc1:FilteredTextBoxExtender ID="ftx1" runat="server" FilterMode="ValidChars" FilterType="Custom"
                    ValidChars="0123456789.," TargetControlID="mTotalRent" />
                <asp:RangeValidator ID="r2" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mTotalRent" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" Enabled="false" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="addGroup"
                    ControlToValidate="mTotalRent" ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
            <td>
                عوارض
            </td>
            <td>
                <asp:TextBox ID="mToll" runat="server" CssClass="T" />
                <cc1:FilteredTextBoxExtender ID="ftx2" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="mToll" />
                %
            </td>
            <td>
                &nbsp;
                <asp:RangeValidator ID="r3" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mToll" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="addGroup"
                    ControlToValidate="mToll" ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                بيمه سرنشين
            </td>
            <td>
                <asp:TextBox runat="server" ID="mInsurance" CssClass="T" />
            </td>
            <td>
                <cc1:FilteredTextBoxExtender ID="ftx7" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="mInsurance" />
                <asp:RangeValidator ID="r5" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="addGroup"
                    ControlToValidate="mInsurance" ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
            <td>
                بيمه بدنه
            </td>
            <td>
                <asp:TextBox runat="server" ID="mBodyInsurance" CssClass="T" />
            </td>
            <td>
                <cc1:FilteredTextBoxExtender ID="ftx4" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="mBodyInsurance" />
                <asp:RangeValidator ID="r6" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mBodyInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="addGroup"
                    ControlToValidate="mBodyInsurance" ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
            <td>
                توشه اضافی
            </td>
            <td>
                <asp:TextBox runat="server" ID="mExtraPrice" CssClass="T" />
            </td>
            <td>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                    FilterType="Numbers" TargetControlID="mExtraPrice" />
                <asp:RangeValidator ID="rvv1" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mExtraPrice" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" />
                <asp:RequiredFieldValidator ID="rvv7" runat="server" ValidationGroup="addGroup" ControlToValidate="mExtraPrice"
                    ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                کارمزد بيمه سرنشين
            </td>
            <td>
                <asp:TextBox ID="mInsuranceWage" runat="server" CssClass="T" Enabled="false" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                کارمزد بيمه بدنه
            </td>
            <td>
                <asp:TextBox ID="mBodyInsuranceWage" runat="server" CssClass="T" Enabled="false" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td colspan="2">
                <asp:Button runat="server" ID="doCalc" Text="محاسبه" CssClass="CB" Width="100px"
                    ValidationGroup="addGroup" OnClick="doCalc_Click" />
                &nbsp;
                <asp:Button runat="server" ID="doRequestInsurance" Text="ارسال به بیمه"
                    CssClass="CB" Width="100px" ValidationGroup="addGroup" Height="19px" Visible="False" OnClick="doRequestInsurance_Click" />
                &nbsp;
                <asp:Button runat="server" ID="doChangeSeries_NoCode" Text="ابطال صورت بدون کد" OnClick="doChangeSeries_NoCode_Click"
                    CssClass="CB" Width="120px" ValidationGroup="addGroup" Height="19px" />
                &nbsp;
                
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <hr style="color: DarkRed; height:1px" />
            </td>
        </tr>
        <tr>
            <td>
                کميسيون
            </td>
            <td>
                <asp:TextBox runat="server" ID="mCommission" CssClass="T" />
                <cc1:FilteredTextBoxExtender ID="ftx3" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                    TargetControlID="mCommission" />
            </td>
            <td>
                <asp:RangeValidator ID="r12" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mCommission" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="0" />
                <asp:Button runat="server" ID="doChangeComission" Text="تایید" CssClass="CB" Width="30px"
                    ValidationGroup="addGroup" OnClick="doChangeComission_Click" />
            </td>
            <td>
                خالص پرداختی
            </td>
            <td>
                <asp:TextBox runat="server" ID="mPrice" CssClass="T" />
                <cc1:FilteredTextBoxExtender ID="ftx5" runat="server" FilterMode="ValidChars" ValidChars="-0123456789"
                    TargetControlID="mPrice" />
            </td>
            <td>
                <asp:RangeValidator ID="r13" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mPrice" ErrorMessage="اين مقدار بايد يک عدد بين -2.000.000.000 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="-20000000" />
            </td>
            <td>
                پرداخت نقدی
            </td>
            <td>
                <asp:TextBox runat="server" ID="mPecuniary" CssClass="T" />
                <cc1:FilteredTextBoxExtender ID="ftx6" runat="server" FilterMode="ValidChars" ValidChars="-0123456789"
                    TargetControlID="mPecuniary" />
            </td>
            <td>
                <asp:RangeValidator ID="r14" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                    ControlToValidate="mPecuniary" ErrorMessage="اين مقدار بايد يک عدد بين -2.000.000.000 و 2.000.000.000 باشد"
                    MaximumValue="2000000000" MinimumValue="-20000000" />
            </td>
        </tr>
        <tr>
            <td>
                عوارض
            </td>
            <td>
                <asp:TextBox runat="server" ID="mToll2" CssClass="T" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                توشه کل اضافی
            </td>
            <td>
                <asp:TextBox runat="server" ID="mExtraPrice2" CssClass="T" ReadOnly="true" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;صندوق&nbsp;
            </td>
            <td>
                &nbsp;<asp:DropDownList runat="server" ID="mFundID" CssClass="DD" Width="120px" />
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <hr style="color: DarkRed; height=1px" />
            </td>
        </tr>
        <tr>
            <td>
                شماره صورت
            </td>
            <td>
                <asp:TextBox ID="mNo" runat="server" CssClass="T"/>
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="addGroup"
                    ControlToValidate="mNo" ErrorMessage="این مقدار الزامی است"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button runat="server" ID='doInvalid2' Text='ابطال شماره صورت' 
                    CssClass="CB" OnClick="doInvalid2_Click" Width="120px" />
            </td>
            <td colspan="2">
                <table border="0" cellpadding="0">
                    <tr>
                        <td>
                            &nbsp; سري صورت
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="mSeriesNo1" Width="30" MaxLength="3" CssClass="T" />
                            <asp:RequiredFieldValidator ID="rfs1" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                ControlToValidate="mSeriesNo1" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            /
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="mSeriesNo2" Width="30" MaxLength="3" CssClass="T" />
                            <asp:RequiredFieldValidator ID="rfs2" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                ControlToValidate="mSeriesNo2" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            /
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="mSeriesNo3" Width="30" MaxLength="3" CssClass="T" />
                            <asp:RequiredFieldValidator ID="rfs3" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                ControlToValidate="mSeriesNo3" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="4">
                <table border="0" cellpadding="0">
                    <tr>
                        <td class="N">
                            کد رهگیری:
                        </td>
                        <td>
                            <asp:Label ID="mTrackCode" runat="server" CssClass="BT" />
                        </td>
                        <td class="N">
                            تاریخ کد رهگیری:
                        </td>
                        <td>
                            <asp:Label ID="mTrackDate" runat="server" CssClass="BT" />
                        </td>
                        <td>تاریخ و ساعت بستن</td>
                        <td>
                            <asp:Label ID="mCloseDate" runat="server" CssClass="BT" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
            <asp:Button runat="server" ID="doEdit" Text="ویرایش صورت" CssClass="CB" Width="100px"
                    ValidationGroup="addGroup2" OnClick="doEdit_Click" />
                &nbsp;
                
            </td>
            <td>
                <asp:Button runat="server" ID="doInvalid" Text="ابطال صورت با کد" CssClass="CB" Width="100px"
                    ValidationGroup="addGroup2" OnClick="doInvalid_Click" />
            </td>
            <td>
                &nbsp;
            </td>
            <td style="text-align: left" colspan="5">
                &nbsp;&nbsp;
                <asp:Button runat="server" ID="doJustSave" Text="ذخيره و بستن" CssClass="CB" Width="100px"
                    ValidationGroup="addGroup" OnClick="doJustSave_Click" />&nbsp;
                &nbsp;
                <asp:Button runat="server" ID="doPrint" Text="چاپ" CssClass="CB" Width="100px" OnClick="doPrint_Click" />
                &nbsp;
                <asp:Button runat="server" ID="doDelete" Text="حذف" CssClass="CB" Width="100px" OnClick="doDelete_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <asp:Label ID="mError" runat="server" CssClass="Err"></asp:Label>
            </td>
        </tr>
    </table>
    <div style="display: none">
        <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
    <cc1:ModalPopupExtender ID="puEx" BehaviorID="puExBehaviorID" runat="server" TargetControlID="puDrag"
        PopupDragHandleControlID="puDrag" PopupControlID="puPanel" OkControlID="puCancel"
        DropShadow="true" CancelControlID="puCancel" BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
    </cc1:ModalPopupExtender>
    <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
        <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
            حذف صورت وضعیت</asp:Panel>
        <table border="0">
            <tbody>
                <tr>
                    <td class="N">
                        <asp:Label runat="server" Visible="false" ID="mIsWithCode"></asp:Label>
                        آیا میخواهید صورت دربستی باطل شود؟
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                            Text="بله" CssClass="CB" Width="60"></asp:Button>&nbsp;
                        <asp:Button ID="puCancel" runat="server" Text="انصراف" CssClass="CB" Width="60">
                        </asp:Button>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="Err" colspan="2">
                        <asp:Label class="Err" runat="server" ID="Label1" />
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
</asp:Content>
