﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="errors.aspx.cs" Inherits="errors" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        نوع
                    </td>
                    <td >
                        <asp:CheckBox ID="mBus" runat="server" CssClass="T" Text='اتوبوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mMini" runat="server" CssClass="T" Text='مینی بوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mSewari" runat="server" CssClass="T" Text='سواری' Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="6">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <input type="hidden" runat="server" id="mIDToDelete" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%" OnRowDeleting="list_RowDeleting">
                <Columns>
                    <asp:BoundField Visible="False" />
                    <asp:BoundField HeaderText="رديف" />
                    <asp:BoundField HeaderText="تاریخ" />
                    <asp:BoundField HeaderText="ساعت" />
                    <asp:BoundField HeaderText="نوع سرويس" />
                    <asp:BoundField HeaderText="مسير" />
                    <asp:BoundField HeaderText="ماشين" />
                    <asp:BoundField HeaderText="راننده1" />
                    <asp:BoundField HeaderText="شماره صورت" />
                    <asp:BoundField HeaderText="سري صورت " />
                    <asp:BoundField HeaderText="ت بليط" />
                    <asp:BoundField HeaderText="اشکال" />
                    <asp:BoundField HeaderText="نوع" />
                    
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
            <cc1:ModalPopupExtender ID="puEx"  BehaviorID="puExBehaviorID"  runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    حذف صورت وضعیت</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                آیا میخواهید علاوه بر حذف، شماره صورت هم باطل شود
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="بله" CssClass="CB" Width="60"></asp:Button>&nbsp;
                                <asp:Button ID="puNo" ValidationGroup="addGroup" OnClick="puNo_Click" runat="server"
                                    Text="خیر" CssClass="CB" Width="60"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف" CssClass="CB" Width="60">
                                </asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label class="Err" runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
