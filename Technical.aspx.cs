﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Technical : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();

        if (!IsPostBack)
        {
            Seller seller = null;
            int sellerid = Tool.GetInt(Request.QueryString["sellerid"], Null.NullInteger);
            if (sellerid != Null.NullInteger)
                seller = Helper.Instance.GetSeller(sellerid);
            int tripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
            if (seller == null)
            {
                Trip mTrip = Helper.Instance.GetTrip(tripID);
                if (mTrip != null)
                {
                    mPath.Text = mTrip.Service.Path.Title;
                    mDate.Text = Tool.ToPersianDate(mTrip.Date, "");
                    mTime.Text = mTrip.Service.DepartureTime;
                    mTechnicalUser.Text = mTrip.TechnicalUser;
                    if (mTrip.FaniDetail!=null)
                    if (mTrip.FaniDetail.Length==16)
                    {
                        BindData(mTrip);
                       // DisableRadios();
                    }
                }
            }
            else
            {
                DataTable dt = Helper.Instance.FillDataTable(
@"SELECT Services.DepartureTime, Trips.Date
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
WHERE Trips.ID=" + tripID, seller.CnnStr);
                if (dt != null && dt.Rows.Count > 0)
                {
                    mPath.Text = Tool.GetString(dt.Rows[0]["SrcCity"], "") + " -> " + Tool.GetString(dt.Rows[0]["DestCity"], "");
                    mDate.Text = Tool.ToPersianDate(dt.Rows[0]["Date"], "");
                    mTime.Text = Tool.GetString(dt.Rows[0]["DepartureTime"], "");
                }
            }
        }
    }

    void BindData(Trip trip)
    {
        string fani = trip.FaniDetail;
        if (fani.Substring(0, 1) == "1")
        {
            Accept1.Checked = true;
        }
        else
        {
            NoAccept1.Checked = true;
        }
        if (fani.Substring(1, 1) == "1")
        {
            Accept2.Checked = true;
        }
        else
        {
            NoAccept2.Checked = true;
        }
        if (fani.Substring(2, 1) == "1")
        {
            Accept3.Checked = true;
        }
        else
        {
            NoAccept3.Checked = true;
        }
        if (fani.Substring(3, 1) == "1")
        {
            Accept4.Checked = true;
        }
        else
        {
            NoAccept4.Checked = true;
        }
        if (fani.Substring(4, 1) == "1")
        {
            Accept5.Checked = true;
        }
        else
        {
            NoAccept5.Checked = true;
        }
        if (fani.Substring(5, 1) == "1")
        {
            Accept6.Checked = true;
        }
        else
        {
            NoAccept6.Checked = true;
        }
        if (fani.Substring(6, 1) == "1")
        {
            Accept7.Checked = true;
        }
        else
        {
            NoAccept7.Checked = true;
        }
        if (fani.Substring(7, 1) == "1")
        {
            Accept8.Checked = true;
        }
        else
        {
            NoAccept8.Checked = true;
        }
        if (fani.Substring(8, 1) == "1")
        {
            Accept9.Checked = true;
        }
        else
        {
            NoAccept9.Checked = true;
        }
        if (fani.Substring(9, 1) == "1")
        {
            Accept10.Checked = true;
        }
        else
        {
            NoAccept10.Checked = true;
        }
        if (fani.Substring(10, 1) == "1")
        {
            Accept11.Checked = true;
        }
        else
        {
            NoAccept11.Checked = true;
        }
        if (fani.Substring(11, 1) == "1")
        {
            Accept12.Checked = true;
        }
        else
        {
            NoAccept12.Checked = true;
        }
        if (fani.Substring(12, 1) == "1")
        {
            Accept13.Checked = true;
        }
        else
        {
            NoAccept13.Checked = true;
        }
        if (fani.Substring(13, 1) == "1")
        {
            Accept14.Checked = true;
        }
        else
        {
            NoAccept14.Checked = true;
        }
        if (fani.Substring(14, 1) == "1")
        {
            Accept15.Checked = true;
        }
        else
        {
            NoAccept15.Checked = true;
        }
        if (fani.Substring(15, 1) == "1")
        {
            Accept16.Checked = true;
        }
        else
        {
            NoAccept16.Checked = true;
        }
        mComment.Text = trip.Comments;
    }

    void DisableRadios()
    {
        Accept1.Enabled= Accept2.Enabled= Accept3.Enabled= Accept4.Enabled= Accept5.Enabled= Accept6.Enabled= Accept7.Enabled= Accept8.Enabled= Accept9.Enabled= Accept10.Enabled= Accept11.Enabled= Accept12.Enabled=Accept13.Enabled=Accept14.Enabled=Accept15.Enabled=Accept16.Enabled=false;

        NoAccept1.Enabled = NoAccept2.Enabled = NoAccept3.Enabled = NoAccept4.Enabled = NoAccept5.Enabled = NoAccept6.Enabled = NoAccept7.Enabled = NoAccept8.Enabled = NoAccept9.Enabled = NoAccept10.Enabled = NoAccept11.Enabled = NoAccept12.Enabled = NoAccept13.Enabled = NoAccept14.Enabled = NoAccept15.Enabled = NoAccept16.Enabled = false;
    }
}
