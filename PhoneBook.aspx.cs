﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PhoneBook : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
            BindData();
    }

    private void BindData()
    {
        var all = from c in Helper.Instance.DB.TelEntries
                  select c;

        if (sName.Text.Trim().Length > 0)
            all = all.Where(c => c.Name.Contains(sName.Text.Trim()));
        if (sSurname.Text.Trim().Length > 0)
            all = all.Where(c => c.Surname.Contains(sSurname.Text.Trim()));
        if (sTel.Text.Trim().Length > 0)
            all = all.Where(c => c.Tel.Contains(sTel.Text.Trim()));
        if (sMobile.Text.Trim().Length > 0)
            all = all.Where(c => c.Mobile.Contains(sMobile.Text.Trim()));
        if (sAddress.Text.Trim().Length > 0)
            all = all.Where(c => c.Address.Contains(sAddress.Text.Trim()));
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        TelEntry info;
        if (Null.NullInteger == EditID)
            info = new TelEntry();
        else
            info = Helper.Instance.GetTelEntry(EditID);
        if (info == null)
            return;


        info.Name = mName.Text;
        info.Surname = mSurname.Text;
        info.Tel = mTel.Text;
        info.Mobile = mMobile.Text;
        info.Address = mAddress.Text;

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.TelEntries.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TelEntry info = e.Row.DataItem as TelEntry;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        TelEntry info = Helper.Instance.GetTelEntry(EditID);
        if (info == null)
            return;

        mName.Text = info.Name;
        mSurname.Text = info.Surname;
        mTel.Text = info.Tel;
        mMobile            .Text = info.Mobile;
        mAddress.Text = info.Address;

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteTelEntry(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mName.Text = mSurname.Text = mTel.Text
            = mMobile.Text = mAddress.Text = "";

        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
