﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;

public partial class Opens : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        if (!IsPostBack)
        {
            //Helper.Instance.AutoDeleteTrips();
            BindInitData();
            BindData();
        }
        deleteAll.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "Are rou sure you want delete all records"));
    }
    private void BindInitData()
    {

        {
            List<Service> all = (from s in Helper.Instance.DB.Services
                                 where s.ServiceType == (int)ServiceTypes.Fixed
                                 select s).ToList(); //Helper.Instance.GetServices();
            Service none = new Service();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sServiceID.DataValueField = "ID";
            sServiceID.DataTextField = "Title";
            sServiceID.DataSource = all;
            sServiceID.DataBind();

            //List<Service> all = Helper.Instance.GetServices();
            //sServiceID.DataValueField = "ID";
            //sServiceID.DataTextField = "Title";
            //sServiceID.DataSource = all;
            //sServiceID.DataBind();
        }
        {
            List<Car> all = Helper.Instance.GetCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarID.DataValueField = "ID";
            sCarID.DataTextField = "Title";
            sCarID.DataSource = all;
            sCarID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            CarType none = new CarType();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarTypeID.DataValueField = "ID";
            sCarTypeID.DataTextField = "Title";
            sCarTypeID.DataSource = all;
            sCarTypeID.DataBind();
        }
        {
            List<Driver> all = Helper.Instance.GetDrivers();
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDriverID1.DataValueField = "ID";
            sDriverID1.DataTextField = "Title";
            sDriverID1.DataSource = all;
            sDriverID1.DataBind();
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            City none = new City();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDestID.DataValueField = "ID";
            sDestID.DataTextField = "Title";
            sDestID.DataSource = all;
            sDestID.DataBind();
        }

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Closeds.sDateStart"]), DateTime.Now);
        sDateEnd.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Closeds.sDateEnd"]), DateTime.Now);

    }
    private List<Trip> GetTrips()
    {
        var all = from c in Helper.Instance.DB.Trips
                  where c.Closed == false
                  orderby c.Date, c.Service.DepartureTime, c.Service.ServiceType
                  select c;

        List<Trip> trips = all.ToList();
        if (trips == null || trips.Count == 0)
            return trips;

        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].ServiceID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarTypeID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sDestID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].Service.Path.City.ID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].DriverID1 != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (!mBus.Checked)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarType == null || trips[i].CarType.Layout.NumChairs > 25)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (!mMini.Checked)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarType == null || (trips[i].CarType.Layout.NumChairs > 6 && trips[i].CarType.Layout.NumChairs < 25))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (!mSewari.Checked)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarType == null || trips[i].CarType.Layout.NumChairs < 6)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        Session["Closeds.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["Closeds.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        if (startDate != Null.NullDate || toDate != Null.NullDate)
        {
            startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
                : DateTime.Now.AddYears(-10);
            toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
                : DateTime.Now.AddYears(10);
            for (int i = 0; i < trips.Count; i++)
            {
                if ((trips[i].Date < startDate || trips[i].Date > toDate))
                {
                    trips.RemoveAt(i);
                    i--;
                }
            }
        }

        return trips;


    }
    private void BindData()
    {
        list.DataSource = GetTrips();
        list.DataBind();
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Trip info = e.Row.DataItem as Trip;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate2(info.Date, "");
                e.Row.Cells[3].Text = info.Service.DepartureTime;// Tool.ToPersianDate(info.Date, "");
                e.Row.Cells[4].Text = Helper.Instance.GetDriverFullName(info.DriverID1);
                e.Row.Cells[5].Text = Helper.Instance.GetCityTitle(info.Service.Path.DestCityID);
                e.Row.Cells[6].Text = GetDay(info.Date);
                e.Row.Cells[7].Text = info.CarType != null ? info.CarType.Title : "";
                e.Row.Cells[8].Text = Helper.Instance.GetNumTickets(info.ID).ToString();
                e.Row.Cells[9].Text = Helper.Instance.GetNumReserves(info.ID).ToString();

                if (info.Locked == true)
                {
                    e.Row.Cells[10].Text = "قفل";
                    e.Row.Cells[11].Text = "قفل";
                    e.Row.Cells[12].Text = "قفل";
                    e.Row.Cells[13].Text = "قفل";
                    e.Row.Cells[14].Text = "قفل";
                    e.Row.CssClass = "lockedRow";
                }

                if (info.CarType == null)
                {
                    e.Row.Cells[10].Text = e.Row.Cells[11].Text = e.Row.Cells[12].Text =
                        e.Row.Cells[13].Text = e.Row.Cells[14].Text = "-";
                }
                //LinkButton btn = e.Row.Cells[e.Row.Cells.Count - 3].Controls[0] as LinkButton;
                //btn.CommandArgument = info.ID.ToString();
                JsTools.HandleDeleteButton(e);
            }
        }
    }
    private string GetDay(DateTime dateTime)
    {
        if (Null.NullDate == dateTime)
            return "";
        switch (dateTime.DayOfWeek)
        {
            case DayOfWeek.Friday:
                return "جمعه";
            case DayOfWeek.Monday:
                return "دوشنبه";
            case DayOfWeek.Saturday:
                return "شنبه";
            case DayOfWeek.Sunday:
                return "یکشنبه";
            case DayOfWeek.Thursday:
                return "پنج شنبه";
            case DayOfWeek.Tuesday:
                return "سه شنبه";
            case DayOfWeek.Wednesday:
                return "چهارشنبه";
        }
        return "";
    }
    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.GetNumReserves(id) > 0 && !Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false))
        {
            mMsg.Text = "این سرویس بلیط رزرو دارد و نمیتوان آنرا حذف کرد.";
            return;
        }
        if (!Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false))
        {
            Trip tr = Helper.Instance.GetTrip(id);
            if (tr.Tickets.Count > 0)
            {
                mMsg.Text = "این سرویس بلیط دارد و نمیتوان آنرا حذف کرد.";
                return;
            }
        }
        // Check for WEB user and PostBank user
        User web = Helper.Instance.GetUser("web");
        if (web != null)
        {
            List<Ticket> soldByWeb = Helper.Instance.GetTickets(id, web.ID);
            if ((soldByWeb != null && soldByWeb.Count > 0))
            {
                mMsg.Text = string.Format("این سرویس {0} بلیط فروخته شده اینترنتی دارد و نمیتوان آنرا حذف کرد.", soldByWeb.Count);
                return;
            }
        }
        User postbankUser = Helper.Instance.GetUser("postbank");
        if (postbankUser != null)
        {
            List<Ticket> soldByPost = Helper.Instance.GetTickets(id, postbankUser.ID);
            if ((soldByPost != null && soldByPost.Count > 0))
            {
                mMsg.Text = string.Format("این سرویس {0} بلیط فروخته شده پست بانک دارد و نمیتوان آنرا حذف کرد.", soldByPost.Count);
                return;
            }
        }
        if (Helper.Instance.DeleteTrip(id))
        {
            list.SelectedIndex = -1;
            BindData();
            mMsg.Text = "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void deleteAll_Click(object sender, EventArgs e)
    {
        List<Trip> list = GetTrips();
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < list.Count; i++)
        {
            if (Helper.Instance.GetNumReserves(list[i].ID) > 0 && !Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false))
            {
                b.AppendFormat("سرویس {0} بلیط رزرو دارد و نمیتوان آنرا حذف کرد.<br>", list[i].No);
                continue; ;
            }
            if (!Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false))
            {
                Trip tr = Helper.Instance.GetTrip(list[i].ID);
                if (tr.Tickets.Count > 0)
                {
                    b.AppendFormat("سرویس {0} بلیط دارد و نمیتوان آنرا حذف کرد.<br>", list[i].No);
                    continue;
                }
            }
            if (!Helper.Instance.DeleteTrip(list[i].ID))
            {
                b.AppendFormat("سرویس {0} استفاده شده است و نمیتوان آنرا حذف کرد.<br>", list[i].No);
            }
        }
        BindData();
        if (b.Length > 0)
        mMsg.Text = b.ToString();
        else
            mMsg.Text = "سرویسها با موفقیت حذف شدند";
    }

}
