﻿using System;

//public enum SpecialType
//{
//    Normal = 0,
//    Special = 1
//}
public enum ServiceTypes
{
    Normal = 0,
    Fixed = 1,
    AnotherDay = 2,
    Singular = 3
}

public enum SaleTypes
{
    Sale = 0,
    Reserve = 1,
    OfficeSale = 2
}

public enum PaidBacks
{
    None = 0,
    FromFund = 1,
    FromBranch = 2
}

public enum PaymentTypes
{
    Pay = 0, 
    Get = 1, 
    Advance = 2, 
    Payback = 3
}

//public enum Roles
//{
//    Admin = 0,
//    Salesman = 1,
//    Owner = 2
//}

public enum WarningLevel
{
    Success,
    Warning,
    Error
}
public enum Months
{
    فروردين= 1,
    ارديبهشت=2,
    خرداد=3,
    تير=4,
    مرداد=5,
    شهريور=6,
    مهر=7,
    آبان=8,
    آذر=9,
    دي=10,
    بهمن=11,
    اسفند=12
}

public enum TripStateError
{
    HasCode = 0,//توسط خودمان اضافه شده است
    HaveErrorAndNeedTryAgain = -1,
    EditTimeIsInvalid = -2,
    InvalidSendingCode = -3,
    InvalidTimeForCanceling = -4,
    SendingParameterInvalid = -5,
    RequestUserIsInvalid = -6,
    OldPassForChangePassIsInvalid = -7,
    FreighterSmartCadIsDisable = -8,
    FirstDriverSmartCardIsDisable = -9,
    SecondDriverSmartCardIsDisable = -10,
    ThirdDriverSmartCardIsDisable = -11,
    CodeOfRequestCompanyIsDisable = -12,
    WithoutCode = -13//توسط خودمان اضافه شده که مقادیر پیش فرض شود و اگر مقدار آن این باشد یعنی هنوز درخواست کد نکرده است
}

public enum TripStatus
{
    None=0,
    Pending=1,
    Confirmed=2,
    Rejected=3
}

public enum Status
{
    NoCheck=1,
    Confirm=2,
    Reject=3
}

public enum TechnicalStatus
{
    Confirm = 2,
    Reject = 3
}