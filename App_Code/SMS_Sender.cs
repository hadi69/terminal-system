﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using GsmComm.GsmCommunication;
using GsmComm.PduConverter;

/// <summary>
/// Summary description for SMS_Sender
/// </summary>
public class SMS_Sender
{
    public string User;
    public string Pass;
    public string Number;
    public string Port;
    public int Flash;
    public SMS_Sender()
    {
        Number = System.Configuration.ConfigurationSettings.AppSettings["SMS_Number"];// "100002972";
        User = System.Configuration.ConfigurationSettings.AppSettings["SMS_Username"];
        Pass = System.Configuration.ConfigurationSettings.AppSettings["SMS_Password"];
        Port = "0";
        Flash = 0;
    }
    public int Send(string recipient, string message)
    {
        string POST_DATA = "username=" + User + "&password=" + Pass +
            "&message=" + message + "&recipient=" + recipient +
            "&number=" + Number + "&port=" + Port + "&flash=" + Flash;
        byte[] Data = Encoding.UTF8.GetBytes(POST_DATA);
        HttpWebRequest HTTP_Request = (HttpWebRequest)WebRequest.Create("http://www.2972.ir/api");
        HTTP_Request.Method = "POST";
        HTTP_Request.ContentType = "application/x-www-form-urlencoded";
        HTTP_Request.ContentLength = Data.Length;

        Stream NewStream = HTTP_Request.GetRequestStream();
        NewStream.Write(Data, 0, Data.Length);
        NewStream.Close();

        WebResponse Response = HTTP_Request.GetResponse();
        Stream ResponseStream = Response.GetResponseStream();
        StreamReader ResponseReader = new StreamReader(ResponseStream);
        return int.Parse(ResponseReader.ReadToEnd());
    }
    public void SendGSM(string recipient, string message)
    {
        try
        {
            // Send an SMS message
            SmsSubmitPdu pdu;

            // The straightforward version
            pdu = new SmsSubmitPdu("cgfcffcf", recipient, "");  // "" indicate SMSC No

            GsmCommMain comm = new GsmCommMain();
            comm.SendMessage(pdu);
        }
        catch (Exception)
        {
            throw;
        }
    }
    public bool SendByGSM(string phoneNo, string message)
    {
        System.IO.Ports.SerialPort port = new System.IO.Ports.SerialPort("COM4", 115200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
        try
        {
            port.Open();
            port.Write("AT\r\n");
            System.Threading.Thread.Sleep(1000);
            port.WriteLine("AT+CMGF=1\r\n");
            System.Threading.Thread.Sleep(1000);
            //port.WriteLine("AT+CMGS=\"+60121212121\"\r\n");
            port.WriteLine("AT+CMGS=\"" + phoneNo + "\"\r\n");
            System.Threading.Thread.Sleep(1000);
            //port.WriteLine("Testing SMS\r\n" + '\x001a');
            port.WriteLine(message + "\r\n" + '\x001a');
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            port.Close();
        }
    }
}
