﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public class Tool
{
    /// <summary>
    /// Prepends 0 to reach to desired length
    /// </summary>
    /// <param name="value"></param>
    /// <param name="len"></param>
    /// <returns></returns>
    public static int FindZeroOROne(float value1)
    {
        if (value1 > 0)
            return 1;
        else
            return 0;
    }
    public static string ToString(int value, int len)
    {
        string res = value.ToString();
        while (res.Length < len)
            res = "0" + res;
        return res;
    }
    public static string ToString(Int64 value, int len)
    {
        string res = value.ToString();
        while (res.Length < len)
            res = "0" + res;
        return res;
    }

    public static string ToString(string value, int len)
    {
        return ToString(value, len, " ");
    }
    public static string ToString(string value, int len, string prepend)
    {
        value = value.TrimEnd().TrimStart();//added by amini
        while (value.Length < len)
            value = prepend + value;
        return value;
    }
    public static string ToPersianDate(object date, DateTime defaultValue)
    {
        if (Null.IsNull(date))
            return ToPersianDate(defaultValue, "");
        return ToPersianDate(date, "");
    }
    public static string ToPersianDate(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return string.Format("{0}/{1}/{2}", ToString(calendar.GetYear(dt), 4)
                    , ToString(calendar.GetMonth(dt), 2), ToString(calendar.GetDayOfMonth(dt), 2));
        }
        return defaultValue;
    }
    public static string ToPersianYear(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return ToString(calendar.GetYear(dt), 4);
        }
        return defaultValue;
    }
    public static string GetTimeOfDate(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return string.Format("{0}:{1}", ToString(calendar.GetHour(dt), 2), ToString(calendar.GetMinute(dt), 2));
        }
        return defaultValue;
    }
    public static string ToShortPersianDate(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return string.Format("{0}/{1}/{2}", ToString(calendar.GetYear(dt), 4).Substring(2)
                    , ToString(calendar.GetMonth(dt), 2), ToString(calendar.GetDayOfMonth(dt), 2));
        }
        return defaultValue;
    }
    public static string ToShortPersianDate2(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return string.Format("<table border=0 cellpadding=0 cellspacing=0 dir=rtl><tr><td>{2}</td><td>/</td><td>{1}</td><td>/</td><td>{0}</td></tr></table>", ToString(calendar.GetYear(dt), 4).Substring(2)
                    , ToString(calendar.GetMonth(dt), 2), ToString(calendar.GetDayOfMonth(dt), 2));
        }
        return defaultValue;
    }
    public static string ToPersianDate2(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return string.Format("<table border=0 cellpadding=0 cellspacing=0 dir=rtl><tr><td>{2}</td><td>/</td><td>{1}</td><td>/</td><td>{0}</td></tr></table>", ToString(calendar.GetYear(dt), 4)
                    , ToString(calendar.GetMonth(dt), 2), ToString(calendar.GetDayOfMonth(dt), 2));
        }
        return defaultValue;
    }

    public static string mSeps = null;
    public static char[] dateSeparators
    {
        get
        {
            string txt = string.IsNullOrEmpty(mSeps) ? Helper.Instance.GetSettingValue("DateSeparators") : mSeps;
            mSeps = txt;
            if (string.IsNullOrEmpty(txt) || txt.Trim().Length == 0)
                return new char[] { '/', '-', '\\', ',', '.' };
            // string[] charsStr = txt.Split(' ');
            List<char> chars = new List<char>();
            for (int i = 0; i < txt.Length; i++)
                if (txt[i] != ' ')
                    chars.Add(txt[i]);
            return chars.ToArray();
        }
    }
    public static DateTime ParsePersianDate(string date, DateTime defaultValue)
    {
        if (date == null || date.IndexOfAny(dateSeparators) < 0)
            return defaultValue;
        System.Globalization.PersianCalendar persianCalendar = new System.Globalization.PersianCalendar();
        string[] ymd = date.Split(dateSeparators);
        if (ymd.Length < 3)
        {
            if (date.Length == 8)
                return persianCalendar.ToDateTime(Convert.ToInt32(date.Substring(0, 4))
                    , Convert.ToInt32(date.Substring(4, 2))
                    , Convert.ToInt32(date.Substring(6, 2)), 12, 0, 0, 0);
            return defaultValue;
        }
        try
        {
            int year = Convert.ToInt32(ymd[0]);
            if (year < 100)
                year += 1300;
            return persianCalendar.ToDateTime(year, Convert.ToInt32(ymd[1]), Convert.ToInt32(ymd[2]), 12, 0, 0, 0);
        }
        catch
        {
            return defaultValue;
        }
    }
    public static DateTime ParsePersianDate(string date, string time, DateTime defaultValue)
    {
        if (date == null || date.IndexOfAny(dateSeparators) < 0)
            return defaultValue;
        System.Globalization.PersianCalendar persianCalendar = new System.Globalization.PersianCalendar();
        string[] ymd = date.Split(dateSeparators);

        if (ymd.Length < 3)
            return defaultValue;
        try
        {
            string[] hm = time.Split(':');
            int year = Convert.ToInt32(ymd[0]);
            if (year < 100)
                year += 1300;
            if (hm.Length != 2)
                return persianCalendar.ToDateTime(year, Convert.ToInt32(ymd[1]), Convert.ToInt32(ymd[2]), 12, 0, 0, 0);
            else
                return persianCalendar.ToDateTime(year, Convert.ToInt32(ymd[1]), Convert.ToInt32(ymd[2]), Convert.ToInt32(hm[0]), Convert.ToInt32(ymd[1]), 0, 0);
        }
        catch
        {
            return defaultValue;
        }
    }
    public static string FixTime(string time)
    {
        try
        {
            if (time.IndexOf(':') < 0)
            {
                int t = GetInt(time, -1);
                if (t == -1)
                    return "";
                if (0 <= t && t <= 23)
                    return string.Format("{0}:00", t.ToString().PadLeft(2, '0'));
                if (0 <= t && t <= 59)
                    return string.Format("00:{0}", t.ToString().PadLeft(2, '0'));
                return "";
            }
            else
            {
                string[] hm = time.Split(':');
                if (hm.Length != 2)
                    return "";
                int h = GetInt(hm[0], -1);
                if (h == -1)
                    return "";
                int m = GetInt(hm[1], -1);
                if (m == -1)
                    return "";
                if (0 <= h && h <= 23)
                    if (0 <= m && m <= 59)
                        return string.Format("{0}:{1}", h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'));
            }
        }
        catch
        {
        }
        return "";
    }
    public static bool ValidatePersianDate(string date)
    {
        if (date == null || date.IndexOfAny(dateSeparators) < 0)
            return false;
        System.Globalization.PersianCalendar persianCalendar = new System.Globalization.PersianCalendar();
        string[] ymd = date.Split(dateSeparators);
        if (ymd.Length < 3)
            return false;
        try
        {
            DateTime dt = persianCalendar.ToDateTime(Convert.ToInt32(ymd[0]), Convert.ToInt32(ymd[1]), Convert.ToInt32(ymd[2]), 12, 1, 1, 1);
            return true;
        }
        catch
        {
            return false;
        }
    }
    public static object GetForDB(object date)
    {
        if (date == null || date is DBNull)
            return null;
        DateTime dt = GetDate(date);
        return dt == Null.NullDate ? null : (object)dt;
    }
    public static object GetForDB(DateTime date)
    {
        return date == Null.NullDate ? null : (object)date;
    }
    public static object GetForDBNoTime(DateTime date)
    {
        return date == Null.NullDate ? null : (object)new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
    }
    public static int GetInt(object value)
    {
        return GetInt(value, Null.NullInteger);
    }
    //public static int GetInt(object value, int defaultValue)
    //{
    //    if (Null.IsNull(value))
    //        return defaultValue;
    //    int res;
    //    if (int.TryParse(value.ToString(), out res))
    //        return res;
    //    return defaultValue;
    //}
    public static int GetInt(object value, int defaultValue)
    {
        if (Null.IsNull(value))
            return defaultValue;
        float res;
        if (float.TryParse(value.ToString(), out res))
            return (int)res;
        try
        {
            return (int)Convert.ToSingle(value);
        }
        catch { }
        return defaultValue;
    }
    public static int? GetInt(object value, int? defaultValue)
    {
        if (Null.IsNull(value))
            return defaultValue;
        float res;
        if (float.TryParse(value.ToString(), out res))
            return (int)res;
        try
        {
            return (int)Convert.ToSingle(value);
        }
        catch { }
        return defaultValue;
    }

    public static double GetDouble(object value)
    {
        return GetDouble(value, Null.NullSingle);
    }
    public static double GetDouble(object value, double defaultValue)
    {
        if (Null.IsNull(value))
            return defaultValue;
        try
        {
            double a = Convert.ToDouble(value);
            return a;
        }
        catch
        {
        }
        double res;
        if (double.TryParse(value.ToString(), out res))
            return res;
        return defaultValue;
    }
    public static decimal GetDecimal(object value, decimal defaultValue)
    {
        if (Null.IsNull(value))
            return defaultValue;
        try
        {
            return Convert.ToDecimal(value);
        }
        catch
        {
        }
        double res;
        if (double.TryParse(value.ToString(), out res))
            return (decimal)res;
        return defaultValue;
    }
    public static float GetFloat(object value)
    {
        return GetFloat(value, Null.NullSingle);
    }
    public static float GetFloat(object value, float defaultValue)
    {
        if (Null.IsNull(value))
            return defaultValue;
        try
        {
            float a = Convert.ToSingle(value);
            return a;
        }
        catch
        {
        }
        float res;
        if (float.TryParse(value.ToString(), out res))
            return res;
        return defaultValue;
    }
    public static bool GetBool(object value, bool defaultValue)
    {
        try
        {
            return Convert.ToBoolean(value);
        }
        catch
        {
            return defaultValue;
        }

    }

    //public static float GetSingleDirect(object value, float defaultValue)
    //{
    //    if (Null.IsNull(value))
    //        return defaultValue;
    //    try
    //    {
    //        float a = (float)value;
    //        return a;
    //    }
    //    catch
    //    {
    //    }
    //    float res;
    //    if (float.TryParse(value.ToString(), out res))
    //        return res;
    //    return defaultValue;
    //}
    public static DateTime GetDate(object value)
    {
        if (Null.IsNull(value))
            return Null.NullDate;
        DateTime res;
        if (DateTime.TryParse(value.ToString(), out res))
            return res;
        return Null.NullDate;
    }
    public static string[] FindFields(string template)
    {
        if (template == null || template.Trim().Length == 0)
            return null;
        System.Text.RegularExpressions.Regex rg = new System.Text.RegularExpressions.Regex(@"\{\w*\}", System.Text.RegularExpressions.RegexOptions.None);
        System.Text.RegularExpressions.MatchCollection matches = rg.Matches(template);
        if (matches.Count == 0)
            return null;

        string[] res = new string[matches.Count];
        for (int i = 0; i < matches.Count; i++)
            res[i] = matches[i].Value.Substring(1, matches[i].Value.Length - 2);

        return res;
    }

    //public static bool GetBool(object b)
    //{
    //    if (b == null)
    //        return Null.NullBoolean;
    //    try
    //    {
    //        return Convert.ToBoolean
    //    }
    //    catch { }
    //}

    public static Control FindControl(ControlCollection controls, Type type)
    {
        if (controls == null || controls.Count == 0)
            return null;
        for (int i = 0; i < controls.Count; i++)
        {
            if (controls[i].GetType().Equals(type))
                return controls[i];
            System.Web.UI.Control res = FindControl(controls[i].Controls, type);
            if (res != null)
                return res;
        }
        return null;
    }


    public static bool ValidateString(ref string value)
    {
        if (value == null)
            return false;
        string tmp = " " + value.Replace("(", " ( ").Replace(")", " ) ") + " ";
        if (value.ToLower().IndexOf(" and ") >= 0)
            return false;
        if (value.ToLower().IndexOf(" or ") >= 0)
            return false;
        if (value.ToLower().IndexOf(" true ") >= 0)
            return false;
        if (value.ToLower().IndexOf(" false ") >= 0)
            return false;
        value = value.Replace("'", "''");
        return true;
    }

    internal static string Truncate(string str, int maxLength)
    {
        if (string.IsNullOrEmpty(str))
            return "";
        return str.Length > maxLength ? str.Substring(0, maxLength) : str;
    }

    internal static string QuotedStr(string str)
    {
        if (str == null)
            return "null";
        return string.Format("'{0}'", str.Replace("'", "''"));
    }

    public static string GetString(object obj)
    {
        return GetString(obj, "");
    }
    public static string GetString(object obj, string defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        return obj.ToString();
    }

    public static string YearMonthToString(int year, int month)
    {
        string m;
        switch (month)
        {
            case 1:
                m = "فروردين";
                break;
            case 2:
                m = "ارديبهشت";
                break;
            case 3:
                m = "خرداد";
                break;
            case 4:
                m = "تير";
                break;
            case 5:
                m = "مرداد";
                break;
            case 6:
                m = "شهريور";
                break;
            case 7:
                m = "مهر";
                break;
            case 8:
                m = "آبان";
                break;
            case 9:
                m = "آذر";
                break;
            case 10:
                m = "دي";
                break;
            case 11:
                m = "بهمن";
                break;
            case 12:
                m = "اسفند";
                break;
            default:
                m = "اسفند";
                break;
        }
        return string.Format("{0} {1}", m, year);
    }
    public static string AppendToWhere(string where, string condition)
    {
        if (where == null || where.Trim().Length == 0)
            return condition;
        return string.Format("{0} AND {1}", where, condition);
    }


    public static int Compare(string a, string b)
    {
        if (a == null || b == null)
        {
            if (a == b)
                return 0;
            if (a == null)
                return 1;
            return -1;
        }
        return a.CompareTo(b);
    }

    public static void DownloadFile(string path, bool forceDownload)
    {
        //string path = HttpContext.Current.Server.MapPath(fname);
        string name = System.IO.Path.GetFileName(path);
        string ext = System.IO.Path.GetExtension(path);
        string type = "";
        // set known types based on file extension  
        if (ext != null)
        {
            switch (ext.ToLower())
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;

                case ".txt":
                    type = "text/plain";
                    break;

                case ".doc":
                case ".rtf":
                    type = "Application/msword";
                    break;
            }
        }
        if (forceDownload)
        {
            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + name);
        }
        if (type != "")
            HttpContext.Current.Response.ContentType = type;
        HttpContext.Current.Response.WriteFile(path);
        HttpContext.Current.Response.End();
    }
    public static void SetSelected(DropDownList dp, string value)
    {
        try
        {
            dp.SelectedValue = value;
        }
        catch
        {
        }
    }
    public static void SetSelected(DropDownList dp, int id)
    {
        try
        {
            dp.SelectedValue = id.ToString();
        }
        catch
        {
        }
    }
    public static void SetSelected(DropDownList dp, int? id)
    {
        if (!id.HasValue)
            return;
        try
        {
            dp.SelectedValue = id.ToString();
        }
        catch
        {
        }
    }

    public static string ToString(ServiceTypes st)
    {
        switch (st)
        {
            case ServiceTypes.Normal:
                return "تکی";
            case ServiceTypes.Fixed:
                return "ثابت";
            case ServiceTypes.AnotherDay:
                return "يکروز درميان از تاریخ شروع";
            case ServiceTypes.Singular:
                return "فوق العاده";
            default:
                break;
        }
        return "ثابت";
    }
    public static string ToString(PaymentTypes st)
    {
        switch (st)
        {
            case PaymentTypes.Pay:
                return "پرداخت";
            case PaymentTypes.Get:
                return "دريافت";
            case PaymentTypes.Advance:
                return "پيش پرداخت";
            case PaymentTypes.Payback:
                return "استرداد";
            default:
                break;
        }
        return "--";
    }
    //public static string ToString(Roles st)
    //{
    //    switch (st)
    //    {
    //        case Roles.Admin:
    //            return "Admin";
    //        case Roles.Owner:
    //            return "مالک";
    //        case Roles.Salesman:
    //            return "فروش";
    //        default:
    //            break;
    //    }
    //    return "--";
    //}
    public static bool IsChecked(HttpRequest request, string id)
    {
        if (request.Form == null || request.Form.AllKeys == null)
            return false;
        for (int i = 0; i < request.Form.AllKeys.Length; i++)
        {
            if (request.Form.AllKeys[i] != null && request.Form.AllKeys[i].EndsWith(id))
                return true;
        }

        return false;
    }
    public static string GetValue(HttpRequest request, string id)
    {
        if (request.Form == null || request.Form.AllKeys == null)
            return "";
        for (int i = 0; i < request.Form.AllKeys.Length; i++)
        {
            if (request.Form.AllKeys[i].EndsWith(id))
                return request.Form[request.Form.AllKeys[i]];
        }

        return "";
    }
    public static string DayToString(int i)
    {
        switch (i)
        {
            case 0:
                return "ش";
            case 1:
                return "1ش";
            case 2:
                return "2ش";
            case 3:
                return "3ش";
            case 4:
                return "4ش";
            case 5:
                return "5ش";
            case 6:
                return "ج";
            default:
                break;
        }
        return "ش";
    }
    public static string Three(int num)
    {
        string s = "";
        int h0, h1, h2;
        if (num == 0)
            return "";

        if (num == 100)
            return "يكصد";

        h0 = num / 100;
        h1 = (num % 100) / 10;
        h2 = num % 10;

        switch (h0)
        {
            case 1:
                s = "يكصد";
                break;
            case 2:
                s = "دويست";
                break;
            case 3:
                s = "سيصد";
                break;
            case 4:
                s = "چهارصد";
                break;
            case 5:
                s = "پانصد";
                break;
            case 6:
                s = "ششصد";
                break;
            case 7:
                s = "هفتصد";
                break;
            case 8:
                s = "هشتصد";
                break;
            case 9:
                s = "نهصد";
                break;
        }

        switch (h1)
        {
            case 1:

                switch (h2)
                {
                    case 0:
                        s = s + " و " + "ده";
                        break;
                    case 1:
                        s = s + " و " + "يازده";
                        break;
                    case 2:
                        s = s + " و " + "دوازده";
                        break;
                    case 3:
                        s = s + " و " + "سيزده";
                        break;
                    case 4:
                        s = s + " و " + "چهارده";
                        break;
                    case 5:
                        s = s + " و " + "پانزده";
                        break;
                    case 6:
                        s = s + " و " + "شانزده";
                        break;
                    case 7:
                        s = s + " و " + "هفده";
                        break;
                    case 8:
                        s = s + " و " + "هجده";
                        break;
                    case 9:
                        s = s + " و " + "نوزده";
                        break;
                }
                break;
            case 2:
                s = s + " و " + "بيست";
                break;
            case 3:
                s = s + " و " + "سي";
                break;
            case 4:
                s = s + " و " + "چهل";
                break;
            case 5:
                s = s + " و " + "پنجاه";
                break;
            case 6:
                s = s + " و " + "شصت";
                break;
            case 7:
                s = s + " و " + "هفتاد";
                break;
            case 8:
                s = s + " و " + "هشتاد";
                break;
            case 9:
                s = s + " و " + "نود";
                break;
        }
        if (h1 != 1)
        {
            switch (h2)
            {
                case 1:
                    s = s + " و " + "يك";
                    break;
                case 2:
                    s = s + " و " + "دو";
                    break;
                case 3:
                    s = s + " و " + "سه";
                    break;
                case 4:
                    s = s + " و " + "چهار";
                    break;
                case 5:
                    s = s + " و " + "پنج";
                    break;
                case 6:
                    s = s + " و " + "شش";
                    break;
                case 7:
                    s = s + " و " + "هفت";
                    break;
                case 8:
                    s = s + " و " + "هشت";
                    break;
                case 9:
                    s = s + " و " + "نه";
                    break;
            }
        }
        if (num < 100)
            s = s.Substring(3, s.Length - 3);

        return s;
    }

    public static string NumberToFarsi(int number)
    {
        if (number == 0)
            return "صفر";
        bool flag = false; ;
        string s = "";
        int i, l;
        int[] k = { 0, 0, 0, 0, 0 };//Dim k() As Integer = {0, 0, 0, 0, 0}
        s = number.ToString().Trim();
        l = s.Length;
        if (l > 15)
            return "بسيار بزرگ";
        for (i = 1; i < 15 - l; i++)
            s = "0" + s;
        int x = l % 3;
        if (x > 0) x = 1;
        for (i = 1; i <= (int)((l / 3) + x); i++)
            k[5 - i] = int.Parse(s.Substring(3 * (5 - i) - 1, 3));
        s = "";
        for (i = 0; i <= 4; i++)
        {
            if (k[i] != 0)
            {
                switch (i)
                {
                    case 0:
                        s = s + Three(k[i]) + " تريليون";
                        break;
                    case 1:
                        if (flag)
                            s = s + " و " + Three(k[i]) + " ميليارد";
                        else
                            s = s + Three(k[i]) + " ميليارد";
                        flag = true;
                        break;
                    case 2:
                        if (flag)
                            s = s + " و " + Three(k[i]) + " ميليون";
                        else
                            s = s + Three(k[i]) + " ميليون";

                        flag = true;
                        break;
                    case 3:
                        if (flag)
                            s = s + " و " + Three(k[i]) + " هزار";
                        else
                            s = s + Three(k[i]) + " هزار";
                        flag = true;
                        break;
                    case 4:
                        if (flag)
                            s = s + " و " + Three(k[i]);
                        else
                            s = s + Three(k[i]);
                        break;
                }
            }
        }
        return s;
    }
    public static string ToPersianDateRtl(object date, string defaultValue)
    {
        if (Null.IsNull(date))
            return defaultValue;
        DateTime dt; //= Convert.ToDateTime(valueInfo.Value);
        if (DateTime.TryParse(date.ToString(), out dt))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            return string.Format("{2}/{1}/{0}", ToString(calendar.GetYear(dt), 4)
                    , ToString(calendar.GetMonth(dt), 2), ToString(calendar.GetDayOfMonth(dt), 2));
        }
        return defaultValue;
    }

    public static bool CanMini()
    {
        return System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/images/Mini.gif"));
    }
    public static bool CanSewari()
    {
        return System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/images/Sewari.gif"));
    }
    public static bool CanBus()
    {
        return System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/images/Bus.gif"));
    }
    public static bool ButtonIsClicked(HttpRequest request, Button btn)
    {
        string clientID = btn.ClientID.Replace('_', '$');

        for (int i = 0; i < request.Form.AllKeys.Length; i++)
        {
            if (request.Form.AllKeys[i] == clientID)
                return true;
        }
        clientID = '$' + btn.ID;
        for (int i = 0; i < request.Form.AllKeys.Length; i++)
        {
            if (request.Form.AllKeys[i] != null && request.Form.AllKeys[i].EndsWith(clientID))
                return true;
        }
        return false;
    }
    public static string ParseSeries(string series)
    {
        if (string.IsNullOrEmpty(series))
            return "";
        string[] ss = series.Split('/', '-');
        if (ss.Length == 3)
            return string.Format("{0},{1},{2}", ss[0], ss[1], ss[2]);
        if (ss.Length == 4)
            return string.Format("{0},{1},{2},{3}", ss[1], ss[2], ss[3], ss[0]);
        return series;

    }
    public static string ConvertPlate(string old)
    {
        if (string.IsNullOrEmpty(old))
            return old;
        string copy = old.Replace(" ", "");
        copy = copy.Replace("الف", "ا");
        string[] split = copy.Split('-');
        if (split.Length == 1)
            return "";
        if (split.Length == 2)
        {
            if (Tool.GetInt(split[0].PadLeft(6, '0').Substring(3, 3), Null.NullInteger) == Null.NullInteger)
                return string.Format("{0}-{1}-{2}-{3}", split[1]
                , split[0].PadLeft(6, '0').Substring(0, 3)
                , split[0].PadLeft(6, '0').Substring(3, 1).Replace("0", "ع")
                , split[0].PadLeft(6, '0').Substring(4, 2)).Replace("ا", "الف");
            return string.Format("{0}-{1}-{2}-{3}", split[1]
                , split[0].PadLeft(6, '0').Substring(3, 3)
                , split[0].PadLeft(6, '0').Substring(2, 1).Replace("0", "ع")
                , split[0].PadLeft(6, '0').Substring(0, 2)).Replace("ا", "الف");
        }
        if (split.Length == 4)
            return old;
        return "";
    }
    public static string GetDay(DateTime dateTime)
    {
        if (Null.NullDate == dateTime)
            return "";
        switch (dateTime.DayOfWeek)
        {
            case DayOfWeek.Friday:
                return "جمعه";
            case DayOfWeek.Monday:
                return "دوشنبه";
            case DayOfWeek.Saturday:
                return "شنبه";
            case DayOfWeek.Sunday:
                return "یکشنبه";
            case DayOfWeek.Thursday:
                return "پنج شنبه";
            case DayOfWeek.Tuesday:
                return "سه شنبه";
            case DayOfWeek.Wednesday:
                return "چهارشنبه";
        }
        return "";
    }
}