﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Summary description for JsTools
/// </summary>
public class JsTools
{
    public static void DefaultButton(Page page, WebControl textControl, string defaultButtonClientID)
    {
        // Sets default buttons.
        // Created by Janus Kamp Hansen - http://www.kamp-hansen.dk
        StringBuilder sScript = new StringBuilder();
        sScript.Append("<SCRIPT language='javascript'>\n");
        sScript.Append("function fnTrapKD(btn){\n");
        sScript.Append(" if (document.all){\n");
        sScript.Append("   if (event.keyCode == 13)\n");
        sScript.Append("   { \n");
        sScript.Append("     event.returnValue=false;\n");
        sScript.Append("     event.cancel = true;\n");
        sScript.Append("     btn.click();\n");
        sScript.Append("   } \n");
        sScript.Append(" } \n");
        sScript.Append("}\n");
        sScript.Append("</SCRIPT>\n");

        textControl.Attributes.Add("onkeydown", "fnTrapKD(document.all." + defaultButtonClientID + ")");
        page.RegisterStartupScript("ForceDefaultToScript", sScript.ToString());
    }
    public static void SetFocus(Control control)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function SetFocus()\r\n");
        sb.Append("{\r\n");
        sb.Append("\t$get('");
        sb.Append(control.ClientID);
        sb.Append("').focus();\r\n");
        sb.Append("}\r\n");
        sb.Append("window.onload = SetFocus;\r\n");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        //control.Page.un
        if (control.Page.ClientScript.IsClientScriptBlockRegistered("SetFocus"))
            control.Page.RegisterClientScriptBlock("SetFocus2", sb.ToString().Replace("SetFocus", "SetFocus2"));
        else
            control.Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
    }
    public static void SetFocusSelect(Control control)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function SetFocus()\r\n");
        sb.Append("{\r\n");
        sb.Append("\t$get('");
        sb.Append(control.ClientID);
        sb.Append("').focus();\r\n");
        sb.Append("\t$get('");
        sb.Append(control.ClientID);
        sb.Append("').select();\r\n");
        sb.Append("}\r\n");
        sb.Append("window.onload = SetFocus;\r\n");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        //control.Page.un
        if (control.Page.ClientScript.IsClientScriptBlockRegistered("SetFocus"))
            control.Page.RegisterClientScriptBlock("SetFocus2", sb.ToString().Replace("SetFocus", "SetFocus2"));
        else
            control.Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
    }
    public static void SetFocus(Page page, string id)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function SetFocus()\r\n");
        sb.Append("{\r\n");
        sb.Append("\t$get('");
        sb.Append(id);
        sb.Append("').focus();\r\n");
        sb.Append("}\r\n");
        sb.Append("window.onload = SetFocus;\r\n");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        //control.Page.un
        if (page.ClientScript.IsClientScriptBlockRegistered("SetFocus"))
            page.RegisterClientScriptBlock("SetFocus2", sb.ToString().Replace("SetFocus", "SetFocus2"));
        else
            page.RegisterClientScriptBlock("SetFocus", sb.ToString());
    }
    public static void HandleDeleteButton(GridViewRowEventArgs e)
    {
        try
        {
            TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
            LinkButton delete = cc.Controls[0] as LinkButton;
            if (delete != null)
                delete.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "Are rou sure you want delete this record"));
        }
        catch
        {
        }
    }
    public static void BindDefaultHotKeysF(Page page, Button doNew, Button puOk, Button puCancel)
    {
        List<string> clientIDs = new List<string>();
        List<int> keyCodes = new List<int>();
        clientIDs.Add(doNew.ClientID);
        clientIDs.Add(puOk.ClientID);
        clientIDs.Add(puCancel.ClientID);
        keyCodes.Add(45);
        keyCodes.Add(118);
        keyCodes.Add(27);
        BindHotKeysF(page, clientIDs, keyCodes);
    }
    public static void BindHotKeysF(Page page, List<string> clientIDs, List<int> keyCodes)
    {
        if (clientIDs == null /*|| clientIDs.Count == 0*/ || keyCodes == null || clientIDs.Count != keyCodes.Count)
            return;
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function keyPressHand(e) { \n");
        builder.Append("    var e = event;\n");
        builder.Append("    if (!window.getSelection) e = window.event;\n");
        builder.Append("    key = e.keyCode;\n");
        builder.Append("    if (key == 119)\n");
        builder.Append("        window.location = 'QQTrips.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        if (Tool.CanMini() || Tool.CanSewari())
        {
            builder.Append("    if (key == 117)\n");
            builder.Append("        window.location = 'QQSale2.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        }
        //builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.click();\n");
            builder.AppendFormat("window.event.keyCode=0;\n");
            builder.AppendFormat("break;\n");
        }
        builder.Append("    }\n");
        builder.Append("return;\n");
        builder.Append("}\n");
        builder.Append("document.onkeydown=keyPressHand;\n");
        builder.Append("    </script>\n");
        page.RegisterStartupScript("HotKeyScript", builder.ToString());
    }
    public static void BindHotKeys(Page page, List<string> clientIDs, List<int> keyCodes)
    {
        if (clientIDs == null || clientIDs.Count == 0 || keyCodes == null || clientIDs.Count != keyCodes.Count)
            return;
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function HotKeyEvent() {\n");
        builder.Append("    var e2 = event.srcElement.tagName\n");
        builder.Append("    if (e2 == 'INPUT' || e2 == 'TEXTAREA' || e2 == 'SELECT' || e2 == 'input' || e2 == 'textarea' || e2 == 'select')\n");
        builder.Append("    {\n");
        builder.Append("        return;\n");
        builder.Append("    }\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.click();\n");
            builder.AppendFormat("break;\n");
        }
        //case 33:
        //var b = document.getElementById("Button1");
        //b.click();
        //break;
        builder.Append("    }\n");
        builder.Append("}\n");

        builder.Append("document.onkeypress = HotKeyEvent;\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        page.RegisterStartupScript("HotKeyScript", builder.ToString());
    }
    public static void BindHotKeysFocus(Page page, List<string> clientIDs, List<int> keyCodes)
    {
        if (clientIDs == null /*|| clientIDs.Count == 0*/ || keyCodes == null || clientIDs.Count != keyCodes.Count)
            return;
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function keyPressHand(event) { \n");
        builder.Append("    var e = event;\n");
        builder.Append("    if (!window.getSelection) e = window.event;\n");
        builder.Append("    key = e.keyCode;\n");
        builder.Append("    if (key == 119)\n");
        builder.Append("        window.location = 'QQTrips.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        if (Tool.CanMini() || Tool.CanSewari())
        {
            builder.Append("    if (key == 117)\n");
            builder.Append("        window.location = 'QQSale2.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        }
        //builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.focus();\n");
            builder.AppendFormat("window.event.keyCode=0;\n");
            builder.AppendFormat("break;\n");
        }
        builder.Append("    }\n");
        builder.Append("return;\n");
        builder.Append("}\n");
        builder.Append("document.onkeydown=keyPressHand;\n");
        builder.Append("    </script>\n");
        page.RegisterStartupScript("HotKeyScript", builder.ToString());
    }
    public static void BindHotKeysFocus2(Page page, List<string> clientIDs, List<int> keyCodes)
    {
        if (clientIDs == null /*|| clientIDs.Count == 0*/ || keyCodes == null || clientIDs.Count != keyCodes.Count)
            return;
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function keyPressHand2(event) { \n");
        builder.Append("    var e = event;\n");
        builder.Append("    if (!window.getSelection) e = window.event;\n");
        builder.Append("    key = e.keyCode;\n");
        builder.Append("    if (key == 119)\n");
        builder.Append("        window.location = 'QQTrips.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        if (Tool.CanMini() || Tool.CanSewari())
        {
            builder.Append("    if (key == 117)\n");
            builder.Append("        window.location = 'QQSale2.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        }
        //builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.focus();\n");
            builder.AppendFormat("window.event.keyCode=0;\n");
            builder.AppendFormat("break;\n");
        }
        builder.Append("    }\n");
        builder.Append("return;\n");
        builder.Append("}\n");
        builder.Append("document.onkeydown=keyPressHand2;\n");
        builder.Append("    </script>\n");
        page.RegisterStartupScript("HotKeyScript2", builder.ToString());
    }
    public static void RegisterScanScript(Button ctl, string cmd, string id, bool workAround, string append)
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function FUNC()\n");
        builder.Append("{\n");
        builder.Append("    var browser=navigator.appVersion;\n");
        builder.Append("    if (browser.indexOf('Windows NT 5.2')>-1)\n");
        builder.Append("    {\n");
        builder.Append("      FXP();\n");
        builder.Append("    }\n");
        builder.Append("    else if (browser.indexOf('Windows NT 5.1')>-1)\n");
        builder.Append("    {\n");
        builder.Append("      FXP();\n");
        builder.Append("    }\n");
        builder.Append("    else\n");
        builder.Append("    {\n");
        builder.Append(string.Format("      window.open('file:///C:/CardReader.exe {0} {1}', 1);\n", cmd, id));
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("function FXP() \n");
        builder.Append("{ \n");
        builder.Append("    var shell = new ActiveXObject('WScript.shell');\n");
        builder.Append(string.Format("    shell.run('c:\\CardReader.exe {0} {1}', 1, true); \n", cmd, id));
        builder.Append("}\n");
        builder.Append("    </script>\n");
        builder = builder.Replace("FUNC", "myRun" + append);
        builder = builder.Replace("FXP", "FXP" + append);
        if (workAround)
        {
            //builder =
            //    builder.Replace("runScanApp", "runScanApp2");
            ctl.OnClientClick = "myRun" + append + "()";
            ctl.Page.RegisterStartupScript("ScaningScript2" + append, builder.ToString());
        }
        else
        {
            ctl.OnClientClick = "myRun" + append + "()";
            ctl.Page.RegisterStartupScript("ScaningScript" + append, builder.ToString());
        }
    }


}
