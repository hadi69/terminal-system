﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{
    public WebService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public bool SetDriver(string id, string driverName)
    {
        {
            Card c = new Card();
            c.ID = id;
            c.DriverName = driverName;
            Helper.Instance.DB.Cards.InsertOnSubmit(c);
            return Helper.Instance.Update();
        }
    }
    [WebMethod]
    public bool ReadTrip(string id, int SooratVazeatSerialNo, short ServeesType, int DateOfSodoor, short TimeOfSodoor
        , int Company, int Source, int Destination, int DriverCartNo1, int DriverCartNo2, short MosaferCount
        , int Price, int UserID)
    {
        Card c = new Card();
        c.ID = id;
        c.SooratVazeatSerialNo = SooratVazeatSerialNo;
        c.ServeesType = ServeesType;
        c.DateOfSodoor = DateOfSodoor;
        c.TimeOfSodoor = TimeOfSodoor;
        c.Company = Company;
        c.Source = Source;
        c.Destination = Destination;
        c.DriverCartNo1 = DriverCartNo1;
        c.DriverCartNo2 = DriverCartNo2;
        c.MosaferCount = MosaferCount;
        c.Price = Price;
        c.UserID = UserID;
        //using (Helper Helper = new Helper())
        {
            Helper.Instance.DB.Cards.InsertOnSubmit(c);
            return Helper.Instance.Update();
        }
    }

}

