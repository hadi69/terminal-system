﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for ExportTool
/// </summary>
public class ExportTool
{
    public static void ToExcel(HttpResponse response, GridView grid)
    {
        Export(response, grid, null, false);
    }
    public static void ToExcel(HttpResponse response, DataTable dt)
    {
        Export(response, dt, null, false);
    }
    public static void ToWord(HttpResponse response, GridView grid)
    {
        Export(response, grid, null, true);
    }
    public static void ToWord(HttpResponse response, GridView grid, string topRow)
    {
        Export(response, grid, topRow, true);
    }
    public static void ToWord(HttpResponse response, List<GridView> grids, string topRow)
    {
        Export(response, grids, topRow, true);
    }
    public static void ToWord(HttpResponse response, DataTable dt)
    {
        Export(response, dt, null, true);
    }
    public static void ToWord(HttpResponse response, DataTable dt, string topRow)
    {
        Export(response, dt, topRow, true);
    }
    private static void Export(HttpResponse response, GridView grid, string topRow, bool isWord)
    {
        response.Clear();
        if (isWord)
            response.AddHeader("content-disposition", "attachment;filename=numFiles.doc");
        else
            response.AddHeader("content-disposition", "attachment;filename=numFiles.xls");
        response.Charset = "Windows-1256";

        // If you want the option to open the Excel file without saving than
        // comment out the line below
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (isWord)
            response.ContentType = "application/vnd.doc";
        else
            response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        stringWrite.Write("<table border='1' cellpadding='0' cellspacing='0' dir='rtl'>");
        if (!string.IsNullOrEmpty(topRow))
        {
            int tdCount = 0;
            if (grid.Columns.Count > 0)
            {
                // the columns are defined staticaly
                for (int i = 0; i < grid.Columns.Count; i++)
                    if (grid.Columns[i].Visible)
                        tdCount++;
            }
            else
            {
                // the columns are generated automaticaly
                if (grid.HeaderRow != null)
                    tdCount = grid.HeaderRow.Cells.Count;
            }
            tdCount = Math.Max(1, tdCount);
            stringWrite.Write(string.Format("<tr><td colspan='{0}'>", tdCount));
            stringWrite.Write(topRow);
            stringWrite.Write("</td></tr>");
        }
        // ---------------------
        // Header
        #region Header
        stringWrite.Write("<tr>");
        if (grid.Columns.Count > 0)
        {
            // the columns are defined staticaly
            for (int i = 0; i < grid.Columns.Count; i++)
                if (grid.Columns[i].Visible)
                {
                    stringWrite.Write("<td>");
                    stringWrite.Write(grid.Columns[i].HeaderText);
                    stringWrite.Write("</td>");
                }
        }
        else
        {
            // the columns are generated automaticaly
            if (grid.HeaderRow != null)
                for (int i = 0; i < grid.HeaderRow.Cells.Count; i++)
                {
                    stringWrite.Write("<td>");
                    stringWrite.Write(grid.HeaderRow.Cells[i].Text);
                    stringWrite.Write("</td>");
                }
        }
        stringWrite.Write("</tr>");
        #endregion

        // Rows
        #region Rows
        for (int i = 0; i < grid.Rows.Count; i++)
        {
            stringWrite.Write("<tr>");
            for (int j = 0; j < grid.HeaderRow.Cells.Count; j++)
            {
                bool show = true;
                if (grid.Columns.Count > 0)
                    show = grid.Columns[j].Visible;
                if (show)
                {
                    stringWrite.Write("<td>");
                    if (grid.Rows[i].Cells[j].Controls.Count > 0)
                    {
                        if (grid.Rows[i].Cells[j].Controls[0] is HyperLink)
                        {
                            stringWrite.Write((grid.Rows[i].Cells[j].Controls[0] as HyperLink).Text);
                        }
                        else if (grid.Rows[i].Cells[j].Controls[0] is CheckBox)
                        {
                            if ((grid.Rows[i].Cells[j].Controls[0] as CheckBox).Checked)
                                stringWrite.Write("+");
                            else
                                stringWrite.Write("-");
                        }
                        else
                        {
                        }
                    }
                    else
                        stringWrite.Write(grid.Rows[i].Cells[j].Text);
                    stringWrite.Write("</td>");
                }
            }
            stringWrite.Write("</tr>");
        }
        #endregion

        stringWrite.Write("</table>");

        // changed for Unicode
        // Response.Write(stringWrite.ToString());
        byte[] converted = System.Text.Encoding.GetEncoding("Windows-1256").GetBytes(stringWrite.ToString());
        response.Write("<html><head> <meta charset='windows-1256'></head><body>");
        if (converted.Length > 0)
            response.BinaryWrite(converted);
        else
        {

        }
        response.Write("</body></html>");
    }
    private static void Export(HttpResponse response, List<GridView> grids, string topRow, bool isWord)
    {
        response.Clear();
        if (isWord)
            response.AddHeader("content-disposition", "attachment;filename=numFiles.doc");
        else
            response.AddHeader("content-disposition", "attachment;filename=numFiles.xls");
        response.Charset = "Windows-1256";

        // If you want the option to open the Excel file without saving than
        // comment out the line below
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (isWord)
            response.ContentType = "application/vnd.doc";
        else
            response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        foreach (GridView grid in grids)
        {

            stringWrite.Write("<table border='1' cellpadding='0' cellspacing='0' dir='rtl'>");
            if (!string.IsNullOrEmpty(topRow))
            {
                int tdCount = 0;
                if (grid.Columns.Count > 0)
                {
                    // the columns are defined staticaly
                    for (int i = 0; i < grid.Columns.Count; i++)
                        if (grid.Columns[i].Visible)
                            tdCount++;
                }
                else
                {
                    // the columns are generated automaticaly
                    if (grid.HeaderRow != null)
                        tdCount = grid.HeaderRow.Cells.Count;
                }
                tdCount = Math.Max(1, tdCount);
                stringWrite.Write(string.Format("<tr><td colspan='{0}'>", tdCount));
                stringWrite.Write(topRow);
                stringWrite.Write("</td></tr>");
            }
            // ---------------------
            // Header
            #region Header
            stringWrite.Write("<tr>");
            if (grid.Columns.Count > 0)
            {
                // the columns are defined staticaly
                for (int i = 0; i < grid.Columns.Count; i++)
                    if (grid.Columns[i].Visible)
                    {
                        stringWrite.Write("<td>");
                        stringWrite.Write(grid.Columns[i].HeaderText);
                        stringWrite.Write("</td>");
                    }
            }
            else
            {
                // the columns are generated automaticaly
                if (grid.HeaderRow != null)
                    for (int i = 0; i < grid.HeaderRow.Cells.Count; i++)
                    {
                        stringWrite.Write("<td>");
                        stringWrite.Write(grid.HeaderRow.Cells[i].Text);
                        stringWrite.Write("</td>");
                    }
            }
            stringWrite.Write("</tr>");
            #endregion

            // Rows
            #region Rows
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                stringWrite.Write("<tr>");
                for (int j = 0; j < grid.HeaderRow.Cells.Count; j++)
                {
                    bool show = true;
                    if (grid.Columns.Count > 0)
                        show = grid.Columns[j].Visible;
                    if (show)
                    {
                        stringWrite.Write("<td>");
                        if (grid.Rows[i].Cells[j].Controls.Count > 0)
                        {
                            if (grid.Rows[i].Cells[j].Controls[0] is HyperLink)
                            {
                                stringWrite.Write((grid.Rows[i].Cells[j].Controls[0] as HyperLink).Text);
                            }
                            else if (grid.Rows[i].Cells[j].Controls[0] is CheckBox)
                            {
                                if ((grid.Rows[i].Cells[j].Controls[0] as CheckBox).Checked)
                                    stringWrite.Write("+");
                                else
                                    stringWrite.Write("-");
                            }
                            else
                            {
                            }
                        }
                        else
                            stringWrite.Write(grid.Rows[i].Cells[j].Text);
                        stringWrite.Write("</td>");
                    }
                }
                stringWrite.Write("</tr>");
            }
            #endregion

            stringWrite.Write("</table>");
            stringWrite.Write("<br><br>");
        }

        // changed for Unicode
        // Response.Write(stringWrite.ToString());
        byte[] converted = System.Text.Encoding.GetEncoding("Windows-1256").GetBytes(stringWrite.ToString());
        response.Write("<html><head> <meta charset='windows-1256'></head><body>");
        if (converted.Length > 0)
            response.BinaryWrite(converted);
        else
        {

        }
        response.Write("</body></html>");
    }
    private static void Export(HttpResponse response, DataTable dt, string topRow, bool isWord)
    {
        response.Clear();
        if (isWord)
            response.AddHeader("content-disposition", "attachment;filename=numFiles.doc");
        else
            response.AddHeader("content-disposition", "attachment;filename=numFiles.xls");
        response.Charset = "Windows-1256";

        // If you want the option to open the Excel file without saving than
        // comment out the line below
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (isWord)
            response.ContentType = "application/vnd.doc";
        else
            response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        stringWrite.Write("<table border='1' cellpadding='0' cellspacing='0' dir='rtl'>");

        if (!string.IsNullOrEmpty(topRow))
        {
            int tdCount = dt.Columns.Count;
            tdCount = Math.Max(1, tdCount);
            stringWrite.Write(string.Format("<tr><td colspan='{0}'>", tdCount));
            stringWrite.Write(topRow);
            stringWrite.Write("</td></tr>");
        }
        // ---------------------
        // Header
        #region Header
        stringWrite.Write("<tr>");
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            stringWrite.Write("<td>");
            stringWrite.Write(dt.Columns[i].ColumnName);
            stringWrite.Write("</td>");
        }
        stringWrite.Write("</tr>");
        #endregion

        // Rows
        #region Rows
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            stringWrite.Write("<tr>");
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                stringWrite.Write("<td>");
                stringWrite.Write(Tool.GetString(dt.Rows[i][j], ""));
                stringWrite.Write("</td>");
            }
            stringWrite.Write("</tr>");
        }
        #endregion

        stringWrite.Write("</table>");

        // changed for Unicode
        // Response.Write(stringWrite.ToString());
        byte[] converted = System.Text.Encoding.GetEncoding("Windows-1256").GetBytes(stringWrite.ToString());
        response.Write("<html><head> <meta charset='windows-1256'></head><body>");
        if (converted.Length > 0)
            response.BinaryWrite(converted);
        else
        {

        }
        response.Write("</body></html>");
    }
    public static void ToXml(HttpResponse response, GridView grid)
    {
        response.Clear();
        response.AddHeader("content-disposition", "attachment;filename=FileName.xml");
        response.Charset = "Windows-1256";

        // If you want the option to open the Excel file without saving than
        // comment out the line below
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        stringWrite.WriteLine("<?xml version='1.0' encoding='utf-8' ?>");
        stringWrite.WriteLine("<File>");

        // ---------------------
        // Header
        #region Header
        stringWrite.WriteLine("<columns>");
        for (int i = 0; i < grid.Columns.Count; i++)
            if (grid.Columns[i].Visible)
            {
                stringWrite.WriteLine("<col>");
                stringWrite.WriteLine(grid.Columns[i].HeaderText);
                stringWrite.WriteLine("</col>");
            }
        stringWrite.Write("</columns>");
        #endregion

        // Rows
        #region Rows
        for (int i = 0; i < grid.Rows.Count; i++)
        {
            stringWrite.WriteLine("<row>");
            for (int j = 0; j < grid.Columns.Count; j++)
                if (grid.Columns[j].Visible)
                {
                    stringWrite.WriteLine("<col>");
                    if (grid.Rows[i].Cells[j].Controls.Count > 0)
                    {
                        if (grid.Rows[i].Cells[j].Controls[0] is HyperLink)
                        {
                            stringWrite.WriteLine((grid.Rows[i].Cells[j].Controls[0] as HyperLink).Text.Replace("&nbsp;", " "));
                        }
                        else if (grid.Rows[i].Cells[j].Controls[0] is CheckBox)
                        {
                            if ((grid.Rows[i].Cells[j].Controls[0] as CheckBox).Checked)
                                stringWrite.WriteLine("+");
                            else
                                stringWrite.WriteLine("-");
                        }
                        else
                        {
                        }
                    }
                    else
                        stringWrite.WriteLine(grid.Rows[i].Cells[j].Text.Replace("&nbsp;", " "));
                    stringWrite.WriteLine("</col>");
                }
            stringWrite.WriteLine("</row>");
        }
        #endregion

        stringWrite.Write("</File>");

        response.Write(stringWrite.ToString());
    }

    public static void ToXml(HttpResponse response, DataTable dt)
    {
        response.Clear();
        response.AddHeader("content-disposition", "attachment;filename=FileName.xml");
        response.Charset = "Windows-1256";

        // If you want the option to open the Excel file without saving than
        // comment out the line below
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        stringWrite.WriteLine("<?xml version='1.0' encoding='utf-8' ?>");
        stringWrite.WriteLine("<File>");

        // ---------------------
        // Header
        #region Header
        stringWrite.WriteLine("<columns>");
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            stringWrite.WriteLine("<col>");
            stringWrite.WriteLine(dt.Columns[i].ColumnName);
            stringWrite.WriteLine("</col>");
        }
        stringWrite.Write("</columns>");
        #endregion

        // Rows
        #region Rows
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            stringWrite.WriteLine("<row>");
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                stringWrite.WriteLine("<col>");
                stringWrite.WriteLine(Tool.GetString(dt.Rows[i][j], " ").Replace("&nbsp;", " "));
                stringWrite.WriteLine("</col>");
            }
            stringWrite.WriteLine("</row>");
        }
        #endregion

        stringWrite.Write("</File>");

        response.Write(stringWrite.ToString());
    }
}
