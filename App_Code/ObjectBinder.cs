﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Reflection;
/// <summary>
/// Class that hydrates custom business objects with data. Please
/// note that this utility class can only be used on objects with "simple"
/// data types. If the object contains "complex" data types such as 
/// ArrayLists, HashTables, Custom Objects, etc... then the developer 
/// will need to write custom code for the hydration of these "complex" 
/// data types.
/// </summary>
public class ObjectBinder
{
    /// <summary>
    /// Get the list of properties for a given type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static List<PropertyInfo> GetPropertyInfo(Type type)
    {
        // Use the cache because the reflection used later is expensive
        List<PropertyInfo> properties = System.Web.HttpContext.Current.Cache[type.FullName] as List<PropertyInfo>;
        if (properties == null)
        {
            properties = new List<PropertyInfo>(type.GetProperties());
            System.Web.HttpContext.Current.Cache.Add(type.FullName, properties,
                null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        return properties;
    }
    /// <summary>
    /// get the ordinal (the index in an array or list) for the properties. It then can be used to 
    /// read data from the IDataReader in more speed
    /// </summary>
    /// <param name="properties"></param>
    /// <param name="dr"></param>
    /// <returns></returns>
    private static int[] GetOrdinals(List<PropertyInfo> properties, IDataReader dr)
    {
        int[] ordinals = new int[properties.Count];

        if (dr != null)
            for (int i = 0; i < properties.Count; i++)
            {
                ordinals[i] = -1;
                try
                {
                    ordinals[i] = dr.GetOrdinal(properties[i].Name);
                }
                catch
                {
                    // property does not exist in datareader
                }
            }
        return ordinals;
    }
    /// <summary>
    /// create an object T by using its default constructor and fill its properties
    /// </summary>
    /// <typeparam name="T">generic type</typeparam>
    /// <param name="dr"></param>
    /// <param name="properties"></param>
    /// <param name="ordinals"></param>
    /// <returns></returns>
    private static T CreateObject<T>(IDataReader dr, List<PropertyInfo> properties, int[] ordinals)
    {
        T t = Activator.CreateInstance<T>();

        // fill object with values from datareader
        for (int i = 0; i < properties.Count; i++)
        {
            #region Check Posiibility
            if (!properties[i].CanWrite)
                continue;
            object value = Null.SetNull(properties[i]);

            // see if the property exists in datareader 
            // or it is DBNull
            if (ordinals[i] == -1 || (dr.GetValue(ordinals[i]) is DBNull))
            {
                properties[i].SetValue(t, value, null);
                continue;
            }
            #endregion

            try
            {
                // try implicit conversion first
                properties[i].SetValue(t, dr.GetValue(ordinals[i]), null);
            }
            catch
            {
                // business object info class member data type does not match datareader member data type
                try
                {
                    // need to handle enumeration conversions differently than other base types
                    if (properties[i].PropertyType.BaseType.Equals(typeof(System.Enum)))
                    {
                        // check if value is numeric and if not convert to integer ( supports databases like Oracle )
                        if (IsNumeric(dr.GetValue(ordinals[i])))
                            properties[i].SetValue(t, System.Enum.ToObject(properties[i].PropertyType, Convert.ToInt32(dr.GetValue(ordinals[i]))), null);
                        else
                            properties[i].SetValue(t, System.Enum.ToObject(properties[i].PropertyType, dr.GetValue(ordinals[i])), null);
                    }
                    else
                        // try explicit conversion
                        properties[i].SetValue(t, Convert.ChangeType(dr.GetValue(ordinals[i]), properties[i].PropertyType), null);
                }
                catch
                {
                    properties[i].SetValue(t, Convert.ChangeType(dr.GetValue(ordinals[i]), properties[i].PropertyType), null);
                }
            }
        }

        return t;
    }
    private static bool IsNumeric(object obj)
    {
        double tmp;
        return double.TryParse(obj.ToString(), out tmp);
    }
    /// <summary>
    /// create an object T by using its default constructor and fill its properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dr"></param>
    /// <returns></returns>
    public static T FillObject<T>(IDataReader dr)
    {
        return FillObject<T>(dr, true);
    }
    /// <summary>
    /// create an object T by using its default constructor and fill its properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dr"></param>
    /// <param name="ManageDataReader">if true, advances the dr then closes it</param>
    /// <returns></returns>
    public static T FillObject<T>(IDataReader dr, bool ManageDataReader)
    {
        T t = default(T);
        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // get ordinal positions in datareader
        int[] ordinals = GetOrdinals(properties, dr);

        if (ManageDataReader)
        {
            // read datareader                    
            if (dr.Read())
                // create custom business object
                t = CreateObject<T>(dr, properties, ordinals);
            // close datareader
            if (dr != null)
                dr.Close();
        }
        else
            // create custom business object
            t = CreateObject<T>(dr, properties, ordinals);

        return t;
    }
    /// <summary>
    /// create a list of objects T by using its default constructor and fill its properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dr"></param>
    /// <returns></returns>
    public static List<T> FillCollection<T>(IDataReader dr)
    {
        List<T> res = new List<T>();
        T t;

        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // get ordinal positions in datareader
        int[] ordinals = GetOrdinals(properties, dr);

        // iterate datareader
        while (dr.Read())
        {
            // fill business object
            t = CreateObject<T>(dr, properties, ordinals);
            // add to collection
            res.Add(t);
        }

        // close datareader
        if (dr != null)
            dr.Close();

        return res;
    }
    /// <summary>
    /// appendsto the list a list of objects T by using its default constructor and fill its properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dr"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static IList<T> FillCollection<T>(IDataReader dr, IList<T> list)
    {
        T t;

        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // get ordinal positions in datareader
        int[] ordinals = GetOrdinals(properties, dr);

        // iterate datareader
        while (dr.Read())
        {
            // fill business object
            t = CreateObject<T>(dr, properties, ordinals);
            // add to collection
            list.Add(t);
        }

        // close datareader
        if (dr != null)
            dr.Close();

        return list;
    }
    /// <summary>
    /// fills all the properties of T with default values
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    /// <returns></returns>
    public static T InitializeObject<T>(T t)
    {
        object value;

        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // initialize properties
        for (int i = 0; i < properties.Count; i++)
            if (properties[i].CanWrite)
            {
                value = Null.SetNull(properties[i]);
                properties[i].SetValue(t, value, null);
            }

        return t;
    }


}