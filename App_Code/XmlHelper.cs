﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

/// <summary>
/// Summary description for XmlHelper
/// </summary>
public class XmlHelper
{
    public static System.Globalization.CultureInfo NumberCulture = new System.Globalization.CultureInfo("en-US");
    public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, object attrValue)
    {
        XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
        attr.Value = attrValue == null ? "" : attrValue.ToString();
        return attr;
    }
    public static string GetString(XmlNode node, string attrName, string defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return node.Attributes[attrName].Value;
        }
        catch { }
        return defaultValue;
    }
    public static bool GetBool(XmlNode node, string attrName, bool defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return bool.Parse(node.Attributes[attrName].Value.ToLower());
        }
        catch { }
        return defaultValue;
    }
    public static float GetFloat(XmlNode node, string attrName, float defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return float.Parse(node.Attributes[attrName].Value, NumberCulture);
        }
        catch { }
        return defaultValue;
    }
    public static int GetInt(XmlNode node, string attrName, int defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return int.Parse(node.Attributes[attrName].Value, NumberCulture);
        }
        catch { }
        return defaultValue;
    }
    public static double GetDouble(XmlNode node, string attrName, double defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return Convert.ToDouble(node.Attributes[attrName].Value, NumberCulture);
        }
        catch { }
        return defaultValue;
    }
    public static T GetEnum<T>(XmlNode node, string attrName, T defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return (T)Enum.Parse(typeof(T), node.Attributes[attrName].Value);
        }
        catch { }
        return defaultValue;
    }

    public static XmlDocument GetXmlDocWithElement(string rootElementName, string version)
    {
        XmlDocument xmlDoc = new XmlDocument();
        string xmlStr = string.Format("<?xml version='1.0' encoding='UTF-8'?><{0} Version='{1}'></{0}>", rootElementName, version);
        xmlDoc.LoadXml(xmlStr);

        return xmlDoc;
    }		
}
