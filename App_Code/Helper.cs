﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Forms.ComponentModel.Com2Interop;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.Linq;
using System.Web.UI.MobileControls;
using System.Collections;

/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper : IDisposable
{
    public Exception LastException = null;
    public DBDataContext DB = null;// = new DBDataContext();
    private Helper() { }

    public static Helper Instance
    {
        get
        {
            IDictionary items = HttpContext.Current.Items;
            if (!items.Contains("TheInstance"))
            {
                items["TheInstance"] = new Helper();
            }
            return items["TheInstance"] as Helper;
        }
    }
    public void Init()
    {
        DB = new DBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["TransportConnectionString"].ConnectionString);
        DB.CommandTimeout = (int)Math.Max(30, GetSettingValue("CommandTimeOut", 120));
    }
    public void Dispose()
    {
        if (null != DB)
        {
            DB.Dispose();
            DB = null;
        }
    }
    public bool Update()
    {
        LastException = null;
        try
        {
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            string fuckTuekry = ex.Message;
            LastException = ex;
        }
        return false;
    }
    public string GetLastException()
    {
        if (LastException == null)
            return "";
        return string.Format("{0}<br /> {1}<br />", LastException.Message, LastException.StackTrace);
    }
    #region Country
    public Country GetCountry(int id)
    {
        try
        {
            var c = from cs in DB.Countries
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Country GetCountryByCode(string code)
    {
        try
        {
            var c = from cs in DB.Countries
                    where cs.Code == code
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Country GetCountryByTitle(string title)
    {
        try
        {
            var c = from cs in DB.Countries
                    where cs.Title == title
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Country> GetCountries()
    {
        try
        {
            return DB.Countries.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteCountry(int id)
    {
        try
        {
            DB.Countries.DeleteOnSubmit(GetCountry(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteCountry(Country info)
    {
        try
        {
            DB.Countries.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public string ValidateCountry(Country info)
    {
        Country old = GetCountryByCode(info.Code);
        if (old != null && old.ID != info.ID)
            return "کد تکراري است";
        old = GetCountryByTitle(info.Title);
        if (old != null && old.ID != info.ID)
            return "نام تکراري است";
        return "";
    }
    #endregion

    #region Special
    public Special GetSpecial(int id)
    {
        try
        {
            var c = from cs in DB.Specials
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public List<Special> GetSpecials()
    {
        try
        {
            return DB.Specials.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteSpecial(int id)
    {
        try
        {
            DB.Specials.DeleteOnSubmit(GetSpecial(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteSpecial(Special info)
    {
        try
        {
            DB.Specials.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    #endregion

    #region city
    public string GetCityTitle(int id)
    {
        City c = GetCity(id);
        return c == null ? "" : c.Title;
    }
    public City GetCity(int id)
    {
        try
        {
            var c = from cs in DB.Cities
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public City GetCity(string title)
    {
        try
        {
            var c = from cs in DB.Cities
                    where cs.Title == title
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public City GetCityByCode(string code)
    {
        try
        {
            var c = from cs in DB.Cities
                    where cs.Code == code
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    //public List<City> GetCities()
    //{
    //    try
    //    {
    //        return DB.Cities.ToList();
    //    }
    //    catch
    //    {
    //    }
    //    return null;
    //}
    public List<City> GetAllCities()
    {
        try
        {
            return DB.Cities.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteCity(City info)
    {
        try
        {
            DB.Cities.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteCity(int id)
    {
        try
        {
            DB.Cities.DeleteOnSubmit(GetCity(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    public List<City> GetCities(Service service)
    {
        List<City> res = new List<City>();
        try
        {
            var c = from co in DB.Cities
                    join sd in DB.ServiceDests on co.ID equals sd.CityID
                    where sd.ServiceID == service.ID
                    select co;
            res.AddRange(c.ToList());
        }
        catch
        {
        }
        res.Add(service.Path.City1); // City1 is the dest
        return res;
    }
    public List<City> GetCities(bool enabled)
    {
        List<City> res = new List<City>();
        try
        {
            var c = from co in DB.Cities
                    where co.Enabled == enabled
                    select co;
            res.AddRange(c.ToList());
        }
        catch
        {
        }
        return res;
    }
    public string GetCityCode(int id)
    {
        City city = GetCity(id);
        if (null != city)
            return city.Code;
        return "";
    }
    #endregion

    #region Path
    public Path GetPath(int id)
    {
        try
        {
            var c = from cs in DB.Paths
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Path GetPath(int srcCityID, int destCityID)
    {
        try
        {
            var c = from cs in DB.Paths
                    where cs.SrcCityID == srcCityID && cs.DestCityID == destCityID
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Path GetPath(string title)
    {
        try
        {
            var c = from cs in DB.Paths
                    where cs.Title == title
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Path GetPathByCode(string code)
    {
        try
        {
            var c = from cs in DB.Paths
                    where cs.Code == code
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Path> GetPaths()
    {
        try
        {
            return DB.Paths.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Path> GetPaths(bool enabled)
    {
        try
        {
            return DB.Paths.Where(p => p.Enabled == enabled).ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeletePath(Path info)
    {
        try
        {
            DB.Paths.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeletePath(int id)
    {
        try
        {
            DB.Paths.DeleteOnSubmit(GetPath(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public string GetPathTitle(int id)
    {
        Path c = GetPath(id);
        return c == null ? "" : c.Title;
    }
    #endregion

    #region Payment
    public Payment GetPayment(int id)
    {
        try
        {
            var c = from cs in DB.Payments
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Payment> GetPayments()
    {
        try
        {
            return DB.Payments.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeletePayment(Payment info)
    {
        try
        {
            DB.Payments.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeletePayment(int id)
    {
        try
        {
            DB.Payments.DeleteOnSubmit(GetPayment(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Branch
    public Branch GetBranch(int id)
    {
        try
        {
            var c = from cs in DB.Branches
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Branch> GetBranches()
    {
        try
        {
            return DB.Branches.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteBranch(Branch info)
    {
        try
        {
            DB.Branches.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteBranch(int id)
    {
        try
        {
            DB.Branches.DeleteOnSubmit(GetBranch(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Fund
    public Fund GetFund(int? id)
    {
        if (id.HasValue)
            return GetFund(id.Value);
        return null;
    }
    public Fund GetFund(int id)
    {
        try
        {
            var c = from cs in DB.Funds
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetFundTitle(int id)
    {
        try
        {
            var c = from cs in DB.Funds
                    where cs.ID == id
                    select cs.Title;
            return c.First();
        }
        catch
        {
        }
        return "";
    }
    public List<Fund> GetFunds()
    {
        try
        {
            return DB.Funds.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteFund(Fund info)
    {
        try
        {
            DB.Funds.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteFund(int id)
    {
        try
        {
            DB.Funds.DeleteOnSubmit(GetFund(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region InsuranceTypes
    public InsuranceType GetInsuranceType(int id)
    {
        try
        {
            var c = from cs in DB.InsuranceTypes
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<InsuranceType> GetInsuranceTypes()
    {
        try
        {
            return DB.InsuranceTypes.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteInsuranceType(int id)
    {
        try
        {
            DB.InsuranceTypes.DeleteOnSubmit(GetInsuranceType(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteInsuranceType(InsuranceType info)
    {
        try
        {
            DB.InsuranceTypes.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Insurances
    public Insurance GetInsurance(int id)
    {
        try
        {
            var c = from cs in DB.Insurances
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Insurance> GetInsurances()
    {
        try
        {
            return DB.Insurances.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteInsurance(Insurance info)
    {
        try
        {
            DB.Insurances.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteInsurance(int id)
    {
        try
        {
            DB.Insurances.DeleteOnSubmit(GetInsurance(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    public InsuranceKM GetInsuranceKM(int id)
    {
        try
        {
            var c = from cs in DB.InsuranceKMs
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<InsuranceKM> GetInsuranceKMs(int insuranceID)
    {
        try
        {
            return DB.InsuranceKMs.Where(w => w.InsuranceID == insuranceID).ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteInsuranceKM(int id)
    {
        try
        {
            DB.InsuranceKMs.DeleteOnSubmit(GetInsuranceKM(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteInsuranceKM(InsuranceKM info)
    {
        try
        {
            DB.InsuranceKMs.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Layouts
    public Layout GetLayout(int id)
    {
        try
        {
            var c = from cs in DB.Layouts
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Layout> GetLayouts()
    {
        try
        {
            return DB.Layouts.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Layout> GetLayouts(bool enabled)
    {
        try
        {
            var c = from cs in DB.Layouts
                    where cs.Enabled == enabled
                    select cs;
            return c.ToList();
            //return DB.Layouts.Where().ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteLayout(int id)
    {
        try
        {
            DB.Layouts.DeleteOnSubmit(GetLayout(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteLayout(Layout info)
    {
        try
        {
            DB.Layouts.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region CarTypes
    //call GetLayouts From up
    public CarType GetCarType(int id)
    {
        try
        {
            var c = from cs in DB.CarTypes
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public CarType GetCarTypeByCode(int code)
    {
        try
        {
            var c = from cs in DB.CarTypes
                    where cs.Code == code
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetCarTypeTitle(int code)
    {
        CarType car = GetCarTypeByCode(code);
        return car == null ? "" : car.Title;
    }
    public List<CarType> GetCarTypes()
    {
        try
        {
            return (from ct in DB.CarTypes
                    orderby ct.Title
                    select ct).ToList();
            return DB.CarTypes.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<CarType> GetCarTypes(int specialID)
    {
        try
        {
            var c = from cs in DB.CarTypes
                    where cs.SpecialID == specialID
                    select cs;
            return c.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<CarType> GetCarTypes(int specialID, int layoutID)
    {
        try
        {
            var c = from cs in DB.CarTypes
                    where cs.SpecialID == specialID && cs.LayoutID == layoutID
                    select cs;
            return c.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteCarType(CarType info)
    {
        try
        {
            DB.CarTypes.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteCarType(int id)
    {
        try
        {
            DB.CarTypes.DeleteOnSubmit(GetCarType(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Cars
    public Car GetCar(int id)
    {
        try
        {
            var c = from cs in DB.Cars
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Car GetCar(int? id)
    {
        if (id.HasValue)
            return GetCar(id.Value);
        return null;
    }
    public Car GetCar(string smartCard)
    {
        if (string.IsNullOrEmpty(smartCard))
            return null;
        try
        {
            var c = from cs in DB.Cars
                    where cs.SmartCard == smartCard
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Car GetCarByCode(string code)
    {
        try
        {
            var c = from cs in DB.Cars
                    where cs.Code == code
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Car GetCarByCode(string code, bool enabled)
    {
        try
        {
            var c = from cs in DB.Cars
                    where cs.Code == code && cs.Enabled == enabled
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Car> GetCars()
    {
        try
        {
            return DB.Cars.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Car> GetEnableCars()
    {
        try
        {
            return DB.Cars.Where(c => c.Enabled == true).ToList();
        }
        catch (Exception)
        {
            throw;
        }
    }
    public List<Car> GetCarsByCarType(int carTypeID)
    {
        try
        {
            return DB.Cars.Where(c => c.CarTypeID == carTypeID).ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Car> GetCarsByCarType(int? carTypeID)
    {
        if (carTypeID == null)
            return GetCars();
        try
        {
            return DB.Cars.Where(c => c.CarTypeID == carTypeID.Value).ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Car> GetCarsBySpecialID(int? specialID)
    {
        if (specialID == null)
            return GetCars();
        try
        {
            return DB.Cars.Where(c => c.CarType.SpecialID == specialID.Value).ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteCar(Car info)
    {
        try
        {
            DB.Cars.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteCar(int id)
    {
        try
        {
            DB.Cars.DeleteOnSubmit(GetCar(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public string GetCarTitle(int? id)
    {
        if (id == null)
            return null;
        Car c = GetCar(id.Value);
        return c == null ? "" : c.Title;
    }
    public string GetCarPlate(int? id)
    {
        if (id == null)
            return null;
        Car c = GetCar(id.Value);
        return c == null ? "" : c.Plate;
    }
    public string GetCarSmartCard(int? id)
    {
        if (id == null)
            return "";
        Car c = GetCar(id.Value);
        return c == null ? "" : c.SmartCard;
    }
    public string GetCityCodeForCar(int? carID)
    {
        if (carID == null)
            return "";
        Car c = GetCar(carID.Value);
        if (c == null)
            return "";
        {
            City city = GetCity(Tool.GetInt(c.PlateCity.Trim(), Null.NullInteger));
            if (city != null)
                return city.Code;
        }
        {
            City city = GetCity(c.PlateCity);
            return city == null ? "" : city.Code;
        }
        //return GetCityCode(c.PlateCity);
    }
    public string GetCarTypeCode(int? carID)
    {
        if (carID == null)
            return "";
        Car c = GetCar(carID.Value);
        return c == null ? "" : c.CarType.Code.HasValue ? c.CarType.Code.Value.ToString() : "";
    }
    public int GetCarCapacity(int? carID)
    {
        if (carID == null)
            return 0;

        return GetCarCapacity(carID.Value);
    }
    public int GetCarCapacity(int carID)
    {
        if (carID == null)
            return 0;
        Car c = GetCar(carID);
        if (c == null || c.CarType == null || c.CarType.Layout == null
            || string.IsNullOrEmpty(c.CarType.Layout.Layout1))
            return 0;
        Layout info = c.CarType.Layout;
        if (info.NumChairs.HasValue && info.NumChairs.Value > 0)
            return info.NumChairs.Value;
        int res = 0;
        for (int f = 0; f < info.Floors; f++)
        {
            for (int i = 0; i < info.Rows; i++)
            {
                for (int j = 0; j < info.Columns; j++)
                {
                    if (info.Layout1.Contains(string.Format(";{0}_{1}_{2};", f, i, j)))
                        res++;
                }
            }
        }
        return res;
    }

    public string GetMaxCarCode()
    {
        try
        {
            var c = from cs in DB.Cars
                    select cs.Code;
            List<string> codes = c.ToList();
            int max = 0;
            for (int i = 0; i < codes.Count; i++)
            {
                int code = Tool.GetInt(codes[i], -1);
                if (code > max)
                    max = code;
            }
            return (max + 1).ToString();
        }
        catch
        {
        }
        return "1";

    }
    public Car CreateCar(string smartCard)
    {
        return null;
        //try
        //{
        //    string userID = Helper.Instance.GetSettingValue("User_Id");
        //    string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        //    int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        //    int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        //    ir.rmto.soratonline.Driver driver = new ir.rmto.soratonline.Driver();
        //    string value = driver.Request_Info_Driver(userID, passwordID, identityNo, smartCard, "", "");
        //    if (string.IsNullOrEmpty(value))
        //        throw new Exception("");
        //    string[] words = value.Split(';');
        //    if (words.Length < 6)
        //    {
        //        int errorCode = Tool.GetInt(words[0], -1);
        //        if (errorCode != 0)
        //            throw new Exception(words[2]);
        //    }
        //    if (words.Length == 14)
        //    {
        //        Car info = new Car();
        //        info.Code = GetMaxCarCode();

        //        info.CarTypeID = Tool.GetInt(mCarTypeID.SelectedValue, Null.NullInteger);
        //        info.OwnerID = Tool.GetInt(mOwnerID.SelectedValue, Null.NullInteger);
        //        info.TransitNo = mTransitNo.Text;
        //        //info.Plate = string.Format("{0}-{1}", mPlate.Text, mPlateSeries.Text.PadLeft(2, '0'));
        //        info.Plate = string.Format("{0}-{1}-{2}-{3}", mPlate0.Text.PadLeft(2, '0'), mPlate1.Text.PadLeft(3, '0'), mPlate2.Text, mPlate3.Text.PadLeft(2, '0'));
        //        if (info.Plate.Contains('0'))
        //        {
        //            mError.Text = "شماره پلاک اشتباه میباشد";
        //            puEx.Show();
        //            return;
        //        }
        //        info.VIN = mVIN.Text;
        //        info.PlateCity = mPlateCity.SelectedValue;//, Null.NullInteger).ToString(); //mPlateCity.Text;// 
        //        info.SmartCard = mSmartCard.Text;
        //        info.VisitDate = Tool.ParsePersianDate(mVisitDate.Text, DateTime.Now);
        //        info.InsuranceID = Tool.GetInt(mInsuranceID.SelectedValue, Null.NullInteger);
        //        info.InsuranceDate = Tool.ParsePersianDate(mInsuranceDate.Text, DateTime.Now);
        //        info.ThirdPersonDate = Tool.ParsePersianDate(mThirdPersonDate.Text, DateTime.Now);
        //        info.ProductionDate = Tool.ParsePersianDate(mProductionYear.Text + "/06/06", DateTime.Now);
        //        info.Commission = Tool.GetDecimal(mCommission.Text, 0);
        //        info.Deleted = mDeleted.Checked;
        //        info.ContractDate = Tool.ParsePersianDate(mContractDate.Text, DateTime.Now);
        //        info.Enabled = mEnabled.Checked;
        //        info.Comments = mComments.Text;

        //        City sne = null;
        //        try
        //        {
        //            var c = from cs in Helper.Instance.DB.Cities
        //                    where cs.Code == "73310000"
        //                    select cs;
        //            sne = c.First();
        //        }
        //        catch
        //        {
        //        }
        //        if (sne != null && mPlate0.Text == "51")
        //            info.PlateCity = sne.Title;






        //        //کرمعلي;سهرابي کلهر;علي قلي;75320000;244;1;10184178;15310000;13930717;4011118214;3116996;265248845;13781022;
        //        info.Name = words[0];
        //        info.Surname = words[1];
        //        info.Father = words[2];
        //        //Tool.SetSelected(mc  // BirthCity
        //        // IDCard
        //        info.Enabled = words[5] == "1";
        //        info.LicenceNo = words[6];
        //        try
        //        {
        //            City city = Helper.Instance.GetCityByCode(words[7]);
        //            info.LicenceCity = city.Title;
        //        }
        //        catch { }
        //        info.LicenceExpireDate = Tool.ParsePersianDate(words[8], DateTime.Now);
        //        info.IDNo = words[9];
        //        //SmartCard.Text = words[10];
        //        info.InsuranceNo = words[11];
        //        //mInsuranceEx.TemplateControl = words[12];  //InsuranceExpireDate

        //        Helper.Instance.DB.Drivers.InsertOnSubmit(info);
        //        if (Helper.Instance.Update())
        //            return info;
        //    }
        //}
        //catch
        //{
        //    throw;
        //}

        //return null;
    }
    #endregion

    #region Rates
    public Rate GetRate(int id)
    {
        if (id == Null.NullInteger)
            return null;
        try
        {
            var c = from cs in DB.Rates
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Rate GetRate(DateTime date)
    {
        if (date == Null.NullDate)
            return null;
        try
        {
            var c = from cs in DB.Rates
                    where cs.StartDate <= date && date <= cs.EndDate
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Rate> GetRates()
    {
        try
        {
            return DB.Rates.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteRate(int id)
    {
        try
        {
            DB.Rates.DeleteOnSubmit(GetRate(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteRate(Rate info)
    {
        try
        {
            DB.Rates.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    public RateItem GetRateItem(int id)
    {
        try
        {
            var c = from cs in DB.RateItems
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public RateItem GetRateItemBySpecial(int rateID, int srcCityID, int destCityID, int specialID, DateTime date)
    {
        if (rateID == Null.NullInteger)
            try
            {
                var c = from cs in DB.RateItems
                        where (cs.SpecialID == specialID)
                        && (cs.SrcCityID == srcCityID) && (cs.DestCityID == destCityID)
                        && cs.Rate.StartDate <= date && date <= cs.Rate.EndDate
                        select cs;
                return c.First();
            }
            catch
            {
                return null;
            }
        try
        {
            var c = from cs in DB.RateItems
                    where (cs.RateID == rateID) && (cs.SpecialID == specialID)
                    && (cs.SrcCityID == srcCityID) && (cs.DestCityID == destCityID)
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<RateItem> GetRateItems(int rateID)
    {
        try
        {
            return DB.RateItems.Where(w => w.RateID == rateID).ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteRateItem(int id)
    {
        try
        {
            DB.RateItems.DeleteOnSubmit(GetRateItem(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteRateItem(RateItem info)
    {
        try
        {
            DB.RateItems.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Owner
    public Owner GetOwner(int id)
    {
        try
        {
            var c = from cs in DB.Owners
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Owner GetOwnerByUserName(string userName)
    {
        try
        {
            var c = from cs in DB.Owners
                    where cs.UserName == userName
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Owner> GetOwners()
    {
        try
        {
            return DB.Owners.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteOwner(Owner info)
    {
        try
        {
            DB.Owners.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteOwner(int id)
    {
        try
        {
            DB.Owners.DeleteOnSubmit(GetOwner(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Driver
    public Driver GetDriver(int? id)
    {
        try
        {
            var c = from cs in DB.Drivers
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Driver GetDriver(string smartCard)
    {
        if (string.IsNullOrEmpty(smartCard))
            return null;
        try
        {
            var c = from cs in DB.Drivers
                    where cs.SmartCard == smartCard
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Driver GetDriverByCode(string code)
    {
        try
        {
            var c = from cs in DB.Drivers
                    where cs.Code == code
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Driver GetDriverByCode(string code, bool enabled)
    {
        try
        {
            var c = from cs in DB.Drivers
                    where cs.Code == code && cs.Enabled == enabled
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Driver GetDriverBySmartCard(string smartCard)
    {
        try
        {
            var c = from cs in DB.Drivers
                    where cs.SmartCard == smartCard
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetDriverFullNameBySmartCard(string smartCard)
    {
        Driver c = GetDriverBySmartCard(smartCard);
        return c == null ? "" : string.Concat(c.Name, " ", c.Surname);
    }
    public List<Driver> GetDrivers()
    {
        try
        {
            return DB.Drivers.ToList();
        }
        catch
        {
        }
        return null;
    }

    public List<Driver> GetEnableDrivers()
    {
        try
        {
            return DB.Drivers.Where(c => c.Enabled == true).ToList();
        }
        catch (Exception)
        {
            throw;
        }
    }
    public bool DeleteDriver(Driver info)
    {
        try
        {
            DB.Drivers.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteDriver(int id)
    {
        try
        {
            DB.Drivers.DeleteOnSubmit(GetDriver(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public string GetDriverSmartCard(int? id)
    {
        if (id == null)
            return "";
        Driver c = GetDriver(id.Value);
        return c == null ? "" : c.SmartCard;
    }
    public string GetDriverFullName(int? id)
    {
        if (id == null)
            return "";
        Driver c = GetDriver(id.Value);
        return c == null ? "" : string.Concat(c.Name, " ", c.Surname);
    }
    public string GetMaxDriverCode()
    {
        try
        {
            var c = from cs in DB.Drivers
                    select cs.Code;
            List<string> codes = c.ToList();
            int max = 0;
            for (int i = 0; i < codes.Count; i++)
            {
                int code = Tool.GetInt(codes[i], -1);
                if (code > max)
                    max = code;
            }
            return (max + 1).ToString();
        }
        catch
        {
        }
        return "1";

    }
    public Driver CreateDriver(string smartCard)
    {
        try
        {
            string userID = Helper.Instance.GetSettingValue("User_Id");
            string passwordID = Helper.Instance.GetSettingValue("Password_Id");
            int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
            int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

            ir.rmto.soratonline.Driver driver = new ir.rmto.soratonline.Driver();
            string value = driver.Request_Info_Driver(userID, passwordID, identityNo, smartCard, "", "");
            if (string.IsNullOrEmpty(value))
                throw new Exception("");
            string[] words = value.Split(';');
            if (words.Length < 6)
            {
                int errorCode = Tool.GetInt(words[0], -1);
                if (errorCode != 0)
                    throw new Exception(words[2]);
            }
            if (words.Length == 14)
            {
                Driver info = new Driver();
                info.Code = GetMaxDriverCode();
                info.Man = true;

                info.DrivingDate = DateTime.Now.AddDays(-1);
                info.LicenceExpireDate = info.HourDate = DateTime.Now.AddDays(-1);
                info.SmartCard = smartCard;
                info.Name = info.Surname = info.Father = info.IDNo = info.LicenceNo
                    = info.LicenceCity = "";
                info.CellNo = info.Tel = info.DrivingNo = "";
                info.PassportNo = info.InsuranceNo = info.InsuranceBranch = "";
                info.Enabled = false;



                //کرمعلي;سهرابي کلهر;علي قلي;75320000;244;1;10184178;15310000;13930717;4011118214;3116996;265248845;13781022;
                info.Name = words[0];
                info.Surname = words[1];
                info.Father = words[2];
                //Tool.SetSelected(mc  // BirthCity
                // IDCard
                info.Enabled = words[5] == "1";
                info.LicenceNo = words[6];
                try
                {
                    City city = Helper.Instance.GetCityByCode(words[7]);
                    info.LicenceCity = city.Title;
                }
                catch { }
                info.LicenceExpireDate = Tool.ParsePersianDate(words[8], DateTime.Now);
                info.IDNo = words[9];
                //SmartCard.Text = words[10];
                info.InsuranceNo = words[11];
                //mInsuranceEx.TemplateControl = words[12];  //InsuranceExpireDate

                Helper.Instance.DB.Drivers.InsertOnSubmit(info);
                if (Helper.Instance.Update())
                    return info;
            }
        }
        catch
        {
            throw;
        }

        return null;
    }
    #endregion

    #region Customer
    public Customer GetCustomer(int id)
    {
        try
        {
            var c = from cs in DB.Customers
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Customer GetCustomer(string no)
    {
        try
        {
            var c = from cs in DB.Customers
                    where cs.No == no
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Customer> GetCustomers()
    {
        try
        {
            return DB.Customers.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteCustomer(Customer info)
    {
        try
        {
            DB.Customers.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteCustomer(int id)
    {
        try
        {
            DB.Customers.DeleteOnSubmit(GetCustomer(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Service
    public Service GetService(int id)
    {
        if (id == Null.NullInteger)
            return null;
        try
        {
            var c = from cs in DB.Services
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Service> GetServices()
    {
        try
        {
            return DB.Services.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Service> GetFixedServices()
    {
        try
        {
            return DB.Services.Where(d => d.ServiceType == (int)ServiceTypes.Fixed).ToList();
        }
        catch
        {
        }
        return null;
    }
    //**
    public List<Service> GetServicesBySpecial(int SpecialID)
    {
        try
        {
            return DB.Services.Where(d => d.SpecialID == SpecialID).ToList();
        }
        catch
        {
        }
        return null;
    }
    //**
    public List<Service> GetServices(bool enabled, int destCityID)
    {
        if (destCityID > 0)
            try
            {
                var c = from cs in DB.Services
                        where cs.Enabled == enabled
                        && cs.Path.DestCityID == destCityID
                        select cs;
                return c.ToList();
            }
            catch
            {
            }
        else
            try
            {
                var c = from cs in DB.Services
                        where cs.Enabled == enabled
                        select cs;
                return c.ToList();
            }
            catch
            {
            }
        return null;
    }
    public bool DeleteService(Service info)
    {
        try
        {
            DB.Services.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteService(int id)
    {
        try
        {
            DB.Services.DeleteOnSubmit(GetService(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region ServiceDest
    public ServiceDest GetServiceDest(int id)
    {
        try
        {
            var c = from cs in DB.ServiceDests
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public ServiceDest GetServiceDest(int serviceID, int cityID)
    {
        try
        {
            var c = from cs in DB.ServiceDests
                    where cs.ServiceID == serviceID && cs.CityID == cityID
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<ServiceDest> GetServiceDests()
    {
        try
        {
            return DB.ServiceDests.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<ServiceDest> GetServiceDests(int serviceID)
    {
        var c = from cs in DB.ServiceDests
                where cs.ServiceID == serviceID
                select cs;
        return c.ToList();
    }
    public bool DeleteServiceDest(ServiceDest info)
    {
        try
        {
            DB.ServiceDests.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteServiceDest(int id)
    {
        try
        {
            DB.ServiceDests.DeleteOnSubmit(GetServiceDest(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Trip
    public bool TripExists(int serviceID, DateTime date)
    {
        try
        {
            var c = from cs in DB.Trips
                    where cs.ServiceID == serviceID && cs.Date == date
                    select cs;
            return c.Count() > 0;
        }
        catch
        {
        }
        return false;
    }
    public Trip GetTrip(int id)
    {
        try
        {
            var c = from cs in DB.Trips
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Trip GetTrip(string trackCode)
    {
        try
        {
            var c = from cs in DB.Trips
                    where cs.TrackCode == trackCode
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Trip> GetTrips()
    {
        try
        {
            return DB.Trips.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Trip> GetTrips(bool closed)
    {
        try
        {
            return DB.Trips.Where(t => t.Closed == closed).ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteTrip(Trip info)
    {
        try
        {
            DB.Trips.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteTrip(int id)
    {
        try
        {
            Trip t = GetTrip(id);
            DB.TripDests.DeleteAllOnSubmit(t.TripDests);
            for (int i = 0; i < t.Tickets.Count; i++)
            {
                DB.TicketsHistories.DeleteAllOnSubmit(t.Tickets[i].TicketsHistories);
                DB.TicketHistories.DeleteAllOnSubmit(t.Tickets[i].TicketHistories);
            }
            DB.Tickets.DeleteAllOnSubmit(t.Tickets);
            DB.Trips.DeleteOnSubmit(t);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public int GetNumTickets(int tripID)
    {
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID && t.PaidBack == 0
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    select t.NumChairs;
            int? res1 = v.Sum();
            if (res1.HasValue)
                return res1.Value;
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
        List<Ticket> ts = null;
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID && t.PaidBack == 0
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    select t;
            ts = v.ToList();
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        if (ts == null)
            return 0;
        int res = 0;
        for (int i = 0; i < ts.Count; i++)
        {
            if (!string.IsNullOrEmpty(ts[i].Chairs))
                res += ts[i].Chairs.Trim(';').Split(';').Length;
        }
        return res;
    }
    public int GetNumTickets(int tripID, int cityID)
    {
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID && t.PaidBack == 0
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    && t.CityID == cityID
                    select t.NumChairs;
            int? res1 = v.Sum();
            if (res1.HasValue)
                return res1.Value;
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
        List<Ticket> ts = null;
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID && t.PaidBack == 0
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    && t.CityID == cityID
                    select t;
            ts = v.ToList();
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        if (ts == null)
            return 0;
        int res = 0;
        for (int i = 0; i < ts.Count; i++)
        {
            if (!string.IsNullOrEmpty(ts[i].Chairs))
                res += ts[i].Chairs.Trim(';').Split(';').Length;
        }
        return res;
    }
    public int GetNumTickets(int tripID, int userID, int fundID)
    {
        try
        {
            if (fundID == Null.NullInteger)
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.PaidBack == 0 && t.SaleUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        select t.NumChairs;
                int? res1 = v.Sum();
                if (res1.HasValue)
                    return res1.Value;
            }
            else
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.PaidBack == 0 && t.SaleUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.FundID == fundID
                        select t.NumChairs;
                int? res1 = v.Sum();
                if (res1.HasValue)
                    return res1.Value;
            }
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }

    public int GetFundNumTickets(int tripID, int userID, int fundID)
    {
        try
        {
            if (fundID == Null.NullInteger)
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.PaidBack == 0 && t.FundUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        select t.NumChairs;
                int? res1 = v.Sum();
                if (res1.HasValue)
                    return res1.Value;
            }
            else
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.PaidBack == 0 && t.FundUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.FundID == fundID
                        select t.NumChairs;
                int? res1 = v.Sum();
                if (res1.HasValue)
                    return res1.Value;
            }
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }
    public int GetNumReserves(int tripID)
    {
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID && t.PaidBack == 0
                    && t.SaleType == (int)SaleTypes.Reserve
                    select t.NumChairs;
            int? res1 = v.Sum();
            if (res1.HasValue)
                return res1.Value;
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
        List<Ticket> ts = null;
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID && t.PaidBack == 0
                    && t.SaleType == (int)SaleTypes.Reserve
                    select t;
            ts = v.ToList();
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        if (ts == null)
            return 0;
        int res = 0;
        for (int i = 0; i < ts.Count; i++)
        {
            if (!string.IsNullOrEmpty(ts[i].Chairs))
                res += ts[i].Chairs.Trim(';').Split(';').Length;
        }
        return res;
    }
    public int GetPrice(int tripID)
    {
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    && t.PaidBack == 0
                    select (t.Price - t.Discount) * t.NumChairs;
            int? res = v.Sum();
            if (res.HasValue)
                return res.Value;
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }
    public int GetPrice(int tripID, int userID, int fundID)
    {
        try
        {
            if (fundID == Null.NullInteger)
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.SaleUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0
                        select (t.Price - t.Discount) * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
            else
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.SaleUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0 && t.FundID == fundID
                        select (t.Price - t.Discount) * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }

    public int GetFundPrice(int tripID, int userID, int fundID)
    {
        try
        {
            if (fundID == Null.NullInteger)
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.FundUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0
                        select (t.Price - t.Discount) * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
            else
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.FundUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0 && t.FundID == fundID
                        select (t.Price - t.Discount) * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }
    public int GetDiscount(int tripID, int userID, int fundID)
    {
        try
        {
            if (fundID == Null.NullInteger)
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.SaleUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0
                        select t.Discount * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
            else
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.SaleUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0 && t.FundID == fundID
                        select t.Discount * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }

    public int GetFundDiscount(int tripID, int userID, int fundID)
    {
        try
        {
            if (fundID == Null.NullInteger)
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.FundUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0
                        select t.Discount * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
            else
            {
                var v = from t in DB.Tickets
                        where t.TripID == tripID && t.FundUserID == userID
                        && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                        && t.PaidBack == 0 && t.FundID == fundID
                        select t.Discount * t.NumChairs;
                int? res = v.Sum();
                if (res.HasValue)
                    return res.Value;
            }
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }
    public Trip GetPreTrip(Trip trip)
    {
        if (trip == null)
            return null;
        List<Trip> ress = null;
        DateTime date = new DateTime(trip.Date.Year, trip.Date.Month, trip.Date.Day, 0, 0, 0, 0);
        DateTime date2 = new DateTime(trip.Date.Year, trip.Date.Month, trip.Date.Day, 23, 59, 59, 999);
        try
        {
            var v = from t in DB.Trips
                    where t.Date >= date && t.Date <= date2 && t.ID != trip.ID && trip.Closed == false
                    && t.Service.PathID == trip.Service.PathID
                    orderby t.Date ascending
                    select t;
            ress = v.ToList();
        }
        catch { }
        if (ress != null)
        {
            int tripDP = string.IsNullOrEmpty(trip.Service.DepartureTime) || !trip.Service.DepartureTime.Contains(':') ?
                        0 : Tool.GetInt(trip.Service.DepartureTime.Split(':')[0], 0) * 100 + Tool.GetInt(trip.Service.DepartureTime.Split(':')[1], 0);
            int maxDP = 0 * 100 + 0;
            Trip selected = null;
            for (int i = 0; i < ress.Count; i++)
            {
                Trip res = ress[i];
                if (res.Date.Year == trip.Date.Year && res.Date.Month == trip.Date.Month && res.Date.Day == trip.Date.Day)
                {
                    // the same day
                    int resDP = string.IsNullOrEmpty(res.Service.DepartureTime) || !res.Service.DepartureTime.Contains(':') ?
                        0 : Tool.GetInt(res.Service.DepartureTime.Split(':')[0], 0) * 100 + Tool.GetInt(res.Service.DepartureTime.Split(':')[1], 0);
                    if (resDP < tripDP && resDP > maxDP)
                    {
                        maxDP = resDP;
                        selected = res;
                    }
                }
            }
            return selected;
        }
        return null;
    }
    public Trip GetNextTrip(Trip trip)
    {
        if (trip == null)
            return null;
        List<Trip> ress = null;
        DateTime date = new DateTime(trip.Date.Year, trip.Date.Month, trip.Date.Day, 0, 0, 0, 0);
        DateTime date2 = new DateTime(trip.Date.Year, trip.Date.Month, trip.Date.Day, 23, 59, 59, 999);
        try
        {
            var v = from t in DB.Trips
                    where t.Date >= date && t.Date <= date2 && t.ID != trip.ID && trip.Closed == false
                    && t.Service.PathID == trip.Service.PathID
                    orderby t.Date ascending
                    select t;
            ress = v.ToList();
        }
        catch { }
        if (ress != null)
        {
            int tripDP = string.IsNullOrEmpty(trip.Service.DepartureTime) || !trip.Service.DepartureTime.Contains(':') ?
                        0 : Tool.GetInt(trip.Service.DepartureTime.Split(':')[0], 0) * 100 + Tool.GetInt(trip.Service.DepartureTime.Split(':')[1], 0);
            int minDP = 23 * 100 + 59;
            Trip selected = null;
            for (int i = 0; i < ress.Count; i++)
            {
                Trip res = ress[i];
                if (res.Date.Year == trip.Date.Year && res.Date.Month == trip.Date.Month && res.Date.Day == trip.Date.Day)
                {
                    // the same day
                    int resDP = string.IsNullOrEmpty(res.Service.DepartureTime) || !res.Service.DepartureTime.Contains(':') ?
                        0 : Tool.GetInt(res.Service.DepartureTime.Split(':')[0], 0) * 100 + Tool.GetInt(res.Service.DepartureTime.Split(':')[1], 0);
                    if (resDP > tripDP && resDP < minDP)
                    {
                        minDP = resDP;
                        selected = res;
                    }
                }
            }
            return selected;
        }
        return null;
    }
    //public bool TripHasUser(Trip trip, int userID, int fundID)
    //{
    //    try
    //    {
    //        if (fundID == Null.NullInteger)
    //            return DB.Tickets.Where(t => t.TripID == trip.ID && t.PaidBack == 0 && t.SaleUserID == userID).Count() > 0;
    //        return DB.Tickets.Where(t => t.TripID == trip.ID && t.PaidBack == 0 && t.SaleUserID == userID && t.FundID == fundID).Count() > 0;
    //    }
    //    catch (Exception ex)
    //    {
    //        string exStr = ex.Message;
    //    }
    //    return false;
    //}
    public bool TripHasUser(Trip trip, int userID, int fundID, int fdTime, int tdTime)
    {
        try
        {
            if (trip.DepartureTime2int < fdTime || trip.DepartureTime2int > tdTime)
                return false;
            if (fundID == Null.NullInteger)
                return DB.Tickets.Where(t => t.TripID == trip.ID && t.PaidBack == 0 && t.SaleUserID == userID).Count() > 0;
            return DB.Tickets.Where(t => t.TripID == trip.ID && t.PaidBack == 0 && t.SaleUserID == userID).Count() > 0;
        }
        catch (Exception ex)
        {
            string exStr = ex.Message;
        }
        return false;
    }

    public bool TripHasFundUser(Trip trip, int userID, int fundID, int fdTime, int tdTime)
    {
        try
        {
            if (trip.DepartureTime2int < fdTime || trip.DepartureTime2int > tdTime)
                return false;
            if (fundID == Null.NullInteger)
                return DB.Tickets.Where(t => t.TripID == trip.ID && t.PaidBack == 0 && t.FundUserID == userID).Count() > 0;
            return DB.Tickets.Where(t => t.TripID == trip.ID && t.PaidBack == 0 && t.FundUserID == userID).Count() > 0;
        }
        catch (Exception ex)
        {
            string exStr = ex.Message;
        }
        return false;
    }

    public int GetNextSeriesStart(string series, bool isMini)
    {
        int start;
        if (isMini)
            start = Tool.GetInt(GetSettingValue("SeriesStartMini"), 1) + 1;
        else
            start = Tool.GetInt(GetSettingValue("SeriesStart"), 1) + 1;
        int used = 0;
        try
        {
            List<int> trips = (from t in DB.Trips
                               where t.No >= start && t.Series == series
                               select t.No).ToList();
            do
            {
                if (trips.Contains(start))
                    start++;
                else
                    break;
            } while (true);
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        //try
        //{
        //    var v = from t in DB.Trips
        //            where t.Series == series && t.No != null
        //            select t.No;
        //    int? usedN = v.Max();
        //    if (usedN.HasValue)
        //        used = usedN.Value + 1;
        //}
        //catch { }
        return used > start ? used : start;
    }
    public Trip CreateTrip(out string error, DateTime date, int serviceID, int carID, int driverID)
    {
        error = "";
        try
        {
            Trip info = new Trip();
            info.Series = "";
            info.TicketlessName = "";
            info.Others = Tool.GetInt(GetSettingValue("Others"), 0);

            //info.Series = string.Format("{0},{1},{2},{3}"
            //    , GetSettingValue("SeriesNo1"), GetSettingValue("SeriesNo2")
            //    , GetSettingValue("SeriesNo3"), GetSettingValue("SeriesNo4"));
            info.No = 0;// GetNextSeriesStart(info.Series);
            info.Date = date;
            info.ServiceID = serviceID;

            info.Service = GetService(info.ServiceID);
            if (info.Service != null)
            {
                info.Price = info.Service.Price;
                info.Toll = info.Service.Toll;
                info.Insurance = info.Service.Insurance;
                info.Reception = info.Service.Reception;
                info.CarTypeID = info.Service.CarTypeID;
                info.DepartureTime = info.Service.DepartureTime;

                RateItem rate = GetRateItemBySpecial(Null.NullInteger, info.Service.Path.SrcCityID, info.Service.Path.DestCityID, info.Service.SpecialID, info.Date);
                if (rate != null)
                {
                    info.ExtraCost = rate.ExtraCost;
                    info.Insurance = rate.Insurance;
                    if (info.Service.ServiceType != (int)ServiceTypes.Singular)
                        info.Toll = rate.Toll;
                    info.Reception = rate.ReceptionCost;
                    info.CommissionPrice = rate.CommissionPrice;
                }
            }

            if (carID != Null.NullInteger)
            {
                #region Process Car : Comission, BodyInsurance
                info.CarID = carID;
                Car c = GetCar(info.CarID.Value);
                if (c == null)
                {
                    error = "اتوکار انتخاب نشده است.";
                    return null;
                }
                if (c.CarType.SpecialID != info.Service.SpecialID)
                {
                    error = "نوع سرویس دهی اتوکار انتخاب شده با نوع نوع سرویس دهی تعريف شده در سرويس يکي نيست.";
                    return null;
                }
                info.Comission = c.Commission;
                if (c.Insurance != null && c.Insurance.InsuranceKMs != null && c.Insurance.InsuranceKMs.Count > 0)
                {
                    for (int i = 0; i < c.Insurance.InsuranceKMs.Count; i++)
                    {
                        if (c.Insurance.InsuranceKMs[i].StartKM <= info.Service.Path.KMs && info.Service.Path.KMs <= c.Insurance.InsuranceKMs[i].EndKM)
                        {
                            info.BodyInsurance = c.Insurance.InsuranceKMs[i].Price;
                            break;
                        }
                    }
                }
                #endregion
            }
            else
                info.CarID = null;


            if (driverID != Null.NullInteger)
                info.Driver = GetDriver(driverID);
            else
                info.DriverID1 = null;

            if (info.IsMini)
                info.Others = 0;
            info.SentToInsurance = false;
            info.TripStatus = (int) TripStatus.None;
            info.CompanyUser = SiteSettings.User.FullName;
            DB.Trips.InsertOnSubmit(info);
            if (Update())
                return info;
            else
            {
                error = LastException.Message;
                return null;
            }
        }
        catch (Exception ex)
        {
            error = ex.Message + "<br>" + ex.Message;
            return null;
        }

    }

    public bool UpdateTrip(CompanyTrips companyTrips)
    {
        string cntStr =companyTrips.Company.ConnectionString;
        string query = string.Format(@"UPDATE Trips SET TripStatus={0}, Comments=N'{1}', FaniDetail='{2}' WHERE ID={3}",
            (int) companyTrips.Status, companyTrips.Comments, companyTrips.FaniDetail, companyTrips.TripID);
        
        return RunQuery(query, cntStr) > 0;
    }

    public static string TripStatusToString(TripStatus tripstatus, string defaultValue)
    {
        switch (tripstatus)
        {
            case (TripStatus.None):
                return "ارسال نشده";
            case (TripStatus.Pending):
                return "در حال بررسی";
            case (TripStatus.Confirmed):
                return "تایید شده";
            case (TripStatus.Rejected):
                return "رد شده";
        }
        return defaultValue;
    }
    #endregion

    #region TripDest
    public TripDest GetTripDest(int id)
    {
        try
        {
            var c = from cs in DB.TripDests
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public TripDest GetTripDest(int tripID, int cityID)
    {
        try
        {
            var c = from cs in DB.TripDests
                    where cs.TripID == tripID && cs.CityID == cityID
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<TripDest> GetTripDests(int tripID)
    {
        try
        {
            var c = from cs in DB.TripDests
                    where cs.TripID == tripID
                    select cs;
            return c.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteTripDest(TripDest info)
    {
        try
        {
            DB.TripDests.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteTripDest(int id)
    {
        try
        {
            DB.TripDests.DeleteOnSubmit(GetTripDest(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Ticket
    public Ticket GetTicket(int id)
    {
        if (id == Null.NullInteger)
            return null;
        try
        {
            var c = from cs in DB.Tickets
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Ticket GetTicket(string no)
    {
        try
        {
            var c = from cs in DB.Tickets
                    where cs.No == no
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Ticket GetTicketByFullname(string fullname)
    {
        try
        {
            var c = from cs in DB.Tickets
                    where cs.Fullname.Contains(fullname)
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Ticket> GetTickets()
    {
        try
        {
            return DB.Tickets.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Ticket> GetTickets(int tripID)
    {
        try
        {
            return DB.Tickets.Where(t => t.TripID == tripID && t.PaidBack == 0).ToList();
        }
        catch
        {
        }
        return null;
    }
    //public List<TicketsHistory> GetTicketsHistory(int tripID)
    //{
    //    try
    //    {
    //        return DB.TicketsHistories.Where(t => t.TripID == tripID && t.PaidBack == 0).ToList();
    //    }
    //    catch
    //    {
    //    }
    //    return null;
    //}
    public List<Ticket> GetTickets(int tripID, int userID)
    {
        if (userID == Null.NullInteger)
            return GetTickets(tripID);
        try
        {
            return DB.Tickets.Where(t => t.TripID == tripID && t.PaidBack == 0 && t.SaleUserID == userID).ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<TicketHistory> GetTicketHistories(int tripID, int userID)
    {
        //if (userID == Null.NullInteger)
        //    return GetTickets(tripID);
        try
        {
            return DB.TicketHistories.Where(t => t.Ticket.TripID == tripID && (userID == Null.NullInteger ? 1 == 1 : ((t.SaleUserID == userID) || (t.Ticket.SaleUserID == userID)))).ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Ticket> GetTickets(string no)
    {
        try
        {
            return DB.Tickets.Where(t => t.No.Contains(no) && t.PaidBack == 0).ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Ticket> GetTicketsByFullname(string fullname)
    {
        try
        {
            return DB.Tickets.Where(t => t.Fullname.Contains(fullname) && t.PaidBack == 0).ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Ticket> GetReserves(int tripID)
    {
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID
                    && t.SaleType == (int)SaleTypes.Reserve
                     && t.PaidBack == 0
                    select t;
            return v.ToList();
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return null;
    }

    public string GetNewTicketNo(int tripID)
    {
        int id = 0;
        try
        {
            var v = from t in DB.Tickets
                    where t.TripID == tripID
                    select t.ID;
            id = v.Count();
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        id++;
        string res = string.Format("{0}-{1}", tripID.ToString(), id.ToString());
        Ticket old = GetTicket(res);
        while (null != old)
        {
            id++;
            res = string.Format("{0}-{1}", tripID.ToString(), id.ToString());
            old = GetTicket(res);
        }
        return res;
    }
    public bool DeleteTicket(Ticket info)
    {
        try
        {
            DB.Tickets.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteTicket(int id)
    {
        try
        {
            DB.Tickets.DeleteOnSubmit(GetTicket(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Exclusive
    public Exclusive GetExclusive(int id)
    {
        try
        {
            var c = from cs in DB.Exclusives
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Exclusive> GetExclusives()
    {
        try
        {
            return DB.Exclusives.ToList();
        }
        catch
        {
        }
        return null;
    }

    public bool DeleteExclusive(Exclusive info)
    {
        try
        {
            DB.Exclusives.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteExclusive(int id)
    {
        try
        {
            Exclusive t = GetExclusive(id);
            DB.Exclusives.DeleteOnSubmit(t);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    public int GetPrice(Exclusive info)
    {
        return 0;
    }
    public int GetNextSeriesStartD(string series)
    {
        int start = Tool.GetInt(GetSettingValue("SeriesStartD"), 1) + 1;
        int used = 0;
        //try
        //{
        //    var v = from t in DB.Exclusives
        //            where t.Series == series
        //            select t.No;
        //    used = v.Max() + 1;
        //}
        //catch { }
        start = used > start ? used : start;
        try
        {
            List<int> invalids = (from v in DB.InValids
                                  where v.No >= start && v.Series == series && v.Exclusive == true
                                  select v.No).ToList();
            do
            {
                if (invalids.Contains(start))
                    start++;
                else
                    break;
            } while (true);
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return start;
    }

    #endregion

    #region User
    public User GetUser(int id)
    {
        try
        {
            var c = from cs in DB.Users
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetUserFullName(int id)
    {
        User user = GetUser(id);
        return user == null ? "" : user.FullName;
    }
    public string GetUserFullName(int? id)
    {
        if (!id.HasValue)
            return "";
        User user = GetUser(id.Value);
        return user == null ? "" : user.FullName;
    }
    public User GetUser(string userName)
    {
        try
        {
            var c = from cs in DB.Users
                    where cs.UserName == userName
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<User> GetUsers()
    {
        try
        {
            return DB.Users.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteUser(User info)
    {
        if (info == null)
            return false;
        try
        {
            DB.Users.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteUser(int id)
    {
        try
        {
            DB.Users.DeleteOnSubmit(GetUser(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    //add by hadi
    public void DeleteOnlineUser(int id)
    {
        try
        {
            List<OnlineUser> onlineusers = getonlineUsers(id);
            foreach (OnlineUser onlineuser in onlineusers)
            {
                DB.OnlineUsers.DeleteOnSubmit(onlineuser);
                DB.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
    }
    public bool SetUserOnline(string UserName, int id)
    {
        OnlineUser user = new OnlineUser();
        user.onlineUserName = UserName;
        user.userID = id;
        try
        {
            DB.OnlineUsers.InsertOnSubmit(user);
        }
        catch
        {
        }
        return Update();
    }
    public OnlineUser getonlineUser(int id)
    {
        try
        {
            var c = from cs in DB.OnlineUsers
                    where cs.userID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<OnlineUser> getonlineUsers(int id)
    {
        try
        {
            var c = from cs in DB.OnlineUsers
                    where cs.userID == id
                    select cs;
            return c.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool SetUserOffline(int id)
    {

        try
        {
            DB.OnlineUsers.DeleteOnSubmit(getonlineUser(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Role
    public Role GetRole(int id)
    {
        try
        {
            var c = from cs in DB.Roles
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public string GetRoleName(int? id)
    {
        if (!id.HasValue)
            return "";
        Role role = GetRole(id.Value);
        return role == null ? "" : role.Name;
    }
    public Role GetRole(string name)
    {
        try
        {
            var c = from cs in DB.Roles
                    where cs.Name == name
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Role> GetRoles()
    {
        try
        {
            return DB.Roles.ToList();
        }
        catch
        {
        }
        return null;
    }
    public Access GetAccess(int id)
    {
        try
        {
            var c = from cs in DB.Accesses
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public Access GetAccess(string name)
    {
        try
        {
            var c = from cs in DB.Accesses
                    where cs.Name == name
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Access> GetAccesses()
    {
        try
        {
            return DB.Accesses.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteRole(Role info)
    {
        if (info == null)
            return false;
        try
        {
            DB.Roles.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteRole(int id)
    {
        try
        {
            DB.Roles.DeleteOnSubmit(GetRole(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Setting
    public Setting GetSetting(string name)
    {
        try
        {
            var c = from cs in DB.Settings
                    where cs.Name == name
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetSettingValue(string name)
    {
        Setting s = GetSetting(name);
        if (s == null)
            return "";
        return s.Value;
    }
    public float GetSettingValue(string name, float defaultValue)
    {
        Setting s = GetSetting(name);
        if (s == null)
            return defaultValue;
        return Tool.GetFloat(s.Value, defaultValue);
    }
    public List<Setting> GetSettings()
    {
        try
        {
            return DB.Settings.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteSetting(Setting info)
    {
        if (info == null)
            return false;
        try
        {
            DB.Settings.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteSetting(string name)
    {
        try
        {
            DB.Settings.DeleteOnSubmit(GetSetting(name));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool UpdateSetting(string name, string value)
    {
        Setting info = GetSetting(name);
        if (info == null)
        {
            info = new Setting();
            info.Name = info.Title = name;
            info.Value = value;
            DB.Settings.InsertOnSubmit(info);
        }
        else
            info.Value = value;
        return Update();
    }
    #endregion

    #region Wage
    public Wage GetWage(int id)
    {
        try
        {
            var c = from cs in DB.Wages
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<Wage> GetWages()
    {
        try
        {
            return DB.Wages.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteWage(Wage info)
    {
        try
        {
            DB.Wages.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteWage(int id)
    {
        try
        {
            DB.Wages.DeleteOnSubmit(GetWage(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public int GetWagePrice(int type, bool isForeign, int auto, int kms)
    {
        try
        {
            var c = from cs in DB.Wages
                    where cs.Type == type &&
                          cs.IsForeign == isForeign &&
                          cs.Auto == auto &&
                          cs.KMFrom <= kms &&
                          cs.KMTo >= kms
                    select cs;
            return c.First().Price;
        }
        catch
        {
            return 0;
        }
        return 0;

    }
    #endregion

    #region InValid
    public InValid GetInValid(int id)
    {
        try
        {
            var c = from cs in DB.InValids
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<InValid> GetInValids()
    {
        try
        {
            return DB.InValids.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteInValid(InValid info)
    {
        try
        {
            DB.InValids.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteInValid(int id)
    {
        try
        {
            DB.InValids.DeleteOnSubmit(GetInValid(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion
    public RoleAccess GetRoleAccess(int Roleid, int Accessid)
    {
        if (Roleid == Null.NullInteger || Accessid == Null.NullInteger)
            return null;
        try
        {
            var c = from cs in DB.RoleAccesses
                    where cs.RoleID == Roleid && cs.AccessID == Accessid
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public Card GetCard(string id)
    {
        try
        {
            var c = from cs in DB.Cards
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public bool AddUser(User u)
    {
        User old = GetUser(u.UserName);
        if (old != null)
        {
            LastException = new Exception("نام کاربری تکراری است");
            return false;
        }
        if (string.IsNullOrEmpty(u.Password))
        {
            LastException = new Exception("رمز عبور خالی است");
            return false;
        }
        DB.Users.InsertOnSubmit(u);
        return Update();
    }

    #region FieldFormula
    public FieldFormula GetFieldFormula(int id)
    {
        try
        {
            var c = from cs in DB.FieldFormulas
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetFieldFormulaName(int id)
    {
        FieldFormula fieldFormula = GetFieldFormula(id);
        return fieldFormula == null ? "" : fieldFormula.Name;
    }
    public FieldFormula GetFieldFormula(string name)
    {
        try
        {
            var c = from cs in DB.FieldFormulas
                    where cs.Name == name
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<FieldFormula> GetFieldFormulas()
    {
        try
        {
            return DB.FieldFormulas.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteFieldFormula(FieldFormula info)
    {
        if (info == null)
            return false;
        try
        {
            DB.FieldFormulas.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteFieldFormula(int id)
    {
        try
        {
            DB.FieldFormulas.DeleteOnSubmit(GetFieldFormula(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    public string GetFieldFormul(string name)
    {

        FieldFormula fieldFormula = GetFieldFormula(name);
        return fieldFormula.Formula;

    }
    #endregion

    #region PassengerList
    public PassengerList GetPassengerList(int id)
    {
        try
        {
            var c = from cs in DB.PassengerLists
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public PassengerList GetPassengerListByList(string list)
    {
        try
        {
            var c = from cs in DB.PassengerLists
                    where cs.List == list
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public PassengerList GetPassengerListByTitle(string title)
    {
        try
        {
            var c = from cs in DB.PassengerLists
                    where cs.Title == title
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<PassengerList> GetPassengerLists()
    {
        try
        {
            return DB.PassengerLists.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeletePassengerList(int id)
    {
        try
        {
            DB.PassengerLists.DeleteOnSubmit(GetPassengerList(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeletePassengerList(PassengerList info)
    {
        try
        {
            DB.PassengerLists.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public string ValidatePassengerList(PassengerList info)
    {
        PassengerList old = GetPassengerListByList(info.List);
        if (old != null && old.ID != info.ID)
            return "لیست مسافرین تکراري است";
        old = GetPassengerListByTitle(info.Title);
        if (old != null && old.ID != info.ID)
            return "عنوان تکراري است";
        return "";
    }
    #endregion

    #region TelEntry
    public TelEntry GetTelEntry(int id)
    {
        try
        {
            var c = from cs in DB.TelEntries
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public List<TelEntry> GetTelEntries()
    {
        try
        {
            return DB.TelEntries.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteTelEntry(TelEntry info)
    {
        try
        {
            DB.TelEntries.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteTelEntry(int id)
    {
        try
        {
            DB.TelEntries.DeleteOnSubmit(GetTelEntry(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Reports
    public ISingleResult<GetSuratVaziatResult> GetSuratVaziat(int id)
    {
        try
        {
            return DB.GetSuratVaziat(id);
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetSuratVaziatExclusiveResult> GetSuratVaziatExclusive(int id)
    {
        try
        {
            return DB.GetSuratVaziatExclusive(id);
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetUserSaleResult> GetUserSale(string fromDate, string toDate, string fdTime, string tdTime, int userID)
    {
        try
        {
            return DB.GetUserSale(fromDate, toDate, fdTime, tdTime, userID);
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetUsersSaleResult> GetUsersSale(int tripID)
    {
        try
        {
            return DB.GetUsersSale(tripID);
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetCartableResult> GetCartable(string fromDate, string toDate, int serviceID, int userID, string series, int destcityID)
    {
        try
        {
            return DB.GetCartable(fromDate, toDate, serviceID, userID, series, destcityID);
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetCartable2Result> GetCartable2(string fromDate, string toDate, int serviceID, int userID, string series)
    {
        try
        {
            return DB.GetCartable2(fromDate, toDate, serviceID, userID, series);
        }
        catch
        {
        }
        return null;
    }
    public List<GetCartable2Result> getmonthCartable(string fromDate, string toDate, int serviceID, int userID, string series)
    {
        try
        {
            return DB.GetCartable2(fromDate, toDate, serviceID, userID, series).ToList();
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetSuratVaziatSummaryResult> GetSuratVaziatSummary(string fromDate, string toDate, int specialID, int serviceID, int driverID, int carTypeID, int carID, int destID, int fundID, int fno, int tno, string series)
    {
        try
        {
            int timeout = DB.CommandTimeout;
            return DB.GetSuratVaziatSummary(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series);
        }
        catch
        {
        }
        return null;
    }
    public ISingleResult<GetInsuranceSummaryResult> GetInsuranceSummary(string fromDate, string toDate, int specialID, int serviceID, int driverID, int carTypeID, int carID, int destID, int fundID, int fno, int tno, string series)
    {
        try
        {
            // List<GetInsuranceSummaryResult> sorted = GetInsuranceSummarySorted(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series);
            return DB.GetInsuranceSummary(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series);
        }
        catch
        {
        }
        return null;
    }
    public List<GetInsuranceSummaryResult> GetInsuranceSummarySorted(string fromDate, string toDate, int specialID, int serviceID, int driverID, int carTypeID, int carID, int destID, int fundID, int fno, int tno, string series)
    {
        try
        {
            bool sortDNT = Tool.GetBool(GetSettingValue("SortDNT"), true);
            List<GetInsuranceSummaryResult> res = DB.GetInsuranceSummary(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series).ToList();
            if (sortDNT)
                res = res.OrderBy(c => c.TripType).ThenBy(c => c.Date).ThenBy(c => c.NO).ThenBy(c => c.DepartureTime).ToList();
            else
                res = res.OrderBy(c => c.TripType).ThenBy(c => c.Date).ThenBy(c => c.DepartureTime).ThenBy(c => c.NO).ToList();
            return res;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return null;
    }

    public IQueryable<TicketClass> GetTicketClass(int ticketID)
    {
        return from d in DB.Tickets
               where d.ID == ticketID
               select new TicketClass
               {
                   NO = d.No,
                   Fullname = d.Fullname,
                   SrcCity = d.Trip.Service.Path.City1.Title,
                   DestCity = d.Trip.Service.Path.City.Title,
                   Date = DateTime.Parse(d.Trip.Date.ToString()),
                   DepartureTime = d.Trip.Service.DepartureTime,
                   PassengersCount = (int)DB.GetPassengersCount(d.TripID),
                   ChairsNo = DB.GetChairsNo(d.TripID),
                   Price = d.Price,
                   SaleDate = d.SaleDate
               };
    }
    public ISingleResult<GetPayGetResult> GetPayGet(string paymentID)
    {
        try
        {
            return DB.GetPayGet(paymentID);
        }
        catch
        {
        }
        return null;
    }

    #endregion

    public bool AutoDeleteTrips()
    {
        try
        {
            if (Tool.GetBool(GetSettingValue("AutoDeleteMini"), false) == false)
                return true;
            List<int> ids = (from tr in DB.Trips
                             where tr.CarType.Layout.NumChairs < 25 && tr.Tickets.Count == 0
                             select tr.ID).ToList();
            for (int i = 0; i < ids.Count; i++)
                DeleteTrip(ids[i]);
        }
        catch (Exception ex)
        {
            LastException = ex;
            return false;
        }
        return true;
    }

    public DataTable FillDataTable(string sql)
    {
        DataTable dt = new DataTable();
        System.Data.SqlClient.SqlDataAdapter adapter = null;
        try
        {
            //DB.Connection.ConnectionString;
            adapter = new System.Data.SqlClient.SqlDataAdapter(sql, System.Configuration.ConfigurationManager.ConnectionStrings["TransportConnectionString3"].ConnectionString);
            adapter.Fill(dt);
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        finally
        {
            adapter.Dispose();
        }
        return dt;
    }
    public DataTable FillDataTable(string sql, string cntStr)
    {
        DataTable dt = new DataTable();

        System.Data.SqlClient.SqlDataAdapter adapter = null;
        try
        {
            //DB.Connection.ConnectionString;

            adapter = new System.Data.SqlClient.SqlDataAdapter(sql, cntStr);
            adapter.Fill(dt);
        }
        catch (Exception ex)
        {
            dt = null;
            LastException = ex;
        }
        finally
        {
            try
            {
                adapter.SelectCommand.Connection.Close();
                adapter.Dispose();
            }
            catch { }
        }
        return dt;
    }
    public int RunQuery(string sql, string cntStr)
    {
        System.Data.SqlClient.SqlConnection cnt = new System.Data.SqlClient.SqlConnection(cntStr);
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, cnt);
        try
        {
            cnt.Open();
            //DB.Connection.ConnectionString;
            return cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        finally
        {
            try
            {
                cnt.Close();
                cnt.Dispose();
            }
            catch { }
            try
            {
                cmd.Dispose();
            }
            catch { }
        }
        return 0;
    }

    #region Seller
    public Seller GetSeller(int? id)
    {
        if (id.HasValue)
            return GetSeller(id.Value);
        return null;
    }
    public Seller GetSeller(int id)
    {
        try
        {
            var c = from cs in DB.Sellers
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public string GetSellerTitle(int id)
    {
        try
        {
            var c = from cs in DB.Sellers
                    where cs.ID == id
                    select cs.Title;
            return c.First();
        }
        catch
        {
        }
        return "";
    }
    public List<Seller> GetSellers()
    {
        try
        {
            return DB.Sellers.ToList();
        }
        catch
        {
        }
        return null;
    }
    public List<Seller> GetSellers(bool enabled)
    {
        try
        {
            return DB.Sellers.Where(s => s.Enabled == enabled).ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteSeller(Seller info)
    {
        try
        {
            DB.Sellers.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    public bool DeleteSeller(int id)
    {
        try
        {
            DB.Sellers.DeleteOnSubmit(GetSeller(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region Companiy

    public int GetCompanyId(string code, string cntStr)
    {
        string qry = string.Format("SELECT * FROM Company WHERE Code=N'{0}'" , code);
        DataTable dt = Helper.Instance.FillDataTable(qry, cntStr);
        if (dt!=null)
        {
            return Tool.GetInt(dt.Rows[0]["ID"]);
        }
        return -1;
    }
    public Company GetCompany(int id)
    {
        try
        {
            var c = from cs in DB.Companies
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public List<Company> GetCompanies()
    {
        try
        {
            return DB.Companies.ToList();
        }
        catch
        {
        }
        return null;
    }
    public bool DeleteCompany(int id)
    {
        try
        {
            DB.Companies.DeleteOnSubmit(GetCompany(id));
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }
    #endregion

    #region CompanyTrips
    public bool AddCompanyTrips(CompanyTrips companyTrips)
    {
        string cntStr =
            System.Configuration.ConfigurationManager.ConnectionStrings["TechnicalConnectionString"].ConnectionString;
        string qry =
            string.Format(
                @"INSERT INTO CompanyTrips (CompanyID, TripID, UserID, CarNo, Status, Comments, Driver, TripSeries, TripNo) VALUES ({0},{1},{2},N'{3}',{4},N'{5}', N'{6}', N'{7}', {8})", companyTrips.CompanyID, companyTrips.TripID, companyTrips.UserID, companyTrips.CarNo, companyTrips.Status, companyTrips.Comments, companyTrips.Driver, companyTrips.TripSeries, companyTrips.TripNo);
        return RunQuery(qry, cntStr) > 0;
    }

    public CompanyTrips GetCompanyTrip(int id)
    {
        try
        {
            var c = from cs in DB.CompanyTrips
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }
    public static string StatusToString(Status status, string defaultValue)
    {
        switch (status)
        {
            case (Status.NoCheck):
                return "بررسی نشده";
            case (Status.Confirm):
                return "تایید شده";
            case (Status.Reject):
                return "رد شده";
        }
        return defaultValue;
    }

    public string CheckCartable()
    {
        var c = from cs in DB.CompanyTrips
                where cs.Status == (int)Status.NoCheck
                select cs;
        int count = c.Count();
        return count.ToString();

    }
    #endregion

    #region CompanyTripImpl
    public bool AddCompanyTripImpl(CompanyTripImpl companyTripImpl)
    {
        string cntStr =
            System.Configuration.ConfigurationManager.ConnectionStrings["TechnicalConnectionString"].ConnectionString;
        string qry =
            string.Format(
                @"INSERT INTO CompanyTripImpl (CompanyID, TripID, UserID, CarNo, Status, Comments, Date, Driver, TripSeries, TripNo) VALUES ({0},{1},{2},N'{3}',{4},N'{5}',N'{6}', N'{7}',N'{8}', {9})", companyTripImpl.CompanyID, companyTripImpl.TripID, companyTripImpl.UserID, companyTripImpl.CarNo, companyTripImpl.Status, companyTripImpl.Comments, companyTripImpl.Date, companyTripImpl.Driver,companyTripImpl.TripSeries,companyTripImpl.TripNo);
        return RunQuery(qry, cntStr) > 0;
    }
    #endregion

    #region CarsCompany
    public CarsCompany GetCarsCompany(string smartCard, string plate, int? companyid)
    {
        if (string.IsNullOrEmpty(smartCard))
            return null;
        try
        {
            var c = from cs in DB.CarsCompanies
                    where cs.SmartCard == smartCard && cs.Plate==plate && cs.CompanyID==companyid
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public CarsCompany GetCarsCompany(int id)
    {
        try
        {
            var c = from cs in DB.CarsCompanies
                    where cs.ID == id
                    select cs;
            return c.First();
        }
        catch
        {
        }
        return null;
    }

    public bool DeleteCarsCompany(CarsCompany info)
    {
        try
        {
            DB.CarsCompanies.DeleteOnSubmit(info);
            DB.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
        return false;
    }

    #endregion

    #region CarTechnical

    public CarTechnical GetCarTechnical(int id)
    {
        try
        {
            var c = from cs in DB.CarTechnicals
                where cs.ID == id
                select cs;
            return c.First();

        }
        catch
        {
        }
        return null;
    }
    public CarTechnical GetCarTechnical(string smartcard, string plate, string date)
    {
        try
        {
            var c = from cs in DB.CarTechnicals
                where cs.SmartCard == smartcard && cs.Plate == plate && cs.Date==date
                select cs;
            return c.First();

        }
        catch
        {
        }
        return null;
    }

    public CarTechnical GetCarTechnical(string cntStr,string smartcard, string plate, string date)
    {
        try
        {
            string query =
                string.Format("SELECT * FROM CarTechnical WHERE SmartCard=N'{0}' AND Plate=N'{1}' AND Date=N'{2}'",
                    smartcard, plate, date);
            IDataReader dr = DbHelper.Instance.ExecuteReader(cntStr, query);
            return ObjectBinder.FillObject<CarTechnical>(dr);
        }
        catch
        {
        }
        return null;
    }

    public static string TechnicalStatusToString(TechnicalStatus status, string defaultValue)
    {
        switch (status)
        {
            case (TechnicalStatus.Confirm):
                return "تایید شده";
            case (TechnicalStatus.Reject):
                return "رد شده";
        }
        return defaultValue;
    }
    #endregion
    #region Tracking Code
    public static string TripStateErrorToString(TripStateError Tse, string defaultValue)
    {
        switch (Tse)
        {
            case (TripStateError.HaveErrorAndNeedTryAgain):
                return "بروز خطا در ثبت اطلاعات لطفا مجددا درخواست نمایید";

            case (TripStateError.EditTimeIsInvalid):
                return "زمان برای ویرایش صورت مجاز نمی باشد";

            case (TripStateError.InvalidSendingCode):
                return "نامعتبر بودن کد رهگیری ارسالی";

            case (TripStateError.InvalidTimeForCanceling):
                return "زمان برای ابطال صورت مجاز نمی باشد";

            case (TripStateError.SendingParameterInvalid):
                return "پارامتر ارسالی مجاز نمی باشد. " + defaultValue;

            case (TripStateError.RequestUserIsInvalid):
                return "کاربر درخواست کننده مجاز نمی باشد";

            case (TripStateError.OldPassForChangePassIsInvalid):
                return "رمز قدیم برای تغییر رمز عبور صحیح نمی باشد";

            case (TripStateError.FreighterSmartCadIsDisable):
                return "کارت هوشمند ناوگان فعال نمی باشد";

            case (TripStateError.FirstDriverSmartCardIsDisable):
                return "کارت هوشمند راننده اول فعال نمی باشد";

            case (TripStateError.SecondDriverSmartCardIsDisable):
                return "کارت هوشمند راننده دوم فعال نمی باشد";

            case (TripStateError.ThirdDriverSmartCardIsDisable):
                return "کارت هوشمند راننده سوم فعال نمی باشد";

            case (TripStateError.CodeOfRequestCompanyIsDisable):
                return "کد شرکت درخواستی فعال نمی باشد";
        }
        return defaultValue;
    }
    public static string GetSeries(string seri, bool checkLen)
    {
        try
        {
            string[] series = seri.Split(',');
            if (series.Length >= 3)
                seri = string.Format("{0}/{1}/{2}", series[2], series[1], series[0]);
        }
        catch { }
        if (checkLen)
            return GetString(Encode(seri.Replace("الف", "ا")), 7);
        return Encode(seri.Replace("الف", "ا"));
    }
    public static string GetString(object obj, int len)
    {
        if (obj == null)
            return new string('0', len);
        string res = obj.ToString().PadLeft(len, '0');
        if (res.Length > len)
            res = res.Substring(0, len);
        return res;
    }
    public static string Encode(string str)
    {
        if (string.IsNullOrEmpty(str))
            return str;
        str = str.Replace('ی', 'ي');
        str = str.Replace('ي', 'ي');
        str = str.Replace('ك', 'ك');
        str = str.Replace('ک', 'ك');
        string ins = "آابپتثجچحخدذرزژسشصضطظعغفقكگلمنوهيئ";
        string outs = "ABCDEFGHabcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < ins.Length; i++)
            str = str.Replace(ins[i], outs[i]);
        return str;
    }
    public static void GetPlateSeri(Car car, out string plateNo, out string plateSeries)
    {
        plateNo = "";
        plateSeries = "";
        if (car != null)
        {
            if (!string.IsNullOrEmpty(car.Plate))
            {
                string[] plates = car.Plate.Split('-');
                if (plates.Length == 2)
                {
                    plateNo = plates[0];
                    plateSeries = plates[1];
                }
                else if (plates.Length == 4)
                {
                    plateNo = string.Format("{0}{1}{2}", plates[3], plates[2], plates[1]);
                    plateSeries = plates[0];
                }
                else
                {
                    plateNo = car.Plate;
                    plateSeries = "";
                }
            }
            //string ins = "آابپتثجچحخدذرزژسشصضطظعغفقكگلمنوهيئ";
            //for (int i = 0; i < ins.Length; i++)
            //{
            //    plateNo = plateNo.Replace(ins[i].ToString(), "");
            //    plateSeries = plateSeries.Replace(ins[i].ToString(), "");
            //}
            plateNo = Encode(GetString(plateNo.Replace(" ", "").Replace("الف", "ا"), 6));// Encode(plateNo);
            plateSeries = Encode(GetString(plateSeries.Replace(" ", ""), 2).Replace("الف", "ا")); //Encode(plateSeries);
        }
    }
    public bool RequestTrackCode(Trip trip)
    {
        // Check if it has already a Tracking Code
        if (!string.IsNullOrEmpty(trip.TrackCode))
            return true;
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        // Drivers
        Driver driver1 = null, driver2 = null, driver3 = null;
        driver1 = Helper.Instance.GetDriver(trip.DriverID1.Value);
        if (trip.DriverID2 != null)
            driver2 = Helper.Instance.GetDriver(trip.DriverID2.Value);
        if (trip.DriverID3 != null)
            driver3 = Helper.Instance.GetDriver(trip.DriverID3.Value);

        // Car
        Car car = GetCar(trip.CarID);
        string plateNo = "", plateSeries = "";
        GetPlateSeri(car, out plateNo, out plateSeries);

        // Insurance
        Service service = GetService(trip.ServiceID);
        //List<InsuranceKM> Inskms = GetInsuranceKMs(car.InsuranceID);
        //int insuranceBody = 0;
        //foreach (InsuranceKM i in Inskms)
        //    if (service.Path.KMs < i.EndKM && service.Path.KMs > i.StartKM)
        //        insuranceBody = i.Price;

        // Cities
        City srcCity = GetCity(service.Path.SrcCityID);
        City destCity = GetCity(service.Path.DestCityID);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Insert(userID, passwordID
                , identityNo, trip.No, GetSeries(trip.Series, false),
                (trip.IsMini || trip.IsSewari) ? 2 : 1
                // Drivers
                , driver1.SmartCard, driver2 != null ? driver2.SmartCard : "", driver3 != null ? driver3.SmartCard : ""
                , car.SmartCard
                // Prices
                , trip.TotalPrice.Value, service.Path.KMs, trip.BodyInsurance, service.Insurance
                // Cities
                , srcCity.Code, destCity.Code
                // Date
                , Tool.ToPersianDate(trip.Date, ""), trip.DepartureTime2
                , GetNumTickets(trip.ID)
                // Car Type
                , (int)trip.CarType.Code, companyCode, (int)trip.CarType.Layout.NumChairs
                // Plaque
                , plateNo, Tool.GetInt(plateSeries, 11)
                , 1);
        }
        catch
        {
            //throw;
            throw new Exception("ارتباط قطع است دوباره سعی کنید و اگر بعد از چند بار ارتباط قطع بود می توانید صورت را به صورت آفلاین بسته و بعدا برای دریافت کد اقدام نمایید");
        }

        if (string.IsNullOrEmpty(value))
            return false;
        string[] words = value.Split(';');
        if (words.Length < 2)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0 && errorCode!=-2)
            throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        trip.TrackCode = words[1];
        trip.TrackDate = DateTime.Now;
        return true;
    }
    public bool RequestTrackCode(Exclusive exclusive)
    {
        // Check if it has already a Tracking Code
        if (!string.IsNullOrEmpty(exclusive.TrackCode))
            return true;
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        // Drivers
        Driver driver1 = null, driver2 = null, driver3 = null;
        driver1 = Helper.Instance.GetDriver(exclusive.DriverID1);
        if (exclusive.DriverID2 != null)
            driver2 = Helper.Instance.GetDriver(exclusive.DriverID2.Value);
        //if (trip.DriverID3 != null)
        //    driver3 = Helper.Instance.GetDriver(exclusive.DriverID3.Value);

        // Car
        Car car = GetCar(exclusive.CarID);
        string plateNo = "", plateSeries = "";
        GetPlateSeri(car, out plateNo, out plateSeries);

        // Cities
        Path path = GetPath(exclusive.PathID);
        City srcCity = GetCity(path.SrcCityID);
        City destCity = GetCity(path.DestCityID);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Insert(userID, passwordID
                , identityNo, exclusive.No, GetSeries(exclusive.Series, false),
                3
                // Drivers
                , driver1.SmartCard, driver2 != null ? driver2.SmartCard : "", driver3 != null ? driver3.SmartCard : ""
                , car.SmartCard
                // Prices
                , exclusive.TotalRent, path.KMs, exclusive.BodyInsurance, exclusive.Insurance
                // Cities
                , srcCity.Code, destCity.Code
                // Date
                , Tool.ToPersianDate(exclusive.DepartureDate, ""), exclusive.DepartureTime
                , exclusive.NumPassangers
                // Car Type
                , (int)car.CarType.Code, companyCode, (int)car.CarType.Layout.NumChairs
                // Plaque
                , plateNo, Tool.GetInt(plateSeries, 11)
                , 1);
        }
        catch
        {
            throw;
            //throw new Exception("با عرض پوزش سرور قادر به پاسخگویی نیست لطفا چند لحظه دیگر درخواست نمایید ");
        }

        if (string.IsNullOrEmpty(value))
            return false;
        string[] words = value.Split(';');
        if (words.Length < 2)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0)
            throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        exclusive.TrackCode = words[1];
        exclusive.TrackDate = DateTime.Now;
        return true;
    }
    public bool CancelTrackCode(Trip trip)
    {
        // Check if it has already a Tracking Code
        if (string.IsNullOrEmpty(trip.TrackCode))
            throw new Exception("این صورت، کد رهگیری ندارد.");
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Cancel_System_Code(userID, passwordID
                , identityNo, companyCode, trip.TrackCode);
        }
        catch
        {
            throw;
        }

        if (string.IsNullOrEmpty(value))
            return false;
        // توضیحات فارسی ; کد رهگیری ارسالی ; کد حاصل از سیستم
        string[] words = value.Split(';');
        if (words.Length < 3)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0)
            throw new Exception(words[2]);
        //throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        trip.TrackCode = "";
        trip.TrackDate = DateTime.Now;
        return true;
    }
    public bool CancelTrackCodeNoCode(Trip trip)
    {
        // Check if it has already a Tracking Code
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Cancel_MOSNO_Code(userID, passwordID
                , identityNo, companyCode, trip.No, GetSeries(trip.Series, false)
                , (trip.IsMini || trip.IsSewari) ? 2 : 1);
        }
        catch
        {
            throw;
        }

        if (string.IsNullOrEmpty(value))
            return false;
        // توضیحات فارسی ; کد رهگیری ارسالی ; کد حاصل از سیستم
        string[] words = value.Split(';');
        if (words.Length < 3)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0)
            throw new Exception(words[2]);
        //throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        trip.TrackCode = "";
        trip.TrackDate = DateTime.Now;
        return true;
    }
    public bool CancelTrackCodeNoCode(int no, string seri, int carType)
    {
        // Check if it has already a Tracking Code
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Cancel_MOSNO_Code(userID, passwordID
                , identityNo, companyCode, no, seri
                , carType);
        }
        catch
        {
            throw new Exception("ارتباط قطع است.");
        }

        if (string.IsNullOrEmpty(value))
            return false;
        // توضیحات فارسی ; کد رهگیری ارسالی ; کد حاصل از سیستم
        string[] words = value.Split(';');
        if (words.Length < 3)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0)
            throw new Exception(words[2]);
        //throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        return true;
    }
    #endregion

    public bool CancelTrackCode(Exclusive exlusive)
    {
        // Check if it has already a Tracking Code
        if (string.IsNullOrEmpty(exlusive.TrackCode))
            throw new Exception("این صورت، کد رهگیری ندارد.");
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Cancel_System_Code(userID, passwordID
                , identityNo, companyCode, exlusive.TrackCode);
        }
        catch
        {
            throw;
        }

        if (string.IsNullOrEmpty(value))
            return false;
        // توضیحات فارسی ; کد رهگیری ارسالی ; کد حاصل از سیستم
        string[] words = value.Split(';');
        if (words.Length < 3)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0)
            throw new Exception(words[2]);
        //throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        exlusive.TrackCode = "";
        exlusive.TrackDate = DateTime.Now;
        return true;
    }
    public bool CancelTrackCodeNoCode(Exclusive exlusive)
    {
        // Check if it has already a Tracking Code
        string userID = Helper.Instance.GetSettingValue("User_Id");
        string passwordID = Helper.Instance.GetSettingValue("Password_Id");
        int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

        string value = null;
        try
        {
            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            value = statement.Statement_Cancel_MOSNO_Code(userID, passwordID
                , identityNo, companyCode, exlusive.No, GetSeries(exlusive.Series, false)
                , 3);
        }
        catch
        {
            throw;
        }

        if (string.IsNullOrEmpty(value))
            return false;
        // توضیحات فارسی ; کد رهگیری ارسالی ; کد حاصل از سیستم
        string[] words = value.Split(';');
        if (words.Length < 3)
            throw new Exception("جواب دریافت شده معتبر نمیباشد");
        int errorCode = Tool.GetInt(words[0], -1);
        if (errorCode != 0)
            throw new Exception(words[2]);
        //throw new Exception(TripStateErrorToString((TripStateError)errorCode, words[2]));
        exlusive.TrackCode = "";
        exlusive.TrackDate = DateTime.Now;
        return true;
    }
    public void ChangeSeries(bool withCode, Trip mTrip)
    {
        if (string.IsNullOrEmpty(mTrip.Series) || mTrip.Series.Trim(',').Length == 0 || mTrip.No < 1)
            throw new Exception("سرویس سری و شماره صورت ندارد و نمیتوان سری و شماره صورت را تغییر داد");
        string seri = mTrip.Series;
        int no = mTrip.No;
        mTrip.Series = "";
        mTrip.No = 0;
        mTrip.Closed = false;
        string trackCode = mTrip.TrackCode;

        try
        {
            if (withCode)
            {
                if (!CancelTrackCode(mTrip))
                    throw new Exception("اشکال در ابطال آنلاین صورت وضعیت ");
            }
            else
            {
                try
                {
                    CancelTrackCodeNoCode(mTrip);
                }
                catch { }
                mTrip.TrackCode = "";
                mTrip.TrackDate = DateTime.Now;
                //if (!CancelTrackCodeNoCode(mTrip))
                //    throw new Exception("اشکال در ابطال آنلاین صورت وضعیت ");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("خطا در ابطال صورت وضعیت: " + ex.Message);
        }

        InValid v = new InValid();
        v.Date = mTrip.Date;
        v.Ticketless = mTrip.Ticketless;
        v.TicketlessPrice = mTrip.TicketlessPrice;
        v.TicketlessName = mTrip.TicketlessName;
        v.Stamp = mTrip.Stamp;
        v.Comission = mTrip.Comission;
        v.BodyInsurance = mTrip.BodyInsurance;
        v.ExtraCost = mTrip.ExtraCost;
        v.Closed = mTrip.Closed;
        v.Price = mTrip.Price;
        v.Toll = mTrip.Toll;
        v.Insurance = mTrip.Insurance;
        v.Reception = mTrip.Reception;
        v.TotalPrice = mTrip.TotalPrice;
        v.Comission2 = mTrip.Comission2;
        v.OtherDeficits = mTrip.OtherDeficits;
        v.SumDeficits = mTrip.SumDeficits;
        v.AutoShare = mTrip.AutoShare;
        v.Toll2 = mTrip.Toll2;
        v.Locked = mTrip.Locked;
        v.Others = mTrip.Others;
        v.TripID = mTrip.ID;
        v.No = no;
        v.Series = seri;
        v.TrackCode = trackCode;
        v.CarID = mTrip.CarID;
        v.DriverID1 = mTrip.DriverID1;
        v.DriverID2 = mTrip.DriverID2;
        v.DriverID3 = mTrip.DriverID3;
        v.ServiceID = mTrip.ServiceID;
        DB.InValids.InsertOnSubmit(v);
        //DB.Trips.DeleteOnSubmit(mTrip);

        if (!Update())
        {
            throw new Exception(Helper.Instance.LastException.Message);
        }
    }

    public void CancelExclusive(bool withCode, Exclusive exlusive)
    {
        if (exlusive == null)
            return;
        //if (string.IsNullOrEmpty(exlusive.Series) || exlusive.Series.Trim(',').Length == 0 || exlusive.No < 1)
        //    throw new Exception("سرویس سری و شماره صورت ندارد و نمیتوان سری و شماره صورت را تغییر داد");
        string seri = exlusive.Series;
        int no = exlusive.No;
        exlusive.Series = "";
        exlusive.No = 0;
        exlusive.CloseDate = null;

        try
        {
            if (withCode)
            {
                if (!CancelTrackCode(exlusive))
                    throw new Exception("اشکال در ابطال آنلاین صورت وضعیت دربستی");
            }
            else
            {
                try
                {
                    if (!CancelTrackCodeNoCode(exlusive))
                        throw new Exception("اشکال در ابطال آنلاین صورت وضعیت دربستی");
                }
                catch { }
                exlusive.TrackCode = "";
                exlusive.TrackDate = DateTime.Now;
            }
            try
            {
                CancelInsurance(exlusive);
            }
            catch { }

        }
        catch (Exception ex)
        {
            throw new Exception("خطا در ابطال صورت وضعیت دربستی: " + ex.Message);
        }

        InValid v = new InValid();
        v.Date = exlusive.DepartureDate;
        v.Ticketless = 0;
        v.TicketlessPrice = 0;
        v.TicketlessName = "";
        v.Stamp = 0;
        v.Comission = exlusive.Commission;
        v.BodyInsurance = exlusive.BodyInsurance;
        v.ExtraCost = exlusive.ExtraPrice.HasValue ? exlusive.ExtraPrice.Value : 0;
        v.Closed = true;
        v.Price = exlusive.Price;
        v.Toll = exlusive.Toll;
        v.Insurance = exlusive.Insurance;
        v.Reception = 0;
        v.TotalPrice = exlusive.TotalRent;
        //v.Comission2 = mExclusive.Comission2;
        //v.OtherDeficits = mExclusive.OtherDeficits;
        //v.SumDeficits = mExclusive.SumDeficits;
        //v.AutoShare = mExclusive.AutoShare;
        //v.Toll2 = mExclusive.Toll2;
        //v.Locked = mExclusive.Locked;
        //v.Others = mExclusive.Others;
        v.TripID = exlusive.ID;
        v.No = no;
        v.Series = seri;
        v.CarID = exlusive.CarID;
        v.DriverID1 = exlusive.DriverID1;
        v.DriverID2 = exlusive.DriverID2;
        v.DriverID3 = exlusive.DriverID3;
        v.ServiceID = exlusive.PathID;
        v.Exclusive = true;
        Helper.Instance.DB.Exclusives.DeleteOnSubmit(exlusive);
        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        if (!Helper.Instance.Update())
            throw new Exception("اشکال در ابطال : " + Helper.Instance.LastException.Message);
    }

    public string RequestInsurance(Trip trip)
    {
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);
        string InsUserName = Helper.Instance.GetSettingValue("InsUserName");
        string InsPassWord = Helper.Instance.GetSettingValue("InsPassWord");

        Driver driver1 = null, driver2 = null, driver3 = null;
        Car car = null;
        if (trip.DriverID1 != null)
            driver1 = GetDriver(trip.DriverID1);
        if (trip.DriverID2 != null)
            driver2 = GetDriver(trip.DriverID2);
        if (trip.DriverID3 != null)
            driver3 = GetDriver(trip.DriverID3);
        if (trip.CarID != null)
            car = GetCar(trip.CarID);
        string plateNo = "", plateSeries = "";
        if (car != null)
            GetPlateSeri(car, out plateNo, out plateSeries);
        Service service = GetService(trip.ServiceID);
        City srcCity = GetCity(service.Path.SrcCityID);
        City desCity = GetCity(service.Path.DestCityID);

        com.danainsurance.statement.StatementWS statement = new com.danainsurance.statement.StatementWS();
        string input = string.Format(@"<Statement>
                                       <GroupId>{0}</GroupId>
                                       <Password>{1}</Password>
                                       <StatementNo>{2}</StatementNo>
                                       <StatementSerial>{3}</StatementSerial>
                                       <StatementType>{4}</StatementType>
                                       <DriverSmartCard1>{5}</DriverSmartCard1>
                                       <DriverSmartCard2>{6}</DriverSmartCard2>
                                       <DriverSmartCard3>{7}</DriverSmartCard3>
                                       <NavySmartCard>{8}</NavySmartCard>
                                       <TotalRentalPrice>{9}</TotalRentalPrice>
                                       <CityDistance>{10}</CityDistance>
                                       <BodyInsurancePrice>{11}</BodyInsurancePrice>
                                       <AboardInsurancePrice>{12}</AboardInsurancePrice>
                                       <OriginCode>{13}</OriginCode>
                                       <DestinationCode>{14}</DestinationCode>
                                       <MoveDate>{15}</MoveDate>
                                       <MoveTime>{16}</MoveTime>
                                       <PassengerCount>{17}</PassengerCount>
                                       <ChairCount>{18}</ChairCount>
                                       <NavyType>{19}</NavyType>
                                       <CompanyCode>{20}</CompanyCode>
                                       <PlaqueNumber>{21}</PlaqueNumber>
                                       <PlaqueSerial>{22}</PlaqueSerial>
                                       <ValidateTime>{23}</ValidateTime>
                                       <IsTtwicePaid>{24}</IsTtwicePaid>
                                       <CarType>{25}</CarType>
                                       </Statement>"
                                       , InsUserName, InsPassWord, trip.No, GetSeries(trip.Series, false)
                                       , (trip.IsMini || trip.IsSewari) ? "MINIBUS-SEDAN" : "BUS"
                                       , driver1 != null ? driver1.SmartCard : "", driver2 != null ? driver2.SmartCard : "", driver3 != null ? driver3.SmartCard : ""
                                       , car != null ? car.SmartCard : "", trip.TotalPrice, service.Path.KMs
                                       , trip.BodyInsurance, trip.Insurance, srcCity.Code
                                       , desCity.Code, Tool.ToPersianDate(trip.Date, "")
                                       , trip.DepartureTime2, GetNumTickets(trip.ID), trip.CarType.Layout.NumChairs
                                       , trip.IsSewari ? "SEDAN" : trip.IsMini ? "MINIBUS" : "BUS", companyCode
                                       , plateNo, plateSeries, 30, 1, "SPECIAL");
        string value = null;
        try
        {
            value = statement.Statement(input);
        }
        catch (Exception)
        {

            throw;
        }
        return value;
    }

    public string RequestInsurance(Exclusive exclusive)
    {
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);
        string InsUserName = Helper.Instance.GetSettingValue("InsUserName");
        string InsPassWord = Helper.Instance.GetSettingValue("InsPassWord");

        Driver driver1 = null, driver2 = null;
        Car car = null;
        if (exclusive.DriverID1 != null)
            driver1 = GetDriver(exclusive.DriverID1);
        if (exclusive.DriverID2 != null)
            driver2 = GetDriver(exclusive.DriverID2);
        if (exclusive.CarID != null)
            car = GetCar(exclusive.CarID);
        string plateNo = "", plateSeries = "";
        if (car != null)
            GetPlateSeri(car, out plateNo, out plateSeries);
        City srcCity = GetCity(exclusive.Path.SrcCityID);
        City desCity = GetCity(exclusive.Path.DestCityID);

        com.danainsurance.statement.StatementWS statement = new com.danainsurance.statement.StatementWS();
        string input = string.Format(@"<Statement>
                                       <GroupId>{0}</GroupId>
                                       <Password>{1}</Password>
                                       <StatementNo>{2}</StatementNo>
                                       <StatementSerial>{3}</StatementSerial>
                                       <StatementType>{4}</StatementType>
                                       <DriverSmartCard1>{5}</DriverSmartCard1>
                                       <DriverSmartCard2>{6}</DriverSmartCard2>
                                       <DriverSmartCard3>{7}</DriverSmartCard3>
                                       <NavySmartCard>{8}</NavySmartCard>
                                       <TotalRentalPrice>{9}</TotalRentalPrice>
                                       <CityDistance>{10}</CityDistance>
                                       <BodyInsurancePrice>{11}</BodyInsurancePrice>
                                       <AboardInsurancePrice>{12}</AboardInsurancePrice>
                                       <OriginCode>{13}</OriginCode>
                                       <DestinationCode>{14}</DestinationCode>
                                       <MoveDate>{15}</MoveDate>
                                       <MoveTime>{16}</MoveTime>
                                       <PassengerCount>{17}</PassengerCount>
                                       <ChairCount>{18}</ChairCount>
                                       <NavyType>{19}</NavyType>
                                       <CompanyCode>{20}</CompanyCode>
                                       <PlaqueNumber>{21}</PlaqueNumber>
                                       <PlaqueSerial>{22}</PlaqueSerial>
                                       <ValidateTime>{23}</ValidateTime>
                                       <IsTtwicePaid>{24}</IsTtwicePaid>
                                       <CarType>{25}</CarType>
                                       </Statement>"
                                       , InsUserName, InsPassWord, exclusive.No, GetSeries(exclusive.Series, false)
                                       , "EXCLUSIVE"
                                       , driver1 != null ? driver1.SmartCard : "", driver2 != null ? driver2.SmartCard : "", ""
                                       , car != null ? car.SmartCard : "", exclusive.Price, exclusive.Path.KMs
                                       , exclusive.BodyInsurance, exclusive.Insurance, srcCity.Code
                                       , desCity.Code, Tool.ToPersianDate(exclusive.CloseDate, "")
                                       , Tool.GetTimeOfDate(exclusive.CloseDate, ""), exclusive.NumPassangers, exclusive.Car != null ? exclusive.Car.CarType.Layout.NumChairs : 0
                                       , "BUS", companyCode
                                       , plateNo, plateSeries, 30, 1, "SPECIAL");
        string value = null;
        try
        {
            value = statement.Statement(input);
        }
        catch (Exception)
        {

            throw;
        }
        return value;
    }

    public void CancelInsurance(Trip trip)
    {
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);
        string value = null;
        try
        {
            City srcCity = GetCity(trip.Service.Path.SrcCityID);
            com.danainsurance.statement.StatementWS statement = new com.danainsurance.statement.StatementWS();
            string Input = string.Format(@"<Statement>
                                           <GroupId>{0}</GroupId>
                                           <Password>{1}</Password>
                                           <StatementNo>{2}</StatementNo>
                                           <StatementSerial>{3}</StatementSerial>
                                           <StatementType>{4}</StatementType>
                                           <OriginCode>{5}</OriginCode>
                                           <CancelDate>{6}</CancelDate>
                                           <CompanyCode>{7}</CompanyCode>
                                           </Statement>"
                                           , 1, 1, trip.No, GetSeries(trip.Series, false)
                                           , (trip.IsMini || trip.IsSewari) ? "MINIBUS-SEDAN" : "BUS"
                                           , srcCity.Code, Tool.ToPersianDate(DateTime.Today, "")
                                           , companyCode);
            value = statement.Statement(Input);
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void CancelInsurance(Exclusive exclusive)
    {
        int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);
        string value = null;
        try
        {
            City srcCity = GetCity(exclusive.Path.SrcCityID);
            com.danainsurance.statement.StatementWS statement = new com.danainsurance.statement.StatementWS();
            string Input = string.Format(@"<Statement>
                                           <GroupId>{0}</GroupId>
                                           <Password>{1}</Password>
                                           <StatementNo>{2}</StatementNo>
                                           <StatementSerial>{3}</StatementSerial>
                                           <StatementType>{4}</StatementType>
                                           <OriginCode>{5}</OriginCode>
                                           <CancelDate>{6}</CancelDate>
                                           <CompanyCode>{7}</CompanyCode>
                                           </Statement>"
                                           , 1, 1, exclusive.No, GetSeries(exclusive.Series, false)
                                           , "EXCLUSIVE"
                                           , srcCity.Code, Tool.ToPersianDate(DateTime.Today, "")
                                           , companyCode);
            value = statement.Statement(Input);
        }
        catch (Exception)
        {

            throw;
        }
    }
}
public class TicketClass
{
    public string NO { get; set; }
    public string Fullname { get; set; }
    public string SrcCity { get; set; }
    public string DestCity { get; set; }
    public DateTime Date { get; set; }
    public string DepartureTime { get; set; }
    public int PassengersCount { get; set; }
    public string ChairsNo { get; set; }
    public int Price { get; set; }
    public System.Nullable<DateTime> SaleDate { get; set; }
}


