﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public class SiteSettings
{
    public static string LoginPage
    {
        get
        {
            if (HttpContext.Current.Application["LoginPage"] == null)
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["LoginPage"] = ConfigurationSettings.AppSettings["LoginPage"];
                HttpContext.Current.Application.UnLock();
            }
            return (string)HttpContext.Current.Application["LoginPage"];
        }
    }
    public static string FirstPage
    {
        get
        {
            if (HttpContext.Current.Application["FirstPage"] == null)
            {
                HttpContext.Current.Application.Lock();
                HttpContext.Current.Application["FirstPage"] = ConfigurationSettings.AppSettings["FirstPage"];
                HttpContext.Current.Application.UnLock();
            }
            return (string)HttpContext.Current.Application["FirstPage"];
        }
    }
    public static int UserID
    {
        get
        {
            if (null == HttpContext.Current.Session["UserID"])
                return Null.NullInteger;
            return (int)HttpContext.Current.Session["UserID"];
        }
        set
        {
            HttpContext.Current.Session["UserID"] = value;
            mUser = null;
        }
    }
    private static User mUser = null;
    public static User User
    {
        get
        {
            if (UserID == Null.NullInteger)
                return null;
            if (null == mUser || mUser.ID != UserID)
                mUser = Helper.Instance.GetUser(UserID);
            return mUser;
        }
    }
  

    public static string GetValue(string key)
    {
        return ConfigurationSettings.AppSettings[key];
    }


}