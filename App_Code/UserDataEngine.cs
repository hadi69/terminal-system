﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for UserDataEngine
/// </summary>
public class UserDataEngine 
{
    #region Login / Logout
    public static bool Login(string userName, string password)
    {
        User info = Helper.Instance.GetUser(userName);
        if (info == null || info.Password != password)
            SiteSettings.UserID = Null.NullInteger;
        else
            SiteSettings.UserID = info.ID;
        return SiteSettings.UserID != Null.NullInteger;
    }
    public static void Logout()
    {
        int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]);
        System.Web.HttpContext.Current.Session["UserID"] = Null.NullInteger;
        Helper.Instance.SetUserOffline(id);
    }

    #endregion
    public static bool UserHasAccess(string softType, string csvAccessList)
    {
        if (csvAccessList == null || csvAccessList.Length < 1)
            return false;
        return UserHasAccess(SiteSettings.User, softType, csvAccessList.Split(','));
    }
    public static bool UserHasAccess(string softType, params string[] accessList)
    {
        try
        {
            return UserHasAccess(SiteSettings.User, softType, accessList);
        }
        catch (Exception ex)
        {
        }
        return false;
    }
    public static bool  UserHasAccess(User user,string SoftType, params string[] accessList)
    {
        try
        {
            if (user == null)
                return false;
            string[] type = Helper.Instance.GetSettingValue("SoftwareType").Split(',');
            if (user.IsAdmin.HasValue && user.IsAdmin.Value)
            {
                
                if (accessList[0] == "OnlyCompanyAdmin")
                {
                    string username = Helper.Instance.GetSettingValue("CompanyUser");
                    string password = Helper.Instance.GetSettingValue("CompanyPassword");
                    User user1 = Helper.Instance.GetUser(SiteSettings.UserID);
                    if (user1.UserName == username && user1.Password == password)
                        return true;
                    return false;
                }
                if(!string.IsNullOrEmpty(SoftType))
                {
                    foreach (var i in type)
                    {
                        if (SoftType.Contains(i))
                            return true;
                    }
                    //if (SoftType.Contains(type))
                    //    return true;
                    return false;
                }
                return true;
                //else if (accessList[0] == "OnlyOtubus")
                //{
                //    if (type == "0")
                //        return true;
                //    return false;
                //}
                //else if (accessList[0] == "OnlyminiBus")
                //{
                //    if (type == "1")
                //        return true;
                //    return false;
                //}
                //else if (accessList[0] == "OnlySevari")
                //{
                //    if (type == "2")
                //        return true;
                //    return false;
                //}
                //else
                //    return true;
            }
            if (user.Role == null)
                return false;

            for (int i = 0; i < accessList.Length; i++)
            {
                Access access = Helper.Instance.GetAccess(accessList[i]);
                if (access != null)
                    for (int j = 0; j < user.Role.RoleAccesses.Count; j++)
                        if (user.Role.RoleAccesses[j].AccessID == access.ID)
                        {
                            if (!string.IsNullOrEmpty(SoftType))
                            {
                                foreach (var k in type)
                                    if (SoftType.Contains(k))
                                        return true;
                                return false;
                            }
                            //if (!string.IsNullOrEmpty(SoftType) && SoftType.Contains(type))
                            //    return true;
                            //else if (!string.IsNullOrEmpty(SoftType) && !SoftType.Contains(type))
                            //    return false;
                            return true;
                        }
            }
        }
        catch (Exception ex)
        {
        }
        return false;
    }



    #region Users
    //public List<User> GetUsers()
    //{
    //    try
    //    {
    //        return Helper.Instance.DB.Users.ToList();
    //    }
    //    catch { }
    //    return null;
    //}

    //public User GetUser(int id)
    //{
    //    try
    //    {
    //        var u = from us in Helper.Instance.DB.Users
    //                where us.ID == id
    //                select us                        ;
    //        return u.First();
    //    }
    //    catch { }
    //    return null;
    //}
    //public User GetUser(string userName)
    //{
    //    try
    //    {
    //        var u = from us in Helper.Instance.DB.Users
    //                where us.UserName == userName
    //                select us;
    //        return u.First();
    //    }
    //    catch { }
    //    return null;
    //}


    #endregion

}
