﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DbHelper
/// </summary>
public class DbHelper
{
    /// <summary>
    /// Unique instance of the DbHelper.
    /// </summary>
    public static DbHelper Instance;
    /// <summary>
    /// ConnectionString used for connect to Database. It will be read from Configuration file
    /// </summary>
    private string mConnectionString;
    /// <summary>
    /// used with Transactions
    /// </summary>
    private System.Data.SqlClient.SqlTransaction mTransaction = null;
    /// <summary>
    /// used with Transactions
    /// </summary>
    private System.Data.SqlClient.SqlConnection mConnection = null;

    /// <summary>
    /// static constructor to initiate the Instance
    /// </summary>
    static DbHelper()
    {
        Instance = new DbHelper();
    }
    /// <summary>
    /// instance constructor
    /// </summary>
    private DbHelper()
    {
        mConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
    }
    /// <summary>
    /// Begins a new Transaction. Only one Transaction is allowed in a time.
    /// Throws an exception if there is another Transaction already
    /// </summary>
    public void BeginTransaction()
    {
        if (mTransaction != null)
            throw new Exception("Transaction is already started");
        mConnection = new System.Data.SqlClient.SqlConnection(mConnectionString);
        mConnection.Open();
        mTransaction = mConnection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
    }
    /// <summary>
    /// Commits an open Transaction if any
    /// </summary>
    public void CommitTransaction()
    {
        if (mTransaction == null)
            return;
        //throw new Exception("Transaction is already started");
        mTransaction.Commit();
        mTransaction = null;
        mConnection.Close();
        mConnection = null;
    }
    /// <summary>
    /// Rollbacks an open Transaction if any
    /// </summary>
    public void RollbackTransaction()
    {
        if (mTransaction != null)
        {
            mTransaction.Rollback();
            mTransaction = null;
        }
        if (mConnection != null)
        {
            mConnection.Close();
            mConnection = null;
        }
    }
    /// <summary>
    /// Get a single value from database
    /// </summary>
    /// <param name="commandText">a query string</param>
    /// <returns></returns>
    internal object ExecuteScalar(string commandText)
    {
        if (mTransaction != null)
            return SqlHelper.ExecuteScalar(mTransaction, CommandType.Text, commandText);
        else
            return SqlHelper.ExecuteScalar(mConnectionString, CommandType.Text, commandText);
    }
    internal object ExecuteScalar(string cnnStr, string commandText)
    {
        return SqlHelper.ExecuteScalar(cnnStr, CommandType.Text, commandText);
    }
    /// <summary>
    /// Get a single value from database
    /// </summary>
    /// <param name="spName">the Stored Procedure name</param>
    /// <param name="parameterValues">the Parameteres for the Stored Procedure</param>
    /// <returns></returns>
    internal object ExecuteScalar(string spName, params object[] parameterValues)
    {
        if (mTransaction != null)
            return SqlHelper.ExecuteScalar(mTransaction, spName, parameterValues);
        else
            return SqlHelper.ExecuteScalar(mConnectionString, spName, parameterValues);
    }
    /// <summary>
    /// Create an IDataReader to read from Database
    /// </summary>
    /// <param name="commandText">a query string</param>
    /// <returns></returns>
    public IDataReader ExecuteReader(string commandText)
    {
        try
        {
            if (mTransaction != null)
                return SqlHelper.ExecuteReader(mTransaction, CommandType.Text, commandText);
            else
                return SqlHelper.ExecuteReader(mConnectionString, CommandType.Text, commandText);
        }
        catch (Exception ex)
        {
            ex.Source = commandText;
            throw ex;
        }

    }
    public IDataReader ExecuteReader(string cntStr, string commandText)
    {
        try
        {
            return SqlHelper.ExecuteReader(cntStr, CommandType.Text, commandText);
        }
        catch (Exception ex)
        {
            ex.Source = commandText;
            throw ex;
        }
    }
    /// <summary>
    /// Create an IDataReader to read from Database
    /// </summary>
    /// <param name="spName">the Stored Procedure name</param>
    /// <param name="parameterValues">the Parameteres for the Stored Procedure</param>
    /// <returns></returns>
    internal IDataReader ExecuteReader(string spName, params object[] parameterValues)
    {
        if (mTransaction != null)
            return SqlHelper.ExecuteReader(mTransaction, spName, parameterValues);
        else
            return SqlHelper.ExecuteReader(mConnectionString, spName, parameterValues);
    }
    /// <summary>
    /// Execute a none query command like INSERT and UPDATE
    /// </summary>
    /// <param name="commandText">the nonquery command</param>
    /// <returns>num of affected records</returns>
    public int ExecuteNonQuery(string commandText)
    {
        if (mTransaction != null)
            return SqlHelper.ExecuteNonQuery(mTransaction, CommandType.Text, commandText);
        else
            return SqlHelper.ExecuteNonQuery(mConnectionString, CommandType.Text, commandText);
    }
    public int ExecuteNonQuery(string cntStr, string commandText)
    {
        return SqlHelper.ExecuteNonQuery(cntStr, CommandType.Text, commandText);
    }
    /// <summary>
    /// Execute a none query command like INSERT and UPDATE
    /// </summary>
    /// <param name="spName">the Stored Procedure name</param>
    /// <param name="parameterValues">the Parameteres for the Stored Procedure</param>
    /// <returns>num of affected records</returns>
    internal int ExecuteNonQuery(string spName, params object[] parameterValues)
    {
        if (mTransaction != null)
            return SqlHelper.ExecuteNonQuery(mTransaction, spName, parameterValues);
        else
            return SqlHelper.ExecuteNonQuery(mConnectionString, spName, parameterValues);
    }

    #region IDisposable Members
    /// <summary>
    /// Dispose all internal obects and rollback the transaction if any
    /// </summary>
    public void Dispose()
    {
        RollbackTransaction();
    }

    #endregion
    /// <summary>
    /// Add a condition to existing WHERE clause and connect them with AND
    /// </summary>
    /// <param name="where">existing WHERE caluse without the word WHERE</param>
    /// <param name="condition">condition to Add</param>
    /// <returns>new WHERE caluse</returns>
    internal static string AddWhere(string where, string condition)
    {
        if (Null.IsNull(where))
            return condition;
        return where.Length == 0 ? condition : string.Format(" {0} AND {1} ", where, condition);
    }

    public DataTable FillDataTable(string commandText, string tableName)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(tableName);
        string[] tableNames = new string[] { tableName };
        if (mTransaction != null)
            SqlHelper.FillDataset(mTransaction, CommandType.Text, commandText, ds, tableNames);
        else
            SqlHelper.FillDataset(mConnectionString, CommandType.Text, commandText, ds, tableNames);
        return ds.Tables[0];
    }
    public DataTable FillDataTable(string cntStr, string commandText, string tableName)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(tableName);
        string[] tableNames = new string[] { tableName };
        SqlHelper.FillDataset(cntStr, CommandType.Text, commandText, ds, tableNames);
        return ds.Tables[0];
    }
    public DataRow FillDataRow(string cntStr, string commandText)
    {
        DataTable dt = FillDataTable(cntStr, commandText, "dt");
        if (dt == null)
            return null;
        if (dt.Rows.Count == 0)
        {
            dt.Dispose();
            return null;
        }
        return dt.Rows[0];
    }

    public static string QtdDateTime(DateTime dt)
    {
        if (Null.IsNull(dt))
            return "NULL";
        return QtdStr(string.Format("{0}-{1}-{2} {3}:{4}:{5}"
            , dt.Year.ToString().PadLeft(4, '0'), dt.Month.ToString().PadLeft(2, '0'), dt.Day.ToString().PadLeft(2, '0')
            , dt.Hour.ToString().PadLeft(2, '0'), dt.Minute.ToString().PadLeft(2, '0'), dt.Second.ToString().PadLeft(2, '0')), false);
    }
    public static string QtdDate(DateTime dt)
    {
        if (Null.IsNull(dt))
            return "NULL";
        return QtdStr(string.Format("{0}-{1}-{2}"
            , dt.Year.ToString().PadLeft(4, '0'), dt.Month.ToString().PadLeft(2, '0'), dt.Day.ToString().PadLeft(2, '0')), false);
    }
    public static string QtdStr(string str)
    {
        return QtdStr(str, false);
    }
    public static string QtdStr(string str, bool nullable)
    {
        if (string.IsNullOrEmpty(str))
            return nullable ? "NULL" : "''";
        return string.Format("N'{0}'", str.Replace("'", "''"));
    }
    public static string Likenize(string str)
    {
        if (string.IsNullOrEmpty(str))
            return "'%'";
        return string.Format("N'%{0}%'", str.Replace("'", "''"));
    }
    public static bool TestConnection(string ConnectionString)
    {
        var canConnect = false;

        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            using (connection)
            {
                connection.Open();

                canConnect = true;
            }
        }
        catch (Exception exception)
        {

        }
        finally
        {
            connection.Close();
        }

        return canConnect;
    }
}
