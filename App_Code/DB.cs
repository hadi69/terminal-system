﻿using System;
partial class Path
{
    public string Title
    {
        get
        {
            if (SrcCityID <= 0)
                return "---";
            if (City != null && City1 != null)
                return string.Format("{0} -> {1}", City.Title, City1.Title);

            return string.Format("{0} -> {1}", Helper.Instance.GetCityTitle(SrcCityID), Helper.Instance.GetCityTitle(DestCityID));
        }
    }
}
partial class Rate
{
    public string StartDate2
    {
        get
        {
            return Tool.ToPersianDate(StartDate, "");
        }
    }
    public string EndDate2
    {
        get
        {
            return Tool.ToPersianDate(EndDate, "");
        }
    }
}

partial class Driver
{
    public string DrivingDate2
    {
        get
        {
            return Tool.ToPersianDate(DrivingDate, "");
        }
    }
    public string HourDate2
    {
        get
        {
            return Tool.ToPersianDate(HourDate, "");
        }
    }
    public string Title
    {
        get
        {
            return string.Format("{0} {1}", Name, Surname);
        }
    }
}

partial class Owner
{
    public string FullName
    {
        get
        {
            return string.Format("{0} {1}", Name, Surname);
        }
    }
}

partial class Car
{
    public string ContractDate2
    {
        get
        {
            return Tool.ToPersianDate(ContractDate, "");
        }
    }
    public string VisitDate2
    {
        get
        {
            return Tool.ToPersianDate(VisitDate, "");
        }
    }
    public string Title
    {
        get
        {
            if (CarType == null)
                return "";
            return string.Format("{0} {1}", CarType.Title, Plate);
        }
    }
}

partial class Service
{
    public bool Today
    {
        get
        {
            return IsInDate(DateTime.Today);
        }
    }
    public bool IsInDate(DateTime date)
    {
        if (this.Date > date)
            return false;
        // it can be used only on its day
        if (ServiceType == (int)ServiceTypes.Normal)
            return this.Date == date;

        if (ServiceType == (int)ServiceTypes.Fixed)
        {
            if (string.IsNullOrEmpty(this.Days))
                return false;
            DayOfWeek oo = new System.Globalization.GregorianCalendar().GetDayOfWeek(date);
            int day = 0;
            #region Find day
            switch (oo)
            {
                case DayOfWeek.Saturday:
                    day = 0;
                    break;
                case DayOfWeek.Sunday:
                    day = 1;
                    break;
                case DayOfWeek.Monday:
                    day = 2;
                    break;
                case DayOfWeek.Tuesday:
                    day = 3;
                    break;
                case DayOfWeek.Wednesday:
                    day = 4;
                    break;
                case DayOfWeek.Thursday:
                    day = 5;
                    break;
                case DayOfWeek.Friday:
                    day = 6;
                    break;
                default:
                    break;
            }
            #endregion
            if (this.Days.Contains(string.Format(";{0};", day)))
                return true;
            return false;
        }

        if (ServiceType == (int)ServiceTypes.AnotherDay)
        {
            TimeSpan ts = date - this.Date;
            return ((int)(ts.TotalDays)) % 2 == 0;
        }

        return true;
    }

    public string ServiceType2
    {
        get
        {
            return Tool.ToString((ServiceTypes)ServiceType);
        }
    }
    public string Days2
    {
        get
        {
            if (string.IsNullOrEmpty(Days))
                return "";
            string res = Days;
            for (int i = 0; i < 7; i++)
                res = res.Replace(i.ToString(), Tool.DayToString(i));

            return res;
        }
    }

    public string Title
    {
        get
        {
            if (Path == null)
                return "";
            return string.Format("{0} ({1})", Path.Title, Tool.ToString((ServiceTypes)ServiceType));
        }
    }
}

partial class Ticket
{
    public int GetNumTickets()
    {
        if (NumChairs.HasValue)
            return NumChairs.Value;
        if (string.IsNullOrEmpty(Chairs))
            return 0;
        string ch = Chairs;
        if (ch.StartsWith(";"))
            ch = ch.Substring(1);
        if (ch.EndsWith(";"))
            ch = ch.Substring(0, ch.Length - 1);
        return ch.Split(';').Length;
    }
    public string NoPlusName
    {
        get
        {
            return string.Concat(No, " ", this.Fullname);
        }
    }
}
partial class Trip
{
    public bool IsMini
    {
        get
        {
            try
            {
                return this.CarType.Layout.NumChairs.Value <
                       Tool.GetInt(Helper.Instance.GetSettingValue("MiniChairs"),24) + 1;
            }
            catch
            {
                return false;
            }
        }
    }
    public bool IsSewari
    {
        get
        {
            try
            {
                return this.CarType.Layout.NumChairs.Value < 6;
            }
            catch
            {
                return false;
            }
        }
    }
    public bool IsBusByCarType
    {
        get
        {
            try
            {
                return this.CarType.Code.HasValue && this.CarType.Code.Value != 7 && this.CarType.Code.Value != 8
                    && this.CarType.Code.Value != 0;
            }
            catch
            {
                return false;
            }
        }
    }
    public string DepartureTime2
    {
        get
        {
            return string.IsNullOrEmpty(this.DepartureTime) ? this.Service.DepartureTime : this.DepartureTime;
        }
    }
    public int DepartureTime2int
    {
        get
        {
            string dt = string.IsNullOrEmpty(this.DepartureTime) ? this.Service.DepartureTime : this.DepartureTime;
            try
            {
                return int.Parse(this.DepartureTime2.Split(':')[0]) * 60 + int.Parse(this.DepartureTime2.Split(':')[1]);
            }
            catch
            {
                return 0;
            }
        }
    }
}
partial class User
{
    public bool IsCompanyAdmin
    {
        get
        {
            string cUsername = Helper.Instance.GetSettingValue("CompanyUser");
            string cPassword = Helper.Instance.GetSettingValue("CompanyPassword");
            if (UserName == cUsername && Password == cPassword)
                return true;
            return false;
        }
    }
}

partial class CarsCompany
{
    public string ContractDate2
    {
        get
        {
            return Tool.ToPersianDate(ContractDate, "");
        }
    }
    public string VisitDate2
    {
        get
        {
            return Tool.ToPersianDate(VisitDate, "");
        }
    }
}