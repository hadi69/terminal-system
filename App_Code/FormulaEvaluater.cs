﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FormulaEvaluater
/// </summary>
public class FormulaEvaluater
{
    public static int tripID { get; set; }

    public static decimal Evaluate(int tripid, string formula)
    {
        tripID = tripid;
        string inner = FindInnerFormula(formula);
        if (formula.IndexOf(',') > 0)
            return Convert.ToDecimal(EvaluateInner(inner));
        while (inner != null)
        {
            // inner = inner.Replace("-", "+#");
            formula = formula.Replace(string.Concat("(", inner, ")"), EvaluateInner(inner).ToString());
            inner = FindInnerFormula(formula);
        }
        return Convert.ToDecimal(EvaluateInner(formula));
    }

    private static string FindInnerFormula(string formula)
    {
        int posL, posR = formula.IndexOf(')');
        if (posR > 0)
        {
            posL = formula.LastIndexOf('(', posR);
            return formula.Substring(posL + 1, posR - posL - 1);
        }
        return null;
    }

    private static decimal EvaluateInner(string inner)
    {
        if (inner.IndexOf('+') < 0)
            return EvaluateMinus(inner);
        string[] pluses = inner.Split(new char[] {'+'});
        decimal res = 0;
        for (int i = 0; i < pluses.Length; i++)
            res += EvaluateMinus(pluses[i]);
        return res;
    }

    private static bool EqualsToAnyOperator(char ch)
    {
        if (ch == '+')
            return true;
        if (ch == '*')
            return true;
        if (ch == '/')
            return true;
        if (ch == '~')
            return true;
        if (ch == '^')
            return true;
        return false;
    }

    private static decimal EvaluateMinus(string plus)
    {
        if (plus.IndexOf('-') < 0)
            return EvaluateMul(plus);
        if (plus.StartsWith("-"))
            plus = string.Concat("0", plus);
        plus = plus.Replace("--", "-0-");
        string[] minuses = plus.Split(new char[] {'-'});
        List<string> modified = new List<string>();
        modified.Add(minuses[0]);
        string last = modified[modified.Count - 1];
        for (int i = 1; i < minuses.Length; i++)
        {
            if (EqualsToAnyOperator(last[last.Length - 1]))
                modified[modified.Count - 1] = string.Format("{0}-{1}", last, minuses[i]);
            else
                modified.Add(minuses[i]);
            last = modified[modified.Count - 1];
        }
        if (minuses.Length != modified.Count)
        {
            minuses = new string[modified.Count];
            modified.CopyTo(minuses);
        }
        decimal res = EvaluateMul(minuses[0]);
        for (int i = 1; i < minuses.Length; i++)
            res -= EvaluateMul(minuses[i]);
        return res;
    }

    private static decimal EvaluateMul(string minus)
    {
        if (minus.IndexOf('*') < 0)
            return EvaluateDiv(minus);
        string[] muls = minus.Split(new char[] {'*'});
        decimal res = 1;
        for (int i = 0; i < muls.Length; i++)
            res *= EvaluateDiv(muls[i]);
        return res;
    }

    private static decimal EvaluateDiv(string mul)
    {
        if (mul.IndexOf('/') < 0)
            return EvaluateMax(mul);
        string[] divs = mul.Split(new char[] {'/'});
        decimal res = EvaluateMax(divs[0]);
        for (int i = 1; i < divs.Length; i++)
            res /= EvaluateMax(divs[i]);
        return res;
    }

    private static decimal EvaluateMax(string div)
    {
        if (div.IndexOf('^') < 0)
            return EvaluateValue(div);
        string[] maxs = div.Split(new char[] {'^'});
        decimal res = EvaluateValue(maxs[0]);
        for (int i = 1; i < maxs.Length; i++)
            res = Math.Max(res, EvaluateValue(maxs[i]));
        return res;
    }

    private static decimal EvaluateValue(string val)
    {
        decimal res = 0;
        if (decimal.TryParse(val, out res))
            return res;
        else
        {
            res = GetFieldValue(tripID, val);
        }
        return res;

    }

    private static decimal GetFieldValue(int id, string fieldName)
    {
        try
        {
            Trip mTrip = Helper.Instance.GetTrip(id);
            RateItem rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, mTrip.Service.Path.SrcCityID,
                mTrip.Service.Path.DestCityID, mTrip.Service.SpecialID, mTrip.Date);
            decimal numTickets = Helper.Instance.GetNumTickets(mTrip.ID);
            if (fieldName.ToLower() == "TotalPrice".ToLower())
            {
                return Helper.Instance.GetPrice(mTrip.ID) - numTickets*(rateItem == null ? 0 : rateItem.PureCost);
            }
            if (fieldName.ToLower() == "BodyInsurance".ToLower())
            {
                return mTrip.BodyInsurance;
            }
            if (fieldName.ToLower() == "Insurance".ToLower())
            {
                return mTrip.Insurance;
            }
            if (fieldName.ToLower() == "ExtraCost".ToLower())
            {
                return mTrip.ExtraCost;
            }
            if (fieldName.ToLower() == "NumTickets".ToLower())
            {
                return numTickets;
            }
            if (fieldName.ToLower() == "TotalReception".ToLower())
            {
                return mTrip.TotalReception;
            }
            if (fieldName.ToLower() == "Stamp".ToLower())
            {
                return mTrip.Stamp;
            }
            if (fieldName.ToLower() == "TollPercent".ToLower())
            {
                return mTrip.Toll;
            }
            if (fieldName.ToLower() == "ComPercent".ToLower())
            {
                return mTrip.Comission;
            }
            if (fieldName.ToLower()!="")
            {
                string formula = Helper.Instance.GetFieldFormul(fieldName);
                return Evaluate(id, formula);
            }
        }
        catch (Exception)
        {

            throw;
        }
        return 0;
    }
}