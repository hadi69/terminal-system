﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Opens.aspx.cs" Inherits="Opens" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع
                    </td>
                    <td colspan="3">
                        <asp:CheckBox ID="mBus" runat="server" CssClass="T" Text='اتوبوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mMini" runat="server" CssClass="T" Text='مینی بوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mSewari" runat="server" CssClass="T" Text='سواری' Checked="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        سرويس
                    </td>
                    <td>
                        <asp:DropDownList ID="sServiceID" runat="server" CssClass="DD" Width="250px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        راننده اول
                    </td>
                    <td>
                        <asp:DropDownList ID="sDriverID1" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="6">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                             <asp:Button ID="deleteAll" runat="server" Text="حذف همه" OnClick="deleteAll_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="Date" HeaderText="ساعت" />
                    <asp:BoundField DataField="DriverID1" HeaderText="نام راننده" Visible="true" />
                    <asp:BoundField DataField="ServiceID" HeaderText="مقصد" />
                    <asp:BoundField DataField="ID" HeaderText="روز" />
                    <asp:BoundField DataField="ID" HeaderText="نوع اتوکار" />
                    <asp:BoundField DataField="ID" HeaderText="ت بليط" />
                    <asp:BoundField DataField="ID" HeaderText="ت رزرو" />
                    <asp:HyperLinkField HeaderText=" فروش " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Sale.aspx?tripid={0}"
                        Text="فروش" Visible="false" />
                    <asp:HyperLinkField HeaderText=" مسافرين " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Passengers.aspx?tripid={0}"
                        Text="مسافرين" />
                    <asp:HyperLinkField HeaderText=" بليطها " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Tickets.aspx?tripid={0}"
                        Text="بليطها" />
                    <asp:HyperLinkField HeaderText=" صورت وضعيت " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Close.aspx?tripid={0}"
                        Text="صورت وضعيت" Visible="false" />
                    <asp:HyperLinkField HeaderText=" راهنما " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="PrintHelp.aspx?tripid={0}"
                        Text="راهنما" Target="_blank" />
                    <asp:CheckBoxField DataField="Locked" HeaderText="قفل" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
