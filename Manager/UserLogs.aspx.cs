﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_UserLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInitDate();
            BindData();
        }
    }

    void BindInitDate()
    {
        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;
    }

    void BindData()
    {
        list.DataSource = GetUserLogs();
        list.DataBind();
    }

    List<UserLog> GetUserLogs()
    {
        var all = from c in Helper.Instance.DB.UserLogs
                  select c;

        DateTime fromDate = sDateStart.SelectedDate.AddHours(-12);
        if (fromDate != new DateTime())
            all = all.Where(c => c.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate.AddHours(11.99);
        if (toDate != new DateTime())
            all = all.Where(c => c.Date <= toDate);
        all = all.OrderBy(c => c.Date);
        return all.ToList();
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            UserLog info = e.Row.DataItem as UserLog;


            if (info != null)
            {
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.User.FullName;
                e.Row.Cells[5].Text = Tool.ToPersianDate(info.Date, "");
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}