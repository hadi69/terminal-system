﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserLogs.aspx.cs" Inherits="Manager_UserLogs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>جستجو
                    </td>
                    <td>از تاريخ
                    </td>
                    <td>
                        <uc1:datepicker id="sDateStart" runat="server" width="250px" />
                    </td>
                    <td>تا تاريخ
                    </td>
                    <td>
                        <uc1:datepicker id="sDateEnd" runat="server" width="250px" />
                    </td>
                    <td>
                        <!--
                        سرويس-->
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" Text="جستجو"
                            Width="50px" OnClick="doSearch_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%" PageSize="1000" EnableModelValidation="True">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="UserID" HeaderText="کاربر" />
                    <asp:BoundField DataField="ChangedTableName" HeaderText="عنوان جدول" />
                    <asp:BoundField DataField="ChangedRecordID" HeaderText="رکورد" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="Comments" HeaderText="شرح">
                    <ItemStyle Width="600px" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span>
                </EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <br />
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

