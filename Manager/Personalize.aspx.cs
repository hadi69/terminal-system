﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Manager_Personalize : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        //mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Fund> all = Helper.Instance.GetFunds();
            mFundID.DataValueField = "ID";
            mFundID.DataTextField = "Title";
            mFundID.DataSource = all;
            mFundID.DataBind();
        }
        {
            List<Role> all = Helper.Instance.GetRoles();
            mRole.DataValueField = "ID";
            mRole.DataTextField = "Name";
            mRole.DataSource = all;
            mRole.DataBind();
        }
    }
    private void BindData()
    {
        User info = SiteSettings.User;
        mFundID.Enabled = false;
        if (info != null)
        {
            mUserName.Text = info.UserName;
            mFullName.Text = info.FullName;
            Tool.SetSelected(mFundID, info.FundID);
            Tool.SetSelected(mRole, info.RoleID);
            mFundID.Enabled = info.IsAdmin.HasValue && info.IsAdmin.Value;
        }

    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        User info = SiteSettings.User;
        if (null == info)
            return;
        info = Helper.Instance.GetUser(info.ID);
        info.FullName = mFullName.Text;
        info.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        // info.Role = Tool.GetInt(mRole.SelectedValue, Null.NullInteger);

        if (Helper.Instance.Update())
        {
            BindData();
            SiteSettings.UserID = info.ID;
            mMsg.Text = "اطلاعات با موفقيت بروزرساني شد";
        }
        else
            mMsg.Text = "خطا در بروزرساني اطلاعات";
    }

    protected void doChangePass_Click(object sender, EventArgs e)
    {
        User info = SiteSettings.User;
        if (null == info)
            return;
        info = Helper.Instance.GetUser(info.ID);
        if (mPassword.Text.Trim() != info.Password)
        {
            mMsg.Text = "رمز عبور فعلي اشتباه است.";
            return;
        }
        if (mPassword1.Text.Trim().Length > 0)
            info.Password = mPassword1.Text;

        if (Helper.Instance.Update())
        {
            SiteSettings.UserID = info.ID;
            BindData();
            mMsg.Text = "رمز عبور با موفقيت بروزرساني شد.";
        }
        else
            mMsg.Text = "خطا در بروزرساني رمز عبور.";
    }
}
