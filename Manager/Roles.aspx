﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Roles.aspx.cs" Inherits="Manager_Roles" Title="رل کاربری" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <asp:Button ID="doAddUser" runat="server" Text="رل INS" CssClass="CB" OnClick="doAddUser_Click">
            </asp:Button>
            <asp:GridView ID="list" runat="server" OnRowDeleting="list_RowDeleting" OnRowDataBound="list_RowDataBound"
                OnSelectedIndexChanged="list_SelectedIndexChanged" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Name" HeaderText="نام" />
                    <asp:BoundField DataField="Comments" HeaderText="شرح" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display:none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن رل جديد</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نام رل
                            </td>
                            <td>
                                <asp:TextBox ID="mName" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="auG" ErrorMessage="نام الزامي است"
                                    ControlToValidate="mName" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شرح
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="auG" ErrorMessage="شرح الزامي است"
                                    ControlToValidate="mComments" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                دسترسیها
                            </td>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            دسترسیهای انتخاب شده
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            دسترسیها
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox runat="server" ID="mAccess" SelectionMode="Multiple" />
                                        </td>
                                        <td>
                                            <asp:Button ID="doAddAll" Text='همه اضافه' runat="server" CssClass="CB" Width="70"
                                                OnClick="doAddAll_Click" ValidationGroup="auG" /><br />
                                            <asp:Button ID="doAdd" Text='اضافه' runat="server" CssClass="CB" Width="70" OnClick="doAdd_Click"
                                                ValidationGroup="auG" /><br />
                                            <asp:Button ID="doDelete" Text='حذف' runat="server" CssClass="CB" Width="70" OnClick="doDelete_Click"
                                                ValidationGroup="auG" /><br />
                                            <asp:Button ID="doDeleteAll" Text='حذف همه' runat="server" CssClass="CB" Width="70"
                                                OnClick="doDeleteAll_Click" ValidationGroup="auG" /><br />
                                        </td>
                                        <td>
                                            <asp:ListBox runat="server" ID="mAllAccess" SelectionMode="Multiple" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" OnClick="puOk_Click" runat="server" Text="تاييد F7" CssClass="CB"
                                    ValidationGroup="auG"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB" CausesValidation="False">
                                </asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;
</asp:Content>
