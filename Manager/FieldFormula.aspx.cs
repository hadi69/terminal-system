﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_fieldformula : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        var all = from c in Helper.Instance.DB.FieldFormulas
                  select c;
        list.DataSource = all;
        list.DataBind();
    }

    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        FieldFormula info;
        if (Null.NullInteger == EditID)
        {
            
            info = new FieldFormula();
            info.Name =mName.Text;
            info.Title = mTitle.Text;
        }
        else
        {
            info = Helper.Instance.GetFieldFormula(EditID);
        }
        if (info == null)
            return;

        info.Formula = mFormula.Text;

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.FieldFormulas.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }

    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            FieldFormula info = e.Row.DataItem as FieldFormula;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        FieldFormula info = Helper.Instance.GetFieldFormula(EditID);
        if (info == null)
            return;


        mName.Text = info.Name;
        mTitle.Text = info.Title;
        mFormula.Text = info.Formula;

        puEx.Show();
    }
}