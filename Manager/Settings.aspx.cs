﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Manager_Settings : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        List<City> all = Helper.Instance.GetCities(true);
        City none = new City();
        none.ID = Null.NullInteger;
        none.Title = "";
        all.Insert(0, none);

        mDefaultSrcCity.DataValueField = "Title";
        mDefaultSrcCity.DataTextField = "Title";
        mDefaultSrcCity.DataSource = all;
        mDefaultSrcCity.DataBind();
        //visible controls
        string softwareType = Helper.Instance.GetSettingValue("SoftwareType");
        if(softwareType.Contains("0"))
        {
            bus1.Visible = bus2.Visible = bus3.Visible = minbus.Visible = bus4.Visible = true;
        }
        if (softwareType.Contains("1"))
        {
            miniSaw1.Visible = minisaw2.Visible = minisaw3.Visible = minbus.Visible = true;
        }
        if(softwareType.Contains("2"))
        {
            miniSaw1.Visible=minisaw2.Visible = minisaw3.Visible=true;
        }

    }
    private void BindData()
    {
        City p = Helper.Instance.GetCity(Helper.Instance.GetSettingValue("DefaultSrcCity"));
        if (p != null)
            Tool.SetSelected(mDefaultSrcCity, p.Title);
        mCommandTimeOut.Text = Helper.Instance.GetSettingValue("CommandTimeOut", 120).ToString();
        mPaidBackPercentOpen.Text = Helper.Instance.GetSettingValue("PaidBackPercentOpen");
        mPaidBackPercentClosed.Text = Helper.Instance.GetSettingValue("PaidBackPercentClosed");

        //mUserID.Text = Helper.Instance.GetSettingValue("UserID");
       // mCompanyCode.Text = Helper.Instance.GetSettingValue("CompanyCode");
       // mCompanyName.Text = Helper.Instance.GetSettingValue("CompanyName");
        mUrl.Text = Helper.Instance.GetSettingValue("Url");
        //mPassword.Text = Helper.Instance.GetSettingValue("Password");
        mCoNo.Text = Helper.Instance.GetSettingValue("CoNo");

        mChangeCloseValues.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("ChangeCloseValues"), true);

        mAllowSaveSeries.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AllowSaveSeries"), true);
        mSeriesStart.Text = Helper.Instance.GetSettingValue("SeriesStart");
        mSeriesStartFrom.Text = Helper.Instance.GetSettingValue("SeriesStartFrom");
        mSeriesStartTo.Text = Helper.Instance.GetSettingValue("SeriesStartTo");
        mSeriesNo1.Text = Helper.Instance.GetSettingValue("SeriesNo1");
        mSeriesNo2.Text = Helper.Instance.GetSettingValue("SeriesNo2");
        mSeriesNo3.Text = Helper.Instance.GetSettingValue("SeriesNo3");
        mSeriesNo4.Text = Helper.Instance.GetSettingValue("SeriesNo4");

        mSeriesStartMini.Text = Helper.Instance.GetSettingValue("SeriesStartMini");
        mSeriesStartMiniFrom.Text = Helper.Instance.GetSettingValue("SeriesStartMiniFrom");
        mSeriesStartMiniTo.Text = Helper.Instance.GetSettingValue("SeriesStartMiniTo");
        mSeriesNoMini1.Text = Helper.Instance.GetSettingValue("SeriesNoMini1");
        mSeriesNoMini2.Text = Helper.Instance.GetSettingValue("SeriesNoMini2");
        mSeriesNoMini3.Text = Helper.Instance.GetSettingValue("SeriesNoMini3");
        mAutoDeleteMini.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AutoDeleteMini"), false);
        mMiniResetNum.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("MiniResetNum"), false);

        mSeriesStartD.Text = Helper.Instance.GetSettingValue("SeriesStartD");
        mSeriesStartDFrom.Text = Helper.Instance.GetSettingValue("SeriesStartDFrom");
        mSeriesStartDTo.Text = Helper.Instance.GetSettingValue("SeriesStartDTo");
        mSeriesNoD1.Text = Helper.Instance.GetSettingValue("SeriesNoD1");
        mSeriesNoD2.Text = Helper.Instance.GetSettingValue("SeriesNoD2");
        mSeriesNoD3.Text = Helper.Instance.GetSettingValue("SeriesNoD3");

        mTicket.Text = Helper.Instance.GetSettingValue("Ticket");
        mStamp.Text = Helper.Instance.GetSettingValue("Stamp");
        mToll.Text = Helper.Instance.GetSettingValue("Toll");
        mCommission.Text = Helper.Instance.GetSettingValue("Commission");
        mReceptionCost.Text = Helper.Instance.GetSettingValue("ReceptionCost");
        mExtraCost.Text = Helper.Instance.GetSettingValue("ExtraCost");
        mAgencyPrice.Text = Helper.Instance.GetSettingValue("AgencyPrice");
        mOthers.Text = Helper.Instance.GetSettingValue("Others");
        mMinForReception.Text = Helper.Instance.GetSettingValue("MinForReception");

        mCanDeleteTrip.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false);
        mCanEditCities.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("CanEditCities"), false);
        mAllowReserve.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserve"), true);
        mAllowReserveClose.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserveClose"), true);
        mAllowReserveInClose.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserveInClose"), true);
        mSortDNT.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("SortDNT"), true);
        mMiniAsBus.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("MiniAsBus"), false);
        mAutoFill.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AutoFill"), false);
        mAutoFillNames.Text = Helper.Instance.GetSettingValue("AutoFillNames");
        mOnlineSale.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("OnlineSale"), false);

        mDateSeparators.Text = Helper.Instance.GetSettingValue("DateSeparators");
        if (mDateSeparators.Text.Trim().Length == 0)
            mDateSeparators.Text = @"/ - \ , .";

        mCanDefineFixed.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("CanDefineFixed"), false);
        mAutoOpen.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false);
        
        mSMSMessage.Text = Helper.Instance.GetSettingValue("SMSMessage");
        mSMSMessageSale.Text = Helper.Instance.GetSettingValue("SMSMessageSale");
        mMobile.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("Mobile"), false);
        mMiniChairs.Text = Helper.Instance.GetSettingValue("MiniChairs");
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        string SoftwareType = Helper.Instance.GetSettingValue("SoftwareType");
        string[] SoftwareTypeItem = SoftwareType.Split(',');
        int busStartSeries=Tool.GetInt(mSeriesStart.Text,-2);
        if (busStartSeries <= Tool.GetInt(mSeriesStartFrom.Text,0) || busStartSeries >=Tool.GetInt(mSeriesStartTo.Text,0) && SoftwareTypeItem[0]=="0")
        {
            mError.Text = "شماره شروع سری صورت اتوبوس باید بین بازه باشد ";
            return;
        }
        int busStartSeriesD = Tool.GetInt(mSeriesStartD.Text, -2);
        if (busStartSeriesD <= Tool.GetInt(mSeriesStartDFrom.Text, 0) || busStartSeriesD >= Tool.GetInt(mSeriesStartDTo.Text, 0))
        {
            mError.Text = "شماره شروع سری صورت دربستی باید بین بازه باشد ";
            return;
        }
        if (SoftwareTypeItem[0] != "0" || SoftwareTypeItem.Length > 1)
        {
            int busStartSeriesM = Tool.GetInt(mSeriesStartMini.Text, -2);
            if (busStartSeriesM > Tool.GetInt(mSeriesStartMiniTo.Text, 0) || busStartSeriesM <= Tool.GetInt(mSeriesStartMiniFrom.Text, 0))
            {
                mError.Text = "شماره شروع سری صورت مینی بوس و سواری باید بین بازه باشد ";
                return;
            }
        }
        bool ok = Helper.Instance.UpdateSetting("PaidBackPercentOpen", mPaidBackPercentOpen.Text);
        ok = Helper.Instance.UpdateSetting("CommandTimeOut", mCommandTimeOut.Text);
        ok = Helper.Instance.UpdateSetting("PaidBackPercentClosed", mPaidBackPercentClosed.Text);
        ok = Helper.Instance.UpdateSetting("DefaultSrcCity", mDefaultSrcCity.SelectedValue);

        //ok = Helper.Instance.UpdateSetting("UserID", mUserID.Text);
        //ok = Helper.Instance.UpdateSetting("CompanyCode", mCompanyCode.Text);
        //ok = Helper.Instance.UpdateSetting("CompanyName", mCompanyName.Text);
        ok = Helper.Instance.UpdateSetting("Url", mUrl.Text);
        //ok = Helper.Instance.UpdateSetting("Password", mPassword.Text);
        ok = Helper.Instance.UpdateSetting("CoNo", mCoNo.Text);

        ok = Helper.Instance.UpdateSetting("ChangeCloseValues", mChangeCloseValues.Checked.ToString());

        ok = Helper.Instance.UpdateSetting("AllowSaveSeries", mAllowSaveSeries.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("SeriesStart", mSeriesStart.Text);
        ok = Helper.Instance.UpdateSetting("SeriesStartFrom", mSeriesStartFrom.Text);
        ok = Helper.Instance.UpdateSetting("SeriesStartTo", mSeriesStartTo.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNo1", mSeriesNo1.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNo2", mSeriesNo2.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNo3", mSeriesNo3.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNo4", mSeriesNo4.Text);

        ok = Helper.Instance.UpdateSetting("SeriesStartMini", mSeriesStartMini.Text);
        ok = Helper.Instance.UpdateSetting("SeriesStartMiniFrom", mSeriesStartMiniFrom.Text);
        ok = Helper.Instance.UpdateSetting("SeriesStartMiniTo", mSeriesStartMiniTo.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNoMini1", mSeriesNoMini1.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNoMini2", mSeriesNoMini2.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNoMini3", mSeriesNoMini3.Text);
        ok = Helper.Instance.UpdateSetting("AutoDeleteMini", mAutoDeleteMini.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("MiniResetNum", mMiniResetNum.Checked.ToString());

        ok = Helper.Instance.UpdateSetting("SeriesStartD", mSeriesStartD.Text);
        ok = Helper.Instance.UpdateSetting("SeriesStartDFrom", mSeriesStartDFrom.Text);
        ok = Helper.Instance.UpdateSetting("SeriesStartDTo", mSeriesStartDTo.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNoD1", mSeriesNoD1.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNoD2", mSeriesNoD2.Text);
        ok = Helper.Instance.UpdateSetting("SeriesNoD3", mSeriesNoD3.Text);

        ok = Helper.Instance.UpdateSetting("Ticket", mTicket.Text);
        ok = Helper.Instance.UpdateSetting("Stamp", mStamp.Text);
        ok = Helper.Instance.UpdateSetting("Toll", mToll.Text);
        ok = Helper.Instance.UpdateSetting("Commission", mCommission.Text);
        ok = Helper.Instance.UpdateSetting("ReceptionCost", mReceptionCost.Text);
        ok = Helper.Instance.UpdateSetting("ExtraCost", mExtraCost.Text);
        ok = Helper.Instance.UpdateSetting("AgencyPrice", mAgencyPrice.Text);
        ok = Helper.Instance.UpdateSetting("Others", mOthers.Text);
        ok = Helper.Instance.UpdateSetting("MinForReception", mMinForReception.Text);

        ok = Helper.Instance.UpdateSetting("CanDeleteTrip", mCanDeleteTrip.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("CanEditCities", mCanEditCities.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("AllowReserve", mAllowReserve.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("AllowReserveClose", mAllowReserveClose.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("AllowReserveInClose", mAllowReserveInClose.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("SortDNT", mSortDNT.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("MiniAsBus", mMiniAsBus.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("AutoFill", mAutoFill.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("AutoFillNames", mAutoFillNames.Text);
        ok = Helper.Instance.UpdateSetting("OnlineSale", mOnlineSale.Checked.ToString());
        

        ok = Helper.Instance.UpdateSetting("DateSeparators", mDateSeparators.Text);
        Tool.mSeps = mDateSeparators.Text;
        ok = Helper.Instance.UpdateSetting("CanDefineFixed", mCanDefineFixed.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("AutoOpen", mAutoOpen.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("SMSMessage", mSMSMessage.Text);
        ok = Helper.Instance.UpdateSetting("SMSMessageSale", mSMSMessageSale.Text);
        ok = Helper.Instance.UpdateSetting("Mobile", mMobile.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("MiniChairs", mMiniChairs.Text);

        if (ok)
        {
            BindData();
            mError.Text = "تنظیمات با موفقیت ذخیره شد.";
        }
        else
            mError.Text = "تنظیمات ذخیره نشد<br />" + Helper.Instance.LastException.Message;
    }
   

}
