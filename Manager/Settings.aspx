﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Settings.aspx.cs" Inherits="Manager_Settings" Title="تنظیمات" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tbody>
            <tr>
                <td class="N" style="width: 352px">
                    شهر مبدا پیشفرض
                </td>
                <td>
                    <asp:DropDownList ID="mDefaultSrcCity" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    تایم آوت اجرای یک دستور بر حسب ثانیه
                </td>
                <td>
                    <asp:TextBox ID="mCommandTimeOut" runat="server" />
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator12" runat="server" ErrorMessage="RangeValidator"
                        MaximumValue="3600" MinimumValue="0" Type="Double" ValidationGroup="auG" ControlToValidate="mCommandTimeOut"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="auG"
                        ErrorMessage="این مقدار الزامي است" ControlToValidate="mCommandTimeOut" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="bus1" runat="server" visible="false">
                        <table style="width: 1075px">
                            <tr>
                                <td class="N" style="width: 351px">
                                    درصد کسر بلیط برگشتی تا 1 ساعت قیل از حرکت
                                </td>
                                <td style="width: 351px">
                                    <asp:TextBox ID="mPaidBackPercentOpen" runat="server" />
                                </td>
                                <td>
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="RangeValidator"
                                        MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="auG" ControlToValidate="mPaidBackPercentOpen"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="auG" ErrorMessage="این مقدار الزامي است"
                                        ControlToValidate="mPaidBackPercentOpen" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 351px">
                                    درصد کسر بلیط برگشتی برای کمتر از 1 ساعت مانده به حرکت و پس از آن
                                </td>
                                <td style="width: 351px">
                                    <asp:TextBox ID="mPaidBackPercentClosed" runat="server" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="prfv3" runat="server" ValidationGroup="auG" ErrorMessage="این مقدار الزامي است"
                                        ControlToValidate="mPaidBackPercentClosed" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="RangeValidator"
                                        MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="auG" ControlToValidate="mPaidBackPercentClosed"></asp:RangeValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    آدرس اینترنتی
                </td>
                <td>
                    <asp:TextBox ID="mUrl" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="bus2" runat="server" visible="false">
                        <table style="width: 1101px">
                            <tr>
                                <td class="N" style="width: 351px">
                                    شماره تعاونی
                                </td>
                                <td style="width: 349px">
                                    <asp:TextBox ID="mCoNo" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    قابلیت تغییر اعداد و ضرایب در محاسبه صورت
                </td>
                <td>
                    <asp:CheckBox ID="mChangeCloseValues" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="bus3" runat="server" visible="false">
                        <table style="width: 1105px">
                            <tr>
                                <td class="N" style="width: 352px">
                                    فعال بودن دکمه "ذخیره در تنظیمات" در صفحه صورت وضعیت
                                </td>
                                <td style="width: 349px">
                                    <asp:CheckBox ID="mAllowSaveSeries" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 352px">
                                    سري صورت&nbsp; <span style="color: #FF0000">اتوبوس</span>
                                </td>
                                <td style="width: 349px">
                                    <table border="0" cellpadding="0">
                                        <tr>
                                            <td style="border-bottom: solid 1px black;">
                                                <asp:TextBox runat="server" ID="mSeriesNo4" Width="120" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="rfs4" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                                    ControlToValidate="mSeriesNo4" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo1" Width="30" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="rfs1" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                                    ControlToValidate="mSeriesNo1" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo2" Width="30" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="rfs2" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                                    ControlToValidate="mSeriesNo2" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo3" Width="30" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="rfs3" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                                    ControlToValidate="mSeriesNo3" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td class="N" style="width: 352px">
                                    شماره شروع سری صورت&nbsp; <span style="color: #FF0000">اتوبوس</span>
                                </td>
                                <td style="width: 349px">
                                    &nbsp;</td>
                                <td>
                                    <asp:RangeValidator ID="rvs1" runat="server" ErrorMessage="این عدد باید بین 1 و 999999999 باشد"
                                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStart"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 352px">
                                    بازه سری صورت&nbsp; <span style="color: #FF0000">اتوبوس</span>
                                </td>
                                <td style="width: 349px">
                                    از
                                    <asp:TextBox runat="server" ID="mSeriesStartFrom" Width="120" MaxLength="9" Visible="false" />
                                    <asp:TextBox runat="server" ID="mSeriesStart" Width="120" MaxLength="9" />
                                    تا
                                    <asp:TextBox runat="server" ID="mSeriesStartTo" Width="120" MaxLength="9" />
                                    </td>
                                <td>
                                    <asp:RangeValidator ID="RangeValidator6" runat="server" ErrorMessage="شروع بازه باید بین 1 و 999999999 باشد"
                                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartFrom"></asp:RangeValidator>
                                    <asp:RangeValidator ID="RangeValidator7" runat="server" ErrorMessage="انتهای بازه باید بین 1 و 999999999 باشد"
                                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartTo"></asp:RangeValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    <div id="miniSaw1" runat="server" visible="false">
                        <table style="width: 1105px">
                            <tr>
                                <td class="N" style="width: 352px">
                                    سري صورت <span style="color: #FF0000">مینی بوس&nbsp; و سواری</span>
                                </td>
                                <td style="width: 350px">
                                    <table border="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNoMini1" Width="30px" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="auG"
                                                    ErrorMessage="*" ControlToValidate="mSeriesNoMini1" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNoMini2" Width="30" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="auG"
                                                    ErrorMessage="*" ControlToValidate="mSeriesNoMini2" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNoMini3" Width="30" MaxLength="3" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="auG"
                                                    ErrorMessage="*" ControlToValidate="mSeriesNoMini3" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    &nbsp;
                                    <asp:RangeValidator ID="rvs2" runat="server" ErrorMessage="این عدد باید بین 1 و 999999999 باشد"
                                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStart"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td class="N" style="width: 352px">
                                    شماره شروع سری صورت&nbsp; <span style="color: #FF0000">مینی بوس و سواری</span>
                                </td>
                                <td style="width: 350px">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 352px">
                                    بازه سری صورت&nbsp; <span style="color: #FF0000">مینی بوس و سواری</span>
                                </td>
                                <td style="width: 350px">
                                    از&nbsp;<asp:TextBox runat="server" ID="mSeriesStartMini" Width="120" MaxLength="9" />
                                            <asp:TextBox runat="server" ID="mSeriesStartMiniFrom" Width="120" MaxLength="9" Visible="false"/>
                                    تا
                                    <asp:TextBox runat="server" ID="mSeriesStartMiniTo" Width="120" MaxLength="9" />
                                </td>
                                <td>
                                    <asp:RangeValidator ID="RangeValidator8" runat="server" ErrorMessage="شروع بازه باید بین 1 و 999999999 باشد"
                                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartMiniFrom"></asp:RangeValidator>
                                    <asp:RangeValidator ID="RangeValidator9" runat="server" ErrorMessage="انتهای بازه باید بین 1 و 999999999 باشد"
                                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartMiniTo"></asp:RangeValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
            <td colspan="3">
            ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    سري صورت دربستی
                </td>
                <td>
                    <table border="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="mSeriesNoD1" Width="30" MaxLength="3" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="auG"
                                    ErrorMessage="*" ControlToValidate="mSeriesNoD1" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                /
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="mSeriesNoD2" Width="30" MaxLength="3" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="auG"
                                    ErrorMessage="*" ControlToValidate="mSeriesNoD2" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                /
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="mSeriesNoD3" Width="30" MaxLength="3" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="auG"
                                    ErrorMessage="*" ControlToValidate="mSeriesNoD3" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="display:none">
                <td class="N" style="width: 352px">
                    شماره شروع سری صورت دربستی
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:RangeValidator ID="sdsds" runat="server" ErrorMessage="این عدد باید بین 1 و 999999999 باشد"
                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartD"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    بازه سری صورت&nbsp; <span style="color: #FF0000">دربستی</span>
                </td>
                <td>
                    از
                    <asp:TextBox runat="server" ID="mSeriesStartD" Width="120" MaxLength="9" />
                    <asp:TextBox runat="server" ID="mSeriesStartDFrom" Width="120" MaxLength="9" Visible="false"/>
                    تا
                    <asp:TextBox runat="server" ID="mSeriesStartDTo" Width="120" MaxLength="9" />
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ErrorMessage="شروع بازه باید بین 1 و 999999999 باشد"
                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartDFrom"></asp:RangeValidator>
                    <asp:RangeValidator ID="RangeValidator11" runat="server" ErrorMessage="انتهای بازه باید بین 1 و 999999999 باشد"
                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mSeriesStartDTo"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    <div id="minisaw2" runat="server" visible="false">
                        <table style="width: 1105px">
                            <tr>
                                <td class="N" style="width: 352px">
                                    حذف خودکار سرویس مینی بوس بدون مسافر
                                </td>
                                <td>
                                    <asp:CheckBox ID="mAutoDeleteMini" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 352px">
                                    برگشت خودکار "تعداد" در سیستم کدی(مینی بوسی – سواری) بعد از هر بار فروش به مقدار
                                    اولیه
                                </td>
                                <td>
                                    <asp:CheckBox ID="mMiniResetNum" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    حق تمبر صورت
                </td>
                <td>
                    <asp:TextBox ID="mStamp" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="این عدد باید بین 1 و 999999999 باشد"
                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mStamp"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    کمیسیون
                </td>
                <td>
                    <asp:TextBox ID="mCommission" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="این عدد باید بین 1 و 999999999 باشد"
                        MaximumValue="999999" MinimumValue="1" Type="Integer" ValidationGroup="auG" ControlToValidate="mCommission"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    درصد عوارض
                </td>
                <td>
                    <asp:TextBox ID="mToll" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="rv7" runat="server" ErrorMessage="این عدد باید بین 0 و 100 باشد"
                        MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="auG" ControlToValidate="mToll"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    پذیرایی
                </td>
                <td>
                    <asp:TextBox ID="mReceptionCost" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="rv5" runat="server" ErrorMessage="این عدد باید بین 0 و 9999999 باشد"
                        MaximumValue="9999999" MinimumValue="0" Type="Integer" ValidationGroup="auG"
                        ControlToValidate="mReceptionCost"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px; height: 33px;">
                    هزينه اضافي
                </td>
                <td style="height: 33px">
                    <asp:TextBox ID="mExtraCost" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td style="height: 33px">
                    <asp:RangeValidator ID="rv6" runat="server" ErrorMessage="این عدد باید بین 0 و 9999999 باشد"
                        MaximumValue="9999999" MinimumValue="0" Type="Integer" ValidationGroup="auG"
                        ControlToValidate="mExtraCost"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px; height: 33px;">
                    هزینه آژانس
                </td>
                <td style="height: 33px">
                    <asp:TextBox ID="mAgencyPrice" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td style="height: 33px">
                    <asp:RangeValidator ID="rv8" runat="server" ErrorMessage="این عدد باید بین 0 و 9999999 باشد"
                        MaximumValue="9999999" MinimumValue="0" Type="Integer" ValidationGroup="auG"
                        ControlToValidate="mAgencyPrice"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    متفرقه
                </td>
                <td>
                    <asp:TextBox ID="mOthers" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ErrorMessage="این عدد باید بین 0 و 9999999 باشد"
                        MaximumValue="9999999" MinimumValue="0" Type="Integer" ValidationGroup="auG"
                        ControlToValidate="mOthers"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    حداقل قیمت برای محاسبه ی هزینه پذیرایی
                </td>
                <td>
                    <asp:TextBox ID="mMinForReception" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator13" runat="server" ErrorMessage="این عدد باید بین 0 و 9999999 باشد"
                        MaximumValue="9999999" MinimumValue="0" Type="Integer" ValidationGroup="auG"
                        ControlToValidate="mMinForReception"></asp:RangeValidator>
                </td>
            </tr>

            <tr>
                <td class="N" colspan="3">
                    <!-- 
                    چاپ بلیط
                    -->
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:TextBox Visible="false" ID="mTicket" runat="server" Height="198px" TextMode="MultiLine"
                        Width="1000px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    حداکثر صندلی مینی بوس
                </td>
                <td>
                    <asp:TextBox ID="mMiniChairs" runat="server" Width="100px" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="bus4" runat="server" visible="false">
                        <table style="width: 1105px">
                            <tr>
                                <td class="N" style="width: 350px">
                                    سرویس رزرو داشته باشد
                                </td>
                                <td>
                                    <asp:CheckBox ID="mAllowReserve" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 350px">
                                    امکان بستن صورت دارای بلیط رزروی
                                </td>
                                <td>
                                    <asp:CheckBox ID="mAllowReserveClose" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="N" style="width: 350px">
                                    رزروی ها در صورت وضعیت نوشته شوند
                                </td>
                                <td>
                                    <asp:CheckBox ID="mAllowReserveInClose" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    مرتب کردن بیمه بر اساس تاريخ - شماره صورت - ساعت
                </td>
                <td>
                    <asp:CheckBox ID="mSortDNT" runat="server" />
                </td>
                <td>
                    (در غیر اینصورت تاريخ - ساعت - شماره صورت)
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="minisaw3" runat="server" visible="False">
                        <table>
                            <tr>
                                <td class="N" style="width: 350px">
                                    صفحه فروش مینی بوس و سواری مثل اتوبوس باشد
                                </td>
                                <td>
                                    <asp:CheckBox ID="mMiniAsBus" runat="server" />
                                </td>
                                <td style="width: 442px">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    پر کردن خودکار سرویس
                </td>
                <td>
                    <asp:CheckBox ID="mAutoFill" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    اجازه حذف سرویس دارای بلیط
                </td>
                <td>
                    <asp:CheckBox ID="mCanDeleteTrip" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    امکان تعریف سرویس ثابت
                </td>
                <td>
                    <asp:CheckBox ID="mCanDefineFixed" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    اجازه ویرایش و حذف شهر
                </td>
                <td>
                    <asp:CheckBox ID="mCanEditCities" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    باز شدن اتوماتیک صفحات برای ویرایش در صورت اتمام تاریخ اعتبار
                </td>
                <td>
                    <asp:CheckBox ID="mAutoOpen" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    فروش اینترنتی اختیاری
                </td>
                <td>
                    <asp:CheckBox ID="mOnlineSale" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    ضروری بودن شماره موبایل
                </td>
                <td>
                    <asp:CheckBox ID="mMobile" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            
            <tr>
                <td class="N" style="width: 352px">
                    متن SMS رزرو
                </td>
                <td>
                    <asp:TextBox ID="mSMSMessage" runat="server" Height="60px" Width="400px" TextMode="MultiLine"/>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    متن SMS فروش
                </td>
                <td>
                    <asp:TextBox ID="mSMSMessageSale" runat="server" Height="60px" Width="400px" TextMode="MultiLine"/>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                    جدا کننده (تاریخ با فاصله از هم نوشته شوند.)
                </td>
                <td>
                    <asp:TextBox ID="mDateSeparators" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="minbus" runat="server" visible="false">
                        <table style="width: 1105px">
                            <tr>
                                <td class="N" colspan="3">
                                    نامهای پر کردن خودکار سرویس (لیست اسامی با ویرگول یا دونقطه : جدا شود). خط تیره
                                    (-) در اول نام بمعنای خانم میباشد
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox ID="mAutoFillNames" runat="server" Height="120px" TextMode="MultiLine"
                                        Width="1000px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="N" style="width: 352px">
                </td>
                <td>
                    <asp:Button ID="puOk" OnClick="puOk_Click" runat="server" Text="تاييد F7" CssClass="CB"
                        ValidationGroup="auG"></asp:Button>&nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="Err" colspan="3">
                    <asp:Label runat="server" ID="mError" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
