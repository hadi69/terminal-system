﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Manager_myadmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
    }
    protected void doUpdateExclusive_Click(object sender, EventArgs e)
    {
        if (mPass.Text != "muhsin")
        {
            mMsg.Text = "Wrong Pass";
            return;
        }
        try
        {
            Helper.Instance.Init();
            var all = from c in Helper.Instance.DB.Exclusives
                      select c;
            List<Exclusive> list = all.ToList();
            for (int i = 0; i < list.Count; i++)
            {
                try
                {
                    list[i].BodyInsuranceWage = Helper.Instance.GetWagePrice(0, int.Parse(list[i].Path.City1.Country.Code) > 1 ? true : false, list[i].Car.CarType.Layout.NumChairs > 30 ? 0 : list[i].Car.CarType.Layout.NumChairs > 5 ? 1 : 2, list[i].Path.KMs);
                    list[i].InsuranceWage = Helper.Instance.GetWagePrice(1, int.Parse(list[i].Path.City1.Country.Code) > 1 ? true : false, list[i].Car.CarType.Layout.NumChairs > 30 ? 0 : list[i].Car.CarType.Layout.NumChairs > 5 ? 1 : 2, list[i].Path.KMs);
                }
                catch { }
            }
            if (Helper.Instance.Update())
                mMsg.Text = list.Count + " exclusives updated.";
            else
                mMsg.Text = "Error:<br>" + Helper.Instance.LastException.Message + "<br>" + Helper.Instance.LastException.StackTrace;
        }
        catch (Exception ex)
        {
            mMsg.Text = "Error:<br>" + ex.Message + "<br>" + ex.StackTrace;
        }
        Helper.Instance.Dispose();
    }
}
