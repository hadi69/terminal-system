﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Manager_Users : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAddUser, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Fund> all = Helper.Instance.GetFunds();
            mFundID.DataValueField = "ID";
            mFundID.DataTextField = "Title";
            mFundID.DataSource = all;
            mFundID.DataBind();
        }
        {
            List<Branch> all = Helper.Instance.GetBranches();
            mBranchID.DataValueField = "ID";
            mBranchID.DataTextField = "Title";
            mBranchID.DataSource = all;
            mBranchID.DataBind();
        }
        {
            List<Role> all = Helper.Instance.GetRoles();
            mRole.DataValueField = "ID";
            mRole.DataTextField = "Name";
            mRole.DataSource = all;
            mRole.DataBind();
        }
    }
    private void BindData()
    {
        string adminUser=Helper.Instance.GetSettingValue("CompanyUser");
        var all = from c in Helper.Instance.DB.Users where c.UserName!=adminUser
                  select c;
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        User info;
        if (Null.NullInteger == EditID)
        {
            User old = Helper.Instance.GetUser(mUserName.Text);
            if (null != old)
            {
                mError.Text = "این نام کاربری قبلا استفاده شده است";
                puEx.Show();
                return;
            }
            info = new User();
            info.UserName = mUserName.Text;
            info.Password = mPassword1.Text;
            if (info.Password.Trim().Length == 0)
            {
                mError.Text = "رمز عبور و تکرار رمز عبور الزامی است";
                puEx.Show();
                return;
            }
        }
        else
        {
            info = Helper.Instance.GetUser(EditID);
            if (mPassword1.Text.Trim().Length > 0)
                info.Password = mPassword1.Text;
        }
        if (info == null)
            return;

        info.FullName = mFullName.Text;
        info.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        info.BranchID = Tool.GetInt(mBranchID.SelectedValue, Null.NullInteger);
        info.RoleID = Tool.GetInt(mRole.SelectedValue, Null.NullInteger);

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Users.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            int colorID = 0;
            if (mColor1.Checked)
                colorID = 1;
            else if (mColor2.Checked)
                colorID = 2;
            else if (mColor3.Checked)
                colorID = 3;
            else if (mColor4.Checked)
                colorID = 4;
            else if (mColor5.Checked)
                colorID = 5;
            else if (mColor6.Checked)
                colorID = 6;
            else if (mColor7.Checked)
                colorID = 7;
            else if (mColor8.Checked)
                colorID = 8;
            else if (mColor9.Checked)
                colorID = 9;
            else if (mColor10.Checked)
                colorID = 10;
            Helper.Instance.UpdateSetting("UserColor" + info.ID, colorID.ToString());
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }

    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            User info = e.Row.DataItem as User;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[4].Text = info.Role != null ? info.Role.Name : "";
                if (info.Fund != null)
                    e.Row.Cells[5].Text = info.Fund.Title;
                else
                    e.Row.Cells[5].Text = "";
                if (info.Branch != null)
                    e.Row.Cells[6].Text = info.Branch.Title;
                else
                    e.Row.Cells[6].Text = "";

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        User info = Helper.Instance.GetUser(EditID);
        if (info == null)
            return;

       
        mUserName.Enabled = false;
        mUserName.Text = info.UserName;
        mFullName.Text = info.FullName;
        Tool.SetSelected(mFundID, info.FundID);
        Tool.SetSelected(mBranchID, info.BranchID);
        Tool.SetSelected(mRole, info.RoleID);

        mColor1.Checked = mColor2.Checked = mColor3.Checked = mColor4.Checked = mColor5.Checked = mColor6.Checked = mColor7.Checked = mColor8.Checked = mColor9.Checked = mColor10.Checked = false;
        int colorID = Tool.GetInt(Helper.Instance.GetSettingValue("UserColor" + info.ID), 1);
        if (colorID == 1)
            mColor1.Checked = true;
        else if (colorID == 2)
            mColor2.Checked = true;
        else if (colorID == 3)
            mColor3.Checked = true;
        else if (colorID == 4)
            mColor4.Checked = true;
        else if (colorID == 5)
            mColor5.Checked = true;
        else if (colorID == 6)
            mColor6.Checked = true;
        else if (colorID == 7)
            mColor7.Checked = true;
        else if (colorID == 8)
            mColor8.Checked = true;
        else if (colorID == 9)
            mColor9.Checked = true;
        else if (colorID == 10)
            mColor10.Checked = true;
        else
            mColor1.Checked = true;
        Helper.Instance.UpdateSetting("UserColor" + info.ID, colorID.ToString());

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        try
        {
            var count = (from t in Helper.Instance.DB.Tickets
                         where t.SaleUserID == id
                         select t).Count();
            if (count > 0)
            {
                mMsg.Text = "این کاربر بلیط فروخته است و نمیتوان آنرا حذف کرد";
                return;
            }
        }
        catch (Exception ex)
        {
        }
        if (Helper.Instance.DeleteUser(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    protected void doAddUser_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mFullName.Text = mUserName.Text = mPassword1.Text = mPassword2.Text = "";
        mUserName.Enabled = true;
        puEx.Show();
    }

}
