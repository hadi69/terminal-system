﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Users.aspx.cs" Inherits="Manager_Users" Title="کاربران" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <asp:Button ID="doAddUser" runat="server" Text="کاربر جديد INS" CssClass="CB" OnClick="doAddUser_Click">
            </asp:Button>
            <asp:GridView ID="list" runat="server" OnRowDeleting="list_RowDeleting" OnRowDataBound="list_RowDataBound"
                OnSelectedIndexChanged="list_SelectedIndexChanged" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="UserName" HeaderText="نام کاربري" />
                    <asp:BoundField DataField="FullName" HeaderText="نام و نام خانوادگي" />
                    <asp:BoundField DataField="Role" HeaderText="رل" />
                    <asp:BoundField DataField="FundID" HeaderText="صندوق" />
                    <asp:BoundField DataField="BranchID" HeaderText="دفتر" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن کاربر جديد</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نام کاربري
                            </td>
                            <td>
                                <asp:TextBox ID="mUserName" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="auG" ErrorMessage="نام کاربري الزامي است"
                                    ControlToValidate="mUserName" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام و نام خانوادگي
                            </td>
                            <td>
                                <asp:TextBox ID="mFullName" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="auG" ErrorMessage="نام و نام خانوادگي الزامي است"
                                    ControlToValidate="mFullName" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                رمزعبور
                            </td>
                            <td>
                                <asp:TextBox ID="mPassword1" runat="server" TextMode="Password" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تکرار رمز عبور
                            </td>
                            <td>
                                <asp:TextBox ID="mPassword2" runat="server" TextMode="Password" />
                            </td>
                            <td>
                                <asp:CompareValidator ID="pcv1" runat="server" ValidationGroup="auG" ErrorMessage="بايد رمز عبور و تکرار آن يکسان باشند"
                                    ControlToValidate="mPassword2" Display="Dynamic" ControlToCompare="mPassword1"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                رل
                            </td>
                            <td>
                                <asp:DropDownList ID="mRole" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                صندوق
                            </td>
                            <td>
                                <asp:DropDownList ID="mFundID" runat="server" />
                                &nbsp;&nbsp;کاربر با رل Admin به همه صندوقها دسترسی دارد
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                دفتر
                            </td>
                            <td>
                                <asp:DropDownList ID="mBranchID" runat="server" />
                                &nbsp;&nbsp;کاربر با رل Admin به همه دفاتر دسترسی دارد
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                رنگ
                            </td>
                            <td>
                                <span style="background-color: #003366">
                                    <asp:RadioButton GroupName="color" ID="mColor1" runat="server" Text='رنگ 1' /></span>
                                <span style="background-color: #FFFF88">
                                    <asp:RadioButton GroupName="color" ID="mColor2" runat="server" Text='رنگ 2' /></span>
                                <span style="background-color: #663300">
                                    <asp:RadioButton GroupName="color" ID="mColor3" runat="server" Text='رنگ 3' /></span>
                                <span style="background-color: #330066">
                                    <asp:RadioButton GroupName="color" ID="mColor4" runat="server" Text='رنگ 4' /></span>
                                <span style="background-color: #FF1415">
                                    <asp:RadioButton GroupName="color" ID="mColor5" runat="server" Text='رنگ 5' /></span>
                                <span style="background-color: #00CC66">
                                    <asp:RadioButton GroupName="color" ID="mColor6" runat="server" Text='رنگ 6' /></span>
                                <span style="background-color: #da70d6">
                                    <asp:RadioButton runat="server" GroupName="color" ID="mColor7" Text="رنگ 7"/>
                                </span>
                                <span style="background-color: #daa520">
                                    <asp:RadioButton runat="server" GroupName="color" ID="mColor8" Text="رنگ 8"/>
                                </span>
                                <span style="background-color: #00ffff">
                                    <asp:RadioButton runat="server" GroupName="color" ID="mColor9" Text="رنگ 9"/>
                                </span>
                                <span style="background-color: #bdb76b">
                                    <asp:RadioButton runat="server" GroupName="color" ID="mColor10" Text="رنگ 10"/>
                                </span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" OnClick="puOk_Click" runat="server" Text="تاييد F7" CssClass="CB"
                                    ValidationGroup="auG"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB" CausesValidation="False">
                                </asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;
</asp:Content>
