﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Personalize.aspx.cs" Inherits="Manager_Personalize" Title="تنظيمات شخصي" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tbody>
            <tr>
                <td class="N">
                    نام کاربري
                </td>
                <td>
                    <asp:TextBox ID="mUserName" runat="server" Enabled="false" CssClass="T" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N">
                    نام و نام خانوادگي
                </td>
                <td>
                    <asp:TextBox ID="mFullName" runat="server" CssClass="T" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="auG" ErrorMessage="نام و نام خانوادگي الزامي است"
                        ControlToValidate="mFullName" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="N">
                    رل
                </td>
                <td>
                    <asp:DropDownList ID="mRole" runat="server" Enabled="false" CssClass="DD" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N">
                    صندوق
                </td>
                <td>
                    <asp:DropDownList ID="mFundID" runat="server" CssClass="DD" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N">
                </td>
                <td style="text-align: left">
                    <asp:Button ID="puOk" OnClick="puOk_Click" runat="server" Text="تاييد" CssClass="CB"
                        ValidationGroup="auG" Width="50px"></asp:Button>&nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N" colspan="3">
                    &nbsp;
                    تغيير رمز عبور
                </td>
            </tr>
            <tr>
                <td class="N">
                    رمز عبور فعلي</td>
                <td>
                    <asp:TextBox ID="mPassword" runat="server" CssClass="T" TextMode="Password" />
                </td>
                <td>
                   <asp:RequiredFieldValidator ID="rfvp1" runat="server" ValidationGroup="auPG" ErrorMessage="رمز عبور فعلي الزامي است"
                        ControlToValidate="mPassword" Display="Dynamic"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="N">
                    رمزعبور
                </td>
                <td>
                    <asp:TextBox ID="mPassword1" runat="server" CssClass="T" TextMode="Password" />
                </td>
                <td>
                <asp:RequiredFieldValidator ID="rfvp2" runat="server" ValidationGroup="auPG" ErrorMessage="رمزعبور الزامي است"
                        ControlToValidate="mPassword1" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="N">
                    تکرار رمز عبور
                </td>
                <td>
                    <asp:TextBox ID="mPassword2" runat="server" CssClass="T" TextMode="Password" />
                </td>
                <td>
                <asp:RequiredFieldValidator ID="rfvp3" runat="server" ValidationGroup="auPG" ErrorMessage="تکرار رمز عبور الزامي است"
                        ControlToValidate="mPassword2" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="pcv1" runat="server" ValidationGroup="auPG" ErrorMessage="بايد رمز عبور و تکرار آن يکسان باشند"
                        ControlToValidate="mPassword2" Display="Dynamic" ControlToCompare="mPassword1"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="N">
                </td>
                <td style="text-align: left">
                    <asp:Button ID="doChangePass" OnClick="doChangePass_Click" runat="server" 
                        Text="تاييد" CssClass="CB"
                        ValidationGroup="auPG" Width="50px"></asp:Button>&nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N">
                    &nbsp;
                </td>
                <td colspan="2">
                    <asp:Label runat="server" ID="mMsg" CssClass="Err" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
