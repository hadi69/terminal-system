﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Manager_admin : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {

    }
    private void BindData()
    {
        string softwareType =Helper.Instance.GetSettingValue("SoftwareType");
        //string[] stype = softwareType.Split(',');
        for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            if (softwareType.Contains(CheckBoxList1.Items[i].Value))
                CheckBoxList1.Items[i].Selected = true;
        //if (softwareType != null)
        //    Tool.SetSelected(mSoftwareType,softwareType);
        User info = SiteSettings.User;
        if (info != null)
            mManagerUserName.Text = info.UserName;
        mUserID.Text = Helper.Instance.GetSettingValue("UserID");
        mCompanyCode.Text = Helper.Instance.GetSettingValue("CompanyCode");
        mCompanyName.Text = Helper.Instance.GetSettingValue("CompanyName");
        mPassword.Text = Helper.Instance.GetSettingValue("Password");

        mIdentity_Id.Text = Helper.Instance.GetSettingValue("Identity_id");
        mUser_Id.Text = Helper.Instance.GetSettingValue("User_Id");
        mPassword_Id.Text = Helper.Instance.GetSettingValue("Password_Id");
        mCompany_code.Text = Helper.Instance.GetSettingValue("CompanyCode");

        txtInsUserName.Text = Helper.Instance.GetSettingValue("InsUserName");
        txtInsPassWord.Text=Helper.Instance.GetSettingValue("InsPassWord");

        mSMSUserName.Text = Helper.Instance.GetSettingValue("SMSUserName");
        mSMSPassword.Text = Helper.Instance.GetSettingValue("SMSPassword");

        mTechnical.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("Technical"), false);
        mSendSMSReserve.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSReserve"), false);
        mSendSMSSale.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSSale"), false);
        mShowMobile.Checked = Tool.GetBool(Helper.Instance.GetSettingValue("ShowMobile"), false);
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
       // ok = Helper.Instance.UpdateSetting("CommandTimeOut", mCommandTimeOut.Text);
        string softtype = "";
        for (int i = 0; i < CheckBoxList1.Items.Count; i++)
        {
            if (CheckBoxList1.Items[i].Selected)
                softtype += CheckBoxList1.Items[i].Value + ",";
        }
        softtype = softtype.Substring(0, softtype.Length - 1);
        bool ok = Helper.Instance.UpdateSetting("SoftwareType", softtype);
        ok = Helper.Instance.UpdateSetting("CompanyCode", mCompanyCode.Text);
        ok = Helper.Instance.UpdateSetting("CompanyName", mCompanyName.Text);
        ok = Helper.Instance.UpdateSetting("UserID", mUserID.Text);
        ok = Helper.Instance.UpdateSetting("Password", mPassword.Text);

        ok = Helper.Instance.UpdateSetting("Identity_id", mIdentity_Id.Text);
        ok = Helper.Instance.UpdateSetting("User_Id", mUser_Id.Text);
        ok = Helper.Instance.UpdateSetting("Password_Id", mPassword_Id.Text);
        ok = Helper.Instance.UpdateSetting("CompanyCode", mCompany_code.Text);

        ok = Helper.Instance.UpdateSetting("InsUserName", txtInsUserName.Text);
        ok = Helper.Instance.UpdateSetting("InsPassWord", txtInsPassWord.Text);

        ok = Helper.Instance.UpdateSetting("SMSUserName", mSMSUserName.Text);
        ok = Helper.Instance.UpdateSetting("SMSPassword", mSMSPassword.Text);

        ok = Helper.Instance.UpdateSetting("Technical", mTechnical.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("SendSMSReserve", mSendSMSReserve.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("SendSMSSale", mSendSMSSale.Checked.ToString());
        ok = Helper.Instance.UpdateSetting("ShowMobile", mShowMobile.Checked.ToString());

        if (ok)
        {
            BindData();
            mError.Text = "تنظیمات با موفقیت ذخیره شد.";
        }
        else
            mError.Text = "تنظیمات ذخیره نشد<br />" + Helper.Instance.LastException.Message;
    }


    protected void doChangePass_Click(object sender, EventArgs e)
    {
        User info = SiteSettings.User;
        if (null == info)
            return;
        info = Helper.Instance.GetUser(info.ID);
        if (mCurPassword.Text.Trim() != info.Password)
        {
            mError.Text = "رمز عبور فعلي اشتباه است.";
            return;
        }
        if (mPassword1.Text.Trim().Length > 0)
            info.Password = mPassword1.Text;
        info.UserName = mManagerUserName.Text;
        info.FullName = "کاربر اصلی";
        info.IsAdmin = true;
        bool ok = Helper.Instance.UpdateSetting("CompanyUser", mManagerUserName.Text);
        ok = Helper.Instance.UpdateSetting("CompanyPassword", mPassword.Text);
        if (Helper.Instance.Update() && ok)
        {
            SiteSettings.UserID = info.ID;
            BindData();
            mError.Text = "رمز عبور با موفقيت بروزرساني شد.";
        }
        else
            mError.Text = "خطا در بروزرساني رمز عبور.";
    }
}
