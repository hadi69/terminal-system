﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Manager_Roles : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAddUser, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {

    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Roles
                  select c;

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Role info;
        if (Null.NullInteger == EditID)
        {
            info = new Role();
        }
        else
            info = Helper.Instance.GetRole(EditID);
        if (info == null)
            return;

        info.Name = mName.Text;
        info.Comments = mComments.Text;

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Roles.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            UpdateAccess(info);
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    bool UpdateAccess(Role info)
    {
        for (int i = 0; i < mAccess.Items.Count; i++)
        {
            Access access = Helper.Instance.GetAccess(Tool.GetInt(mAccess.Items[i].Value, Null.NullInteger));
            if (!HasAccess(info, access))
            {
                RoleAccess ra = new RoleAccess();
                ra.Access = access;
                ra.Role = info;
                Helper.Instance.DB.RoleAccesses.InsertOnSubmit(ra);
                info.RoleAccesses.Add(ra);
            }
        }
        for (int j = 0; j < mAllAccess.Items.Count; j++)
        {
            Access access1 = Helper.Instance.GetAccess(Tool.GetInt(mAllAccess.Items[j].Value, Null.NullInteger));
            if (HasAccess(info, access1))
            {
                RoleAccess ra1 = Helper.Instance.GetRoleAccess(info.ID, access1.ID);
                Helper.Instance.DB.RoleAccesses.DeleteOnSubmit(ra1);
                info.RoleAccesses.Remove(ra1);

            }
        }
        return Helper.Instance.Update();
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Role info = e.Row.DataItem as Role;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                //e.Row.Cells[4].Text = info.Role != null ? info.Role.Name : "";

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Role info = Helper.Instance.GetRole(EditID);
        if (info == null)
            return;

        mName.Text = info.Name;
        mComments.Text = info.Comments;
        BindAccess(info);

        puEx.Show();
    }
    void BindAccess(Role info)
    {
        mAccess.Items.Clear();
        mAllAccess.Items.Clear();
        List<Access> all = Helper.Instance.GetAccesses();
        for (int i = 0; i < all.Count; i++)
        {
            ListItem li = new ListItem(all[i].Comments, all[i].ID.ToString());
            if (HasAccess(info, all[i]))
                mAccess.Items.Add(li);
            else
                mAllAccess.Items.Add(li);
        }
    }
    bool HasAccess(Role info, Access access)
    {
        if (info == null)
            return false;
        for (int i = 0; i < info.RoleAccesses.Count; i++)
        {
            if (info.RoleAccesses[i].AccessID == access.ID)
                return true;
        }
        return false;
    }
    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteRole(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    protected void doAddUser_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mName.Text = mComments.Text = "";

        BindAccess(null);

        puEx.Show();
    }
    protected void doAddAll_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < mAllAccess.Items.Count; i++)
        {
            mAccess.Items.Add(mAllAccess.Items[i]);
        }
        mAllAccess.Items.Clear();
        puEx.Show();
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < mAllAccess.Items.Count; i++)
        {
            if (mAllAccess.Items[i].Selected)
            {
                mAccess.Items.Add(mAllAccess.Items[i]);
                mAllAccess.Items.RemoveAt(i);
                i--;
            }
        }
        puEx.Show();
    }
    protected void doDelete_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < mAccess.Items.Count; i++)
        {
            if (mAccess.Items[i].Selected)
            {
                mAllAccess.Items.Add(mAccess.Items[i]);
                mAccess.Items.RemoveAt(i);
                i--;
            }
        }
        puEx.Show();
    }
    protected void doDeleteAll_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < mAccess.Items.Count; i++)
            mAllAccess.Items.Add(mAccess.Items[i]);
        mAccess.Items.Clear();
        puEx.Show();
    }
}
