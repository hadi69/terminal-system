﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="admin.aspx.cs" Inherits="Manager_admin" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tbody>
            <tr>
                <td class="N">
                    نسخه نرم افزار
                </td>
                <td>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">اتوبوس</asp:ListItem>
                        <asp:ListItem Value="1">مینی بوس</asp:ListItem>
                        <asp:ListItem Value="2">سواری</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N">
                    نام شرکت
                </td>
                <td>
                    <asp:TextBox ID="mCompanyName" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    کد شرکت
                </td>
                <td>
                    <asp:TextBox ID="mCompanyCode" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    شماره کاربر
                </td>
                <td>
                    <asp:TextBox ID="mUserID" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    رمز
                </td>
                <td>
                    <asp:TextBox ID="mPassword" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    مدیر فنی
                </td>
                <td>
                    <asp:CheckBox ID="mTechnical" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    صدور صورت وضعیت:
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" >
                    &nbsp;
                </td>
                <td colspan="2">
                    <table border="0" cellpadding="2">
                        <tr>
                            <td class="N">
                                کد شناسایی
                            </td>
                            <td>
                                <asp:TextBox ID="mIdentity_Id" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td class="N">
                                کد شرکت
                            </td>
                            <td>
                                <asp:TextBox ID="mCompany_code" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد کاربر
                            </td>
                            <td>
                                <asp:TextBox ID="mUser_Id" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td class="N">
                                رمز عبور
                            </td>
                            <td>
                                <asp:TextBox ID="mPassword_Id" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="N">
                    اطلاعات ارسال بیمه:
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" >
                    &nbsp;
                </td>
                <td colspan="2">
                    <table border="0" cellpadding="2"> 
                        <tr>
                            <td class="N">
                                کد کاربر
                            </td>
                            <td>
                                <asp:TextBox ID="txtInsUserName" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td class="N">
                                رمز عبور
                            </td>
                            <td>
                                <asp:TextBox ID="txtInsPassWord" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="N">
                    اطلاعات ارسال SMS:
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N" >
                    &nbsp;
                </td>
                <td colspan="2">
                    <table border="0" cellpadding="2"> 
                        <tr>
                            <td class="N">
                                کد کاربری پیام کوتاه
                            </td>
                            <td>
                                <asp:TextBox ID="mSMSUserName" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td class="N">
                                رمز عبور پیام کوتاه
                            </td>
                            <td>
                                <asp:TextBox ID="mSMSPassword" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="N">
                    ارسال SMS هنگام رزرو
                </td>
                <td>
                    <asp:CheckBox ID="mSendSMSReserve" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    ارسال SMS هنگام فروش
                </td>
                <td>
                    <asp:CheckBox ID="mSendSMSSale" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    نمایش شماره تماس اینترنتی
                </td>
                <td>
                    <asp:CheckBox ID="mShowMobile" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                </td>
                <td>
                    <asp:Button ID="puOk" OnClick="puOk_Click" runat="server" Text="تاييد " CssClass="CB"
                        ValidationGroup="auG"></asp:Button>&nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="N" colspan="3">
                    &nbsp; تغيير رمز عبور
                </td>
            </tr>
            <tr>
                <td class="N">
                    نام کاربری مدیر
                </td>
                <td>
                    <asp:TextBox ID="mManagerUserName" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="N">
                    رمز عبور فعلي
                </td>
                <td>
                    <asp:TextBox ID="mCurPassword" runat="server" CssClass="T" TextMode="Password" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvp1" runat="server" ValidationGroup="auPG" ErrorMessage="رمز عبور فعلي الزامي است"
                        ControlToValidate="mPassword" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="N">
                    رمزعبور
                </td>
                <td>
                    <asp:TextBox ID="mPassword1" runat="server" CssClass="T" TextMode="Password" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvp2" runat="server" ValidationGroup="auPG" ErrorMessage="رمزعبور الزامي است"
                        ControlToValidate="mPassword1" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="N">
                    تکرار رمز عبور
                </td>
                <td>
                    <asp:TextBox ID="mPassword2" runat="server" CssClass="T" TextMode="Password" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvp3" runat="server" ValidationGroup="auPG" ErrorMessage="تکرار رمز عبور الزامي است"
                        ControlToValidate="mPassword2" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="pcv1" runat="server" ValidationGroup="auPG" ErrorMessage="بايد رمز عبور و تکرار آن يکسان باشند"
                        ControlToValidate="mPassword2" Display="Dynamic" ControlToCompare="mPassword1"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="N">
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="Err" colspan="3">
                    <asp:Label runat="server" ID="mError" />
                    <asp:Button ID="doChangePass" OnClick="doChangePass_Click" runat="server" Text="تاييد"
                        CssClass="CB" ValidationGroup="auPG" Width="50px"></asp:Button>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
