﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class errors : System.Web.UI.Page
{
    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        msg = DateTime.Now.ToString();
        DateTime dt1 = DateTime.Now;
        //list.EnableViewState = false;
        if (!IsPostBack)
        {
            //Helper.Instance.AutoDeleteTrips();
            BindInitData();
            //if (!Tool.ButtonIsClicked(Request, doSearch))
            BindData();
        }
        //ScriptManager manager = ScriptManager.GetCurrent(this);
        //if (!manager.IsInAsyncPostBack)
        //    BindData();
        StringBuilder sb = new StringBuilder();
        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function $d(id){$get('" + mIDToDelete.ClientID + "').value=id;$find('puExBehaviorID').show();return false;}");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        Page.RegisterClientScriptBlock("Ask_Del", sb.ToString());
        msg += "<br> PageLoad: " + (DateTime.Now - dt1).ToString();
    }
    private void BindInitData()
    {
        {
            List<string> all = (from h in Helper.Instance.DB.Trips
                                select h.Series).ToList();
            all.Insert(0, "");

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();
        }

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Errors.sDateStart"]), DateTime.Now);
        sDateEnd.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Errors.sDateEnd"]), DateTime.Now);

    }
    private List<Trip> GetTrips()
    {
        var all = from c in Helper.Instance.DB.Trips
                  where c.Closed == true
                  orderby c.Date, c.Service.DepartureTime, c.Service.ServiceType
                  select c;

        List<Trip> trips = all.ToList();
        if (trips == null || trips.Count == 0)
            return trips;

        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].No < sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].No > sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            for (int i = 0; i < trips.Count; i++)
                if (!trips[i].Series.Contains(s))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (!mBus.Checked)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarType == null || trips[i].CarType.Layout.NumChairs > 25)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (!mMini.Checked)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarType == null || (trips[i].CarType.Layout.NumChairs > 6 && trips[i].CarType.Layout.NumChairs < 25))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (!mSewari.Checked)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarType == null || trips[i].CarType.Layout.NumChairs < 6)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        Session["Errors.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["Errors.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        if (startDate != Null.NullDate || toDate != Null.NullDate)
        {
            startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
                : DateTime.Now.AddYears(-10);
            toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
                : DateTime.Now.AddYears(10);
            for (int i = 0; i < trips.Count; i++)
            {
                if ((trips[i].Date < startDate || trips[i].Date > toDate))
                {
                    trips.RemoveAt(i);
                    i--;
                }
            }
        }

        return trips;


    }
    private List<Exclusive> GetExclusives()
    {
        var all = from c in Helper.Instance.DB.Exclusives
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;// Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
            all = all.Where(c => c.DepartureDate >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            all = all.Where(c => c.DepartureDate <= toDate);

        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            try { all = all.Where(c => c.No >= sID); }
            catch { };
        }

        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            try { all = all.Where(c => c.No <= sID); }
            catch { };
        }

        if (sSeries.Text.Trim().Length > 0)
            all = all.Where(c => c.Series.Contains(sSeries.Text.Trim()));

        List<Exclusive> res = all.ToList();

        if (res.Count > 0)
        {
            if (!mBus.Checked)
            {
                for (int i = 0; i < res.Count; i++)
                    try
                    {
                        int numChairs = res[i].Car.CarType.Layout.NumChairs.Value;
                        if (numChairs > 25)
                        {
                            res.RemoveAt(i);
                            i--;
                        }
                    }
                    catch { }
            }
            if (!mMini.Checked)
            {
                for (int i = 0; i < res.Count; i++)
                    try
                    {
                        int numChairs = res[i].Car.CarType.Layout.NumChairs.Value;
                        if (numChairs >= 6 && numChairs < 25)
                        {
                            res.RemoveAt(i);
                            i--;
                        }
                    }
                    catch { }
            }
            if (!mSewari.Checked)
            {
                for (int i = 0; i < res.Count; i++)
                    try
                    {
                        int numChairs = res[i].Car.CarType.Layout.NumChairs.Value;
                        if (numChairs < 6)
                        {
                            res.RemoveAt(i);
                            i--;
                        }
                    }
                    catch { }
            }
        }
        return res;
    }
    private List<InValid> GetInvalids()
    {
        // ---------------------------------
        var all = from c in Helper.Instance.DB.InValids
                  orderby c.Date, c.No
                  select c;

        List<InValid> trips = all.ToList();
        if (trips == null || trips.Count == 0)
            return trips;


        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].No < sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].No > sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            for (int i = 0; i < trips.Count; i++)
                if (!trips[i].Series.Contains(s))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate != Null.NullDate || toDate != Null.NullDate)
        {
            startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
                : DateTime.Now.AddYears(-10);
            toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
                : DateTime.Now.AddYears(10);
            for (int i = 0; i < trips.Count; i++)
            {
                if ((trips[i].Date < startDate || trips[i].Date > toDate))
                {
                    trips.RemoveAt(i);
                    i--;
                }
            }
        }

        return trips;
        // ---------------------------------

        //all = all.OrderBy(c => c.Date);
        //return all.ToList();
    }
    private void BindData()
    {
        DateTime dt1 = DateTime.Now;
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        string qry = string.Format(@"SELECT Trips.*
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
	, Services.ServiceType
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
	, Layouts.NumChairs
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
LEFT JOIN Cars ON Trips.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
WHERE Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59'
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);
        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            qry += " AND Trips.No>=" + sID;
        }
        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            qry += " AND Trips.No<=" + sID;
        }

        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.No, Trips.Series, Trips.Date, Services.DepartureTime, Services.ServiceType");
        if (dt != null && dt.Rows.Count > 0)
        {
            if (sSeries.Text.Trim().Length > 0)
            {
                string s = Tool.ParseSeries(sSeries.Text.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series.Contains(s))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mBus.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs > 25)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mMini.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs >= 6 && numChairs < 25)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mSewari.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs < 6)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        Session["Errors.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["Errors.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        // ------------------------------------
        // Create Hash
        Dictionary<string, int> hash = new Dictionary<string, int>();
        if (dt != null && dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string series = Tool.GetString(dt.Rows[i]["Series"], "");
                int no = Tool.GetInt(dt.Rows[i]["No"], 0);
                string h = string.Format("N{0}_S{1}", no, series);
                if (!hash.ContainsKey(h))
                    hash.Add(h, 1);
                else
                    hash[h] = hash[h] + 1;
            }
        }

        // -----------------------------------
        List<Exclusive> exs = GetExclusives();
        if (exs != null && exs.Count > 0)
        {
            for (int i = 0; i < exs.Count; i++)
            {
                string series = exs[i].Series;
                int no = exs[i].No;
                string h = string.Format("N{0}_S{1}", no, series);
                if (!hash.ContainsKey(h))
                    hash.Add(h, 1);
                else
                    hash[h] = hash[h] + 1;
            }
        }

        // -----------------------------------
        List<InValid> invs = GetInvalids();
        if (invs != null && invs.Count > 0)
        {
            for (int i = 0; i < invs.Count; i++)
            {
                string series = invs[i].Series;
                int no = invs[i].No;
                string h = string.Format("N{0}_S{1}", no, series);
                if (!hash.ContainsKey(h))
                    hash.Add(h, 1);
                else
                    hash[h] = hash[h] + 1;
            }
        }

        
        // -----------------------------------
        // Show only duplicated or numtickets=0
        string[] str=new string[10000];
        int num = 0;
        if (dt != null && dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int numTickets = Tool.GetInt(dt.Rows[i]["NumTickets"], 0);
                if (0 == numTickets)
                {
                    str[num] = "تعداد بلیط صفر";
                    num++;
                    continue;
                }
                string series = Tool.GetString(dt.Rows[i]["Series"], "");
                int no = Tool.GetInt(dt.Rows[i]["No"], 0);
                string h = string.Format("N{0}_S{1}", no, series);
                if (!hash.ContainsKey(h) || hash[h] < 2)
                {
                    dt.Rows.RemoveAt(i);
                    i--;
                }
                else
                {
                    str[num] = "شماره صورت تکراری";
                    num++;
                }
            } dt.Columns.Add("");
        }
        if (exs != null && exs.Count > 0)
        {
            for (int i = 0; i < exs.Count; i++)
            {
                int numTickets = exs[i].NumPassangers;
                if (0 == exs[i].No)
                {
                    str[num] = "شماره صورت صفر";
                    num++;
                    continue;
                }
                if (0 == numTickets)
                {
                    str[num] = "تعداد بلیط صفر";
                    num++;
                    continue;
                }
                string series = exs[i].Series;
                int no = exs[i].No;
                string h = string.Format("N{0}_S{1}", no, series);
                if (!hash.ContainsKey(h) || hash[h] < 2)
                {
                    exs.RemoveAt(i);
                    i--;
                }
                else
                {
                    str[num] = "شماره صورت تکراری";
                    num++;
                }
            }
        }

        if (invs != null && invs.Count > 0)
        {
            for (int i = 0; i < invs.Count; i++)
            {
                //int numTickets = invs[i].NumPassangers;
                //if (0 == numTickets)
                //    continue;
                string series = invs[i].Series;
                int no = invs[i].No;
                string h = string.Format("N{0}_S{1}", no, series);
                if (!hash.ContainsKey(h) || hash[h] < 2)
                {
                    invs.RemoveAt(i);
                    i--;
                }
                else
                {
                    str[num] = "شماره صورت تکراری";
                    num++;
                }
            }
        }

        List<object> all = new List<object>();
        if (dt != null)
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                all.Add(dt.Rows[i]);
            }
        
        // -----------------------------------
        if (exs != null)
            for (int i = 0; i < exs.Count; i++)
                all.Add(exs[i]);
        

        if (invs != null)
            for (int i = 0; i < invs.Count; i++)
                all.Add(invs[i]);
        list.DataSource = all;
        int k = 0;
        
        msg += "<br> GetTrips:" + (DateTime.Now - dt1).ToString();
        dt1 = DateTime.Now;
        list.DataBind();
        foreach (GridViewRow row in list.Rows)
        {
            row.Cells[11].Text=str[k];
            k++;
        }
        msg += "<br> Bind: " + (DateTime.Now - dt1).ToString();
        // mMsg.Text = msg;
    }
    string lastHash = null, lastClass = "GR";
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRow row = e.Row.DataItem as DataRow;
            if (row != null)
            {
                #region Row
                string series = Tool.GetString(row["Series"], "");
                int no = Tool.GetInt(row["No"], 0);
                string h = string.Format("N{0}_S{1}", no, series);
                if (h != lastHash)
                {
                    lastClass = lastClass == "GR" ? "GAR" : "GR";
                }
                e.Row.CssClass = lastClass;
                lastHash = h;

                e.Row.Cells[0].Text = row["ID"].ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(row["Date"], "");
                e.Row.Cells[3].Text = Tool.GetString(row["DepartureTime2"], "");
                e.Row.Cells[4].Text = Tool.ToString((ServiceTypes)Tool.GetInt(row["ServiceType"]));
                e.Row.Cells[5].Text = Tool.GetString(row["SrcCity"], "") + " -> " + Tool.GetString(row["DestCity"], "");
                e.Row.Cells[6].Text = Tool.GetString(row["CarTitle"], "");
                e.Row.Cells[7].Text = Tool.GetString(row["DriverTitle"], "");
                //e.Row.Cells[7].Text = info.Driver != null ? info.Driver.Title : "";
                //string series = Tool.GetString(row["Series"], "");
                if (series != null && series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                    string[] ss = series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                        b.AppendFormat("<td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td>", ss[0], ss[1], ss[2]);
                    b.Append("</tr></table>");
                    e.Row.Cells[9].Text = b.ToString();
                }
                e.Row.Cells[10].Text = Tool.GetInt(row["NumTickets"], 0).ToString();
                e.Row.Cells[8].Text = Tool.GetInt(row["No"], 0).ToString();
                //e.Row.Cells[12].Text = Tool.GetString(row["Series"], "").ToString();
                
                //JsTools.HandleDeleteButton(e);
                e.Row.Cells[e.Row.Cells.Count - 2].Text = "عادی";
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = cc.Controls[0] as LinkButton;
                if (delete != null)
                    delete.Attributes.Add("OnClick", string.Format("$d({0});", row["ID"]));
                #endregion
            }
            Trip info = e.Row.DataItem as Trip;
            if (info != null)
            {
                #region Trip
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(info.Date, "");
                e.Row.Cells[3].Text = info.DepartureTime2;
                e.Row.Cells[4].Text = info.Service.ServiceType2;
                e.Row.Cells[5].Text = info.Service.Path.Title;
                e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(info.CarID);
                e.Row.Cells[7].Text = info.Driver != null ? info.Driver.Title : "";
                e.Row.Cells[8].Text = info.No.ToString();
                if (info.Series != null && info.Series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                    string[] ss = info.Series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            b.AppendFormat("<td>{0}</td>", ss[i]);
                            if (i < 2)
                                b.Append("<td>/</td>");
                        }
                    }
                    b.Append("</tr></table>");
                    e.Row.Cells[9].Text = b.ToString();
                }
                e.Row.Cells[10].Text = Helper.Instance.GetNumTickets(info.ID).ToString();

                //JsTools.HandleDeleteButton(e);
                e.Row.Cells[e.Row.Cells.Count - 2].Text = "عادی";
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = cc.Controls[0] as LinkButton;
                if (delete != null)
                    delete.Attributes.Add("OnClick", string.Format("$d({0});", info.ID));
                #endregion
            }

            Exclusive ex = e.Row.DataItem as Exclusive;
            if (ex != null)
            {
                #region Trip
                e.Row.Cells[0].Text = ex.ID.ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(ex.DepartureDate, "");
                e.Row.Cells[3].Text = ex.DepartureTime;
                e.Row.Cells[4].Text = "";// ex.Service.ServiceType2;
                e.Row.Cells[5].Text = ex.Path != null ? ex.Path.Title : "";
                e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(ex.CarID);
                e.Row.Cells[7].Text = ex.Driver != null ? ex.Driver.Title : "";
                e.Row.Cells[8].Text = ex.No.ToString();
                if (ex.Series != null && ex.Series.Length > 0)
                {
                    e.Row.Cells[9].Text = ex.Series;
                }
                e.Row.Cells[10].Text = ex.NumPassangers.ToString();

                //JsTools.HandleDeleteButton(e);
                e.Row.Cells[e.Row.Cells.Count - 2].Text = "دربستی";
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = cc.Controls[0] as LinkButton;
                if (delete != null)
                    delete.Attributes.Add("OnClick", string.Format("$d({0});", ex.ID));
                #endregion
            }

            InValid inv = e.Row.DataItem as InValid;
            if (inv != null)
            {
                #region Invalid
                e.Row.Cells[0].Text = inv.ID.ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(inv.Date, "");
                
                e.Row.Cells[4].Text = "";// inv.Service.ServiceType2;
                Path path = null;
                if (inv.Exclusive == true)
                {
                    path = Helper.Instance.GetPath(inv.ServiceID);
                    e.Row.Cells[3].Text = "";
                }
                else
                {
                    Service sv1 = Helper.Instance.GetService(inv.ServiceID);
                    path = sv1 != null ? sv1.Path : null;
                    e.Row.Cells[3].Text = sv1 != null ? sv1.DepartureTime : "";
                }
                e.Row.Cells[5].Text = path != null ? path.Title : "";
                e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(inv.CarID);
                e.Row.Cells[7].Text = Helper.Instance.GetDriverFullName(inv.DriverID1);
                e.Row.Cells[8].Text = inv.No.ToString();
                if (inv.Series != null && inv.Series.Length > 0)
                {
                    e.Row.Cells[9].Text = inv.Series;
                }
                e.Row.Cells[10].Text = "0";

                //JsTools.HandleDeleteButton(e);
                e.Row.Cells[e.Row.Cells.Count - 2].Text = "باطل شده";
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = cc.Controls[0] as LinkButton;
                if (delete != null)
                    delete.Attributes.Add("OnClick", string.Format("$d({0});", inv.ID));
                #endregion
            }
        }
    }
    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        string type = Tool.GetString(list.Rows[e.RowIndex].Cells[list.Rows[e.RowIndex].Cells.Count - 2].Text, Null.NullString);
        if (type == "باطل شده")
        {
            if (Helper.Instance.DeleteInValid(id))
            {
                list.SelectedIndex = -1;
                mMsg.Text = "سرویس با موفقیت حذف شد";
                BindData();
            }
            else
            {
                mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
            }
        }
        else if (type == "دربستی")
        {
            if (Helper.Instance.DeleteExclusive(id))
            {
                list.SelectedIndex = -1;
                mMsg.Text = "سرویس با موفقیت حذف شد";
                BindData();
            }
            else
            {
                mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
            }
        }
        else if (type == "عادی")
        {
            ToDeleteID = id;
            puEx.Show();
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void puOk_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == ToDeleteID)
            return;

        Trip mTrip = Helper.Instance.GetTrip(ToDeleteID);
        if (mTrip == null)
            return;
        InValid v = new InValid();
        v.Date = mTrip.Date;
        v.Ticketless = mTrip.Ticketless;
        v.TicketlessPrice = mTrip.TicketlessPrice;
        v.TicketlessName = mTrip.TicketlessName;
        v.Stamp = mTrip.Stamp;
        v.Comission = mTrip.Comission;
        v.BodyInsurance = mTrip.BodyInsurance;
        v.ExtraCost = mTrip.ExtraCost;
        v.Closed = mTrip.Closed;
        v.Price = mTrip.Price;
        v.Toll = mTrip.Toll;
        v.Insurance = mTrip.Insurance;
        v.Reception = mTrip.Reception;
        v.TotalPrice = mTrip.TotalPrice;
        v.Comission2 = mTrip.Comission2;
        v.OtherDeficits = mTrip.OtherDeficits;
        v.SumDeficits = mTrip.SumDeficits;
        v.AutoShare = mTrip.AutoShare;
        v.Toll2 = mTrip.Toll2;
        v.Locked = mTrip.Locked;
        v.Others = mTrip.Others;
        v.TripID = mTrip.ID;
        v.No = mTrip.No;
        v.Series = mTrip.Series;
        v.CarID = mTrip.CarID;
        v.DriverID1 = mTrip.DriverID1;
        v.DriverID2 = mTrip.DriverID2;
        v.DriverID3 = mTrip.DriverID3;
        v.ServiceID = mTrip.ServiceID;
        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        Helper.Instance.Update();

        mMsg.Text = "شماره صورت وضعیت باطل شد - ";


        if (Helper.Instance.DeleteTrip(ToDeleteID))
        {
            list.SelectedIndex = -1;
            BindData();
            mMsg.Text += "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text += "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
        ToDeleteID = Null.NullInteger;
    }
    protected void puNo_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == ToDeleteID)
            return;
        if (Helper.Instance.DeleteTrip(ToDeleteID))
        {
            list.SelectedIndex = -1;
            BindData();
            mMsg.Text = "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
        ToDeleteID = Null.NullInteger;
    }
    int ToDeleteID
    {
        get
        {
            return Tool.GetInt(mIDToDelete.Value, Null.NullInteger);
            return Tool.GetInt(ViewState["ToDeleteID"], Null.NullInteger);
        }
        set
        {
            mIDToDelete.Value = value.ToString();
            ViewState["ToDeleteID"] = value;
        }
    }
    //string ToDeleteType
    //{
    //    get
    //    {
    //        return Tool.GetString(mToDeleteType.Value, Null.NullString);
    //        return Tool.GetInt(ViewState["ToDeleteType"], Null.NullString);
    //    }
    //    set
    //    {
    //        mToDeleteType.Value = value.ToString();
    //        ViewState["ToDeleteType"] = value;
    //    }
    //}
}
