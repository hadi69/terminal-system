﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Funds_Sellers : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
            BindData();
    }

    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Sellers
                  select c;
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Seller info;
        if (Null.NullInteger == EditID)
            info = new Seller();
        else
            info = Helper.Instance.GetSeller(EditID);
        if (info == null)
            return;

        info.Title = mTitle.Text;
        info.CnnStr = mCnnStr.Text;
        info.Enabled = mEnabled.Checked;

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Sellers.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void testCnt_Click(object sender, EventArgs e)
    {
        string cntStr = mCnnStr.Text;
        string qry = "SELECT TOP 1 * FROM Tickets";
        Helper.Instance.LastException = null;
        DataTable dt = Helper.Instance.FillDataTable(qry, cntStr);
        if (Helper.Instance.LastException != null)
        {
            mError.Text = "خطا" + Helper.Instance.LastException.Message;
        }
        else
        {
            mError.Text = "تست با موفقیت انجام شد" ;
        }
        puEx.Show();
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Seller info = e.Row.DataItem as Seller;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Seller info = Helper.Instance.GetSeller(EditID);
        if (info == null)
            return;
        mCnnStr.Text = info.CnnStr;
        mTitle.Text = info.Title;
        mEnabled.Checked = info.Enabled;

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteSeller(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mTitle.Text = mCnnStr.Text = "";
        mEnabled.Checked = true;
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
