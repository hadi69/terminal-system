﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Advance.aspx.cs" Inherits="Funds_Advance" Title="مساعده ماشين" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        صندوق
                    </td>
                    <td>
                        <asp:DropDownList ID="sFundID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        ماشين
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        مبلغ از
                    </td>
                    <td>
                        <asp:TextBox ID="sAmount1" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        مبلغ
                    </td>
                    <td>
                        <asp:TextBox ID="sAmount2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="FundID" HeaderText="صندوق" />
                    <asp:BoundField DataField="CarID" HeaderText="ماشين" />
                    <asp:BoundField DataField="Amount" HeaderText="مبلغ" />
                    <asp:BoundField DataField="Comments" HeaderText="شرح" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate><br /><span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate><HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش مساعده ماشين</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                صندوق
                            </td>
                            <td>
                                <asp:DropDownList ID="mFundID" runat="server" CssClass="DD" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="صندوق الزامي است" ControlToValidate="mFundID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ماشين
                            </td>
                            <td>
                                <asp:DropDownList ID="mCarID" runat="server" CssClass="DD" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rff1" runat="server" ValidationGroup="addGroup" ErrorMessage="ماشين الزامي است"
                                    ControlToValidate="mCarID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مبلغ
                            </td>
                            <td>
                                <asp:TextBox ID="mAmount" runat="server" CssClass="T" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="مبلغ الزامي است"
                                    ControlToValidate="mAmount" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mAmount" />
                                <asp:RangeValidator ID="RangeValidator1" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mAmount" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ
                            </td>
                            <td>
                                <asp:TextBox ID="mDate" runat="server" CssClass="T" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvr2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="تاريخ الزامي است" ControlToValidate="mDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                     <cc1:MaskedEditExtender ID="MaskedEditExtender1" Mask="1399/99/99" ClearMaskOnLostFocus="false" runat="server"
                                    TargetControlID="mDate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شرح
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" CssClass="T" Width="300" TextMode="MultiLine"
                                    Height="100" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOK_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
