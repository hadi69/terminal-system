﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Funds_PayGet : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        List<Fund> all = Helper.Instance.GetFunds();
        sFundID.DataValueField = mFundID.DataValueField = "ID";
        sFundID.DataTextField = mFundID.DataTextField = "Title";
        mFundID.DataSource = all;
        mFundID.DataBind();

        Fund none = new Fund();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);
        sFundID.DataSource = all;
        sFundID.DataBind();

        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Payments
                  select c;

        if (sFundID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.FundID == sID);
        }
        int amount = Tool.GetInt(sAmount1.Text, Null.NullInteger);
        if (amount != Null.NullInteger)
            all = all.Where(c => c.Amount >= amount);
        amount = Tool.GetInt(sAmount2.Text, Null.NullInteger);
        if (amount != Null.NullInteger)
            all = all.Where(c => c.Amount <= amount);

        if ((sPay.Checked && sGet.Checked)
            || (!sPay.Checked && !sGet.Checked))
            all = all.Where(c => c.Type == (int)PaymentTypes.Pay || c.Type == (int)PaymentTypes.Get);
        else if (sPay.Checked)
            all = all.Where(c => c.Type == (int)PaymentTypes.Pay);
        else
            all = all.Where(c => c.Type == (int)PaymentTypes.Get);

        DateTime fromDate = sDateStart.SelectedDate;
        if (fromDate != new DateTime())
            all = all.Where(c => c.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;
        if (toDate != new DateTime())
            all = all.Where(c => c.Date <= toDate);

        list.DataSource = all;
        list.DataBind();
    }
   
    protected void puOK_Click(object sender, EventArgs e)
    {
        Payment info;
        if (Null.NullInteger == EditID)
            info = new Payment();
        else
            info = Helper.Instance.GetPayment(EditID);
        if (info == null)
            return;

        if (!Tool.ValidatePersianDate(mDate.Text))
        {
            mError.Text = "تاريخ اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        info.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        info.Amount = Tool.GetInt(mAmount.Text, 0);
        info.Comments = mComments.Text;
        info.Type = mGet.Checked ? (short)PaymentTypes.Get : (short)PaymentTypes.Pay; 
        info.Date = Tool.ParsePersianDate(mDate.Text, DateTime.Now);

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Payments.InsertOnSubmit(info);

        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Payment info = e.Row.DataItem as Payment;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Helper.Instance.GetFundTitle(info.FundID);
                e.Row.Cells[3].Text = Tool.ToPersianDate(info.Date, "");
                e.Row.Cells[5].Text = Tool.ToString((PaymentTypes)info.Type);

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Payment info = Helper.Instance.GetPayment(EditID);
        if (info == null)
            return;

        Tool.SetSelected(mFundID, info.FundID);
        mAmount.Text = info.Amount.ToString();
        mComments.Text = info.Comments;
        mGet.Checked = info.Type == (short)PaymentTypes.Get;
        mPay.Checked = !mGet.Checked;
        mDate.Text = Tool.ToPersianDate(info.Date, "");

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeletePayment(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mComments.Text = mAmount.Text = "";
        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");

        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void doPrint_Click(object sender, EventArgs e)
    {
        string trips = "0";
        for (int i = 0; i < list.Rows.Count; i++)
        {
            if (trips.Length > 0)
                trips = trips + ",";
            trips = trips + "'" + list.Rows[i].Cells[0].Text + "'";
        }
        string fundName = "همه صندوقها";

        if (sFundID.SelectedIndex > 0)
        {
            Fund fnd = Helper.Instance.GetFund(int.Parse(sFundID.SelectedValue));
            fundName = fnd.Title;
        }
        string repParam = "از تاریخ:" + Tool.ToPersianDateRtl(sDateStart.SelectedDate, "?") + " تا تاریخ: " + Tool.ToPersianDateRtl(sDateEnd.SelectedDate, "?");
        Response.Redirect(string.Format("../Reports/ReportSale.aspx?tripid={0}&userid={1}&repname={2}&repTitle={3}&fullName={4}&repParam={5}", trips,"0", "PayGet", "گزارش دريافت و پرداخت", fundName, repParam), true);

    }
}
