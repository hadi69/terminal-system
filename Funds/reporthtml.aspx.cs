﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class Funds_reporthtml : System.Web.UI.Page
{
    // sellerid=-1&fromDate=9/30/2011&toDate=9/30/2011&series=&no=&no2=&type=0
    int sellerid, no, no2, type;
    string fromDate, toDate, series, dest;
    const int rowsPerPage = 28;
    protected void Page_Load(object sender, EventArgs e)
    {
        sellerid = Tool.GetInt(Request.QueryString["sellerid"], -1);
        fromDate = Tool.GetString(Request.QueryString["fromDate"], "");
        toDate = Tool.GetString(Request.QueryString["toDate"], "");
        series = Tool.GetString(Request.QueryString["series"], "");
        dest = Tool.GetString(Request.QueryString["dest"], "");
        no = Tool.GetInt(Request.QueryString["no"], 0);
        no2 = Tool.GetInt(Request.QueryString["no2"], 0);
        type = Tool.GetInt(Request.QueryString["type"], 0);
        if (!string.IsNullOrEmpty(toDate))
        {

            try
            {
                toDate = Convert.ToDateTime(toDate).AddDays(1).ToShortDateString();
            }
            catch { }
        }
        Helper.Instance.Init();
        Helper.Instance.LastException = null;
        if (type == 0)
            BindTrips();
        else
            BindTickets();

        Helper.Instance.Dispose();

    }
    private void BindTrips()
    {
        List<Seller> all = null;
        if (sellerid > 0)
        {
            Seller seller = Helper.Instance.GetSeller(sellerid);
            all = new List<Seller>();
            if (seller != null)
                all.Add(seller);
        }
        else
            all = Helper.Instance.GetSellers();
        if (all == null || all.Count == 0)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("هیچ سرویسی یافت نشد"));
            return;
        }

        #region Build query
        string query = string.Format(@"SELECT N'_SELLER_' AS Seller, _SELLERID_ AS SellerID, Trips.*
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
	, Services.ServiceType
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
	, Layouts.NumChairs
    , Trips.Comission2, Trips.AutoShare, Trips.Others, Trips.TotalPrice, Trips.Series
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
LEFT JOIN Cars ON Trips.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
WHERE (Trips.Archive IS NULL OR Trips.Archive=0) AND Trips.Date>= '{0}' AND Trips.Date<='{1} 23:59:59'
"
            , fromDate, toDate);

        if (no > 0)
            query += " AND Trips.No>=" + no;
        if (no2 > 0)
            query += " AND Trips.No<=" + no2;
        #endregion
        DataTable table = null;
        #region fill table
        for (int s = 0; s < all.Count; s++)
        {
            string qry = query.Replace("_SELLER_", all[s].Title.Replace("'", "''")).Replace("_SELLERID_", all[s].ID.ToString());
            Helper.Instance.LastException = null;
            DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Services.ServiceType", all[s].CnnStr);
            if (dt == null)
            {
                if (Helper.Instance.LastException != null)
                    mPlaceHolder.Controls.Add(new LiteralControl("Error for ` " + all[s].Title + "` : " + Helper.Instance.LastException.Message + "<br />"));
                continue;
            }
            if (dt == null || dt.Rows.Count == 0)
                continue;

            #region Filter
            if (series.Trim().Length > 0)
            {
                string str = Tool.ParseSeries(series.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series2 = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series2.Contains(str))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            #endregion

            if (table == null)
                table = dt;
            else
            {
                while (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    DataRow r2 = table.NewRow();
                    for (int jjj = 0; jjj < dt.Columns.Count; jjj++)
                    {
                        r2[jjj] = row[jjj];
                    }
                    dt.Rows.RemoveAt(0);
                    table.Rows.Add(r2);
                }
            }
        }
        #endregion

        try
        {
            StringBuilder builder = new StringBuilder();
            int i = 0, page = 1;
            bool closed = true;
            int sumAutoShare = 0, sumComission2 = 0, sumOthers = 0, sumTotalPrice = 0, sumTickets = 0;
            for (; i < table.Rows.Count; i++)
            {
                if (i % rowsPerPage == 0)
                {
                    if (builder.Length > 0)
                    {
                        // Close Table
                        builder.AppendFormat("<tr class=rowHead><td colspan=10 style='tex-align:left'>جمع</td><td align='middle'>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>\r\n"
                            , sumTickets, sumAutoShare, sumComission2, sumOthers, sumTotalPrice);
                        builder.AppendLine("</tbody></table>");
                        closed = true;
                    }
                    // Start New
                    closed = false;
                    if (table.Rows.Count - i <= rowsPerPage)
                        builder.AppendLine("<table width='580' align='center' cellspacing=0 border=0><tbody>");
                    else
                        builder.AppendLine("<table style='page-break-after: always' width='580' align='center' cellspacing=0 border=0><tbody>");
                    builder.AppendLine("<tr><td colspan='15'>&nbsp;</td></tr>");
                    builder.AppendFormat("<tr><td colspan='15'><div align='left'><strong>{0}</strong></div></td></tr>", Helper.Instance.GetSettingValue("CompanyName"));
                    builder.AppendFormat("<tr><td dir='rtl' align='middle' colspan='15'>صفحه {0}</td></tr>", page++);
                    builder.Append("<tr class=rowHead><td>رديف</td><td>تعاونی</td><td>تاریخ</td><td>ساعت</td><td>نوع سرويس</td><td>مسير</td><td>ماشين</td><td>راننده1</td><td>شماره صورت</td><td>سري صورت </td><td>ت بليط</td><td>پرداختی به راننده</td><td>کمیسیون</td><td>متفرقه</td><td>باقیمانده</td></tr>");
                }
                builder.AppendFormat("<tr dir='rtl'  class=rowNoLine><td  nowrap>{0}</td><td  nowrap>{1}</td><td  nowrap>{2}</td><td  nowrap>{3}</td><td  nowrap>{4}</td><td  nowrap>{5}</td><td  nowrap dir=rtl>{6}</td><td  nowrap>{7}</td><td  nowrap>{8}</td><td  nowrap>{9}</td><td  nowrap>{10}</td><td  nowrap>{11}</td><td  nowrap>{12}</td><td  nowrap>{13}</td><td  nowrap>{14}</td></tr>\r\n"
                    , i + 1
                    , table.Rows[i]["Seller"]
                    , Tool.ToPersianDate(table.Rows[i]["Date"], "")
                    , table.Rows[i]["DepartureTime2"]
                    , Tool.ToString((ServiceTypes)(int)(table.Rows[i]["ServiceType"]))
                    , table.Rows[i]["SrcCity"]
                    , table.Rows[i]["CarTitle"]
                    , table.Rows[i]["DriverTitle"]
                    , table.Rows[i]["No"]
                    , ShowSeries(Tool.GetString(table.Rows[i]["Series"], ""))
                    , table.Rows[i]["NumTickets"]
                    , table.Rows[i]["AutoShare"]
                    , table.Rows[i]["Comission2"]
                    , Tool.GetInt(table.Rows[i]["Others"], 0)
                    , Tool.GetInt(table.Rows[i]["TotalPrice"], 0));
                sumAutoShare += Tool.GetInt(table.Rows[i]["AutoShare"], 0);
                sumComission2 += Tool.GetInt(table.Rows[i]["Comission2"], 0);
                sumOthers += Tool.GetInt(table.Rows[i]["Others"], 0);
                sumTotalPrice += Tool.GetInt(table.Rows[i]["TotalPrice"], 0);
                sumTickets += Tool.GetInt(table.Rows[i]["NumTickets"], 0);
            }
            if (!closed)
            {
                while (i++ % rowsPerPage != 0)
                    builder.Append(" <tr dir=rtl><td  align=middle colSpan=15>&nbsp;</td></tr>");
                // Close Table
                builder.AppendFormat("<tr class=rowHead><td colspan=10 style='tex-align:left'>جمع</td><td align='middle'>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>\r\n"
                            , sumTickets, sumAutoShare, sumComission2, sumOthers, sumTotalPrice);
                builder.AppendLine("</tbody></table>");
            }
            mPlaceHolder.Controls.Clear();
            mPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + ex.Message + "<br />StackTrace: " + ex.StackTrace));
        }

    }
    private void BindTickets()
    {
        List<Seller> all = null;
        if (sellerid > 0)
        {
            Seller seller = Helper.Instance.GetSeller(sellerid);
            all = new List<Seller>();
            if (seller != null)
                all.Add(seller);
        }
        else
            all = Helper.Instance.GetSellers();
        if (all == null || all.Count == 0)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("هیچ سرویسی یافت نشد"));
            return;
        }
        DataTable table = null;
        #region Fill table
        for (int s = 0; s < all.Count; s++)
        {
            if (!all[s].Enabled)
                continue;
            #region Build query
            string qry = string.Format(@"SELECT Tickets.ID, N'{0}' AS Seller, {1} AS SellerID, Tickets.No, Tickets.Chairs, Tickets.Fullname, Tickets.Tel, Tickets.Price
, Tickets.NumChairs,  ((Tickets.Price - Discount) * Tickets.NumChairs) AS Total
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
, Branches.Title AS Branch
, Users.FullName AS  SaleUser, SaleDate, SaleType, Trips.Series
FROM Tickets 
INNER JOIN Trips ON Tickets.TripID=Trips.ID
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Branches ON Tickets.BranchID=Branches.ID
LEFT JOIN Users ON Tickets.SaleUserID=Users.ID
WHERE Tickets.SaleDate>= '{2}' AND Trips.Date<='{3} 23:59:59'"
                , all[s].Title.Replace("'", "''"), all[s].ID.ToString()
                , fromDate, toDate);
            if (no > 0)
                qry += " AND Trips.No>=" + no;
            if (no2 > 0)
                qry += " AND Trips.No<=" + no2;
            #endregion

            Helper.Instance.LastException = null;
            DataTable dt = Helper.Instance.FillDataTable(qry, all[s].CnnStr);
            if (dt == null)
            {
                if (Helper.Instance.LastException != null)
                    mPlaceHolder.Controls.Add(new LiteralControl("Error for ` " + all[s].Title + "` : " + Helper.Instance.LastException.Message + "<br />"));
                continue;
            }
            if (dt == null || dt.Rows.Count == 0)
                continue;
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                if (series.Trim().Length > 0)
                {
                    string str = Tool.ParseSeries(series.Trim());
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string series2 = Tool.GetString(dt.Rows[i]["Series"], "");
                        if (!series2.Contains(str))
                        {
                            dt.Rows.RemoveAt(i);
                            i--;
                        }
                    }
                }
                if (dest.Trim().Length > 0)
                    if (!Tool.GetString(dt.Rows[j]["DestCity"], "").Contains(dest))
                    {
                        dt.Rows.RemoveAt(j);
                        j--;
                        continue;
                    }
                string chairs = Tool.GetString(dt.Rows[j]["Chairs"], "");
                dt.Rows[j]["Chairs"] = chairs == null ? "" : chairs.Replace("f", "").Replace("m", "").Replace(";", "-").Trim('-');

                SaleTypes st = (SaleTypes)Tool.GetInt(dt.Rows[j]["SaleType"], (int)SaleTypes.OfficeSale);
                if (st == SaleTypes.Reserve)
                {
                    dt.Rows[j]["Price"] = dt.Rows[j]["Total"] = 0;
                    dt.Rows[j]["Branch"] = "رزروی";
                }
                //dt.Rows[j]["SaleDate"] = Tool.ToPersianDate(Tool.GetDate(dt.Rows[j]["SaleDate"]), "");

                //rows.Add(dt.Rows[j]);
            }
            if (table == null)
                table = dt;
            else
            {
                while (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    DataRow r2 = table.NewRow();
                    for (int jjj = 0; jjj < dt.Columns.Count; jjj++)
                    {
                        r2[jjj] = row[jjj];
                    }
                    dt.Rows.RemoveAt(0);
                    //table.ImportRow(row);
                    table.Rows.Add(r2);
                }
            }
        }
        #endregion

        try
        {
            StringBuilder builder = new StringBuilder();
            int i = 0, page = 1;
            bool closed = true;
            int sumPrice = 0, sumNumChairs = 0, sumTotal = 0;
            for (; i < table.Rows.Count; i++)
            {
                if (i % rowsPerPage == 0)
                {
                    if (builder.Length > 0)
                    {
                        // Close Table
                        builder.AppendFormat("<tr class=rowHead><td colspan=6 style='tex-align:left'>جمع</td><td align='middle'>{0}</td><td>{1}</td><td>{2}</td><td colspan=5>&nbsp;</td></tr>\r\n"
                            , sumPrice, sumNumChairs, sumTotal);
                        builder.AppendLine("</tbody></table>");
                        closed = true;
                    }
                    // Start New
                    closed = false;
                    if (table.Rows.Count - i <= rowsPerPage)
                        builder.AppendLine("<table width='580' align='center' cellspacing=0 border=0><tbody>");
                    else
                        builder.AppendLine("<table style='page-break-after: always' width='580' align='center' cellspacing=0 border=0><tbody>");
                    builder.AppendLine("<tr><td colspan='15'>&nbsp;</td></tr>");
                    builder.AppendFormat("<tr><td colspan='15'><div align='left'><strong>{0}</strong></div></td></tr>", Helper.Instance.GetSettingValue("CompanyName"));
                    builder.AppendFormat("<tr><td dir='rtl' align='middle' colspan='15'>صفحه {0}</td></tr>", page++);
                    builder.Append("<tr class=rowHead><td>رديف</td><td>تعاونی</td><td>شماره بليط</td><td>صندلیها</td><td>نام</td><td>تلفن</td><td>کرايه</td><td>تعداد</td><td>جمع</td><td>مبدا </td><td>مقصد</td><td>دفتر</td><td>فروشنده</td><td>ساعت</td></tr>");
                }
                builder.AppendFormat("<tr dir='rtl'  class=rowNoLine><td  nowrap>{0}</td><td  nowrap>{1}</td><td  nowrap>{2}</td><td  nowrap>{3}</td><td  nowrap>{4}</td><td  nowrap>{5}</td><td  nowrap dir=rtl>{6}</td><td  nowrap>{7}</td><td  nowrap>{8}</td><td  nowrap>{9}</td><td  nowrap>{10}</td><td  nowrap>{11}</td><td  nowrap>{12}</td><td  nowrap>{13}</td></tr>\r\n"
                    , i + 1
                    , table.Rows[i]["Seller"]
                    , table.Rows[i]["No"]
                    , table.Rows[i]["Chairs"]
                    , table.Rows[i]["Fullname"]
                    , table.Rows[i]["Tel"]
                    , table.Rows[i]["Price"]
                    , table.Rows[i]["NumChairs"]
                    , table.Rows[i]["Total"]
                    , table.Rows[i]["SrcCity"]
                    , table.Rows[i]["DestCity"]
                    , table.Rows[i]["Branch"]
                    , table.Rows[i]["SaleUser"]
                    , GetTime(table.Rows[i]["SaleDate"]));
                sumPrice += Tool.GetInt(table.Rows[i]["Price"], 0);
                sumNumChairs += Tool.GetInt(table.Rows[i]["NumChairs"], 0);
                sumTotal += Tool.GetInt(table.Rows[i]["Total"], 0);
            }
            if (!closed)
            {
                while (i++ % rowsPerPage != 0)
                    builder.Append(" <tr dir=rtl><td  align=middle colSpan=15>&nbsp;</td></tr>");
                // Close Table
                builder.AppendFormat("<tr class=rowHead><td colspan=6 style='tex-align:left'>جمع</td><td align='middle'>{0}</td><td>{1}</td><td>{2}</td><td colspan=5>&nbsp;</td></tr>\r\n"
                            , sumPrice, sumNumChairs, sumTotal);
                builder.AppendLine("</tbody></table>");
            }
            mPlaceHolder.Controls.Clear();
            mPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + ex.Message + "<br />StackTrace: " + ex.StackTrace));
        }
    }
    string ShowSeries(string ser)
    {
        string[] sp = ser.Split(',');
        if (sp.Length == 3)
            return string.Format("<table border=0 cellpadding='0' cellspacing='0' dir=rtl class=tt><tr><td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td></tr></table>"
                , sp[0], sp[1], sp[2]);
        //return sp[0] + "/" + sp[1] + "/" + sp[2];
        if (sp.Length == 4)
            return string.Format("<table border=0 cellpadding='0' cellspacing='0' dir=rtl class=tt><tr><td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td><td>/</td><td>{3}</td></tr></table>"
               , sp[3], sp[0], sp[1], sp[2]);
        //return sp[0] + "/" + sp[1] + "/" + sp[2] + "/" + sp[3];
        //return sp[1] + "/" + sp[0] + "/" + sp[2] + "/" + sp[3];
        return ser;
    }
    string GetTime(object obj)
    {
        DateTime dt = Tool.GetDate(obj);
        if (dt != Null.NullDate)
            return string.Format("{0}:{1}"
                   , dt.Hour.ToString().PadLeft(2, '0')
                   , dt.Minute.ToString().PadLeft(2, '0'));
            return "";
    }
}
