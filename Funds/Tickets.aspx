﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Tickets.aspx.cs" Inherits="Funds_Tickets" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        تعاونی
                    </td>
                    <td>
                        <asp:DropDownList ID="sSellers" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" width="150px" />
                    </td>
                    <td>
                        مبدا
                    </td>
                    <td>
                        <asp:TextBox ID="sSrc" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:TextBox ID="sDest" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sNew" Text='جدید' Checked="true" /><asp:CheckBox
                            runat="server" ID="sArchive" Text='آرشیو' />
                    </td>
                    <td>
                        <asp:Button ID="doRefresh" runat="server" Text="جستجو" OnClick="doRefresh_Click"
                            CssClass="CB" Width="50px"></asp:Button>
                    </td>
                    <td>
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Width="100%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Seller" HeaderText="تعاونی" />
                    <asp:BoundField DataField="No" HeaderText="شماره بليط" />
                    <asp:BoundField DataField="Chairs" HeaderText="صندلیها" />
                    <asp:BoundField DataField="Fullname" HeaderText="نام" />
                    <asp:BoundField DataField="Tel" HeaderText="تلفن" />
                    <asp:BoundField DataField="Price" HeaderText="کرايه" />
                    <asp:BoundField DataField="NumChairs" HeaderText="تعداد" />
                    <asp:BoundField DataField="Total" HeaderText="جمع" />
                    <asp:BoundField DataField="SrcCity" HeaderText="مبدا" />
                    <asp:BoundField DataField="DestCity" HeaderText="مقصد" />
                    <asp:BoundField DataField="Branch" HeaderText="دفتر" />
                    <asp:BoundField DataField="SaleUser" HeaderText="فروشنده" />
                    <asp:BoundField DataField="SaleDate" HeaderText="ساعت" />
                    <asp:HyperLinkField HeaderText="چاپ" DataNavigateUrlFields="ID, SellerID" DataNavigateUrlFormatString="../Reports/TicketAll.aspx?id={0}&sellerid={1}"
                        Text="چاپ" Target="_blank" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
