﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class Funds_Report : System.Web.UI.Page
{
    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        msg = DateTime.Now.ToString();
        DateTime dt1 = DateTime.Now;
        if (!IsPostBack)
        {
            BindInitData();
            doSearch_Click(null, null);
        }
    }
    void BindInitData()
    {
        List<Seller> all = Helper.Instance.GetSellers(true);
        if (all == null)
            all = new List<Seller>();
        Seller none = new Seller();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);
        sSellers.DataValueField = "ID";
        sSellers.DataTextField = "Title";
        sSellers.DataSource = all;
        sSellers.DataBind();

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["FundCloseds.sDateStart"]), DateTime.Now);
        sDateEnd.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["FundCloseds.sDateEnd"]), DateTime.Now);
    }

    private void BindTrips()
    {
        List<Seller> all = null;
        if (sSellers.SelectedIndex > 0)
        {
            int sellerID = Tool.GetInt(sSellers.SelectedValue, Null.NullInteger);
            Seller seller = Helper.Instance.GetSeller(sellerID);
            all = new List<Seller>();
            if (seller != null)
                all.Add(seller);
        }
        else
            all = Helper.Instance.GetSellers();
        if (all == null || all.Count == 0)
        {
            list.DataSource = null;
            list.DataBind();
            return;
        }

        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);

        #region Build query
        string query = string.Format(@"SELECT N'_SELLER_' AS Seller, _SELLERID_ AS SellerID, Trips.*
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
	, Services.ServiceType
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
	, Layouts.NumChairs
    , Trips.Comission2, Trips.AutoShare, Trips.Others, Trips.TotalPrice, Trips.Series
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
LEFT JOIN Cars ON Trips.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
WHERE (Trips.Archive IS NULL OR Trips.Archive=0) AND Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59'
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);


        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            query += " AND Trips.No>=" + sID;
        }
        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            query += " AND Trips.No<=" + sID;
        }
        #endregion
        DataTable table = null;
        for (int s = 0; s < all.Count; s++)
        {
            string qry = query.Replace("_SELLER_", all[s].Title.Replace("'", "''")).Replace("_SELLERID_", all[s].ID.ToString());
            Helper.Instance.LastException = null;
            DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Services.ServiceType", all[s].CnnStr);
            if (dt == null)
            {
                if (Helper.Instance.LastException != null)
                    mMsg.Text += "Error for ` " + all[s].Title + "` : " + Helper.Instance.LastException.Message + "<br />";
                continue;
            }
            if (dt == null || dt.Rows.Count == 0)
                continue;

            #region Filter
            if (sSeries.Text.Trim().Length > 0)
            {
                string str = Tool.ParseSeries(sSeries.Text.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series.Contains(str))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            
            if (sDest.Text.Trim().Length > 0)
                for (int i = 0; i < dt.Rows.Count; i++)
                    if (!Tool.GetString(dt.Rows[i]["DestCity"], "").Contains(sDest.Text))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
            #endregion

            if (table == null)
                table = dt;
            else
            {
                while (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    DataRow r2 = table.NewRow();
                    for (int jjj = 0; jjj < dt.Columns.Count; jjj++)
                    {
                        r2[jjj] = row[jjj];
                    }
                    dt.Rows.RemoveAt(0);
                    //table.ImportRow(row);
                    table.Rows.Add(r2);
                }
            }
        }

        Session["FundCloseds.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["FundCloseds.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        list.DataSource = table;
        list.DataBind();
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRowView row = e.Row.DataItem as DataRowView;
            if (row != null)
            {
                e.Row.Cells[0].Text = row["ID"].ToString();
                e.Row.Cells[3].Text = Tool.ToPersianDate(row["Date"], "");
                e.Row.Cells[5].Text = Tool.ToString((ServiceTypes)Tool.GetInt(row["ServiceType"]));
                e.Row.Cells[6].Text = Tool.GetString(row["SrcCity"], "") + " -> " + Tool.GetString(row["DestCity"], "");
                string series = Tool.GetString(row["Series"], "");
                if (series != null && series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                    string[] ss = series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                        b.AppendFormat("<td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td>", ss[0], ss[1], ss[2]);
                    b.Append("</tr></table>");
                    e.Row.Cells[10].Text = b.ToString();
                }
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        #region printBimeQry.Value
        {
            DateTime startDate = sDateStart.SelectedDate;
            DateTime toDate = sDateEnd.SelectedDate;
            if (startDate == Null.NullDate)
                startDate = DateTime.Now.AddYears(-2);
            if (toDate == Null.NullDate)
                toDate = DateTime.Now.AddYears(2);

            string fDate = startDate.ToShortDateString(); ;
            string tDate = toDate.ToShortDateString();
            string series = "";
            if (sSeries.Text.Trim().Length > 0)
                series = Tool.ParseSeries(sSeries.Text);
            string no = "", no2 = "";
            if (sNo.Text.Trim().Length > 0)
                no = sNo.Text;
            if (sNo2.Text.Trim().Length > 0)
                no2 = sNo2.Text;
            prinQry.Value = string.Format("sellerid={0}&fromDate={1}&toDate={2}&series={3}&no={4}&no2={5}&type={6}",
                sSellers.SelectedValue, fDate, tDate, series, no, no2, mTrips.Checked ? 0 : 1);
        }
        #endregion
        list.Visible = tList.Visible = false;
        list.DataSource = tList.DataSource = null;
        list.DataBind();
        tList.DataBind();
        if (mTrips.Checked)
        {
            list.Visible = true;
            BindTrips();
        }
        else
        {
            tList.Visible = true;
            BindTickets();
        }
    }

    private void BindTickets()
    {
        List<Seller> all = null;
        if (sSellers.SelectedIndex > 0)
        {
            int sellerID = Tool.GetInt(sSellers.SelectedValue, Null.NullInteger);
            Seller seller = Helper.Instance.GetSeller(sellerID);
            all = new List<Seller>();
            if (seller != null)
                all.Add(seller);
        }
        else
            all = Helper.Instance.GetSellers();
        if (all == null || all.Count == 0)
        {
            tList.DataSource = null;
            tList.DataBind();
            return;
        }
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        DataTable table = null;
        for (int s = 0; s < all.Count; s++)
        {
            if (!all[s].Enabled)
                continue;
            #region Build query
            string qry = string.Format(@"SELECT Tickets.ID, N'{0}' AS Seller, {1} AS SellerID, Tickets.No, Tickets.Chairs, Tickets.Fullname, Tickets.Tel, Tickets.Price
, Tickets.NumChairs,  ((Tickets.Price - Discount) * Tickets.NumChairs) AS Total
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
, Branches.Title AS Branch
, Users.FullName AS  SaleUser, SaleDate, SaleType, Trips.Series
FROM Tickets 
INNER JOIN Trips ON Tickets.TripID=Trips.ID
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Branches ON Tickets.BranchID=Branches.ID
LEFT JOIN Users ON Tickets.SaleUserID=Users.ID
WHERE Tickets.SaleDate>= '{2}/{3}/{4}' AND Trips.Date<='{5}/{6}/{7} 23:59:59'"
                , all[s].Title.Replace("'", "''"), all[s].ID.ToString()
                , startDate.Year, startDate.Month, startDate.Day
                , toDate.Year, toDate.Month, toDate.Day);
            if (sNo.Text.Trim().Length > 0)
            {
                int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
                qry += " AND Trips.No>=" + sID;
            }
            if (sNo2.Text.Trim().Length > 0)
            {
                int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
                qry += " AND Trips.No<=" + sID;
            }
        #endregion

            Helper.Instance.LastException = null;
            DataTable dt = Helper.Instance.FillDataTable(qry, all[s].CnnStr);
            if (dt == null)
            {
                if (Helper.Instance.LastException != null)
                    mMsg.Text += "Error for ` " + all[s].Title + "` : " + Helper.Instance.LastException.Message + "<br />";
                continue;
            }
            if (dt == null || dt.Rows.Count == 0)
                continue;
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                if (sSeries.Text.Trim().Length > 0)
                {
                    string str = Tool.ParseSeries(sSeries.Text.Trim());
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string series = Tool.GetString(dt.Rows[i]["Series"], "");
                        if (!series.Contains(str))
                        {
                            dt.Rows.RemoveAt(i);
                            i--;
                        }
                    }
                }
                if (sDest.Text.Trim().Length > 0)
                    if (!Tool.GetString(dt.Rows[j]["DestCity"], "").Contains(sDest.Text))
                    {
                        dt.Rows.RemoveAt(j);
                        j--;
                        continue;
                    }
                string chairs = Tool.GetString(dt.Rows[j]["Chairs"], "");
                dt.Rows[j]["Chairs"] = chairs == null ? "" : chairs.Replace("f", "").Replace("m", "").Replace(";", "-").Trim('-');

                SaleTypes st = (SaleTypes)Tool.GetInt(dt.Rows[j]["SaleType"], (int)SaleTypes.OfficeSale);
                if (st == SaleTypes.Reserve)
                {
                    dt.Rows[j]["Price"] = dt.Rows[j]["Total"] = 0;
                    dt.Rows[j]["Branch"] = "رزروی";
                }
                //dt.Rows[j]["SaleDate"] = Tool.ToPersianDate(Tool.GetDate(dt.Rows[j]["SaleDate"]), "");

                //rows.Add(dt.Rows[j]);
            }
            if (table == null)
                table = dt;
            else
            {
                while (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    DataRow r2 = table.NewRow();
                    for (int jjj = 0; jjj < dt.Columns.Count; jjj++)
                    {
                        r2[jjj] = row[jjj];
                    }
                    dt.Rows.RemoveAt(0);
                    //table.ImportRow(row);
                    table.Rows.Add(r2);
                }
            }
        }
        tList.DataSource = table;
        tList.DataBind();
    }
    protected void tList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView info = e.Row.DataItem as DataRowView;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info["ID"].ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                DateTime dt = Tool.GetDate(info["SaleDate"]);
                if (dt != Null.NullDate)
                    e.Row.Cells[14].Text = string.Format("{0}:{1}"
                           , dt.Hour.ToString().PadLeft(2, '0')
                           , dt.Minute.ToString().PadLeft(2, '0'));
                else
                    e.Row.Cells[14].Text = "";
            }
        }
    }
}
