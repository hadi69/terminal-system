﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Funds_Tickets : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
            //mTripTitle.Text = string.Format("{0} ({1}) ({2}) شماره: {3}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDate(mTrip.Date, ""), mTrip.ID);
        }
        //else
        //    mTrip = Helper.Instance.GetTrip(TripID);
    }
    private void BindInitData()
    {
        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;
        List<Seller> all = Helper.Instance.GetSellers(true);
        if (all == null)
            all = new List<Seller>();
        Seller none = new Seller();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);
        sSellers.DataValueField = "ID";
        sSellers.DataTextField = "Title";
        sSellers.DataSource = all;
        sSellers.DataBind();
    }

    private void BindData()
    {
        List<Seller> all = null;
        if (sSellers.SelectedIndex > 0)
        {
            int sellerID = Tool.GetInt(sSellers.SelectedValue, Null.NullInteger);
            Seller seller = Helper.Instance.GetSeller(sellerID);
            all = new List<Seller>();
            if (seller != null)
                all.Add(seller);
        }
        else
            all = Helper.Instance.GetSellers();
        if (all == null || all.Count == 0)
        {
            list.DataSource = null;
            list.DataBind();
            return;
        }
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        DataTable table = null;
        for (int i = 0; i < all.Count; i++)
        {
            if (!all[i].Enabled)
                continue;
            string qry = string.Format(@"SELECT Tickets.ID, N'{0}' AS Seller, {1} AS SellerID, Tickets.No, Tickets.Chairs, Tickets.Fullname, Tickets.Tel, Tickets.Price
, Tickets.NumChairs,  ((Tickets.Price - Discount) * Tickets.NumChairs) AS Total
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
, Branches.Title AS Branch
, Users.FullName AS  SaleUser, SaleDate, SaleType
FROM Tickets 
INNER JOIN Trips ON Tickets.TripID=Trips.ID
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Branches ON Tickets.BranchID=Branches.ID
LEFT JOIN Users ON Tickets.SaleUserID=Users.ID
WHERE Tickets.SaleDate>= '{2}/{3}/{4}' AND Trips.Date<='{5}/{6}/{7} 23:59:59'"
                , all[i].Title.Replace("'", "''"), all[i].ID.ToString()
                , startDate.Year, startDate.Month, startDate.Day
                , toDate.Year, toDate.Month, toDate.Day);
            if (sNew.Checked && !sArchive.Checked)
                qry += " AND (Tickets.Archive IS NULL OR Tickets.Archive=0)";
            if (!sNew.Checked && sArchive.Checked)
                qry += " AND Tickets.Archive=1";
            Helper.Instance.LastException = null;
            DataTable dt = Helper.Instance.FillDataTable(qry, all[i].CnnStr);
            if (dt == null)
            {
                if (Helper.Instance.LastException != null)
                    mMsg.Text += "Error for ` " + all[i].Title + "` : " + Helper.Instance.LastException.Message + "<br />";
                continue;
            }
            if (dt != null)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (sSrc.Text.Trim().Length > 0)
                        if (!Tool.GetString(dt.Rows[j]["SrcCity"], "").Contains(sSrc.Text))
                        {
                            dt.Rows.RemoveAt(j);
                            j--;
                            continue;
                        }
                    if (sDest.Text.Trim().Length > 0)
                        if (!Tool.GetString(dt.Rows[j]["DestCity"], "").Contains(sDest.Text))
                        {
                            dt.Rows.RemoveAt(j);
                            j--;
                            continue;
                        }
                    string chairs = Tool.GetString(dt.Rows[j]["Chairs"], "");
                    dt.Rows[j]["Chairs"] = chairs == null ? "" : chairs.Replace("f", "").Replace("m", "").Replace(";", "-").Trim('-');

                    SaleTypes st = (SaleTypes)Tool.GetInt(dt.Rows[j]["SaleType"], (int)SaleTypes.OfficeSale);
                    if (st == SaleTypes.Reserve)
                    {
                        dt.Rows[j]["Price"] = dt.Rows[j]["Total"] = 0;
                        dt.Rows[j]["Branch"] = "رزروی";
                    }
                    //dt.Rows[j]["SaleDate"] = Tool.ToPersianDate(Tool.GetDate(dt.Rows[j]["SaleDate"]), "");

                    //rows.Add(dt.Rows[j]);
                }
                if (table == null)
                    table = dt;
                else
                {
                    while (dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        DataRow r2 = table.NewRow();
                        for (int jjj = 0; jjj < dt.Columns.Count; jjj++)
                        {
                            r2[jjj] = row[jjj];
                        }
                        dt.Rows.RemoveAt(0);
                        //table.ImportRow(row);
                        table.Rows.Add(r2);
                    }
                }
            }
        }
        list.DataSource = table;
        list.DataBind();
    }
    protected void doRefresh_Click(object sender, EventArgs e)
    {
        BindData();
        //BindData();
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView info = e.Row.DataItem as DataRowView;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info["ID"].ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                DateTime dt = Tool.GetDate(info["SaleDate"]);
                if (dt != Null.NullDate)
                    e.Row.Cells[14].Text = string.Format("{0}:{1}"
                           , dt.Hour.ToString().PadLeft(2, '0')
                           , dt.Minute.ToString().PadLeft(2, '0'));
                else
                    e.Row.Cells[14].Text = "";
            }
        }
    }

}
