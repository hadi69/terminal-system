﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Report.aspx.cs" Inherits="Funds_Report" %>

<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">

    <script src="../js/jquery.min.js" type="text/javascript"></script>

    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                </tr>
                <tr>
                    <td>
                        تعاونی
                    </td>
                    <td>
                        <asp:DropDownList ID="sSellers" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:TextBox ID="sDest" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع
                    </td>
                    <td>
                        <asp:RadioButton ID="mTrips" runat="server" Checked="true" GroupName="reportmode"
                            Text="صورت وضعیت" />
                        &nbsp; &nbsp;
                        <asp:RadioButton ID="mTickets" runat="server" GroupName="reportmode" Text="بلیط" />
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                    <td style="text-align: left" colspan="4">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2" DisplayAfter="1">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button><a href='#' class="CB" id="printBime" onclick='showHtml(event)'
                                style='color: #fff'>&nbsp;&nbsp;چاپ&nbsp;&nbsp;</a>
                        <input type="hidden" runat="server" id="prinQry" class="printQry" />
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Seller" HeaderText="تعاونی" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                    <asp:BoundField DataField="ServiceType" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="SrcCity" HeaderText="مسير" />
                    <asp:BoundField DataField="CarTitle" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverTitle" HeaderText="راننده1" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                    <asp:BoundField DataField="NumTickets" HeaderText="ت بليط" />
                    <asp:BoundField DataField="AutoShare" HeaderText="پرداختی به راننده" />
                    <asp:BoundField DataField="Comission2" HeaderText="کمیسیون" />
                    <asp:BoundField DataField="Others" HeaderText="متفرقه" />
                    <asp:BoundField DataField="TotalPrice" HeaderText="باقیمانده" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:GridView ID="tList" runat="server" AutoGenerateColumns="False" OnRowDataBound="tList_RowDataBound"
                Width="100%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Seller" HeaderText="تعاونی" />
                    <asp:BoundField DataField="No" HeaderText="شماره بليط" />
                    <asp:BoundField DataField="Chairs" HeaderText="صندلیها" />
                    <asp:BoundField DataField="Fullname" HeaderText="نام" />
                    <asp:BoundField DataField="Tel" HeaderText="تلفن" />
                    <asp:BoundField DataField="Price" HeaderText="کرايه" />
                    <asp:BoundField DataField="NumChairs" HeaderText="تعداد" />
                    <asp:BoundField DataField="Total" HeaderText="جمع" />
                    <asp:BoundField DataField="SrcCity" HeaderText="مبدا" />
                    <asp:BoundField DataField="DestCity" HeaderText="مقصد" />
                    <asp:BoundField DataField="Branch" HeaderText="دفتر" />
                    <asp:BoundField DataField="SaleUser" HeaderText="فروشنده" />
                    <asp:BoundField DataField="SaleDate" HeaderText="ساعت" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
        function showHtml(event) {
            //event.preventDefault();
            //alert(2);
            var url = "reporthtml.aspx?" + $('.printQry').val();
            //alert(url);
            window.open(url, "Zweitfenster", "location=1,status=1,scrollbars=1,width=1000,height=600,left=10,top=10")
            return false;
        }
    </script>

</asp:Content>
