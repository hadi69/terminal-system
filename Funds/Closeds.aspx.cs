﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class Funds_Closeds : System.Web.UI.Page
{

    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        msg = DateTime.Now.ToString();
        DateTime dt1 = DateTime.Now;
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    void BindInitData()
    {
        List<Seller> all = Helper.Instance.GetSellers(true);
        if (all == null)
            all = new List<Seller>();
        Seller none = new Seller();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);
        sSellers.DataValueField = "ID";
        sSellers.DataTextField = "Title";
        sSellers.DataSource = all;
        sSellers.DataBind();

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["FundCloseds.sDateStart"]), DateTime.Now);
        sDateEnd.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["FundCloseds.sDateEnd"]), DateTime.Now);
    }

    private void BindData()
    {
        List<Seller> all = null;
        if (sSellers.SelectedIndex > 0)
        {
            int sellerID = Tool.GetInt(sSellers.SelectedValue, Null.NullInteger);
            Seller seller = Helper.Instance.GetSeller(sellerID);
            all = new List<Seller>();
            if (seller != null)
                all.Add(seller);
        }
        else
            all = Helper.Instance.GetSellers();
        if (all == null || all.Count == 0)
        {
            list.DataSource = null;
            list.DataBind();
            return;
        }

        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);

        #region Build query
        string query = string.Format(@"SELECT N'_SELLER_' AS Seller, _SELLERID_ AS SellerID, Trips.*
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
	, Services.ServiceType
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
	, Layouts.NumChairs
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
LEFT JOIN Cars ON Trips.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
WHERE (Trips.Archive IS NULL OR Trips.Archive=0) AND Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59'
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);


        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            query += " AND Trips.No>=" + sID;
        }
        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            query += " AND Trips.No<=" + sID;
        }
        #endregion
        DataTable table = null;
        for (int s = 0; s < all.Count; s++)
        {
            string qry = query.Replace("_SELLER_", all[s].Title.Replace("'", "''")).Replace("_SELLERID_", all[s].ID.ToString());
            Helper.Instance.LastException = null;
            DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Services.ServiceType", all[s].CnnStr);
            if (dt == null)
            {
                if (Helper.Instance.LastException != null)
                    mMsg.Text += "Error for ` " + all[s].Title + "` : " + Helper.Instance.LastException.Message + "<br />";
                continue;
            }
            if (dt == null || dt.Rows.Count == 0)
                continue;

            #region Filter
            if (sSeries.Text.Trim().Length > 0)
            {
                string str = Tool.ParseSeries(sSeries.Text.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series.Contains(str))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mBus.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs > 25)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mMini.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs >= 6 && numChairs < 25)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mSewari.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs < 6)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            //if (sSrc.Text.Trim().Length > 0)
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //        if (!Tool.GetString(dt.Rows[i]["SrcCity"], "").Contains(sSrc.Text))
            //        {
            //            dt.Rows.RemoveAt(i);
            //            i--;
            //        }
            if (sDest.Text.Trim().Length > 0)
                for (int i = 0; i < dt.Rows.Count; i++)
                    if (!Tool.GetString(dt.Rows[i]["DestCity"], "").Contains(sDest.Text))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
            #endregion

            if (table == null)
                table = dt;
            else
            {
                while (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    DataRow r2 = table.NewRow();
                    for (int jjj = 0; jjj < dt.Columns.Count; jjj++)
                    {
                        r2[jjj] = row[jjj];
                    }
                    dt.Rows.RemoveAt(0);
                    //table.ImportRow(row);
                    table.Rows.Add(r2);
                }
            }
        }

        Session["FundCloseds.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["FundCloseds.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        list.DataSource = table;
        list.DataBind();
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRowView row = e.Row.DataItem as DataRowView;
            if (row != null)
            {
                e.Row.Cells[0].Text = row["ID"].ToString();
                e.Row.Cells[3].Text = Tool.ToPersianDate(row["Date"], "");
                e.Row.Cells[5].Text = Tool.ToString((ServiceTypes)Tool.GetInt(row["ServiceType"]));
                e.Row.Cells[6].Text = Tool.GetString(row["SrcCity"], "") + " -> " + Tool.GetString(row["DestCity"], "");
                string series = Tool.GetString(row["Series"], "");
                if (series != null && series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                    string[] ss = series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                        b.AppendFormat("<td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td>", ss[0], ss[1], ss[2]);
                    b.Append("</tr></table>");
                    e.Row.Cells[10].Text = b.ToString();
                }
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

}
