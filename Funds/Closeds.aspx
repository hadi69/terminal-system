﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Closeds.aspx.cs" Inherits="Funds_Closeds" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                </tr>
                <tr>
                    <td>
                        تعاونی
                    </td>
                    <td>
                        <asp:DropDownList ID="sSellers" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:TextBox ID="sDest" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع
                    </td>
                    <td>
                        <asp:CheckBox ID="mBus" runat="server" CssClass="T" Text='اتوبوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mMini" runat="server" CssClass="T" Text='مینی بوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mSewari" runat="server" CssClass="T" Text='سواری' Checked="true" />
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="7">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2" DisplayAfter="1">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Seller" HeaderText="تعاونی" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                    <asp:BoundField DataField="ServiceType" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="SrcCity" HeaderText="مسير" />
                    <asp:BoundField DataField="CarTitle" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverTitle" HeaderText="راننده1" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                    <asp:BoundField DataField="NumTickets" HeaderText="ت بليط" />
                    <asp:HyperLinkField HeaderText="صورت وضعيت" DataNavigateUrlFields="ID, SellerID"
                        DataNavigateUrlFormatString="../Reports/Report.aspx?id={0}&sellerID={1}&repname=SuratVaziatM"
                        Text="صورت وضعيت" Target="_blank" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
