﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Funds_Advance : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Fund> all = Helper.Instance.GetFunds();
            sFundID.DataValueField = mFundID.DataValueField = "ID";
            sFundID.DataTextField = mFundID.DataTextField = "Title";
            mFundID.DataSource = all;
            mFundID.DataBind();

            Fund none = new Fund();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);
            sFundID.DataSource = all;
            sFundID.DataBind();
        }

        {
            List<Car> all = Helper.Instance.GetCars();
            sCarID.DataValueField = mCarID.DataValueField = "ID";
            sCarID.DataTextField = mCarID.DataTextField = "Title";
            mCarID.DataSource = all;
            mCarID.DataBind();

            Car none = new Car();
            none.ID = Null.NullInteger;
            none.CarType = null;
            all.Insert(0, none);
            sCarID.DataSource = all;
            sCarID.DataBind();
        }

        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Payments
                  where c.Type == (int)PaymentTypes.Advance
                  select c;

        if (sFundID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.FundID == sID);
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.CarID == sID);
        }
        int amount = Tool.GetInt(sAmount1.Text, Null.NullInteger);
        if (amount != Null.NullInteger)
            all = all.Where(c => c.Amount >= amount);
        amount = Tool.GetInt(sAmount2.Text, Null.NullInteger);
        if (amount != Null.NullInteger)
            all = all.Where(c => c.Amount <= amount);

      
        list.DataSource = all;
        list.DataBind();
    }

    protected void puOK_Click(object sender, EventArgs e)
    {
        Payment info;
        if (Null.NullInteger == EditID)
            info = new Payment();
        else
            info = Helper.Instance.GetPayment(EditID);
        if (info == null)
            return;

        if (!Tool.ValidatePersianDate(mDate.Text))
        {
            mError.Text = "تاريخ اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        info.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        info.Amount = Tool.GetInt(mAmount.Text, 0);
        info.Comments = mComments.Text;
        info.Type = (short)PaymentTypes.Advance;
        info.Date = Tool.ParsePersianDate(mDate.Text, DateTime.Now);
        info.CarID = Tool.GetInt(mCarID.SelectedValue, Null.NullInteger);
        if (info.CarID == Null.NullInteger)
        {
            mError.Text = "لطفا ماشین را انتخاب کنید";
            puEx.Show();
            return;
        }

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Payments.InsertOnSubmit(info);

        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Payment info = e.Row.DataItem as Payment;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Helper.Instance.GetFundTitle(info.FundID);
                e.Row.Cells[3].Text = Helper.Instance.GetCarTitle(info.CarID);

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Payment info = Helper.Instance.GetPayment(EditID);
        if (info == null)
            return;

        Tool.SetSelected(mFundID, info.FundID);
        Tool.SetSelected(mCarID, info.CarID);
        mAmount.Text = info.Amount.ToString();
        mComments.Text = info.Comments;
        //mGet.Checked = info.Type == (short)PaymentTypes.Get;
        //mPay.Checked = !mGet.Checked;
        mDate.Text = Tool.ToPersianDate(info.Date, "");

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeletePayment(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mComments.Text = mAmount.Text = "";
        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");

        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
