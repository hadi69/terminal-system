﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PrintTickets : System.Web.UI.Page
{
    Trip mTrip = null;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        //mError.Text = "";
        if (!IsPostBack)
        {
            this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
            mTrip = Helper.Instance.GetTrip(this.TripID);
            if (mTrip == null)
                Response.Redirect("Closeds.aspx", true);            
        }
        else
            mTrip = Helper.Instance.GetTrip(TripID);
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        mHour.Text = Tool.FixTime(mTrip.Service.DepartureTime);
        mDate.Text = Tool.ToPersianDate(mTrip.Date, "");
        mPath.Text = mTrip.Service.Path.Title;
        mNum.Text = Helper.Instance.GetNumTickets(mTrip.ID).ToString();

        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        Layout layout = mTrip.CarType.Layout;
        if (string.IsNullOrEmpty(layout.Layout1))
            return;
        int index = 1;
        int totalprice = 0;
        mTickets.Controls.Add(new LiteralControl("<table cellpadding=5 cellspacing=0 border=1 class='PrintHelp'>"));
        mTickets.Controls.Add(new LiteralControl("<tr><td>ردیف</td><td>نام مسافر</td><td>تعداد</td><td>تلفن</td><td>آدرس</td><td>فتر</td><td>فروشنده</td></tr>"));
        for (int i = 0; i < tickets.Count; i++)
        {
            mTickets.Controls.Add(new LiteralControl("<tr>"));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", i+1)));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", tickets[i].Fullname)));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", tickets[i].NumChairs)));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", tickets[i].Tel)));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", tickets[i].Address)));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", (tickets[i].SaleType == (int)SaleTypes.Reserve) ? "رزروی" : tickets[i].Branch == null ? "" : tickets[i].Branch.Title)));
            mTickets.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Helper.Instance.GetUserFullName(tickets[i].SaleUserID))));
            mTickets.Controls.Add(new LiteralControl("</tr>"));
        }
        mTickets.Controls.Add(new LiteralControl("</table>"));
    }
    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
}