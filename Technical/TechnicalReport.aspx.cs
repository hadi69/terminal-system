﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Technical_TechnicalReport : System.Web.UI.Page
{
    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        msg = DateTime.Now.ToString();
        DateTime dt1 = DateTime.Now;
        //list.EnableViewState = false;
        if (!IsPostBack)
        {
            //Helper.Instance.AutoDeleteTrips();
            BindInitData();
            //if (!Tool.ButtonIsClicked(Request, doSearch))
            BindData();
        }
    }
    private void BindInitData()
    {

        List<Company> all = Helper.Instance.GetCompanies();


        ddlCompany.DataValueField = "ID";
        ddlCompany.DataTextField = "Title";
        ddlCompany.DataSource = all;
        ddlCompany.DataBind();

        ddlCompany.Items.Insert(0, new ListItem("همه", "-1"));

    }
    private void BindData()
    {
        string qry = string.Format(@"SELECT CompanyTrips.*
        FROM CompanyTrips WHERE 1=1");
        if (ddlCompany.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(ddlCompany.SelectedValue, Null.NullInteger);
            qry += " AND CompanyID=" + sID;
        }
        if (sStatus.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sStatus.SelectedValue, Null.NullInteger);
            qry += " AND Status=" + sID;
        }
        if (sCarNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sCarNo.Text, Null.NullInteger);
            qry += " AND CarNo Like N'%" + sID+"%'";
        }
        if (sSeries.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sSeries.Text, Null.NullInteger);
            qry += " AND TripSeries Like N'%" + sID + "%'";
        }
        if (sDriver.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sDriver.Text, Null.NullInteger);
            qry += " AND Driver Like N'%" + sID + "%'";
        }

        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            qry += " AND TripNo>=" + sID;
        }
        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            qry += " AND TripNo<=" + sID;
        }
        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY ID");


        list.DataSource = dt;
        list.DataBind();
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRowView row = e.Row.DataItem as DataRowView;
            if (row != null)
            {
                CompanyTrips companyTrips=Helper.Instance.GetCompanyTrip(Tool.GetInt(row["ID"]));
                e.Row.Cells[0].Text = companyTrips.ID.ToString();
                e.Row.Cells[2].Text = companyTrips.Company.Title;
                e.Row.Cells[3].Text = companyTrips.User == null ? "" : companyTrips.User.FullName;
                e.Row.Cells[8].Text = Helper.StatusToString((Status)companyTrips.Status, "");
            }
        }
    }
    
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}