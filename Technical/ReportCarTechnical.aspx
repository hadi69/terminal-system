﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportCarTechnical.aspx.cs" Inherits="Technical_ReportCarTechnical" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="sSmartCard" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 29px">
                        کاربر
                    </td>
                    <td style="height: 29px">
                        <asp:TextBox ID="sUserName" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td style="height: 29px">
                        شماره پلاک ماشین
                    </td>
                    <td style="height: 29px">
                        <asp:TextBox ID="sPlate" runat="server" Width="150px" />
                    </td>
                    <td style="height: 29px">
                        وضعیت
                    </td>
                    <td style="height: 29px">
                        <asp:DropDownList ID="sTechnicalStatus" runat="server" CssClass="DD" Width="150px" >
                            <asp:ListItem Text="همه" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="تایید شده" Value="2"></asp:ListItem>
                            <asp:ListItem Text="رد شده" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="height: 29px"></td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="6">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <input type="hidden" runat="server" id="mIDToDelete" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="SmartCard" HeaderText="کارت هوشمند" />
                    <asp:BoundField DataField="UserName" HeaderText="کاربر" />
                    <asp:BoundField DataField="Plate" HeaderText="شماره ماشين" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="TechnicalStatus" HeaderText="وضعیت" />
                    <asp:HyperLinkField DataNavigateUrlFields="ID" NavigateUrl="~/Technical/CarTechnical.aspx" Text="کنترل اجزاء فنی" DataNavigateUrlFormatString="~/Technical/CarTechnical.aspx?CarTechnicalID={0}" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

