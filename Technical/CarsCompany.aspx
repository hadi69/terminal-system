﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CarsCompany.aspx.cs" Inherits="Technical_CarsCompany" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td colspan="8">
                        اعتبارسنجی
                    </td>
                </tr>
                <tr>
                    <td>
                        اتوکار:
                    </td>
                    <td>
                        شماره کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="cSmartCard" runat="server" />
                    </td>
                    <td>
                        VIN
                    </td>
                        <td style="display: none">
                                <!--
                                ایران //-->
                                <asp:TextBox ID="cPlate0" runat="server" MaxLength="2" Width="50" BackColor="#FF1111" />
                                <asp:TextBox ID="cPlate1" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <asp:DropDownList ID="cPlate2" runat="server" CssClass="TL" Width="50" BackColor="#FF1111">
                                    <asp:ListItem Text="ع" Value="ع" />
                                    <asp:ListItem Text="الف" Value="الف" />
                                </asp:DropDownList>
                                <asp:TextBox ID="cPlate3" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars"
                                    TargetControlID="cPlate0" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars"
                                    TargetControlID="cPlate1" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars"
                                    TargetControlID="cPlate3" ValidChars='123456789' />
                            </td>
                    <td>
                        <asp:TextBox runat="server" ID="cVIN"></asp:TextBox>
                    </td>
                    <td>
                        شماره پرونده
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="cNo"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doGetCar" runat="server" CssClass="CB" OnClick="doGetCar_Click"
                            Text="دریافت اطلاعات از تهران" Width="150px" />
                    </td>
                </tr>
            </table>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabled" Text='نمایش فعالها' Checked="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabledNo" Text='نمایش غیرفعالها' />
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                    <td align="left">
                        <asp:Button ID="doPrint" runat="server" CssClass="CB" Text="چاپ" Width="50px" OnClick="doPrint_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="sSmartCard" runat="server" Width="150px" />
                    </td>
                    <td>
                        شماره ترانزيت
                    </td>
                    <td>
                        <asp:TextBox ID="sTransitNo" runat="server" Width="150px" />
                    </td>
                    <td class="N">
                        شماره پلاک
                    </td>
                    <td>
                        ایران<asp:TextBox ID="sPlate0" runat="server" MaxLength="2" Width="50" />
                        <asp:TextBox ID="sPlate1" runat="server" CssClass="TL" Width="50" />
                        <asp:DropDownList ID="sPlate2" runat="server" CssClass="TL" Width="50">
                            <asp:ListItem Text="" Value="" />
                            <asp:ListItem Text="ع" Value="ع" />
                            <asp:ListItem Text="الف" Value="الف" />
                        </asp:DropDownList>
                        <asp:TextBox ID="sPlate3" runat="server" CssClass="TL" Width="50" />
                    </td>
                </tr>
                <tr>
                    
                    <td colspan="6" style="text-align: left">
                        <asp:Button ID="doUpdate" runat="server" Text="بروزرسانی" Width="100px"
                            CssClass="CB" OnClick="doUpdate_Click"></asp:Button>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="CompanyID" HeaderText="شرکت" />
                    <asp:BoundField DataField="Code" HeaderText="کد" />
                    <asp:BoundField DataField="ID" HeaderText="شهر پلاک" />
                    <asp:BoundField DataField="Plate" HeaderText="شماره پلاک" />
                    <asp:BoundField DataField="SmartCard" HeaderText="کارت هوشمند" />
                    <asp:BoundField DataField="TransitNo" HeaderText="شماره ترانزيت" />
                    <asp:BoundField DataField="ContractDate2" HeaderText="تاريخ قرارداد" />
                    <asp:BoundField DataField="VisitDate2" HeaderText="تاریخ معاینه فنی" />
                    <asp:BoundField DataField="Enabled" HeaderText="نمایش" />
                    <asp:HyperLinkField DataNavigateUrlFields="ID" NavigateUrl="~/Technical/CarTechnical.aspx" Text="کنترل اجزاء فنی" DataNavigateUrlFormatString="~/Technical/CarTechnical.aspx?CarCompanyID={0}" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none;" ID="puPanel" runat="server" Width="550px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش اتوکار)</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                شرکت
                            </td>
                            <td>
                                <asp:TextBox ID="mCompanyName" runat="server" Enabled="False"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد
                            </td>
                            <td>
                                <asp:TextBox ID="mCode" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره پلاک
                            </td>
                            <td>
                                <!--
                                ایران //-->
                                <asp:TextBox ID="mPlate0" runat="server" MaxLength="2" Width="50" BackColor="#FF1111" />
                                <asp:TextBox ID="mPlate1" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <asp:DropDownList ID="mPlate2" runat="server" CssClass="TL" Width="50" BackColor="#FF1111">
                                    <asp:ListItem Text="ع" Value="ع" />
                                    <asp:ListItem Text="الف" Value="الف" />
                                </asp:DropDownList>
                                <asp:TextBox ID="mPlate3" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mPlate0" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mPlate1" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mPlate3" ValidChars='123456789' />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شناسه خودرو VIN
                            </td>
                            <td>
                                <asp:TextBox ID="mVIN" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شهر/کشور پلاک
                            </td>
                            <td>
                                <asp:DropDownList ID="mPlateCity" runat="server" BackColor="#FF1111">
                                </asp:DropDownList>
                                <asp:HiddenField ID="mCityPlak" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کارت هوشمند
                            </td>
                            <td>
                                <asp:TextBox ID="mSmartCard" MinLength="7" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="شماره کارت هوشمند الزامي است"
                                    ControlToValidate="mSmartCard" Display="Dynamic"></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator Display = "Dynamic" ValidationGroup="addGroup" ControlToValidate = "mSmartCard" ID="rev1" ValidationExpression = "^[\s\S]{7,}$" runat="server" ErrorMessage="شماره کارت نباید کمتر از 7 رقم باشد"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاریخ معاینه فنی
                            </td>
                            <td>
                                <asp:TextBox ID="mVisitDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="mee1" TargetControlID="mVisitDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره ترانزيت
                            </td>
                            <td>
                                <asp:TextBox ID="mTransitNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                سال توليد
                            </td>
                            <td>
                                <asp:TextBox ID="mProductionYear" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                <!-- 
                                سيستم
                                -->
                            </td>
                            <td>
                                <asp:TextBox ID="mDrivingNo" runat="server" Visible="false" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ بيمه بدنه
                            </td>
                            <td>
                                <asp:TextBox ID="mInsuranceDate" runat="server" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="mInsuranceDate"
                                    runat="server" Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ بيمه شخص ثالث
                            </td>
                            <td>
                                <asp:TextBox ID="mThirdPersonDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="mThirdPersonDate"
                                    runat="server" Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره بیمه شخص ثالث
                            </td>
                            <td>
                                <asp:TextBox ID="mThirdPersonNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کميسيون
                            </td>
                            <td>
                                <asp:TextBox ID="mCommission" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                حذف شده
                            </td>
                            <td>
                                <asp:CheckBox ID="mDeleted" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ قرارداد
                            </td>
                            <td>
                                <asp:TextBox ID="mContractDate" runat="server" />
                                <cc1:MaskedEditExtender ID="mee4" TargetControlID="mContractDate" runat="server"
                                    Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button runat="server" ID="doGetDataFromServer" Text='دریافت اطلاعات از تهران'
                                    OnClick="doGetDataFromServer_Click" CssClass="CB" />
                                <asp:Button ID="ReadCard" runat="server" Text="خواندن از کارت" OnClick="ReadCard_Click"
                                    CssClass="CB" />
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                    <asp:Button ID="puClose" runat="server" Text="بستن" CssClass="CB" OnClick="puClose_Click"/>
                                <asp:Button ID="puCancel" OnClick="puCancel_Click" runat="server"  Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label ID="mError" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="Label1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

