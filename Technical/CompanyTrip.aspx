﻿<%@ Page Title="صورت های ارسالی" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CompanyTrip.aspx.cs" Inherits="Technical_CompanyTrip" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <table border="0" class="filter">
                <tr>
                    <td>
                        شرکت
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCompany" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                AllowPaging="True"  OnPageIndexChanging="list_PageIndexChanging" OnRowEditing="list_RowEditing"
                PageSize="50" Style="margin-top: 0px" EnableModelValidation="True">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="CompanyID" HeaderText="شرکت" />
                    <asp:BoundField DataField="TripSeries" HeaderText="سری صورت" />
                    <asp:BoundField DataField="TripNo" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Driver" HeaderText="راننده" />
                    <asp:BoundField DataField="CarNo" HeaderText="شماره ماشین" />
                    <asp:BoundField DataField="Status" HeaderText="وضعیت" />
                    <asp:HyperLinkField DataNavigateUrlFields="ID" NavigateUrl="~/Technical/Technical.aspx" Text="کنترل اجزاء فنی" DataNavigateUrlFormatString="~/Technical/Technical.aspx?CompanyTripID={0}" />
                    <asp:CommandField SelectText="تایید" ShowSelectButton="True"/>
                    <asp:CommandField DeleteText="رد" ShowDeleteButton="True"/>
                    <asp:CommandField EditText="ویرایش" ShowEditButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    ويرايش صورت فنی</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                شرکت
                            </td>
                            <td>
                                <asp:DropDownList ID="mCompany" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره ماشین
                            </td>
                            <td>
                                <asp:TextBox ID="mCarNo" runat="server">
                                </asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضیح
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" Width="400px" Height="100px" TextMode="MultiLine">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

