﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Technical_ReportCarTechnical : System.Web.UI.Page
{
    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        msg = DateTime.Now.ToString();
        DateTime dt1 = DateTime.Now;
        //list.EnableViewState = false;
        if (!IsPostBack)
        {
            BindInitial();
            BindData();
        }
    }

    void BindInitial()
    {
        sDateStart.SelectedDate = DateTime.Today;
        sDateEnd.SelectedDate = DateTime.Today;
    }
    private void BindData()
    {
        string qry = string.Format(@"SELECT CarTechnical.*
        FROM CarTechnical WHERE 1=1");
        if (sTechnicalStatus.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sTechnicalStatus.SelectedValue, Null.NullInteger);
            qry += " AND TechnicalStatus=" + sID;
        }
        if (sPlate.Text.Trim().Length > 0)
        {
            string sID = sPlate.Text;
            qry += " AND Plate Like N'%" + sID + "%'";
        }
        if (sSmartCard.Text.Trim().Length > 0)
        {
            string sID = sSmartCard.Text;
            qry += " AND SmartCard Like N'%" + sID + "%'";
        }
        if (sUserName.Text.Trim().Length > 0)
        {
            string sID = sUserName.Text;
            qry += " AND UserName Like N'%" + sID + "%'";
        }

        if (Tool.ToPersianDate(sDateStart.SelectedDate,DateTime.Now).Trim().Length > 0)
        {
            string sID = Tool.ToPersianDate(sDateStart.SelectedDate, DateTime.Now);
            qry += " AND Date>='" + sID+"'";
        }
        if (Tool.ToPersianDate(sDateEnd.SelectedDate, DateTime.Now).Trim().Length > 0)
        {
            string sID = Tool.ToPersianDate(sDateEnd.SelectedDate, DateTime.Now);
            qry += " AND Date<='" + sID+"'";
        }
        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY ID");


        list.DataSource = dt;
        list.DataBind();
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRowView row = e.Row.DataItem as DataRowView;
            if (row != null)
            {
                CarTechnical carTechnical = Helper.Instance.GetCarTechnical(Tool.GetInt(row["ID"]));
                e.Row.Cells[0].Text = carTechnical.ID.ToString();
                e.Row.Cells[6].Text = Helper.TechnicalStatusToString((TechnicalStatus)carTechnical.TechnicalStatus, "");
            }
        }
    }

    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}