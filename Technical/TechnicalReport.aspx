﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TechnicalReport.aspx.cs" Inherits="Technical_TechnicalReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        شرکت
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlCompany" CssClass="DD" Width="150"/>
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 29px">
                        راننده
                    </td>
                    <td style="height: 29px">
                        <asp:TextBox ID="sDriver" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td style="height: 29px">
                        شماره ماشین
                    </td>
                    <td style="height: 29px">
                        <asp:TextBox ID="sCarNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td style="height: 29px">
                        وضعیت
                    </td>
                    <td style="height: 29px">
                        <asp:DropDownList ID="sStatus" runat="server" CssClass="DD" Width="150px" >
                            <asp:ListItem Text="همه" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="بررسی نشده" Value="1"></asp:ListItem>
                            <asp:ListItem Text="تایید شده" Value="2"></asp:ListItem>
                            <asp:ListItem Text="رد شده" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="height: 29px"></td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="6">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <input type="hidden" runat="server" id="mIDToDelete" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="CompanyID" HeaderText="شرکت" />
                    <asp:BoundField DataField="UserID" HeaderText="کاربر" />
                    <asp:BoundField DataField="CarNo" HeaderText="شماره ماشين" />
                    <asp:BoundField DataField="Driver" HeaderText="راننده" />
                    <asp:BoundField DataField="TripNo" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="TripSeries" HeaderText="سري صورت " />
                    <asp:BoundField DataField="Status" HeaderText="وضعیت" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

