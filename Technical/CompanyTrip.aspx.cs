﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class Technical_CompanyTrip : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            mMsg.Text = mError.Text = "";
            BindInitData();
            BindData();
            //puOk.Enabled = doAdd.Enabled = Tool.GetBool(Helper.Instance.GetSettingValue("CanEditCities"), false);
        }
    }
    private void BindInitData()
    {
        List<Company> all = Helper.Instance.GetCompanies();
        mCompany.DataValueField = "ID";
        mCompany.DataTextField = "Title";
        mCompany.DataSource = all;
        mCompany.DataBind();


        ddlCompany.DataValueField = "ID";
        ddlCompany.DataTextField = "Title";
        ddlCompany.DataSource = all;
        ddlCompany.DataBind();

        ddlCompany.Items.Insert(0,new ListItem("همه","-1"));
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.CompanyTrips
                  select c;// new { c.ID, c.Title, c.Code, c.Enabled, c.CountryID, CountryTitle = c.Country.Title };

        if (ddlCompany.SelectedIndex > 0)
        {
            int comID = Tool.GetInt(ddlCompany.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.CompanyID == comID);
        }
        all = all.Where(c => c.Status == (int) Status.NoCheck);
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        CompanyTrips info = new CompanyTrips();
            info = Helper.Instance.GetCompanyTrip(EditID);
        if (info == null)
            return;

        info.Comments = mComments.Text;
        if (Helper.Instance.Update())
        {
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CompanyTrips info = e.Row.DataItem as CompanyTrips;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Company.Title;
                e.Row.Cells[7].Text = Helper.StatusToString((Status)info.Status,"");
                //JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        CompanyTrips info = Helper.Instance.GetCompanyTrip(EditID);
        if (info == null)
            return;
        info.Status = (int) Status.Confirm;
        info.UserID = SiteSettings.UserID;

        if (Helper.Instance.UpdateTrip(info))
        {
            CompanyTripImpl companyTripImpl = new CompanyTripImpl();
            companyTripImpl.CompanyID = info.CompanyID;
            companyTripImpl.TripID = info.TripID;
            companyTripImpl.UserID = SiteSettings.UserID;
            companyTripImpl.CarNo = info.CarNo;
            companyTripImpl.Status = (int)Status.Confirm;
            companyTripImpl.Date = DateTime.Now;
            companyTripImpl.Driver = info.Driver;
            companyTripImpl.TripSeries = info.TripSeries;
            companyTripImpl.TripNo = info.TripNo;
            Helper.Instance.DB.CompanyTripImpls.InsertOnSubmit(companyTripImpl);

            if (Helper.Instance.Update())
            {
                
                BindData();
            }
        }
        else
        {
            mMsg.Text = "ارتباط با تعاونی قطع است لطفأ دوباره سعی کنید";
            mMsg.Visible = true;
        }
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        EditID = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        CompanyTrips info = Helper.Instance.GetCompanyTrip(EditID);
        if (info == null)
            return;
        info.Status = (int)Status.Reject;
        info.UserID = SiteSettings.UserID;

        if (Helper.Instance.UpdateTrip(info))
        {
            CompanyTripImpl companyTripImpl = new CompanyTripImpl();
            companyTripImpl.CompanyID = info.CompanyID;
            companyTripImpl.TripID = info.TripID;
            companyTripImpl.UserID = SiteSettings.UserID;
            companyTripImpl.CarNo = info.CarNo;
            companyTripImpl.Status = (int)Status.Reject;
            companyTripImpl.Date = DateTime.Now;
            companyTripImpl.Driver = info.Driver;
            companyTripImpl.TripSeries = info.TripSeries;
            companyTripImpl.TripNo = info.TripNo;
            Helper.Instance.DB.CompanyTripImpls.InsertOnSubmit(companyTripImpl);

            if (Helper.Instance.Update())
            {
                
                BindData();
            }
        }
        else
        {
            mMsg.Text = "ارتباط با تعاونی قطع است لطفأ دوباره سعی کنید";
            mMsg.Visible = true;
        }
    }
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    protected void list_RowEditing(object sender, GridViewEditEventArgs e)
    {
        EditID = Tool.GetInt(list.Rows[e.NewEditIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        CompanyTrips info = Helper.Instance.GetCompanyTrip(EditID);
        if (info == null)
            return;
        mCarNo.Text = info.CarNo;
        mComments.Text = info.Comments;
        try
        {
            mCompany.SelectedValue = info.CompanyID.ToString();
        }
        catch
        {
        }
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        list.PageIndex = 0;
        BindData();
    }

}