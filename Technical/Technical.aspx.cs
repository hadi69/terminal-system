﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Technical_Technical : System.Web.UI.Page
{
    CompanyTrips mCompanyTrip = new CompanyTrips();
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int companytripID = Tool.GetInt(Request.QueryString["CompanyTripID"], Null.NullInteger);
        mCompanyTrip = Helper.Instance.GetCompanyTrip(companytripID);
        if (!IsPostBack)
        {
            if (mCompanyTrip != null)
            {
                mCompany.Text = mCompanyTrip.Company.Title;
                mTripSeries.Text = mCompanyTrip.TripSeries;
                mTripNo.Text = Tool.GetString(mCompanyTrip.TripNo);
                mCarNo.Text = mCompanyTrip.CarNo;
                if (mCompanyTrip.FaniDetail != null)
                    if (mCompanyTrip.FaniDetail.Length == 16)
                    {
                        BindData();
                    }
            }
        }
    }

    void BindData()
    {
        string fani = mCompanyTrip.FaniDetail;
        if (fani.Substring(0, 1) == "1")
        {
            Accept1.Checked = true;
        }
        else
        {
            NoAccept1.Checked = true;
        }
        if (fani.Substring(1, 1) == "1")
        {
            Accept2.Checked = true;
        }
        else
        {
            NoAccept2.Checked = true;
        }
        if (fani.Substring(2, 1) == "1")
        {
            Accept3.Checked = true;
        }
        else
        {
            NoAccept3.Checked = true;
        }
        if (fani.Substring(3, 1) == "1")
        {
            Accept4.Checked = true;
        }
        else
        {
            NoAccept4.Checked = true;
        }
        if (fani.Substring(4, 1) == "1")
        {
            Accept5.Checked = true;
        }
        else
        {
            NoAccept5.Checked = true;
        }
        if (fani.Substring(5, 1) == "1")
        {
            Accept6.Checked = true;
        }
        else
        {
            NoAccept6.Checked = true;
        }
        if (fani.Substring(6, 1) == "1")
        {
            Accept7.Checked = true;
        }
        else
        {
            NoAccept7.Checked = true;
        }
        if (fani.Substring(7, 1) == "1")
        {
            Accept8.Checked = true;
        }
        else
        {
            NoAccept8.Checked = true;
        }
        if (fani.Substring(8, 1) == "1")
        {
            Accept9.Checked = true;
        }
        else
        {
            NoAccept9.Checked = true;
        }
        if (fani.Substring(9, 1) == "1")
        {
            Accept10.Checked = true;
        }
        else
        {
            NoAccept10.Checked = true;
        }
        if (fani.Substring(10, 1) == "1")
        {
            Accept11.Checked = true;
        }
        else
        {
            NoAccept11.Checked = true;
        }
        if (fani.Substring(11, 1) == "1")
        {
            Accept12.Checked = true;
        }
        else
        {
            NoAccept12.Checked = true;
        }
        if (fani.Substring(12, 1) == "1")
        {
            Accept13.Checked = true;
        }
        else
        {
            NoAccept13.Checked = true;
        }
        if (fani.Substring(13, 1) == "1")
        {
            Accept14.Checked = true;
        }
        else
        {
            NoAccept14.Checked = true;
        }
        if (fani.Substring(14, 1) == "1")
        {
            Accept15.Checked = true;
        }
        else
        {
            NoAccept15.Checked = true;
        }
        if (fani.Substring(15, 1) == "1")
        {
            Accept16.Checked = true;
        }
        else
        {
            NoAccept16.Checked = true;
        }
    }
    protected void doSaveSend_Click(object sender, EventArgs e)
    {
        mCompanyTrip.FaniDetail = FaniResult();
        if (Helper.Instance.Update())
        {
            if (Helper.Instance.UpdateTrip(mCompanyTrip))
            {
                Response.Redirect("CompanyTrip.aspx");
            }
            else
            {
                Msg.Text = "خطا در ذخیره کنترل اجزاء فنی";
            }
        }
    }

    string FaniResult()
    {
        string FaniDetail = "";
        if (Accept1.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept2.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept3.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept4.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept5.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept6.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept7.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept8.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept9.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept10.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept11.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept12.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept13.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept14.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept15.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept16.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }

        return FaniDetail;
    }
}
