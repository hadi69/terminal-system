﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Technical_Companies : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            BindData();
            //puOk.Enabled = doAdd.Enabled = Tool.GetBool(Helper.Instance.GetSettingValue("CanEditCities"), false);
        }
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Companies
                  select c;// new { c.ID, c.Title, c.Code, c.Enabled, c.CountryID, CountryTitle = c.Country.Title };

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Company info=new Company();
        if (Null.NullInteger != EditID)
            info = Helper.Instance.GetCompany(EditID);
        if (info == null)
            return;

        info.Title = mTitle.Text;
        info.Code = mCode.Text;
        //info.CountryID = int.Parse(mCountry.SelectedValue);
        info.ConnectionString = mConnectionString.Text;
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Companies.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            mTitle.Text = mCode.Text = "";
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Company info = e.Row.DataItem as Company;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Company info = Helper.Instance.GetCompany(EditID);
        if (info == null)
            return;
        mTitle.Text = info.Title;
        mCode.Text = info.Code;
        mConnectionString.Text = info.ConnectionString;
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        Company info = Helper.Instance.GetCompany(id);
        if (info == null)
            return;
        if (Helper.Instance.DeleteCompany(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mTitle.Text = mCode.Text = mConnectionString.Text = "";
        puEx.Show();
    }
}