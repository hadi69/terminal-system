﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CarTechnical.aspx.cs" Inherits="Technical_CarTechnical" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>فهرست کنترل اجزاء فنی</title>
    <style type="text/css">
        .myRadio
        {
            border-right: 0px;
            border-top: 0px;
            border-left: 0px;
            border-bottom: 0px;
        }
        .styletitr
        {
            font-weight: 600;
            font-size: 14px;
            font-family: "Courier New" , Courier, mono;
        }
        .kenar
        {
            font-weight: bold;
            font-size: 16px;
            color: #000000;
            background-color: #ffffff;
        }
        .navar
        {
            font-weight: bold;
            font-size: 18px;
            background-color: #b2b2b2;
        }
    </style>

    <script>
function changeColor(){
var curObj =document.activeElement ;
fname = curObj.name;
switch (fname){
	case ('fani1'):
		if (document.form1.fani1[1].checked){
			document.form1.fani1[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani1[1].style.backgroundColor='';
			}
		break;
	case ('fani2'):
		if (document.form1.fani2[1].checked) {
			document.form1.fani2[1].style.backgroundColor='pink';
			}			
		else {
			document.form1.fani2[1].style.backgroundColor='';
			}			
		break;
	case ('fani3'):
		if (document.form1.fani3[1].checked) {
			document.form1.fani3[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani3[1].style.backgroundColor='';
			}
		break;
	case ('fani4'):
		if (document.form1.fani4[1].checked) {
			document.form1.fani4[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani4[1].style.backgroundColor='';
			}
		break;
	case ('fani5'):
		if (document.form1.fani5[1].checked) {
			document.form1.fani5[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani5[1].style.backgroundColor='';
			}
		break;
	case ('fani6'):
		if (document.form1.fani6[1].checked) {
			document.form1.fani6[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani6[1].style.backgroundColor='';
			}
		break;
	case ('fani7'):
		if (document.form1.fani7[1].checked) {
			document.form1.fani7[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani7[1].style.backgroundColor='';
			}
		break;
	case ('fani8'):
		if (document.form1.fani8[1].checked) { 
			document.form1.fani8[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani8[1].style.backgroundColor='';
			}
		break;
	case ('fani9'):
		if (document.form1.fani9[1].checked) {
			document.form1.fani9[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani9[1].style.backgroundColor='';
			}
		break;
	case ('fani10'):
		if (document.form1.fani10[1].checked) {
			document.form1.fani10[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani10[1].style.backgroundColor='';
			}
		break;
	case ('fani11'):
		if (document.form1.fani11[1].checked) {
			document.form1.fani11[1].style.backgroundColor='pink';
			}
		else {		
			document.form1.fani11[1].style.backgroundColor='';
			}
		break;
	case ('fani12'):
		if (document.form1.fani12[1].checked) {
			document.form1.fani12[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani12[1].style.backgroundColor='';
			}
		break;
	case ('fani13'):
		if (document.form1.fani13[1].checked) {
			document.form1.fani13[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani13[1].style.backgroundColor='';
			}
		break;
	case ('fani14'):
		if (document.form1.fani14[1].checked) {
			document.form1.fani14[1].style.backgroundColor='pink';
			}
		else {
			document.form1.fani14[1].style.backgroundColor='';
			}
		break;
}		
}	


    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="styletitr" dir="rtl" style="background-color: #cccccc" width="80%"
            align="center">
            <tbody>
                <tr valign="baseline">
                    <td class="kenar" valign="top" nowrap colspan="8">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tbody>
                                <tr valign="baseline">
                                    <td class="navar" valign="top" nowrap>
                                        &nbsp;
                                    </td>
                                    <td class="navar" valign="top" nowrap align="middle">
                                        شماره کارت هوشمند: <b>
                                            <asp:Label runat="server" ID="mSmartCard" />
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #cccccc" nowrap>
                                        &nbsp;
                                    </td>
                                    <td style="background-color: #cccccc" nowrap align="middle">
                                        &nbsp;&nbsp;تاریخ :&nbsp;
                                        <asp:Label runat="server" ID="mDate" />
                                        &nbsp;&nbsp;شماره ماشین:&nbsp;<asp:Label runat="server" ID="mPlate" /><br>
                                        &nbsp;&nbsp;-
                                    </td>
                                </tr>
                                <tr class="styletitr">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <table class="styletitr" width="100%" border="0">
                                            <tbody>
                                                <tr style="background-color: #dfdfdf">
                                                    <td align="middle" width="48">
                                                        ردیف
                                                    </td>
                                                    <td width="352">
                                                        <div align="center">
                                                            اجزاء فنی وسیله</div>
                                                    </td>
                                                    <td width="66">
                                                        <div align="center">
                                                            قبول</div>
                                                    </td>
                                                    <td width="223">
                                                        <div align="center">
                                                            غیرقابل قبول</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        1
                                                    </td>
                                                    <td>
                                                        برگ معاینه فنی
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept1" runat="server" GroupName="Fani1" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept1" runat="server" GroupName="Fani1"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        2
                                                    </td>
                                                    <td>
                                                        لاستیکهای استاندارد و آجدار
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept2" runat="server" GroupName="Fani2" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept2" runat="server" GroupName="Fani2"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        3
                                                    </td>
                                                    <td>
                                                        دستگاه سنجش سرعت (تاخوگراف)
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept3" runat="server" GroupName="Fani3" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept3" runat="server" GroupName="Fani3"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        4
                                                    </td>
                                                    <td>
                                                        بخاری یا کولر سالم
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept4" runat="server" GroupName="Fani4" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept4" runat="server" GroupName="Fani4"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        5
                                                    </td>
                                                    <td>
                                                        برف پاک کن و آبپاش سالم</td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept5" runat="server" GroupName="Fani5" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept5" runat="server" GroupName="Fani5"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        6
                                                    </td>
                                                    <td>
                                                        شیشه های جلوی وسیله نقلیه
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept6" runat="server" GroupName="Fani6" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept6" runat="server" GroupName="Fani6"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        7
                                                    </td>
                                                    <td>
                                                        سایر شیشه ها
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept7" runat="server" GroupName="Fani7" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept7" runat="server" GroupName="Fani7"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        8
                                                    </td>
                                                    <td>
                                                        کپسول آتش نشانی
                                                        2 عدد 10 کیلویی</td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept8" runat="server" GroupName="Fani8" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept8" runat="server" GroupName="Fani8"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        9
                                                    </td>
                                                    <td>
                                                        جعبه کمکهای اولیه
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept9" runat="server" GroupName="Fani9" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept9" runat="server" GroupName="Fani9"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        10
                                                    </td>
                                                    <td>
                                                        مثلث شبرنگ و چراغ چشمک زن
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept10" runat="server" GroupName="Fani10" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept10" runat="server" GroupName="Fani10"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        11
                                                    </td>
                                                    <td>
                                                        چراغهای جلو و عقب وسیله نقلیه
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept11" runat="server" GroupName="Fani11" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept11" runat="server" GroupName="Fani11"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        12
                                                    </td>
                                                    <td>
                                                        زنجیر چرخ در صورت لزوم
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept12" runat="server" GroupName="Fani12" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept12" runat="server" GroupName="Fani12"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        13
                                                    </td>
                                                    <td>
                                                        چراغهای راهنما و بوق و آینه
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept13" runat="server" GroupName="Fani13" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept13" runat="server" GroupName="Fani13"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        14
                                                    </td>
                                                    <td>
                                                        لباس فرم خدمه
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept14" runat="server" GroupName="Fani14" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept14" runat="server" GroupName="Fani14"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        15
                                                    </td>
                                                    <td>
                                                        وضعیت ترمزهای پایی و دستی
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept15" runat="server" GroupName="Fani15" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept15" runat="server" GroupName="Fani15"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="middle">
                                                        16
                                                    </td>
                                                    <td>
                                                        چکش اضطراری 4 عدد
                                                    </td>
                                                    <td>
                                                        <asp:RadioButton ID="Accept16" runat="server" GroupName="Fani16" Checked="True"/>
                                                    </td>
                                                    <td align="middle">
                                                        <asp:RadioButton ID="NoAccept16" runat="server" GroupName="Fani16"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:TextBox runat="server" ID="mComment" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Label runat="server" ID="Msg" CssClass="Err" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="middle">
                                        <asp:Button runat="server" ID="doConfirm" Text="  تایید  " OnClick="doConfirm_Click"/>
                                        &nbsp;
                                        <asp:Button runat="server" ID="doReject" Text="  رد  " OnClick="doReject_Click"/>
                                        &nbsp;
                                        <asp:Button runat="server" ID="doCancel" Text="انصراف" OnClick="doCancel_Click"/>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
