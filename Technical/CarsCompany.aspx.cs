﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Technical_CarsCompany : System.Web.UI.Page
{
    ir.rmto.smartcard.PKG_WEB_SERVICESService car = new ir.rmto.smartcard.PKG_WEB_SERVICESService();
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["do"] == "Edit")
            {
                FromClose = true;
                int CarID = Tool.GetInt(Request.QueryString["CarID"], 0);
                EditCar(CarID);
                puCancel.Enabled = false;
            }
            if (Request.QueryString["do"] == "SaleEdit")
            {
                FromSale = true;
                int CarID = Tool.GetInt(Request.QueryString["CarID"], 0);
                EditCar(CarID);
                puCancel.Enabled = false;
            }
        }
        string reg = @"
        <script language='javascript'>
function plak_filler_reader(conversionString,inChar,outChar)
{
  var convertedString = conversionString.split(inChar);
  $String = '';
  $No1 = '';
  $No2 = '';
  $step = 1;
  for ($i=0;$i<convertedString.length;$i++) {  
  	$convert = convertedString[$i];
  	$temp = isDigit($convert);
  	if($temp == false){
		$String = $String + convertedString[$i];
		$step = 2;
	} else if($step == 1){
		$No1 = $No1 + convertedString[$i];
	} else if($step == 2){
		$No2 = $No2 + convertedString[$i];
	}
  }
  //document.getElementById('Plak1').value =  $No1;
  //document.getElementById('Plak2').value =  $String;
  //document.getElementById('Plak3').value =  $No2;
  return $No2+$String+$No1
}
function setCode() {
document.getElementById('fld_Plak_City_Code').style.backgroundColor='white' ;
city=document.getElementById('fld_Plak_Code').value;
city=trim(city);

if(city=='ایران' || city=='')
	{document.getElementById('fld_Plak_City_Code').value=99960000;
	document.getElementById('fld_Plak_Code').value='ایران';
	return 0;}
for(i=0;i<334;i++){
	if (city== plakcity[i]){
	document.getElementById('fld_Plak_City_Code').value=plakcode[i];
	return 0;	
	}
}
document.getElementById('fld_Plak_City_Code').value=99980000;
document.getElementById('fld_Plak_City_Code').style.backgroundColor='pink' ;
return 0;	
}
function Check(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRB.OmnikeyRB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 }
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	 //کارت هوشمند 


	data = obj.SCGetSHOMARE_PLAQUE;
    //12ع321
    document.getElementById('MPLATE1').value=data.substring(3,6); // 321
    document.getElementById('MPLATE2').value=data.substring(2,3);//
    document.getElementById('MPLATE3').value=data.substring(0,2);//12
 
	//data=plak_filler_reader(data,'','');
	//document.getElementById('MPLATE').value=data;	//شماره پلاک


	data = obj.SCGetSHOMARE_SERIAL_PLAQUE;
///
    var map=getcity(data);
    document.getElementById('mPlateCity').value=map;
///
	document.getElementById('mPlateSeries').value=data;	//سری پلاک


    data = obj.SCGetNAME_SHAHR_SHOMARE_GOZARI;
//alert(data);
//    if (data == null || data == '')
//        data = '0';
//	document.getElementById('mPlateCity').value=data;	//شهر پلاک

	data = obj.SCGetSAL_SAKHT;


    if (data<100)
        data += 1300;
    //data = data; + '/12/30';
	document.getElementById('mProductionYear').value=data;	//تاریخ تولید
	data = obj.SCGetSHENASEH_KHODRO;
	document.getElementById('mVIN').value=data;	//شناسه خودرو VIN

	data = obj.SCGetSYSTEM_NAME;
	//document.getElementById('mDrivingNo').value=data;	//سیستم

	data = obj.SCGetTYPE_NAME;
	//document.getElementById('mDrivingNo').value=data;	//سیستم

    obj.SCDisconnect;
    obj.SCRelease;
	//document.getElementById('ReadCard').disabled = false;	

	//setCode();	
}
</script>".Replace("ReadCard", ReadCard.ClientID).Replace("ReaDCard", ReadCard.ClientID)
         .Replace("mSmartCard", mSmartCard.ClientID)
         .Replace("mPlateSeries", mPlate0.ClientID)
         .Replace("mPlateCity", mPlateCity.ClientID)
         .Replace("MPLATE1", mPlate1.ClientID)
         .Replace("MPLATE2", mPlate2.ClientID)
         .Replace("MPLATE3", mPlate3.ClientID)
         .Replace("mProductionYear", mProductionYear.ClientID)
         .Replace("mVIN", mVIN.ClientID)
         .Replace("mDrivingNo", mDrivingNo.ClientID);
        RegisterStartupScript("ReadClickScript", reg);
        ReadCard.Attributes.Add("OnClick", "Check()");
        reg = @"
<script language='javascript'>
function changeToSne()
{
  var txt = $get('mPlate0');
  if (txt.value == '51')
  {
    var combo = $get('mPlateCity');
    combo.value = 'سنندج';
  }
}
</script>".Replace("mPlate0", mPlate0.ClientID).Replace("mPlateCity", mPlateCity.ClientID);
        RegisterStartupScript("changeToSneScript", reg);
        mPlate0.Attributes.Add("OnBlur", "changeToSne()");
    }
    protected bool FromClose
    {
        get
        {
            return Tool.GetBool(ViewState["FromClose"], false);
        }
        set
        {
            ViewState["FromClose"] = value;
        }
    }
    protected bool FromSale
    {
        get
        {
            return Tool.GetBool(ViewState["FromSale"], false);
        }
        set
        {
            ViewState["FromSale"] = value;
        }
    }
    private void BindInitData()
    {
        {
            #region Plakcity
            string nice = @"11320000	تهران
11420000	كرج
11430000	ورامين
14310000	قم
16310000	سارى
16330000	بابل
16420000	قايم شهر
16350000	بهشهر
16450000	نوشهر
16380000	تنكابن
16320000	آمل
97310000	گرگان
97350000	گنبدكاووس
97340000	كردكوى
21310000	اصفهان
21390000	قمشه شهرضا
21440000	گلپايگان
21470000	نجف آباداصفهان
26310000	تبريز
26360000	مراغه
26390000	ميانه
26370000	مرند
31310000	مشهد
31570000	نيشابور
31430000	سبزوار
31550000	گناباد
33310000	بيرجند
36310000	اهواز
36380000	خرمشهر
36320000	آبادان
36450000	شوشتر
36390000	دزفول
36370000	بهبهان
41310000	شيراز
41480000	مرودشت
41440000	كازرون
41370000	جهرم
41410000	فسا
41380000	داراب فارس
45310000	كرمان
45360000	جيرفت سبزواران
45370000	رفسنجان
45350000	بم
45390000	سيرجان
51310000	اراك
51360000	ساوه
51380000	محلات
51340000	خمين
54310000	رشت
54330000	بندرانزلي
54450000	لاهيجان گيلان
54390000	هشتپر تالش
57310000	اروميه
57350000	خوى
57370000	سلماس
61310000	زاهدان
61350000	زابل
61340000	خاش
61330000	چابهار
64310000	بندرعباس
64380000	ميناب
67310000	زنجان
67350000	ابهر
71310000	كرمانشاه
71330000	پاوه
73310000	سنندج
73330000	بيجار
75310000	همدان
75370000	ملاير
77310000	شهركرد
77330000	بروجن
81310000	خرم آبادلرستان
81360000	بروجرد
83310000	ايلام
83350000	دهلران
85310000	ياسوج بويراحمد
85320000	دهدشت كهگيلويه
87310000	سمنان
87330000	شاهرود
87320000	دامغان
93310000	يزد
93330000	اردكان
95310000	بندربوشهر
95370000	گناوه بندرگناوه
16460000	چالوس
16440000	نور
21420000	كاشان
73380000	كامياران
98000000	اظهارنشده
11340000	دماوند
16390000	رامسر
21330000	خميني شهر
21340000	خوانسار
21370000	فريدونشهر
21380000	فلاورجان
21460000	نايين
21320000	اردستان
21490000	نطنز
21350000	سميرم
21450000	لنجان
26340000	اهر
26350000	سراب
31380000	تربت حيدريه
31370000	تربت جام
31520000	قوچان
31450000	شيروان
31420000	درگز
32310000	بجنورد
31320000	اسفراين
31540000	كاشمر
31360000	تايباد
31470000	فردوس
31460000	طبس
36330000	انديمشك
36440000	رامهرمز
36360000	بندرماهشهر
36410000	شادگان
36460000	مسجد سليمان
36340000	ايذه
36420000	شوش
36430000	دشت آزادگان سوسنگرد
41320000	آباده
41430000	فيروزآباد
41450000	نيريز
41490000	ممسني نورآبادفارس
41350000	استهبان
41360000	اقليد
41390000	سپيدان
41460000	لار
45330000	بافت
45410000	شهربابك
45420000	كهنوج
45380000	زرندكرمان
51330000	تفرش
51320000	آشتيان
51350000	دليجان
51370000	سربندشازند
54360000	رودسر
54430000	فومن
54380000	صومعه سرا
54340000	آستانه
54350000	رودبار
54470000	لنگرود
54320000	آستارا
57390000	مهاباد
57410000	ماكو
57430000	مياندوآب
57440000	نقده
57360000	سردشت
57330000	پيرانشهر
61360000	سراوان
61320000	ايرانشهر
64330000	بندرلنگه
64370000	قشم
64320000	جاسك
67370000	خدابنده
71320000	اسلام آباد
71370000	قصرشيرين
71360000	سنقر
71390000	كنگاور
71410000	گيلان غرب
71350000	سرپل ذهاب
73370000	مريوان
73360000	قروه
73320000	بانه
73340000	سقز
75380000	نهاوند
75340000	تويسركان
77340000	فارسان
77350000	لردگان
81340000	اليگودرز
81380000	درودلرستان
81390000	كوهدشت
83330000	دره شهر
83370000	مهران
85330000	گچساران
87340000	گرمسار
93350000	تفت
93370000	مهريز
93340000	بافق
93380000	ميبد
95340000	دشتستان برازجان
95330000	ديربندردير
95350000	دشتي
41980000	ساير
15310000	قزوين
91310000	اردبيل
15330000	تاكستان
11360000	رى
91380000	گرمي مغان
91390000	مشگين شهر
95320000	تنگستان اهرم
91360000	خلخال
36350000	باغملك خوزستان
97330000	علي آبادگلستان
26410000	هشترودسراسكند
97370000	بندرگز
75360000	كبودرآهنكٌ
11370000	ساوجبلاغ
21480000	تيران
91320000	نير
26440000	بستان آباد
67330000	طارم زنجان
93360000	ابركوه ابرقو
45320000	راور
16340000	بابلسر
67320000	ايجرود
11390000	شهريار
21520000	مباركه
54440000	شفت گيلان
64360000	رودان هرمزگان
57320000	بوكان
41470000	لامرد
54410000	لامرد
41420000	ارسنجان
67360000	خرمدره
57380000	شاهين دژ
71420000	هرسين
91350000	پارس آباد
21510000	برخوار
95360000	كنگان بوشهر
81320000	سلسله
97320000	بندر تركمن
75350000	رزن
26320000	آذرشهر
26470000	كليبر
16410000	سوادكوه
71380000	صحنه
83320000	ايوان
81330000	پلدختر
83340000	آبدانان
31490000	قاينات قاين
75320000	اسد آبادهمدان
26430000	ملكان ملك كندى
31410000	خواف
54420000	ماسال
81350000	ازنا
91370000	كوثر
26450000	شبستر
54370000	املش
41340000	بوانات
41330000	خرم بيد
31340000	جاجرم
64340000	ابوموسي
26420000	بناب آذربايجان شرقي
15320000	بويين زهرا
21430000	آران وبيدگل
73350000	ديواندره
31390000	چناران
31480000	فريمان
57450000	اشنويه
36470000	اميديه خوزستان
54460000	سياهكل
91340000	بيله سوار
71340000	جوانرود
83360000	شيروان و چرداول
61370000	نيك شهر
11330000	اسلامشهر
31440000	سرخس
57340000	تكاب آذربايجان غربي
21360000	فريدن
91330000	نمين
45340000	بردسير
31530000	برداسكن
77320000	اردل
16430000	جويبار
16370000	محمودآباد مازندران
75330000	بهار
26380000	جلفا
11320001	كشورى
11320002	ايران
11350000	فيروزكوه
11380000	شميرانات
11410000	رباط كريم
11440000	پاكدشت
15340000	آبيك
16470000	نكا
21530000	چادگان
26330000	اسكو
26480000	هريس
26490000	عجب شير
26510000	چاراويماق
31590000	رشتخوار
32320000	جاجرم
32330000	مانه وسملقان
32340000	اسفراين
32350000	شيروان خراسان
33320000	نهبندان
33330000	سربيشه
33340000	قاينات قاين
41510000	حاجي آباد فارس
45430000	منوجان كرمان
57420000	چالدران
61380000	راسك
64350000	حاجي آباد هرمزگان
64390000	بستك
67340000	ماه نشان
71430000	ثلاث باباجاني
73390000	سروآباد
77360000	كوهرنكٌ
81370000	دلفان
85340000	دنا
93320000	صدوق اشكذر
93390000	طبس
93410000	خاتم
95380000	ديلم
97360000	مينودشت گلستان
97380000	آق قلا
97390000	آزادشهر
97410000	راميان
97420000	كلاله";
            #endregion
            //mPlateCity.Items.Clear();
            //string[] lines = nice.Split('\n');
            //for (int i = 0; i < lines.Length; i++)
            //{
            //    string[] valTxt = lines[i].Trim().Split('\t');
            //    if (valTxt.Length == 2)
            //        mPlateCity.Items.Add(new ListItem(valTxt[1], valTxt[0]));
            //}
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            //City iran = new City();
            //iran.ID = 0;
            //iran.Title = "ایران";
            //all.Insert(0, iran);
            mPlateCity.DataValueField = "Title";
            mPlateCity.DataTextField = "Title";
            mPlateCity.DataSource = all;
            mPlateCity.DataBind();
        }
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.CarsCompanies
                  orderby c.Code
                  select c;

        if (sSmartCard.Text.Trim().Length > 0)
            all = all.Where(c => c.SmartCard.Contains(sSmartCard.Text.Trim())).OrderBy(c => c.Code);
        if (sTransitNo.Text.Trim().Length > 0)
            all = all.Where(c => c.TransitNo.Contains(sTransitNo.Text.Trim())).OrderBy(c => c.Code);
        if (sEnabled.Checked && !sEnabledNo.Checked)
            all = all.Where(c => c.Enabled == true).OrderBy(c => c.Code);
        else if (!sEnabled.Checked && sEnabledNo.Checked)
            all = all.Where(c => c.Enabled == false).OrderBy(c => c.Code);
        string plate = string.Format("{0}-{1}-{2}-{3}"
            , sPlate0.Text.Length == 0 ? "%" : sPlate0.Text
            , sPlate1.Text.Length == 0 ? "%" : sPlate1.Text
            , sPlate2.Text.Length == 0 ? "%" : sPlate2.Text
            , sPlate3.Text.Length == 0 ? "%" : sPlate3.Text);
        List<CarsCompany> cars = all.ToList();
        if (plate != "%-%-%-%")
            if (!plate.Contains('%'))
                for (int i = 0; i < cars.Count; i++)
                {
                    if (cars[i].Plate != plate)
                    {
                        cars.RemoveAt(i);
                        i--;
                    }
                }
            else
                for (int i = 0; i < cars.Count; i++)
                {
                    try
                    {
                        if (sPlate0.Text.Length > 0 && cars[i].Plate.Split('-')[0] != sPlate0.Text)
                        {
                            cars.RemoveAt(i);
                            i--;
                        }
                        else if (sPlate1.Text.Length > 0 && cars[i].Plate.Split('-')[1] != sPlate1.Text)
                        {
                            cars.RemoveAt(i);
                            i--;
                        }
                        else if (sPlate2.Text.Length > 0 && cars[i].Plate.Split('-')[2] != sPlate2.Text)
                        {
                            cars.RemoveAt(i);
                            i--;
                        }
                        else if (sPlate3.Text.Length > 0 && cars[i].Plate.Split('-')[3] != sPlate3.Text)
                        {
                            cars.RemoveAt(i);
                            i--;
                        }
                    }
                    catch
                    {
                        cars.RemoveAt(i);
                        i--;
                    }
                }
        cars.Sort(Compare);
        list.DataSource = cars;
        list.DataBind();
    }
    int Compare(CarsCompany a, CarsCompany b)
    {
        return Tool.GetInt(a.Code, 0).CompareTo(Tool.GetInt(b.Code, 0));
    }
    protected void ReadCard_Click(object sender, EventArgs e)
    {
        puEx.Show();
    }
    protected void puCancel_Click(object sender, EventArgs e)
    {
        if (FromClose)
        {
            Response.Redirect("~/Close.aspx?tripid=" + Tool.GetInt(Request.QueryString["TripID"], 0));
        }
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        CarsCompany info;
        if (Null.NullInteger == EditID)
            info = new CarsCompany();
        else
            info = Helper.Instance.GetCarsCompany(EditID);
        if (info == null)
            return;

        if (mVisitDate.Text.Length > 0 && !Tool.ValidatePersianDate(mVisitDate.Text))
        {
            mError.Text = "تاريخ معاینه فني اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        int year = Tool.GetInt(mProductionYear.Text, 0);
        if (year < 1300 || year > 1500)
        {
            mError.Text = "سال توليد اشتباه ميباشد. سال باید بین 1300 و 1500 باشد";
            puEx.Show();
            return;
        }
        if (mInsuranceDate.Text.Length > 0 && !Tool.ValidatePersianDate(mInsuranceDate.Text))
        {
            mError.Text = "تاريخ بيمه بدنه اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (mThirdPersonDate.Text.Length > 0 && !Tool.ValidatePersianDate(mThirdPersonDate.Text))
        {
            mError.Text = "تاريخ بيمه شخص ثالث اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (mContractDate.Text.Length > 0 && !Tool.ValidatePersianDate(mContractDate.Text))
        {
            mError.Text = "تاريخ قرارداد اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        
        info.Code = mCode.Text;

        info.TransitNo = mTransitNo.Text;
        info.ThirdPersonNo = mThirdPersonNo.Text;
        //info.Plate = string.Format("{0}-{1}", mPlate.Text, mPlateSeries.Text.PadLeft(2, '0'));
        info.Plate = string.Format("{0}-{1}-{2}-{3}", mPlate0.Text.PadLeft(2, '0'), mPlate1.Text.PadLeft(3, '0'), mPlate2.Text, mPlate3.Text.PadLeft(2, '0'));
        if (info.Plate.Contains('0'))
        {
            mError.Text = "شماره پلاک اشتباه میباشد";
            puEx.Show();
            return;
        }

        if (mSmartCard.Text.Trim().Length > 0)
        {
            CarsCompany other = Helper.Instance.GetCarsCompany(mSmartCard.Text, string.Format("{0}-{1}-{2}-{3}", mPlate0.Text.PadLeft(2, '0'), mPlate1.Text.PadLeft(3, '0'), mPlate2.Text, mPlate3.Text.PadLeft(2, '0')),info.CompanyID);
            if (other != null && other.ID != info.ID)
            {
                mError.Text = "کارت هوشمند وارد شده تکراری میباشد.";
                puEx.Show();
                return;
            }
        }
        info.VIN = mVIN.Text;
        info.PlateCity = mPlateCity.SelectedValue;//, Null.NullInteger).ToString(); //mPlateCity.Text;// 
        info.SmartCard = mSmartCard.Text;
        info.VisitDate = Tool.ParsePersianDate(mVisitDate.Text, DateTime.Now);
        info.InsuranceDate = Tool.ParsePersianDate(mInsuranceDate.Text, DateTime.Now);
        info.ThirdPersonDate = Tool.ParsePersianDate(mThirdPersonDate.Text, DateTime.Now);
        info.ProductionDate = Tool.ParsePersianDate(mProductionYear.Text + "/06/06", DateTime.Now);
        info.Commission = Tool.GetDecimal(mCommission.Text, 0);
        info.Deleted = mDeleted.Checked;
        info.ContractDate = Tool.ParsePersianDate(mContractDate.Text, DateTime.Now);
        info.Enabled = mEnabled.Checked;
        info.Comments = mComments.Text;

        City sne = null;
        try
        {
            var c = from cs in Helper.Instance.DB.Cities
                    where cs.Code == "73310000"
                    select cs;
            sne = c.First();
        }
        catch
        {
        }
        if (sne != null && mPlate0.Text == "51")
            info.PlateCity = sne.Title;
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.CarsCompanies.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            if (FromClose)
            {
                Response.Redirect("~/Close.aspx?tripid=" + Tool.GetInt(Request.QueryString["TripID"], 0));
            }
            if (FromSale)
            {
                // Response.Redirect("~/sale2.aspx");
            }
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }

    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CarsCompany info = e.Row.DataItem as CarsCompany;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Company.Title;
                e.Row.Cells[4].Text = info.PlateCity;// ? "X" : "";
                e.Row.Cells[10].Text = info.Enabled ? "X" : "";

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        CarsCompany info = Helper.Instance.GetCarsCompany(EditID);
        if (info == null)
            return;

        mTransitNo.Text = info.TransitNo;
        mThirdPersonNo.Text = info.ThirdPersonNo;
        string plate = Tool.ConvertPlate(info.Plate);
        string[] split = null;
        if (!string.IsNullOrEmpty(plate))
            split = plate.Split('-');
        if (split == null || split.Length != 4)
        {
            mPlate0.Text = mPlate1.Text = mPlate3.Text = "";
        }
        else
        {
            mPlate0.Text = split[0];
            mPlate1.Text = split[1];
            mPlate2.SelectedValue = split[2];
            mPlate3.Text = split[3];
        }
        mCode.Text = info.Code;
        mVIN.Text = info.VIN;
        //Tool.SetSelected(mPlateCity, "11320002"); //ایران
        Tool.SetSelected(mPlateCity, info.PlateCity);//mPlateCity.Text = info.PlateCity;//Tool.SetSelected( mPlateCity, info.PlateCity);
        mSmartCard.Text = info.SmartCard;
        mVisitDate.Text = Tool.ToPersianDate(info.VisitDate, "");
        mInsuranceDate.Text = Tool.ToPersianDate(info.InsuranceDate, "");
        mThirdPersonDate.Text = Tool.ToPersianDate(info.ThirdPersonDate, "");
        mProductionYear.Text = Tool.ToPersianYear(info.ProductionDate, "");
        mCommission.Text = info.Commission.ToString();
        mDeleted.Checked = info.Deleted;
        mContractDate.Text = Tool.ToPersianDate(info.ContractDate, "");
        mEnabled.Checked = info.Enabled;
        mComments.Text = info.Comments;
        mCompanyName.Text = info.Company.Title;
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        CarsCompany info = Helper.Instance.GetCarsCompany(id);
        if (Helper.Instance.DeleteCarsCompany(info))
        {
            mMsg.Text = "اتوکار با موفقیت حذف گردید";
            mMsg.Visible = true;
            BindData();
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void puClose_Click(object sender, EventArgs e)
    {
        if (FromClose)
        {
            Response.Redirect("~/Close.aspx?tripid=" + Tool.GetInt(Request.QueryString["TripID"], 0));
        }
        if (FromSale)
        {
            // Response.Redirect("~/sale2.aspx");
        }
        EditID = Null.NullInteger;
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;

        mCode.Text = mThirdPersonNo.Text = mTransitNo.Text = mPlate0.Text = mPlate1.Text = mPlate2.Text = mVIN.Text
             = mSmartCard.Text = mVisitDate.Text = mVisitDate.Text = mInsuranceDate.Text
             = mThirdPersonDate.Text = mCommission.Text = mContractDate.Text
             = mComments.Text = "";
        //mPlateCity.Text = "ایران";
        mContractDate.Text = mThirdPersonDate.Text = mInsuranceDate.Text =
            mVisitDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        mProductionYear.Text = Tool.ToPersianYear(DateTime.Now, "");
        mEnabled.Checked = true;
        mDeleted.Checked = false;
        mCommission.Text = Helper.Instance.GetSettingValue("Commission");
        if (mCommission.Text == "")
            mCommission.Text = "15";
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    
    protected void doPrint_Click(object sender, EventArgs e)
    {
        string fDate = "";
        string tDate = "";
        int specialID = 0;
        int serviceID = 0;
        int driverID1 = 0;
        int carTypeID = 0;
        int carID = 0;
        int destID = 0;
        int fundID = 0;
        int fno = 0;
        int tno = 0;
        string series = "";

        Response.Redirect(
            string.Format("~/Reports/ReportNew.aspx?fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}&repname={13}&repTitle={14}&fullName={15}&repParam={16}",
            fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, 0, "Car", "", "", ""), true);

    }
    protected void doGetDataFromServer_Click(object sender, EventArgs e)
    {
        string plate = string.Format("{2}{1}{0}", mPlate1.Text, mPlate2.Text, mPlate3.Text);
        string serie = mPlate0.Text;
        if (string.IsNullOrEmpty(mSmartCard.Text) && string.IsNullOrEmpty(mVIN.Text))
        {
            mError.Text = "لطفا 'شماره کارت هوشمند'  یا `شماره پلاک` را وارد کنید.";
            puEx.Show();
            return;
        }
        try
        {
            string value = GetCarNew(mSmartCard.Text, mVIN.Text, "");
            if (value.Length > 0)
            {
                /* 17ع771		0	plate
                ;87		1	plateno
                ;1381		2	build year
                ;1		3	enabled
                ;I  		4	plate type
                ;1915695		5	smart card
                ;IRGC817A2SB001729	6	vin
                ;			7	type
                ;1			8	
                ;بنز			9
                ;اتوبوس ويژه		10
                ;16			11
                ;C457			12
                                 */
                string[] resualt = value.Split(';');
                mPlate0.Text = resualt[3].Substring(resualt[3].IndexOf(':') + 1);
                mPlate1.Text = resualt[5].Substring(resualt[5].IndexOf(':') + 1);
                mPlate2.Text = resualt[6].Substring(resualt[6].IndexOf(':') + 1);
                mPlate3.Text = resualt[7].Substring(resualt[7].IndexOf(':') + 1);
                mProductionYear.Text = resualt[9].Substring(resualt[9].IndexOf(':') + 1);
                mEnabled.Checked = resualt[8].Substring(resualt[8].IndexOf(':') + 1) == "1";
                mSmartCard.Text = resualt[1].Substring(resualt[1].IndexOf(':') + 1);
                mVIN.Text = resualt[2].Substring(resualt[2].IndexOf(':') + 1);
            }


        }
        catch (Exception ex)
        {
            mError.Text = "خطا در دریافت اطلاعات از سرور: " + ex.Message;
        }
        puEx.Show();
    }
    protected void doGetCar_Click(object sender, EventArgs e)
    {
        string plate = string.Format("{2}{1}{0}", cPlate1.Text, cPlate2.Text, cPlate3.Text);
        string serie = cPlate0.Text;
        if (serie.Trim().Length == 0)
            plate = "";
        if (string.IsNullOrEmpty(cSmartCard.Text) && string.IsNullOrEmpty(cVIN.Text) && string.IsNullOrEmpty(cNo.Text))
        {
            mMsg.Text = "لطفا 'شماره کارت هوشمند'  یا `شماره خدرو` یا 'شماره پرونده' را وارد کنید.";
            return;
        }
        try
        {
            string value = GetCarNew(cSmartCard.Text, cVIN.Text, cNo.Text);
            if (value.Length > 0)
            {
                string[] resualt = value.Split(';');
                string msg = "سری پلاک: " + resualt[3].Substring(resualt[3].IndexOf(':') + 1);
                msg += string.Format("<br />شماره پلاک: {0}-{1}-{2}", resualt[5].Substring(resualt[5].IndexOf(':') + 1), resualt[6].Substring(resualt[6].IndexOf(':') + 1), resualt[7].Substring(resualt[7].IndexOf(':') + 1));
                msg += "<br />وضعیت: " + (resualt[8].Substring(resualt[8].IndexOf(':') + 1) == "1" ? "فعال" : "غیر فعال");
                msg += "<br />شماره کارت هوشمند: " + resualt[1].Substring(resualt[1].IndexOf(':') + 1);
                msg += "<br />شماره خودرو: " + resualt[2].Substring(resualt[2].IndexOf(':') + 1);
                msg += "<br />شماره پرونده: " + resualt[0].Substring(resualt[0].IndexOf(':') + 1);
                mMsg.Text = msg;
            }
        }
        catch (Exception ex)
        {
            mMsg.Text = "خطا در دریافت اطلاعات از سرور: " + ex.Message;
        }
    }
    private void EditCar(int CarID)
    {
        EditID = CarID;
        if (Null.NullInteger == EditID)
            return;
        CarsCompany info = Helper.Instance.GetCarsCompany(EditID);
        if (info == null)
            return;

        mTransitNo.Text = info.TransitNo;
        mThirdPersonNo.Text = info.ThirdPersonNo;
        string plate = Tool.ConvertPlate(info.Plate);
        string[] split = null;
        if (!string.IsNullOrEmpty(plate))
            split = plate.Split('-');
        if (split == null || split.Length != 4)
        {
            mPlate0.Text = mPlate1.Text = mPlate3.Text = "";
        }
        else
        {
            mPlate0.Text = split[0];
            mPlate1.Text = split[1];
            mPlate2.SelectedValue = split[2];
            mPlate3.Text = split[3];
        }
        mCode.Text = info.Code;
        mVIN.Text = info.VIN;
        //Tool.SetSelected(mPlateCity, "11320002"); //ایران
        Tool.SetSelected(mPlateCity, info.PlateCity);//mPlateCity.Text = info.PlateCity;//Tool.SetSelected( mPlateCity, info.PlateCity);
        mSmartCard.Text = info.SmartCard;
        mVisitDate.Text = Tool.ToPersianDate(info.VisitDate, "");
        mInsuranceDate.Text = Tool.ToPersianDate(info.InsuranceDate, "");
        mThirdPersonDate.Text = Tool.ToPersianDate(info.ThirdPersonDate, "");
        mProductionYear.Text = Tool.ToPersianYear(info.ProductionDate, "");
        mCommission.Text = info.Commission.ToString();
        mDeleted.Checked = info.Deleted;
        mContractDate.Text = Tool.ToPersianDate(info.ContractDate, "");
        mEnabled.Checked = info.Enabled;
        mComments.Text = info.Comments;

        puEx.Show();
    }

    private string GetCarNew(string SmartCard, string VIN, string FileNo)
    {
        string value = "";
        authentication();
        if (!string.IsNullOrEmpty(SmartCard))
            value = car.GET_PASSENGER_BY_SHC(SmartCard);
        else if (!string.IsNullOrEmpty(VIN))
        {
            value = car.GET_PASSENGER_BY_VIN(VIN);
        }
        else if (!string.IsNullOrEmpty(FileNo))
        {
            value = car.GET_PASSENGER_BY_SHP(FileNo);
        }
        return value;
    }
    private void authentication()
    {
        String user = "tr_web_service";
        String password = "tr_web_service123";


        System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
        System.Net.NetworkCredential netCred = new System.Net.NetworkCredential(user, password);
        myCredentials.Add(new Uri(car.Url), "Basic", netCred);
        car.Credentials = myCredentials;
    }
    protected void doUpdate_Click(object sender, EventArgs e)
    {
        List<Company> companies = Helper.Instance.GetCompanies();
        for (int i = 0; i < companies.Count; i++)
        {
            string query = "SELECT * FROM Cars WHERE Enabled=1";
            IDataReader dr = DbHelper.Instance.ExecuteReader(companies[i].ConnectionString, query);
            List<Car> cars = ObjectBinder.FillCollection<Car>(dr);
            for (int j = 0; j < cars.Count; j++)
            {
                if (!string.IsNullOrEmpty(cars[j].SmartCard))
                    if (Helper.Instance.GetCarsCompany(cars[j].SmartCard, cars[j].Plate,companies[i].ID) == null)
                    {
                        CarsCompany carCompany = new CarsCompany();
                        carCompany.CompanyID = companies[i].ID;
                        carCompany.TransitNo = cars[j].TransitNo;
                        carCompany.InsuranceDate = cars[j].InsuranceDate;
                        carCompany.Plate = cars[j].Plate;
                        carCompany.PlateCity = cars[j].PlateCity;
                        carCompany.VIN = cars[j].VIN;
                        carCompany.SmartCard = cars[j].SmartCard;
                        carCompany.ThirdPersonDate = cars[j].ThirdPersonDate;
                        carCompany.ThirdPersonNo = cars[j].ThirdPersonNo;
                        carCompany.VisitDate = cars[j].VisitDate;
                        carCompany.ProductionDate = cars[j].ProductionDate;
                        carCompany.ContractDate = cars[i].ContractDate;
                        carCompany.Comments = cars[i].Comments;
                        carCompany.Enabled = cars[i].Enabled;
                        Helper.Instance.DB.CarsCompanies.InsertOnSubmit(carCompany);
                        Helper.Instance.Update();
                    }
            }
        }
        BindData();
    }
}