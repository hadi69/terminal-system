﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Technical_CarTechnical : System.Web.UI.Page
{
    CarsCompany mCarCompany = new CarsCompany();
    CarTechnical mCarTechnical=new CarTechnical();
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int carcompanyID = Tool.GetInt(Request.QueryString["CarCompanyID"], Null.NullInteger);
        if (carcompanyID!=Null.NullInteger)
        {
            mCarCompany = Helper.Instance.GetCarsCompany(carcompanyID);
            mCarTechnical = Helper.Instance.GetCarTechnical(mCarCompany.SmartCard, mCarCompany.Plate, Tool.ToPersianDate(DateTime.Now, DateTime.Now));
        }
        
        int carTechnicalID = Tool.GetInt(Request.QueryString["CarTechnicalID"], Null.NullInteger);
        if (carTechnicalID!=Null.NullInteger)
        {
            mCarTechnical = Helper.Instance.GetCarTechnical(carTechnicalID);
        }
        
        if (!IsPostBack)
        {
            if (mCarTechnical!=null)
            {
                mSmartCard.Text = mCarTechnical.SmartCard;
                mDate.Text = mCarTechnical.Date;
                mPlate.Text = mCarTechnical.Plate;
                mComment.Text = mCarTechnical.Comment;
                if (mCarTechnical.FaniDetail.Length == 16)
                {
                    BindData();
                }
            }
            else if (mCarCompany != null)
            {
                mSmartCard.Text = mCarCompany.SmartCard;
                mDate.Text = Tool.ToPersianDate(DateTime.Now, DateTime.Now);
                mPlate.Text = mCarCompany.Plate;
            }
        }
    }

    void BindData()
    {
        string fani = mCarTechnical.FaniDetail;
        if (fani.Substring(0, 1) == "1")
        {
            Accept1.Checked = true;
        }
        else
        {
            NoAccept1.Checked = true;
        }
        if (fani.Substring(1, 1) == "1")
        {
            Accept2.Checked = true;
        }
        else
        {
            NoAccept2.Checked = true;
        }
        if (fani.Substring(2, 1) == "1")
        {
            Accept3.Checked = true;
        }
        else
        {
            NoAccept3.Checked = true;
        }
        if (fani.Substring(3, 1) == "1")
        {
            Accept4.Checked = true;
        }
        else
        {
            NoAccept4.Checked = true;
        }
        if (fani.Substring(4, 1) == "1")
        {
            Accept5.Checked = true;
        }
        else
        {
            NoAccept5.Checked = true;
        }
        if (fani.Substring(5, 1) == "1")
        {
            Accept6.Checked = true;
        }
        else
        {
            NoAccept6.Checked = true;
        }
        if (fani.Substring(6, 1) == "1")
        {
            Accept7.Checked = true;
        }
        else
        {
            NoAccept7.Checked = true;
        }
        if (fani.Substring(7, 1) == "1")
        {
            Accept8.Checked = true;
        }
        else
        {
            NoAccept8.Checked = true;
        }
        if (fani.Substring(8, 1) == "1")
        {
            Accept9.Checked = true;
        }
        else
        {
            NoAccept9.Checked = true;
        }
        if (fani.Substring(9, 1) == "1")
        {
            Accept10.Checked = true;
        }
        else
        {
            NoAccept10.Checked = true;
        }
        if (fani.Substring(10, 1) == "1")
        {
            Accept11.Checked = true;
        }
        else
        {
            NoAccept11.Checked = true;
        }
        if (fani.Substring(11, 1) == "1")
        {
            Accept12.Checked = true;
        }
        else
        {
            NoAccept12.Checked = true;
        }
        if (fani.Substring(12, 1) == "1")
        {
            Accept13.Checked = true;
        }
        else
        {
            NoAccept13.Checked = true;
        }
        if (fani.Substring(13, 1) == "1")
        {
            Accept14.Checked = true;
        }
        else
        {
            NoAccept14.Checked = true;
        }
        if (fani.Substring(14, 1) == "1")
        {
            Accept15.Checked = true;
        }
        else
        {
            NoAccept15.Checked = true;
        }
        if (fani.Substring(15, 1) == "1")
        {
            Accept16.Checked = true;
        }
        else
        {
            NoAccept16.Checked = true;
        }
    }
    protected void doConfirm_Click(object sender, EventArgs e)
    {
        if (mCarTechnical==null)
        {
            mCarTechnical=new CarTechnical();
        }
        mCarTechnical.SmartCard = mSmartCard.Text;
        mCarTechnical.Plate = mPlate.Text;
        mCarTechnical.FaniDetail = FaniResult();
        mCarTechnical.TechnicalStatus = (int) TechnicalStatus.Confirm;
        mCarTechnical.Date = mDate.Text;
        mCarTechnical.UserName = SiteSettings.User.FullName;
        mCarTechnical.Comment = mComment.Text;
        if (mCarTechnical.ID==0)
        {
            Helper.Instance.DB.CarTechnicals.InsertOnSubmit(mCarTechnical);
        }
        if (Helper.Instance.Update())
        {
            Response.Redirect("CarsCompany.aspx");
        }
    }

    string FaniResult()
    {
        string FaniDetail = "";
        if (Accept1.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept2.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept3.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept4.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept5.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept6.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept7.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept8.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept9.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept10.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept11.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept12.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept13.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept14.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept15.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }
        if (Accept16.Checked)
        {
            FaniDetail += "1";
        }
        else
        {
            FaniDetail += "0";
        }

        return FaniDetail;
    }
    protected void doReject_Click(object sender, EventArgs e)
    {
        mCarTechnical.SmartCard = mSmartCard.Text;
        mCarTechnical.Plate = mPlate.Text;
        mCarTechnical.FaniDetail = FaniResult();
        mCarTechnical.TechnicalStatus = (int)TechnicalStatus.Reject;
        mCarTechnical.Date = mDate.Text;
        mCarTechnical.UserName = SiteSettings.User.FullName;
        mCarTechnical.Comment = mComment.Text;
        if (mCarTechnical.ID == -1)
        {
            Helper.Instance.DB.CarTechnicals.InsertOnSubmit(mCarTechnical);
        }
        if (Helper.Instance.Update())
        {
            Response.Redirect("CarsCompany.aspx");
        }
    }
    protected void doCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("CarsCompany.aspx");
    }
}