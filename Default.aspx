﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" Title="سيستم پايانه" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <div style="text-align: center">
        <div style="text-align: center;">
            <br />
            &nbsp;<br />
            <br />
            <br />
            <span style="font-family: Arial; font-size: 2cm; font-weight: bold; color: #808080">
                <asp:Label runat="server" ID="mCompanyName" Text='تعاونی 12' /></span>
            <br />
            <br />
            <br />
        </div>
        <table border="0" width="100%">
            <tr>
                <td style="text-align: center">
                    <table runat="server" border="0" id='logined' style="width: 350px">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="userDisplay" CssClass="MainPageLink"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="MainPageLink">
                                خوش آمديد
                            </td>
                        </tr>
                    </table>
                    <table border="0" runat="server" id='notLogined' style="width: 350px">
                        <tr>
                            <td class="N" colspan="2">
                                ورود به سيستم
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام کاربري
                            </td>
                            <td>
                                <asp:TextBox ID="userName" runat="server" class="T"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="userName"
                                    ErrorMessage="نام کاربري الزامي است" ValidationGroup="login"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                رمز عبور
                            </td>
                            <td>
                                <asp:TextBox ID="password" runat="server" class="T" TextMode="Password"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ControlToValidate="password"
                                    ErrorMessage="رمز عبور الزامي است" ValidationGroup="login"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td align="left">
                                <asp:Button ID="doLogin" runat="server" CssClass="CB" OnClick="doLogin_Click" Text="ورود"
                                    Width="50px" ValidationGroup="login" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="mUserError" runat="server" ForeColor="Red" Text="نام کاربري يا رمز عبور اشتباه است"
                                    Visible="False"></asp:Label>
                                <asp:Label ID="mDBError" runat="server" ForeColor="Red" Text="خطا در وصل شدن به ديتابيس يا قفل"
                                    Visible="False"></asp:Label>
                                <asp:Label ID="mLockError" runat="server" ForeColor="Red" Text="خطا در وصل شدن به قفل يا ديتابيس"
                                    Visible="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
