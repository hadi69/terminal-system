﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TrackingReport.aspx.cs" Inherits="TrackingReport" %>

<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <table border="0" class="filter" width="95%">
        <tr>
            <td colspan="6">
                <asp:Label ID="mError" runat="server" CssClass="Err"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                جستجو
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td colspan="3" style="text-align: left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                سري صورت
            </td>
            <td>
                <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px"  />
            </td>
            <td>
                از شماره صورت
            </td>
            <td>
                <asp:TextBox ID="sFNo" runat="server" CssClass="T" Width="150px" />
            </td>
            <td>
                تا شماره صورت
            </td>
            <td>
                <asp:TextBox ID="sTNo" runat="server" CssClass="T" Width="150px" />
            </td>
        </tr>
        <tr>
            <td>
                از تاريخ
            </td>
            <td>
                <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
            </td>
            <td>
                تا تاريخ
            </td>
            <td>
                <table border="0">
                    <tr>
                        <td>
                            <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                    Width="50px" /><a href='#' class="CB" style='display:inline-block;width:50px' id="printBime" onclick='showHtml(event)'>چاپ</a>
                <input type="hidden" runat="server" id="printBimeQryDisabled" class="printBimeQryDisabled" />
                <input type="hidden" runat="server" id="printBimeQry" class="printBimeQry" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Label ID="mMsg" runat="server" CssClass="Err"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
        Style="margin-top: 0px" Width="95%">
        <Columns>
            <asp:BoundField DataField="ID" Visible="False" />
            <asp:BoundField DataField="ID" HeaderText="رديف" />
            <asp:BoundField DataField="Date" HeaderText="تاریخ" />
            <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />            
            <asp:BoundField DataField="No" HeaderText="شماره صورت" />
            <asp:BoundField DataField="Series" HeaderText="سری صورت " />
            <asp:BoundField DataField="TrackCode" HeaderText="کد رهگیری" />
            <asp:BoundField DataField="CloseDate" HeaderText="ساعت بستن" />
            <asp:BoundField DataField="UserName" HeaderText="کاربر" />
        </Columns>
        <EmptyDataTemplate>
            <br />
            <span class="BErr">هيچ سرويس دوره ای در بازه مورد نظر وجود ندارد</span></EmptyDataTemplate>
        <HeaderStyle CssClass="GH" />
        <RowStyle CssClass="GR" />
        <AlternatingRowStyle CssClass="GAR" />
    </asp:GridView>
    <br />
    <asp:GridView ID="listE" runat="server" AutoGenerateColumns="False" OnRowDataBound="listE_RowDataBound"
        Style="margin-top: 0px" Width="95%" AllowPaging="False" OnPageIndexChanging="listE_PageIndexChanging"
        PageSize="20">
        <Columns>
            <asp:BoundField DataField="ID" Visible="False" />
            <asp:BoundField DataField="ID" HeaderText="رديف" />
            <asp:BoundField DataField="DepartureDate" HeaderText="تاریخ" />
            <asp:BoundField DataField="DepartureTime" HeaderText="ساعت" />
            <asp:BoundField DataField="No" HeaderText="شماره صورت" />
            <asp:BoundField DataField="Series" HeaderText="سری صورت " />
            <asp:BoundField DataField="TrackCode" HeaderText="کد رهگیری" />
            <asp:BoundField DataField="CloseDate" HeaderText="ساعت بستن" />
            <asp:BoundField DataField="UserName" HeaderText="کاربر" />
        </Columns>
        <EmptyDataTemplate>
            <br />
            <span class="BErr">هيچ سرويس دربستی در بازه مورد نظر وجود ندارد</span></EmptyDataTemplate>
        <HeaderStyle CssClass="GH" />
        <RowStyle CssClass="GR" />
        <AlternatingRowStyle CssClass="GAR" />
    </asp:GridView>
    <div style="display: none">
        <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>

    <script type="text/javascript" language="javascript">
        function showHtml(event) {
            //alert(1);
            //event.preventDefault();
            //alert(2);
            if ($('.printBimeQryDisabled').val() == "1")
                return;
            var url = "insurancehtml.aspx?" + $('.printBimeQry').val();
            //alert(url);
            window.open(url, "Zweitfenster", "location=1,status=1,scrollbars=1,width=1000,height=600,left=10,top=10")
            return false;
        }
    </script>

</asp:Content>
