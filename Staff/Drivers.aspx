﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Drivers.aspx.cs" Inherits="Staff_Drivers" Title="رانندگان" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td colspan="8">
                        اعتبارسنجی
                    </td>
                </tr>
                <tr>
                    <td>
                        راننده:
                    </td>
                    <td>
                        شماره کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="dSmartCard" runat="server" />
                    </td>
                    <td>
                        شماره پرونده
                    </td>
                    <td>
                        <asp:TextBox ID="dLicenceNo" runat="server" />
                    </td>
                    <td>
                        کد ملي
                    </td>
                    <td>
                        <asp:TextBox ID="dIDNo" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="doGetDriver" runat="server" CssClass="CB" OnClick="doGetDriver_Click"
                            Text="دریافت اطلاعات از تهران" Width="150px" />
                    </td>
                </tr>
             </table>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabled" Text='نمایش فعالها' Checked="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabledNo" Text='نمایش غیرفعالها' />
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        نام
                    </td>
                    <td>
                        <asp:TextBox ID="sName" runat="server" />
                    </td>
                    <td>
                        نام خانوادگي
                    </td>
                    <td>
                        <asp:TextBox ID="sSurname" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="doPrint" runat="server" CssClass="CB" OnClick="doPrint_Click" Text="چاپ"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        شماره گواهينامه
                    </td>
                    <td>
                        <asp:TextBox ID="sLicenceNo" runat="server" />
                    </td>
                    <td>
                        شماره کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="sُSmartCard" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Code" HeaderText="کد" />
                    <asp:BoundField DataField="SmartCard" HeaderText="شماره کارت هوشمند" />
                    <asp:BoundField DataField="Name" HeaderText="نام" />
                    <asp:BoundField DataField="Surname" HeaderText="نام خانوادگي" />
                    <asp:BoundField DataField="LicenceNo" HeaderText="شماره گواهينامه" />
                    <asp:BoundField DataField="DrivingNo" HeaderText="شماره کارت رانندگي" />
                    <asp:BoundField DataField="DrivingDate2" HeaderText="تاریخ کارت سلامت" />
                    <asp:BoundField DataField="Tel" HeaderText="تلفن" />
                    <asp:BoundField DataField="CellNo" HeaderText="موبايل" />
                    <asp:BoundField DataField="CarID" HeaderText="ماشین" />
                    <asp:BoundField DataField="Enabled" HeaderText="نمایش" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="" ID="puPanel" runat="server" Width="700px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش راننده)</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                کد
                            </td>
                            <td>
                                <asp:TextBox ID="mCode" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره کارت هوشمند
                            </td>
                            <td>
                                <asp:TextBox ID="mSmartCard" runat="server" />
                            </td>
                            <td>                            
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="شماره کارت هوشمند الزامي است"
                                    ControlToValidate="mSmartCard" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator Display = "Dynamic" ValidationGroup="addGroup" ControlToValidate = "mSmartCard" ID="rev1" ValidationExpression = "^[\s\S]{7,}$" runat="server" ErrorMessage="شماره کارت نباید کمتر از 7 رقم باشد"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                عنوان
                            </td>
                            <td>
                                <asp:RadioButton ID="mManYes" runat="server" Text="آقا" GroupName="Man" Checked="true" />
                                <asp:RadioButton ID="mManNo" runat="server" Text="خانم" GroupName="Man" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام
                            </td>
                            <td>
                                <asp:TextBox ID="mName" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvr2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="نام الزامي است" ControlToValidate="mName" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام خانوادگي
                            </td>
                            <td>
                                <asp:TextBox ID="mSurname" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvr3" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="نام خانوادگي الزامي است" ControlToValidate="mSurname" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام پدر
                            </td>
                            <td>
                                <asp:TextBox ID="mFather" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره گواهينامه
                            </td>
                            <td>
                                <asp:TextBox ID="mLicenceNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شهر گواهينامه
                            </td>
                            <td>
                                <asp:TextBox ID="mLicenceCity" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ اتمام اعتبار گواهينامه
                            </td>
                            <td>
                                <asp:TextBox ID="mLicenceExpireDate" runat="server" />
                                <cc1:MaskedEditExtender ID="mee2" TargetControlID="mLicenceExpireDate" runat="server"
                                    Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد ملي
                            </td>
                            <td>
                                <asp:TextBox ID="mIDNo" runat="server"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تلفن همراه
                            </td>
                            <td>
                                <asp:TextBox ID="mCellNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تلفن
                            </td>
                            <td>
                                <asp:TextBox ID="mTel" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره کارت رانندگي
                            </td>
                            <td>
                                <asp:TextBox ID="mDrivingNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاریخ کارت هوشمند
                            </td>
                            <td>
                                <asp:TextBox ID="mSmartCardDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="mSmartCardDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاریخ دفترچه ساعت
                            </td>
                            <td>
                                <asp:TextBox ID="mHourDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="mee1" TargetControlID="mHourDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاریخ کارت سلامت
                            </td>
                            <td>
                                <asp:TextBox ID="mDrivingDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="mDrivingDate" runat="server"
                                    Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره گذرنامه
                            </td>
                            <td>
                                <asp:TextBox ID="mPassportNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد بيمه
                            </td>
                            <td>
                                <asp:TextBox ID="mInsuranceNo" runat="server"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شعبه بيمه
                            </td>
                            <td>
                                <asp:TextBox ID="mInsuranceBranch" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                ماشین پیشفرض
                            </td>
                            <td>
                                <asp:DropDownList ID="mCars" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            <asp:Button runat=server ID=doGetDataFromServer Text='دریافت اطلاعات از تهران' OnClick="doGetDataFromServer_Click" CssClass="CB" />
                                <asp:Button ID="ReadCard" runat="server" Text="خواندن از کارت" OnClick="ReadCard_Click"
                                    CssClass="CB" />
                                    
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puClose" OnClick="puClose_Click" runat="server" Text="بستن" CssClass="CB">
                                </asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label ID="mError" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>    
</asp:Content>
