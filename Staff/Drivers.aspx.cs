﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using ir.rmto.smartcard;

public partial class Staff_Drivers : System.Web.UI.Page
{
    ir.rmto.smartcard.PKG_WEB_SERVICESService driver = new ir.rmto.smartcard.PKG_WEB_SERVICESService();
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["do"] == "Edit")
            {
                FromClose = true;
                int DriverID = Tool.GetInt(Request.QueryString["DriverID"], 0);
                EditDriver(DriverID);
                puCancel.Enabled = false;
            }
            if (Request.QueryString["do"] == "SaleEdit")
            {
                FromSale = true;
                int DriverID = Tool.GetInt(Request.QueryString["DriverID"], 0);
                EditDriver(DriverID);
                puCancel.Enabled = false;
            }
        }
        string reg = @"
        <script language='javascript'>
function Check(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
	data = obj.SCGetFAMILY;
	document.getElementById('mSurname').value=data;
	data = obj.SCGetNAME;
	document.getElementById('mName').value=data;
	data = obj.SCGetCODE_MELI;
	document.getElementById('mIDNo').value=data;
	data = obj.SCGetSHOMARE_GAVAHINAMEH ;
	document.getElementById('mLicenceNo').value=data;
	data = obj.SCGetMAHAL_SODUR_GAVAHINAME;
	document.getElementById('mLicenceCity').value=data;
	data = obj.SCGetSOMARE_BIME;
	document.getElementById('mInsuranceNo').value=data;
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", ReadCard.ClientID)
          .Replace("mSmartCard", mSmartCard.ClientID)
          .Replace("mSurname", mSurname.ClientID)
          .Replace("mName", mName.ClientID)
          .Replace("mIDNo", mIDNo.ClientID)
          .Replace("mLicenceNo", mLicenceNo.ClientID)
          .Replace("mLicenceCity", mLicenceCity.ClientID)
          .Replace("mInsuranceNo", mInsuranceNo.ClientID);
        RegisterStartupScript("ReadClickScript", reg);
        ReadCard.Attributes.Add("OnClick", "Check()");
    }
    protected bool FromClose
    {
        get
        {
            return Tool.GetBool(ViewState["FromClose"], false);
        }
        set
        {
            ViewState["FromClose"] = value;
        }
    }
    protected bool FromSale
    {
        get
        {
            return Tool.GetBool(ViewState["FromSale"], false);
        }
        set
        {
            ViewState["FromSale"] = value;
        }
    }
    private void BindInitData()
    {
        {
            List<Car> all = Helper.Instance.GetCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            //none.Title = "";
            all.Insert(0, none);

            mCars.DataValueField = "ID";
            mCars.DataTextField = "Title";
            mCars.DataSource = all;
            mCars.DataBind();
        }
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Drivers
                  orderby c.Code
                  select c;
        if (sName.Text.Trim().Length > 0)
            all = all.Where(c => c.Name.Contains(sName.Text.Trim())).OrderBy(c => c.Code);
        if (sSurname.Text.Trim().Length > 0)
            all = all.Where(c => c.Surname.Contains(sSurname.Text.Trim())).OrderBy(c => c.Code);
        if (sLicenceNo.Text.Trim().Length > 0)
            all = all.Where(c => c.LicenceNo.Contains(sLicenceNo.Text.Trim())).OrderBy(c => c.Code);
        if (sُSmartCard.Text.Trim().Length > 0)
            all = all.Where(c => c.SmartCard.Contains(sُSmartCard.Text.Trim())).OrderBy(c => c.Code);
        if (sEnabled.Checked && !sEnabledNo.Checked)
            all = all.Where(c => c.Enabled == true).OrderBy(c => c.Code);
        else if (!sEnabled.Checked && sEnabledNo.Checked)
            all = all.Where(c => c.Enabled == false).OrderBy(c => c.Code);
        List<Driver> drivers = all.ToList();
        drivers.Sort(Compare);
        list.DataSource = drivers;
        list.DataBind();
    }
    int Compare(Driver a, Driver b)
    {
        return Tool.GetInt(a.Code, 0).CompareTo(Tool.GetInt(b.Code, 0));
    }
    protected void ReadCard_Click(object sender, EventArgs e)
    {
        puEx.Show();
    }
    protected void puCancel_Click(object sender, EventArgs e)
    {
        if (FromClose)
        {
            Response.Redirect("~/Close.aspx?tripid=" + Tool.GetInt(Request.QueryString["TripID"], 0));
        }
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Driver info;
        if (Null.NullInteger == EditID)
            info = new Driver();
        else
            info = Helper.Instance.GetDriver(EditID);
        if (info == null)
            return;

        if (mLicenceExpireDate.Text.Length > 0 && !Tool.ValidatePersianDate(mLicenceExpireDate.Text))
        {
            mError.Text = "تاريخ اتمام اعتبار گواهينامه اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (mSmartCardDate.Text.Length > 0 && !Tool.ValidatePersianDate(mSmartCardDate.Text))
        {
            mError.Text = "تاريخ کارت هوشمند اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (mHourDate.Text.Length > 0 && !Tool.ValidatePersianDate(mHourDate.Text))
        {
            mError.Text = "تاريخ دفترچه ساعت اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (mDrivingDate.Text.Length > 0 && !Tool.ValidatePersianDate(mDrivingDate.Text))
        {
            mError.Text = "تاريخ کارت سلامت اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (mSmartCard.Text.Trim().Length > 0)
        {
            Driver other = Helper.Instance.GetDriver(mSmartCard.Text);
            if (other != null && other.ID != info.ID)
            {
                mError.Text = "کارت هوشمند وارد شده تکراری میباشد.";
                puEx.Show();
                return;
            }
        }
        if (mCode.Text.Trim().Length > 0)
        {
            Driver other = Helper.Instance.GetDriverByCode(mCode.Text);
            if (other != null && other.ID != info.ID)
            {
                mError.Text = "کد وارد شده تکراری میباشد.";
                puEx.Show();
                return;
            }
        }
        info.Code = mCode.Text;
        info.SmartCard = mSmartCard.Text;
        info.Man = mManYes.Checked;
        info.Name = mName.Text;
        info.Surname = mSurname.Text;
        info.Father = mFather.Text;
        info.IDNo = mIDNo.Text;
        info.LicenceNo = mLicenceNo.Text;
        info.LicenceCity = mLicenceCity.Text;
        info.LicenceExpireDate = Tool.ParsePersianDate(mLicenceExpireDate.Text, DateTime.Now);
        info.CellNo = mCellNo.Text;
        info.Tel = mTel.Text;
        info.DrivingNo = mDrivingNo.Text;
        info.DrivingDate = Tool.ParsePersianDate(mDrivingDate.Text, DateTime.Now);
        info.SmartCardDate = Tool.ParsePersianDate(mSmartCardDate.Text, DateTime.Now);
        info.HourDate = Tool.ParsePersianDate(mHourDate.Text, DateTime.Now);
        info.PassportNo = mPassportNo.Text;
        info.InsuranceNo = mInsuranceNo.Text;
        info.InsuranceBranch = mInsuranceBranch.Text;
        info.CarID = Tool.GetInt(mCars.SelectedValue, Null.NullInteger);
        if (info.CarID == Null.NullInteger)
            info.CarID = null;
        info.Enabled = mEnabled.Checked;

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Drivers.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            //EditID = Null.NullInteger;
            if (FromClose)
            {
                //Response.Redirect("~/Close.aspx?tripid=" + Tool.GetInt(Request.QueryString["TripID"], 0));

            }
            if (FromSale)
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.close();
}
window.close();
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("ReadClickScript", open);
               // Response.Redirect("~/sale2.aspx");
            }
            list.SelectedIndex = -1;
            BindData();
            if (info.CarID == null)
                mError.Text = "اتوکار برای این راننده انتخاب نشده است - ذخیره شد.";
            puEx.Show();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void puClose_Click(object sender, EventArgs e)
    {
        if (FromClose)
        {
            Response.Redirect("~/Close.aspx?tripid=" + Tool.GetInt(Request.QueryString["TripID"], 0));
        }
        if (FromSale)
        {
            
           // Response.Redirect("~/sale2.aspx");
        }
        EditID = Null.NullInteger;
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Driver info = e.Row.DataItem as Driver;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                e.Row.Cells[11].Text = Helper.Instance.GetCarTitle(info.CarID);
                e.Row.Cells[12].Text = info.Enabled.HasValue && info.Enabled.Value == true ? "X" : "";
                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Driver info = Helper.Instance.GetDriver(EditID);
        if (info == null)
            return;

        mCode.Text = info.Code;
        mSmartCard.Text = info.SmartCard;
        mManYes.Checked = info.Man;
        mManNo.Checked = !info.Man;
        mName.Text = info.Name;
        mSurname.Text = info.Surname;
        mFather.Text = info.Father;
        mIDNo.Text = info.IDNo;
        mLicenceNo.Text = info.LicenceNo;
        mLicenceCity.Text = info.LicenceCity;
        mLicenceExpireDate.Text = Tool.ToPersianDate(info.LicenceExpireDate, "");
        mCellNo.Text = info.CellNo;
        mTel.Text = info.Tel;
        mDrivingNo.Text = info.DrivingNo;
        mDrivingDate.Text = Tool.ToPersianDate(info.DrivingDate, "");
        mSmartCardDate.Text = Tool.ToPersianDate(info.SmartCardDate, "");
        mHourDate.Text = Tool.ToPersianDate(info.HourDate, "");
        mPassportNo.Text = info.PassportNo;
        mInsuranceNo.Text = info.InsuranceNo;
        mInsuranceBranch.Text = info.InsuranceBranch;
        mEnabled.Checked = info.Enabled == true;
        Tool.SetSelected(mCars, Null.NullInteger);
        Tool.SetSelected(mCars, info.CarID);

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        int o = (from t in Helper.Instance.DB.Trips
                 where (t.DriverID1 == id || t.DriverID2 == id || t.DriverID3 == id) && t.Closed == false
                 select t).Count();
        if (o > 0)
        {
            mMsg.Text = string.Format("این راننده در {0} سرویس باز استفاده شده است و نمیتوان آنرا حذف کرد", o);
            return;
        }
        o = (from t in Helper.Instance.DB.Trips
             where (t.DriverID1 == id || t.DriverID2 == id || t.DriverID3 == id) && t.Closed == true
             select t).Count();
        if (o > 0)
        {
            Driver info = Helper.Instance.GetDriver(id);
            info.Enabled = false;
            Helper.Instance.Update();
            mMsg.Text = string.Format("این راننده در {0} سرویس بسته استفاده شده است و بجای حذف کردن، نمایش آن غیرفعال شد.", o);
            BindData();
            return;
        }
        if (Helper.Instance.DeleteDriver(id))
        {
            list.SelectedIndex = -1;
            mMsg.Text = "این راننده با موفقیت حذف شد.";
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mSmartCard.Text = "";
        mManYes.Checked = true;
        mCode.Text = mName.Text = mSurname.Text = mFather.Text = mIDNo.Text
            = mLicenceNo.Text = mLicenceCity.Text = mLicenceExpireDate.Text
            = mCellNo.Text = mTel.Text = mDrivingNo.Text = mDrivingDate.Text
            = mSmartCardDate.Text = mHourDate.Text = mPassportNo.Text = mInsuranceNo.Text = mInsuranceBranch.Text = "";
        mDrivingDate.Text = mSmartCardDate.Text = mHourDate.Text = mLicenceExpireDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        mEnabled.Checked = true;
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void doPrint_Click(object sender, EventArgs e)
    {
        string fDate = "";
        string tDate = "";
        int specialID = 0;
        int serviceID = 0;
        int driverID1 = 0;
        int carTypeID = 0;
        int carID = 0;
        int destID = 0;
        int fundID = 0;
        int fno = 0;
        int tno = 0;
        string series = "";

        Response.Redirect(
            string.Format("~/Reports/ReportNew.aspx?fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}&repname={13}&repTitle={14}&fullName={15}&repParam={16}",
            fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, 0, "DriverList", "", "", ""), true);

    }
    protected void doGetDataFromServer_Click(object sender, EventArgs e)
    {
        // Check if it has already a Tracking Code
        if (string.IsNullOrEmpty(mSmartCard.Text) && string.IsNullOrEmpty(mIDNo.Text))
        {
            mError.Text = "لطفا یکی از فیلدهای 'شماره کارت هوشمند' یا 'کد ملي' را وارد کنید.";
            puEx.Show();
            return;
        }
        try
        {
            string value = GetDriverNew(mSmartCard.Text,mIDNo.Text,"");
            if (value.Length >0)
            {
                string [] resualt=value.Split(';');
                mName.Text = resualt[4].Substring(resualt[4].IndexOf(':') + 1);
                mSurname.Text = resualt[5].Substring(resualt[5].IndexOf(':') + 1);
                mFather.Text = resualt[6].Substring(resualt[6].IndexOf(':') + 1);
                mEnabled.Checked = resualt[11].Substring(resualt[11].IndexOf(':') + 1) == "1";
                mLicenceNo.Text = resualt[9].Substring(resualt[9].IndexOf(':') + 1);
                mIDNo.Text = resualt[3].Substring(resualt[3].IndexOf(':') + 1);
                mSmartCard.Text = resualt[1].Substring(resualt[1].IndexOf(':') + 1);
                mInsuranceNo.Text = resualt[10].Substring(resualt[10].IndexOf(':') + 1);
            }
        }
        catch (Exception ex)
        {
            mError.Text = "خطا در دریافت اطلاعات از سرور: " + ex.Message;
        }
        puEx.Show();
    }
    private void EditDriver(int DriverID)
    {
        EditID = DriverID;
        if (Null.NullInteger == EditID)
            return;
        Driver info = Helper.Instance.GetDriver(EditID);
        if (info == null)
            return;

        mCode.Text = info.Code;
        mSmartCard.Text = info.SmartCard;
        mManYes.Checked = info.Man;
        mManNo.Checked = !info.Man;
        mName.Text = info.Name;
        mSurname.Text = info.Surname;
        mFather.Text = info.Father;
        mIDNo.Text = info.IDNo;
        mLicenceNo.Text = info.LicenceNo;
        mLicenceCity.Text = info.LicenceCity;
        mLicenceExpireDate.Text = Tool.ToPersianDate(info.LicenceExpireDate, "");
        mCellNo.Text = info.CellNo;
        mTel.Text = info.Tel;
        mDrivingNo.Text = info.DrivingNo;
        mDrivingDate.Text = Tool.ToPersianDate(info.DrivingDate, "");
        mSmartCardDate.Text = Tool.ToPersianDate(info.SmartCardDate, "");
        mHourDate.Text = Tool.ToPersianDate(info.HourDate, "");
        mPassportNo.Text = info.PassportNo;
        mInsuranceNo.Text = info.InsuranceNo;
        mInsuranceBranch.Text = info.InsuranceBranch;
        mEnabled.Checked = info.Enabled == true;
        Tool.SetSelected(mCars, Null.NullInteger);
        Tool.SetSelected(mCars, info.CarID);
        puEx.Show();
    }
    protected void doGetDriver_Click(object sender, EventArgs e)
    {
        // Check if it has already a Tracking Code
        if (string.IsNullOrEmpty(dSmartCard.Text) && string.IsNullOrEmpty(dIDNo.Text)
            && string.IsNullOrEmpty(dLicenceNo.Text))
        {
            mMsg.Text = "لطفا یکی از فیلدهای 'شماره کارت هوشمند' یا  'شماره پرونده' یا 'کد ملي' را وارد کنید.";
            return;
        }
        try
        {
            string value=GetDriverNew(dSmartCard.Text,dIDNo.Text,dLicenceNo.Text);
            if (value.Length > 0)
            {
                string[] resualt = value.Split(';');
                string msg = string.Format("{0} {1} فرزند {2}", resualt[4].Substring(resualt[4].IndexOf(':') + 1), resualt[5].Substring(resualt[5].IndexOf(':') + 1), resualt[6].Substring(resualt[6].IndexOf(':') + 1));
                msg += "<br />وضعیت: " + (resualt[11].Substring(resualt[11].IndexOf(':') + 1) == "1" ? "فعال" : "غیر فعال");
                msg += "<br />شماره گواهینامه: " + resualt[9].Substring(resualt[9].IndexOf(':') + 1);
                msg += "<br />کد ملی: " + resualt[3].Substring(resualt[3].IndexOf(':') + 1);
                msg += "<br />شماره کارت هوشمند: " + resualt[1].Substring(resualt[1].IndexOf(':') + 1);
                msg += "<br />شماره بیمه: " + resualt[10].Substring(resualt[10].IndexOf(':') + 1);
                msg += "<br />شماره پرونده: " + resualt[0].Substring(resualt[0].IndexOf(':') + 1);
                mMsg.Text = msg;
            }
        }
        catch (Exception ex)
        {
            mMsg.Text = "خطا در دریافت اطلاعات از سرور: " + ex.Message;
        }
    }

    private string GetDriverNew(string SmartCard, string IDNo, string FileNo)
    {
        string value = "";
        authentication();
        if (!string.IsNullOrEmpty(SmartCard))
            value = driver.GET_DRIVER_BY_SHC(SmartCard);
        else if (!string.IsNullOrEmpty(IDNo))
        {
            value = driver.GET_DRIVER_BY_SHM(IDNo);
        }
        else if (!string.IsNullOrEmpty(FileNo))
        {
            value = driver.GET_DRIVER_BY_SHP(FileNo);
        }
        return value; 
    }
    private void authentication()
    {
        String user = "tr_web_service";
        String password = "tr_web_service123";


        System.Net.CredentialCache myCredentials = new System.Net.CredentialCache();
        System.Net.NetworkCredential netCred = new System.Net.NetworkCredential(user, password);
        myCredentials.Add(new Uri(driver.Url), "Basic", netCred);
        driver.Credentials = myCredentials;
    }
}
