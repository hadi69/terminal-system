﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Staff_Owners : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
            BindData();
    }

    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Owners
                  select c;

        if (sName.Text.Trim().Length > 0)
            all = all.Where(c => c.Name.Contains(sName.Text.Trim()));
        if (sSurname.Text.Trim().Length > 0)
            all = all.Where(c => c.Surname.Contains(sSurname.Text.Trim()));
        if (sCompany.Text.Trim().Length > 0)
            all = all.Where(c => c.Company.Contains(sCompany.Text.Trim()));
        if (sCellNo.Text.Trim().Length > 0)
            all = all.Where(c => c.CellNo.Contains(sCellNo.Text.Trim()));

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Owner info;
        if (Null.NullInteger == EditID)
        {
            info = new Owner();
            #region Validation
            //if (Helper.Instance.GetOwnerByUserName(mUserName.Text.Trim()) != null)
            //{
            //    mError.Text = "نام کاربري تکراري است";
            //    puEx.Show();
            //    return;
            //}
            //if (mPassword.Text.Trim().Length == 0)
            //{
            //    mError.Text = "رمز عبور الزامي است ";
            //    puEx.Show();
            //    return;
            //}
            //if (mPassword.Text.Trim() != mPassword2.Text.Trim())
            //{
            //    mError.Text = "رمز عبور و تکرار رمز عبور يکسان نيستند";
            //    puEx.Show();
            //    return;
            //}
            #endregion
            info.UserName = "";// mUserName.Text;
            info.Password = "";// mPassword.Text;
        }
        else
            info = Helper.Instance.GetOwner(EditID);
        if (info == null)
            return;


        //if (mPassword.Text.Trim().Length > 0)
        //{
        //    if (mPassword.Text.Trim() != mPassword2.Text.Trim())
        //    {
        //        mError.Text = "رمز عبور و تکرار رمز عبور يکسان نيستند";
        //        puEx.Show();
        //        return;
        //    }
        //    info.Password = mPassword.Text;
        //    User u = Helper.Instance.GetUser(info.UserName);
        //    if (u != null)
        //        u.Password = info.Password;
        //}
        info.Man = mManYes.Checked;
        info.Name = mName.Text;
        info.Surname = mSurname.Text;
        info.Email = mEmail.Text;
        info.Url = mUrl.Text;
        info.Tel = mTel.Text;
        info.CellNo = mCellNo.Text;
        info.Fax = mFax.Text;
        info.Company = mCompany.Text;
        info.Address = mAddress.Text;
        info.Comments = mComments.Text;
        info.Picture = "";
        // add a new User for this Owner
        if (EditID == Null.NullInteger)
        {
            //User u = new User();
            //u.UserName = info.UserName;
            //u.Password = info.Password;
            //u.FullName = info.Name + " " + info.Surname;
            //u.Role = Helper.Instance.GetRole("Owner");
            //if (u.Role == null)
            //{
            //    mError.Text = "لطفا یک رل با نام Owner تعریف کنید. این رل برای تعریف مالکان الزامی است.";
            //    puEx.Show();
            //    return;
            //}
            //if (!Helper.Instance.AddUser(u))
            //{
            //    mError.Text = Helper.Instance.LastException.Message;
            //    puEx.Show();
            //    return;
            //}
        }
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Owners.InsertOnSubmit(info);
       
        if (Helper.Instance.Update())
        {            
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            if (EditID == Null.NullInteger)
            {
                //User u = Helper.Instance.GetUser(info.UserName);
                //Helper.Instance.DeleteUser(u);
            }
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Owner info = e.Row.DataItem as Owner;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Owner info = Helper.Instance.GetOwner(EditID);
        if (info == null)
            return;

        //mUserName.Enabled = false;
        //mUserName.Text = info.UserName;
        mManYes.Checked = info.Man;
        mManNo.Checked = !info.Man;
        mName.Text = info.Name;
        mSurname.Text = info.Surname;
        mEmail.Text = info.Email;
        mUrl.Text = info.Url;
        mTel.Text = info.Tel;
        mCellNo.Text = info.CellNo;
        mFax.Text = info.Fax;
        mCompany.Text = info.Company;
        mAddress.Text = info.Address;
        mComments.Text = info.Comments;

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        Owner info = Helper.Instance.GetOwner(id);
        if (info == null)
            return;
        User u = Helper.Instance.GetUser(info.UserName);
        
        if (Helper.Instance.DeleteOwner(id))
        {
            Helper.Instance.DeleteUser(u);
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        //mUserName.Text = 
            mName.Text = mSurname.Text = mEmail.Text = mUrl.Text = mTel.Text = "";
        mCellNo.Text = mFax.Text = mCompany.Text = mAddress.Text = mComments.Text = "";
        //mUserName.Enabled = true;

        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
