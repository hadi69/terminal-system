﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Cars.aspx.cs" Inherits="Staff_Cars" Title="اتوکار" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">

    <script language='javascript'>
        var plakcity = new Array(334);
        var plakcode = new Array(334);
        var citys = { "سنندج": 51, "تهران": 11, "تهران1": 22,
            "تهران2": 33, "تهران3": 44, "تهران4": 55, "تهران5": 66,
            "تهران6": 77, "تهران7": 88, "كرج": 78, "كرج1": 68,
            "کرج3": 21, "اصفهان": 13, "اصفهان1": 23, "كرمانشاه": 19,
            "اردبیل": 91, "ایلام": 98, "بوشهر": 48, "مشهد": 12,
            "مشهد1": 32, "اهواز": 14, "زنجان": 87, "سمنان": 86,
            "زاهدان": 86, "شيراز": 63, "شيراز1": 73, "قم": 16,
            "قزوين": 79, "کرمان": 45, "خرمآباد": 31, "اراک": 47,
            "يزد": 54, "بندرعباس": 84, "ساري": 62, "ساري1": 72,
            "گرگان": 59, "ياسوج": 49, "شهركرد": 71, "رشت": 46
        };
        function getcity(j) {
            for (var city in citys) {
                if (citys[city] == j) {
                    if (hasNumbers(city))
                        city = city.substring(0, city.length - 1);
                    return city;
                }
            }
        }
        function hasNumbers(t) {
            var IsNumber = false;
            var ValidChars = "0123456789";
            for (i = 0; i < t.length && IsNumber == false; i++) {
                Char = t.charAt(i);
                if (ValidChars.indexOf(Char) != -1) {
                    IsNumber = true;
                }
            }
            return IsNumber;

        }
        var i = 0;
        plakcode[i] = 11320000; plakcity[i] = 'تهران'; i = i + 1;
        plakcode[i] = 11330000; plakcity[i] = 'اسلامشهر'; i = i + 1;
        plakcode[i] = 11340000; plakcity[i] = 'دماوند'; i = i + 1;
        plakcode[i] = 11350000; plakcity[i] = 'فیروزکوه'; i = i + 1; plakcode[i] = 11360000; plakcity[i] = 'رئ'; i = i + 1;
        plakcode[i] = 11370000; plakcity[i] = 'ساوجبلاغ'; i = i + 1; plakcode[i] = 11380000; plakcity[i] = 'شمیرانات'; i = i + 1;
        plakcode[i] = 11390000; plakcity[i] = 'شهریار'; i = i + 1; plakcode[i] = 11410000; plakcity[i] = 'رباط کریم'; i = i + 1;
        plakcode[i] = 11420000; plakcity[i] = 'کرج'; i = i + 1; plakcode[i] = 11430000; plakcity[i] = 'ورامین'; i = i + 1;
        plakcode[i] = 11440000; plakcity[i] = 'پاکدشت'; i = i + 1; plakcode[i] = 11450000; plakcity[i] = 'نظرآباد'; i = i + 1;
        plakcode[i] = 14310000; plakcity[i] = 'قم'; i = i + 1; plakcode[i] = 15310000; plakcity[i] = 'قزوین'; i = i + 1;
        plakcode[i] = 15320000; plakcity[i] = 'بوئین  زهرا'; i = i + 1; plakcode[i] = 15330000; plakcity[i] = 'تاکستان'; i = i + 1;
        plakcode[i] = 15340000; plakcity[i] = 'آبیک'; i = i + 1; plakcode[i] = 15350000; plakcity[i] = 'البرز'; i = i + 1;
        plakcode[i] = 16310000; plakcity[i] = 'سارئ'; i = i + 1; plakcode[i] = 16320000; plakcity[i] = 'آمل'; i = i + 1;
        plakcode[i] = 16330000; plakcity[i] = 'بابل'; i = i + 1; plakcode[i] = 16340000; plakcity[i] = 'بابلسر'; i = i + 1;
        plakcode[i] = 16350000; plakcity[i] = 'بهشهر'; i = i + 1; plakcode[i] = 16370000; plakcity[i] = 'محمودآباد '; i = i + 1;
        plakcode[i] = 16380000; plakcity[i] = 'تنکابن'; i = i + 1; plakcode[i] = 16390000; plakcity[i] = 'رامسر'; i = i + 1;
        plakcode[i] = 16410000; plakcity[i] = 'سوادکوه'; i = i + 1; plakcode[i] = 16420000; plakcity[i] = 'قائم  شهر'; i = i + 1;
        plakcode[i] = 16430000; plakcity[i] = 'جویبار'; i = i + 1; plakcode[i] = 16440000; plakcity[i] = 'نور'; i = i + 1;
        plakcode[i] = 16450000; plakcity[i] = 'نوشهر'; i = i + 1; plakcode[i] = 16460000; plakcity[i] = 'چالوس'; i = i + 1;
        plakcode[i] = 16470000; plakcity[i] = 'نکا'; i = i + 1; plakcode[i] = 21310000; plakcity[i] = 'اصفهان'; i = i + 1;
        plakcode[i] = 21320000; plakcity[i] = 'اردستان'; i = i + 1; plakcode[i] = 21330000; plakcity[i] = 'خمینی  شهر'; i = i + 1;
        plakcode[i] = 21340000; plakcity[i] = 'خوانسار'; i = i + 1; plakcode[i] = 21350000; plakcity[i] = 'سمیرم'; i = i + 1;
        plakcode[i] = 21360000; plakcity[i] = 'فریدن'; i = i + 1; plakcode[i] = 21370000; plakcity[i] = 'فریدونشهر'; i = i + 1;
        plakcode[i] = 21380000; plakcity[i] = 'فلاورجان'; i = i + 1; plakcode[i] = 21390000; plakcity[i] = 'شهرضا'; i = i + 1;
        plakcode[i] = 21420000; plakcity[i] = 'کاشان'; i = i + 1; plakcode[i] = 21430000; plakcity[i] = 'آران  وبیدگل'; i = i + 1;
        plakcode[i] = 21440000; plakcity[i] = 'گلپایگان'; i = i + 1; plakcode[i] = 21450000; plakcity[i] = 'لنجان'; i = i + 1;
        plakcode[i] = 21460000; plakcity[i] = 'نائین'; i = i + 1; plakcode[i] = 21470000; plakcity[i] = 'نجف  آباد '; i = i + 1;
        plakcode[i] = 21480000; plakcity[i] = 'تیران'; i = i + 1; plakcode[i] = 21490000; plakcity[i] = 'نطنز'; i = i + 1; plakcode[i] = 21510000; plakcity[i] = 'برخوار'; i = i + 1; plakcode[i] = 21520000; plakcity[i] = 'مبارکه'; i = i + 1; plakcode[i] = 21530000; plakcity[i] = 'چادگان'; i = i + 1; plakcode[i] = 21540000; plakcity[i] = 'سمیرم  سفلی'; i = i + 1; plakcode[i] = 26310000; plakcity[i] = 'تبریز'; i = i + 1; plakcode[i] = 26320000; plakcity[i] = 'آذرشهر'; i = i + 1; plakcode[i] = 26330000; plakcity[i] = 'اسکو'; i = i + 1; plakcode[i] = 26340000; plakcity[i] = 'اهر'; i = i + 1;
        plakcode[i] = 26350000; plakcity[i] = 'سراب  '; i = i + 1; plakcode[i] = 26360000; plakcity[i] = 'مراغه'; i = i + 1; plakcode[i] = 26370000; plakcity[i] = 'مرند'; i = i + 1; plakcode[i] = 26380000; plakcity[i] = 'جلفا'; i = i + 1; plakcode[i] = 26390000; plakcity[i] = 'میانه'; i = i + 1; plakcode[i] = 26410000; plakcity[i] = 'هشترود '; i = i + 1; plakcode[i] = 26420000; plakcity[i] = 'بناب  '; i = i + 1;
        plakcode[i] = 26430000; plakcity[i] = 'ملکان  '; i = i + 1; plakcode[i] = 26440000; plakcity[i] = 'بستان آباد'; i = i + 1; plakcode[i] = 26450000; plakcity[i] = 'شبستر'; i = i + 1; plakcode[i] = 26470000; plakcity[i] = 'کلیبر'; i = i + 1; plakcode[i] = 26480000; plakcity[i] = 'هریس'; i = i + 1; plakcode[i] = 26490000; plakcity[i] = 'عجب  شیر'; i = i + 1; plakcode[i] = 26510000; plakcity[i] = 'چاراویماق'; i = i + 1; plakcode[i] = 26520000; plakcity[i] = 'ورزقان'; i = i + 1; plakcode[i] = 31310000; plakcity[i] = 'مشهد '; i = i + 1; plakcode[i] = 31360000; plakcity[i] = 'تایباد'; i = i + 1;
        plakcode[i] = 31370000; plakcity[i] = 'تربت  جام'; i = i + 1; plakcode[i] = 31380000; plakcity[i] = 'تربت  حیدریه'; i = i + 1; plakcode[i] = 31390000; plakcity[i] = 'چناران'; i = i + 1; plakcode[i] = 31410000; plakcity[i] = 'خواف'; i = i + 1; plakcode[i] = 31420000; plakcity[i] = 'درگز'; i = i + 1; plakcode[i] = 31430000; plakcity[i] = 'سبزوار'; i = i + 1; plakcode[i] = 31440000; plakcity[i] = 'سرخس'; i = i + 1;
        plakcode[i] = 31480000; plakcity[i] = 'فریمان'; i = i + 1; plakcode[i] = 31520000; plakcity[i] = 'قوچان'; i = i + 1; plakcode[i] = 31530000; plakcity[i] = 'برداسکن'; i = i + 1; plakcode[i] = 31540000; plakcity[i] = 'کاشمر'; i = i + 1; plakcode[i] = 31550000; plakcity[i] = 'گناباد'; i = i + 1; plakcode[i] = 31570000; plakcity[i] = 'نیشابور'; i = i + 1; plakcode[i] = 31590000; plakcity[i] = 'رشتخوار'; i = i + 1; plakcode[i] = 31610000; plakcity[i] = 'مه ولات'; i = i + 1; plakcode[i] = 31620000; plakcity[i] = 'خلیل  آباد '; i = i + 1; plakcode[i] = 31630000; plakcity[i] = 'کلات  '; i = i + 1;
        plakcode[i] = 32310000; plakcity[i] = 'بجنورد'; i = i + 1; plakcode[i] = 32320000; plakcity[i] = 'جاجرم'; i = i + 1; plakcode[i] = 32330000; plakcity[i] = 'مانه  وسملقان'; i = i + 1; plakcode[i] = 32340000; plakcity[i] = 'اسفراین'; i = i + 1; plakcode[i] = 32350000; plakcity[i] = 'شیروان  '; i = i + 1; plakcode[i] = 32360000; plakcity[i] = 'فاروج'; i = i + 1; plakcode[i] = 33310000; plakcity[i] = 'بیرجند'; i = i + 1;
        plakcode[i] = 33320000; plakcity[i] = 'نهبندان'; i = i + 1; plakcode[i] = 33330000; plakcity[i] = 'سربیشه'; i = i + 1; plakcode[i] = 33340000; plakcity[i] = 'قائنات  '; i = i + 1; plakcode[i] = 33460000; plakcity[i] = 'سرایان'; i = i + 1; plakcode[i] = 33470000; plakcity[i] = 'فردوس  '; i = i + 1; plakcode[i] = 33480000; plakcity[i] = 'درمیان'; i = i + 1; plakcode[i] = 36310000; plakcity[i] = 'اهواز'; i = i + 1; plakcode[i] = 36320000; plakcity[i] = 'آبادان'; i = i + 1; plakcode[i] = 36330000; plakcity[i] = 'اندیمشک'; i = i + 1; plakcode[i] = 36340000; plakcity[i] = 'ایذه'; i = i + 1;
        plakcode[i] = 36350000; plakcity[i] = 'باغملک'; i = i + 1; plakcode[i] = 36360000; plakcity[i] = 'ماهشهر'; i = i + 1; plakcode[i] = 36370000; plakcity[i] = 'بهبهان'; i = i + 1; plakcode[i] = 36380000; plakcity[i] = 'خرمشهر'; i = i + 1; plakcode[i] = 36390000; plakcity[i] = 'دزفول'; i = i + 1; plakcode[i] = 36410000; plakcity[i] = 'شادگان  '; i = i + 1; plakcode[i] = 36420000; plakcity[i] = 'شوش'; i = i + 1; plakcode[i] = 36430000;
        plakcity[i] = 'دشت  آزادگان'; i = i + 1; plakcode[i] = 36440000; plakcity[i] = 'رامهرمز'; i = i + 1; plakcode[i] = 36450000; plakcity[i] = 'شوشتر'; i = i + 1; plakcode[i] = 36460000; plakcity[i] = 'مسجدسلیمان'; i = i + 1; plakcode[i] = 36470000; plakcity[i] = 'امیدیه  '; i = i + 1; plakcode[i] = 36480000; plakcity[i] = 'هندیجان'; i = i + 1; plakcode[i] = 36490000; plakcity[i] = 'رامشیر'; i = i + 1; plakcode[i] = 36510000; plakcity[i] = 'گتوند'; i = i + 1; plakcode[i] = 36520000; plakcity[i] = 'لالی'; i = i + 1; plakcode[i] = 41310000; plakcity[i] = 'شیراز'; i = i + 1; plakcode[i] = 41320000;
        plakcity[i] = 'آباده'; i = i + 1; plakcode[i] = 41330000; plakcity[i] = 'خرم  بید'; i = i + 1; plakcode[i] = 41340000; plakcity[i] = 'بوانات'; i = i + 1; plakcode[i] = 41350000; plakcity[i] = 'استهبان'; i = i + 1; plakcode[i] = 41360000; plakcity[i] = 'اقلید'; i = i + 1; plakcode[i] = 41370000; plakcity[i] = 'جهرم'; i = i + 1; plakcode[i] = 41380000; plakcity[i] = 'داراب  '; i = i + 1; plakcode[i] = 41390000; plakcity[i] = 'سپیدان'; i = i + 1;
        plakcode[i] = 41410000; plakcity[i] = 'فسا'; i = i + 1; plakcode[i] = 41420000; plakcity[i] = 'ارسنجان'; i = i + 1; plakcode[i] = 41430000; plakcity[i] = 'فیروزآباد '; i = i + 1; plakcode[i] = 41440000; plakcity[i] = 'کازرون'; i = i + 1; plakcode[i] = 41450000; plakcity[i] = 'نی  ریز'; i = i + 1; plakcode[i] = 41460000; plakcity[i] = 'لار '; i = i + 1; plakcode[i] = 41470000; plakcity[i] = 'لامرد'; i = i + 1; plakcode[i] = 41480000; plakcity[i] = 'مرودشت'; i = i + 1; plakcode[i] = 41490000; plakcity[i] = 'ممسنی'; i = i + 1; plakcode[i] = 41510000; plakcity[i] = 'حاجی  آباد '; i = i + 1;
        plakcode[i] = 41520000; plakcity[i] = 'فراشبند'; i = i + 1; plakcode[i] = 41530000; plakcity[i] = 'قیروکارزین'; i = i + 1; plakcode[i] = 41540000; plakcity[i] = 'خنج'; i = i + 1; plakcode[i] = 41550000; plakcity[i] = 'پاسارگاد'; i = i + 1; plakcode[i] = 45310000; plakcity[i] = 'کرمان'; i = i + 1; plakcode[i] = 45320000; plakcity[i] = 'راور'; i = i + 1; plakcode[i] = 45330000; plakcity[i] = 'بافت'; i = i + 1; plakcode[i] = 45340000; plakcity[i] = 'بردسیر'; i = i + 1;
        plakcode[i] = 45350000; plakcity[i] = 'بم'; i = i + 1; plakcode[i] = 45360000; plakcity[i] = 'جیرفت'; i = i + 1; plakcode[i] = 45370000; plakcity[i] = 'رفسنجان'; i = i + 1; plakcode[i] = 45380000; plakcity[i] = 'زرند '; i = i + 1; plakcode[i] = 45390000; plakcity[i] = 'سیرجان'; i = i + 1; plakcode[i] = 45410000; plakcity[i] = 'شهربابک  '; i = i + 1; plakcode[i] = 45420000; plakcity[i] = 'کهنوج'; i = i + 1; plakcode[i] = 45430000; plakcity[i] = 'منوجان  '; i = i + 1; plakcode[i] = 45440000; plakcity[i] = 'رودبار'; i = i + 1; plakcode[i] = 45450000; plakcity[i] = 'قلعه  گنج'; i = i + 1;
        plakcode[i] = 45460000; plakcity[i] = 'کوهبنان'; i = i + 1; plakcode[i] = 45470000; plakcity[i] = 'عنبرآباد'; i = i + 1; plakcode[i] = 51310000; plakcity[i] = 'اراک'; i = i + 1; plakcode[i] = 51320000; plakcity[i] = 'آشتیان'; i = i + 1; plakcode[i] = 51330000; plakcity[i] = 'تفرش'; i = i + 1; plakcode[i] = 51340000; plakcity[i] = 'خمین'; i = i + 1; plakcode[i] = 51350000; plakcity[i] = 'دلیجان'; i = i + 1; plakcode[i] = 51360000; plakcity[i] = 'ساوه'; i = i + 1;
        plakcode[i] = 51370000; plakcity[i] = 'سربند '; i = i + 1; plakcode[i] = 51380000; plakcity[i] = 'محلات'; i = i + 1; plakcode[i] = 51390000; plakcity[i] = 'کمیجان'; i = i + 1; plakcode[i] = 51410000; plakcity[i] = 'زرندیه'; i = i + 1; plakcode[i] = 54310000; plakcity[i] = 'رشت'; i = i + 1; plakcode[i] = 54320000; plakcity[i] = 'آستارا'; i = i + 1; plakcode[i] = 54330000; plakcity[i] = 'بندرانزلی'; i = i + 1; plakcode[i] = 54340000; plakcity[i] = 'آستانه  اشرفیه'; i = i + 1; plakcode[i] = 54350000; plakcity[i] = 'رودبار '; i = i + 1; plakcode[i] = 54360000; plakcity[i] = 'رودسر'; i = i + 1;
        plakcode[i] = 54370000; plakcity[i] = 'املش'; i = i + 1; plakcode[i] = 54380000; plakcity[i] = 'صومعه  سرا'; i = i + 1; plakcode[i] = 54390000; plakcity[i] = 'طوالش'; i = i + 1; plakcode[i] = 54410000; plakcity[i] = 'رضوانشهر'; i = i + 1; plakcode[i] = 54420000; plakcity[i] = 'ماسال'; i = i + 1; plakcode[i] = 54430000; plakcity[i] = 'فومن'; i = i + 1; plakcode[i] = 54440000; plakcity[i] = 'شفت  '; i = i + 1; plakcode[i] = 54450000; plakcity[i] = 'لاهیجان  '; i = i + 1;
        plakcode[i] = 54460000; plakcity[i] = 'سیاهکل'; i = i + 1; plakcode[i] = 54470000; plakcity[i] = 'لنگرود'; i = i + 1; plakcode[i] = 57310000; plakcity[i] = 'ارومیه'; i = i + 1; plakcode[i] = 57320000; plakcity[i] = 'بوکان'; i = i + 1; plakcode[i] = 57330000; plakcity[i] = 'پیرانشهر'; i = i + 1; plakcode[i] = 57340000; plakcity[i] = 'تکاب  '; i = i + 1; plakcode[i] = 57350000; plakcity[i] = 'خوئ'; i = i + 1; plakcode[i] = 57360000; plakcity[i] = 'سردشت  '; i = i + 1; plakcode[i] = 57370000; plakcity[i] = 'سلماس'; i = i + 1; plakcode[i] = 57380000; plakcity[i] = 'شاهین  دژ'; i = i + 1;
        plakcode[i] = 57390000; plakcity[i] = 'مهاباد '; i = i + 1; plakcode[i] = 57410000; plakcity[i] = 'ماکو'; i = i + 1; plakcode[i] = 57420000; plakcity[i] = 'چالدران'; i = i + 1; plakcode[i] = 57430000; plakcity[i] = 'میاندوآب'; i = i + 1; plakcode[i] = 57440000; plakcity[i] = 'نقده'; i = i + 1; plakcode[i] = 57450000; plakcity[i] = 'اشنویه'; i = i + 1; plakcode[i] = 61310000; plakcity[i] = 'زاهدان'; i = i + 1; plakcode[i] = 61320000; plakcity[i] = 'ایرانشهر'; i = i + 1;
        plakcode[i] = 61330000; plakcity[i] = 'چابهار'; i = i + 1; plakcode[i] = 61340000; plakcity[i] = 'خاش'; i = i + 1; plakcode[i] = 61350000; plakcity[i] = 'زابل'; i = i + 1; plakcode[i] = 61360000; plakcity[i] = 'سراوان  '; i = i + 1; plakcode[i] = 61370000; plakcity[i] = 'نیک  شهر'; i = i + 1; plakcode[i] = 61380000; plakcity[i] = 'راسک'; i = i + 1; plakcode[i] = 61390000; plakcity[i] = 'کنارک'; i = i + 1; plakcode[i] = 61410000; plakcity[i] = 'زهک'; i = i + 1; plakcode[i] = 64310000; plakcity[i] = 'بندرعباس'; i = i + 1; plakcode[i] = 64320000; plakcity[i] = 'جاسک'; i = i + 1; plakcode[i] = 64330000;
        plakcity[i] = 'بندرلنگه'; i = i + 1; plakcode[i] = 64340000; plakcity[i] = 'ابوموسی'; i = i + 1; plakcode[i] = 64350000; plakcity[i] = 'حاجی  آباد '; i = i + 1; plakcode[i] = 64360000; plakcity[i] = 'رودان  '; i = i + 1; plakcode[i] = 64370000; plakcity[i] = 'قشم'; i = i + 1; plakcode[i] = 64380000; plakcity[i] = 'میناب'; i = i + 1; plakcode[i] = 64390000; plakcity[i] = 'بستک'; i = i + 1; plakcode[i] = 64410000; plakcity[i] = 'خمیر'; i = i + 1;
        plakcode[i] = 64420000; plakcity[i] = 'گاوبندئ'; i = i + 1; plakcode[i] = 67310000; plakcity[i] = 'زنجان'; i = i + 1; plakcode[i] = 67320000; plakcity[i] = 'ایجرود'; i = i + 1; plakcode[i] = 67330000; plakcity[i] = 'طارم  '; i = i + 1; plakcode[i] = 67340000; plakcity[i] = 'ماه  نشان'; i = i + 1; plakcode[i] = 67350000; plakcity[i] = 'ابهر'; i = i + 1; plakcode[i] = 67360000; plakcity[i] = 'خرمدره'; i = i + 1; plakcode[i] = 67370000; plakcity[i] = 'خدابنده  '; i = i + 1; plakcode[i] = 71310000; plakcity[i] = 'کرمانشاه'; i = i + 1; plakcode[i] = 71320000; plakcity[i] = 'اسلام  آباد غرب'; i = i + 1;
        plakcode[i] = 71330000; plakcity[i] = 'پاوه'; i = i + 1; plakcode[i] = 71340000; plakcity[i] = 'جوانرود'; i = i + 1; plakcode[i] = 71350000; plakcity[i] = 'سرپل  ذهاب'; i = i + 1; plakcode[i] = 71360000; plakcity[i] = 'سنقر'; i = i + 1; plakcode[i] = 71370000; plakcity[i] = 'قصرشیرین'; i = i + 1; plakcode[i] = 71380000; plakcity[i] = 'صحنه'; i = i + 1; plakcode[i] = 71390000; plakcity[i] = 'کنگاور '; i = i + 1; plakcode[i] = 71410000;
        plakcity[i] = 'گیلانغرب'; i = i + 1; plakcode[i] = 71420000; plakcity[i] = 'هرسین'; i = i + 1; plakcode[i] = 71430000; plakcity[i] = 'ثلاث  باباجانی'; i = i + 1; plakcode[i] = 71440000; plakcity[i] = 'روانسر'; i = i + 1; plakcode[i] = 71450000; plakcity[i] = 'دالاهو'; i = i + 1; plakcode[i] = 73310000; plakcity[i] = 'سنندج'; i = i + 1; plakcode[i] = 73320000; plakcity[i] = 'بانه'; i = i + 1; plakcode[i] = 73330000; plakcity[i] = 'بیجار'; i = i + 1; plakcode[i] = 73340000; plakcity[i] = 'سقز'; i = i + 1; plakcode[i] = 73350000; plakcity[i] = 'دیواندره'; i = i + 1; plakcode[i] = 73360000; plakcity[i] = 'قروه'; i = i + 1;
        plakcode[i] = 73370000; plakcity[i] = 'مریوان'; i = i + 1; plakcode[i] = 73380000; plakcity[i] = 'کامیاران'; i = i + 1; plakcode[i] = 73390000; plakcity[i] = 'سروآباد'; i = i + 1; plakcode[i] = 75310000; plakcity[i] = 'همدان'; i = i + 1; plakcode[i] = 75320000; plakcity[i] = 'اسد آباد'; i = i + 1; plakcode[i] = 75330000; plakcity[i] = 'بهار'; i = i + 1; plakcode[i] = 75340000; plakcity[i] = 'تویسرکان'; i = i + 1;
        plakcode[i] = 75350000; plakcity[i] = 'رزن'; i = i + 1; plakcode[i] = 75360000; plakcity[i] = 'کبودرآهنگ'; i = i + 1; plakcode[i] = 75370000; plakcity[i] = 'ملایر'; i = i + 1; plakcode[i] = 75380000; plakcity[i] = 'نهاوند '; i = i + 1; plakcode[i] = 77310000; plakcity[i] = 'شهرکرد'; i = i + 1; plakcode[i] = 77320000; plakcity[i] = 'اردل'; i = i + 1; plakcode[i] = 77330000; plakcity[i] = 'بروجن'; i = i + 1; plakcode[i] = 77340000; plakcity[i] = 'فارسان'; i = i + 1; plakcode[i] = 77350000; plakcity[i] = 'لردگان'; i = i + 1; plakcode[i] = 77360000; plakcity[i] = 'کوهرنگ'; i = i + 1; plakcode[i] = 81310000;
        plakcity[i] = 'خرم  آباد '; i = i + 1; plakcode[i] = 81320000; plakcity[i] = 'سلسله'; i = i + 1; plakcode[i] = 81330000; plakcity[i] = 'پلدختر'; i = i + 1; plakcode[i] = 81340000; plakcity[i] = 'الیگودرز'; i = i + 1; plakcode[i] = 81350000; plakcity[i] = 'ازنا'; i = i + 1; plakcode[i] = 81360000; plakcity[i] = 'بروجرد'; i = i + 1; plakcode[i] = 81370000; plakcity[i] = 'دلفان'; i = i + 1; plakcode[i] = 81380000; plakcity[i] = 'دورود'; i = i + 1;
        plakcode[i] = 81390000; plakcity[i] = 'کوهدشت  '; i = i + 1; plakcode[i] = 83310000; plakcity[i] = 'ایلام'; i = i + 1; plakcode[i] = 83320000; plakcity[i] = 'ایوان'; i = i + 1; plakcode[i] = 83330000; plakcity[i] = 'دره  شهر'; i = i + 1; plakcode[i] = 83340000; plakcity[i] = 'آبدانان'; i = i + 1; plakcode[i] = 83350000; plakcity[i] = 'دهلران'; i = i + 1; plakcode[i] = 83360000; plakcity[i] = 'شیروان  و چرداول'; i = i + 1; plakcode[i] = 83370000; plakcity[i] = 'مهران  '; i = i + 1; plakcode[i] = 85310000; plakcity[i] = 'یاسوج  '; i = i + 1; plakcode[i] = 85320000; plakcity[i] = 'کهگیلویه  '; i = i + 1;
        plakcode[i] = 85330000; plakcity[i] = 'گچساران  '; i = i + 1; plakcode[i] = 85340000; plakcity[i] = 'دنا'; i = i + 1; plakcode[i] = 85350000; plakcity[i] = 'بهمئی  '; i = i + 1; plakcode[i] = 87310000; plakcity[i] = 'سمنان'; i = i + 1; plakcode[i] = 87320000; plakcity[i] = 'دامغان'; i = i + 1; plakcode[i] = 87330000; plakcity[i] = 'شاهرود '; i = i + 1; plakcode[i] = 87340000; plakcity[i] = 'گرمسار'; i = i + 1;
        plakcode[i] = 91310000; plakcity[i] = 'اردبیل'; i = i + 1; plakcode[i] = 91320000; plakcity[i] = 'نیر'; i = i + 1; plakcode[i] = 91330000; plakcity[i] = 'نمین'; i = i + 1; plakcode[i] = 91340000; plakcity[i] = 'بیله  سوار'; i = i + 1; plakcode[i] = 91350000; plakcity[i] = 'پارس  آباد'; i = i + 1; plakcode[i] = 91360000; plakcity[i] = 'خلخال'; i = i + 1; plakcode[i] = 91370000; plakcity[i] = 'کوثر'; i = i + 1; plakcode[i] = 91380000; plakcity[i] = 'گرمی'; i = i + 1; plakcode[i] = 91390000; plakcity[i] = 'مشگین  شهر'; i = i + 1; plakcode[i] = 93310000; plakcity[i] = 'یزد'; i = i + 1; plakcode[i] = 93320000;
        plakcity[i] = 'صدوق  '; i = i + 1; plakcode[i] = 93330000; plakcity[i] = 'اردکان  '; i = i + 1; plakcode[i] = 93340000; plakcity[i] = 'بافق'; i = i + 1; plakcode[i] = 93350000; plakcity[i] = 'تفت'; i = i + 1; plakcode[i] = 93360000; plakcity[i] = 'ابرکوه  '; i = i + 1; plakcode[i] = 93370000; plakcity[i] = 'مهریز'; i = i + 1; plakcode[i] = 93380000; plakcity[i] = 'میبد'; i = i + 1;
        plakcode[i] = 93390000; plakcity[i] = 'طبس'; i = i + 1; plakcode[i] = 93410000; plakcity[i] = 'خاتم'; i = i + 1; plakcode[i] = 95310000; plakcity[i] = 'بوشهر'; i = i + 1; plakcode[i] = 95320000; plakcity[i] = 'تنگستان'; i = i + 1; plakcode[i] = 95330000; plakcity[i] = 'دیر'; i = i + 1; plakcode[i] = 95340000; plakcity[i] = 'دشتستان'; i = i + 1; plakcode[i] = 95350000; plakcity[i] = 'دشتی  '; i = i + 1; plakcode[i] = 95360000; plakcity[i] = 'کنگان  '; i = i + 1; plakcode[i] = 95370000; plakcity[i] = 'گناوه'; i = i + 1; plakcode[i] = 95380000; plakcity[i] = 'دیلم'; i = i + 1;
        plakcode[i] = 95390000; plakcity[i] = 'جم'; i = i + 1; plakcode[i] = 97310000; plakcity[i] = 'گرگان'; i = i + 1; plakcode[i] = 97320000; plakcity[i] = 'بندر ترکمن'; i = i + 1; plakcode[i] = 97330000; plakcity[i] = 'علی  آباد  '; i = i + 1; plakcode[i] = 97340000; plakcity[i] = 'کردکوئ'; i = i + 1; plakcode[i] = 97350000; plakcity[i] = 'گنبدکاووس'; i = i + 1; plakcode[i] = 97360000; plakcity[i] = 'مینودشت  '; i = i + 1;
        plakcode[i] = 97370000; plakcity[i] = 'بندرگز'; i = i + 1; plakcode[i] = 97380000; plakcity[i] = 'آق  قلا'; i = i + 1; plakcode[i] = 97390000; plakcity[i] = 'آزادشهر'; i = i + 1; plakcode[i] = 97410000; plakcity[i] = 'رامیان'; i = i + 1; plakcode[i] = 97420000; plakcity[i] = 'کلاله'; i = i + 1;
    </script>

    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td colspan="8">
                        اعتبارسنجی
                    </td>
                </tr>
                <tr>
                    <td>
                        اتوکار:
                    </td>
                    <td>
                        شماره کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="cSmartCard" runat="server" />
                    </td>
                    <td>
                        VIN
                    </td>
                        <td style="display: none">
                                <!--
                                ایران //-->
                                <asp:TextBox ID="cPlate0" runat="server" MaxLength="2" Width="50" BackColor="#FF1111" />
                                <asp:TextBox ID="cPlate1" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <asp:DropDownList ID="cPlate2" runat="server" CssClass="TL" Width="50" BackColor="#FF1111">
                                    <asp:ListItem Text="ع" Value="ع" />
                                    <asp:ListItem Text="الف" Value="الف" />
                                </asp:DropDownList>
                                <asp:TextBox ID="cPlate3" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars"
                                    TargetControlID="cPlate0" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars"
                                    TargetControlID="cPlate1" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars"
                                    TargetControlID="cPlate3" ValidChars='123456789' />
                            </td>
                    <td>
                        <asp:TextBox runat="server" ID="cVIN"></asp:TextBox>
                    </td>
                    <td>
                        شماره پرونده
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="cNo"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doGetCar" runat="server" CssClass="CB" OnClick="doGetCar_Click"
                            Text="دریافت اطلاعات از تهران" Width="150px" />
                    </td>
                </tr>
            </table>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabled" Text='نمایش فعالها' Checked="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabledNo" Text='نمایش غیرفعالها' />
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                    <td align="left">
                        <asp:Button ID="doPrint" runat="server" CssClass="CB" Text="چاپ" Width="50px" OnClick="doPrint_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" Width="150px" />
                    </td>
                    <td>
                        مالک
                    </td>
                    <td>
                        <asp:DropDownList ID="sOwnerID" runat="server" Width="150px" />
                    </td>
                    <td class="N">
                        شماره پلاک
                    </td>
                    <td>
                        ایران<asp:TextBox ID="sPlate0" runat="server" MaxLength="2" Width="50" />
                        <asp:TextBox ID="sPlate1" runat="server" CssClass="TL" Width="50" />
                        <asp:DropDownList ID="sPlate2" runat="server" CssClass="TL" Width="50">
                            <asp:ListItem Text="" Value="" />
                            <asp:ListItem Text="ع" Value="ع" />
                            <asp:ListItem Text="الف" Value="الف" />
                        </asp:DropDownList>
                        <asp:TextBox ID="sPlate3" runat="server" CssClass="TL" Width="50" />
                    </td>
                </tr>
                <tr>
                    <td>
                        کارت هوشمند
                    </td>
                    <td>
                        <asp:TextBox ID="sSmartCard" runat="server" Width="150px" />
                    </td>
                    <td>
                        شماره ترانزيت
                    </td>
                    <td>
                        <asp:TextBox ID="sTransitNo" runat="server" Width="150px" />
                    </td>
                    <td colspan="2" style="text-align: left">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                        <asp:Button ID="doChange" runat="server" Text="تصحیح پلاکها" OnClick="doChange_Click"
                            Width="80px" CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Code" HeaderText="کد" />
                    <asp:BoundField DataField="CarTypeID" HeaderText="نوع اتوکار" />
                    <asp:BoundField DataField="OwnerID" HeaderText="مالک" />
                    <asp:BoundField DataField="ID" HeaderText="شهر پلاک" />
                    <asp:BoundField DataField="Plate" HeaderText="شماره پلاک" />
                    <asp:BoundField DataField="SmartCard" HeaderText="کارت هوشمند" />
                    <asp:BoundField DataField="TransitNo" HeaderText="شماره ترانزيت" />
                    <asp:BoundField DataField="ContractDate2" HeaderText="تاريخ قرارداد" />
                    <asp:BoundField DataField="VisitDate2" HeaderText="تاریخ معاینه فنی" />
                    <asp:BoundField DataField="Enabled" HeaderText="نمایش" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none;" ID="puPanel" runat="server" Width="750px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش اتوکار)</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نوع اتوکار
                            </td>
                            <td>
                                <asp:DropDownList ID="mCarTypeID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="نوع اتوکار الزامي است"
                                    ControlToValidate="mCarTypeID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد
                            </td>
                            <td>
                                <asp:TextBox ID="mCode" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مالک
                            </td>
                            <td>
                                <asp:DropDownList ID="mOwnerID" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره پلاک
                            </td>
                            <td>
                                <!--
                                ایران //-->
                                <asp:TextBox ID="mPlate0" runat="server" MaxLength="2" Width="50" BackColor="#FF1111" />
                                <asp:TextBox ID="mPlate1" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <asp:DropDownList ID="mPlate2" runat="server" CssClass="TL" Width="50" BackColor="#FF1111">
                                    <asp:ListItem Text="ع" Value="ع" />
                                    <asp:ListItem Text="الف" Value="الف" />
                                </asp:DropDownList>
                                <asp:TextBox ID="mPlate3" runat="server" CssClass="TL" Width="50" BackColor="#FF1111" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mPlate0" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mPlate1" ValidChars='123456789' />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mPlate3" ValidChars='123456789' />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شناسه خودرو VIN
                            </td>
                            <td>
                                <asp:TextBox ID="mVIN" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شهر/کشور پلاک
                            </td>
                            <td>
                                <asp:DropDownList ID="mPlateCity" runat="server" BackColor="#FF1111">
                                </asp:DropDownList>
                                <asp:HiddenField ID="mCityPlak" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کارت هوشمند
                            </td>
                            <td>
                                <asp:TextBox ID="mSmartCard" MinLength="7" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="شماره کارت هوشمند الزامي است"
                                    ControlToValidate="mSmartCard" Display="Dynamic"></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator Display = "Dynamic" ValidationGroup="addGroup" ControlToValidate = "mSmartCard" ID="rev1" ValidationExpression = "^[\s\S]{7,}$" runat="server" ErrorMessage="شماره کارت نباید کمتر از 7 رقم باشد"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاریخ معاینه فنی
                            </td>
                            <td>
                                <asp:TextBox ID="mVisitDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="mee1" TargetControlID="mVisitDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره ترانزيت
                            </td>
                            <td>
                                <asp:TextBox ID="mTransitNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                سال توليد
                            </td>
                            <td>
                                <asp:TextBox ID="mProductionYear" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                <!-- 
                                سيستم
                                -->
                            </td>
                            <td>
                                <asp:TextBox ID="mDrivingNo" runat="server" Visible="false" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                بيمه بدنه
                            </td>
                            <td>
                                <asp:DropDownList ID="mInsuranceID" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ بيمه بدنه
                            </td>
                            <td>
                                <asp:TextBox ID="mInsuranceDate" runat="server" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="mInsuranceDate"
                                    runat="server" Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ بيمه شخص ثالث
                            </td>
                            <td>
                                <asp:TextBox ID="mThirdPersonDate" runat="server" BackColor="#FF1111" />
                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="mThirdPersonDate"
                                    runat="server" Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شماره بیمه شخص ثالث
                            </td>
                            <td>
                                <asp:TextBox ID="mThirdPersonNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کميسيون
                            </td>
                            <td>
                                <asp:TextBox ID="mCommission" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                حذف شده
                            </td>
                            <td>
                                <asp:CheckBox ID="mDeleted" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ قرارداد
                            </td>
                            <td>
                                <asp:TextBox ID="mContractDate" runat="server" />
                                <cc1:MaskedEditExtender ID="mee4" TargetControlID="mContractDate" runat="server"
                                    Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button runat="server" ID="doGetDataFromServer" Text='دریافت اطلاعات از تهران'
                                    OnClick="doGetDataFromServer_Click" CssClass="CB" />
                                <asp:Button ID="ReadCard" runat="server" Text="خواندن از کارت" OnClick="ReadCard_Click"
                                    CssClass="CB" />
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                    <asp:Button ID="puClose" runat="server" Text="بستن" CssClass="CB" OnClick="puClose_Click"/>
                                <asp:Button ID="puCancel" OnClick="puCancel_Click" runat="server"  Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label ID="mError" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="Label1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
