﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Customers.aspx.cs" Inherits="Customers" Title="مشتركين" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        نام
                    </td>
                    <td>
                        <asp:TextBox ID="sName" runat="server" Width="150px" />
                    </td>
                    <td>
                        نام خانوادگي
                    </td>
                    <td>
                        <asp:TextBox ID="sSurname" runat="server" Width="150px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        شماره اشتراك
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" Width="150px" />
                    </td>
                    <td>
                        تلفن
                    </td>
                    <td>
                        <asp:TextBox ID="sTel" runat="server" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="No" HeaderText="شماره اشتراك" />
                    <asp:BoundField DataField="Name" HeaderText="نام" />
                    <asp:BoundField DataField="Surname" HeaderText="نام خانوادگي" />
                    <asp:BoundField DataField="Tel" HeaderText="تلفن" />
                    <asp:BoundField DataField="CellNo" HeaderText="موبايل" />
                    <asp:BoundField DataField="Fax" HeaderText="فاکس" />
                    <asp:BoundField DataField="Address" HeaderText="آدرس" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش مشترك)</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                شماره اشتراك
                            </td>
                            <td>
                                <asp:TextBox ID="mNo" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="شماره اشتراك الزامي است"
                                    ControlToValidate="mNo" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                عنوان
                            </td>
                            <td>
                                <asp:RadioButton ID="mManYes" runat="server" Text="آقا" GroupName="Man" Checked="true" />
                                <asp:RadioButton ID="mManNo" runat="server" Text="خانم" GroupName="Man" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام
                            </td>
                            <td>
                                <asp:TextBox ID="mName" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvr2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="نام الزامي است" ControlToValidate="mName" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نام خانوادگي
                            </td>
                            <td>
                                <asp:TextBox ID="mSurname" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvr3" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="نام خانوادگي الزامي است" ControlToValidate="mSurname" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تلفن
                            </td>
                            <td>
                                <asp:TextBox ID="mTel" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تلفن همراه
                            </td>
                            <td>
                                <asp:TextBox ID="mCellNo" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                فاکس
                            </td>
                            <td>
                                <asp:TextBox ID="mFax" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                آدرس
                            </td>
                            <td>
                                <asp:TextBox ID="mAddress" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                پست الکترونيکي
                            </td>
                            <td>
                                <asp:TextBox ID="mEmail" runat="server" />
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" runat="server" ControlToValidate="mEmail"
                                 ValidationGroup="addGroup"    ErrorMessage="آدرس پست الکترونيکي اشتباه است" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N" colspan="3">
                                <asp:Label ID="mError" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
