﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Trips : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        mMsg.Text = "";

        #region Hide
        //System.Globalization.CultureInfo calture = new System.Globalization.CultureInfo("fa-IR");
        //System.Globalization.DateTimeFormatInfo info = calture.DateTimeFormat;
        //info.AbbreviatedDayNames = new string[] { "ي", "د", "س", "چ", "پ", "ج", "ش" };
        //info.DayNames = new string[] { "يکشنبه", "دوشنبه", "ﺳﻪشنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه" };
        //info.AbbreviatedMonthNames = new string[] { "فروردين", "ارديبهشت", "خرداد", "تير", "مرداد", "شهريور", "مهر", "آبان", "آذر", "دي", "بهمن", "اسفند", "" };
        //info.MonthNames = new string[] { "فروردين", "ارديبهشت", "خرداد", "تير", "مرداد", "شهريور", "مهر", "آبان", "آذر", "دي", "بهمن", "اسفند", "" };
        //info.AMDesignator = "ق.ظ";
        //info.PMDesignator = "ب.ظ";
        //info.ShortDatePattern = "yyyy/MM/dd";
        //info.FirstDayOfWeek = DayOfWeek.Saturday;
        //System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
        //typeof(System.Globalization.DateTimeFormatInfo).GetField("calendar", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(info, cal);
        //object obj = typeof(System.Globalization.DateTimeFormatInfo).GetField("m_cultureTableRecord", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(info);
        //obj.GetType().GetMethod("UseCurrentCalendar", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Invoke(obj, new object[] { cal.GetType().GetProperty("ID", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(cal, null) });
        //typeof(System.Globalization.CultureInfo).GetField("calendar", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(calture, cal);
        //System.Threading.Thread.CurrentThread.CurrentCulture = calture;
        //System.Threading.Thread.CurrentThread.CurrentUICulture = calture;
        //System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat = info;
        //System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat = info;
        #endregion

        if (!IsPostBack)
        {
            //Helper.Instance.AutoDeleteTrips();
            PathID = Tool.GetInt(Request.QueryString["pathid"], Null.NullInteger);
            BindInitData();
            BindData();

            if ((Tool.CanMini() || Tool.CanSewari())
                && Tool.GetBool(Helper.Instance.GetSettingValue("MiniAsBus"), false) == false)
                doSaleMini.Visible = mMinies.Visible = mMiniTitle.Visible = true;
            else
                doSaleMini.Visible = mMinies.Visible = mMiniTitle.Visible = false;
        }
        else
            BindPaths();
        sDateStart.DataSelected += new EventHandler(sDateStart_DataSelected);
    }

    void sDateStart_DataSelected(object sender, EventArgs e)
    {
        BindData();
    }
    private void BindInitData()
    {
        mShowAll.Checked = true;

        int lastPathID = Tool.GetInt(Session["Trips.PathID"], Null.NullInteger);
        if (Null.NullInteger != lastPathID && PathID == Null.NullInteger)
            PathID = lastPathID;

        BindPaths();
        //try
        //{
        //    List<Service> all = Helper.Instance.GetServices();
        //    mServiceID.DataValueField = "ID";
        //    mServiceID.DataTextField = "Title";
        //    mServiceID.DataSource = all;
        //    mServiceID.DataBind();
        //}
        //catch (Exception ex)
        //{
        //    mError.Text += "--" + ex.Message + "<br />";
        //}
        try
        {
            List<Car> all = Helper.Instance.GetCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            mCarID.DataValueField = "ID";
            mCarID.DataTextField = "Title";
            mCarID.DataSource = all;
            mCarID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        try
        {
            List<Driver> all = Helper.Instance.GetDrivers();
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            mDriverID1.DataValueField = "ID";
            mDriverID1.DataTextField = "Title";
            mDriverID1.DataSource = all;
            mDriverID1.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        try
        {
            sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Trips.sDateStart"]), DateTime.Now);
            mShowAll.Checked = Tool.GetBool(Session["Trips.mShowAll"], true);
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        try
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            for (int i = 0; i < all.Count; i++)
                if (all[i].Layout.NumChairs >= 27)
                {
                    all.RemoveAt(i);
                    i--;
                }
            mMinies.DataValueField = "ID";
            mMinies.DataTextField = "Title";
            mMinies.DataSource = all;
            mMinies.DataBind();

            doSaleMini.Enabled = mMinies.Items.Count > 0;

        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
    }
    private void BindPaths()
    {
        try
        {
            mPaths2.Controls.Clear();
            List<Path> all = Helper.Instance.GetPaths(true);
            if (PathID != Null.NullInteger)
            {
                bool shows = false;
                for (int i = 0; i < all.Count; i++)
                    if (PathID == all[i].ID)
                    {
                        shows = true;
                        break;
                    }
                if (!shows)
                    PathID = Null.NullInteger;
            }
            mPaths2.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0 Width='150' style='{border:1px solid #8AA8D0}' >"));
            for (int i = 0; i < all.Count; i++)
            {
                bool selected = PathID == all[i].ID || (i == 0 && PathID == Null.NullInteger);
                if (selected)
                    PathID = all[i].ID;
                string it = "pa_" + i;
                string up = i > 0 ? "pa_" + (i - 1) : it;
                string down = i < all.Count - 1 ? "pa_" + (i + 1) : it;
                mPaths2.Controls.Add(new LiteralControl(
                    string.Format("<tr><td><a href='Trips.aspx?pathid={0}' id='pa_{1}' {3} {4} {5} class='{6}' >{2}</a></td></tr>"
                    , all[i].ID, i, all[i].Title
                    , string.Format("onkeyup=\"return hK('{0}', '{1}');\"", up, down)
                    , string.Format("onfocus=\"v($get('{0}'));\"", it)
                    , string.Format("onblur=\"u($get('{0}'));\"", it)
                    , selected ? "pathS" : "path")));
                if (selected)
                    JsTools.SetFocus(Page, it);
            }
            mPaths2.Controls.Add(new LiteralControl("</table>"));
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
    }
    private void BindData()
    {
        if (mShowAll.Checked || Tool.ToPersianDate(DateTime.Now, "") != Tool.ToPersianDate(sDateStart.SelectedDate, ""))
            mFilter.BgColor = "#AAF0F0";
        else
            mFilter.BgColor = "Transparent";

        // -------------------------------
        DateTime startDate = sDateStart.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now;
        else
            Session["Trips.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        DateTime toDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 23, 59, 59, 999);
        if (mShowAll.Checked)
            toDate = DateTime.Now.AddYears(2);
        string qry = string.Format(@"SELECT Trips.*
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
	, Services.ServiceType
	, Paths.ID AS PathTitle
	, CarTypes.Title AS CarTypeTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
    , (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND SaleType = 1) AS NumReserves
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN CarTypes ON Trips.CarTypeID=CarTypes.ID
WHERE (Trips.Closed IS NULL OR Trips.Closed=0) AND Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59'
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);


        if (PathID != Null.NullInteger)
        {
            int sID = PathID;// Tool.GetInt(mPaths.SelectedValue, Null.NullInteger);
            Session["Trips.PathID"] = sID;
            qry += " AND Services.PathID=" + sID;
        }

        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Services.ServiceType");

        list.DataSource = dt;
        list.DataBind();
        // --------------------------------


        Session["Trips.mShowAll"] = mShowAll.Checked;


        BindHotKeys();
    }
    private void BindDataGut()
    {
        if (mShowAll.Checked || Tool.ToPersianDate(DateTime.Now, "") != Tool.ToPersianDate(sDateStart.SelectedDate, ""))
            mFilter.BgColor = "#AAF0F0";
        else
            mFilter.BgColor = "Transparent";

        var all = from c in Helper.Instance.DB.Trips
                  where c.Closed == false
                  orderby c.Date, c.Service.DepartureTime, c.Service.ServiceType
                  select c;
        List<Trip> trips = all.ToList();

        if (PathID != Null.NullInteger)
        {
            int sID = PathID;// Tool.GetInt(mPaths.SelectedValue, Null.NullInteger);
            Session["Trips.PathID"] = sID;
            if (trips != null && trips.Count > 0)
                for (int i = 0; i < trips.Count; i++)
                    if (trips[i].Service.PathID != sID)
                    {
                        trips.RemoveAt(i);
                        i--;
                    }
        }
        DateTime startDate = sDateStart.SelectedDate;
        if (startDate != Null.NullDate)
        {
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0);
            DateTime endDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 23, 59, 59, 999);
            Session["Trips.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
            if (trips != null && trips.Count > 0)
                for (int i = 0; i < trips.Count; i++)
                {
                    if (mShowAll.Checked && trips[i].Date < startDate)
                    {
                        trips.RemoveAt(i);
                        i--;
                    }
                    else if (mShowAll.Checked == false && (trips[i].Date < startDate || trips[i].Date > endDate))
                    {
                        trips.RemoveAt(i);
                        i--;
                    }
                }
        }

        Session["Trips.mShowAll"] = mShowAll.Checked;
        list.DataSource = trips;
        list.DataBind();

        BindHotKeys();
    }
    List<HyperLink> mSaleLinks = new List<HyperLink>();
    void BindHotKeys()
    {
        for (int i = 0; i < mSaleLinks.Count; i++)
        {
            string up = i > 0 ? mSaleLinks[i - 1].ClientID : mSaleLinks[0].ClientID;
            string down = i < mSaleLinks.Count - 1 ? mSaleLinks[i + 1].ClientID : mSaleLinks[i].ClientID;
            mSaleLinks[i].Attributes.Add("onkeyup", string.Format("hK('{0}', '{1}');"
                , up, down));//javascript:
            GridViewRow container = mSaleLinks[i].NamingContainer as GridViewRow;
            mSaleLinks[i].Attributes["onfocus"] = string.Format("v($get('{0}'));", container.ClientID);//javascript:
            mSaleLinks[i].Attributes["onblur"] = string.Format("u($get('{0}'));", container.ClientID);//javascript:
        }
        if (mSaleLinks.Count > 0)
        {
            List<string> clientIDs = new List<string>();
            List<int> keyCodes = new List<int>();
            clientIDs.Add(mSaleLinks[0].ClientID);
            keyCodes.Add(118);
            clientIDs.Add("pa_0");
            keyCodes.Add(120);
            clientIDs.Add(puCancel.ClientID);
            keyCodes.Add(27);
            if (IsPostBack)
                JsTools.BindHotKeysFocus2(Page, clientIDs, keyCodes);
            else
                JsTools.BindHotKeysFocus(Page, clientIDs, keyCodes);
        }
    }
    private int GetNumChairs(Layout info)
    {
        if (info.NumChairs.HasValue)
            return info.NumChairs.Value;
        int res = 0;
        for (int f = 0; f < info.Floors; f++)
            for (int i = 0; i < info.Rows; i++)
                for (int j = 0; j < info.Columns; j++)
                    if (info.Layout1 != null && info.Layout1.Contains(string.Format(";{0}_{1}_{2};", f, i, j)))
                        res++;
        return res;
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        try
        {
            Trip info;
            if (Null.NullInteger == EditID)
            {
                info = new Trip();
                info.Series = "";
                info.TicketlessName = "";

                //info.Series = string.Format("{0},{1},{2},{3}"
                //    , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
                //    , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
                //info.No = Helper.Instance.GetNextSeriesStart(info.Series);
            }
            else
                info = Helper.Instance.GetTrip(EditID);
            if (info == null)
                return;

            if (!Tool.ValidatePersianDate(mDate.Text))
            {
                mError.Text = "تاريخ اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
                puEx.Show();
                return;
            }

            info.ServiceID = Tool.GetInt(mServiceID.SelectedValue, info.ServiceID);
            if (info.ServiceID == Null.NullInteger)
            {
                mError.Text = "تعریف سرویس انتخاب نشده است.";
                puEx.Show();
                return;
            }

            info.Date = Tool.ParsePersianDate(mDate.Text, DateTime.Now);
            info.Locked = mLocked.Checked;
            if (Null.NullInteger == EditID)
            {
                Service s = Helper.Instance.GetService(info.ServiceID);
                if (s != null)
                {
                    info.Price = s.Price;
                    info.Toll = s.Toll;
                    info.Insurance = s.Insurance;
                    info.Reception = s.Reception;

                    RateItem rate = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, s.Path.SrcCityID, s.Path.DestCityID, s.SpecialID, info.Date);
                    if (rate != null)
                    {
                        info.ExtraCost = rate.ExtraCost;
                        info.Insurance = rate.Insurance;
                        info.Toll = rate.Toll;
                        info.Reception = rate.ReceptionCost;
                        //info.Stamp = rate.st
                    }
                }
            }

            int carTypeID = Tool.GetInt(mCarTypeID.SelectedValue, Null.NullInteger);
            int oldNum = Helper.Instance.GetNumTickets(info.ID) + Helper.Instance.GetNumReserves(info.ID);
            if (oldNum > 0 && Null.NullInteger != EditID && info.CarTypeID != carTypeID && info.CarTypeID != null)
            {
                if (carTypeID == Null.NullInteger)
                {
                    mError.Text = "سرویس فروش یا رزرو دارد و نمیتوان نوع اتوکار آنرا حذف کرد.";
                    puEx.Show();
                    return;
                }

                // it wants to change CarTypeID
                CarType c2 = Helper.Instance.GetCarType(carTypeID), c1 = Helper.Instance.GetCarType(info.CarTypeID.Value);
                if (c2.LayoutID != c1.LayoutID)
                {
                    int numChairs = GetNumChairs(c2.Layout);
                    if (numChairs < oldNum)
                    {
                        mError.Text = "تعداد صندلی فروخته شده و رزرو بیشتر از تعداد صندلی نوع اتوکار جدید است و نمیتوان نوع اتوکار را عوض کرد.";
                        puEx.Show();
                        return;
                    }
                    if (numChairs < GetNumChairs(c1.Layout))
                    {
                        List<Ticket> tickets = Helper.Instance.GetTickets(info.ID);
                        List<int> freeChairs = new List<int>();
                        int index = 1;
                        for (int f = 0; f < c2.Layout.Floors; f++)
                            for (int i = 0; i < c2.Layout.Rows; i++)
                                for (int j = 0; j < c2.Layout.Columns; j++)
                                {
                                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                                    if (c2.Layout.Layout1.Contains(key))
                                    {
                                        bool man;
                                        Ticket ticket = GetTicket(tickets, index, out man);
                                        if (ticket == null)
                                            freeChairs.Add(index);
                                        index++;
                                    }
                                }
                        for (int i = 0; i < tickets.Count; i++)
                        {
                            string[] chairs = tickets[i].Chairs.Trim(';').Split(';');
                            for (int j = 0; j < chairs.Length; j++)
                            {
                                index = Convert.ToInt32(chairs[j].Substring(1));
                                if (index > numChairs)
                                {
                                    tickets[i].Chairs = tickets[i].Chairs.Replace(";" + chairs[j] + ";"
                                        , ";" + chairs[j].Substring(0, 1) + freeChairs[0] + ";");
                                    freeChairs.RemoveAt(0);
                                }
                            }
                        }
                    }
                }

            }
            if (carTypeID == Null.NullInteger)
                info.CarTypeID = null;
            else
                info.CarTypeID = carTypeID;

            int id = Tool.GetInt(mCarID.SelectedValue, Null.NullInteger);
            if (id != Null.NullInteger)
            {
                int? oldCarID = info.CarID;
                info.CarID = id;
                Car c = Helper.Instance.GetCar(info.CarID.Value);
                if (c == null)
                {
                    mError.Text = "اتوکار انتخاب نشده است.";
                    puEx.Show();
                    return;
                }
                Service s = Helper.Instance.GetService(info.ServiceID);
                if (c.CarType.SpecialID != s.SpecialID)
                {
                    mError.Text = "نحوه سرویس دهی اتوکار انتخاب شده با نحوه سرویس دهی  تعريف شده در سرويس يکي نيست.";
                    puEx.Show();
                    return;
                }
                if (oldCarID == null || oldCarID.Value != id)
                {
                    info.Comission = c.Commission;
                    if (c.Insurance != null && c.Insurance.InsuranceKMs != null && c.Insurance.InsuranceKMs.Count > 0)
                    {
                        for (int i = 0; i < c.Insurance.InsuranceKMs.Count; i++)
                        {
                            if (c.Insurance.InsuranceKMs[i].StartKM <= s.Path.KMs && s.Path.KMs <= c.Insurance.InsuranceKMs[i].EndKM)
                            {
                                info.BodyInsurance = c.Insurance.InsuranceKMs[i].Price;
                                break;
                            }
                        }
                        //info.BodyInsurance = c.Insurance.InsuranceKMs
                    }
                }
            }
            else
                info.CarID = null;


            id = Tool.GetInt(mDriverID1.SelectedValue, Null.NullInteger);
            if (id != Null.NullInteger)
                info.Driver = Helper.Instance.GetDriver(id);
            else
                info.DriverID1 = null;

            if (Null.NullInteger == EditID)
                Helper.Instance.DB.Trips.InsertOnSubmit(info);
            if (Helper.Instance.Update())
            {
                try
                {
                    mServiceID.DataSource = null;
                    mServiceID.DataBind();
                }
                catch (Exception ex)
                {
                    mError.Text += "--" + ex.Message + "<br />";
                }
                EditID = Null.NullInteger;
                list.SelectedIndex = -1;
                BindData();
            }
            else
            {
                mError.Text = Helper.Instance.LastException.Message;
                puEx.Show();
            }
        }
        catch (Exception ex)
        {
            mError.Text = ex.Message + "<br>" + ex.Message;
            puEx.Show();
        }

    }
    Ticket GetTicket(List<Ticket> tickets, int chairIndex, out bool man)
    {
        man = true;
        string mKey = string.Format(";m{0};", chairIndex);
        string fKey = string.Format(";f{0};", chairIndex);
        for (int i = 0; i < tickets.Count; i++)
            if (tickets[i].Chairs.Contains(mKey))
                return tickets[i];
            else if (tickets[i].Chairs.Contains(fKey))
            {
                man = false;
                return tickets[i];
            }
        return null;
    }
    protected void mServices_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //mServiceID.SelectedIndex = mPaths.SelectedIndex;
        }
        catch { }
        BindData();
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRowView row = e.Row.DataItem as DataRowView;
            if (row != null)
            {
                e.Row.Cells[0].Text = row["ID"].ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(row["Date"], "");
                e.Row.Cells[4].Text = Tool.ToString((ServiceTypes)Tool.GetInt(row["ServiceType"]));
                //e.Row.Cells[5].Text = row["PathTitle"].ToString();
                e.Row.Cells[6].Text = GetDay((DateTime)row["Date"]);
                //e.Row.Cells[7].Text = Tool.GetString(row["CarTypeTitle"], "");
                //e.Row.Cells[8].Text = Tool.GetInt(row["NumTickets"], 0).ToString();
                //e.Row.Cells[9].Text = Tool.GetInt(row["NumReserves"], 0).ToString();

                int carTypeID = Tool.GetInt(row["CarTypeID"], Null.NullInteger);
                if (Tool.GetBool(row["Locked"], false) == true)
                {
                    e.Row.Cells[10].Text = "قفل";
                    e.Row.Cells[11].Text = "قفل";
                    e.Row.Cells[12].Text = "قفل";
                    e.Row.Cells[13].Text = "قفل";
                    e.Row.Cells[14].Text = "قفل";
                    e.Row.CssClass = "lockedRow";
                }
                else
                {
                    HyperLink saleLink = e.Row.Cells[10].Controls[0] as HyperLink;
                    if (carTypeID != Null.NullInteger && saleLink != null)
                        mSaleLinks.Add(saleLink);
                }
                if (carTypeID == Null.NullInteger)
                {
                    e.Row.Cells[10].Text = e.Row.Cells[11].Text = e.Row.Cells[12].Text =
                        e.Row.Cells[13].Text = e.Row.Cells[14].Text = "-";
                }
                //LinkButton btn = e.Row.Cells[e.Row.Cells.Count - 3].Controls[0] as LinkButton;
                //btn.CommandArgument = info.ID.ToString();
                JsTools.HandleDeleteButton(e);
            }
            else
            {
                Trip info = e.Row.DataItem as Trip;
                if (info != null)
                {
                    // Set the ID as Text to Cells[0]
                    e.Row.Cells[0].Text = info.ID.ToString();
                    e.Row.Cells[2].Text = Tool.ToPersianDate2(info.Date, "");
                    //e.Row.Cells[3].Text = info.Service.DepartureTime;// Tool.ToPersianDate(info.Date, "");
                    e.Row.Cells[4].Text = info.Service.ServiceType2;
                    //e.Row.Cells[5].Text = info.Service.Path.Title;
                    e.Row.Cells[6].Text = GetDay(info.Date);
                    e.Row.Cells[7].Text = info.CarType != null ? info.CarType.Title : "";
                    e.Row.Cells[8].Text = Helper.Instance.GetNumTickets(info.ID).ToString();
                    e.Row.Cells[9].Text = Helper.Instance.GetNumReserves(info.ID).ToString();

                    if (info.Locked == true)
                    {
                        e.Row.Cells[10].Text = "قفل";
                        e.Row.Cells[11].Text = "قفل";
                        e.Row.Cells[12].Text = "قفل";
                        e.Row.Cells[13].Text = "قفل";
                        e.Row.Cells[14].Text = "قفل";
                        e.Row.CssClass = "lockedRow";
                    }
                    else
                    {
                        HyperLink saleLink = e.Row.Cells[10].Controls[0] as HyperLink;
                        if (info.CarType != null && saleLink != null)
                            mSaleLinks.Add(saleLink);
                    }
                    if (info.CarType == null)
                    {
                        e.Row.Cells[10].Text = e.Row.Cells[11].Text = e.Row.Cells[12].Text =
                            e.Row.Cells[13].Text = e.Row.Cells[14].Text = "-";
                    }
                    //LinkButton btn = e.Row.Cells[e.Row.Cells.Count - 3].Controls[0] as LinkButton;
                    //btn.CommandArgument = info.ID.ToString();
                    JsTools.HandleDeleteButton(e);
                }
            }
        }
    }
    private string GetDay(DateTime dateTime)
    {
        if (Null.NullDate == dateTime)
            return "";
        switch (dateTime.DayOfWeek)
        {
            case DayOfWeek.Friday:
                return "جمعه";
            case DayOfWeek.Monday:
                return "دوشنبه";
            case DayOfWeek.Saturday:
                return "شنبه";
            case DayOfWeek.Sunday:
                return "یکشنبه";
            case DayOfWeek.Thursday:
                return "پنج شنبه";
            case DayOfWeek.Tuesday:
                return "سه شنبه";
            case DayOfWeek.Wednesday:
                return "چهارشنبه";
        }
        return "";
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Trip info = Helper.Instance.GetTrip(EditID);
        if (info == null)
            return;

        mServiceID.Enabled = false;
        try
        {
            List<Service> all = new List<Service>();
            all.Add(info.Service);
            mServiceID.DataValueField = "ID";
            mServiceID.DataTextField = "Title";
            mServiceID.DataSource = all;
            mServiceID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        Tool.SetSelected(mServiceID, info.ServiceID);
        {
            List<CarType> all = null;
            if (false && info.CarType != null && (Helper.Instance.GetNumTickets(info.ID) > 0 || Helper.Instance.GetNumReserves(info.ID) > 0))
            {
                // show CarTypes with the same SpecialID, LayoutID
                all = Helper.Instance.GetCarTypes(info.Service.SpecialID, info.CarType.LayoutID);
            }
            else
            {
                all = Helper.Instance.GetCarTypes(info.Service.SpecialID);
                CarType none = new CarType();
                none.ID = Null.NullInteger;
                all.Insert(0, none);
            }

            mCarTypeID.DataValueField = "ID";
            mCarTypeID.DataTextField = "Title";
            mCarTypeID.DataSource = all;
            mCarTypeID.DataBind();
        }
        {
            if (info.CarType != null)
            {
                List<Car> all = Helper.Instance.GetCarsByCarType(info.CarTypeID);
                Car none = new Car();
                none.ID = Null.NullInteger;
                all.Insert(0, none);

                mCarID.DataValueField = "ID";
                mCarID.DataTextField = "Title";
                mCarID.DataSource = all;
            }
            else
                mCarID.DataSource = new List<Car>();
            mCarID.DataBind();
        }
        if (info.CarTypeID.HasValue)
            Tool.SetSelected(mCarTypeID, info.CarTypeID.Value);
        else
            Tool.SetSelected(mCarTypeID, Null.NullInteger);
        if (info.CarID.HasValue)
            Tool.SetSelected(mCarID, info.CarID.Value);
        else
            Tool.SetSelected(mCarID, Null.NullInteger);
        if (info.DriverID1.HasValue)
            Tool.SetSelected(mDriverID1, info.DriverID1.Value);
        else
            Tool.SetSelected(mDriverID1, Null.NullInteger);
        mDate.Text = Tool.ToPersianDate(info.Date, "");
        mLocked.Checked = info.Locked == true;

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.GetNumReserves(id) > 0 && !Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false))
        {
            mMsg.Text = "این سرویس بلیط رزرو دارد و نمیتوان آنرا حذف کرد.";
            return;
        }
        if (!Tool.GetBool(Helper.Instance.GetSettingValue("CanDeleteTrip"), false))
        {
            Trip tr = Helper.Instance.GetTrip(id);
            if (tr.Tickets.Count > 0)
            {
                mMsg.Text = "این سرویس بلیط دارد و نمیتوان آنرا حذف کرد.";
                return;
            }
        }
        // Check for WEB user and PostBank user
        User web = Helper.Instance.GetUser("web");
        if (web != null)
        {
            List<Ticket> soldByWeb = Helper.Instance.GetTickets(id, web.ID);
            if ((soldByWeb != null && soldByWeb.Count > 0))
            {
                mMsg.Text = string.Format("این سرویس {0} بلیط فروخته شده اینترنتی دارد و نمیتوان آنرا حذف کرد.", soldByWeb.Count);
                return;
            }
        }
        User postbankUser = Helper.Instance.GetUser("postbank");
        if (postbankUser != null)
        {
            List<Ticket> soldByPost = Helper.Instance.GetTickets(id, postbankUser.ID);
            if ((soldByPost != null && soldByPost.Count > 0))
            {
                mMsg.Text = string.Format("این سرویس {0} بلیط فروخته شده پست بانک دارد و نمیتوان آنرا حذف کرد.", soldByPost.Count);
                return;
            }
        }

        if (Helper.Instance.DeleteTrip(id))
        {
            list.SelectedIndex = -1;
            System.Diagnostics.Stopwatch tw = System.Diagnostics.Stopwatch.StartNew();
            BindData();
            tw.Stop();
            long xyz = tw.ElapsedMilliseconds;
            mMsg.Text = "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected int PathID
    {
        get
        {
            return Tool.GetInt(ViewState["PathID"], Null.NullInteger);
        }
        set
        {
            ViewState["PathID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mServiceID.Enabled = true;
        try
        {
            List<Service> all = Helper.Instance.GetServices();
            mServiceID.DataValueField = "ID";
            mServiceID.DataTextField = "Title";
            mServiceID.DataSource = all;
            mServiceID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        puEx.Show();
    }
    protected void doSaleMini_Click(object sender, EventArgs e)
    {
        if (Tool.CanMini() || Tool.CanSewari())
            Response.Redirect(string.Format("~/sale2.aspx?pathid={0}&cartypeid={1}"
                , this.PathID, Tool.GetInt(mMinies.SelectedValue, Null.NullInteger)));
    }
    protected void list_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Close")
        {
            EditID = Tool.GetInt(e.CommandArgument, Null.NullInteger);
            if (Null.NullInteger == EditID)
                return;
        }
    }
    protected void mServiceID_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<Car> all;
        int serviceID = Tool.GetInt(mServiceID.SelectedValue, Null.NullInteger);
        Service s = Helper.Instance.GetService(serviceID);
        if (s == null)
            all = new List<Car>();
        else
            all = Helper.Instance.GetCarsBySpecialID(s.SpecialID);
        Car none = new Car();
        none.ID = Null.NullInteger;
        all.Insert(0, none);

        mCarID.DataValueField = "ID";
        mCarID.DataTextField = "Title";
        mCarID.DataSource = all;
        mCarID.DataBind();
        puEx.Show();
    }
    protected void mCarTypeID_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<Car> all;
        int carTypeID = Tool.GetInt(mCarTypeID.SelectedValue, Null.NullInteger);
        CarType s = Helper.Instance.GetCarType(carTypeID);
        if (s == null)
            all = new List<Car>();
        else
            all = Helper.Instance.GetCarsByCarType(s.ID);
        Car none = new Car();
        none.ID = Null.NullInteger;
        all.Insert(0, none);

        mCarID.DataValueField = "ID";
        mCarID.DataTextField = "Title";
        mCarID.DataSource = all;
        mCarID.DataBind();
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Stopwatch tw = System.Diagnostics.Stopwatch.StartNew();
        BindData();
        tw.Stop();
        long xyz = tw.ElapsedMilliseconds;
    }
    protected void mShowAll_CheckedChanged(object sender, EventArgs e)
    {
        BindData();
    }
    protected void doPreDate_Click(object sender, EventArgs e)
    {
        DateTime dt = sDateStart.SelectedDate;
        if (dt == Null.NullDate)
            return;
        sDateStart.SelectedDate = dt.AddDays(-1);
        BindData();

    }
    protected void doNextDate_Click(object sender, EventArgs e)
    {
        DateTime dt = sDateStart.SelectedDate;
        if (dt == Null.NullDate)
            return;
        sDateStart.SelectedDate = dt.AddDays(+1);
        BindData();
    }
    protected void showToday_Click(object sender, EventArgs e)
    {
        sDateStart.SelectedDate = DateTime.Now;
        BindData();
    }
}
