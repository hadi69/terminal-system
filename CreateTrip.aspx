﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CreateTrip.aspx.cs" Inherits="CreateTrip" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr class="Title">
                    <td colspan="6">
                        ایجاد سرویسهای دوره ای
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        زمان حرکت
                    </td>
                    <td>
                        ساعت<asp:TextBox ID="sHour" runat="server" CssClass="BT" Width="30px" />
                        دقیقه<asp:TextBox ID="sMinute" runat="server" CssClass="BT" Width="30px" />
                    </td>
                    <td>
                        <asp:Button ID="doMorgen" runat="server" Text="فردا" OnClick="doMorgen_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع سرویس دهی
                        <!-- 
                        جدول نرخ
                        -->
                    </td>
                    <td>
                        <asp:DropDownList ID="sSpecials" runat="server" CssClass="DD" Width="150px" />
                        <asp:DropDownList ID="sRates" runat="server" CssClass="DD" Width="150px" Enabled="false"
                            Visible="false" />
                    </td>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypes" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="نمایش" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--
                        تاریخ جدید
                        -->
                    </td>
                    <td>
                        <asp:TextBox ID="mNewDate" runat="server" CssClass="T" Width="150px" Visible="false" />
                    </td>
                    <td colspan="4">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td>
                        <asp:Button ID="doCreate" runat="server" CssClass="CB" OnClick="doCreate_Click" Text="ايجاد"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <span class="Err">لیست سرویسهای ایجاد نشده</span><br />
            <asp:Label class="Err" runat="server" ID="mError" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ServiceID" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="ServiceID" HeaderText="مسير" />
                    <asp:BoundField DataField="ID" HeaderText="نوع اتوکار" />
                    <asp:BoundField DataField="CarID" HeaderText="ماشين" Visible="false" />
                    <asp:BoundField DataField="DriverID1" HeaderText="راننده1" Visible="false" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
