﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Close.aspx.cs" Inherits="Close" Title="صورت وضعيت سرويس" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0" cellpadding="2">
        <tr>
            <td class="Title" colspan="7">
                صورت وضعيت سرويس
                <asp:Label ID="mTripTitle" runat="server" />&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx'
                    runat="server" ID="doReturn">برگشت</asp:HyperLink>&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx'
                        runat="server" ID="doTechnical" Target="_blank">فهرست کنترل اجزاء فنی</asp:HyperLink></td>
        </tr>
        <tr>
            <td class="BErr" colspan="7">
                <asp:Label ID="mWarnings" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <asp:Label ID="mError" runat="server" CssClass="Err"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <table>
                    <tr>
                        <td style="display:none">
                            نوع سرويس
                        </td>
                        <td>
                            <asp:Label ID="mServiceID" runat="server" CssClass="T" Visible="false"/>
                            &nbsp;&nbsp;&nbsp; مسير
                            <asp:Label ID="mPathID" runat="server" BackColor="Green" Font-Bold="true" Font-Size="13"
                                ForeColor="Yellow" />
                            زمان حرکت ساعت<asp:TextBox ID="mHour" runat="server" CssClass="BT" Width="30px" />
                            دقیقه<asp:TextBox ID="mMinute" runat="server" CssClass="BT" Width="30px" />
                        </td>
                        <td>
                            تاريخ
                        </td>
                        <td>
                            <uc1:DatePicker ID="mDate" runat="server" Width="150px" />
                        </td>
                        <td>
                            شماره صورت
                            <asp:TextBox ID="mNo" runat="server" CssClass="T" Enabled="True" Width='100px' />
                        </td>
                        <td>
                            <table border="0" cellpadding="0">
                                <tr>
                                    <td>
                                        سري صورت
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo4" Width="76px" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs4" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo4" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        &nbsp;-&nbsp;
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo1" Width="30" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs1" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo1" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        /
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo2" Width="30" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs2" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo2" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        /
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo3" Width="30" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs3" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo3" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="doChangeSeries_Open" Text="ابطال شماره صورت" OnClick="doChangeSeries_Open_Click"
                                            CssClass="CB" Width="120px" ValidationGroup="addGroup" Height="19px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td>
      راننده اول
            </td>
            <td>
                <asp:DropDownList ID="mDriverID1" runat="server" CssClass="DD" Width="240px" />
                <cc1:ListSearchExtender TargetControlID='mDriverID1' runat="server" ID="ListSearchExtender1"
                    QueryPattern="Contains" PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseDriver1" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseDriver1_Click"  />
                <asp:Button ID="do_Driver1Edit" runat="server" CssClass="CB" Text="..." 
                    ToolTip="ویرایش" onclick="do_Driver1Edit_Click" />
                <asp:HiddenField runat="server" ID="mDriver1" />
            </td>
            
            <td>
                اتوکار
            
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mCarID" CssClass="DD" Width="320px" />
                <cc1:ListSearchExtender TargetControlID='mCarID' runat="server" ID="mCarIDS" QueryPattern="Contains"
                    PromptText='جستجو' />
            </td>
            <td colspan="2">
                <asp:Button ID="doChooseAuto" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="110px" OnClick="doChooseAuto_Click" />
                    <asp:Button ID="do_CarEdit" runat="server" CssClass="CB" 
                    onclick="do_CarEdit_Click" Text="..." ToolTip="ویرایش" />
                <asp:HiddenField runat="server" ID="mCar" />
                &nbsp;<asp:Button runat="server" ID="doChangeSeries_NoCode" Text="ابطال صورت بدون کد"
                    OnClick="doChangeSeries_NoCode_Click" CssClass="CB" Width="120px" ValidationGroup="addGroup"
                    Height="19px" />&nbsp
                <asp:Button ID="doCheckFani" runat="server" CssClass="CB" Text="استعلام فنی" OnClick="doCheckFani_Click"/>
            </td>
        </tr>
        <tr>
            <td>
                راننده دوم
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mDriverID2" CssClass="DD" Width="240px" />
                <cc1:ListSearchExtender TargetControlID='mDriverID2' runat="server" ID="ListSearchExtender2"
                    QueryPattern="Contains" PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseDriver2" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseDriver2_Click" />
                <asp:Button ID="do_Driver2Edit" runat="server" CssClass="CB" Text="..." 
                    ToolTip="ویرایش" onclick="do_Driver2Edit_Click" />
                <asp:HiddenField runat="server" ID="mDriver2" />
            </td>
            <td colspan="2">
                &nbsp;<asp:Button runat="server" ID="doJustSave" Text="محاسبه دریافت کدرهگیری" CssClass="CB" Width="100px"
                    ValidationGroup="addGroup" OnClick="doJustSave_Click" />&nbsp;&nbsp;کد رهگیری:&nbsp;
                <asp:Label ID="mTrackCode" runat="server" CssClass="BT" />
                &nbsp; &nbsp;
            </td>
            <td>
                <asp:Button runat="server" ID="doClose" Text="ذخيره و بستن F7" CssClass="CB" Width="110px"
                    ValidationGroup="addGroup" OnClick="doClose_Click" />&nbsp;
                <asp:Button runat="server" ID="doPrint" Text="چاپ F9" CssClass="CB" Width="90px"
                    OnClick="doPrint_Click" />
                &nbsp; <asp:Label ID="mCloseDate" runat="server" CssClass="BT" Visible="false"/>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                راننده سوم
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mDriverID3" CssClass="DD" Width="240px" />
                <cc1:ListSearchExtender TargetControlID='mDriverID3' runat="server" ID="ListSearchExtender3"
                    QueryPattern="Contains" PromptText='جستجو' />
            </td>
            <td>
                <asp:Button ID="doChooseDriver3" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                    Width="100px" OnClick="doChooseDriver3_Click" />
                <asp:Button ID="do_Driver3Edit" runat="server" CssClass="CB" 
                    EnableViewState="False" Text="..." ToolTip="ویرایش" 
                    onclick="do_Driver3Edit_Click" />
                <asp:HiddenField runat="server" ID="mDriver3" />
            </td>
            <td colspan="2">
                &nbsp;<asp:Button runat="server" ID="doChangeSeries" Text="ابطال صورت با کد" CssClass="CB"
                    Width="100px" ValidationGroup="addGroup" OnClick="doChangeSeries_Click" Height="19px" />
                &nbsp;&nbsp; &nbsp;<asp:Label ID="mTrackDate" runat="server" CssClass="BT" Visible="false"/>
                &nbsp; 
                <asp:Label ID="mTripState" runat="server" CssClass="BT" Visible="false"/>
            </td>
            <td>
                <asp:Button runat="server" ID="doRequestInsurance" Text="ثبت صورت در بیمه" CssClass="CB"
                    Width="120px" OnClick="doRequestInsurance_Click" Visible="False"/>
                <asp:Button runat="server" ID="doSendToTechnical" Text="ارسال به مدیر فنی" CssClass="CB"
                    Width="120px" OnClick="doSendToTechnical_Click" Visible="False"/>
            </td>
        </tr>
        <tr>
            <td style="display:none">
                کارمزد بيمه بدنه
            </td>
            <td>
                <asp:TextBox ID="mBodyInsuranceWage" runat="server" CssClass="T" Enabled="false" Visible="false"/>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="display:none">
                کارمزد بيمه سرنشين
            </td>
            <td colspan="2">
                <asp:TextBox ID="mInsuranceWage" runat="server" CssClass="T" Enabled="false" Visible="false"/>
                &nbsp; &nbsp;
                <asp:Button runat="server" ID='doInvalid' Text='ابطال' CssClass="CB" OnClick="doInvalid_Click"
                    Visible="false" />
                <asp:Button runat="server" ID="doGetTrackingCode" Text="دریافت کد رهگیری" CssClass="CB"
                    Width="120px" OnClick="doGetTrackingCode_Click" Visible="False" />
                <asp:Button runat="server" ID="doSaveInSetting" Text="ذخیره در تنظیمات" CssClass="CB"
                    Width="120px" OnClick="doSaveInSetting_Click" Visible="false" />
                &nbsp; &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <cc1:CollapsiblePanelExtender ID="ccex34" runat="server" TargetControlID="valPanel"
        Collapsed="true" TextLabelID='valLabel' CollapsedText="+" ExpandedText="-" CollapseControlID="valColPanel"
        ExpandControlID="valColPanel">
    </cc1:CollapsiblePanelExtender>
    <asp:Panel ID="valColPanel" runat="server" CssClass="Title">
        <asp:Label ID="valLabel" runat="server" Text="Label"></asp:Label>&nbsp;هزينه ها
    </asp:Panel>
    <asp:Panel ID="valPanel" runat="server">
        <table>
            <tr>
                <td>
                    عوارض
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mToll" CssClass="T" Width="70" />%
                    <cc1:FilteredTextBoxExtender ID="dff1" runat="server" FilterMode="ValidChars" ValidChars='0123456789.'
                        TargetControlID="mToll" />
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator1" Type="Double" runat="server" Display="Dynamic"
                        ValidationGroup="addGroup" ControlToValidate="mToll" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 100 باشد"
                        MaximumValue="100" MinimumValue="0" />
                </td>
                <td>
                    کميسيون
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mComission" CssClass="T" />
                    %
                </td>
                <td>
                    <cc1:FilteredTextBoxExtender ID="ftx3" runat="server" FilterMode="ValidChars" ValidChars='0123456789.'
                        TargetControlID="mComission" />
                    <asp:RangeValidator ID="RangeValidator4" Type="Integer" runat="server" Display="Dynamic"
                        ValidationGroup="addGroup" ControlToValidate="mComission" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                        MaximumValue="2000000000" MinimumValue="0" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    بيمه بدنه
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mBodyInsurance" CssClass="T" />
                </td>
                <td>
                    <cc1:FilteredTextBoxExtender ID="ftx7" runat="server" FilterMode="ValidChars" ValidChars='0123456789.'
                        TargetControlID="mBodyInsurance" />
                    <asp:RangeValidator ID="RangeValidator5" Type="Integer" runat="server" Display="Dynamic"
                        ValidationGroup="addGroup" ControlToValidate="mBodyInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                        MaximumValue="2000000000" MinimumValue="0" />
                </td>
                <td>
                    بیمه سرنشین
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mInsurance" CssClass="T" />
                </td>
                <td>
                    <asp:RangeValidator ID="rv22" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                        ControlToValidate="mInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                        MaximumValue="2000000000" MinimumValue="0" />
                    <cc1:FilteredTextBoxExtender ID="dff3" runat="server" FilterMode="ValidChars" ValidChars='0123456789.'
                        TargetControlID="mInsurance" />
                </td>
                <td>
                    متفرقه
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mOthers" CssClass="T" />
                </td>
                <td>
                    <asp:RangeValidator ID="rv2w" Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                        ControlToValidate="mOthers" ErrorMessage="اين مقدار بايد يک عدد بين -2000000000 و 2.000.000.000 باشد"
                        MaximumValue="2000000000" MinimumValue="-2000000000" />
                    <cc1:FilteredTextBoxExtender ID="ftey1" runat="server" FilterMode="ValidChars" ValidChars="-0123456789"
                        TargetControlID="mOthers" />
                </td>
            </tr>
            <tr style="border-bottom: solid 2px red">
                <td>
                    حق تمبر صورت
                </td>
                <td>
                    <asp:TextBox ID="mStamp" runat="server" CssClass="T" />
                </td>
                <td>
                    <cc1:FilteredTextBoxExtender ID="ftx2" runat="server" FilterMode="ValidChars" ValidChars='0123456789.'
                        TargetControlID="mStamp" />
                    <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                        ValidationGroup="addGroup" ControlToValidate="mStamp" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                        MaximumValue="2000000000" MinimumValue="0" />
                </td>
                <td>
                    اضافه
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mExtraCost" CssClass="T" />
                </td>
                <td>
                    <asp:Label runat="server" ID="mExtraCost2" />
                    <cc1:FilteredTextBoxExtender ID="ftx5" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                        TargetControlID="mExtraCost" />
                    <asp:RangeValidator ID="RangeValidator7" Type="Integer" runat="server" Display="Dynamic"
                        ValidationGroup="addGroup" ControlToValidate="mExtraCost" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                        MaximumValue="2000000000" MinimumValue="0" />
                </td>
                <td>
                    پذیرایی
                </td>
                <td>
                    <asp:TextBox runat="server" ID="mReception" CssClass="T" />
                </td>
                <td>
                    <cc1:FilteredTextBoxExtender ID="dff4" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                        TargetControlID="mReception" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:CollapsiblePanelExtender ID="ccex32" runat="server" TargetControlID="moshPanel"
        Collapsed="true" TextLabelID='moshLabel' CollapsedText="+" ExpandedText="-" CollapseControlID="moshColPanel"
        ExpandControlID="moshColPanel">
    </cc1:CollapsiblePanelExtender>
    <asp:Panel ID="moshColPanel" runat="server" CssClass="Title">
        <asp:Label ID="moshLabel" runat="server" Text="Label"></asp:Label>&nbsp;مشخصات سرويس
    </asp:Panel>
    <asp:Panel ID="moshPanel" runat="server">
        <table>
            <tr>
                <td>
                    <table cellpadding="2" cellspacing="0" border="1">
                        <tr>
                            <td>
                                قیمت
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="mPrice" CssClass="T" />
                                <cc1:FilteredTextBoxExtender ID="dff2" runat="server" FilterMode="ValidChars" FilterType="Numbers"
                                    TargetControlID="mPrice" />
                            </td>
                            <td>
                                ت بليط
                            </td>
                            <td>
                                <asp:Label ID="mNumTickets" runat="server" />
                            </td>
                            <td>
                                کل کرايه
                            </td>
                            <td>
                                <asp:Label ID="mTotalPricelbl" CssClass="T" runat="server" ToolTip="کرایه دریافتی از مسافران"
                                    Width="70" />
                            </td>
                            <td>
                                &nbsp; بسته &nbsp;
                            </td>
                            <td>
                                &nbsp;
                                <asp:CheckBox ID="mClosed" runat="server" Enabled="False" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:CollapsiblePanelExtender ID="cce3" runat="server" TargetControlID="kosPanel"
        Collapsed="true" TextLabelID='kosLabel' CollapsedText="+" ExpandedText="-" CollapseControlID="kosColPanel"
        ExpandControlID="kosColPanel">
    </cc1:CollapsiblePanelExtender>
    <asp:Panel ID="kosColPanel" runat="server" CssClass="Title">
        <asp:Label ID="kosLabel" runat="server" Text="Label"></asp:Label>&nbsp;کسورات
    </asp:Panel>
    <asp:Panel ID="kosPanel" runat="server">
        <table>
            <tr>
                <td>
                    <table cellpadding="2" cellspacing="0" border="1" width="100%">
                        <tr>
                            <td>
                                کل کرايه
                            </td>
                            <td>
                                عوارض
                            </td>
                            <td>
                                کمیسیون
                            </td>
                            <td>
                                پذیرایی
                            </td>
                            <td>
                                سایر کسورات
                            </td>
                            <td>
                                جمع کسورات
                            </td>
                            <td>
                                سهم اتوکار
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="mTotalPrice" CssClass="T" runat="server" ToolTip="کرایه دریافتی از مسافران + کرايه بدون بليط"
                                    Width="70" />
                            </td>
                            <td>
                                <asp:TextBox ID="mToll2" CssClass="T" Width="70" runat="server" ToolTip="(کل کرایه – کسورات ( عوارض + بیمه بدنه +  بیمه  سرنشین +  حق تمبر + هزینه اضافی ) )*(درصد کمیسیون)" />
                            </td>
                            <td>
                                <asp:TextBox ID="mComission2" CssClass="T" Width="70" runat="server" ToolTip="(کل کرایه – کسورات ( عوارض + بیمه بدنه +  بیمه  سرنشین +  حق تمبر + هزینه اضافی ) )*(درصد کمیسیون)" />
                                <asp:Button runat="server" ID="doChangeComission" Text="تایید" CssClass="CB" Width="30px"
                                    ValidationGroup="addGroup" OnClick="doChangeComission_Click" />
                            </td>
                            <td>
                                <asp:TextBox ID="mTotalReception" CssClass="T" Width="70" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="mOtherDeficits" CssClass="T" Width="70" runat="server" ToolTip="= بيمه بدنه، بیمه سرنشین، اضافه و حق تمبر صورت" />
                            </td>
                            <td>
                                <asp:TextBox ID="mSumDeficits" CssClass="T" Width="70" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="mAutoShare" CssClass="T" runat="server" Width="70" ToolTip="کل کرایه - عوارض - سقف بيمه سرنشين - حق تمبر صورت - کمیسیون - بیمه بدنه" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:CollapsiblePanelExtender ID="ccex1" runat="server" TargetControlID="destPanel"
        Collapsed="true" TextLabelID='destLabel' CollapsedText="+" ExpandedText="-" CollapseControlID="destColPanel"
        ExpandControlID="destColPanel">
    </cc1:CollapsiblePanelExtender>
    <asp:Panel ID="destColPanel" runat="server" CssClass="Title">
        <asp:Label ID="destLabel" runat="server" Text="Label"></asp:Label>&nbsp;مقاصد
    </asp:Panel>
    <asp:Panel ID="destPanel" runat="server">
        <table>
            <tr>
                <td style="height: 31px">
                    <asp:PlaceHolder ID="mTripDests" runat="server"></asp:PlaceHolder>
                    <asp:Label ID="mTripDestsLbl" runat="server" Text="Label" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel1">
        <table>
            <tr>
                <td>
                    <hr style="color: DarkRed; height: 1px" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;برای محاسبه مجدد، دکمه محاسبه(=محاسبه + ذخیره) را کلیک کنید &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Button runat="server" ID="doSave" Text="ذخيره" CssClass="CB" Width="90px" ValidationGroup="addGroup"
                        OnClick="doSave_Click" />&nbsp;
                    <asp:Button runat="server" ID="doReadFromCard" Text="مشاهده کارت" CssClass="CB" Width="90px"
                        ValidationGroup="addGroup1" OnClick="doSaveInCard_Click" />&nbsp;
                    <asp:Button ID="doSaveInCard" runat="server" CssClass="CB" OnClick="doSaveInCard_Click"
                        Text="نوشتن در کارت" ValidationGroup="addGroup1" Width="90px" />
                    &nbsp;
                    <asp:Button ID="doDelete" runat="server" CssClass="CB" OnClick="doDelete_Click"
                        Text="حذف" ValidationGroup="none" Width="90px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
        PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
        BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
    </cc1:ModalPopupExtender>
    <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
        <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
            اطلاعات ثبت شده روی کارت</asp:Panel>
        <table border="0">
            <tbody>
                <tr>
                    <td class="N">
                        کد شرکت
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="company">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        شماره صورت
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="surat">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        تاریخ
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="date">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        ساعت
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="time">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        کد مبدا
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="source">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        کد مقصد
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="distin">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        مسافر
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="pass">
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        مبلغ
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="price" />
                    </td>
                </tr>
                <tr>
                    <td class="N">
                    </td>
                    <td>
                        <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td class="N" colspan="2">
                        <asp:Label ID="mWriteMsg" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
</asp:Content>
