﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Helper.Instance.Init();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblVersion.Text = "16.02.16";
        if (SiteSettings.User == null)
        {
            DynMenu2.Visible = false;
            if (!Request.Url.AbsoluteUri.ToLower().Contains("default.aspx"))
                Response.Redirect("~/default.aspx");
        }
        else
            mUserName.Text = SiteSettings.User.FullName;
        if (mBindHotKeys)
            JsTools.BindHotKeysF(Page, new System.Collections.Generic.List<string>(), new System.Collections.Generic.List<int>());
        
            CheckCartable();
    }
    bool mBindHotKeys = true;
    public bool BindHotKeys { get { return mBindHotKeys; } set { mBindHotKeys = value; } }
    public void ShowMenu()
    {
        DynMenu2.Visible = SiteSettings.User != null;
    }

    public string CheckCartable()
    {
        var c = from cs in Helper.Instance.DB.CompanyTrips
            where cs.Status == (int) Status.NoCheck
            select cs;
        int count = c.Count();
        return count.ToString();

    }
}
