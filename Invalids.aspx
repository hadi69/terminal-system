﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Invalids.aspx.cs" Inherits="Invalids" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">

    <script src="js/jquery.min.js" type="text/javascript"></script>

    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <div style='border: solid 1px #000; width: 95%'>
                <table border="0">
                    <tr>
                        <td colspan="8">
                            جستجو
                        </td>
                    </tr>
                    <tr>
                        <td>
                            از تاريخ
                        </td>
                        <td>
                            <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                        </td>
                        <td>
                            تا تاريخ
                        </td>
                        <td>
                            <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                        </td>
                        <td colspan="6">
                            <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                                CssClass="CB"></asp:Button>&nbsp;<a href='#' class="CB" id="printBime" onclick='showHtml(event)'
                                    style='color: #fff'>&nbsp;&nbsp;چاپ&nbsp;&nbsp;</a>
                            <input type="hidden" runat="server" id="prinQry" class="printQry" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan=8>
                            <asp:DropDownList ID="sServiceID" runat="server" CssClass="DD" Width="150px" Visible="false" />
                            <!--
                        نوع اتوکار-->
                            <asp:DropDownList ID="sCarTypeID" runat="server" CssClass="DD" Width="150px" Visible="false" />
                            <asp:DropDownList ID="sDestID" runat="server" CssClass="DD" Width="150px" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            راننده اول
                        </td>
                        <td>
                            <asp:DropDownList ID="sDriverID1" runat="server" CssClass="DD" Width="150px" />
                        </td>
                        <td>
                            اتوکار
                        </td>
                        <td>
                            <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="150px" />
                        </td>
                        <td>
                            سري صورت
                        </td>
                        <td>
                            <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px" />
                        </td>
                        <td>
                            شماره صورت
                        </td>
                        <td colspan=4>
                            <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan=2>
                            <asp:Button ID="doRefineNew" runat="server" CssClass="CB" OnClick="doRefineNew_Click"
                                Text="ابطال صورت بدون کد" Width="134px" />
                        </td>
                        <td>
                            کد رهگیری
                        </td>
                        <td>
                            <asp:TextBox ID="mTrackCode" runat="server" CssClass="T" Width="180px" />
                        </td>
                        <td colspan=4>
                            <asp:Button ID="doInvalidWithCode" runat="server" CssClass="CB" OnClick="doInvalidWithCode_Click"
                                Text="ابطال صورت با کد" Width="116px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" >
                            توجه: فقط شماره صورتهای استفاده نشده باطل میشوند
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <asp:Label ID="mMsg" runat="server" CssClass="Err"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style='clear: both'>
                </div>
            </div>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                    <asp:BoundField DataField="ServiceID" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="ServiceID" HeaderText="مسير" />
                    <asp:BoundField DataField="CarTitle" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverTitle" HeaderText="راننده1" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                    <asp:BoundField DataField="TrackCode" HeaderText="کد رهگیری" />
                    <asp:CheckBoxField DataField="Exclusive" HeaderText="دربستی " />
                    <asp:CommandField SelectText="برگرداندن" ShowSelectButton="True" Visible="false" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
        function showHtml(event) {
            //event.preventDefault();
            //alert(2);
            var url = "invalidshtml.aspx?" + $('.printQry').val();
            //alert(url);
            window.open(url, "Zweitfenster", "location=1,status=1,scrollbars=1,width=1000,height=600,left=10,top=10")
            return false;
        }
    </script>

</asp:Content>
