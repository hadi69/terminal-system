﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Sale.aspx.cs" Inherits="Sale" Title="فروش" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">


    <script language="javascript" type="text/JavaScript">
<!--
        function timedRefresh(timeoutPeriod) {
            setTimeout("location.reload(true);", timeoutPeriod);
        }
        //timedRefresh(5000);
//   -->
    </script>

    <script language="javascript" type="text/javascript">
        function check(a) {
            alert(0);
            var s = a.checked; //$get(a);//.checked;
            alert(s);
            alert(22);
            var b;
            var q = 0; //$get('test').value;
            alert(s);
            if (s == true) {
                alert(1);
                (q == 0) ? b = "Blue" : b = "Green";
                a.style.backgroundColor = b;
                a.style.backgroundImage = "url(pic/" + q + ".gif)";
            }
            else {
                alert(2);
                a.style.backgroundColor = "Red";
                a.style.backgroundImage = "url(pic/01.ico)";
            }

        }
    </script>

    <script language="JavaScript" type="text/JavaScript">
        function setColor($who) {
            $i = $who;
            if (($get("cb" + $i).checked) && ($get("cb" + $i).disabled == false)) {
                $get("select_span" + $i).style.background = '#FFA6A6';
                p
            } else if (($get("cb" + $i).checked == false) && ($get("cb" + $i).disabled == false)) {
                $get("select_span" + $i).parent.style.background = '#FFFF99';
            }
        }

        function resetColor($who, $total) {
            if (($get("cb" + $who).checked == false) && ($get("cb" + $who).disabled == false))
                $get("select_span" + $who).style.background = '';
        }

        function setCheck($who) {
            if ($get("cb" + $who).disabled == true) {
                window.location = window.location + "&do=payback&who=" + $who;
                return;
            }
            $get("cb" + $who).checked = !($get("cb" + $who).checked);
            //if($get("cb"+$who).checked && $get("fld_Title_No").value==2) {
            //	if($get("cb"+$who).checked) {
            //				$get("cbPic"+$who).src = 'images/woman.gif';				
            //				$get("cbSex"+$who).value = 0;
            //	}			
        }

        function path_finder($p) {
            $source = $p.split("/");
            $step = $source.length - 1;
            $path = $source[$step];
            //alert($path);
            return $path;
        }

        function changeSex($key) {
            //0 means woman    &&&    1 means man    &&&   2 means disabled
            //$sex=1;
            $ppp = $get("cbPic" + $key).src;
            if (path_finder($ppp) == path_finder('images/man.gif')) {
                $sex = 0;
            } else if (path_finder($ppp) == path_finder('images/woman.gif')) {
                $sex = 1;
            } else {
                $sex = 2;
            }
            if ($sex == 1) {
                $get("cbSex" + $key).value = 1;
                $get("cbPic" + $key).src = 'images/man.gif';
                $get("cbPic" + $key).className = "";
            } else if ($sex == 0) {
                $get("cbPic" + $key).src = 'images/woman.gif';
                $get("cbPic" + $key).className = "";
                $get("cb" + $key).checked = true;
                $get("cbSex" + $key).value = 0;
            } else {
            }
        }

        function setText(abc) {
            //alert(0);
            $get("passName").innerText = abc;
            //  alert(1);
        }
    </script>

    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                </ContentTemplate>
            </asp:UpdatePanel>
            <table border="0">
                <tr>
                    <td valign="top">
                        <table border="0">
                            <tr>
                                <td colspan="6" runat="server" id="mFilter" class="TitleTransparent">
                                    فروش سرويس
                                    <asp:Label ID="mTripTitle" runat="server" />&nbsp;-&nbsp;<a href='Trips.aspx'>برگشت</a>
                                    &nbsp;-&nbsp;<asp:HyperLink ID="doClose" runat="server" Text='صورت وضعيت F4' />&nbsp;-&nbsp;<asp:HyperLink
                                        ID="doRefresh" runat="server" Text='فراخوانی' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    عنوان
                                </td>
                                <td>
                                    <asp:DropDownList ID="mTitle" runat="server" CssClass="DD" AutoPostBack="True" OnSelectedIndexChanged="mTitle_SelectedIndexChanged" />
                                </td>
                                <td>
                                    نام
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="mName" CssClass="BT" Width="120px" /><span class="red">*</span>
                                </td>
                                <td>
                                    اشتراک
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="mCustomerNo" CssClass="BT" Width="75px" />
                                    <asp:Button ID="doShowCustomer" runat="server" CssClass="CB" OnClick="doShowCustomer_Click"
                                        Text="F12" Width="25px" />
                                </td>
                            </tr>
                            <tr>
                                <td>آدرس</td>
                                <td colspan="5">
                                    <asp:TextBox runat="server" ID="mAddress" CssClass="BT" Width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    مقصد
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="mCityID" CssClass="DD" AutoPostBack="True" OnSelectedIndexChanged="mCityID_SelectedIndexChanged"
                                        Width="120px" />
                                </td>
                                <td>
                                    صندوق
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="mFundID" CssClass="DD" Width="120px" />
                                </td>
                                <td>
                                    دفتر
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList runat="server" ID="mBranchID" CssClass="DD" Width="120px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    موبایل
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="mTel" CssClass="BT" Width="120px" />
                                </td>
                                <td>
                                    تخفيف
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="mDiscount" CssClass="BT" Width="120px" />
                                </td>
                                <td>
                                    &nbsp; نرخ
                                </td>
                                <td style="text-align: left">
                                    <asp:Label ID="mPrice" runat="server" Text="0"></asp:Label>
                                    &nbsp; &nbsp;
                                    <asp:Button runat="server" ID="doSale" Text='فروش F2' Width="80px" CssClass="CBBJ"
                                        OnClick="doSale_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!-- 
                                    تاريخ انقضاء
                                    -->
                                </td>
                                <td>
                                    <uc1:DatePicker ID="mExpireDate" runat="server" Width="150px" Visible="false" />
                                    <asp:HiddenField ID="hiddentripID" runat="server" />
                                </td>
                                <td>
                                    <!-- 
                                    زمان
                                    -->
                                </td>
                                <td colspan="3" style="text-align: left">
                                    <asp:TextBox runat="server" ID="mExpireTime" CssClass="BT" Width="120px" Visible="false" />
                                    <cc1:FilteredTextBoxExtender FilterMode="ValidChars" ValidChars="0123456789:" runat="server"
                                        TargetControlID="mExpireTime" />
                                    <asp:Button ID="doReserve" runat="server" CssClass="CBBJ" OnClick="doReserve_Click"
                                        Text="رزرو" Width="80px" />
                                    <asp:Button ID="doSale2" runat="server" CssClass="CBBJ" OnClick="doSale2_Click" Text="فروش بدون چاپ"
                                        Width="110px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox ID="mAutoPrint" runat="server" Text="چاپ خودکار" Checked="true" />
                                </td>
                                <td colspan="3">
                                    <asp:LinkButton ID="doSelectAll" runat="server" Text='انتخاب همه' OnClick="doSelectAll_Click" />&nbsp;&nbsp;
                                    <asp:LinkButton ID="doDeSelectAll" runat="server" Text='پاک کردن همه' OnClick="doDeSelectAll_Click" />
                                    &nbsp;&nbsp; &nbsp;
                                </td>
                                <td style="text-align: left">
                                    <asp:Button ID="doSale3" runat="server" CssClass="CBBJ" Text="فروش و چاپ کامل"
                                        Width="120px" OnClick="doSale3_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" class="Err">
                                    <asp:Label ID="mMsg" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    اصلاح بليط
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList runat="server" ID="mTickets" CssClass="DD" Width="150px" />
                                </td>
                                <td style="text-align: left" colspan="3">
                                    <asp:Button runat="server" ID="doChange" Text='اصلاح بلیط' Width="100px" CssClass="CBBJ"
                                        OnClick="doChange_Click" />
                                    <asp:Button runat="server" ID="doChangeOk" Text='تاييد' Width="80px" CssClass="CBBD"
                                        OnClick="doChangeOk_Click" />
                                    <asp:Button runat="server" ID="doChangeCancel" Text='انصراف' Width="80px" CssClass="CBBD"
                                        OnClick="doChangeCancel_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="mIsWeb1" runat="server" CssClass="BErr" Visible="false">توجه: این بلیط از طریق فروش اینترنتی فروخته شده است</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="mIsPost1" runat="server" CssClass="BErr" Visible="false">توجه: این بلیط از طریق دفاتر پست بانک فروخته شده است</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    فروش رزرو
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList runat="server" ID="mReserves" CssClass="DD" Width="150px" />
                                </td>
                                <td colspan="3" style="text-align: left">
                                    &nbsp;
                                    <asp:Button runat="server" ID="doConfirmReserve" Text='فروش رزرو' Width="80px" CssClass="CBBJ"
                                        OnClick="doConfirmReserve_Click" />
                                    <asp:Button runat="server" ID="doCancel" Text='لغو رزرو' Width="80px" CssClass="CBBJ"
                                        OnClick="doCancel_Click" />
                                    <asp:Button runat="server" ID="doCancelAll" Text='لغو همه رزروها' Width="100px" CssClass="CBBJ"
                                        OnClick="doCancelAll_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    &nbsp;
                                    <asp:HyperLink ID="doShowPre" runat="server">قبلی</asp:HyperLink>
                                    &nbsp;
                                </td>
                                <td style="text-align: left">
                                    <asp:Button runat="server" ID="doAutoFill" Text='پرکردن خودکار' Width="100px" CssClass="CBBJ"
                                        OnClick="doAutoFill_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    &nbsp;
                                    <asp:HyperLink ID="doShowNext" runat="server">بعدی</asp:HyperLink>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="Title">
                                    مقاصد
                                </td>
                                <td colspan="3">
                                    <asp:PlaceHolder ID="mTripDests" runat="server"></asp:PlaceHolder>
                                </td>
                                <td class="Title">
                                    تخفیف
                                </td>
                                <td>
                                    <asp:Label ID="mSumDiscount" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" class="BErr">
                                    <asp:Label ID="mError" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <p style="margin-bottom: 0cm; text-align: left; font-size: 12px; font-weight: bold;
                            font-family: tahoma">
                            <span style="font-weight: normal">F9=نام، F7=صندلیها &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>مسافر:
                            <span id="passName"></span>
                            <br />
                            <span style="color: #009933">کل:
                                <asp:Label ID="mAll" runat="server" />
                            </span>&nbsp;&nbsp; <span style="color: #FF0000">پر:
                                <asp:Label ID="mFull" runat="server" />
                            </span>&nbsp;&nbsp; <span style="color: #6666FF">رزرو:
                                <asp:Label ID="mNumReserves" runat="server" />
                            </span>&nbsp;&nbsp; خالی:
                            <asp:Label runat="server" ID="mNumEmpty" />
                        </p>
                        <asp:PlaceHolder runat="server" ID="mLayout" />
                    </td>
                </tr>
            </table>
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    استرداد بليط</asp:Panel>
                <table border="0">
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="mIsWeb" runat="server" CssClass="BErr">توجه: این بلیط از طریق فروش اینترنتی فروخته شده است</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="mIsPost" runat="server" CssClass="BErr">توجه: این بلیط از طریق دفاتر پست بانک فروخته شده است</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            نام مسافر
                        </td>
                        <td>
                            <asp:Label ID="pbFullname" runat="server" CssClass="L" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            شماره صندلی
                        </td>
                        <td>
                            <asp:Label ID="pbChairs" runat="server" CssClass="L" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            کسر از حساب
                        </td>
                        <td>
                            <asp:RadioButton ID="mFromFund" runat="server" Text='صندوق' Checked="True" GroupName="Acc" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:RadioButton ID="mFromSale" runat="server" Text='دفتر فروش' GroupName="Acc" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr runat=server id=mNoTR>
                        <td>
                            شماره بليط
                        </td>
                        <td>
                            <asp:TextBox ID="mTicketNo" runat="server" CssClass="T" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left" colspan="3">
                            <asp:Button runat="server" ID="doSubtract" Text="با کسر" CssClass="CB" Width="80px"
                                OnClick="doSubtract_Click" />
                            &nbsp;
                            <asp:Button runat="server" ID="doPayback" Text="بي کسر" CssClass="CB" Width="80px"
                                OnClick="doPayback_Click" />&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx' runat="server"
                                    ID="doReturn">انصراف</asp:HyperLink>
                            <span style="visibility: hidden">
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="mMsg2" runat="server" CssClass="BErr"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="doSale" />
            <asp:PostBackTrigger ControlID="doSale2" />
            <asp:PostBackTrigger ControlID="doReserve" />
            <asp:PostBackTrigger ControlID="doSale3" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
