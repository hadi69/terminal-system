﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Tickets.aspx.cs" Inherits="Tickets" Title="ليست بليطها" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0" width="1200">
        <tr>
            <td class="Title" colspan="6">
                ليست بليطهاي مربوط به سرويس
                <asp:Label ID="mTripTitle" runat="server" />&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx'
                    runat="server" ID="doReturn">برگشت</asp:HyperLink><asp:Button runat="server" ID="doPrint" Text="چاپ گزارش فروش خالص" CssClass="CB" 
                     onclick="doPrint_Click"  />
                <asp:Button runat="server" ID="doPrintTickets" Text="چاپ لیست بلیط ها" CssClass="CB" OnClick="doPrintTickets_Click" OnClientClick="aspnetForm.target ='_blank';"/>
            
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                    Width="100%">
                    <Columns>
                        <asp:BoundField DataField="ID" Visible="False" />
                        <asp:BoundField DataField="ID" HeaderText="رديف" />
                        <asp:BoundField DataField="No" HeaderText="شماره بليط" />
                        <asp:BoundField DataField="No" HeaderText="صندلیها" />
                        <asp:BoundField DataField="Fullname" HeaderText="نام" />
                        <asp:BoundField DataField="Tel" HeaderText="تلفن" />
                        <asp:BoundField DataField="Address" HeaderText="آدرس" />
                        <asp:BoundField DataField="ID" HeaderText="کرايه" />
                        <asp:BoundField DataField="ID" HeaderText="تعداد" />
                        <asp:BoundField DataField="ID" HeaderText="جمع" />
                        <asp:BoundField DataField="CityID" HeaderText="مقصد" />
                        <asp:BoundField DataField="BranchID" HeaderText="دفتر" />
                        <asp:BoundField DataField="SaleUserID" HeaderText="فروشنده" />
                        <asp:BoundField HeaderText="تاریخ حرکت" />
                        <asp:BoundField DataField="SaleDate" HeaderText="تاریخ فروش" />
                        <asp:BoundField DataField="SaleDate" HeaderText="ساعت فروش" />
                        <asp:BoundField DataField="ReturnUserID" HeaderText="برگشت دهنده" />
                        <asp:BoundField DataField="PaidBackDate" HeaderText="تاریخ برگشت" />
                        <asp:BoundField DataField="PaidBackDate" HeaderText="ساعت برگشت" />
                        <asp:HyperLinkField HeaderText=" استرداد بليط " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="PayBack.aspx?ticketid={0}"
                            Text="استرداد بليط" />
                        <asp:HyperLinkField HeaderText="چاپ" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Reports/Ticket.aspx?id={0}"
                            Text="چاپ" Target="_blank" />
                        <asp:HyperLinkField HeaderText="چاپ کامل" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Reports/AutoTicket.aspx?id={0}"
                            Text="چاپ کامل" Target="_blank" />
                    </Columns>
                    <EmptyDataTemplate>
                        <br />
                        <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                    <HeaderStyle CssClass="GH" />
                    <RowStyle CssClass="GR" />
                    <AlternatingRowStyle CssClass="GAR" />
                </asp:GridView>
            </td>
        </tr>
                <tr>
            <td class="Title" colspan="6">
                ليست سابقه بليطها
            طها
            </td>
        </tr>

        <tr>
            <td>
                <asp:GridView ID="listHistory" runat="server" AutoGenerateColumns="False" OnRowDataBound="listHistory_RowDataBound"
                    Width="100%">
                    <Columns>
                        <asp:BoundField DataField="ID" Visible="False" />
                        <asp:BoundField DataField="ID" HeaderText="رديف" />
                        <asp:BoundField DataField="Type" HeaderText="نوع" />
                        <asp:BoundField DataField="ID" HeaderText="شماره بليط" />
                        <asp:BoundField DataField="ID" HeaderText="نام" />
                        <asp:BoundField DataField="ID" HeaderText="تلفن" />
                        <asp:BoundField DataField="ID" HeaderText="کرايه" />
                        <asp:BoundField DataField="ID" HeaderText="تعداد" />
                        <asp:BoundField DataField="ID" HeaderText="جمع" />
                        <asp:BoundField DataField="ID" HeaderText="مقصد" />
                        <asp:BoundField DataField="ID" HeaderText="دفتر" />
                        <asp:BoundField DataField="SaleUserID" HeaderText="فروشنده" />
                        <asp:BoundField DataField="SaleUserID" HeaderText="اصلاح کننده" />
                    </Columns>
                    <HeaderStyle CssClass="GH" />
                    <RowStyle CssClass="GR" />
                    <AlternatingRowStyle CssClass="GAR" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
