﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Trips.aspx.cs" Inherits="Trips" Title="فروش" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">

    <script language="javascript" type="text/javascript">
    
var oldgridSelectedColor;

//setMouseOverColor-->sMOv
function v(element)
{
    oldgridSelectedColor = element.style.backgroundColor;
    element.style.backgroundColor='yellow';
    element.style.cursor='hand';
    element.style.textDecoration='none';
}
//setMouseOutColor-->sMOu
function u(element)
{
    element.style.backgroundColor=oldgridSelectedColor;
    element.style.textDecoration='none';
}
function hK(up, down)
{
    if ((event.keyCode==40))
    {
        if (down != null)   
        {
            $get(down).focus();
            event.keyCode= 0;
            return false;
        }
        return event.keyCode ;
    }
     if ((event.keyCode==38))
    {
        if (up != null)   
        {
            $get(up).focus();
            event.keyCode= 0;
            return false;
        }
        return event.keyCode ;
    }
}
    </script>

    <table border="0" class="filter" runat="server" id="mFilter">
        <tr>
            <td>
                ليست سرويسها
            </td>
            <td>
                تاریخ
            </td>
            <td>
                <asp:LinkButton ID="doPreDate" runat="server" Text="&lt;&lt;" ToolTip="روز قبلی"
                    OnClick="doPreDate_Click" />
            </td>
            <td>
                <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
            </td>
            <td>
                <asp:LinkButton ID="doNextDate" runat="server" Text=">>" ToolTip="روز بعدی" OnClick="doNextDate_Click" />
            </td>
            <td>
                <asp:LinkButton ID="showToday" runat="server" Text='امروز' OnClick='showToday_Click' />&nbsp;&nbsp;
                <a href='Services/Services.aspx?do=new'>تعریف سرویس</a>&nbsp;&nbsp;
            </td>
            <td>
                <asp:CheckBox ID="mShowAll" runat="server" Text='نمایش همه' AutoPostBack="True" OnCheckedChanged="mShowAll_CheckedChanged" />
            </td>
            <td>
                <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                    CssClass="CB"></asp:Button>&nbsp;
            </td>
            <td>
                <asp:UpdateProgress ID="upp1" runat="server" AssociatedUpdatePanelID="up2" DisplayAfter="10">
                    <ProgressTemplate>
                        <img src="images/loading.gif" />
                        صبر کنيد
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
            <td>
                <a href='CreateTrip.aspx?do=sibey'>ایجاد سرویسهای دوره ای فردا</a>&nbsp;&nbsp; F9=مسیرها،
                F7=سرویسها
                <asp:Button ID="doAdd1" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                    Width="50px" Visible="false" />
            </td>
            <td class="N">
                &nbsp;&nbsp;
                <asp:Label runat="server" ID="mMiniTitle" Text='فروش مینی بوس :' />
                <asp:DropDownList ID="mMinies" runat="server" />
            </td>
            <td>
                <asp:LinkButton runat="server" ID="doSaleMini" OnClick="doSaleMini_Click" Text='فروش' />
            </td>
        </tr>
    </table>
    <table border="1" width="100%" cellspacing="0">
        <tr>
            <td valign="top">
                <asp:PlaceHolder ID='mPaths2' runat="server" />
            </td>
            <td valign="top">
                <asp:Label runat="server" ID="mMsg" class="Err" />
                <asp:PlaceHolder ID='mList2' runat="server" />
                <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                    OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                    Style="margin-top: 0px" Width="95%">
                    <Columns>
                        <asp:BoundField DataField="ID" Visible="False" />
                        <asp:BoundField DataField="ID" HeaderText="رديف" />
                        <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                        <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                        <asp:BoundField DataField="ServiceID" HeaderText="نوع سرويس" Visible="true" />
                        <asp:BoundField DataField="ServiceID" HeaderText="مسير" Visible="false" />
                        <asp:BoundField DataField="ID" HeaderText="روز" />
                        <asp:BoundField DataField="CarTypeTitle" HeaderText="نوع اتوکار" />
                        <asp:BoundField DataField="NumTickets" HeaderText="ت بليط" />
                        <asp:BoundField DataField="NumReserves" HeaderText="ت رزرو" />
                        <asp:HyperLinkField HeaderText=" فروش " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Sale.aspx?tripid={0}"
                            Text="فروش" />
                        <asp:HyperLinkField HeaderText=" مسافرين " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Passengers.aspx?tripid={0}"
                            Text="مسافرين" />
                        <asp:HyperLinkField HeaderText=" بليطها " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Tickets.aspx?tripid={0}"
                            Text="بليطها" />
                        <asp:HyperLinkField HeaderText=" صورت وضعيت " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Close.aspx?tripid={0}"
                            Text="صورت وضعيت" />
                        <asp:HyperLinkField HeaderText=" راهنما " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="PrintHelp.aspx?tripid={0}"
                            Text="راهنما" Target="_blank" />
                        <asp:CheckBoxField DataField="Locked" HeaderText="قفل" />
                        <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                        <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        <br />
                        <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                    <HeaderStyle CssClass="GH" />
                    <RowStyle CssClass="GR" />
                    <AlternatingRowStyle CssClass="GAR" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
        PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
        BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
    </cc1:ModalPopupExtender>
    <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="650px" CssClass="modalPopup">
        <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
            اضافه نمودن / ويرايش سرويس)</asp:Panel>
        <table border="0">
            <tbody>
                <tr>
                    <td class="N">
                        تعريف سرويس
                    </td>
                    <td>
                        <asp:DropDownList ID="mServiceID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="mServiceID_SelectedIndexChanged" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="تعريف سرويس الزامي است"
                            ControlToValidate="mServiceID" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        تاريخ
                    </td>
                    <td>
                        <asp:TextBox ID="mDate" runat="server" />
                        <cc1:MaskedEditExtender ID="mee1" TargetControlID="mDate" runat="server" Mask="1399/99/99"
                            ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                        <span dir="ltr">Mask=13--/--/--</span>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="mCarTypeID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="mCarTypeID_SelectedIndexChanged" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        ماشين
                    </td>
                    <td>
                        <asp:DropDownList ID="mCarID" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        راننده 1
                    </td>
                    <td>
                        <asp:DropDownList ID="mDriverID1" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="N">
                        قفل
                    </td>
                    <td>
                        <asp:CheckBox ID="mLocked" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="N">
                    </td>
                    <td>
                        <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                            Text="تاييد" CssClass="CB"></asp:Button>&nbsp;
                        <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="Err" colspan="3">
                        <asp:Label class="Err" runat="server" ID="mError" />
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
