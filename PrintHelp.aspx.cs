﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class PrintHelp : System.Web.UI.Page
{
    Trip mTrip = null;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        //mError.Text = "";
        if (!IsPostBack)
        {
            this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
            mTrip = Helper.Instance.GetTrip(this.TripID);
            if (mTrip == null)
                Response.Redirect("Closeds.aspx", true);            
        }
        else
            mTrip = Helper.Instance.GetTrip(TripID);
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        mHour.Text = Tool.FixTime(mTrip.Service.DepartureTime);
        mDate.Text = Tool.ToPersianDate(mTrip.Date, "");
        mPath.Text = mTrip.Service.Path.Title;
        mNum.Text = Helper.Instance.GetNumTickets(mTrip.ID).ToString();

        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        Layout layout = mTrip.CarType.Layout;
        if (string.IsNullOrEmpty(layout.Layout1))
            return;
        int index = 1;
        int totalprice = 0;
        for (int f = 0; f < layout.Floors; f++)
        {
            //mLayout.Controls.Add(new LiteralControl(string.Format("طبقه {0}:<br />", f + 1)));
            mLayout.Controls.Add(new LiteralControl("<table cellpadding=5 cellspacing=0 border=1 class='PrintHelp'>"));
            for (int i = 0; i < layout.Rows; i++)
            {
                mLayout.Controls.Add(new LiteralControl("<tr>"));
                for (int j = 0; j < layout.Columns; j++)
                {
                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                    if (layout.Layout1.Contains(key))
                    {
                        bool man;
                        Ticket ticket = GetTicket(tickets, index, out man);
                        if (ticket == null)
                            mLayout.Controls.Add(new LiteralControl(string.Format("<td>{0}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>", index)));
                        else
                        {
                            int price = Tool.GetInt(ticket.SaleType == 1 ? 0 : (ticket.Price - ticket.Discount), 0); 
                            string code = string.Format("{0}&nbsp;&nbsp;&nbsp;{1}&nbsp;{2} <br/>کرایه: {3}", index, man ? "آقای" : "خانم", ticket.Fullname,price.ToString("N0"));
                            totalprice = totalprice + price;
                            mLayout.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", code)));
                        }
                        index++;
                    }
                    else
                        mLayout.Controls.Add(new LiteralControl("<td align=center>&nbsp;==========&nbsp;</td>"));
                    if (layout.ColumnSpace == j + 1)
                        mLayout.Controls.Add(new LiteralControl("<td >&nbsp;&nbsp;&nbsp;&nbsp;</td>"));
                }
                mLayout.Controls.Add(new LiteralControl("</tr>"));
            }
            mLayout.Controls.Add(new LiteralControl("</table>"));
            mLayout.Controls.Add(new LiteralControl(string.Format("<br>کل کرایه:{0}",totalprice.ToString("N0"))));
        }
    }
    Ticket GetTicket(List<Ticket> tickets, int chairIndex, out bool man)
    {
        man = true;
        string mKey = string.Format(";m{0};", chairIndex);
        string fKey = string.Format(";f{0};", chairIndex);
        for (int i = 0; i < tickets.Count; i++)
            if (tickets[i].Chairs.Contains(mKey))
                return tickets[i];
            else if (tickets[i].Chairs.Contains(fKey))
            {
                man = false;
                return tickets[i];
            }
        return null;
    }
    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
}
