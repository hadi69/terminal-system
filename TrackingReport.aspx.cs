﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class TrackingReport : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            //BindData();
            doSearch_Click(null, null);
        }
    }
    private void BindInitData()
    {
        {
            List<string> all = (from h in Helper.Instance.DB.Trips
                                select h.Series).ToList();
            all.Insert(0, "");

            List<string> all2 = (from hh in Helper.Instance.DB.Exclusives
                                 select hh.Series).ToList();
            if (all2 != null)
                all.AddRange(all2);

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();
        }

        sDateStart.SelectedDate = DateTime.Now.AddDays(-10);
        sDateEnd.SelectedDate = DateTime.Now;

        if (Null.NullDate != Tool.GetDate(Session["TrackingReport.DateStart"]))
            sDateStart.SelectedDate = Tool.GetDate(Session["TrackingReport.DateStart"]);
        if (Null.NullDate != Tool.GetDate(Session["TrackingReport.DateEnd"]))
            sDateEnd.SelectedDate = Tool.GetDate(Session["TrackingReport.DateEnd"]);
        try { sFNo.Text = Session["TrackingReport.FNo"].ToString(); }
        catch { };
        try { sTNo.Text = Session["TrackingReport.TNo"].ToString(); }
        catch { };
        try { sSeries.SelectedValue = Session["TrackingReport.Series"].ToString(); }
        catch { };
    }
  
    private DataTable GetTrips()
    {
        int fn = (sFNo.Text.Trim().Length > 0) ? int.Parse(sFNo.Text) : 0;
        int tn = (sTNo.Text.Trim().Length > 0) ? int.Parse(sTNo.Text) : int.MaxValue;

        DateTime dt1 = DateTime.Now;
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        string qry = string.Format(@"SELECT Trips.ID, Trips.Date, ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2, Trips.No, Trips.Series, Trips.TrackCode
, Trips.CloseDate, Trips.CloseUserID, Users.UserName from Trips
LEFT JOIN Users ON Trips.CloseUserID=Users.ID
INNER JOIN Services ON Trips.ServiceID = Services.ID
WHERE Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59' AND Trips.Closed=1
AND Trips.TrackCode IS NOT NULL AND Trips.TrackCode <> ''"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);

        qry += " AND Trips.No>=" + fn;
        qry += " AND Trips.No<=" + tn;

        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Trips.TrackCode");

        if (dt != null && dt.Rows.Count > 0)
        {
            if (sSeries.Text.Trim().Length > 0)
            {
                string s = Tool.ParseSeries(sSeries.Text.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series.Contains(s))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }// ---------------------------------
    
        return dt;
    }
    private DataTable GetExclusives()
    {
        int fn = (sFNo.Text.Trim().Length > 0) ? int.Parse(sFNo.Text) : 0;
        int tn = (sTNo.Text.Trim().Length > 0) ? int.Parse(sTNo.Text) : int.MaxValue;

        DateTime dt1 = DateTime.Now;
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        string qry = string.Format(@"SELECT Exclusives.ID, Exclusives.DepartureDate, Exclusives.DepartureTime, Exclusives.No, Exclusives.Series, Exclusives.TrackCode
, Exclusives.CloseDate, Exclusives.CloseUserID, Users.UserName from Exclusives
LEFT JOIN Users ON Exclusives.CloseUserID=Users.ID
WHERE Exclusives.DepartureDate>= '{0}/{1}/{2}' AND Exclusives.DepartureDate<='{3}/{4}/{5} 23:59:59' AND Exclusives.CloseDate IS NOT NULL
AND Exclusives.TrackCode IS NOT NULL AND Exclusives.TrackCode <> ''"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);

        qry += " AND Exclusives.No>=" + fn;
        qry += " AND Exclusives.No<=" + tn;

        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Exclusives.DepartureDate, Exclusives.DepartureTime, Exclusives.TrackCode");

        if (dt != null && dt.Rows.Count > 0)
        {
            if (sSeries.Text.Trim().Length > 0)
            {
                string s = Tool.ParseSeries(sSeries.Text.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series.Contains(s))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }// ---------------------------------

        return dt;
    }
    private void BindData()
    {
        #region printBimeQry.Value
        {
            DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
            string fDate = "";
            if (fromDate != new DateTime())
                fDate = fromDate.ToShortDateString();
            string tDate = "";
            DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
            if (toDate != new DateTime())
                tDate = toDate.ToShortDateString();
            int specialID = 0;
            int fno = 0;
            if (sFNo.Text.Trim().Length > 0)
                fno = Tool.GetInt(sFNo.Text, 0);
            int tno = 0;
            if (sTNo.Text.Trim().Length > 0)
                tno = Tool.GetInt(sTNo.Text, 0);
            string series = "";
            if (sSeries.Text.Trim().Length > 0)
                series = Tool.ParseSeries(sSeries.Text);

            printBimeQry.Value = string.Format("fromDate={0}&toDate={1}&fno={2}&tno={3}&series={4}",
                fDate, tDate, specialID, fno, tno, series);
        }
        #endregion

        Session["TrackingReport.DateStart"] = sDateStart.SelectedDate;
        Session["TrackingReport.DateEnd"] = sDateEnd.SelectedDate;
        Session["TrackingReport.FNo"] = sFNo.Text;
        Session["TrackingReport.TNo"] = sTNo.Text;
        Session["TrackingReport.Series"] = sSeries.Text;

        list.DataSource = GetTrips();
        list.DataBind();

        listE.DataSource = GetExclusives();
        listE.DataBind();

        printBimeQryDisabled.Value = "";
    }
    static System.Drawing.Color warnColor = System.Drawing.Color.FromArgb(255, 255, 32, 32);
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView info = e.Row.DataItem as DataRowView;
            if (info != null)
            {
                e.Row.Cells[0].Text = info["ID"].ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                try
                {
                    e.Row.Cells[2].Text = Tool.ToPersianDate((DateTime)info["Date"], "");
                }
                catch { }
                try
                {
                    e.Row.Cells[7].Text = Tool.ToPersianDate((DateTime)info["CloseDate"], "");
                }
                catch { }
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
   
    protected void listE_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView info = e.Row.DataItem as DataRowView;
            if (info != null)
            {
                e.Row.Cells[0].Text = info["ID"].ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                try
                {
                    e.Row.Cells[2].Text = Tool.ToPersianDate((DateTime)info["DepartureDate"], "");
                }
                catch { }
                try
                {
                    e.Row.Cells[7].Text = Tool.ToPersianDate((DateTime)info["CloseDate"], "");
                }
                catch { }
            }
        }
    }
    protected void listE_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        listE.PageIndex = e.NewPageIndex;
        DataBind();
    }
}
