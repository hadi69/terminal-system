﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Payback.aspx.cs" Inherits="Payback" Title="استرداد بليط" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tr>
            <td class="Title" colspan="2">
                استرداد بليط
            </td>
            <td class="Title">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                شماره بليط
            </td>
            <td>
                <asp:TextBox ID="sNo" runat="server" CssClass="T" />
            </td>
            <td>
                <asp:Button ID="doSearch" runat="server" Text="جستجو" CssClass="CB" OnClick="doSearch_Click"
                    Width="80px" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                شماره بلیط بصورت کامل نوشته شود شامل شماره سرویس، خط تیره، شماره بلیط
                <br />
                8-1 : بلیط 1 از سرویس 8
            </td>
        </tr>
        <tr>
            <td class="Title" colspan="2">
                مشخصات
            </td>
            <td class="Title">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                نام مسافر
            </td>
            <td>
                <asp:Label ID="mName" runat="server" CssClass="L" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
          <tr>
            <td>
                شماره صندلی
            </td>
            <td>
                <asp:Label ID="mChairs" runat="server" CssClass="L" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                سرويس
            </td>
            <td>
                <asp:Label ID="mService" runat="server" CssClass="L" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                تاريخ
            </td>
            <td>
                <asp:Label ID="mDate" runat="server" CssClass="L" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                وضعيت سرويس
            </td>
            <td>
                <asp:Label ID="mClosed" runat="server" CssClass="L" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                کسر از حساب
            </td>
            <td>
                <asp:RadioButton ID="mFromFund" runat="server" Text='صندوق' Checked="True" GroupName="Acc" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:RadioButton ID="mFromSale" runat="server" Text='دفتر فروش' GroupName="Acc" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: left" colspan="3">
                <asp:Button runat="server" ID="doSubtract" Text="با کسر" CssClass="CB" Width="80px"
                    OnClick="doSubtract_Click" />
                &nbsp;
                <asp:Button runat="server" ID="doPayback" Text="بي کسر" CssClass="CB" Width="80px"
                    OnClick="doPayback_Click" />&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx'
                    runat="server" ID="doReturn">برگشت</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="mMsg" runat="server" CssClass="BErr"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
