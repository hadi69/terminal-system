﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;

public partial class Exclusives : System.Web.UI.Page
{
    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dt1 = DateTime.Now;
        msg = DateTime.Now.ToString();
        mMsg.Text = "";
        if (!IsPostBack)
        {
            sDateStart.SelectedDate = DateTime.Now;
            sDateEnd.SelectedDate = DateTime.Now;
            BindInitData();
            BindData();
        }
        StringBuilder sb = new StringBuilder();
        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function $d(id){$get('" + mIDToDelete.ClientID + "').value=id;$find('puExBehaviorID').show();return false;}");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        Page.RegisterClientScriptBlock("Ask_Del", sb.ToString());
        msg += "<br> PageLoad: " + (DateTime.Now - dt1).ToString();

    }
    private void BindInitData()
    {
        {
            List<Path> all = Helper.Instance.GetPaths();
            Path none = new Path();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sPathID.DataValueField = "ID";
            sPathID.DataTextField = "Title";
            sPathID.DataSource = all;
            sPathID.DataBind();
        }
        {
            List<Car> all = Helper.Instance.GetCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarID.DataValueField = "ID";
            sCarID.DataTextField = "Title";
            sCarID.DataSource = all;
            sCarID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            CarType none = new CarType();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarTypeID.DataValueField = "ID";
            sCarTypeID.DataTextField = "Title";
            sCarTypeID.DataSource = all;
            sCarTypeID.DataBind();
        }
        {
            List<Driver> all = Helper.Instance.GetDrivers();
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDriverID1.DataValueField = "ID";
            sDriverID1.DataTextField = "Title";
            sDriverID1.DataSource = all;
            sDriverID1.DataBind();
        }
        sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Exclusives.sDateStart"]), DateTime.Now);
        sDateEnd.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Exclusives.sDateEnd"]), DateTime.Now);
    }
    private List<Exclusive> GetExclusives()
    {
        var all = from c in Helper.Instance.DB.Exclusives
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;// Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
            all = all.Where(c => c.DepartureDate >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            all = all.Where(c => c.DepartureDate <= toDate);
        if (sPathID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sPathID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.PathID == sID);
        }
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.DriverID1 == sID);
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.Car.CarTypeID == sID);
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.CarID == sID);
        }

        if (sNo.Text.Trim().Length > 0)
            all = all.Where(c => c.No.ToString().Contains(sNo.Text.Trim()));

        if (sSeries.Text.Trim().Length > 0)
            all = all.Where(c => c.Series.Contains(sSeries.Text.Trim()));

        if (sPath2.Text.Trim().Length > 0)
            all = all.Where(c => c.Path2.Contains(sPath2.Text.Trim()));

        if (mOffilne.Checked != mHasCode.Checked)
        {
            if (mHasCode.Checked)
                all = all.Where(c => c.TrackCode != null && c.TrackCode.Length > 0);
            if (mOffilne.Checked)
                all = all.Where(c => c.TrackCode == null || c.TrackCode.Length == 0);
        }
        all = all.OrderBy(c => c.DepartureDate);
        return all.ToList();
    }
    private void BindData()
    {
        Session["Exclusives.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["Exclusives.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");
        list.DataSource = GetExclusives();
        list.DataBind();
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Exclusive info = e.Row.DataItem as Exclusive;
            if (info != null)
            {
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(info.DepartureDate, "");
                e.Row.Cells[4].Text = info.Path.Title;
                e.Row.Cells[5].Text = info.Car.Title;
                e.Row.Cells[6].Text = info.Driver.Title;

               // JsTools.HandleDeleteButton(e);
            }
        }
    }
    //protected void list_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (list.SelectedIndex < 0)
    //        return;
    //    EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
    //    if (Null.NullInteger == EditID)
    //        return;
    //    Trip info = Helper.Instance.GetTrip(EditID);
    //    if (info == null)
    //        return;

    //    mServiceID.Enabled = false;
    //    Tool.SetSelected(mServiceID, info.ServiceID);

    //    {
    //        List<Car> all = Helper.Instance.GetCars(info.Service.CarTypeID);
    //        Car none = new Car();
    //        none.ID = Null.NullInteger;
    //        all.Insert(0, none);

    //        mCarID.DataValueField = "ID";
    //        mCarID.DataTextField = "Title";
    //        mCarID.DataSource = all;
    //        mCarID.DataBind();
    //    }

    //    if (info.CarID.HasValue)
    //        Tool.SetSelected(mCarID, info.CarID.Value);
    //    else
    //        Tool.SetSelected(mCarID, Null.NullInteger);
    //    if (info.DriverID1.HasValue)
    //        Tool.SetSelected(mDriverID1, info.DriverID1.Value);
    //    else
    //        Tool.SetSelected(mDriverID1, Null.NullInteger);
    //    mDate.Text = Tool.ToPersianDate(info.Date, "");

    //    puEx.Show();
    //}

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        ToDeleteID = id;
        if (Null.NullInteger == id)
            return;
        puEx.Show();
        //if (Helper.Instance.DeleteExclusive(id))
        //{
        //    list.SelectedIndex = -1;
        //    BindData();
        //}
        //else
        //{
        //    mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        //}
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == ToDeleteID)
            return;

        Exclusive mExclusive = Helper.Instance.GetExclusive(ToDeleteID);
        if (mExclusive == null)
            return;
        InValid v = new InValid();
        v.Date = mExclusive.DepartureDate;
        v.Ticketless = 0;
        v.TicketlessPrice = 0;
        v.TicketlessName = "";
        v.Stamp = 0;
        v.Comission = mExclusive.Commission;
        v.BodyInsurance = mExclusive.BodyInsurance;
        v.ExtraCost = mExclusive.ExtraPrice.HasValue ? mExclusive.ExtraPrice.Value : 0;
        v.Closed = true;
        v.Price = mExclusive.Price;
        v.Toll = mExclusive.Toll;
        v.Insurance = mExclusive.Insurance;
        v.Reception = 0;
        v.TotalPrice = mExclusive.TotalRent;
        //v.Comission2 = mExclusive.Comission2;
        //v.OtherDeficits = mExclusive.OtherDeficits;
        //v.SumDeficits = mExclusive.SumDeficits;
        //v.AutoShare = mExclusive.AutoShare;
        //v.Toll2 = mExclusive.Toll2;
        //v.Locked = mExclusive.Locked;
        //v.Others = mExclusive.Others;
        v.TripID = mExclusive.ID;
        v.No = mExclusive.No; ;
        v.Series = mExclusive.Series;
        //v.Series = string.Format("{0},{1},{2}"
        //                  , Helper.Instance.GetSettingValue("SeriesNoD1"), Helper.Instance.GetSettingValue("SeriesNoD2")
        //                  , Helper.Instance.GetSettingValue("SeriesNoD3"));
        v.CarID = mExclusive.CarID;
        v.DriverID1 = mExclusive.DriverID1;
        v.DriverID2 = mExclusive.DriverID2;
        v.DriverID3 = mExclusive.DriverID3;
        v.ServiceID = mExclusive.PathID;
        v.Exclusive = true;
        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        if (!Helper.Instance.Update())
        {
            mError.Text = "اشکال در ابطال : " + Helper.Instance.LastException.Message;
            return;
        }
        mMsg.Text = "شماره صورت وضعیت باطل شد - ";

        if (Helper.Instance.DeleteExclusive(ToDeleteID))
        {
            list.SelectedIndex = -1;
            BindData();
            mMsg.Text += "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
        ToDeleteID = Null.NullInteger;
    }
    protected void puNo_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == ToDeleteID)
            return;
        if (Helper.Instance.DeleteExclusive(ToDeleteID))
        {
            list.SelectedIndex = -1;
            BindData();
            mMsg.Text = "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
        ToDeleteID = Null.NullInteger;
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        list.PageIndex = 0;
        BindData();
    }
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected void doNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditExclusive.aspx");
    }
    int ToDeleteID
    {
        get
        {
            return Tool.GetInt(mIDToDelete.Value, Null.NullInteger);
            return Tool.GetInt(ViewState["ToDeleteID"], Null.NullInteger);
        }
        set
        {
            mIDToDelete.Value = value.ToString();
            ViewState["ToDeleteID"] = value;
        }
    }
}
