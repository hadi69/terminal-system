﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Cartable2.aspx.cs" Inherits="Reports_Cartable2" Title="Untitled Page" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        <!--
                        سرويس-->
                    </td>
                    <td>
                        <asp:DropDownList ID="sServiceID" runat="server" CssClass="DD" Width="250px" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        سری صورت
                    </td>
                    <td>
                        <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                        <asp:Button ID="doWord" runat="server" CssClass="CB" OnClick="doWord_Click" Text="Word"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <table width="95%">
                <tr>
                    <td align="right">
                        <asp:Button runat="server" ID="Button1" Text="چاپ گزارش ریز فروش" CssClass="CB" OnClick="doPrint_Click" />
                        <asp:HyperLink runat="server" ID="doPrint" Text="چاپ گزارش ریز فروش" Target="_blank"
                            Visible="False" />
                    </td>
                    <td align="left"><asp:Button runat="server" ID="Button2" Text="چاپ گزارش ریز فروش روزانه" CssClass="CB" OnClick="doPrint2_Click" /></td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%" PageSize="1000">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ID" HeaderText="تاریخ" />
                    <asp:BoundField DataField="ID" HeaderText="ساعت" />
                    <asp:BoundField DataField="ID" HeaderText="مسير" />
                    <asp:BoundField DataField="ID" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="ID" HeaderText="کمسیون" />
                    <asp:BoundField DataField="ID" HeaderText="متفرقه" />
                    <asp:BoundField DataField="ID" HeaderText="حق تمبر صورت" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جمع کل کمیسیون:
                    </td>
                    <td>
                        <asp:Label ID="lblSumCommission" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="listE" runat="server" AutoGenerateColumns="False" OnRowDataBound="listE_RowDataBound"
                Style="margin-top: 0px" Width="95%" PageSize="1000">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ID" HeaderText="تاریخ" />
                    <asp:BoundField DataField="ID" HeaderText="ساعت" />
                    <asp:BoundField DataField="ID" HeaderText="مسير" />
                    <asp:BoundField DataField="ID" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="ID" HeaderText="کمسیون" />
                    <asp:BoundField DataField="ID" HeaderText="متفرقه" />
                    <asp:BoundField DataField="ID" HeaderText="حق تمبر صورت" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">هيچ سرويس دربستی در بازه مورد نظر وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جمع کل کمیسیون:
                    </td>
                    <td>
                        <asp:Label ID="lblSumCommissionE" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="doWord" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
