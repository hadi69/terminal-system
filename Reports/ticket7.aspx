﻿<%@ page language="C#" autoeventwireup="true" CodeFile="Ticket7.aspx.cs" inherits="Reports_ticket7" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <object id="OurActiveX" name='OurActiveX' classid="clsid:DE6CDCC5-F2F4-455f-B782-1D1F78EBDAF8"
        viewastext codebase="MyPrinter.cab">
    </object>

    <script language="javascript" type="text/javascript">
        function DoPrint()
        {
           try
	        {
                //alert('Set Printer ');
		        document.OurActiveX.InitPrinter(210, 70);
                //alert('done');
	        }
	        catch(Err)
	        {
		        alert(Err.description);
	        }
            print();
            window.close();
        }
    </script>
 <link href="../PrintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body onload="javascript:DoPrint()">
   <form id="form1" runat="server">
    &nbsp;<asp:Label Style="position: absolute; top: 145px; left: 168px;" ID="lblPassengersCountL"
        runat="server" class='printLeftlbl'   />
    <asp:Label Style="position: absolute; top: 0px; left: 26px;" ID="lblCarTypeL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 0px; left: 169px;" ID="lblNoL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 35px; left: 26px;" ID="lblFullNameL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 70px; left: 27px;" ID="lblChairsNoL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 100px; left: 26px;" ID="lblSaleDateL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 100px; left: 168px;" ID="lblDestCityL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 146px; left: 124px;" ID="lblDayL" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 145px; left: 27px;" ID="lblDateL" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 26px;" ID="lblDepartureTimeL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 167px;" ID="lblPriceL" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 0px; left: 332px;" ID="lblCarType" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 0px; left: 499px;" ID="lblPlate" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 0px; left: 643px;" ID="lblNo" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 35px; left: 330px;" ID="lblSaleDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 35px; left: 499px;" ID="lblFullName" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 75px; left: 331px;" 
        ID="lblSaleTime" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 75px; left: 499px;" 
        ID="lblChairsNo" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 110px; left: 330px;" ID="lblPriceForOne"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 110px; left: 498px;" ID="lblSrcCity" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 110px; left: 696px;" ID="lblDay" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 110px; left: 603px;" ID="lblDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 145px; left: 332px;" ID="lblPassengersCount"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 145px; left: 498px; height: 11px;" 
        ID="lblDestCity" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 145px; left: 642px;" ID="lblDepartureTime"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 190px; left: 332px; height: 11px;" 
        ID="lblPrice" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 190px; left: 469px;" ID="lblPriceH" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 170px; left: 415px;" ID="lblUrl"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 170px; left: 515px;" ID="lblReception"
        runat="server" class='printLeftlbl' />
        <asp:Label Style="position: absolute; top: 190px; left: 603px;" ID="lblSaler" runat="server"
        class='printlbl' />
    </form>
</body>
</html>
