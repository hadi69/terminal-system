﻿<%@ Page Title="گزارش فنی" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportTechnical.aspx.cs" Inherits="Reports_ReportTechnical" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <script src="../js/jquery.min.js" type="text/javascript"></script>
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع
                    </td>
                    <td>
                        <asp:CheckBox ID="mBus" runat="server" CssClass="T" Text='اتوبوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mMini" runat="server" CssClass="T" Text='مینی بوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mSewari" runat="server" CssClass="T" Text='سواری' Checked="true" />
                    </td>
                    <td>
                       وضعیت صورت
                    </td>
                    <td>
                        <asp:DropDownList ID="sTripStatus" runat="server" CssClass="DD" Width="150px" >
                            <asp:ListItem Text="همه" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="ارسال نشده" Value="0"></asp:ListItem>
                            <asp:ListItem Text="درحال بررسی" Value="1"></asp:ListItem>
                            <asp:ListItem Text="تایید شده" Value="2"></asp:ListItem>
                            <asp:ListItem Text="رد شده" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        سرويس
                    </td>
                    <td>
                        <asp:DropDownList ID="sServiceID" runat="server" CssClass="DD" Width="250px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        راننده اول
                    </td>
                    <td>
                        <asp:DropDownList ID="sDriverID1" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="6">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <input type="hidden" runat="server" id="mIDToDelete" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                    <asp:BoundField DataField="ServiceType" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="SrcCity" HeaderText="مسير" />
                    <asp:BoundField DataField="CarTitle" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverTitle" HeaderText="راننده1" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                    <asp:BoundField DataField="NumTickets" HeaderText="ت بليط" />
                    <asp:BoundField DataField="TripStatus" HeaderText="وضعیت صورت" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        <%--$("#<%=list.ClientID%>").load(function () {
            $("#<%=list.ClientID%>").find('tr').each(function() {
                debugger;
                var tripstatus = $(this).find('td:nth-child(11)').text();
                if (tripstatus == "تایید شده") {
                    $(this).css('background-color', '#66ff66');
                }
            });
            $("#<%=doSearch.ClientID%>").click(function() {
                $("#<%=list.ClientID%>").find('tr').each(function() {
                    debugger;
                    var tripstatus = $(this).find('td:nth-child(11)').text();
                    if (tripstatus == "تایید شده") {
                        $(this).css('background-color', '#66ff66');
                    }
                });
            });
        });

        function setBackground() {
            $("#<%=list.ClientID%>").find('tr').each(function () {
                debugger;
                var tripstatus = $(this).find('td:nth-child(11)').text();
                if (tripstatus == "تایید شده") {
                    $(this).css('background-color', '#66ff66');
                }
            });
        }--%>
    </script>
</asp:Content>

