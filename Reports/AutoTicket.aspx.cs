﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_AutoTicket : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int id = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);

        IQueryable<TicketClass> tc = Helper.Instance.GetTicketClass(id);
        Ticket ticket = Helper.Instance.GetTicket(id);
        lblNo.Text = ticket.No;
        lblCompanyName.Text = Helper.Instance.GetSettingValue("CompanyName");
        lblFullName.Text = ticket.Fullname;
        lblDestCity.Text = Helper.Instance.GetCityTitle(ticket.CityID);
        lblSrcCity.Text = ticket.Trip.Service.Path.City.Title;
        lblDepartureTime.Text = ticket.Trip.Service.DepartureTime;
        lblDate.Text = Tool.ToShortPersianDate(ticket.Trip.Date, "");
        lblUserName.Text = Helper.Instance.GetUserFullName(ticket.SaleUserID);
        try
        {
            lblChairNo.Text = ticket.Chairs.Replace("m", "").Replace("f", "").Replace(";", "-").Trim('-');
        }
        catch { }
        lblPassengersCount.Text = ticket.GetNumTickets().ToString();
        lblTotalPrice.Text = ((ticket.Price - ticket.Discount) * ticket.GetNumTickets()).ToString();
        lblPrice.Text = ticket.Price.ToString();
        lblDiscount.Text = ticket.Discount.ToString();
        lblSaleDate.Text = Tool.ToShortPersianDate(ticket.SaleDate, "");

        //lblCar.Text = Helper.Instance.GetCarTitle(ticket.Trip.CarID);
    }
}