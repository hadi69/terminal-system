﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ticket1.aspx.cs" Inherits="Reports_Ticket1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <object id="OurActiveX" name='OurActiveX' classid="clsid:DE6CDCC5-F2F4-455f-B782-1D1F78EBDAF8"
        viewastext codebase="MyPrinter.cab">
    </object>

    <script language="javascript" type="text/javascript">
        function DoPrint() {
            try {
                //alert('Set Printer ');
                document.OurActiveX.InitPrinter(210, 54);
                //alert('done');
            }
            catch (Err) {
                alert(Err.description);
            }
            print();
            window.close();
        }
    </script>

    <link href="../PrintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0px;" onload="javascript:DoPrint()">
    <form id="form1" runat="server" style="width: 21cm; height: 7cm">
    &nbsp;
    <asp:Label Style="position: absolute; top: 10px; left: 360px;" ID="lblNo" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 45px; left: 320px; height: 14px" ID="lblFullName"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 80px; left: 320px; height: 14px" ID="lblDestCity"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 100px; left: 110px;" ID="lblDepartureTime"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 100px; left: 368px;" ID="lblDay" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 100px; left: 280px;" ID="lblDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 130px; left: 180px;" ID="lblChairsNo"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 130px; left: 410px;" ID="lblPassengersCount"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 265px;" ID="lblPrice" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 90px;" ID="lblSaleDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 10px; left: 600px; height: 11px;" ID="lblNoL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 30px; left: 621px; height: 14px" ID="lblFullNameL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 60px; left: 657px; height: 15px;" ID="lblDestCityL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 85px; left: 550px;" ID="lblDateL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 100px; left: 600px;" ID="lblDepartureTimeL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 115px; left: 600px;" ID="lblPassengersCountL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 135px; left: 600px;" ID="lblChairsNoL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 505px;" ID="lblPriceL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 625px;" ID="lblSaleDateL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 570px;" ID="lblSaleTime"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 415px;" ID="lblUrl" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 180px; left: 363px;" ID="lblSaler" runat="server"
        class='printlbl' />
        
    <asp:Label Style="position: absolute; top: 85px; left: 655px;" ID="lblDayL" runat="server"
        class='printLeftlbl' />
        
    </form>
</body>
</html>

