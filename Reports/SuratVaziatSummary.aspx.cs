﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;

public partial class SuratVaziatSummary : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            //BindData();
            doSearch_Click(null, null);
        }
    }
    private void BindInitData()
    {
        {
            List<string> all = (from h in Helper.Instance.DB.Trips
                                select h.Series).ToList();
            all.Insert(0, "");

            List<string> all2 = (from hh in Helper.Instance.DB.Exclusives
                                 select hh.Series).ToList();
            if (all2 != null)
                all.AddRange(all2);

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();
        }
        {
            List<Special> all = Helper.Instance.GetSpecials();
            Special none = new Special();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sSpecial.DataValueField = "ID";
            sSpecial.DataTextField = "Title";
            sSpecial.DataSource = all;
            sSpecial.DataBind();
        }
        {
            List<Service> all = Helper.Instance.GetFixedServices();
            Service none = new Service();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sServiceID.DataValueField = "ID";
            sServiceID.DataTextField = "Title";
            sServiceID.DataSource = all;
            sServiceID.DataBind();
        }
        {
            List<Fund> allf = Helper.Instance.GetFunds();
            sFundID.DataValueField = "ID";
            sFundID.DataTextField = "Title";
            sFundID.DataSource = allf;

            Fund nonef = new Fund();
            nonef.ID = Null.NullInteger;
            allf.Insert(0, nonef);

            sFundID.DataBind();
        }
        {
            List<Car> all = Helper.Instance.GetEnableCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarID.DataValueField = "ID";
            sCarID.DataTextField = "Title";
            sCarID.DataSource = all;
            sCarID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            CarType none = new CarType();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarTypeID.DataValueField = "ID";
            sCarTypeID.DataTextField = "Title";
            sCarTypeID.DataSource = all;
            sCarTypeID.DataBind();
        }
        {
            List<Driver> all = Helper.Instance.GetEnableDrivers();
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDriverID1.DataValueField = "ID";
            sDriverID1.DataTextField = "Title";
            sDriverID1.DataSource = all;
            sDriverID1.DataBind();
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            City none = new City();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDestID.DataValueField = "ID";
            sDestID.DataTextField = "Title";
            sDestID.DataSource = all;
            sDestID.DataBind();
        }

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        if (Null.NullDate != Tool.GetDate(Session["SuratVaziatSummary.DateStart"]))
            sDateStart.SelectedDate = Tool.GetDate(Session["SuratVaziatSummary.DateStart"]);
        if (Null.NullDate != Tool.GetDate(Session["SuratVaziatSummary.DateEnd"]))
            sDateEnd.SelectedDate = Tool.GetDate(Session["SuratVaziatSummary.DateEnd"]);
        try { sSpecial.SelectedIndex = (int)Session["SuratVaziatSummary.Special"]; }
        catch { };
        try { sServiceID.SelectedIndex = (int)Session["SuratVaziatSummary.ServiceID"]; ; }
        catch { };
        try { sDriverID1.SelectedIndex = (int)Session["SuratVaziatSummary.DriverID1"]; ; }
        catch { };
        try { sCarTypeID.SelectedIndex = (int)Session["SuratVaziatSummary.arTypeID"]; ; }
        catch { };
        try { sCarID.SelectedIndex = (int)Session["SuratVaziatSummary.CarID"]; }
        catch { };
        try { sDestID.SelectedIndex = (int)Session["SuratVaziatSummary.DestID"]; ; }
        catch { };
        try { sFundID.SelectedIndex = (int)Session["SuratVaziatSummary.FundID"]; ; }
        catch { };
        try { sFNo.Text = Session["SuratVaziatSummary.FNo"].ToString(); }
        catch { };
        try { sTNo.Text = Session["SuratVaziatSummary.TNo"].ToString(); }
        catch { };
        try { sSeries.SelectedValue = Session["SuratVaziatSummary.Series"].ToString(); }
        catch { };
    }
    private Dictionary<int, MyTrip> GetTripsHash()
    {
        int fn = (sFNo.Text.Trim().Length > 0) ? int.Parse(sFNo.Text) : 0;
        int tn = (sTNo.Text.Trim().Length > 0) ? int.Parse(sTNo.Text) : int.MaxValue;
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        string qry = string.Format(@"SELECT Trips.ID
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
	, Specials.Title AS SpecialTitle
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Specials ON Specials.ID=Services.SpecialID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
LEFT JOIN Cars ON Trips.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
WHERE Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59'
AND Trips.No >= {6} AND Trips.No <= {7}
"
           , startDate.Year, startDate.Month, startDate.Day
           , toDate.Year, toDate.Month, toDate.Day, fn, tn);

        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            qry += " AND Trips.ServiceID=" + sID;
        }
        if (sSpecial.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sSpecial.SelectedValue, Null.NullInteger);
            qry += " AND Services.SpecialID=" + sID;
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            qry += " AND Trips.CarTypeID=" + sID;
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            qry += " AND Trips.CarID=" + sID;
        }
        if (sDestID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
            qry += " AND Paths.DestCityID=" + sID;
        }
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            qry += " AND (Trips.DriverID1=" + sID + " OR Trips.DriverID2=" + sID + ")";
        }

        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            qry += " AND Trips.Series LIKE N'%" + s + "%'";
        }
        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Services.ServiceType");
        Dictionary<int, MyTrip> res = new Dictionary<int, MyTrip>();
        if (dt != null && dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MyTrip trip = new MyTrip();
                int id = Tool.GetInt(dt.Rows[i]["ID"], -1);
                if (res.ContainsKey(id))
                    continue;
                trip.DepartureTime = Tool.GetString(dt.Rows[i]["DepartureTime2"], "");
                trip.SrcCity = Tool.GetString(dt.Rows[i]["SrcCity"], "");
                trip.DestCity = Tool.GetString(dt.Rows[i]["DestCity"], "");
                trip.SpecialTitle = Tool.GetString(dt.Rows[i]["SpecialTitle"], "");
                trip.CarTitle = Tool.GetString(dt.Rows[i]["CarTitle"], "");
                trip.DriverTitle = Tool.GetString(dt.Rows[i]["DriverTitle"], "");
                trip.NumTickets = Tool.GetInt(dt.Rows[i]["NumTickets"], 0);
                res.Add(id, trip);
            }
        }
        return res;
    }
    private class MyTrip
    {
        public string DepartureTime { get; set; }
        public string SrcCity { get; set; }
        public string DestCity { get; set; }
        public string SpecialTitle { get; set; }
        public string CarTitle { get; set; }
        public string DriverTitle { get; set; }
        public int NumTickets { get; set; }
    }
    private List<Trip> GetTrips()
    {
        int fn = (sFNo.Text.Trim().Length > 0) ? int.Parse(sFNo.Text) : 0;
        int tn = (sTNo.Text.Trim().Length > 0) ? int.Parse(sTNo.Text) : int.MaxValue;
        // ---------------------------------
        var all = from c in Helper.Instance.DB.Trips
                  where ((c.Closed == true) && (c.No >= fn) && (c.No <= tn))
                  orderby c.Date, c.DepartureTime, c.No, c.Service.ServiceType
                  select c;

        List<Trip> trips = all.ToList();
        if (trips == null || trips.Count == 0)
            return trips;


        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].ServiceID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (sSpecial.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sSpecial.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].Service.SpecialID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarTypeID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].CarID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sDestID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].Service.Path.DestCityID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }


        if (sFundID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (!trips[i].Tickets.Any(d => d.FundID == sID))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        //if (sFundID.SelectedIndex > 0)
        //{
        //    int fID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.Tickets.Any(d => d.FundID == fID));
        //}

        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].DriverID1 != sID && trips[i].DriverID2 != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }


        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            for (int i = 0; i < trips.Count; i++)
                if (!trips[i].Series.Contains(s))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate != Null.NullDate || toDate != Null.NullDate)
        {
            startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
                : DateTime.Now.AddYears(-10);
            toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
                : DateTime.Now.AddYears(10);
            for (int i = 0; i < trips.Count; i++)
            {
                if ((trips[i].Date < startDate || trips[i].Date > toDate))
                {
                    trips.RemoveAt(i);
                    i--;
                }
            }
        }

        return trips;
        // ---------------------------------

        //all = all.OrderBy(c => c.Date);
        //return all.ToList();
    }
    private List<Exclusive> GetExclusives()
    {
        var all = from c in Helper.Instance.DB.Exclusives
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;// Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
            all = all.Where(c => c.DepartureDate >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            all = all.Where(c => c.DepartureDate <= toDate);
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.DriverID1 == sID || c.DriverID2 == sID);
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.Car.CarTypeID == sID);
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.CarID == sID);
        }

        if (sDestID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.Path.DestCityID == sID);
        }


        if (sFNo.Text.Trim().Length > 0)
            try { all = all.Where(c => c.No >= int.Parse(sFNo.Text)); }
            catch { };

        if (sTNo.Text.Trim().Length > 0)
            try { all = all.Where(c => c.No <= int.Parse(sTNo.Text)); }
            catch { };


        if (sSeries.Text.Trim().Length > 0)
            all = all.Where(c => c.Series.Contains(sSeries.Text.Trim()));

        all = all.OrderBy(c => c.DepartureDate);
        return all.ToList();
    }
    private List<InValid> GetInvalids()
    {
        // ---------------------------------
        var all = from c in Helper.Instance.DB.InValids
                  orderby c.Date, c.No
                  select c;

        List<InValid> trips = all.ToList();
        if (trips == null || trips.Count == 0)
            return trips;


        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].ServiceID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (sSpecial.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sSpecial.SelectedValue, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
            {
                Service sp = Helper.Instance.GetService(trips[i].ServiceID);
                if (sp == null || sp.SpecialID != sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
            }
        }
        //if (sCarTypeID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
        //    for (int i = 0; i < trips.Count; i++)
        //        if (trips[i].CarTypeID != sID)
        //        {
        //            trips.RemoveAt(i);
        //            i--;
        //        }
        //}
        //if (sCarID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
        //    for (int i = 0; i < trips.Count; i++)
        //        if (trips[i].CarID != sID)
        //        {
        //            trips.RemoveAt(i);
        //            i--;
        //        }
        //}
        //if (sDestID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
        //    for (int i = 0; i < trips.Count; i++)
        //        if (trips[i].Service.Path.City.ID != sID)
        //        {
        //            trips.RemoveAt(i);
        //            i--;
        //        }
        //}


        //if (sFundID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);
        //    for (int i = 0; i < trips.Count; i++)
        //        if (!trips[i].Tickets.Any(d => d.FundID == sID))
        //        {
        //            trips.RemoveAt(i);
        //            i--;
        //        }
        //}


        //if (sDriverID1.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
        //    for (int i = 0; i < trips.Count; i++)
        //        if (trips[i].DriverID1 != sID)
        //        {
        //            trips.RemoveAt(i);
        //            i--;
        //        }
        //}

        if (sFNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sFNo.Text, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].No < sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }
        if (sTNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sTNo.Text, Null.NullInteger);
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].No > sID)
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            for (int i = 0; i < trips.Count; i++)
                if (!trips[i].Series.Contains(s))
                {
                    trips.RemoveAt(i);
                    i--;
                }
        }

        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate != Null.NullDate || toDate != Null.NullDate)
        {
            startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
                : DateTime.Now.AddYears(-10);
            toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
                : DateTime.Now.AddYears(10);
            for (int i = 0; i < trips.Count; i++)
            {
                if ((trips[i].Date < startDate || trips[i].Date > toDate))
                {
                    trips.RemoveAt(i);
                    i--;
                }
            }
        }

        return trips;
        // ---------------------------------

        //all = all.OrderBy(c => c.Date);
        //return all.ToList();
    }
    Dictionary<int, MyTrip> mDict = null;
    private void BindData()
    {
        #region printBimeQry.Value
        {
            DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
            string fDate = "";
            if (fromDate != new DateTime())
                fDate = fromDate.ToShortDateString();
            string tDate = "";
            DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
            if (toDate != new DateTime())
                tDate = toDate.ToShortDateString();
            int specialID = 0;
            if (sSpecial.SelectedIndex > 0)
                specialID = Tool.GetInt(sSpecial.SelectedValue, 0);
            int serviceID = 0;
            if (sServiceID.SelectedIndex > 0)
                serviceID = Tool.GetInt(sServiceID.SelectedValue, 0);
            int driverID1 = 0;
            if (sDriverID1.SelectedIndex > 0)
                driverID1 = Tool.GetInt(sDriverID1.SelectedValue, 0);
            int carTypeID = 0;
            if (sCarTypeID.SelectedIndex > 0)
                carTypeID = Tool.GetInt(sCarTypeID.SelectedValue, 0);
            int carID = 0;
            if (sCarID.SelectedIndex > 0)
                carID = Tool.GetInt(sCarID.SelectedValue, 0);
            int destID = 0;
            if (sDestID.SelectedIndex > 0)
                destID = Tool.GetInt(sDestID.SelectedValue, 0);
            int fundID = 0;
            if (sFundID.SelectedIndex > 0)
                fundID = Tool.GetInt(sFundID.SelectedValue, 0);
            int fno = 0;
            if (sFNo.Text.Trim().Length > 0)
                fno = Tool.GetInt(sFNo.Text, 0);
            int tno = 0;
            if (sTNo.Text.Trim().Length > 0)
                tno = Tool.GetInt(sTNo.Text, 0);
            string series = "";
            if (sSeries.Text.Trim().Length > 0)
                series = Tool.ParseSeries(sSeries.Text);

            printBimeQry.Value = string.Format("fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}",
                fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, "0");
        }
        #endregion

        Session["SuratVaziatSummary.DateStart"] = sDateStart.SelectedDate;
        Session["SuratVaziatSummary.DateEnd"] = sDateEnd.SelectedDate;
        Session["SuratVaziatSummary.Special"] = sSpecial.SelectedIndex;
        Session["SuratVaziatSummary.ServiceID"] = sServiceID.SelectedIndex;
        Session["SuratVaziatSummary.DriverID1"] = sDriverID1.SelectedIndex;
        Session["SuratVaziatSummary.arTypeID"] = sCarTypeID.SelectedIndex;
        Session["SuratVaziatSummary.CarID"] = sCarID.SelectedIndex;
        Session["SuratVaziatSummary.DestID"] = sDestID.SelectedIndex;
        Session["SuratVaziatSummary.FundID"] = sFundID.SelectedIndex;
        Session["SuratVaziatSummary.FNo"] = sFNo.Text;
        Session["SuratVaziatSummary.TNo"] = sTNo.Text;
        Session["SuratVaziatSummary.Series"] = sSeries.Text;

        mDict = GetTripsHash();
        List<Trip> trips = GetTrips();
        list.DataSource = trips;
        list.DataBind();

        List<Exclusive> exclusives = GetExclusives();
        listE.DataSource = exclusives;
        listE.DataBind();
        doCreateXML.Enabled = doPrintInsurance.Enabled = true;
        printBimeQryDisabled.Value = "";
        List<int> invalidsNos = new List<int>();
        List<int> invalidsExNos = new List<int>();
        if ((null != trips && trips.Count > 1) || (null != exclusives && exclusives.Count > 1))
        {
            List<InValid> invalids = GetInvalids();
            for (int i = 0; i < invalids.Count; i++)
                if (invalids[i].Exclusive.HasValue && invalids[i].Exclusive.Value)
                    invalidsExNos.Add(invalids[i].No);
                else
                    invalidsNos.Add(invalids[i].No);
        }
        if (null != trips && trips.Count > 1)
        {
            List<int> tripsNo = new List<int>();
            List<int> lasttripNo=new List<int>();
            string SeriesStartFrom=Helper.Instance.GetSettingValue("SeriesStartFrom");
            int i=0;
            for (; i < trips.Count; i++)
            {
                if (trips[i].No.ToString() != SeriesStartFrom)
                    tripsNo.Add(trips[i].No);
                else
                    break;
            }
            for (int j = i; j < trips.Count; j++)
            {
                lasttripNo.Add(trips[j].No);
            }
            tripsNo.Sort();
            lasttripNo.Sort();
            string tripNofehler = "";
            int count = 0;
            if (tripsNo.Count > 0)
            {
                int index=0;
                for (int k = tripsNo[0]; k < tripsNo[tripsNo.Count - 1]; k++)
                {
                    if(tripsNo.Contains(k))
                        index = tripsNo.IndexOf(k);
                    if (!tripsNo.Contains(k))
                    {

                        if (tripsNo[index + 1] - tripsNo[index] < 100)
                        {
                            if (!invalidsNos.Contains(k))
                            {
                                if (k%25 != 0)
                                {
                                    tripNofehler += k.ToString() + ", ";
                                    count++;
                                }
                                if (count > 20)
                                    break;
                            }
                        }
                        else
                        {
                            k = tripsNo[index + 1] - 1;
                        }
                    }
                }
            }
            try
            {
                for (int k = lasttripNo[0]; k < lasttripNo[lasttripNo.Count - 1]; k++)
                    if (!lasttripNo.Contains(k))
                        if (!invalidsNos.Contains(k))
                        {
                            tripNofehler += k.ToString() + ", ";
                            count++;
                            if (count > 0)
                                break;
                        }
            }
            catch { }
            if (tripNofehler.Length > 0)
            {
                mMsg.Text = "مسئولیت این شماره صورت ها با کاربر سیستم می باشد لطفا تکلیف آن ها را مشخص نمایید:<br>";
                mMsg.Text += tripNofehler.Trim(' ', ',');
                mMsg.Text += "<br />";
                doCreateXML.Enabled = doPrintInsurance.Enabled = true;
                printBimeQryDisabled.Value = "1";
            }
        }
        if (null != exclusives && exclusives.Count > 1)
        {
            List<int> tripsNo = new List<int>();
            List<int> lasttripNo = new List<int>();
            string SeriesStartFrom = Helper.Instance.GetSettingValue("SeriesStartDFrom");
            int i = 0;
            for (; i < exclusives.Count; i++)
            {
                if (exclusives[i].No.ToString() != SeriesStartFrom)
                    tripsNo.Add(exclusives[i].No);
                else
                    break;
            }
            for (int j = i; j < exclusives.Count; j++)
            {
                lasttripNo.Add(exclusives[j].No);
            }
            tripsNo.Sort();
            lasttripNo.Sort();
            string tripNofehler = "";
            int count = 0;
            int index = 0;
            for (int k = tripsNo[0]; k < tripsNo[tripsNo.Count - 1]; k++)
            {
                if (tripsNo.Contains(k))
                    index = tripsNo.IndexOf(k);
                if (!tripsNo.Contains(k))
                {
                    if (tripsNo[index + 1] - tripsNo[index] < 100)
                    {
                        if (!invalidsExNos.Contains(k))
                        {
                            if (k%25 != 0)
                            {
                                tripNofehler += k.ToString() + ", ";
                                count++;
                            }
                            if (count > 20)
                                break;
                        }
                    }
                    else
                    {
                        k = tripsNo[index + 1] - 1;
                    }
                }
            }
            try
            {
                for (int k = lasttripNo[0]; k < lasttripNo[lasttripNo.Count - 1]; k++)
                    if (!lasttripNo.Contains(k))
                        if (!invalidsNos.Contains(k))
                        {
                            tripNofehler += k.ToString() + ", ";
                            count++;
                            if (count > 0)
                                break;
                        }
            }
            catch { }

            if (tripNofehler.Length > 0)
            {
                mMsg.Text += "مسئولیت این شماره صورت ها با کاربر سیستم می باشد لطفا تکلیف آن ها را مشخص نمایید:<br>";
                mMsg.Text += tripNofehler.Trim(' ', ',');
                doCreateXML.Enabled = doPrintInsurance.Enabled = true;
                printBimeQryDisabled.Value = "1";
            }
        }
        sSeries_SelectedIndexChanged(null, null);
    }

    static System.Drawing.Color warnColor = System.Drawing.Color.FromArgb(255, 255, 32, 32);
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Trip info = e.Row.DataItem as Trip;
            if (info != null)
            {
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(info.Date, "");
                if (mDict != null && mDict.ContainsKey(info.ID))
                {
                    MyTrip trip = mDict[info.ID];
                    e.Row.Cells[3].Text = trip.DepartureTime;
                    e.Row.Cells[4].Text = trip.SrcCity + " -> " + trip.DestCity;
                    e.Row.Cells[7].Text = trip.SpecialTitle;
                    e.Row.Cells[8].Text = trip.CarTitle;
                    e.Row.Cells[9].Text = trip.DriverTitle;
                    e.Row.Cells[10].Text = trip.NumTickets.ToString();
                }
                else
                {
                    e.Row.Cells[3].Text = info.DepartureTime == null ? info.Service.DepartureTime : info.DepartureTime;
                    e.Row.Cells[4].Text = info.Service.Path == null ? "مسیر ناموجود" : info.Service.Path.Title;
                    e.Row.Cells[7].Text = info.Service.Special.Title;
                    e.Row.Cells[8].Text = Helper.Instance.GetCarTitle(info.CarID);
                    e.Row.Cells[9].Text = info.Driver != null ? info.Driver.Title : "";
                    e.Row.Cells[10].Text = Helper.Instance.GetNumTickets(info.ID).ToString();
                }
                if (mWarnDuplicates.Checked)
                {
                    try
                    {
                        var v = from t in Helper.Instance.DB.Trips
                                where t.ID != info.ID && t.Series == info.Series && t.No == info.No
                                select t;
                        if (v.Count() > 0)
                            e.Row.BackColor = warnColor;
                    }
                    catch { }
                }
                //JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void showToday_Click(object sender, EventArgs e)
    {
        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;
        doSearch_Click(null, null);
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
        if (!doPrintSumToll.Enabled && (list.Rows.Count > 0 || listE.Rows.Count > 0))
        {
            doPrintSumToll.Enabled = true;
            doPrintToll.Enabled = true;
            doPrint.Enabled = true;
            doPrintInsurance.Enabled = true;
            doPrintAutoShare.Enabled = true;
        }
        if (sDriverID1.SelectedIndex > 0 && doPrintSumToll.Enabled)
            doPrintDriver.Enabled = true;
        else
            doPrintDriver.Enabled = false;

        if (sCarID.SelectedIndex > 0 && doPrintSumToll.Enabled)
            doPrintAutoCar.Enabled = true;
        else
            doPrintAutoCar.Enabled = false;

        //SDateStart = sDateStart.SelectedDate;
        //SDateEnd = sDateEnd.SelectedDate;

    }
    protected void doRefineNew_Click(object sender, EventArgs e)
    {
        List<Trip> trips = GetTrips();
        List<Exclusive> exclusives = GetExclusives();
        if ((trips == null || trips.Count == 0) && (exclusives == null || exclusives.Count == 0))
            return;
        int no = Tool.GetInt(mNewNo.Text, Null.NullInteger);
        bool updateNo = no != Null.NullInteger;
        //if (no == Null.NullInteger)
        //{
        //    mMsg.Text = "شماره صورت شروع وارد شده اشتباه است";
        //    return;
        //}
        if (mSeriesNo1.Text.Trim().Length == 0
           || mSeriesNo2.Text.Trim().Length == 0 || mSeriesNo3.Text.Trim().Length == 0)
        {
            mMsg.Text = "سری صورت وارد شده اشتباه است";
            return;
        }
        bool isBus = false, isMini = false, isExclusive = false;
        if (mSeriesNo4.Text.Trim().Length == 0)
        {
            string ser = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
            if ((from h in Helper.Instance.DB.Trips
                 where h.Series == ser
                 select h.Series).Count() > 0)
                isMini = true;
            else
                if ((from hh in Helper.Instance.DB.Exclusives
                     where hh.Series == ser
                     select hh.Series).Count() > 0)
                    isExclusive = true;
        }
        else
            isBus = true;
        if (isBus)
            mMsg.Text = "سری صورت برای سرویسهای عادی (اتوبوس) است<br>";
        if (isMini)
            mMsg.Text = "سری صورت برای سرویسهای عادی (مینی بوس و سواری) است<br>";
        if (isExclusive)
            mMsg.Text = "سری صورت برای سرویسهای دربستی است<br>";
        if (isBus)
        {
            #region Bus
            string series = string.Format("{0},{1},{2},{3}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text, mSeriesNo4.Text);
            for (int i = 0; i < trips.Count; i++)
            {
                if (updateNo && mKeepInvalids.Checked)
                    do
                    {
                        try
                        {
                            var all = from c in Helper.Instance.DB.InValids
                                      where c.Series == series && c.No == no
                                      select c;
                            if (all.Count() > 0)
                                no++;
                            else
                                break;
                        }
                        catch (Exception ex)
                        {
                            break;
                        }
                    }
                    while (true);
                trips[i].Series = series;
                if (updateNo)
                {
                    trips[i].No = no;
                    no++;
                }
            }
            try
            {
                if (Helper.Instance.Update())
                    mMsg.Text += string.Format("بروزرسانی {0} سرویس عادی (اتوبوس) با موفقیت انجام شد", trips.Count);
                else
                    mMsg.Text += "اشکال در بروز رسانی سرویسهای عادی (اتوبوس) : " + Helper.Instance.LastException.Message;

            }
            catch (Exception ex)
            {
                mMsg.Text += "اشکال در بروز رسانی سرویسهای عادی (اتوبوس) : " + Helper.Instance.LastException.Message;
            }
            #endregion
        }
        if (isMini)
        {
            #region Mini
            string series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
            for (int i = 0; i < trips.Count; i++)
            {
                if (updateNo && mKeepInvalids.Checked)
                    do
                    {
                        try
                        {
                            var all = from c in Helper.Instance.DB.InValids
                                      where c.Series == series && c.No == no
                                      select c;
                            if (all.Count() > 0)
                                no++;
                            else
                                break;
                        }
                        catch (Exception ex)
                        {
                            break;
                        }
                    }
                    while (true);
                trips[i].Series = series;
                if (updateNo)
                {
                    trips[i].No = no;
                    no++;
                }
            }
            try
            {
                if (Helper.Instance.Update())
                    mMsg.Text += string.Format("بروزرسانی {0} سرویس عادی (مینی بوس و سواری) با موفقیت انجام شد", trips.Count);
                else
                    mMsg.Text += "اشکال در بروز رسانی سرویسهای عادی (مینی بوس و سواری) : " + Helper.Instance.LastException.Message;

            }
            catch (Exception ex)
            {
                mMsg.Text += "اشکال در بروز رسانی سرویسهای عادی (مینی بوس و سواری) : " + Helper.Instance.LastException.Message;
            }
            #endregion
        }
        if (isExclusive)
        {
            #region
            string series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
            for (int i = 0; i < exclusives.Count; i++)
            {
                if (updateNo && mKeepInvalids.Checked)
                    do
                    {
                        try
                        {
                            var all = from c in Helper.Instance.DB.InValids
                                      where c.Series == series && c.No == no && c.Exclusive == true
                                      select c;
                            if (all.Count() > 0)
                                no++;
                            else
                                break;
                        }
                        catch
                        {
                            break;
                        }
                    }
                    while (true);
                exclusives[i].Series = series;
                if (updateNo)
                {
                    exclusives[i].No = no;
                    no++;
                }
            }
            try
            {
                if (Helper.Instance.Update())
                    mMsg.Text += string.Format("بروزرسانی {0} سرویس دربستی با موفقیت انجام شد", exclusives.Count);
                else
                    mMsg.Text += "اشکال در بروز رسانی سرویسهای دربستی : " + Helper.Instance.LastException.Message;

            }
            catch (Exception ex)
            {
                mMsg.Text += "اشکال در بروز رسانی سرویسهای دربستی : " + Helper.Instance.LastException.Message;
            }
            #endregion
        }
        BindData();

    }
    private void ReportGenerator(string repName, string repTitle, string fullName, string repParam)
    {
        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        string fDate = "";
        if (fromDate != new DateTime())
            fDate = fromDate.ToShortDateString();

        string tDate = "";
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            tDate = toDate.ToShortDateString();
        int specialID = 0;
        if (sSpecial.SelectedIndex > 0)
            specialID = Tool.GetInt(sSpecial.SelectedValue, 0);
        int serviceID = 0;
        if (sServiceID.SelectedIndex > 0)
            serviceID = Tool.GetInt(sServiceID.SelectedValue, 0);
        int driverID1 = 0;
        if (sDriverID1.SelectedIndex > 0)
            driverID1 = Tool.GetInt(sDriverID1.SelectedValue, 0);
        int carTypeID = 0;
        if (sCarTypeID.SelectedIndex > 0)
            carTypeID = Tool.GetInt(sCarTypeID.SelectedValue, 0);
        int carID = 0;
        if (sCarID.SelectedIndex > 0)
            carID = Tool.GetInt(sCarID.SelectedValue, 0);
        int destID = 0;
        if (sDestID.SelectedIndex > 0)
            destID = Tool.GetInt(sDestID.SelectedValue, 0);

        int fundID = 0;
        if (sFundID.SelectedIndex > 0)
            fundID = Tool.GetInt(sFundID.SelectedValue, 0);

        int fno = 0;
        if (sFNo.Text.Trim().Length > 0)
            fno = Tool.GetInt(sFNo.Text, 0);
        int tno = 0;
        if (sTNo.Text.Trim().Length > 0)
            tno = Tool.GetInt(sTNo.Text, 0);

        string series = "";
        if (sSeries.Text.Trim().Length > 0)
            series = sSeries.Text;

        Response.Redirect(
            string.Format("ReportNew.aspx?fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}&repname={13}&repTitle={14}&fullName={15}&repParam={16}",
            fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, "0", repName, repTitle, fullName, repParam), true);

    }
    protected void doPrint_Click(object sender, EventArgs e)
    {
        ReportGenerator("SuratVaziatSummary", "", "", "");
    }

    protected void doPrintToll_Click(object sender, EventArgs e)
    {
        ReportGenerator("Toll2", "", "", "");
    }

    protected void doPrintSumToll_Click(object sender, EventArgs e)
    {
        ReportGenerator("SumToll2", "", "", "");
    }
    protected void doPrintInsurance_Click(object sender, EventArgs e)
    {
        ReportGenerator("Insurance", "", Helper.Instance.GetSettingValue("CompanyName"), "");
    }
    protected void doPrintAutoCar_Click(object sender, EventArgs e)
    {

        if (sCarID.SelectedIndex > 0)
        {
            Car car = Helper.Instance.GetCar(int.Parse(sCarID.SelectedValue));
            string repParam = "از تاریخ:" + Tool.ToPersianDateRtl(sDateStart.SelectedDate, "?") + " تا تاریخ: " + Tool.ToPersianDateRtl(sDateEnd.SelectedDate, "?");
            ReportGenerator("AutoCar", "گزارش اتوکار", car.Plate, repParam);
        }


    }
    protected void doPrintDriver_Click(object sender, EventArgs e)
    {
        if (sDriverID1.SelectedIndex > 0)
        {
            Driver drvr = Helper.Instance.GetDriver(int.Parse(sDriverID1.SelectedValue));
            string repParam = "از تاریخ:" + Tool.ToPersianDateRtl(sDateStart.SelectedDate, "?") + " تا تاریخ: " + Tool.ToPersianDateRtl(sDateEnd.SelectedDate, "?");
            ReportGenerator("Driver", "گزارش راننده", drvr.Name + " " + drvr.Surname + " شماره گواهينامه " + drvr.LicenceNo.ToString(), repParam);
        }

    }
    protected void doPrintAutoShare_Click(object sender, EventArgs e)
    {
        string fundName = "همه صندوقها";

        if (sFundID.SelectedIndex > 0)
        {
            Fund fnd = Helper.Instance.GetFund(int.Parse(sFundID.SelectedValue));
            fundName = fnd.Title;
        }
        string repParam = "از تاریخ:" + Tool.ToPersianDateRtl(sDateStart.SelectedDate, "?") + " تا تاریخ: " + Tool.ToPersianDateRtl(sDateEnd.SelectedDate, "?");
        ReportGenerator("AutoShare", "گزارش پرداختيها", fundName, repParam);

    }

    protected void sDriverID1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sDriverID1.SelectedIndex > 0 && doPrintSumToll.Enabled)
            doPrintDriver.Enabled = true;
        else
            doPrintDriver.Enabled = false;

        BindData();
    }
    protected void sCarID_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sCarID.SelectedIndex > 0 && doPrintSumToll.Enabled)
            doPrintAutoCar.Enabled = true;
        else
            doPrintAutoCar.Enabled = false;
        BindData();
    }
    protected void listE_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Exclusive info = e.Row.DataItem as Exclusive;
            if (info != null)
            {
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + listE.PageIndex * listE.PageCount).ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(info.DepartureDate, "");
                e.Row.Cells[4].Text = info.Path.Title;
                e.Row.Cells[7].Text = info.Car.Title;
                e.Row.Cells[8].Text = info.Driver.Title;


            }
        }
    }
    protected void listE_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        listE.PageIndex = e.NewPageIndex;
        DataBind();
    }

    private bool CheckDuplicates(List<Trip> trips, List<Exclusive> exclusives, List<InValid> invalids)
    {
        List<string> hash = new List<string>();
        if (trips != null && trips.Count > 0)
        {
            for (int i = 0; i < trips.Count; i++)
                if (trips[i].Series != null)
                {
                    string key = string.Format("{0}_*_{1}", trips[i].Series, trips[i].No);
                    if (hash.Contains(key))
                    {
                        mError.Text += "خطا:<br />صفحه  اشکالات صورتهایی که دارای مشکل هستند را درست نماید";
                        return false;
                    }
                    hash.Add(key);
                }
        }
        if (exclusives != null && exclusives.Count > 0)
        {
            for (int i = 0; i < exclusives.Count; i++)
                if (exclusives[i].Series != null)
                {
                    string key = string.Format("{0}_*_{1}", exclusives[i].Series, exclusives[i].No);
                    if (hash.Contains(key))
                    {
                        mError.Text += "خطا:<br />صفحه  اشکالات صورتهایی که دارای مشکل هستند را درست نماید";
                        return false;
                    }
                    hash.Add(key);
                }
        }
        if (invalids != null && invalids.Count > 0)
        {
            for (int i = 0; i < invalids.Count; i++)
                if (invalids[i].Series != null)
                {
                    string key = string.Format("{0}_*_{1}", invalids[i].Series, invalids[i].No);
                    if (hash.Contains(key))
                    {
                        mError.Text += "خطا:<br />صفحه  اشکالات صورتهایی که دارای مشکل هستند را درست نماید";
                        return false;
                    }
                    hash.Add(key);
                }
        }
        return true;
    }

    protected void doWord_Click(object sender, EventArgs e)
    {
        List<GridView> grids = new List<GridView>();
        grids.Add(list);
        grids.Add(listE);
        ExportTool.ToWord(Response, grids, "");
    }
    protected void doCreateXML_Click(object sender, EventArgs e)
    {
        
        List<Trip> trips = GetTrips();
        List<Exclusive> exclusives = GetExclusives();
        List<InValid> invalids = GetInvalids();
        if ((trips == null || trips.Count == 0) && (exclusives == null || exclusives.Count == 0) && (invalids == null || invalids.Count == 0))
            return;
        if (!CheckDuplicates(trips, exclusives, invalids))
            return;
        XmlDocument xmlDoc = new XmlDocument();
        string xmlStr = "<?xml version='1.0' encoding='UTF-8'?><hc:XmlMosaferIntS xmlns:hc=\"www.tto.ir\" xsi:schemaLocation=\"www.tto.ir newmos.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"></hc:XmlMosaferIntS>";
        xmlDoc.LoadXml(xmlStr);

        #region Trips
        string companyCode = Helper.Instance.GetSettingValue("CompanyCode");
        if (trips != null)
            for (int i = 0; i < trips.Count; i++)
            {
                if (trips[i].Service.Path == null)
                    continue;
                int code = Tool.GetInt(trips[i].Service.Path.City.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                code = Tool.GetInt(trips[i].Service.Path.City1.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                int numTickets = Helper.Instance.GetNumTickets(trips[i].ID);
                if (numTickets < 1)
                    continue;
                if (trips[i].Service.Path.KMs == 0)
                    continue;
                int fare = Helper.Instance.GetPrice(trips[i].ID);
                if (fare < 1)
                    continue;

                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "XmlMosaferInt", "");
                // -----------------
                XmlNode n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Seri", "");
                string seri = GetSeries(trips[i].Series, false);
                seri = seri;
                n1.InnerText = seri;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "MosNo", "");
                n1.InnerText = trips[i].No.ToString();// Helper.Instance.GetNumTickets(trips[i].ID).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Company", "");
                n1.InnerText = companyCode;
                node.AppendChild(n1);

                Car car = Helper.Instance.GetCar(trips[i].CarID);
                string plateNo = "", plateSeries = "";
                GetPlateSeri(car, out plateNo, out plateSeries);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                n1.InnerText = Encode(plateNo);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                n1.InnerText = plateSeries;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                n1.InnerText = Helper.Instance.GetCityCodeForCar(trips[i].CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Fare", "");
                n1.InnerText = fare.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                n1.InnerText = numTickets.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Dist", "");
                n1.InnerText = trips[i].Service.Path.KMs.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "IssuDate", "");
                n1.InnerText = Tool.ToPersianDate(trips[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "CarType", "");
                n1.InnerText = Helper.Instance.GetCarTypeCode(trips[i].CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Stat", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "OriginCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(trips[i].Service.Path.SrcCityID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DestCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(trips[i].Service.Path.DestCityID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeAmnt", "");
                n1.InnerText = trips[i].Insurance.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "NavCode", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DriverCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetDriverSmartCard(trips[i].DriverID1);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "VehicleCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetCarSmartCard(trips[i].CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "RecType", "");
                n1.InnerText = "1"; //نوع صورت وضعيت است ( دربستی (2)-  معمولی(1))
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "ThvilDate", "");
                n1.InnerText = Tool.ToPersianDate(trips[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BusCapa", "");
                n1.InnerText = Helper.Instance.GetCarCapacity(trips[i].CarID).ToString();
                node.AppendChild(n1);

                xmlDoc.DocumentElement.AppendChild(node);
            }
        #endregion

        #region Exclusives
        if (exclusives != null)
            for (int i = 0; i < exclusives.Count; i++)
            {
                Exclusive exl = exclusives[i];
                if (exl.Path == null)
                    continue;
                //if (exl.NumPassangers < 1)
                //    continue;
                int code = Tool.GetInt(exl.Path.City.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                code = Tool.GetInt(exl.Path.City1.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                if (exl.NumPassangers < 1)
                    continue;
                if (exl.TotalRent < 1)
                    continue;
                if (exl.Path.KMs < 1)
                    continue;

                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "XmlMosaferInt", "");
                // -----------------
                XmlNode n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Seri", "");
                string seri = GetSeries(exl.Series, false);
                n1.InnerText = seri;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "MosNo", "");
                n1.InnerText = exl.No.ToString();// Helper.Instance.GetNumTickets(exl.ID).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Company", "");
                n1.InnerText = companyCode;// Helper.Instance.GetSettingValue("CompanyCode");
                node.AppendChild(n1);

                Car car = Helper.Instance.GetCar(exl.CarID);
                string plateNo = "", plateSeries = "";
                GetPlateSeri(car, out plateNo, out plateSeries);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                n1.InnerText = Encode(plateNo);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                n1.InnerText = plateSeries;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                n1.InnerText = Helper.Instance.GetCityCodeForCar(exl.CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Fare", "");
                n1.InnerText = exl.TotalRent.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                n1.InnerText = Math.Max(1, exl.NumPassangers).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Dist", "");
                n1.InnerText = exl.Path.KMs.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "IssuDate", "");
                n1.InnerText = Tool.ToPersianDate(exl.DepartureDate, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "CarType", "");
                n1.InnerText = Helper.Instance.GetCarTypeCode(exl.CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Stat", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "OriginCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(exl.Path.SrcCityID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DestCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(exl.Path.DestCityID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeAmnt", "");
                n1.InnerText = exl.Insurance.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "NavCode", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DriverCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetDriverSmartCard(exl.DriverID1);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "VehicleCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetCarSmartCard(exl.CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "RecType", "");
                n1.InnerText = "2"; //نوع صورت وضعيت است ( دربستی (2)-  معمولی(1))
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "ThvilDate", "");
                n1.InnerText = Tool.ToPersianDate(exl.DepartureDate, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BusCapa", "");
                n1.InnerText = Helper.Instance.GetCarCapacity(exl.CarID).ToString();
                node.AppendChild(n1);

                xmlDoc.DocumentElement.AppendChild(node);
            }
        #endregion

        #region Invalids
        if (invalids != null)
            for (int i = 0; i < invalids.Count; i++)
            {
                Service sv1 = null;
                Path path = null;
                if (invalids[i].Exclusive == true)
                    path = Helper.Instance.GetPath(invalids[i].ServiceID);
                else
                {
                    sv1 = Helper.Instance.GetService(invalids[i].ServiceID);
                    path = sv1 != null ? sv1.Path : null;
                }
                if (path != null)
                {
                    int code = Tool.GetInt(path.City.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                    code = Tool.GetInt(path.City1.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                }
                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "XmlMosaferInt", "");
                // -----------------
                XmlNode n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Seri", "");
                string seri = GetSeries(invalids[i].Series, false);
                n1.InnerText = seri;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "MosNo", "");
                n1.InnerText = invalids[i].No.ToString();// Helper.Instance.GetNumTickets(trips[i].ID).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Company", "");
                n1.InnerText = companyCode;// Helper.Instance.GetSettingValue("CompanyCode");
                node.AppendChild(n1);


                // -----------------
                Car car = Helper.Instance.GetCar(invalids[i].CarID);
                string plateNo = "", plateSeries = "";
                GetPlateSeri(car, out plateNo, out plateSeries);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                n1.InnerText = Encode(plateNo);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                n1.InnerText = plateSeries;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                n1.InnerText = Helper.Instance.GetCityCodeForCar(invalids[i].CarID);
                node.AppendChild(n1);



                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                //n1.InnerText = "";
                //node.AppendChild(n1);

                //// -----------------
                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                //n1.InnerText = "0";
                //node.AppendChild(n1);

                //// -----------------
                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                //n1.InnerText = "0";
                //node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Fare", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                int numTickets = 0;
                try { numTickets = Helper.Instance.GetNumTickets(invalids[i].TripID); }
                catch { }

                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                n1.InnerText = numTickets.ToString();
                node.AppendChild(n1);
                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                //n1.InnerText = "0";
                //node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Dist", "");
                n1.InnerText = path == null ? "0" : path.KMs.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "IssuDate", "");
                n1.InnerText = Tool.ToPersianDate(invalids[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "CarType", "");
                n1.InnerText = "8";// Helper.Instance.GetCarTypeCode(trips[i].CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Stat", "");
                n1.InnerText = "1";
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "OriginCity", "");
                n1.InnerText = path == null ? "0" : Helper.Instance.GetCityCode(path.SrcCityID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DestCity", "");
                n1.InnerText = path == null ? "0" : Helper.Instance.GetCityCode(path.DestCityID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeAmnt", "");
                n1.InnerText = "0";// trips[i].Insurance.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "NavCode", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DriverCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetDriverSmartCard(trips[i].DriverID1);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "VehicleCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetCarSmartCard(trips[i].CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "RecType", "");
                n1.InnerText = invalids[i].Exclusive == true ? "2" : "1"; //نوع صورت وضعيت است ( دربستی (2)-  معمولی(1))
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "ThvilDate", "");
                n1.InnerText = Tool.ToPersianDate(invalids[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BusCapa", "");
                n1.InnerText = "0";// Helper.Instance.GetCarCapacity(trips[i].CarID).ToString();
                node.AppendChild(n1);

                xmlDoc.DocumentElement.AppendChild(node);
            }
        #endregion
        Response.AppendHeader("Content-Disposition", String.Format("attachment;filename={0}.xml"
            , "MosaferPayaneh" + Tool.ToPersianDate(DateTime.Now, "").Replace("/", "")));
        Response.ContentType = "application/download";
        Response.Write(xmlDoc.OuterXml);
        Response.End();

        //            <Seri>86/1/w</Seri> 
        //<MosNo>987717</MosNo> 
        //    <Company>73025</Company> 
        //      <PlkNo>32n242</PlkNo> 
        //        <PlkSeri>51</PlkSeri> 
        //        <PlkLoc>99960000</PlkLoc> 
        //<Fare>390000</Fare> 
        //<PassNum>13</PassNum> 
        //<Dist>512</Dist> 
        //<IssuDate>13880801</IssuDate> 
        //<CarType>2</CarType> 
        //<Stat>0</Stat> 
        //<OriginCity>73310000</OriginCity> 
        //<DestCity>11320000</DestCity> 
        //<BimeDeptNo1>0</BimeDeptNo1> 
        //<BimeSeqNo1>0</BimeSeqNo1> 
        //<BimeDeptNo2>0</BimeDeptNo2> 
        //<BimeSeqNo2>0</BimeSeqNo2> 
        //<BimeAmnt>9800</BimeAmnt> 
        //<NavCode>0</NavCode> 
        //<DriverCardNo>0</DriverCardNo> 
        //<VehicleCardNo>0</VehicleCardNo> 
        //<RecType>1</RecType> 
        //<ThvilDate>13880801</ThvilDate> 
        //<BusCapa>32</BusCapa> 

    }
    string Encode(string str)
    {
        if (string.IsNullOrEmpty(str))
            return str;
        str = str.Replace('ی', 'ي');
        str = str.Replace('ي', 'ي');
        str = str.Replace('ك', 'ك');
        str = str.Replace('ک', 'ك');
        string ins = "آابپتثجچحخدذرزژسشصضطظعغفقكگلمنوهيئ";
        string outs = "ABCDEFGHabcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < ins.Length; i++)
            str = str.Replace(ins[i], outs[i]);
        return str;
    }
    string GetString(object obj, int len)
    {
        if (obj == null)
            return new string('0', len);
        string res = obj.ToString().PadLeft(len, '0');
        if (res.Length > len)
            res = res.Substring(0, len);
        return res;
    }
    protected void doDanaText_Click(object sender, EventArgs e)
    {
        List<Trip> trips = GetTrips();
        List<Exclusive> exclusives = GetExclusives();
        List<InValid> invalids = GetInvalids();
        if ((trips == null || trips.Count == 0) && (exclusives == null || exclusives.Count == 0) && (invalids == null || invalids.Count == 0))
            return;

        if (!CheckDuplicates(trips, exclusives, invalids))
            return;

        //trips = from tt in trips
        if (trips != null && trips.Count > 0)
            trips = (from c in trips
                     orderby c.Date, c.No, c.DepartureTime
                     select c).ToList();
        if (exclusives != null && exclusives.Count > 0)
            exclusives = (from ce in exclusives
                          orderby ce.DepartureDate, ce.No, ce.DepartureTime
                          select ce).ToList();
        if (invalids != null && invalids.Count > 0)
            invalids = (from ci in invalids
                        orderby ci.Date, ci.No
                        select ci).ToList();

        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        string cmpCode = GetString(Helper.Instance.GetSettingValue("CompanyCode"), 5);
        if (trips != null)
            for (int i = 0; i < trips.Count; i++)
            {
                try
                {
                    if (trips[i].Service.Path == null)
                        continue;
                    int code = Tool.GetInt(trips[i].Service.Path.City.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                    code = Tool.GetInt(trips[i].Service.Path.City1.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;

                    Car car = Helper.Instance.GetCar(trips[i].CarID);

                    string plateNo = "", plateSeries = "";
                    GetPlateSeri(car, out plateNo, out plateSeries);

                    builder.Append(cmpCode); // 1-5 for CompanyCode
                    builder.Append('0'); // 6 for e : " if (Alltrim(copy(StrTemp,6,1))='C') then { SetLength(SeryPelak,8); SeryPelak[6]:='C'; SeryPelak[7]:='?'; SeryPelak[8]:='?';}
                    builder.Append(new string(' ', 4)); // 7-11 empty
                    builder.Append(GetSeries(trips[i].Series, true)); // 12-18 for SerySoorat
                    builder.Append(GetString(trips[i].No, 6)); // 19-24 for NoSoorat
                    builder.Append(new string(' ', 8)); // 25-32 empty
                    builder.Append(GetString(plateSeries, 3)); // 33-35 for SeryPelak
                    builder.Append(GetString(plateNo, 6)); // 36-41 for SeryPelak
                    builder.Append(new string(' ', 7)); // 42-48 empty
                    builder.Append(GetString(Math.Max(1, Helper.Instance.GetNumTickets(trips[i].ID)), 2)); // 49-50 for TedadMosafer = g
                    builder.Append(GetString(Tool.ToPersianDate(trips[i].Date, "").Replace("/", ""), 6)); // 51-56 for TarikhHarkat = h
                    builder.Append(GetString(trips[i].CarType.Code, 1)); // 57 for KindOfCar = i
                    builder.Append(GetString(Helper.Instance.GetCityCode(trips[i].Service.Path.SrcCityID), 8)); // 58-65 for StartCity = j
                    builder.Append(GetString(Helper.Instance.GetCityCode(trips[i].Service.Path.DestCityID), 8)); // 66-73 for EndCity = k
                    builder.Append(new string(' ', 131)); // 74-202 empty
                    builder.Append(GetString(trips[i].BodyInsurance, 6)); // 203-208 for BadanehPardakhti = l
                    builder.Append(GetString(trips[i].Insurance, 6)); // 209-214 for SarneshinPardakhti = m
                    builder.Append(GetString(trips[i].DepartureTime == null ? trips[i].Service.DepartureTime : trips[i].DepartureTime, 5)); // 215-219 for Saat = n
                    builder.Append(new string(' ', 37)); // 220-256 empty

                    builder.Append("\r\n");
                }
                catch { }
            }
        if (exclusives != null)
            for (int i = 0; i < exclusives.Count; i++)
            {
                try
                {
                    Exclusive exl = exclusives[i];
                    if (exl.Path == null)
                        continue;
                    int code = Tool.GetInt(exclusives[i].Path.City.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                    code = Tool.GetInt(exclusives[i].Path.City1.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                    Car car = Helper.Instance.GetCar(exl.CarID);
                    string plateNo = "", plateSeries = "";
                    GetPlateSeri(car, out plateNo, out plateSeries);

                    builder.Append(cmpCode); // 1-5 for CompanyCode
                    builder.Append('0'); // 6 for e : " if (Alltrim(copy(StrTemp,6,1))='C') then { SetLength(SeryPelak,8); SeryPelak[6]:='C'; SeryPelak[7]:='?'; SeryPelak[8]:='?';}
                    builder.Append(new string(' ', 4)); // 7-11 empty
                    builder.Append(GetSeries(exl.Series, true)); // 12-18 for SerySoorat
                    builder.Append(GetString(exl.No, 6)); // 19-24 for NoSoorat
                    builder.Append(new string(' ', 8)); // 25-32 empty
                    builder.Append(GetString(plateSeries, 3)); // 33-35 for SeryPelak
                    builder.Append(GetString(plateNo, 6)); // 36-41 for SeryPelak
                    builder.Append(new string(' ', 7)); // 42-48 empty
                    builder.Append(GetString(Math.Max(1, exl.NumPassangers), 2)); // 49-50 for TedadMosafer = g
                    builder.Append(GetString(Tool.ToPersianDate(exl.DepartureDate, "").Replace("/", ""), 6)); // 51-56 for TarikhHarkat = h
                    builder.Append(GetString(exl.Car.CarType.Code, 1)); // 57 for KindOfCar = i
                    builder.Append(GetString(Helper.Instance.GetCityCode(exl.Path.SrcCityID), 8)); // 58-65 for StartCity = j
                    builder.Append(GetString(Helper.Instance.GetCityCode(exl.Path.DestCityID), 8)); // 66-73 for EndCity = k
                    builder.Append(new string(' ', 131)); // 74-202 empty
                    builder.Append(GetString(exl.BodyInsurance, 6)); // 203-208 for BadanehPardakhti = l
                    builder.Append(GetString(exl.Insurance, 6)); // 209-214 for SarneshinPardakhti = m
                    builder.Append(GetString(exl.DepartureTime, 5)); // 215-219 for Saat = n
                    builder.Append(new string(' ', 37)); // 220-256 empty

                    builder.Append("\r\n");
                }
                catch { }
            }
        if (invalids != null)
            for (int i = 0; i < invalids.Count; i++)
            {
                try
                {
                    InValid inv = invalids[i];
                    Service sv1 = null;
                    Path path = null;
                    if (inv.Exclusive == true)
                        path = Helper.Instance.GetPath(inv.ServiceID);
                    else
                    {
                        sv1 = Helper.Instance.GetService(inv.ServiceID);
                        path = sv1 != null ? sv1.Path : null;
                    }
                    if (path != null)
                    {
                        int code = Tool.GetInt(path.City.Code, 0);
                        if (code >= 1 && code <= 999)
                            continue;
                        code = Tool.GetInt(path.City1.Code, 0);
                        if (code >= 1 && code <= 999)
                            continue;
                    }
                    Car car = Helper.Instance.GetCar(inv.CarID);
                    string plateNo = "", plateSeries = "";
                    GetPlateSeri(car, out plateNo, out plateSeries);

                    builder.Append(cmpCode); // 1-5 for CompanyCode
                    builder.Append('0'); // 6 for e : " if (Alltrim(copy(StrTemp,6,1))='C') then { SetLength(SeryPelak,8); SeryPelak[6]:='C'; SeryPelak[7]:='?'; SeryPelak[8]:='?';}
                    builder.Append(new string(' ', 4)); // 7-11 empty
                    builder.Append(GetSeries(inv.Series, true)); // 12-18 for SerySoorat
                    builder.Append(GetString(inv.No, 6)); // 19-24 for NoSoorat
                    builder.Append(new string(' ', 8)); // 25-32 empty
                    builder.Append(GetString("", 3)); // 33-35 for SeryPelak
                    builder.Append(GetString("", 6)); // 36-41 for SeryPelak
                    builder.Append(new string(' ', 7)); // 42-48 empty
                    builder.Append(GetString(0, 2)); // 49-50 for TedadMosafer = g
                    builder.Append(GetString(Tool.ToPersianDate(inv.Date, "").Replace("/", ""), 6)); // 51-56 for TarikhHarkat = h
                    builder.Append(GetString(8, 1)); // 57 for KindOfCar = i
                    builder.Append(GetString(path == null ? "" : Helper.Instance.GetCityCode(path.SrcCityID), 8)); // 58-65 for StartCity = j
                    builder.Append(GetString(path == null ? "" : Helper.Instance.GetCityCode(path.DestCityID), 8)); // 66-73 for EndCity = k
                    builder.Append(new string(' ', 131)); // 74-202 empty
                    builder.Append(GetString(0, 6)); // 203-208 for BadanehPardakhti = l
                    builder.Append(GetString(0, 6)); // 209-214 for SarneshinPardakhti = m
                    builder.Append(GetString(0, 5)); // 215-219 for Saat = n
                    builder.Append(new string(' ', 37)); // 220-256 empty

                    builder.Append("\r\n");
                }
                catch { }
            }
        //byte[] btFile = new System.Text.ASCIIEncoding().GetBytes(builder.ToString());
        Response.AppendHeader("Content-Disposition", String.Format("attachment;filename={0}.txt"
            , "Mosafer" + Tool.ToPersianDate(DateTime.Now, "").Replace("/", "-")));
        Response.ContentType = "application/download";
        Response.Write(builder.ToString());
        Response.End();
        //Response.ContentType = "application/download";
        //Response.BinaryWrite(btFile);
        //Response.End();


        //Response.ContentType = "application/octet-stream";
        //Response.Write(builder.ToString());
        //Response.End();
    }
    protected void doDanaXml_Click(object sender, EventArgs e)
    {
        List<Trip> trips = GetTrips();
        List<Exclusive> exclusives = GetExclusives();
        List<InValid> invalids = GetInvalids();
        if ((trips == null || trips.Count == 0) && (exclusives == null || exclusives.Count == 0) && (invalids == null || invalids.Count == 0))
            return;

        if (!CheckDuplicates(trips, exclusives, invalids))
            return;

        if (trips != null && trips.Count > 0)
            trips = (from c in trips
                     orderby c.Date, c.No, c.DepartureTime
                     select c).ToList();
        if (exclusives != null && exclusives.Count > 0)
            exclusives = (from ce in exclusives
                          orderby ce.DepartureDate, ce.No, ce.DepartureTime
                          select ce).ToList();
        if (invalids != null && invalids.Count > 0)
            invalids = (from ci in invalids
                        orderby ci.Date, ci.No
                        select ci).ToList();

        XmlDocument xmlDoc = new XmlDocument();
        string xmlStr = "<?xml version='1.0' encoding='UTF-8'?><hc:XmlMosaferIntS xmlns:hc=\"www.tto.ir\" xsi:schemaLocation=\"www.tto.ir newmos.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"></hc:XmlMosaferIntS>";
        xmlDoc.LoadXml(xmlStr);

        //string RecTypeNormal = 1;// SiteSettings.GetValue("RecTypeNormal");
        //string RecTypeDarbasti = SiteSettings.GetValue("RecTypeDarbasti");
        //string StatNormal = 0;// SiteSettings.GetValue("StatNormal");
        //string StatDarbasti = SiteSettings.GetValue("StatDarbasti");
        //string StatInvalid = 1;// SiteSettings.GetValue("StatInvalid");

        #region Trips
        string companyCode = Helper.Instance.GetSettingValue("CompanyCode");
        if (trips != null)
            for (int i = 0; i < trips.Count; i++)
            {
                if (trips[i].Service.Path == null)
                    continue;
                int code = Tool.GetInt(trips[i].Service.Path.City.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                code = Tool.GetInt(trips[i].Service.Path.City1.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                int numTickets = Helper.Instance.GetNumTickets(trips[i].ID);
                if (numTickets < 1)
                    numTickets = 1;

                // if (numTickets < 1)
                //    continue;
                //if (trips[i].Service.Path.KMs == 0)
                //    continue;
                int fare = Helper.Instance.GetPrice(trips[i].ID);
                //if (fare < 1)
                //    continue;

                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "XmlMosaferInt", "");
                // -----------------
                XmlNode n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Seri", "");
                string seri = GetSeries(trips[i].Series, false);
                seri = seri;
                n1.InnerText = seri;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "MosNo", "");
                n1.InnerText = trips[i].No.ToString();// Helper.Instance.GetNumTickets(trips[i].ID).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Company", "");
                n1.InnerText = companyCode;
                node.AppendChild(n1);

                Car car = Helper.Instance.GetCar(trips[i].CarID);
                string plateNo = "", plateSeries = "";
                GetPlateSeri(car, out plateNo, out plateSeries);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                n1.InnerText = Encode(plateNo);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                n1.InnerText = plateSeries;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                n1.InnerText = Helper.Instance.GetCityCodeForCar(trips[i].CarID);
                if (string.IsNullOrEmpty(n1.InnerText))
                {
                    n1.InnerText = Helper.Instance.GetCityCodeForCar(trips[i].CarID);
                }
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                n1.InnerText = numTickets.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "IssuDate", "");
                n1.InnerText = Tool.ToPersianDate(trips[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "CarType", "");
                n1.InnerText = Helper.Instance.GetCarTypeCode(trips[i].CarID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "OriginCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(trips[i].Service.Path.SrcCityID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DestCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(trips[i].Service.Path.DestCityID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "NavCode", "");
                n1.InnerText = "0";
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "RecType", "");
                n1.InnerText = "1";// RecTypeNormal; 
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Stat", "");
                n1.InnerText = "0";// StatNormal;
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "ThvilDate", "");
                n1.InnerText = Tool.ToPersianDate(trips[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Fare", "");
                n1.InnerText = fare.ToString();
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Dist", "");
                n1.InnerText = trips[i].Service.Path.KMs.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeAmnt", "");
                n1.InnerText = trips[i].Insurance.ToString();
                node.AppendChild(n1);



                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DriverCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetDriverSmartCard(trips[i].DriverID1);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "VehicleCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetCarSmartCard(trips[i].CarID);
                node.AppendChild(n1);


                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimehBadaneh", "");
                n1.InnerText = trips[i].BodyInsurance.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimehSarneshin", "");
                n1.InnerText = trips[i].Insurance.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Saat", "");
                n1.InnerText = trips[i].DepartureTime2;
                node.AppendChild(n1);

                xmlDoc.DocumentElement.AppendChild(node);
            }
        #endregion

        #region Exclusives
        if (exclusives != null)
            for (int i = 0; i < exclusives.Count; i++)
            {
                Exclusive exl = exclusives[i];
                //if (exl.NumPassangers < 1)
                //    continue;
                //if (exl.TotalRent < 1)
                //    continue;
                //if (exl.Path.KMs < 1)
                //    continue;
                if (exl.Path == null)
                    continue;
                int code = Tool.GetInt(exl.Path.City.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;
                code = Tool.GetInt(exl.Path.City1.Code, 0);
                if (code >= 1 && code <= 999)
                    continue;

                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "XmlMosaferInt", "");
                // -----------------
                XmlNode n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Seri", "");
                string seri = GetSeries(exl.Series, false);
                n1.InnerText = seri;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "MosNo", "");
                n1.InnerText = exl.No.ToString();// Helper.Instance.GetNumTickets(exl.ID).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Company", "");
                n1.InnerText = companyCode;// Helper.Instance.GetSettingValue("CompanyCode");
                node.AppendChild(n1);

                Car car = Helper.Instance.GetCar(exl.CarID);
                string plateNo = "", plateSeries = "";
                GetPlateSeri(car, out plateNo, out plateSeries);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                n1.InnerText = Encode(plateNo);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                n1.InnerText = plateSeries;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                n1.InnerText = Helper.Instance.GetCityCodeForCar(exl.CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                n1.InnerText = Math.Max(1, exl.NumPassangers).ToString();
                node.AppendChild(n1);


                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "IssuDate", "");
                n1.InnerText = Tool.ToPersianDate(exl.DepartureDate, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "CarType", "");
                n1.InnerText = Helper.Instance.GetCarTypeCode(exl.CarID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "OriginCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(exl.Path.SrcCityID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DestCity", "");
                n1.InnerText = Helper.Instance.GetCityCode(exl.Path.DestCityID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "NavCode", "");
                n1.InnerText = "0";
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "RecType", "");
                n1.InnerText = "2";// RecTypeDarbasti;
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Stat", "");
                n1.InnerText = "0";// StatDarbasti;
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "ThvilDate", "");
                n1.InnerText = Tool.ToPersianDate(exl.DepartureDate, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Fare", "");
                n1.InnerText = exl.TotalRent.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Dist", "");
                n1.InnerText = exl.Path.KMs.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeAmnt", "");
                n1.InnerText = exl.Insurance.ToString();
                node.AppendChild(n1);


                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DriverCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetDriverSmartCard(exl.DriverID1);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "VehicleCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetCarSmartCard(exl.CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimehBadaneh", "");
                n1.InnerText = exl.BodyInsurance.ToString();
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimehSarneshin", "");
                n1.InnerText = exl.Insurance.ToString();
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Saat", "");
                n1.InnerText = exl.DepartureTime;
                node.AppendChild(n1);

                xmlDoc.DocumentElement.AppendChild(node);
            }
        #endregion

        #region Invalids
        if (invalids != null)
            for (int i = 0; i < invalids.Count; i++)
            {
                Service sv1 = null;
                Path path = null;
                if (invalids[i].Exclusive == true)
                    path = Helper.Instance.GetPath(invalids[i].ServiceID);
                else
                {
                    sv1 = Helper.Instance.GetService(invalids[i].ServiceID);
                    path = sv1 != null ? sv1.Path : null;
                }
                if (path != null)
                {
                    int code = Tool.GetInt(path.City.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                    code = Tool.GetInt(path.City1.Code, 0);
                    if (code >= 1 && code <= 999)
                        continue;
                }
                XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "XmlMosaferInt", "");
                // -----------------
                XmlNode n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Seri", "");
                string seri = GetSeries(invalids[i].Series, false);
                n1.InnerText = seri;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "MosNo", "");
                n1.InnerText = invalids[i].No.ToString();// Helper.Instance.GetNumTickets(trips[i].ID).ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Company", "");
                n1.InnerText = companyCode;// Helper.Instance.GetSettingValue("CompanyCode");
                node.AppendChild(n1);


                // -----------------
                Car car = Helper.Instance.GetCar(invalids[i].CarID);
                string plateNo = "", plateSeries = "";
                GetPlateSeri(car, out plateNo, out plateSeries);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                n1.InnerText = Encode(plateNo);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                n1.InnerText = plateSeries;
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                n1.InnerText = Helper.Instance.GetCityCodeForCar(invalids[i].CarID);
                node.AppendChild(n1);
                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkNo", "");
                //n1.InnerText = "";
                //node.AppendChild(n1);

                //// -----------------
                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkSeri", "");
                //n1.InnerText = "0";
                //node.AppendChild(n1);

                //// -----------------
                //n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PlkLoc", "");
                //n1.InnerText = "0";
                //node.AppendChild(n1);

                // -----------------
                int numTickets = 0;
                try { numTickets = Helper.Instance.GetNumTickets(invalids[i].TripID); }
                catch { }

                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "PassNum", "");
                n1.InnerText = numTickets.ToString();
                node.AppendChild(n1);


                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "IssuDate", "");
                n1.InnerText = Tool.ToPersianDate(invalids[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "CarType", "");
                n1.InnerText = "8";// Helper.Instance.GetCarTypeCode(trips[i].CarID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "OriginCity", "");
                n1.InnerText = path == null ? "0" : Helper.Instance.GetCityCode(path.SrcCityID);
                node.AppendChild(n1);

                //-----
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DestCity", "");
                n1.InnerText = path == null ? "0" : Helper.Instance.GetCityCode(path.DestCityID);
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "NavCode", "");
                n1.InnerText = "0";
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "RecType", "");
                n1.InnerText = "0";// invalids[i].Exclusive == true ? RecTypeDarbasti : RecTypeNormal; 
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Stat", "");
                n1.InnerText = "1";// StatInvalid;
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "ThvilDate", "");
                n1.InnerText = Tool.ToPersianDate(invalids[i].Date, DateTime.Now).Replace("/", "");
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Fare", "");
                n1.InnerText = "0";
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Dist", "");
                n1.InnerText = path == null ? "0" : path.KMs.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo1", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeDeptNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeSeqNo2", "");
                n1.InnerText = "0";
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimeAmnt", "");
                n1.InnerText = "0";// trips[i].Insurance.ToString();
                node.AppendChild(n1);


                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "DriverCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetDriverSmartCard(trips[i].DriverID1);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "VehicleCardNo", "");
                n1.InnerText = "0";// Helper.Instance.GetCarSmartCard(trips[i].CarID);
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimehBadaneh", "");
                n1.InnerText = "0";// trips[i].Insurance.ToString();
                node.AppendChild(n1);
                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "BimehSarneshin", "");
                n1.InnerText = "0";// trips[i].Insurance.ToString();
                node.AppendChild(n1);

                // -----------------
                n1 = xmlDoc.CreateNode(XmlNodeType.Element, "Saat", "");
                n1.InnerText = sv1 != null ? sv1.DepartureTime : "00:00";
                node.AppendChild(n1);

                xmlDoc.DocumentElement.AppendChild(node);
            }
        #endregion
        Response.AppendHeader("Content-Disposition", String.Format("attachment;filename={0}.xml"
            , "MosaferDana" + Tool.ToPersianDate(DateTime.Now, "").Replace("/", "")));
        Response.ContentType = "application/download";
        string outter = xmlDoc.OuterXml.Replace("<XmlMosaferInt>", "<XmlMosaferInt>\n");
        outter = outter.Replace("</XmlMosaferInt>", "\n</XmlMosaferInt>\n");
        //outter = outter.Replace("</hc:XmlMosaferIntS>", "</hc:XmlMosaferIntS>");
        outter = outter.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">", "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");

        //string outter = xmlDoc.OuterXml;
        //outter = outter.Replace("</XmlMosaferInt></hc:XmlMosaferIntS>", "\n</XmlMosaferInt>\n</hc:XmlMosaferIntS>");
        //outter = outter.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><XmlMosaferInt>", "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n<XmlMosaferInt>\n");
        Response.Write(outter);
        Response.End();

        //Fild[1,0]:='Seri';//Seri sooratvaziyat
        //Fild[2,0]:='MosNo';//NoSooratVaziyat
        //Fild[3,0]:='Company';//Sherkat
        //Fild[4,0]:='PlkNo';//Shomare Pelak
        //Fild[5,0]:='PlkSeri';//Seri Pelak
        //Fild[6,0]:='PlkLoc';//City Pelak
        //Fild[7,0]:='PassNum';//Tedad Mosafer
        //Fild[8,0]:='IssuDate';//Tarikh Harkat
        //Fild[9,0]:='CarType';//Kind Of Car
        //Fild[10,0]:='OriginCity';//StartCity
        //Fild[11,0]:='DestCity';//EndCity

        //Fild[12,0]:='NavCode';//کد اتوبوس

        //Fild[13,0]:='RecType';//1 Then Stat=16 ****** Darbasti
        //Fild[14,0]:='Stat';//Status ***** 1 Then Status=14*****Ebtali Else Status=15 ******Adi

        //Fild[15,0]:='ThvilDate';
        //Fild[16,0]:='Fare';
        //Fild[17,0]:='Dist';
        //Fild[18,0]:='BimeDeptNo1';
        //Fild[19,0]:='BimeSeqNo1';
        //Fild[20,0]:='BimeDeptNo2';
        //Fild[21,0]:='BimeSeqNo2';
        //Fild[22,0]:='BimeAmnt';
        //Fild[23,0]:='DriverCardNo';
        //Fild[24,0]:='VehicleCardNo';

        //Fild[25,0]:='BimehBadaneh';//Bime Badaneh Pardakhti
        //Fild[26,0]:='BimehSarneshin';//Bime Sarneshin Pardakhti
        //Fild[27,0]:='Saat';//Saat Harkat
    }
    void GetPlateSeri(Car car, out string plateNo, out string plateSeries)
    {
        plateNo = "";
        plateSeries = "";
        if (car != null)
        {
            if (!string.IsNullOrEmpty(car.Plate))
            {
                string[] plates = car.Plate.Split('-');
                if (plates.Length == 2)
                {
                    plateNo = plates[0];
                    plateSeries = plates[1];
                }
                else if (plates.Length == 4)
                {
                    plateNo = string.Format("{0}{1}{2}", plates[3], plates[2], plates[1]);
                    plateSeries = plates[0];
                }
                else
                {
                    plateNo = car.Plate;
                    plateSeries = "";
                }
            }
            //string ins = "آابپتثجچحخدذرزژسشصضطظعغفقكگلمنوهيئ";
            //for (int i = 0; i < ins.Length; i++)
            //{
            //    plateNo = plateNo.Replace(ins[i].ToString(), "");
            //    plateSeries = plateSeries.Replace(ins[i].ToString(), "");
            //}
            plateNo = Encode(GetString(plateNo.Replace(" ", "").Replace("الف", "ا"), 6));// Encode(plateNo);
            plateSeries = Encode(GetString(plateSeries.Replace(" ", ""), 2).Replace("الف", "ا")); //Encode(plateSeries);
        }
    }
    string GetSeries(string seri, bool checkLen)
    {
        try
        {
            string[] series = seri.Split(',');
            if (series.Length >= 3)
                seri = string.Format("{0}/{1}/{2}", series[2], series[1], series[0]);
        }
        catch { }
        if (checkLen)
            return GetString(Encode(seri.Replace("الف", "ا")), 7);
        return Encode(seri.Replace("الف", "ا"));
    }
    protected void doDanaDat_Click(object sender, EventArgs e)
    {

    }

    protected void sSeries_SelectedIndexChanged(object sender, EventArgs e)
    {
        doRefineNew.Enabled = sSeries.Text.Trim().Length > 0;
        mSeriesNo1.Text = mSeriesNo2.Text = mSeriesNo3.Text = mSeriesNo4.Text = "";
        if (doRefineNew.Enabled)
            try
            {
                string[] series = sSeries.Text.Trim().Split(',');
                mSeriesNo1.Text = series[0];
                mSeriesNo2.Text = series[1];
                mSeriesNo3.Text = series[2];
                mSeriesNo4.Text = series[3];
            }
            catch { }
        else
        {
            mSeriesNo1.Text = mSeriesNo2.Text = mSeriesNo3.Text = mSeriesNo4.Text = "";
        }
    }
    protected void doPrintReception_Click(object sender, EventArgs e)
    {
        ReportGenerator("Reception","","","");
    }
}
