﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Reports_Ticket1 : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);        
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int id = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);
         //NO = d.No,
         //          Fullname = d.Fullname,
         //          DestCity = d.Trip.Service.Path.City.Title,
         //          Date = DateTime.Parse(d.Trip.Date.ToString()),
         //          DepartureTime = d.Trip.Service.DepartureTime,
         //          PassengersCount = (int)DB.GetPassengersCount(d.TripID),
         //          ChairsNo = DB.GetChairsNo(d.TripID),
         //          Price = d.Price,
         //          SaleDate = d.SaleDate
        IQueryable<TicketClass> tc = Helper.Instance.GetTicketClass(id);
        Ticket ticket = Helper.Instance.GetTicket(id);
        lblNo.Text = lblNoL.Text = ticket.No;
        lblFullName.Text = lblFullNameL.Text = ticket.Fullname;
        lblDestCity.Text = lblDestCityL.Text = Helper.Instance.GetCityTitle(ticket.CityID);
        lblDepartureTime.Text = lblDepartureTimeL.Text = ticket.Trip.Service.DepartureTime;
        lblDate.Text = lblDateL.Text = Tool.ToShortPersianDate(ticket.Trip.Date, "");
        lblDay.Text = lblDayL.Text = Tool.GetDay(ticket.Trip.Date);
        try
        {
            lblChairsNo.Text = lblChairsNoL.Text = ticket.Chairs.Replace("m", "").Replace("f", "").Replace(";", "-").Trim('-');
        }
        catch { }
        lblPassengersCount.Text = lblPassengersCountL.Text = ticket.GetNumTickets().ToString();
        lblPrice.Text = lblPriceL.Text = ((ticket.Price - ticket.Discount) * ticket.GetNumTickets()).ToString();
        lblSaleDate.Text = lblSaleDateL.Text = Tool.ToShortPersianDate(ticket.SaleDate, "");
        lblSaleTime.Text = Tool.GetTimeOfDate(ticket.SaleDate, "-:-");

        //lblSaler.Text = lblSalerL.Text = Helper.Instance.GetUser((int)ticket.SaleUserID).FullName;
        //lblPhone.Text = "تلفن:" + Helper.Instance.GetSetting("CompanyPhone").Value.ToString();
        
        //lblNo.Text = lblNoL.Text = ticket.No.ToString();
        //lblFullName.Text = lblFullNameL.Text = tc.First().Fullname.ToString();
        //lblDestCity.Text = lblDestCityL.Text = tc.First().DestCity.ToString();
        //lblDepartureTime.Text = lblDepartureTimeL.Text = tc.First().DepartureTime.ToString();
        //lblDate.Text = lblDateL.Text = Tool.ToShortPersianDate(tc.First().Date, "");
        //try
        //{
        //    lblChairsNo.Text = lblChairsNoL.Text = tc.First().ChairsNo.ToString();
        //}
        //catch { }
        //lblPassengersCount.Text = lblPassengersCountL.Text = tc.First().PassengersCount.ToString();
        //lblPrice.Text = lblPriceL.Text = tc.First().Price.ToString();
        //lblSaleDate.Text = lblSaleDateL.Text = Tool.ToShortPersianDate(tc.First().SaleDate, "");
        lblUrl.Text = Helper.Instance.GetSettingValue("Url");
        lblSaler.Text = Helper.Instance.GetUserFullName((int)ticket.SaleUserID);
        ticket.Printed = true;
        Helper.Instance.Update();
    }
}
