﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AutoTicket.aspx.cs" Inherits="Reports_AutoTicket" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        .align-center {
            text-align: center
        }
        .align-right {
            text-align: right
        }
    </style>
    <object id="OurActiveX" name='OurActiveX' classid="clsid:DE6CDCC5-F2F4-455f-B782-1D1F78EBDAF8"
        viewastext codebase="MyPrinter.cab">
    </object>

    <script language="javascript" type="text/javascript">
        function DoPrint()
        {
           try
	        {
                //alert('Set Printer ');
		        document.OurActiveX.InitPrinter(210, 70);
                //alert('done');
	        }
	        catch(Err)
	        {
		        alert(Err.description);
	        }
            print();
        }
    </script>

    <link href="../PrintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0px;" onload="javascript:DoPrint()">
    <form id="form1" runat="server" style="width: 21cm; height: 7cm">
    <table style="width: 21cm; height: 7cm; table-layout: fixed; font-size: 8pt; border-collapse: COLLAPSE"
        cellpadding="0" cellspacing="0" dir="ltr">
                    <tr>
                        <td colspan="4" class="align-center">
                            <asp:Label ID="Label1" runat="server" class='printLeftlbl'>شرکت:</asp:Label>
                            <asp:Label ID="lblCompanyName" runat="server" class='printLeftlbl' />
                        </td>
                    </tr>
                    <tr class="align-right">
                        <td colspan="2">
                            <asp:Label ID="Label3" runat="server" class='printLeftlbl'>متصدی:</asp:Label>
                            <asp:Label ID="lblUserName" runat="server" class='printLeftlbl' />
                        </td>
                        <td>
                            <asp:Label ID="Label10" runat="server" class='printLeftlbl'>تاریخ صدور:</asp:Label>
                            <asp:Label ID="lblSaleDate" runat="server" class='printlbl' />
                        </td>
                        <td>
                            <asp:Label ID="Label11" runat="server" class='printLeftlbl'>شماره بليط:</asp:Label>
                            <asp:Label ID="lblNo" runat="server" class='printlbl' />
                            
                        </td>
                    </tr>
                    <tr class="align-right">
                        <td>
                            <asp:Label ID="Label2" runat="server" class='printLeftlbl'>شماره صندلی:</asp:Label>
                            <asp:Label ID="lblChairNo" runat="server" class='printLeftlbl' />
                            
                        </td>
                        <td>
                            <asp:Label ID="Label14" runat="server" class='printLeftlbl'>تعداد مسافر: </asp:Label>
                            <asp:Label ID="lblPassengersCount" runat="server" class='printlbl' />
                            
                        </td>
                        <td colspan="2">
                            <asp:Label ID="Label13" runat="server" class='printLeftlbl'>نام مسافر:</asp:Label>
                            <asp:Label ID="lblFullName" runat="server" class='printlbl' />
                            
                        </td>
                    </tr>
                    <tr class="align-right">
                        <td>
                            <asp:Label ID="Label12" runat="server" class='printLeftlbl'>ساعت حرکت:</asp:Label>
                            <asp:Label ID="lblDepartureTime" runat="server" class='printlbl' />
                            
                        </td>
                        <td>
                            <asp:Label ID="Label5" runat="server" class='printLeftlbl'>تاریخ حرکت:</asp:Label>
                            <asp:Label ID="lblDate" runat="server" class='printlbl' />
                            
                        </td>
                        <td>
                            <asp:Label ID="Label16" runat="server" class='printLeftlbl'>مقصد:</asp:Label>
                            <asp:Label ID="lblDestCity" runat="server" class='printLeftlbl' />
                        </td>
                        <td style="text-align: right">
                            <asp:Label ID="Label15" runat="server" class='printLeftlbl'>مبدا:</asp:Label>
                            <asp:Label ID="lblSrcCity" runat="server" class='printLeftlbl' />
                        </td>
                    </tr>
                    <tr class="align-right">
                        <td colspan="2">
                           
                            <asp:Label ID="Label8" runat="server" class="printLeftlbl">کرايه کل:</asp:Label>
                            <asp:Label ID="lblTotalPrice" runat="server" class="printLeftlbl" />
                            <asp:Label ID="Label9" runat="server" class="printLeftlbl"> ريال </asp:Label>
                           
                        </td>
                        <td>
                            <asp:Label ID="Label19" runat="server" class="printLeftlbl">تخفیف:</asp:Label>
                            <asp:Label ID="lblDiscount" runat="server" class="printLeftlbl" />
                        </td>
                        <td>
                            <asp:Label ID="Label17" runat="server" class="printLeftlbl">کرايه:</asp:Label>
                            <asp:Label ID="lblPrice" runat="server" class="printLeftlbl" />
                            <asp:Label ID="Label18" runat="server" class="printLeftlbl"> ريال </asp:Label>
                        </td>
                    </tr>
    </table>
    </form>
</body>
</html>
