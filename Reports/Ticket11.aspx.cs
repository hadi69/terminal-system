﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Reports_Ticket11 : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int id = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);
        IQueryable<TicketClass> tc = Helper.Instance.GetTicketClass(id);
        Ticket ticket = Helper.Instance.GetTicket(id);
        lblNo.Text = lblNoL.Text=  ticket.No;
        lblFullName.Text = lblFullNameL.Text = ticket.Fullname;
        lblDestCity.Text = lblDestCityL.Text = Helper.Instance.GetCityTitle(ticket.CityID);
        lblSrcCity.Text = ticket.Trip.Service.Path.City.Title;
        lblDepartureTime.Text = lblDepartureTimeL.Text = ticket.Trip.Service.DepartureTime;
        lblDate.Text = lblDateL.Text = Tool.ToShortPersianDate(ticket.Trip.Date, "");
        lblDay.Text = lblDayL.Text = Tool.GetDay(ticket.Trip.Date);
        try
        {
            lblChairsNo.Text = ticket.Chairs.Replace("m", "").Replace("f", "").Replace(";", "-").Trim('-');
            //lblChairsNoL.Text = lblChairsNo.Text;
        }
        catch { }
        lblPassengersCount.Text = lblPassengersCountL.Text = ticket.GetNumTickets().ToString();
        lblPrice.Text = lblPriceL.Text = ((ticket.Price - ticket.Discount) * ticket.GetNumTickets()).ToString();
        lblPriceForOne.Text = ticket.Price.ToString();
        lblPriceL0.Text = Tool.NumberToFarsi((int)(ticket.Price - ticket.Discount) * ticket.GetNumTickets());
        lblSaleDate.Text = lblSaleDateL.Text = Tool.ToShortPersianDate(ticket.SaleDate, "");
        lblSaleTime.Text = Tool.GetTimeOfDate(ticket.SaleDate, "-:-");
        lblKMs.Text = ticket.Trip.Service.Path.KMs.ToString();

        //lblSaler.Text = lblSalerL.Text = Helper.Instance.GetUser((int)ticket.SaleUserID).FullName;
        //lblPhone.Text = "تلفن:" + Helper.Instance.GetSetting("CompanyPhone").Value.ToString();
        lblUrl.Text = Helper.Instance.GetSettingValue("Url");
        lblReception.Text = ticket.Trip.Reception.ToString("N0");
        lblSaler.Text = Helper.Instance.GetUserFullName((int)ticket.SaleUserID);

        ticket.Printed = true;
        Helper.Instance.Update();

    }
}
