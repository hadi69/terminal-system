﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Reports_TicketAll : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Helper.Instance.Init();
        int sellerID = Tool.GetInt(Request.QueryString["sellerid"], Null.NullInteger);
        Seller seller = Helper.Instance.GetSeller(sellerID);
        if (seller == null)
            return;
        string qry = @"SELECT Tickets.ID, Tickets.No, Tickets.Chairs, Tickets.Fullname, Tickets.Tel, Tickets.Price
, Tickets.NumChairs,  ((Tickets.Price - Discount) * Tickets.NumChairs) AS Total, Tickets.Discount
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
, Branches.Title AS Branch
, Users.FullName AS  SaleUser, SaleDate, SaleType, Services.DepartureTime, Trips.Date AS TripsDate
FROM Tickets 
INNER JOIN Trips ON Tickets.TripID=Trips.ID
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Branches ON Tickets.BranchID=Branches.ID
LEFT JOIN Users ON Tickets.SaleUserID=Users.ID
WHERE Tickets.ID=" + Request.QueryString["id"];
        DataTable dt = Helper.Instance.FillDataTable(qry, seller.CnnStr);
        if (dt == null || dt.Rows.Count == 0)
            return;
        Helper.Instance.RunQuery("UPDATE Tickets SET Archive=1 WHERE ID=" + Request.QueryString["id"], seller.CnnStr);
        DataRow ticket = dt.Rows[0];
        lblSeller.Text = seller.Title;
        lblNo.Text = lblNoL.Text = Tool.GetString(ticket["No"], "");
        lblFullName.Text = lblFullNameL.Text = Tool.GetString(ticket["Fullname"], "");
        lblDestCity.Text = lblDestCityL.Text = Tool.GetString(ticket["DestCity"], "");
        lblSrcCity.Text = lblSrcCityL.Text = Tool.GetString(ticket["SrcCity"], "");
        lblDepartureTime.Text = lblDepartureTimeL.Text = Tool.GetString(ticket["DepartureTime"], "");
        lblDate.Text = lblDateL.Text = Tool.ToShortPersianDate(ticket["TripsDate"], "");
        try
        {
            lblChairsNo.Text = lblChairsNoL.Text = Tool.GetString(ticket["Chairs"], "").Replace("m", "").Replace("f", "").Replace(";", "-").Trim('-');
        }
        catch { }
        lblPassengersCount.Text = lblPassengersCountL.Text = Tool.GetInt(ticket["NumChairs"], 0).ToString();
        lblPrice.Text = Tool.GetInt(ticket["Price"], 0).ToString();
        lblPrice2.Text = lblPriceL.Text = ((Tool.GetInt(ticket["Price"], 0) - Tool.GetInt(ticket["Discount"], 0)) * Tool.GetInt(ticket["NumChairs"], 0)).ToString();
        lblSaleDate.Text = lblSaleDateL.Text = Tool.ToShortPersianDate(ticket["SaleDate"], "");
        lblSaleTime.Text = Tool.GetTimeOfDate(ticket["SaleDate"], "-:-");
        lblSaler.Text = lblSalerL.Text = Tool.GetString(ticket["SaleUser"], "");


        lblUrl.Text = Helper.Instance.GetSettingValue("Url");
    }
}
