﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Reports_MiniTicket : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int id = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);

        IQueryable<TicketClass> tc = Helper.Instance.GetTicketClass(id);
        Ticket ticket = Helper.Instance.GetTicket(id);
        lblNo.Text = lblNoL.Text = ticket.No;
        lblCompanyName.Text = lblCompanyNameL.Text = Helper.Instance.GetSettingValue("CompanyName");
        lblFullName.Text = lblFullNameL.Text = ticket.Fullname;
        lblDestCity.Text = lblDestCityL.Text = Helper.Instance.GetCityTitle(ticket.CityID);
        lblSrcCity.Text = lblSrcCityL.Text = ticket.Trip.Service.Path.City.Title;
        lblDepartureTime.Text =  ticket.Trip.Service.DepartureTime;
        lblDate.Text =  Tool.ToShortPersianDate(ticket.Trip.Date, "");
        //try
        //{
        //    lblChairsNo.Text = ticket.Chairs.Replace("m", "").Replace("f", "").Replace(";", "-").Trim('-');
        //    //lblChairsNoL.Text = lblChairsNo.Text;
        //}
        //catch { }
        //lblPassengersCount.Text = lblPassengersCountL.Text = ticket.GetNumTickets().ToString();
        lblPrice.Text = lblPriceL.Text = ((ticket.Price - ticket.Discount) * ticket.GetNumTickets()).ToString();
        //lblSaleDate.Text = lblSaleDateL.Text = Tool.ToShortPersianDate(ticket.SaleDate, "");
        lblSaleDateL.Text = Tool.ToShortPersianDate(ticket.SaleDate, "");
        lblSaleTime.Text = Tool.GetTimeOfDate(ticket.SaleDate, "-:-");

        lblCar.Text = Helper.Instance.GetCarTitle(ticket.Trip.CarID);
    }
}
