﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ticket12.aspx.cs" Inherits="Reports_Ticket12" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <object id="OurActiveX" name='OurActiveX' classid="clsid:DE6CDCC5-F2F4-455f-B782-1D1F78EBDAF8"
        viewastext codebase="MyPrinter.cab">
    </object>

    <script language="javascript" type="text/javascript">
        function DoPrint() {
            try {
                //alert('Set Printer ');
                document.OurActiveX.InitPrinter(210, 54);
                //alert('done');
            }
            catch (Err) {
                alert(Err.description);
            }
            print();
            window.close();
        }
    </script>

    <link href="../PrintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0px;" onload="javascript:DoPrint()">
    <form id="form1" runat="server" style="width: 11cm; height: 28cm">
    &nbsp;
    <asp:Label Style="position: absolute; top: 60px; left: 293px;" ID="lblNo" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 95px; left: 287px; height: 14px" ID="lblFullName"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 129px; left: 57px; height: 14px" ID="lblDestCity"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 164px; left: 25px;" ID="lblDepartureTime"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 164px; left: 328px;" ID="lblDay" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 164px; left: 254px;" ID="lblDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 200px; left: 59px;" ID="lblChairsNo"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 232px; left: 318px;" ID="lblPrice" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 95px; left: 7px;" ID="lblSaleDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 400px; left: 302px; height: 11px;" ID="lblNoL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 428px; left: 280px; height: 14px" ID="lblFullNameL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 453px; left: 56px; height: 15px;" ID="lblDestCityL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 480px; left: 261px;" ID="lblDateL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 480px; left: 34px;" ID="lblDepartureTimeL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 512px; left: 336px;" ID="lblPassengersCountL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 512px; left: 72px;" ID="lblChairsNoL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 550px; left: 249px;" ID="lblPriceL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 428px; left: 33px;" ID="lblSaleDateL"
        runat="server" class='printLeftlbl' />
    <%--<asp:Label Style="position: absolute; top: 456px; left: -51px; width: 29px; height: 24px;" ID="lblSaleTime"
        runat="server" class='printlbl' />--%>
    <asp:Label Style="position: absolute; top: 343px; left: 242px; margin-top: 0px;" ID="lblUrl" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 62px; left: 15px;" ID="lblSaler" runat="server"
        class='printlbl' />
        
    <asp:Label Style="position: absolute; top: 480px; left: 338px;" ID="lblDayL" runat="server"
        class='printLeftlbl' />
        
    <asp:Label Style="position: absolute; top: 200px; left: 317px;" ID="lblPassengersCount"
        runat="server" class='printlbl' />
        
        
        <asp:Label Style="position: absolute; top: 1135px; left: 296px; height: 11px;" ID="dblNo2"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1160px; left: 298px; height: 14px" ID="lblFullName2"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1190px; left: 46px; height: 15px;" ID="lblDestCity2"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1220px; left: 264px;" ID="lblDate2" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1220px; left: 17px;" ID="lblDepartureTime2"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1160px; left: 54px;" ID="lblPassengersCount2"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1250px; left: 41px;" ID="lblPrice2" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 1220px; left: 350px; margin-top: 0px;" ID="lblDay2" runat="server"
        class='printLeftlbl' />
        
    <asp:Label Style="position: absolute; top: 1133px; left: 26px;" ID="lblSaler2" runat="server"
        class='printLeftlbl' />
    </form>
</body>
</html>
