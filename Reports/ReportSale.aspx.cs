﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
public partial class Reports_ReportSale : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //tripid={0}&userid
        string tripid = Tool.GetString(Request.QueryString["tripid"], "");
        string etripid = Tool.GetString(Request.QueryString["etripid"], "");
        int userid = Tool.GetInt(Request.QueryString["userid"], Null.NullInteger);
        string repname = Tool.GetString(Request.QueryString["repname"], "");
        string repTitle = Tool.GetString(Request.QueryString["repTitle"], "");
        string fullName = Tool.GetString(Request.QueryString["fullName"], "");
        string repParam = Tool.GetString(Request.QueryString["repParam"], "");
        LoadReport(tripid,etripid, userid,repname,repTitle,fullName,repParam);

        if (repname == "Cartable")
            doReturn.NavigateUrl = "Cartable.aspx";
        else if(repname == "PayGet")
            doReturn.NavigateUrl = "~/Funds/PayGet.aspx";
        else if (repname == "UserSale" || repname == "UsersSale")
            doReturn.NavigateUrl = "UserActivity.aspx";
        else
            doReturn.NavigateUrl = "SuratVaziatSummary.aspx";


    }
    public void LoadReport(string tripid, string etripid, int userid, string repname, string repTitle, string fullName, string repParam)
    {
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportDataSource dsLoanList = null;
        //ReportDataSource dsUserList = null;
        //if (repname == "UserSale")
        //    dsLoanList = new ReportDataSource("UserSale", Helper.Instance.GetUserSale(tripid, userid));
        if (repname == "UsersSale")
            dsLoanList = new ReportDataSource("UsersSale", Helper.Instance.GetUsersSale(int.Parse(tripid)));
        //else if (repname == "Cartable")
        //    dsLoanList = new ReportDataSource("Cartable", Helper.Instance.GetCartable(tripid, userid));
        //else if ((repname == "SuratVaziatSummary") || (repname == "Toll2") || (repname == "SumToll2") || (repname == "AutoCar") || (repname == "Driver") || (repname == "AutoShare"))
        //    dsLoanList = new ReportDataSource("SuratVaziatSummary", Helper.Instance.GetSuratVaziatSummary(tripid,etripid,false));
        //else if (repname == "Insurance")
        //    dsLoanList = new ReportDataSource("SuratVaziatSummary", Helper.Instance.GetSuratVaziatSummary(tripid,etripid,false));
        else if (repname == "PayGet")
            dsLoanList = new ReportDataSource("PayGet", Helper.Instance.GetPayGet(tripid));
            

    
        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        if (repname == "UserSale")
            ReportViewer1.LocalReport.ReportPath = "Reports/UserSale.rdlc";
        else if (repname == "UsersSale")
            ReportViewer1.LocalReport.ReportPath = "Reports/UsersSale.rdlc";
        else if (repname == "Cartable")
            ReportViewer1.LocalReport.ReportPath = "Reports/Cartable.rdlc";
        else if (repname == "SuratVaziatSummary")
            ReportViewer1.LocalReport.ReportPath = "Reports/SuratVaziatSummary.rdlc";
        else if (repname == "Toll2")
            ReportViewer1.LocalReport.ReportPath = "Reports/Toll2.rdlc";
        else if (repname == "SumToll2")
            ReportViewer1.LocalReport.ReportPath = "Reports/SumToll2.rdlc";
        else if (repname == "Insurance")
            ReportViewer1.LocalReport.ReportPath = "Reports/Insurance.rdlc";
        else if ((repname == "AutoCar") || (repname == "Driver"))
            ReportViewer1.LocalReport.ReportPath = "Reports/AutoCar.rdlc";
        else if (repname == "AutoShare")
            ReportViewer1.LocalReport.ReportPath = "Reports/AutoShare.rdlc";
        else if (repname == "PayGet")
            ReportViewer1.LocalReport.ReportPath = "Reports/PayGet.rdlc";
        



        ReportParameter[] paramss = null;
        if (repname == "UserSale" || repname == "Cartable")
        {
            paramss = new ReportParameter[3];
            paramss[0] = new ReportParameter("tripID", tripid, false);
            paramss[1] = new ReportParameter("userID", userid.ToString(), false);
            paramss[2] = new ReportParameter("FullName", Helper.Instance.GetUserFullName(userid), false);
        }
        else if ((repname == "UsersSale"))
        {
            paramss = new ReportParameter[1];
            paramss[0] = new ReportParameter("tripID", tripid, false);
        }
        else if ((repname == "SuratVaziatSummary") ||(repname == "Toll2") || (repname == "SumToll2"))
        {
            paramss = new ReportParameter[2];
            paramss[0] = new ReportParameter("tripID", tripid, false);
            paramss[1] = new ReportParameter("addEmptyItems", "False", false);
        }
        else if (repname == "Insurance")
        {
            paramss = new ReportParameter[3];
            paramss[0] = new ReportParameter("tripID", tripid, false);
            paramss[1] = new ReportParameter("FullName", Helper.Instance.GetSettingValue("CompanyName"), false);
            paramss[2] = new ReportParameter("addEmptyItems", "False", false);
        }
        else if ((repname == "AutoCar") || (repname == "Driver"))
        {
            paramss = new ReportParameter[5];
            paramss[0] = new ReportParameter("tripID", tripid, false);
            paramss[1] = new ReportParameter("RepTitle", repTitle, false);
            paramss[2] = new ReportParameter("RepParam", repParam, false);
            paramss[3] = new ReportParameter("FullName", fullName, false);
            paramss[4] = new ReportParameter("addEmptyItems", "True", false);
        }
        else if (repname == "AutoShare")
        {
            paramss = new ReportParameter[6];
            paramss[0] = new ReportParameter("tripID", tripid, false);
            paramss[1] = new ReportParameter("etripID", etripid, false);
            paramss[2] = new ReportParameter("RepTitle", repTitle, false);
            paramss[3] = new ReportParameter("RepParam", repParam, false);
            paramss[4] = new ReportParameter("FullName", fullName, false);
            paramss[5] = new ReportParameter("addEmptyItems", "False", false);
        }
        else if (repname == "PayGet")
        {
            paramss = new ReportParameter[4];
            paramss[0] = new ReportParameter("paymentID", tripid, false);
            paramss[1] = new ReportParameter("RepTitle", repTitle, false);
            paramss[2] = new ReportParameter("RepParam", repParam, false);
            paramss[3] = new ReportParameter("FullName", fullName, false);
        }

        ReportViewer1.LocalReport.SetParameters(paramss);
        ReportViewer1.LocalReport.DataSources.Add(dsLoanList);

        ReportViewer1.LocalReport.Refresh();
    }
}
