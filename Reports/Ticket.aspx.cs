﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Reports_Ticket : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (null == Helper.Instance.DB)
            Helper.Instance.Init();
        int id = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);
        string coNo = Helper.Instance.GetSettingValue("CoNo");
        string ticketText = Helper.Instance.GetSettingValue("Ticket");
        if (string.IsNullOrEmpty(coNo) && string.IsNullOrEmpty(ticketText))
            coNo = "1";
        if (!string.IsNullOrEmpty(coNo))
            Response.Redirect(Request.Url.OriginalString.ToLower().Replace("ticket.aspx"
                , string.Format("ticket{0}.aspx", coNo)));
        else
        {
            IQueryable<TicketClass> tc = Helper.Instance.GetTicketClass(id);
            Ticket ticket = Helper.Instance.GetTicket(id);
            ticketText = ticketText
         .Replace("[No]", ticket.No)
         .Replace("[FullName]", ticket.Fullname)
         .Replace("[DestCity]", Helper.Instance.GetCityTitle(ticket.CityID))
         .Replace("[SrcCity]", ticket.Trip.Service.Path.City.Title)
         .Replace("[DepartureTime]", ticket.Trip.Service.DepartureTime)
         .Replace("[Date]", Tool.ToShortPersianDate(ticket.Trip.Date, ""))
         .Replace("[ChairsNo]", ticket.Chairs.Replace("m", "").Replace("f", "").Replace(";", "-").Trim('-'))
         .Replace("[PassengersCount]", ticket.GetNumTickets().ToString())
         .Replace("[Price]", ((ticket.Price - ticket.Discount) * ticket.GetNumTickets()).ToString())
         .Replace("[SaleDate]", Tool.ToShortPersianDate(ticket.SaleDate, ""))
         .Replace("[Saler]", Helper.Instance.GetUser((int)ticket.SaleUserID).FullName)
         .Replace("\"", "'");
            printed.Text = ticketText.Replace("\r\n", "<br>").Replace(" ", "&nbsp;").Replace("[b]", "<b>").Replace("[\b]", "<\b>");
            ticketText = ticketText.Replace("[b]", "\x1bE").Replace("[\b]", "\x1bF");
            ticket.Printed = true;
            Helper.Instance.Update();
           
            string script = @"<script language='javascript' type='text/javascript'>
function OpenActiveX()
{
	try
	{
        //alert('Printing: ' + 'WHAT');
		document.OurActiveX.Print('Ticket', 'WHAT');
        //alert('done');
	}
	catch(Err)
	{
		alert(Err.description);
	}
}	
OpenActiveX();
</script>".Replace("WHAT", ticketText.Replace("\r", "\\r").Replace("\n", "\\n"));
            MMMM.OnClientClick = "OpenActiveX()";
            Page.RegisterStartupScript("PrintIt", script);
        }
    }
}
/*
 *  LPrinter - A simple line printer class in C#
 *  ============================================
 *  
 *  Written by Antonino Porcino, iz8bly@yahoo.it
 *
 *  26-sep-2008, public domain.
 *
 * 
 *  some useful print codes:
 *  ========================
 *    12 = FF (form feed)
 *    14 = enlarged on
 *    20 = enlarged off
 *    15 = compress on
 *    18 = compress off
 *    ESC + "E" = bold on
 *    ESC + "F" = bold off
 *    
 * 
 * //Generic Print Escape Sequence 
//1) Set Line Spacing a) 1/8 inch - 27,48 b) 1/6 inch - 27,50
//2) Select Draft Quality a) 27,120,0 / 27,120,48
//3) Letter Quality a) 27,120,1 / 27,120,49
//4) Double Height a) 27,119,n i) n = 1 On ii) n = 0 Off
//5) Bidirectional Printing a) 27,85,n i) 0 - Both Way ii) 1 - One Way
//6) Increase character space a) 27,32,n (Increase by n / 12 inch)
//7) Select Bold Font a) 27,69
//8) Cancel Bold Font a) 27,70
//9) Select Italic Font a) 27,52
//10) Cancel Italic Font a) 27,53
//11) Select a) 10cpi 27,8 b) 12cpi 27,77 c) 15cpi 27,103 d) 18cpi 27,103
//12) Set Right Margin a) 27,81,n
//13) Set Left Margin a) 27,108,n
//14) Form Feed a) 12
//15) Condensed Printing a) 0F On b) 12 Off
//16) Double Strike Printing a) 27,71
//17) Cancel Strike Printing a) 27,72
//18) Under line a) 27,45,0 Off b) 27,45,1 On
//19) Double Width a) 27,84,0 Off b) 27,84,1 ON
 * 
 *  //	ESC is 27 or 0x1b so ESC+E is: MyPrinter.Print("\x1bE");
 */