﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Reports_insurancehtml : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string fromDate = Tool.GetString(Request.QueryString["fromDate"], "");
        string toDate = Tool.GetString(Request.QueryString["toDate"], "");
        int specialID = Tool.GetInt(Request.QueryString["specialID"], 0);
        int serviceID = Tool.GetInt(Request.QueryString["serviceID"], 0);
        int driverID = Tool.GetInt(Request.QueryString["driverID"], 0);
        int carTypeID = Tool.GetInt(Request.QueryString["carTypeID"], 0);
        int carID = Tool.GetInt(Request.QueryString["carID"], 0);
        int destID = Tool.GetInt(Request.QueryString["destID"], 0);
        int fundID = Tool.GetInt(Request.QueryString["fundID"], 0);
        int fno = Tool.GetInt(Request.QueryString["fno"], 0);
        int tno = Tool.GetInt(Request.QueryString["tno"], 0);
        string series = Tool.GetString(Request.QueryString["series"], "");
        int userid = Tool.GetInt(Request.QueryString["userid"], 0);
        if (userid <= 0)
            userid = 0;
        if (!string.IsNullOrEmpty(toDate))
        {

            try
            {
                toDate = Convert.ToDateTime(toDate).AddDays(1).ToShortDateString();
            }
            catch { }
        }
        Helper.Instance.Init();
        Helper.Instance.LastException = null;
        List<GetInsuranceSummaryResult> list = Helper.Instance.GetInsuranceSummarySorted(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series);
        if (list == null && Helper.Instance.LastException != null)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + Helper.Instance.LastException.Message + "<br />StackTrace: " + Helper.Instance.LastException.StackTrace));
            return;
        }
        if (list == null || list.Count == 0)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("هیچ سرویسی یافت نشد"));
            return;
        }
        try
        {
            StringBuilder builder = new StringBuilder();

            // Move invalids to first
            //List<GetInsuranceSummaryResult> invalids = new List<GetInsuranceSummaryResult>();
            //list[1].Plate = "INVALID";
            //list[2].Plate = "INVALID";
            //list.Sort(Compare);
            //for (int j = 0; j < invalids.Count; j++)
            //{
            //    list.Insert(0, invalids[j]);
            //}
            int i = 0, page = 1;
            bool closed = true;
            int sumBodyInsurance = 0, sumBodyInsuranceWage = 0, sumInsurance = 0, sumInsuranceWage = 0;
            int sumBodyInsurancep = 0, sumBodyInsuranceWagep = 0, sumInsurancep = 0, sumInsuranceWagep = 0;
            for (; i < list.Count; i++)
            {
                if (i % 25 == 0)
                {
                    if (builder.Length > 0)
                    {
                        // Close Table
                        builder.AppendFormat("<tr hight='15'><td  dir='rtl' align='middle' width='60' hight='15'>{0}<br>{1}<br>{2}</td><td  dir='rtl' align='middle' width='60' hight='15'>{3}<br>{4}<br>{5}</td><td  dir='rtl' align='middle' hight='15' colspan='3'>جمع كل<br>كارمزدها<br>خالص پرداختي</td><td align='middle'>&nbsp;</td></tr>\r\n"
                            , sumBodyInsurancep, sumBodyInsuranceWagep, sumBodyInsurancep - sumBodyInsuranceWagep
                            , sumInsurancep, sumInsuranceWagep, sumInsurancep - sumInsuranceWagep);

                        //   builder.AppendFormat("<tr hight='10'><td  dir='rtl' align='middle' width='60' height='10'>{0}</td><td  dir='rtl' align='middle' width='60' height='10'>{1}</td><td  dir='rtl' align='middle' width='60' height='10'>جمع كل</td><td dir='rtl' align='middle' width='60' height='10'>&nbsp</td></tr>",
                        //sumBodyInsurancep, sumInsurancep);
                        //   builder.AppendFormat("<tr hight='10'><td  dir='rtl' align='middle' width='60' height='10'>{0}</td><td  dir='rtl' align='middle' width='60' height='10'>{1}</td><td  dir='rtl' align='middle' width='60' height='10'>كارمزدها</td><td dir='rtl' align='middle' width='60' height='10'>&nbsp</td></tr>",
                        //       sumBodyInsuranceWagep, sumInsuranceWagep);
                        //   builder.AppendFormat("<tr hight='10'><td  dir='rtl' align='middle' width='60' height='10'>{0}</td><td  dir='rtl' align='middle' width='60' height='10'>{1}</td><td  dir='rtl' align='middle' width='120' hight='10' colspan='2'>خالص پرداختي</td></tr>",
                        //       sumBodyInsurancep - sumBodyInsuranceWagep, sumInsurancep - sumInsuranceWagep);
                        //   builder.AppendLine("</tbody></table>");
                        closed = true;
                    }
                    // Start New
                    closed = false;
                    if (list.Count - i <= 25)
                        builder.AppendLine("<table width='580' align='center' border=0><tbody>");
                    else
                        builder.AppendLine("<table style='page-break-after: always' width='580' align='center' border=0><tbody>");
                    builder.AppendLine("<tr><td colspan='8' align='middle'>&nbsp;</td></tr>");
                    builder.AppendFormat("<tr><td align='middle' colspan='8' style='font-size:24px;'><div align='left'>{0}</div></td></tr>", Helper.Instance.GetSettingValue("CompanyName"));
                    builder.AppendFormat("<tr><td dir='rtl' align='middle' colspan='8'>صفحه {0}</td></tr>", page++);
                    builder.AppendLine("<tr><td align='middle' colspan='8' height='45'>&nbsp;</td></tr>");
                    sumBodyInsurancep = sumBodyInsuranceWagep = sumInsurancep = sumInsuranceWagep = 0;
                }
                bool star = false;
                bool exclusive = false;
                if (list[i].CloseDate.HasValue && list[i].Date.HasValue)
                    try
                    {
                        DateTime dt = new DateTime(list[i].Date.Value.Year, list[i].Date.Value.Month, list[i].Date.Value.Day
                            , Tool.GetInt(list[i].DepartureTime.Split(':')[0], 0)
                            , Tool.GetInt(list[i].DepartureTime.Split(':')[0], 0)
                            , 0);
                        int hours = (int)(dt - list[i].CloseDate.Value).TotalHours;
                        if (hours > 0 && dt.Day != list[i].CloseDate.Value.Day)
                            star = true;
                        else if (hours < 0)
                            star = true;

                    }
                    catch { }
                if (list[i].Others == 1 || (list[i].Others==2 && list[i].TripType==1))
                    exclusive = true;
                builder.AppendFormat("<tr dir='rtl'><td class='rowNoLine' nowrap  width='60'>{0}</td><td class='rowNoLine' nowrap  width='60'>{1}</td><td class='rowNoLine' nowrap  width='30'>{2}</td><td class='rowNoLine' nowrap  width='60'>{3}</td><td  nowrap >{4}</td><td class='rowNoLine' nowrap  width='145'>{5}</td><td class='rowNoLine' nowrap  width='145' dir=rtl>{7}{6}</td><td class='rowNoLine' nowrap align='left' width='110'>{8}</td></tr>\r\n"
                    , list[i].BodyInsurance.Value.ToString() /*+ (list[i].Others == 1 ? "D" : "")*/
                    , list[i].Insurance
                    , list[i].NumChairs
                    , list[i].DepartureTime
                    , list[i].DestCity
                    , list[i].Plate == "INVALID" ? "باطله" : list[i].Plate
                    , ShowSeries(list[i].Series, list[i].NO.Value.ToString())
                    , ""
                    , (star ? "<span style='color:red;'>*</span>" : "") +
                    Tool.ToPersianDate(list[i].Date, "") + (exclusive ? "<span><strong>D</strong></span>" : ""));
                sumBodyInsurancep += list[i].BodyInsurance.Value;
                sumBodyInsuranceWagep += list[i].BodyInsuranceWage.Value;
                sumInsurancep += list[i].Insurance.Value;
                sumInsuranceWagep += list[i].InsuranceWage.Value;
                sumBodyInsurance += list[i].BodyInsurance.Value;
                sumBodyInsuranceWage += list[i].BodyInsuranceWage.Value;
                sumInsurance += list[i].Insurance.Value;
                sumInsuranceWage += list[i].InsuranceWage.Value;
            }
            if (!closed)
            {
                while (i++ % 25 != 0)
                    builder.Append(" <tr dir=rtl><td class=rowNoLine align=middle colSpan=8>&nbsp;</td></tr>");
                // Close Table
                builder.AppendFormat("<tr hight='15'><td  dir='rtl' align='middle' width='60' hight='15'>{0}<br>{1}<br>{2}</td><td  dir='rtl' align='middle' width='60' hight='15'>{3}<br>{4}<br>{5}</td><td  dir='rtl' align='middle' hight='15' colspan='3'>جمع كل<br>كارمزدها<br>خالص پرداختي</td><td align='middle'>&nbsp;</td></tr>\r\n"
                    , sumBodyInsurance, sumBodyInsuranceWage, sumBodyInsurance - sumBodyInsuranceWage
                    , sumInsurance, sumInsuranceWage, sumInsurance - sumInsuranceWage);
                //builder.AppendFormat("<tr height='10'><td  dir='rtl' align='middle' width='60' height='10'>{0}</td><td  dir='rtl' align='middle' width='60' height='10'>{1}</td><td  dir='rtl' align='middle' width='100' height='10'>جمع كل</td><td dir='rtl' align='middle' width='60' height='10'>&nbsp</td></tr>",
                //     sumBodyInsurance, sumInsurance);
                //builder.AppendFormat("<tr height='10'><td  dir='rtl' align='middle' width='60' height='10'>{0}</td><td  dir='rtl' align='middle' width='60' height='10'>{1}</td><td  dir='rtl' align='middle' width='100' height='10'>كارمزدها</td><td dir='rtl' align='middle' width='60' height='10'>&nbsp</td></tr>",
                //    sumBodyInsuranceWage, sumInsuranceWage);
                //builder.AppendFormat("<tr height='10'><td  dir='rtl' align='middle' width='80' height='10'>{0}</td><td  dir='rtl' align='middle' width='60' height='10'>{1}</td><td  dir='rtl' align='right' width='100' height='10' colspan='2'>خالص پرداختي</td></tr>",
                //    sumBodyInsurance - sumBodyInsuranceWage, sumInsurance - sumInsuranceWage);
                builder.AppendLine("</tbody></table>");
            }
            mPlaceHolder.Controls.Clear();
            mPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + ex.Message + "<br />StackTrace: " + ex.StackTrace));
        }

        Helper.Instance.Dispose();
    }
    int Compare(GetInsuranceSummaryResult a, GetInsuranceSummaryResult b)
    {
        if ((a.Others == 2 && a.TripType==1) && (b.TripType==1&& b.Others!=2))
            return -1;
        if ((a.Others != 2 && a.TripType == 1) && (b.TripType == 1 && b.Others == 2))
            return 1;
        if (a.Others == 1 && b.Others != 1)
            return -1;
        if (a.Others != 1 && b.Others == 1)
            return 1;
        bool sortDNT = Tool.GetBool(Helper.Instance.GetSettingValue("SortDNT"), true);
        try
        {
            if (sortDNT)
            {
                DateTime dta = new DateTime(a.Date.Value.Year, a.Date.Value.Month, a.Date.Value.Day);
                DateTime dtb = new DateTime(b.Date.Value.Year, b.Date.Value.Month, b.Date.Value.Day);
                int res = dta.CompareTo(dtb);
                if (res != 0)
                    return res;
                return a.NO.Value.CompareTo(b.NO.Value);
            }
            else
            {
                DateTime dta = new DateTime(a.Date.Value.Year, a.Date.Value.Month, a.Date.Value.Day
                                     , Tool.GetInt(a.DepartureTime.Split(':')[0], 0)
                                     , Tool.GetInt(a.DepartureTime.Split(':')[0], 0)
                                     , 0);
                DateTime dtb = new DateTime(b.Date.Value.Year, b.Date.Value.Month, b.Date.Value.Day
                                     , Tool.GetInt(b.DepartureTime.Split(':')[0], 0)
                                     , Tool.GetInt(b.DepartureTime.Split(':')[0], 0)
                                     , 0);
                int res = dta.CompareTo(dtb);
                if (res != 0)
                    return res;
                return a.NO.Value.CompareTo(b.NO.Value);
            }

        }
        catch
        {
            try
            {
                return a.Date.Value.CompareTo(b.Date.Value);
            }
            catch
            {
                return 0;
            }
        }


    }
    string ShowSeries(string ser, string no)
    {
        string[] sp = ser.Split(',');
        if (sp.Length == 3)
            return string.Format("<table border=0 cellpadding='0' cellspacing='0' dir=rtl class=tt><tr><td >{0}&nbsp;&nbsp;</td><td>{1}</td><td>/</td><td>{2}</td><td>/</td><td>{3}</td></tr></table>"
                , no, sp[0], sp[1], sp[2]);
        //return sp[0] + "/" + sp[1] + "/" + sp[2];
        if (sp.Length == 4)
            return string.Format("<table border=0 cellpadding='0' cellspacing='0' dir=rtl class=tt><tr><td>{0}&nbsp;&nbsp;</td><td>{1}</td><td>/</td><td>{2}</td><td>/</td><td>{3}</td><td>/</td><td>{4}</td></tr></table>"
               , no, sp[3], sp[0], sp[1], sp[2]);
        //return sp[0] + "/" + sp[1] + "/" + sp[2] + "/" + sp[3];
        //return sp[1] + "/" + sp[0] + "/" + sp[2] + "/" + sp[3];
        return ser;
    }
}
