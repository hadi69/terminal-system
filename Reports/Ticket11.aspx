﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ticket11.aspx.cs" Inherits="Reports_Ticket11" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <object id="OurActiveX" name='OurActiveX' classid="clsid:DE6CDCC5-F2F4-455f-B782-1D1F78EBDAF8"
        viewastext codebase="MyPrinter.cab">
    </object>

    <script language="javascript" type="text/javascript">
        function DoPrint()
        {
           try
	        {
                //alert('Set Printer ');
		        document.OurActiveX.InitPrinter(210, 70);
                //alert('done');
	        }
	        catch(Err)
	        {
		        alert(Err.description);
	        }
            print();
            window.close();
        }
    </script>

    <link href="../PrintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0px;" onload="javascript:DoPrint()">
    <form id="form1" runat="server" style="width: 20cm; height: 6.5cm">
    <asp:Label Style="position: absolute; top: 16px; left: 5px;" ID="lblSaleDate" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 15px; left: 114px;" ID="lblNo" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 56px; left: 4px;" ID="lblFullName" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 96px; left: 4px;" ID="lblChairsNo" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 135px; left: 3px;" ID="lblDepartureTime"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 136px; left: 161px;" ID="lblDay" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 136px; left: 115px;" ID="lblDate" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 176px; left: 4px;" ID="lblDestCity" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 176px; left: 113px;" ID="lblPassengersCount"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 216px; left: 4px;" ID="lblPrice" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 18px; left: 230px;" ID="lblSaleDateL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 18px; left: 362px;" ID="lblNoL" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 57px; left: 231px;" ID="lblSaleTime" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 58px; left: 362px;" ID="lblFullNameL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 94px; left: 237px;" ID="lblKMs" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 95px; left: 365px;" ID="lblSrcCity" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 93px; left: 440px;" ID="lblDestCityL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 135px; left: 231px;" ID="lblPassengersCountL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 134px; left: 368px;" ID="lblDateL" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 178px; left: 236px;" ID="lblPriceForOne"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 178px; left: 367px;" ID="lblDepartureTimeL"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 216px; left: 231px;" ID="lblPriceL" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 216px; left: 363px;" ID="lblPriceL0" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 216px; left: 515px;" ID="lblUrl" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 170px; left: 515px;" ID="lblReception"
        runat="server" class='printLeftlbl' />
        <asp:Label Style="position: absolute; top: 205px; left: 433px;" ID="lblSaler" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 134px; left: 425px;" ID="lblDayL" runat="server"
        class='printlbl' />
    </form>
</body>
</html>
