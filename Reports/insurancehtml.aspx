﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="insurancehtml.aspx.cs" Inherits="Reports_insurancehtml" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        BODY {
	FONT-SIZE: 22px; MARGIN: 0px; LINE-HEIGHT: 24px; FONT-FAMILY: Arial, Tahoma, Helvetica, sans-serif
}
TD {
	PADDING-RIGHT: 5px; PADDING-LEFT: 5px; FONT-SIZE: 19px; PADDING-BOTTOM: 5px; COLOR: #330000; LINE-HEIGHT: 24px; PADDING-TOP: 5px; FONT-FAMILY: Arial, Tahoma,Helvetica, sans-serif
}
TH {
	PADDING-RIGHT: 5px; PADDING-LEFT: 5px; FONT-SIZE: 19px; PADDING-BOTTOM: 5px; COLOR: #330000; LINE-HEIGHT: 24px; PADDING-TOP: 5px; FONT-FAMILY: Arial, Tahoma,Helvetica, sans-serif
}
A {
	COLOR: #330000
}
H1 {
	FONT-SIZE: 22px; COLOR: #000000; FONT-FAMILY: Nazli,Arial,Tahoma,  Helvetica, sans-serif
}
H2 {
	FONT-WEIGHT: bold; FONT-SIZE: 24px; COLOR: #000000; FONT-FAMILY: Nazli,Arial, Tahoma,Helvetica, sans-serif
}
        .row
        {
            font-size: 18px;
            border-bottom: #555555 1px solid;
            font-family: Arial;
        }
        .rowNoLine, TD
        {
            font-size: 18px;
            line-height: 1.15;
            font-family: Arial;
            border-spacing: 0;
        }
        .tt TD
        {
        	margin:0;
        	padding:0;
        }
    </style>
    <script src='../js/jquery.min.js' type="text/javascript" ></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:PlaceHolder runat="server" ID="mPlaceHolder"></asp:PlaceHolder>
    <table width="100%" align="center" border="0" id='buttons'>
        <tbody>
            <tr>
                <td align="middle">
                    <input id="PrintButt" onclick="javascript:window.print()" type="button" value="  چاپ  "
                        name="PrintButt">
                    &nbsp;&nbsp;&nbsp;
                    <input id="Close" onclick="window.close()" type="button" value="انصراف" name="Close">
                </td>
            </tr>
        </tbody>
    </table>
    <script>
        $(function() {
            $('#PrintButt').click(function() {
                $('#buttons').remove();
                window.print();
                return false;
            });
        });
	</script>
    </form>
</body>
</html>
