﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SuratVaziatSummary.aspx.cs" Inherits="SuratVaziatSummary" Title="صورت وضعيت ها" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">

    <script src="../js/jquery.min.js" type="text/javascript"></script>

    &nbsp;<asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td colspan="6">
                        <asp:Label ID="mError" runat="server" CssClass="Err"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px" OnSelectedIndexChanged="sSeries_SelectedIndexChanged" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="mpayanehProcess" runat="server" EnableTheming="True"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sFNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sTNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        <asp:CheckBox ID="mWarnDuplicates" runat="server" Text='نمایش تکراریها' Checked="false" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <table border="0">
                            <tr>
                                <td>
                                    <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                                </td>
                                <td>
                                    &nbsp;
                                    <asp:Button Width="80px" CssClass="CB" ID="showToday" runat="server" Text='امروز'
                                        OnClick='showToday_Click' />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        نوع سرويس دهی
                    </td>
                    <td>
                        <asp:DropDownList ID="sSpecial" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        سرويس
                    </td>
                    <td>
                        <asp:DropDownList ID="sServiceID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" CssClass="DD" Width="150px" />
                        <cc1:ListSearchExtender TargetControlID='sCarTypeID' runat="server" ID="mCarIDS"
                            QueryPattern="Contains" PromptText='جستجو' />
                    </td>
                    <td>
                        اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="250px" AutoPostBack="True"
                            OnSelectedIndexChanged="sCarID_SelectedIndexChanged" />
                        <cc1:ListSearchExtender TargetControlID='sCarID' runat="server" ID="ListSearchExtender3"
                            QueryPattern="Contains" PromptText='جستجو' />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        صندوق
                    </td>
                    <td>
                        <asp:DropDownList ID="sFundID" runat="server" AutoPostBack="True" CssClass="DD" OnSelectedIndexChanged="sDriverID1_SelectedIndexChanged"
                            Width="150px" />
                    </td>
                    <td>
                        راننده اول / دوم
                    </td>
                    <td>
                        &nbsp;
                        <asp:DropDownList ID="sDriverID1" runat="server" AutoPostBack="True" CssClass="DD"
                            OnSelectedIndexChanged="sDriverID1_SelectedIndexChanged" Width="250px" />
                        <cc1:ListSearchExtender ID="ListSearchExtender1" runat="server" PromptText="جستجو"
                            QueryPattern="Contains" TargetControlID="sDriverID1" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                        &nbsp;<asp:Button ID="doCreateXML" runat="server" Text="ایجاد فایل XML پایانه" OnClick="doCreateXML_Click"
                            Width="150px" CssClass="CB"></asp:Button>&nbsp;<asp:Button ID="doDanaText" runat="server"
                                Text="بیمه دانا txt" OnClick="doDanaText_Click" Width="80px" CssClass="CB" Visible="false">
                            </asp:Button>&nbsp;<asp:Button ID="doDanaDat" runat="server" Text="بیمه دانا dat"
                                Visible="false" OnClick="doDanaDat_Click" Width="80px" CssClass="CB"></asp:Button>&nbsp;<asp:Button
                                    ID="doDanaXml" runat="server" Text="فایل XML بیمه دانا" OnClick="doDanaXml_Click"
                                    Width="150px" CssClass="CB"></asp:Button>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrint" runat="server" CssClass="CB" OnClick="doPrint_Click" Text="چاپ ريز صورت وضعیت"
                            Width="120px" Enabled="False" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrintToll" runat="server" CssClass="CB" OnClick="doPrintToll_Click"
                            Text="چاپ ریز عوارض" Width="100px" Enabled="False" />
                        &nbsp;
                        <asp:Button runat="server" ID="doPrintReception" CssClass="CB" Text="چاپ ریز پذیرایی" OnClick="doPrintReception_Click"/>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrintSumToll" runat="server" CssClass="CB" OnClick="doPrintSumToll_Click"
                            Text=" چاپ تجمیع عوارض" Width="100px" Enabled="False" />
                        &nbsp;<asp:Button ID="doWord" runat="server" CssClass="CB" OnClick="doWord_Click"
                            Text="ایجاد فایل ورد" Width="100px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrintInsurance" runat="server" CssClass="CB" OnClick="doPrintInsurance_Click"
                            Text="چاپ بيمه" Width="80px" Enabled="False" /><a href='#' class="CB" id="printBime" onclick='showHtml(event)'>چاپ
                                بيمه</a>
                        <input type="hidden" runat="server" id="printBimeQryDisabled" class="printBimeQryDisabled" />
                        <input type="hidden" runat="server" id="printBimeQry" class="printBimeQry" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrintAutoCar" runat="server" CssClass="CB" OnClick="doPrintAutoCar_Click"
                            Text="چاپ کارکرد اتوکار" Width="100px" Enabled="False" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrintDriver" runat="server" CssClass="CB" OnClick="doPrintDriver_Click"
                            Text="چاپ کارکرد راننده" Width="100px" Enabled="False" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doPrintAutoShare" runat="server" CssClass="CB" Enabled="False" OnClick="doPrintAutoShare_Click"
                            Text="چاپ پرداختیها " Width="100px" />
                    </td>
                    <td colspan="5">
                        <table cellpadding="0" cellspacing="2">
                            <tr>
                                <td>
                                    تغییر شماره ها:
                                </td>
                                <td>
                                    شماره صورت شروع
                                </td>
                                <td>
                                    <asp:TextBox ID="mNewNo" runat="server" CssClass="T" Width="150px" />
                                </td>
                                <td>
                                    سري صورت
                                </td>
                                <td>
                                    <table border="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo4" Width="76px" MaxLength="3" CssClass="T" />
                                            </td>
                                            <td>
                                                &nbsp;-&nbsp;
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo1" Width="30" MaxLength="3" CssClass="T" />
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo2" Width="30" MaxLength="3" CssClass="T" />
                                            </td>
                                            <td>
                                                /
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="mSeriesNo3" Width="30" MaxLength="3" CssClass="T" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="mKeepInvalids" Text='حفظ باطل شده ها' Checked="true" />
                                </td>
                                <td>
                                    <asp:Button ID="doRefineNew" runat="server" CssClass="CB" OnClick="doRefineNew_Click"
                                        Text="انجام شود " Width="100px" Enabled="false" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7">
                                    اگر شماره صورت شروع خالی باشد فقط سری صورت تغییر میکند
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:Label ID="mMsg" runat="server" CssClass="Err"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="Date" HeaderText="ساعت" />
                    <asp:BoundField DataField="ServiceID" HeaderText="مسير" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="AutoShare" HeaderText="پرداختی به راننده " />
                    <asp:BoundField DataField="ServiceID" HeaderText=" نوع سرويس دهی" />
                    <asp:BoundField DataField="CarID" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverID1" HeaderText="راننده1" />
                    <asp:BoundField DataField="ID" HeaderText="ت بليط" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">هيچ سرويس دوره ای در بازه مورد نظر وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <br />
            <asp:GridView ID="listE" runat="server" AutoGenerateColumns="False" OnRowDataBound="listE_RowDataBound"
                Style="margin-top: 0px" Width="95%" AllowPaging="False" OnPageIndexChanging="listE_PageIndexChanging"
                PageSize="20">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="DepartureDate" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime" HeaderText="ساعت" />
                    <asp:BoundField DataField="PathID" HeaderText="مسير" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Price" HeaderText="پرداختی به راننده " />
                    <asp:BoundField DataField="CarID" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverID1" HeaderText="راننده1" />
                    <asp:BoundField DataField="NumPassangers" HeaderText="ت مسافر" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">هيچ سرويس دربستی در بازه مورد نظر وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="doCreateXML" />
            <asp:PostBackTrigger ControlID="doDanaText" />
            <asp:PostBackTrigger ControlID="doDanaXml" />
            <asp:PostBackTrigger ControlID="doWord" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript" language="javascript">
        function showHtml(event) {
            //alert(1);
            //event.preventDefault();
            //alert(2);
            if ($('.printBimeQryDisabled').val() == "1")
                return; 
            var url = "insurancehtml.aspx?" + $('.printBimeQry').val();
            //alert(url);
            window.open(url, "Zweitfenster", "location=1,status=1,scrollbars=1,width=1000,height=600,left=10,top=10")
            return false;
        }
//        $(function() {
//            $('#printBime').click(function(event) {
//                event.preventDefault();
//                var url = "insurancehtml.aspx?" + $('.printBimeQry').val();
//                window.open(url, "Zweitfenster", "width=1000,height=600,left=100,top=200")
//            });
//        });
    </script>

</asp:Content>
