﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserActivity.aspx.cs" Inherits="Reports_UserActivity" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        متصدی
                    </td>
                    <td>
                        <asp:DropDownList ID="sUserID" runat="server" CssClass="DD" Width="150px" 
                            AutoPostBack="True" onselectedindexchanged="sUserID_SelectedIndexChanged" />
                    </td>
                    <td>
                        دفتر
                    </td>
                    <td>
                        <asp:DropDownList ID="sFundID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        از ساعت
                    </td>
                    <td>
                        <asp:TextBox ID="txtfdTime" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        تا ساعت
                    </td>
                    <td>
                        <asp:TextBox ID="txttdTime" runat="server"></asp:TextBox>
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="../wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:Button runat="server" ID="doPrint" Text="چاپ گزارش بدهکار و بستانکار" 
                CssClass="CB" OnClick="doPrint_Click" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ID" HeaderText="نام متصدی" />
                    <asp:BoundField DataField="ID" HeaderText="ماشين" />
                    <asp:BoundField DataField="ID" HeaderText="تاریخ" />
                    <asp:BoundField DataField="ID" HeaderText="ساعت" />
                    <asp:BoundField DataField="ID" HeaderText="ت بليط" />
                    <asp:BoundField DataField="ID" HeaderText="مبلغ فروش" />
                    <asp:BoundField DataField="ID" HeaderText="مبلخ تخفیف" />
                    <asp:BoundField DataField="ID" HeaderText="مسير" />
                    <asp:BoundField DataField="ID" HeaderText="چاپ ریز فروش" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جمع کل فروش:
                    </td>
                    <td>
                        <asp:Label ID="lblSumSale" runat="server" />
                    </td>
                    <td>
                        جمع کل تخفيف:
                    </td>
                    <td>
                        <asp:Label ID="lblSumDiscount" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
