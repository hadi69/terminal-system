﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Reports_UserActivity : System.Web.UI.Page
{
    int sumSale = 0;
    int sumDiscount = 0;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Service> all = Helper.Instance.GetServices();
            Service none = new Service();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

        }
        {
            List<Branch> all = Helper.Instance.GetBranches();
            Branch none = new Branch();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sFundID.DataValueField = "ID";
            sFundID.DataTextField = "Title";
            sFundID.DataSource = all;
            sFundID.DataBind();
        }
        {
            List<User> all = Helper.Instance.GetUsers();
            User none = new User();
            none.ID = Null.NullInteger;
            all.Insert(0, none);
            sUserID.DataValueField = "ID";
            sUserID.DataTextField = "FullName";
            sUserID.DataSource = all;
            sUserID.DataBind();
        }
        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        if (Null.NullDate != Tool.GetDate(Session["UserActivity.DateStart"]))
            sDateStart.SelectedDate = Tool.GetDate(Session["UserActivity.DateStart"]);
        if (Null.NullDate != Tool.GetDate(Session["UserActivity.DateEnd"]))
            sDateEnd.SelectedDate = Tool.GetDate(Session["UserActivity.DateEnd"]);
        try { sUserID.SelectedIndex = (int)Session["UserActivity.UserID"]; }
        catch { };
        try { sFundID.SelectedIndex = (int)Session["UserActivity.FundID"]; }
        catch { };

        if (Null.NullString != Tool.GetString(Session["UserActivity.fdTime"]))
            txtfdTime.Text = Tool.GetString(Session["UserActivity.fdTime"]);
        if (Null.NullString != Tool.GetString(Session["UserActivity.tdTime"]))
            txttdTime.Text = Tool.GetString(Session["UserActivity.tdTime"]);
    }
    class Temp : IComparable<Temp>
    {
        public int ID { get; set; }
        public User User { get; set; }
        public Trip Trip { get; set; }

        #region IComparable<Temp> Members

        public int CompareTo(Temp other)
        {
            int res = this.User.FullName.CompareTo(other.User.FullName);
            if (res != 0)
                return res;
            res = this.Trip.Date.CompareTo(other.Trip.Date);
            if (res == 0)
                return this.Trip.DepartureTime2.CompareTo(other.Trip.DepartureTime2);
            return res;
        }

        #endregion
    }
    private List<Trip> GetTrips()
    {
        var all = from c in Helper.Instance.DB.Trips
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;
        if (fromDate != new DateTime())
            all = all.Where(c => c.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;
        if (toDate != new DateTime())
            all = all.Where(c => c.Date <= toDate);


        return all.ToList();
    }
    private List<Temp> GetTemps()
    {
        int fdTime = txtfdTime.Text.Length > 0 ? int.Parse(txtfdTime.Text.Split(':')[0]) * 60 + int.Parse(txtfdTime.Text.Split(':')[1]) : 0;
        int tdTime = txttdTime.Text.Length > 0 ? int.Parse(txttdTime.Text.Split(':')[0]) * 60 + int.Parse(txttdTime.Text.Split(':')[1]) : 1440; //24*60+0

        List<User> userIDs = new List<User>();
        if (sUserID.SelectedIndex != 0)
            userIDs.Add(Helper.Instance.GetUser(Tool.GetInt(sUserID.SelectedValue, Null.NullInteger)));
        else
            userIDs.AddRange(Helper.Instance.GetUsers());

        List<Temp> res = new List<Temp>();
        List<Trip> trips = GetTrips();
        int fundID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);
        for (int i = 0; i < userIDs.Count; i++)
        {
            foreach (Trip trip in trips)
            {
                if (Helper.Instance.TripHasUser(trip, userIDs[i].ID,fundID,fdTime,tdTime))
                    res.Add(new Temp { ID = trip.ID, Trip = trip, User = userIDs[i] });

            }
        }
        res.Sort();
        return res;
    }
  
    private void BindData()
    {
        sumSale = sumDiscount = 0;
        list.DataSource = GetTemps();
        list.DataBind();
        lblSumSale.Text = sumSale.ToString();
        lblSumDiscount.Text = sumDiscount.ToString();
//        lblSumPureSale.Text = (sumSale - sumDiscount).ToString();
        Session["UserActivity.DateStart"] = sDateStart.SelectedDate;
        Session["UserActivity.DateEnd"] = sDateEnd.SelectedDate;
        Session["UserActivity.UserID"] = sUserID.SelectedIndex;
        Session["UserActivity.FundID"] = sFundID.SelectedIndex;
        Session["UserActivity.fdTime"] = txtfdTime.Text;
        Session["UserActivity.tdTime"] = txttdTime.Text;
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Temp info = e.Row.DataItem as Temp;
            int fundID = Tool.GetInt(sFundID.SelectedValue, Null.NullInteger);

            if (info != null)
            {
                e.Row.Cells[0].Text = info.Trip.ID.ToString();// info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = string.Format("<a href='../Tickets.aspx?tripid={0}&userid={1}'>{2}</a>", info.Trip.ID, info.User.ID, info.User.FullName);
                try
                {
                    e.Row.Cells[3].Text = Helper.Instance.GetCarTitle(info.Trip.CarID);
                }
                catch { }
                e.Row.Cells[4].Text = Tool.ToPersianDate(info.Trip.Date, "");
                e.Row.Cells[5].Text = info.Trip.DepartureTime2;
                e.Row.Cells[6].Text = Helper.Instance.GetNumTickets(info.Trip.ID, info.User.ID, fundID).ToString();
                e.Row.Cells[7].Text = Helper.Instance.GetPrice(info.Trip.ID, info.User.ID, fundID).ToString();
                sumSale = sumSale + int.Parse(e.Row.Cells[7].Text);
                e.Row.Cells[8].Text = Helper.Instance.GetDiscount(info.Trip.ID, info.User.ID, fundID).ToString();
                sumDiscount = sumDiscount + int.Parse(e.Row.Cells[8].Text);
                e.Row.Cells[9].Text = info.Trip.Service.Path.Title;
                e.Row.Cells[10].Text = string.Format("<a href='ReportSale.aspx?tripid={0}&userid={1}&repname=UsersSale' target=_blank>{2}</a>", info.Trip.ID.ToString(), info.User.ID, "چاپ ریز فروش");
            }
        }
    }

    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void doPrint_Click(object sender, EventArgs e)
    {

        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        string fDate = "";
        if (fromDate != new DateTime())
            fDate = fromDate.ToShortDateString();

        string tDate = "";
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            tDate = toDate.ToShortDateString();
        int specialID = 0;
        int serviceID = 0;
        int driverID1 = 0;
        int carTypeID = 0;
        int carID = 0;
        int destID = 0;
        int fundID = 0;
        int fno = 0;
        int tno = 0;

        string repTitle = "", fullName = "";
        repTitle = txtfdTime.Text == "" ? "00:00" : txtfdTime.Text;
        fullName = txttdTime.Text == "" ? "24:00" : txttdTime.Text;
        string series = "";

        Response.Redirect(
            string.Format("ReportNew.aspx?fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}&repname={13}&repTitle={14}&fullName={15}&repParam={16}",
            fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, sUserID.SelectedValue.ToString(), "UserSale", repTitle, fullName, ""), true);

        //string trips = "";
        //for (int i = 0; i < list.Rows.Count; i++)
        //{
        //    if (trips.Length > 0)
        //        trips = trips + ",";
        //    trips = trips + "'"+list.Rows[i].Cells[0].Text+"'";
        //}
        //if(sUserID.SelectedIndex >0)
        //    Response.Redirect(string.Format("ReportSale.aspx?tripid={0}&userid={1}&repname={2}", trips,sUserID.SelectedValue.ToString(),"UserSale"), true);


    }
    protected void sUserID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (list.Rows.Count > 0) //sUserID.SelectedIndex > 0 && 
        //    doPrint.Enabled = true;
        //else
        //    doPrint.Enabled = false;
    }
}
