﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
public partial class ReportNew : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //tripid={0}&userid
        string fromDate = Tool.GetString(Request.QueryString["fromDate"], "");
        string toDate = Tool.GetString(Request.QueryString["toDate"], "");
        int specialID = Tool.GetInt(Request.QueryString["specialID"], 0);
        int serviceID = Tool.GetInt(Request.QueryString["serviceID"], 0);
        int driverID = Tool.GetInt(Request.QueryString["driverID"], 0);
        int carTypeID = Tool.GetInt(Request.QueryString["carTypeID"], 0);
        int carID = Tool.GetInt(Request.QueryString["carID"], 0);
        int destID = Tool.GetInt(Request.QueryString["destID"], 0);
        int fundID = Tool.GetInt(Request.QueryString["fundID"], 0);
        int fno = Tool.GetInt(Request.QueryString["fno"], 0);
        int tno = Tool.GetInt(Request.QueryString["tno"], 0);
        string series = Tool.GetString(Request.QueryString["series"], "");
        int userid = Tool.GetInt(Request.QueryString["userid"], 0);
        if (userid <= 0)
            userid = 0;
        string repname = Tool.GetString(Request.QueryString["repname"], "");
        string repTitle = Tool.GetString(Request.QueryString["repTitle"], "");
        string fullName = Tool.GetString(Request.QueryString["fullName"], "");
        string repParam = Tool.GetString(Request.QueryString["repParam"], "");
        if (!string.IsNullOrEmpty(toDate))
        {

            try
            {
                toDate = Convert.ToDateTime(toDate).AddDays(1).ToShortDateString();
            }
            catch { }
        }
        //if (!string.IsNullOrEmpty(fromDate))
        //{
        //    fromDate = fromDate + " 00:00:00";
        //}
        LoadReport(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series, userid, repname, repTitle, fullName, repParam);

        if (repname.Contains("Cartable_"))
            doReturn.NavigateUrl = "Cartable2.aspx";
        else if (repname.Contains("Cartable"))
            doReturn.NavigateUrl = "Cartable.aspx";
        else if (repname == "Car")
            doReturn.NavigateUrl = "~/Staff/Cars.aspx";
        else if (repname == "DriverList")
            doReturn.NavigateUrl = "~/Staff/Drivers.aspx";
        else if (repname == "PayGet")
            doReturn.NavigateUrl = "~/Funds/PayGet.aspx";
        else if (repname == "UserSale")
            doReturn.NavigateUrl = "UserActivity.aspx";
        else
            doReturn.NavigateUrl = "SuratVaziatSummary.aspx";


    }
    public void LoadReport(string fromDate, string toDate, int specialID, int serviceID, int driverID, int carTypeID, int carID, int destID, int fundID, int fno, int tno, string series, int userid, string repname, string repTitle, string fullName, string repParam)
    {
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportDataSource dsLoanList = null;
        //ReportDataSource dsUserList = null;
        if (repname == "UserSale")
            dsLoanList = new ReportDataSource("UserSale", Helper.Instance.GetUserSale(fromDate, toDate, repTitle, fullName, userid));
        //else if (repname == "UsersSale")
        //    dsLoanList = new ReportDataSource("UsersSale", Helper.Instance.GetUsersSale(int.Parse(tripid)));
        else if (repname.Contains("Cartable_"))
            dsLoanList = new ReportDataSource("Cartable", Helper.Instance.GetCartable2(fromDate, toDate, serviceID, userid, series));
        else if (repname.Contains("Cartable"))
            dsLoanList = new ReportDataSource("Cartable", Helper.Instance.GetCartable(fromDate, toDate, serviceID, userid, series, destID));

        //    dsLoanList = new ReportDataSource("Cartable", Helper.Instance.GetCartable(tripid, userid));
        else if ((repname == "SuratVaziatSummary") || (repname == "Toll2") || (repname == "SumToll2") || (repname == "AutoCar") || (repname == "Driver") || (repname == "AutoShare") || (repname=="Reception"))
            dsLoanList = new ReportDataSource("SuratVaziatSummary", Helper.Instance.GetSuratVaziatSummary(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series));
        else if (repname == "Insurance")
            dsLoanList = new ReportDataSource("SuratVaziatSummary", Helper.Instance.GetInsuranceSummarySorted(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series));
        //dsLoanList = new ReportDataSource("SuratVaziatSummary", Helper.Instance.GetInsuranceSummarySorted(fromDate, toDate, specialID, serviceID, driverID, carTypeID, carID, destID, fundID, fno, tno, series));
        else if (repname == "Car")
            dsLoanList = new ReportDataSource("Car", Helper.Instance.GetCars());
        else if (repname == "DriverList")
            dsLoanList = new ReportDataSource("Driver", Helper.Instance.GetDrivers());



        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        if (repname == "UserSale")
            ReportViewer1.LocalReport.ReportPath = "Reports/UserSale.rdlc";
        else if (repname == "UsersSale")
            ReportViewer1.LocalReport.ReportPath = "Reports/UsersSale.rdlc";
        else if (repname == "Cartable")
            ReportViewer1.LocalReport.ReportPath = "Reports/Cartable.rdlc";
        else if (repname == "Cartable2")
            ReportViewer1.LocalReport.ReportPath = "Reports/Cartable2.rdlc";
        else if (repname == "Cartable_2")
            ReportViewer1.LocalReport.ReportPath = "Reports/Cartable_2.rdlc";
        else if (repname == "Cartable_22")
            ReportViewer1.LocalReport.ReportPath = "Reports/Cartable_22.rdlc";
        else if (repname == "SuratVaziatSummary")
            ReportViewer1.LocalReport.ReportPath = "Reports/SuratVaziatSummary.rdlc";
        else if (repname == "Toll2")
            ReportViewer1.LocalReport.ReportPath = "Reports/Toll2.rdlc";
        else if (repname == "Reception")
            ReportViewer1.LocalReport.ReportPath = "Reports/Reception.rdlc";
        else if (repname == "SumToll2")
            ReportViewer1.LocalReport.ReportPath = "Reports/SumToll2.rdlc";
        else if (repname == "Insurance")
        {
            bool sortDNT = Tool.GetBool(Helper.Instance.GetSettingValue("SortDNT"), true);
            if (sortDNT)
                ReportViewer1.LocalReport.ReportPath = "Reports/InsuranceDNT.rdlc";
            else
                ReportViewer1.LocalReport.ReportPath = "Reports/Insurance.rdlc";
        }
        else if ((repname == "AutoCar") || (repname == "Driver"))
            ReportViewer1.LocalReport.ReportPath = "Reports/AutoCar.rdlc";
        else if (repname == "AutoShare")
            ReportViewer1.LocalReport.ReportPath = "Reports/AutoShare.rdlc";
        else if (repname == "PayGet")
            ReportViewer1.LocalReport.ReportPath = "Reports/PayGet.rdlc";
        else if (repname == "Car")
            ReportViewer1.LocalReport.ReportPath = "Reports/Car.rdlc";
        else if (repname == "DriverList")
            ReportViewer1.LocalReport.ReportPath = "Reports/Driver.rdlc";




        ReportParameter[] paramss = null;
        if (repname == "UserSale")
        {
            paramss = new ReportParameter[6];
            paramss[0] = new ReportParameter("fromDate", fromDate, false);
            paramss[1] = new ReportParameter("toDate", toDate, false);
            paramss[2] = new ReportParameter("fdTime", repTitle, false);
            paramss[3] = new ReportParameter("tdTime", fullName, false);
            paramss[4] = new ReportParameter("userID", userid.ToString(), false);
            paramss[5] = new ReportParameter("FullName", Helper.Instance.GetUserFullName(userid), false);
        }
        else if (repname.Contains("Cartable"))
        {
            paramss = new ReportParameter[6];
            paramss[0] = new ReportParameter("fromDate", fromDate, false);
            paramss[1] = new ReportParameter("toDate", toDate, false);
            paramss[2] = new ReportParameter("serviceID", serviceID.ToString(), false);
            paramss[3] = new ReportParameter("userID", userid.ToString(), false);
            paramss[4] = new ReportParameter("FullName", Helper.Instance.GetUserFullName(userid), false);
            paramss[5] = new ReportParameter("series", series, false);
        }
        //else if ((repname == "UsersSale"))
        //{
        //    paramss = new ReportParameter[1];
        //    paramss[0] = new ReportParameter("tripID", tripid, false);
        //}
        else if ((repname == "SuratVaziatSummary") || (repname == "Toll2") || (repname == "SumToll2") || (repname=="Reception"))
        {
            paramss = new ReportParameter[12];
            paramss[0] = new ReportParameter("fromDate", fromDate, false);
            paramss[1] = new ReportParameter("toDate", toDate, false);
            paramss[2] = new ReportParameter("specialID", specialID.ToString(), false);
            paramss[3] = new ReportParameter("serviceID", serviceID.ToString(), false);
            paramss[4] = new ReportParameter("driverID", driverID.ToString(), false);
            paramss[5] = new ReportParameter("carTypeID", carTypeID.ToString(), false);
            paramss[6] = new ReportParameter("carID", carID.ToString(), false);
            paramss[7] = new ReportParameter("destID", destID.ToString(), false);
            paramss[8] = new ReportParameter("fundID", fundID.ToString(), false);
            paramss[9] = new ReportParameter("fno", fno.ToString(), false);
            paramss[10] = new ReportParameter("tno", tno.ToString(), false);
            paramss[11] = new ReportParameter("series", series, false);
        }
        //else if (repname == "Insurance")
        //{
        //    paramss = new ReportParameter[3];
        //    paramss[0] = new ReportParameter("tripID", tripid, false);
        //    paramss[1] = new ReportParameter("FullName", Helper.Instance.GetSettingValue("CompanyName"), false);
        //    paramss[2] = new ReportParameter("addEmptyItems", "False", false);
        //}
        else if ((repname == "AutoCar") || (repname == "Driver") || (repname == "AutoShare") || (repname == "Insurance"))
        {
            paramss = new ReportParameter[15];
            paramss[0] = new ReportParameter("fromDate", fromDate, false);
            paramss[1] = new ReportParameter("toDate", toDate, false);
            paramss[2] = new ReportParameter("specialID", specialID.ToString(), false);
            paramss[3] = new ReportParameter("serviceID", serviceID.ToString(), false);
            paramss[4] = new ReportParameter("driverID", driverID.ToString(), false);
            paramss[5] = new ReportParameter("carTypeID", carTypeID.ToString(), false);
            paramss[6] = new ReportParameter("carID", carID.ToString(), false);
            paramss[7] = new ReportParameter("destID", destID.ToString(), false);
            paramss[8] = new ReportParameter("fundID", fundID.ToString(), false);
            paramss[9] = new ReportParameter("fno", fno.ToString(), false);
            paramss[10] = new ReportParameter("tno", tno.ToString(), false);
            paramss[11] = new ReportParameter("series", series, false);
            paramss[12] = new ReportParameter("RepTitle", repTitle, false);
            paramss[13] = new ReportParameter("RepParam", repParam, false);
            paramss[14] = new ReportParameter("FullName", fullName, false);
        }
        //else if (repname == "PayGet")
        //{
        //    paramss = new ReportParameter[4];
        //    paramss[0] = new ReportParameter("paymentID", tripid, false);
        //    paramss[1] = new ReportParameter("RepTitle", repTitle, false);
        //    paramss[2] = new ReportParameter("RepParam", repParam, false);
        //    paramss[3] = new ReportParameter("FullName", fullName, false);
        //}
        if (repname != "Car" && repname != "DriverList")
            ReportViewer1.LocalReport.SetParameters(paramss);
        ReportViewer1.LocalReport.DataSources.Add(dsLoanList);

        ReportViewer1.LocalReport.Refresh();
    }
}
