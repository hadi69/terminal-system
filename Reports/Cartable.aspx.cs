﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Cartable : System.Web.UI.Page
{
    private int sumCommission, sumCommissionE;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Service> all = Helper.Instance.GetServices();
            Service none = new Service();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sServiceID.DataValueField = "ID";
            sServiceID.DataTextField = "Title";
            sServiceID.DataSource = all;
            sServiceID.DataBind();

            List<string> all1 = (from h in Helper.Instance.DB.Trips
                                select h.Series).ToList();
            all1.Insert(0, "");

            List<string> all2 = (from hh in Helper.Instance.DB.Exclusives
                                 select hh.Series).ToList();
            if (all2 != null)
                all1.AddRange(all2);

            sSeries.DataSource = all1.Distinct();
            sSeries.DataBind();

        }
        {
            List<Branch> all = Helper.Instance.GetBranches();
            Branch none = new Branch();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

        }
        {
            List<City> allCities = Helper.Instance.GetCities(true);
            City none=new City();
            none.ID = Null.NullInteger;
            allCities.Insert(0,none);

            sDestCity.DataValueField = "ID";
            sDestCity.DataTextField = "Title";
            sDestCity.DataSource = allCities;
            sDestCity.DataBind();
        }
        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;
    }
    class Temp
    {
        public int ID { get; set; }
        public User User { get; set; }
        public Trip Trip { get; set; }
    }
    class ETemp
    {
        public int ID { get; set; }
        public User User { get; set; }
        public Exclusive Exclusive  { get; set; }
    }
    private List<Trip> GetTrips()
    {
        var all = from c in Helper.Instance.DB.Trips
                  where c.Closed == true
                  select c ;

        DateTime fromDate = sDateStart.SelectedDate;
        if (fromDate != new DateTime())
            all = all.Where(c => c.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;
        if (toDate != new DateTime())
            all = all.Where(c => c.Date <= toDate);
        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.ServiceID == sID);
        }
        if (sSeries.SelectedIndex > 0)
            all = all.Where(c => c.Series == Tool.ParseSeries(sSeries.Text.Trim()));
        if (sDestCity.SelectedIndex>0)
        {
            int cID = Tool.GetInt(sDestCity.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.Service.Path.DestCityID == cID);
        }


        all = all.OrderBy(c => c.Date);
        all = all.OrderBy(c => c.DepartureTime);
        all = all.OrderBy(c => c.No);
        return all.ToList();
    }
    private List<Exclusive> GetETrips()
    {
        var all = from c in Helper.Instance.DB.Exclusives
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;
        if (fromDate != new DateTime())
            all = all.Where(c => c.DepartureDate>= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;
        if (toDate != new DateTime())
            all = all.Where(c => c.DepartureDate <= toDate);

        if (sSeries.SelectedIndex > 0)
            all = all.Where(c => c.Series == Tool.ParseSeries(sSeries.Text.Trim()));
        if (sDestCity.SelectedIndex > 0)
        {
            int cID = Tool.GetInt(sDestCity.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.Path.DestCityID == cID);
        }

        all = all.OrderBy(c => c.DepartureDate);
        all = all.OrderBy(c => c.DepartureTime);
        all = all.OrderBy(c => c.No);
        return all.ToList();
    }
    private List<Temp> GetTemps()
    {
        List<User> userIDs = new List<User>();
        userIDs.Add(Helper.Instance.GetUser(SiteSettings.UserID));

        List<Temp> res = new List<Temp>();
        List<Trip> trips = GetTrips();
        //for (int i = 0; i < userIDs.Count; i++)
        //{
        //    foreach (Trip trip in trips)
        //    {
        //        if (Helper.Instance.TripHasUser(trip, userIDs[i].ID, Null.NullInteger))
        //            res.Add(new Temp { ID = trip.ID, Trip = trip, User = userIDs[i] });

        //    }
        //}
        foreach (Trip trip in trips)
        {
            res.Add(new Temp { ID = trip.ID, Trip = trip, User = null });
        }

        return res;
    }

    private List<ETemp> GetETemps()
    {
        List<User> userIDs = new List<User>();
        userIDs.Add(Helper.Instance.GetUser(SiteSettings.UserID));

        List<ETemp> res = new List<ETemp>();
        List<Exclusive> exclusives = GetETrips();
        //for (int i = 0; i < userIDs.Count; i++)
        //{
        //    foreach (Trip trip in trips)
        //    {
        //        if (Helper.Instance.TripHasUser(trip, userIDs[i].ID, Null.NullInteger))
        //            res.Add(new Temp { ID = trip.ID, Trip = trip, User = userIDs[i] });

        //    }
        //}
        foreach (Exclusive exclusive in exclusives)
        {
            res.Add(new ETemp { ID = exclusive.ID,  Exclusive= exclusive, User = null });
        }

        return res;
    }
    private void BindData()
    {
        sumCommission = 0;
        sumCommissionE = 0;
        list.DataSource = GetTemps();
        list.DataBind();
        lblSumCommission.Text = sumCommission.ToString();


        listE.DataSource = GetETemps();
        listE.DataBind();
        lblSumCommissionE.Text = sumCommissionE.ToString();
        
        //string trips = "";
        //for (int i = 0; i < list.Rows.Count; i++)
        //{
        //    if (trips.Length > 0)
        //        trips = trips + ",";
        //    trips = trips + "'" + list.Rows[i].Cells[0].Text + "'";
        //}
        //doPrint.NavigateUrl = string.Format("ReportSale.aspx?tripid={0}&userid={1}&repname={2}", trips, SiteSettings.UserID.ToString(), "Cartable");

    }


    protected void listE_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ETemp info = e.Row.DataItem as ETemp;


            if (info != null)
            {
                e.Row.Cells[0].Text = info.Exclusive.ID.ToString();// info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(info.Exclusive.DepartureDate, "");
                e.Row.Cells[3].Text = info.Exclusive.DepartureTime;
                e.Row.Cells[4].Text = info.Exclusive.Path.City1.Title;
                e.Row.Cells[5].Text = info.Exclusive.No.ToString();
                e.Row.Cells[6].Text = info.Exclusive.Commission.ToString();
                sumCommissionE += Tool.GetInt(e.Row.Cells[6].Text, 0);
            }
        }
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Temp info = e.Row.DataItem as Temp;


            if (info != null)
            {
                e.Row.Cells[0].Text = info.Trip.ID.ToString();// info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(info.Trip.Date, "");
                e.Row.Cells[3].Text = info.Trip.Service.DepartureTime;
                e.Row.Cells[4].Text = info.Trip.Service.Path.Title;
                e.Row.Cells[5].Text = info.Trip.No.ToString();
                e.Row.Cells[6].Text = info.Trip.Comission2.ToString();
                sumCommission += Tool.GetInt(e.Row.Cells[6].Text, 0);
            }
        }
    }

    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void doPrint_Click(object sender, EventArgs e)
    {
        //string trips = "";
        //for (int i = 0; i < list.Rows.Count; i++)
        //{
        //    if (trips.Length > 0)
        //        trips = trips + ",";
        //    trips = trips + "'" + list.Rows[i].Cells[0].Text + "'";
        //}
        //Response.Redirect(string.Format("ReportSale.aspx?tripid={0}&userid={1}&repname={2}", trips, SiteSettings.UserID.ToString(), "Cartable"), true);
        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        string fDate = "";
        if (fromDate != new DateTime())
            fDate = fromDate.ToShortDateString();

        string tDate = "";
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            tDate = toDate.ToShortDateString();
        int specialID = 0;
        int serviceID = 0;
        if (sServiceID.SelectedIndex > 0)
            serviceID = Tool.GetInt(sServiceID.SelectedValue, 0);
        int driverID1 = 0;
        int carTypeID = 0;
        int carID = 0;
        int destID = Tool.GetInt(sDestCity.SelectedValue,0);
        int fundID = 0;
        int fno = 0;
        int tno = 0;

        string series = "";
        if (sSeries.Text.Trim().Length > 0)
            series = sSeries.Text;

        Response.Redirect(
            string.Format("ReportNew.aspx?fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}&repname={13}&repTitle={14}&fullName={15}&repParam={16}",
            fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, 0, "Cartable", "", "", ""), true);

    }
    protected void doPrint2_Click(object sender, EventArgs e)
    {
        //string trips = "";
        //for (int i = 0; i < list.Rows.Count; i++)
        //{
        //    if (trips.Length > 0)
        //        trips = trips + ",";
        //    trips = trips + "'" + list.Rows[i].Cells[0].Text + "'";
        //}
        //Response.Redirect(string.Format("ReportSale.aspx?tripid={0}&userid={1}&repname={2}", trips, SiteSettings.UserID.ToString(), "Cartable"), true);
        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        string fDate = "";
        if (fromDate != new DateTime())
            fDate = fromDate.ToShortDateString();

        string tDate = "";
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            tDate = toDate.ToShortDateString();
        int specialID = 0;
        int serviceID = 0;
        if (sServiceID.SelectedIndex > 0)
            serviceID = Tool.GetInt(sServiceID.SelectedValue, 0);
        int driverID1 = 0;
        int carTypeID = 0;
        int carID = 0;
        int destID = Tool.GetInt(sDestCity.SelectedValue,0);
        int fundID = 0;
        int fno = 0;
        int tno = 0;

        string series = "";
        if (sSeries.Text.Trim().Length > 0)
            series = sSeries.Text;

        Response.Redirect(
            string.Format("ReportNew.aspx?fromDate={0}&toDate={1}&specialID={2}&serviceID={3}&driverID={4}&carTypeID={5}&carID={6}&destID={7}&fundID={8}&fno={9}&tno={10}&series={11}&userid={12}&repname={13}&repTitle={14}&fullName={15}&repParam={16}",
            fDate, tDate, specialID, serviceID, driverID1, carTypeID, carID, destID, fundID, fno, tno, series, 0, "Cartable2", "", "", ""), true);

    }
    protected void doWord_Click(object sender, EventArgs e)
    {
        List<Temp> temps = GetTemps();
        List<ETemp> eTemps = GetETemps();
        if (temps.Count == 0 && eTemps.Count == 0)
            return;
        List<int> dates = new List<int>();
        for (int i = 0; i < temps.Count; i++)
        {
            int date = temps[i].Trip.Date.Year * 10000 + temps[i].Trip.Date.Month * 100 + temps[i].Trip.Date.Day;
            if (!dates.Contains(date))
                dates.Add(date);
        }
        for (int i = 0; i < eTemps.Count; i++)
        {
            int date = eTemps[i].Exclusive.DepartureDate.Year * 10000 + eTemps[i].Exclusive.DepartureDate.Month * 100 + eTemps[i].Exclusive.DepartureDate.Day;
            if (!dates.Contains(date))
                dates.Add(date);
        }
        dates.Sort();

        //string topRow = string.Format("گزارش کمسیون از تاریخ {0} تا {1}", Tool.ToPersianDate(sDateStart.SelectedDate, ""), Tool.ToPersianDate(sDateEnd.SelectedDate, ""));
        //ExportTool.ToWord(Response, list, topRow);


        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=numFiles.doc");
        Response.Charset = "Windows-1256";
        Response.ContentType = "application/vnd.doc";


        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        decimal sumAll = 0;
        for (int d = 0; d < dates.Count; d++)
        {
            int count = 1;
            decimal sum = 0;
            for (int i = 0; i < temps.Count; i++)
            {
                Temp info = temps[i];
                int date = temps[i].Trip.Date.Year * 10000 + temps[i].Trip.Date.Month * 100 + temps[i].Trip.Date.Day;
                if (dates[d] != date)
                    continue;
                if (count == 1)
                {
                    stringWrite.Write("<p style='page-break-before:always'>");
                    stringWrite.Write("<table  style='page-break-before:always;width:12cm' border='1' cellpadding='0' cellspacing='0' dir='rtl'>");
                    stringWrite.Write("<tr><td colspan='6'>");
                    stringWrite.Write(string.Format("گزارش کمسیون تاریخ {0}", Tool.ToPersianDate(info.Trip.Date, "")));
                    stringWrite.Write("</td></tr>");
                    // ---------------------
                    // Header
                    #region Header
                    stringWrite.Write("<tr>");
                    stringWrite.Write("<td>رديف</td>");
                    stringWrite.Write("<td>تاریخ</td>");
                    stringWrite.Write("<td>تاریخ</td>");
                    stringWrite.Write("<td>مسير</td>");
                    stringWrite.Write("<td>شماره صورت</td>");
                    stringWrite.Write("<td>کمسیون</td>");
                    stringWrite.Write("<td>متفرقه</td>");
                    stringWrite.Write("<td>حق تمبر صورت</td>");
                    stringWrite.Write("</tr>");
                    #endregion
                }
                stringWrite.Write("<tr>");
                stringWrite.Write(string.Format("<td>{0}</td>", count));
                stringWrite.Write(string.Format("<td>{0}</td>",  Tool.ToPersianDate(info.Trip.Date, "")));
                stringWrite.Write(string.Format("<td>{0}</td>",  info.Trip.Service.DepartureTime));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Trip.Service.Path.Title));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Trip.No.ToString()));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Trip.Comission2.ToString()));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Trip.Others.ToString()));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Trip.Stamp.ToString()));
                stringWrite.Write("</tr>");
                count++;
                if (info.Trip.Comission2.HasValue)
                {
                    sum += info.Trip.Comission2.Value;
                    sumAll += info.Trip.Comission2.Value;
                }
            }
            for (int i = 0; i < eTemps.Count; i++)
            {
                ETemp info = eTemps[i];
                int date = info.Exclusive.DepartureDate.Year * 10000 + info.Exclusive.DepartureDate.Month * 100 + info.Exclusive.DepartureDate.Day;
                if (dates[d] != date)
                    continue;
                if (count == 1)
                {
                    stringWrite.Write("<p style='page-break-before:always'>");
                    stringWrite.Write("<table  style='page-break-before:always' border='1' cellpadding='0' cellspacing='0' dir='rtl' width=100%>");
                    stringWrite.Write("<tr><td colspan='6'>");
                    stringWrite.Write(string.Format("گزارش کمسیون تاریخ {0}", Tool.ToPersianDate(info.Exclusive.DepartureDate, "")));
                    stringWrite.Write("</td></tr>");
                    // ---------------------
                    // Header
                    #region Header
                    stringWrite.Write("<tr>");
                    stringWrite.Write("<td>رديف</td>");
                    stringWrite.Write("<td>تاریخ</td>");
                    stringWrite.Write("<td>تاریخ</td>");
                    stringWrite.Write("<td>مسير</td>");
                    stringWrite.Write("<td>شماره صورت</td>");
                    stringWrite.Write("<td>کمسیون</td>");
                    stringWrite.Write("</tr>");
                    #endregion
                }
                stringWrite.Write("<tr>");
                stringWrite.Write(string.Format("<td>{0}</td>", count));
                stringWrite.Write(string.Format("<td>{0}</td>", Tool.ToPersianDate(info.Exclusive.DepartureDate, "")));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Exclusive.DepartureTime));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Exclusive.Path.Title));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Exclusive.No.ToString()));
                stringWrite.Write(string.Format("<td>{0}</td>", info.Exclusive.Commission.ToString()));
                stringWrite.Write("</tr>");
                count++;
                //if (info.Exclusive.Commission.HasValue)
                {
                    sum += info.Exclusive.Commission;
                    sumAll += info.Exclusive.Commission;
                }
            }
            stringWrite.Write("<tr><td></td><td></td><td></td><td></td><td>جمع</td>");
            stringWrite.Write(string.Format("<td>{0}</td></tr>", sum));
            stringWrite.Write("</table>");
            stringWrite.Write("</p>");
        }

        stringWrite.Write("<p style='page-break-before:always'>");
        stringWrite.Write("<table  border='1' cellpadding='0' cellspacing='0' dir='rtl' width=100%>");
        stringWrite.Write("<tr><td colspan='2'>");
        stringWrite.Write(string.Format("گزارش کمسیون از تاریخ {0} تا {1}", Tool.ToPersianDate(sDateStart.SelectedDate, ""), Tool.ToPersianDate(sDateEnd.SelectedDate, "")));
        stringWrite.Write("</td></tr>");
        stringWrite.Write(string.Format("<tr><td>جمع کل:</td><td>{0}</td></tr>", sumAll));
        stringWrite.Write("</table>");
        stringWrite.Write("</p>");
       
        // changed for Unicode
        // Response.Write(stringWrite.ToString());
        byte[] converted = System.Text.Encoding.GetEncoding("Windows-1256").GetBytes(stringWrite.ToString());
        Response.Write("<html><head> <meta charset='windows-1256'></head><body>");
        if (converted.Length > 0)
            Response.BinaryWrite(converted);
        else
        {

        }
        Response.Write("</body></html>");

    }
}
