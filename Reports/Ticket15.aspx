﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ticket15.aspx.cs" Inherits="Reports_Ticket15" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <object id="OurActiveX" name='OurActiveX' classid="clsid:DE6CDCC5-F2F4-455f-B782-1D1F78EBDAF8"
        viewastext codebase="MyPrinter.cab">
    </object>

    <script language="javascript" type="text/javascript">
        function DoPrint()
        {
           try
	        {
                //alert('Set Printer ');
		        document.OurActiveX.InitPrinter(210, 70);
                //alert('done');
	        }
	        catch(Err)
	        {
		        alert(Err.description);
	        }
	        print();
	        window.close();
        }
    </script>

    <link href="../PrintStyle.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0px;" onload="javascript:DoPrint()">
    <form id="form1" runat="server" style="width: 21cm; height: 6.5cm">
    &nbsp;
    <asp:Label ID="lblNo" runat="server" class='printlbl' Style="position: absolute;
        top: 3px; left: 414px;" />
    <asp:Label Style="position: absolute; top: 40px; left: 25px;" ID="lblPrice" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 37px; left: 341px;" ID="lblFullName" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 74px; left: 202px;" ID="lblDay" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 74px; left: 279px;" ID="lblDate" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 74px; left: 398px;" ID="lblDepartureTime"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 107px; left: 385px;" ID="lblSrcCity" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 141px; left: 384px;" ID="lblDestCity"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 165px; left: 288px;" ID="lblChairsNo"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 166px; left: 454px;" ID="lblPassengersCount"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 196px; left: 25px;" ID="lblPrice2" runat="server"
        class='printlbl' />
    <asp:Label Style="position: absolute; top: 196px; left: 201px;" ID="lblSaleDate"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 196px; left: 300px;" ID="lblSaleTime"
        runat="server" class='printlbl' />
    <asp:Label Style="position: absolute; top: 195px; left: 433px;" ID="lblSaler" runat="server"
        class='printlbl' />
    <asp:Label ID="lblNoL" runat="server" class='printLeftlbl' Style="position: absolute;
        top: 3px; left: 663px; height: 11px;" />
    <asp:Label Style="position: absolute; top: 25px; left: 556px;" ID="lblFullNameL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 52px; left: 556px;" ID="lblSrcCityL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 75px; left: 555px;" ID="lblDestCityL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 118px; left: 555px;" ID="lblDepartureTimeL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 119px; left: 700px;" ID="lblDayL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 118px; left: 620px;" ID="lblDateL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 163px; left: 660px;" ID="lblPassengersCountL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 142px; left: 660px;" ID="lblChairsNoL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 184px; left: 660px;" ID="lblPriceL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 209px; left: 555px;" ID="lblSaleDateL"
        runat="server" class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 209px; left: 659px;" ID="lblSalerL" runat="server"
        class='printLeftlbl' />
    <asp:Label Style="position: absolute; top: 37px; left: 150px;" ID="lblUrl" runat="server"
        class='printLeftlbl' />
    </form>
</body>
</html>
