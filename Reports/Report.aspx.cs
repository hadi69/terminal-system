﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
public partial class Reports_Report : System.Web.UI.Page
{
    Seller seller = null;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        int sellerid = Tool.GetInt(Request.QueryString["sellerid"], Null.NullInteger);
        if (sellerid != Null.NullInteger)
        {
            Helper.Instance.Init();
            seller = Helper.Instance.GetSeller(sellerid);
        }

        int id = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);
        string repname = Tool.GetString(Request.QueryString["repname"], "");
        string otherDests = Tool.GetString(Request.QueryString["otherDests"], "");
        string companycode = Tool.GetString(Request.QueryString["cc"], "");
        string warning = Tool.GetString(Request.QueryString["warning"], "");
        LoadReport(id, repname, otherDests, companycode, warning);
        doTechnical.NavigateUrl = "../Technical.aspx?tripid=" + id + "&sellerid=" + sellerid;
    }
    public void LoadReport(int id, string repname, string otherDests, string companycode, string warning)
    {
        Trip trip = null;
        Exclusive exclusive = null;
        if (repname == "SuratVaziat" || repname == "SuratVaziatM" || repname == "SuratVaziatMD")
            trip = Helper.Instance.GetTrip(id);
        if (repname == "SuratVaziatExclusive")
            exclusive = Helper.Instance.GetExclusive(id);

        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.EnableExternalImages = true;
        ReportDataSource dsLoanList = null;
        if ((repname == "SuratVaziat") || (repname == "SuratVaziatM") || (repname == "SuratVaziatMD"))
        {
            if (seller == null)
                dsLoanList = new ReportDataSource("SuratVaziat", Helper.Instance.GetSuratVaziat(id));
            else
            {
                dsLoanList = new ReportDataSource("SuratVaziat", Helper.Instance.FillDataTable("dbo.GetSuratVaziat " + id, seller.CnnStr));
                Helper.Instance.RunQuery("UPDATE Trips SET Archive=1 WHERE ID=" + id, seller.CnnStr);
            }
        }
        if (repname == "SuratVaziatExclusive")
            dsLoanList = new ReportDataSource("SuratVaziatExclusive", Helper.Instance.GetSuratVaziatExclusive(id));
        else if (repname == "Ticket")
            dsLoanList = new ReportDataSource("Ticket", Helper.Instance.GetTicketClass(id));

        //ReportDataSource dsDates = new ReportDataSource("Dates", FishDataEngine.GetDatesList().Tables[0]);
        //ReportDataSource dsFunds = new ReportDataSource("LoanList", FundDataEngine.GetFundsList().Tables[0]);

        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        if ((repname == "SuratVaziat") || (repname == "SuratVaziatM") || (repname == "SuratVaziatMD"))
            ReportViewer1.LocalReport.ReportPath = "Reports/" + repname + ".rdlc";
        else if (repname == "SuratVaziatExclusive")
            ReportViewer1.LocalReport.ReportPath = "Reports/SuratVaziatExclusive.rdlc";
        else if (repname == "Ticket")
            ReportViewer1.LocalReport.ReportPath = "Reports/Ticket.rdlc";


        string closeDateBefore = "";
        if (repname == "SuratVaziat" || (repname == "SuratVaziatM") || (repname == "SuratVaziatMD"))
            try
            {
                if (trip != null && trip.CloseDate.HasValue)
                {
                    DateTime dt = new DateTime(trip.Date.Year, trip.Date.Month, trip.Date.Day
                              , Tool.GetInt(trip.DepartureTime2.Split(':')[0], 0)
                              , Tool.GetInt(trip.DepartureTime2.Split(':')[0], 0)
                              , 0);
                    int hours = (int)(dt - trip.CloseDate.Value).TotalHours;
                    if (hours > 0 && dt.Day != trip.CloseDate.Value.Day)
                        closeDateBefore = "تاریخ بستن: " + string.Format(" {0}:{1}"
                    , trip.CloseDate.Value.Hour.ToString().PadLeft(2, '0'), trip.CloseDate.Value.Minute.ToString().PadLeft(2, '0'))
                    + " " + Tool.ToPersianDate(trip.CloseDate, "") + " برای " + hours + "ساعت آینده";
                    else if (hours < 0)
                        closeDateBefore = "تاریخ بستن: " + string.Format(" {0}:{1}"
                    , trip.CloseDate.Value.Hour.ToString().PadLeft(2, '0'), trip.CloseDate.Value.Minute.ToString().PadLeft(2, '0'))
                    + " " + Tool.ToPersianDate(trip.CloseDate, "") + " برای " + (-hours) + "ساعت گذشته";
                }
            }
            catch { }
        if (repname == "SuratVaziatExclusive")
            try
            {
                if (exclusive != null && exclusive.CloseDate.HasValue)
                {
                    DateTime dt = new DateTime(exclusive.DepartureDate.Year, exclusive.DepartureDate.Month, exclusive.DepartureDate.Day
                              , Tool.GetInt(exclusive.DepartureTime.Split(':')[0], 0)
                              , Tool.GetInt(exclusive.DepartureTime.Split(':')[0], 0)
                              , 0);
                    int hours = (int)(dt - exclusive.CloseDate.Value).TotalHours;
                    if (hours > 0 && dt.Day != exclusive.CloseDate.Value.Day)
                        closeDateBefore = "تاریخ بستن: " + string.Format(" {0}:{1}"
                    , exclusive.CloseDate.Value.Hour.ToString().PadLeft(2, '0'), exclusive.CloseDate.Value.Minute.ToString().PadLeft(2, '0'))
                    + " " + Tool.ToPersianDate(exclusive.CloseDate, "") + " برای " + hours + "ساعت آینده";
                    else if (hours < 0)
                        closeDateBefore = "تاریخ بستن: " + string.Format(" {0}:{1}"
                    , exclusive.CloseDate.Value.Hour.ToString().PadLeft(2, '0'), exclusive.CloseDate.Value.Minute.ToString().PadLeft(2, '0'))
                    + " " + Tool.ToPersianDate(exclusive.CloseDate, "") + " برای " + (-hours) + "ساعت گذشته";
                }
            }
            catch { }

        string trackCode = "";
        string trackPath = "";
        if (trip != null)
            trackCode = trip.TrackCode;
        else if (exclusive != null)
            trackCode = exclusive.TrackCode;
        if (!string.IsNullOrEmpty(trackCode))
        {
            trackPath = Server.MapPath("~/barcodes/" + trackCode + ".jpg");
            if (!System.IO.File.Exists(trackPath))
            {
                Barcode bar = new Barcode();
                bar.Width = 220;
                bar.Height = 20;
                bar.BarCode = trackCode;
                bar.SaveImage(trackPath);
            }
        }
        string url = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf('?'));
        trackPath = url.ToLower().Replace("/reports/report.aspx", "/barcodes/" + trackCode + ".jpg");//@"/images/bus.gif");

        ReportParameter[] paramss = null;
        if (repname == "SuratVaziat")
        {
            paramss = new ReportParameter[5];
            paramss[0] = new ReportParameter("tripID", id.ToString(), false);
            paramss[1] = new ReportParameter("otherDests", otherDests, false);

            paramss[2] = new ReportParameter("closeDateBefore", closeDateBefore, false);

            paramss[3] = new ReportParameter("TrackPath", trackPath, false);
            paramss[4] = new ReportParameter("CompanyCode", companycode,false);
        }
        else if (repname == "SuratVaziatM")
        {
            paramss = new ReportParameter[5];
            paramss[0] = new ReportParameter("tripID", id.ToString(), false);
            paramss[1] = new ReportParameter("closeDateBefore", closeDateBefore, false);
            paramss[2] = new ReportParameter("TrackPath", trackPath, false);
            paramss[3] = new ReportParameter("CompanyCode", companycode,false);
                paramss[4] = new ReportParameter("Warning", warning, false);
        }
        else if ((repname == "SuratVaziatMD") || repname == "SuratVaziatExclusive")
        {
            paramss = new ReportParameter[4];
            paramss[0] = new ReportParameter("tripID", id.ToString(), false);
            paramss[1] = new ReportParameter("closeDateBefore", closeDateBefore, false);
            paramss[2] = new ReportParameter("TrackPath", trackPath, false);
            paramss[3] = new ReportParameter("CompanyCode", companycode, false);
        }
        else if (repname == "Ticket")
        {
            paramss = new ReportParameter[1];
            paramss[0] = new ReportParameter("ticketID", id.ToString(), false);
        }
        ReportViewer1.LocalReport.SetParameters(paramss);

        ReportViewer1.LocalReport.DataSources.Add(dsLoanList);
        //ReportViewer1.LocalReport.DataSources.Add(dsDates);
        //ReportViewer1.LocalReport.DataSources.Add(dsFunds);

        ReportViewer1.LocalReport.Refresh();
    }
}
