﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;

public partial class Invalids : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<string> all = new List<string>();// (from h in Helper.Instance.DB.Trips select h.Series).ToList();
            all.Insert(0, "");
            if (Tool.CanMini() || Tool.CanSewari())
            {
                string mini = string.Format("{0},{1},{2}"
                       , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                       , Helper.Instance.GetSettingValue("SeriesNoMini3"));
                if (!all.Contains(mini))
                    all.Add(mini);
            }
            if (Tool.CanBus())
            {
                string bus = string.Format("{0},{1},{2},{3}"
                       , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
                       , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
                if (!all.Contains(bus))
                    all.Add(bus);
            }
            string der = string.Format("{0},{1},{2}"
                       , Helper.Instance.GetSettingValue("SeriesNoD1"), Helper.Instance.GetSettingValue("SeriesNoD2")
                       , Helper.Instance.GetSettingValue("SeriesNoD3"));
            if (!all.Contains(der))
                all.Add(der);

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();

            //mSeries.DataSource = all.Distinct();
            //mSeries.DataBind();
        }
        //{
        //    List<string> all = (from h in Helper.Instance.DB.Exclusives
        //                        select h.Series).ToList();
        //    all.Insert(0, "");
        //    string bus = string.Format("{0},{1},{2}"
        //               , Helper.Instance.GetSettingValue("SeriesNoD1"), Helper.Instance.GetSettingValue("SeriesNoD2")
        //               , Helper.Instance.GetSettingValue("SeriesNoD3"));
        //    if (!all.Contains(bus))
        //        all.Add(bus);

        //    mSeriesD.DataSource = all.Distinct();
        //    mSeriesD.DataBind();
        //}
        {
            //List<Service> all = Helper.Instance.GetServices();
            //Service none = new Service();
            //none.ID = Null.NullInteger;
            //all.Insert(0, none);

            //sServiceID.DataValueField = "ID";
            //sServiceID.DataTextField = "Title";
            //sServiceID.DataSource = all;
            //sServiceID.DataBind();
        }
        {
            List<Car> all = Helper.Instance.GetCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarID.DataValueField = "ID";
            sCarID.DataTextField = "Title";
            sCarID.DataSource = all;
            sCarID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            CarType none = new CarType();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarTypeID.DataValueField = "ID";
            sCarTypeID.DataTextField = "Title";
            sCarTypeID.DataSource = all;
            sCarTypeID.DataBind();
        }
        {
            List<Driver> all = Helper.Instance.GetDrivers();
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDriverID1.DataValueField = "ID";
            sDriverID1.DataTextField = "Title";
            sDriverID1.DataSource = all;
            sDriverID1.DataBind();
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            City none = new City();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDestID.DataValueField = "ID";
            sDestID.DataTextField = "Title";
            sDestID.DataSource = all;
            sDestID.DataBind();
        }

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;
    }
    private DataTable GetTripsDT()
    {
        // DateTime dt1 = DateTime.Now;
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        #region old
        string qryOLD = string.Format(@"SELECT InValids.*
    , Services.ServiceType, Services.DepartureTime
    , ISNULL(Exclusives.DepartureTime, Services.DepartureTime) AS DepartureTime2 
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
FROM InValids
LEFT outer JOIN Exclusives ON InValids.TripID=Exclusives.ID
LEFT outer JOIN Services ON Services.ID=InValids.ServiceID
left outer JOIN Paths ON Services.PathID=Paths.ID
LEFT outer JOIN Drivers ON InValids.DriverID1=Drivers.ID
LEFT outer JOIN Cars ON InValids.CarID=Cars.ID
left outer JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
WHERE InValids.Date>= '{0}/{1}/{2}' AND InValids.Date<='{3}/{4}/{5} 23:59:59'
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);
        #endregion
        string qry = string.Format(@"SELECT * FROM 
(
SELECT InValids.*
    , Services.ServiceType, Services.DepartureTime
    , ISNULL(Exclusives.DepartureTime, Services.DepartureTime) AS DepartureTime2 
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
FROM InValids
LEFT outer JOIN Exclusives ON InValids.TripID=Exclusives.ID
LEFT outer JOIN Services ON Services.ID=InValids.ServiceID
left outer JOIN Paths ON Services.PathID=Paths.ID
LEFT outer JOIN Drivers ON InValids.DriverID1=Drivers.ID
LEFT outer JOIN Cars ON InValids.CarID=Cars.ID
left outer JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
WHERE InValids.Exclusive=1

UNION
SELECT InValids.*
    , Services.ServiceType, Services.DepartureTime
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2 
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
FROM InValids
LEFT outer JOIN Trips ON InValids.TripID=Trips.ID
LEFT outer JOIN Services ON Services.ID=InValids.ServiceID
left outer JOIN Paths ON Services.PathID=Paths.ID
LEFT outer JOIN Drivers ON InValids.DriverID1=Drivers.ID
LEFT outer JOIN Cars ON InValids.CarID=Cars.ID
left outer JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
WHERE InValids.Exclusive IS NULL OR InValids.Exclusive=0
) t
WHERE t.Date>= '{0}/{1}/{2}' AND t.Date<='{3}/{4}/{5} 23:59:59'
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);
        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            qry += " AND t.ServiceID=" + sID;
        }
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            qry += " AND t.DriverID1=" + sID;
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            qry += " AND t.CarID=" + sID;
        }
        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            qry += " AND t.Series LIKE N'%" + s + "%'";
        }
        //if (sNo.Text.Trim().Length > 0)
        //{
        //    string s = sNo.Text.Trim();
        //    qry += " AND InValids.No LIKE N'%" + s + "%'";
        //}

        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY t.Date");
        if (dt != null && dt.Rows.Count > 0)
        {
            if (sNo.Text.Trim().Length > 0)
            {
                string s = sNo.Text.Trim();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int no = Tool.GetInt(dt.Rows[i]["No"], -1);
                    if (!no.ToString().Contains(s))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }
        // mMsg.Text = msg;
        return dt;
    }

    private List<InValid> GetTrips()
    {
        var all = from c in Helper.Instance.DB.InValids
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
        {
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0, 0);
            all = all.Where(c => c.Date >= fromDate);
        }
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
        {
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999);
            all = all.Where(c => c.Date <= toDate);
        }
        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.ServiceID == sID);
        }
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.DriverID1 == sID);
        }
        //if (sCarTypeID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.Service.CarTypeID == sID);
        //}
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.CarID == sID);
        }

        //if (sDestID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.Service.Path.City.ID == sID);
        //}

        if (sNo.Text.Trim().Length > 0)
            all = all.Where(c => c.No.ToString().Contains(sNo.Text.Trim()));

        if (sSeries.Text.Trim().Length > 0)
        {
            string s = Tool.ParseSeries(sSeries.Text.Trim());
            all = all.Where(c => c.Series.Contains(s));
        }
        all = all.OrderBy(c => c.Date);
        return all.ToList();
    }
    private void BindData()
    {
        #region printBimeQry.Value
        {
            DateTime startDate = sDateStart.SelectedDate;
            DateTime toDate = sDateEnd.SelectedDate;
            if (startDate == Null.NullDate)
                startDate = DateTime.Now.AddYears(-2);
            if (toDate == Null.NullDate)
                toDate = DateTime.Now.AddYears(2);

            string fDate = startDate.ToShortDateString(); ;
            string tDate = toDate.ToShortDateString();
            int serviceID = 0;
            if (sServiceID.SelectedIndex > 0)
                serviceID = Tool.GetInt(sServiceID.SelectedValue, 0);
            int driverID1 = 0;
            if (sDriverID1.SelectedIndex > 0)
                driverID1 = Tool.GetInt(sDriverID1.SelectedValue, 0);
            int carID = 0;
            if (sCarID.SelectedIndex > 0)
                carID = Tool.GetInt(sCarID.SelectedValue, 0);
            string series = "";
            if (sSeries.Text.Trim().Length > 0)
                series = sSeries.Text;
            string no = "";
            if (sNo.Text.Trim().Length > 0)
                no = sNo.Text;
            prinQry.Value = string.Format("fromDate={0}&toDate={1}&serviceID={2}&driverID={3}&carID={4}&series={5}&no={6}",
                fDate, tDate, serviceID, driverID1, carID, series, no);
        }
        #endregion

        list.DataSource = GetTripsDT();
        list.DataBind();
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView row = e.Row.DataItem as DataRowView;
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            if (row != null)
            {
                e.Row.Cells[0].Text = row["ID"].ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(row["Date"], "");
                e.Row.Cells[4].Text = Tool.ToString((ServiceTypes)Tool.GetInt(row["ServiceType"]));
                e.Row.Cells[5].Text = Tool.GetString(row["SrcCity"], "") + " -> " + Tool.GetString(row["DestCity"], "");
                //e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(info.CarID);
                //e.Row.Cells[7].Text = info.Driver != null ? info.Driver.Title : "";
                string series = Tool.GetString(row["Series"], "");
                if (series != null && series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                    string[] ss = series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                        b.AppendFormat("<td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td>", ss[0], ss[1], ss[2]);
                    b.Append("</tr></table>");
                    e.Row.Cells[9].Text = b.ToString();
                }
                //JsTools.HandleDeleteButton(e);
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = cc.Controls[0] as LinkButton;
                if (delete != null)
                    delete.Attributes.Add("OnClick", string.Format("$d({0});", row["ID"]));
            }
            else
            {
                InValid info = e.Row.DataItem as InValid;
                if (info != null)
                {
                    Service s = Helper.Instance.GetService(info.ServiceID);
                    Driver d = info.DriverID1.HasValue ? Helper.Instance.GetDriver(info.DriverID1.Value) : null;
                    e.Row.Cells[0].Text = info.ID.ToString();
                    e.Row.Cells[2].Text = Tool.ToPersianDate(info.Date, "");
                    e.Row.Cells[3].Text = s == null ? "" : s.DepartureTime;
                    e.Row.Cells[4].Text = s == null ? "" : s.ServiceType2;
                    e.Row.Cells[5].Text = s == null ? "" : s.Path.Title;
                    e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(info.CarID);
                    e.Row.Cells[7].Text = d != null ? d.Title : "";
                    //   e.Row.Cells[10].Text = Helper.Instance.GetNumTickets(info.ID).ToString();

                    JsTools.HandleDeleteButton(e);
                }
            }
        }
    }

    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void doRefineNew_Click(object sender, EventArgs e)
    {
        // Cancel with no code
        int no1 = Tool.GetInt(sNo.Text, Null.NullInteger);
        if (no1 == Null.NullInteger)
        {
            mMsg.Text = "شماره صورت وارد شده اشتباه است";
            return;
        }
        int no2 = Tool.GetInt(sNo.Text, Null.NullInteger);
        if (no2 == Null.NullInteger)
        {
            mMsg.Text = "شماره صورت وارد شده اشتباه است";
            return;
        }
        if (sSeries.Text.Trim().Length == 0)
        {
            mMsg.Text = "سری صورت انتخاب شده اشتباه است";
            return;
        }
        string series = sSeries.Text.Trim();
        string exc = string.Format("{0},{1},{2}"
                     , Helper.Instance.GetSettingValue("SeriesNoD1"), Helper.Instance.GetSettingValue("SeriesNoD2")
                     , Helper.Instance.GetSettingValue("SeriesNoD3"));
        if (exc == series)
        {
            doRefineNewD_Click(null, null);
            return;
        }
         string mini = string.Format("{0},{1},{2}"
                       , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                       , Helper.Instance.GetSettingValue("SeriesNoMini3"));
        List<int> trips = (from t in Helper.Instance.DB.Trips
                           where no1 <= t.No && t.No <= no2
                           && t.Series == series
                           select t.No).ToList();

        StringBuilder dones = new StringBuilder();
        string errors = "";
        for (int i = no1; i <= no2; i++)
        {
            if (trips.Contains(no2))
            {
                errors += i + " : این شماره صورت استفاده شده است و قابل ابطال نیست. برای ابطال آن به صفحه فروش یا صورت وضعیت مراجعه کنید.<br />";
                continue;
            }

            try
            {
                Helper.Instance.CancelTrackCodeNoCode(i, series, series == mini ? 2 : 1);
                dones.Append(i + ", ");
            }
            catch (Exception ex)
            {
                //errors += i + " : " + ex.Message + "<br />";
                //continue;
            }

            InValid v = new InValid();
            v.Date = DateTime.Now;
            v.Ticketless = 0;
            v.TicketlessPrice = 0;
            v.TicketlessName = "";
            v.Stamp = 0;
            v.Comission = 0;
            v.BodyInsurance = 0;
            v.ExtraCost = 0;
            v.Closed = false;
            v.Price = 0;
            v.Toll = 0;
            v.Insurance = 0;
            v.Reception = 0;
            v.TotalPrice = 0;
            v.Comission2 = 0;
            v.OtherDeficits = 0;
            v.SumDeficits = 0;
            v.AutoShare = 0;
            v.Toll2 = 0;
            v.Locked = false;
            v.Others = 0;
            v.TripID = 0;
            v.No = i;
            v.Series = series;
            v.CarID = null;
            v.DriverID1 = null;
            v.DriverID2 = null;
            v.DriverID3 = null;
            v.ServiceID = 0;
            Helper.Instance.DB.InValids.InsertOnSubmit(v);
            Helper.Instance.Update();
            dones.Append(i + ", ");
        }
        mMsg.Text = "ابطال شماره صورتهای زیر با موفقیت انجام شد: " + dones.ToString() + "<br />";
        if (errors.Length > 0)
            mMsg.Text = "ابطال شماره صورتهای زیر با انجام نشد:<br />" + errors;
        BindData();
    }
    protected void doRefineNewD_Click(object sender, EventArgs e)
    {
        int no1 = Tool.GetInt(sNo.Text, Null.NullInteger);
        if (no1 == Null.NullInteger)
        {
            mMsg.Text = "شماره صورت وارد شده اشتباه است";
            return;
        }
        int no2 = Tool.GetInt(sNo.Text, Null.NullInteger);
        if (no2 == Null.NullInteger)
        {
            mMsg.Text = "شماره صورت وارد شده اشتباه است";
            return;
        }

        if (sSeries.Text.Trim().Length == 0)
        {
            mMsg.Text = "سری صورت انتخاب شده اشتباه است";
            return;
        }
        string series = sSeries.Text.Trim();

        List<int> trips = (from t in Helper.Instance.DB.Exclusives
                           where no1 <= t.No && t.No <= no2
                           && t.Series == series
                           select t.No).ToList();

        StringBuilder dones = new StringBuilder();
        for (int i = no1; i <= no2; i++)
        {
            if (trips.Contains(no2))
                continue;

            InValid v = new InValid();
            v.Date = DateTime.Now;
            v.Ticketless = 0;
            v.TicketlessPrice = 0;
            v.TicketlessName = "";
            v.Stamp = 0;
            v.Comission = 0;
            v.BodyInsurance = 0;
            v.ExtraCost = 0;
            v.Closed = false;
            v.Price = 0;
            v.Toll = 0;
            v.Insurance = 0;
            v.Reception = 0;
            v.TotalPrice = 0;
            v.Comission2 = 0;
            v.OtherDeficits = 0;
            v.SumDeficits = 0;
            v.AutoShare = 0;
            v.Toll2 = 0;
            v.Locked = false;
            v.Others = 0;
            v.TripID = 0;
            v.No = i;
            v.Series = series;
            v.CarID = null;
            v.DriverID1 = null;
            v.DriverID2 = null;
            v.DriverID3 = null;
            v.ServiceID = 0;
            v.Exclusive = true;
            Helper.Instance.DB.InValids.InsertOnSubmit(v);
            Helper.Instance.Update();

            dones.Append(v.No + ", ");
        }
        mMsg.Text = "ابطال شماره صورتهای زیر با موفقیت انجام شد:<br>" + dones.ToString();
        BindData();
    }
    protected void doInvalidWithCode_Click(object sender, EventArgs e)
    {
        Trip trip = Helper.Instance.GetTrip(mTrackCode.Text);
        if (trip == null)
        {
            mMsg.Text = "صورتی با این کد رهگیری پیدا نشد";
            return;
        }
        StringBuilder dones = new StringBuilder();
        try
        {
            Helper.Instance.ChangeSeries(true, trip);
            dones.Append(trip.No);
        }
        catch (Exception ex)
        {
            mMsg.Text = ex.Message;
            return;
        }
        mMsg.Text = "ابطال شماره صورتهای زیر با موفقیت انجام شد:<br>" + dones.ToString();
        BindData();
    }

    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        int id = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        InValid info = Helper.Instance.GetInValid(id);
        if (info == null)
            return;
        Trip trip = Helper.Instance.GetTrip(info.TripID);
        if (trip == null)
        {
            mMsg.Text = "این شماره صورت ابطال شده، سرویس مرتبط ندارد";
            return;
        }
        if (trip.Closed)
        {
            mMsg.Text = "سرویس مربوطه بسته شده است و نمیتوان این شماره را به آن نسبت داد";
            return;
        }
        trip.No = info.No;
        trip.Series = info.Series;
        trip.Closed = true;
        if (Helper.Instance.Update())
        {
            mMsg.Text = "سرویس مربوطه از ابطال خارج شد و شماره صورت قبلی به آن اختصاص داده شد";
        }
        else
        {
            mMsg.Text = Helper.Instance.GetLastException();
            return;
        }
        if (Helper.Instance.DeleteInValid(id))
            mMsg.Text += "<br>شماره ابطال شده حذف شد";
        else
            mMsg.Text += "<br>شماره ابطال شده حذف نشد " + Helper.Instance.GetLastException();
        BindData();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteInValid(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
}
