﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CarTypes.aspx.cs" Inherits="Basics_CarTypes" Title="نوع اتوکار" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        عنوان چيدمان
                    </td>
                    <td>
                        <asp:DropDownList ID="sLayout" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        نام ماشين
                    </td>
                    <td>
                        <asp:TextBox ID="sCarTitle" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Title" HeaderText="نام اتوکار" />
                    <asp:BoundField DataField="LayoutID" HeaderText="چيدمان صندلي" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:BoundField DataField="SpecialID" HeaderText="نوع سرویس دهی" />
                    <asp:BoundField DataField="Code" HeaderText="کد نوع وسیله" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="550px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش نوع اتوکار</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نام اتوکار
                            </td>
                            <td>
                                <asp:TextBox ID="mTitle" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="نام اتوکار الزامي است"
                                    ControlToValidate="mTitle" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                چيدمان صندلي
                            </td>
                            <td>
                                <asp:DropDownList ID="mLayoutID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="چيدمان صندلي الزامي است"
                                    ControlToValidate="mLayoutID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" TextMode="MultiLine" Height="200px" Width="300px">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نوع سرویس دهی
                            </td>
                            <td>
                                <asp:DropDownList ID="mSpecialID" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                         <tr>
                            <td class="N">
                                کد نوع وسیله
                            </td>
                            <td>
                              <asp:DropDownList ID="mCode2" runat="server">
                                    <asp:ListItem Value="0" Text="[لطفا یک کد را انتخاب کنید]" />
                                    <asp:ListItem Value="1" Text="اتوبوس سوپر 36 صندلی کد 1" />
                                    <asp:ListItem Value="2" Text="اتوبوس سوپر 32 صندلی کد 2" />
                                    <asp:ListItem Value="3" Text="اتوبوس سوپر 27 صندلی کد 3" />
                                    <asp:ListItem Value="4" Text="اتوبوس دولوکس 36 صندلی کد 4" />
                                    <asp:ListItem Value="5" Text="اتوبوس دولوکس40 صندلی کد 5" />
                                    <asp:ListItem Value="6" Text="اتوبوس عادی کد 6" />
                                    <asp:ListItem Value="7" Text="مینی بوس کد 7" />
                                    <asp:ListItem Value="8" Text="سواری کد 8" />
                                    <asp:ListItem Value="9" Text="اتوبوس ویژه یک طبقه کد 9" />
                                    <asp:ListItem Value="10" Text="اتوبوس ویژه دو طبقه کد 10" />
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
