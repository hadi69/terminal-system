﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InsuranceKMs.aspx.cs" Inherits="Basics_InsuranceKMs" Title="مسافتهاي مربوط به بيمه بدنه" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <span class="Title">قیمت گذاری بیمه مسافتهای متفاوت مربوط به نوع بیمه نامه بدنه
                <asp:Label ID="mInsurance" runat="server" CssClass="Title2" />
                :</span>
            <asp:Button ID="doAdd" runat="server" CssClass="CB" Width="50px" OnClick="doAdd_Click"
                Text="جديد INS" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="StartKM" HeaderText="از کيلومتر" />
                    <asp:BoundField DataField="EndKM" HeaderText="تا کيلومتر" />
                    <asp:BoundField DataField="Price" HeaderText="مبلغ" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش مسافت</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                از کيلومتر
                            </td>
                            <td>
                                <asp:TextBox ID="mStartKM" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv11" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="از کيلومتر الزامي است" ControlToValidate="mStartKM" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mStartKM" />
                                <asp:RangeValidator ID="RangeValidator1" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mStartKM" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تا کيلومتر
                            </td>
                            <td>
                                <asp:TextBox ID="mEndKM" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv21" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="تا کيلومتر الزامي است" ControlToValidate="mEndKM" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mEndKM" />
                                <asp:RangeValidator ID="RangeValidator2" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mEndKM" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مبلغ
                            </td>
                            <td>
                                <asp:TextBox ID="mPrice" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="مبلغ الزامي است"
                                    ControlToValidate="mPrice" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mPrice" />
                                <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mPrice" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
