﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Insurances.aspx.cs" Inherits="Basics_Insurances" Title="بيمه بدنه" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" Width="50px" CssClass="CB" OnClick="doAdd_Click"
                            Text="جديد INS" />
                    </td>
                </tr>
                <tr>
                    <td>
                        نوع بيمه
                    </td>
                    <td>
                        <asp:DropDownList ID="cmboInsurance" runat="server" Width="150px" />
                    </td>
                    <td>
                        توضيحات
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" Width="300px" ></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkHasInsurance" runat="server" Text="بيمه دارد" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ID" HeaderText="نوع بيمه" />                    
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:CheckBoxField DataField="HasInsurance" HeaderText="بيمه نامه دارد" />
                    <asp:HyperLinkField HeaderText="قیمت گذاری بیمه مسافتهای متفاوت" DataNavigateUrlFields="ID" 
                        DataNavigateUrlFormatString="InsuranceKMs.aspx?insuranceid={0}" Text="قیمت گذاری بیمه مسافتهای متفاوت" />
                    <asp:BoundField DataField="Comments" HeaderText="توضيحات" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate><br /><span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate><HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش بيمه</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نوع بيمه
                            </td>
                            <td>
                                <asp:DropDownList ID="mInsuranceType" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="نوع بيمه الزامي است"
                                    ControlToValidate="mInsuranceType" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                بيمه نامه دارد
                            </td>
                            <td>
                                <asp:CheckBox ID="mHasInsurance" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server" Text="تاييد F7" CssClass="CB">
                                </asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                         <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
