﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Basics_Cities : System.Web.UI.Page
{
    bool canDeleteEdit = true;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        canDeleteEdit = Tool.GetBool(Helper.Instance.GetSettingValue("CanEditCities"), false);
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
            //puOk.Enabled = doAdd.Enabled = Tool.GetBool(Helper.Instance.GetSettingValue("CanEditCities"), false);
        }
    }
    private void BindInitData()
    {
        List<Country> all = Helper.Instance.GetCountries();
        mCountry.DataValueField = "ID";
        mCountry.DataTextField = "Title";
        mCountry.DataSource = all;
        mCountry.DataBind();

        Country none = new Country();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);

        cmboCountry.DataValueField = "ID";
        cmboCountry.DataTextField = "Title";
        cmboCountry.DataSource = all;
        cmboCountry.DataBind();      
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Cities
                  select c;// new { c.ID, c.Title, c.Code, c.Enabled, c.CountryID, CountryTitle = c.Country.Title };

        if (cmboCountry.SelectedIndex > 0)
        {
            int cntID = Tool.GetInt(cmboCountry.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.CountryID == cntID);
        }                 
        if (ctiyTitle.Text.Trim().Length > 0)
            all = all.Where(c => c.Title.Contains(ctiyTitle.Text.Trim()));
        if (cityCode.Text.Trim().Length > 0)
            all = all.Where(c => c.Code.Contains(cityCode.Text.Trim()));
        if (sEnabled.Checked && !sEnabledNo.Checked)
            all = all.Where(c => c.Enabled == true);
        else if (!sEnabled.Checked && sEnabledNo.Checked)
            all = all.Where(c => c.Enabled == false);
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        City info;
        if (Null.NullInteger == EditID)
        {
            int code = Tool.GetInt(mCode.Text, 0);
            if (mCode.Text.Trim().Length != 8)
            {
                mError.Text = "کد برای شهر جدید باید 8 رقمی باشد";
                puEx.Show();
                return;
            }
            City old = null;
            try
            {
                old = (from cc in Helper.Instance.DB.Cities where cc.Code == mCode.Text select cc).First();
            }
            catch { }
              if (old == null)
            {
                mError.Text = "کد برای شهر جدید باید یکی از کدهای موجود باشد";
                puEx.Show();
                return;
            }
            
            //if (code < 1 || code > 999)
            //{
            //    mError.Text = "کد برای شهر جدید باید بین 1 تا 999 باشد";
            //    puEx.Show();
            //    return;
            //}
            info = new City();
        }
        else
            info = Helper.Instance.GetCity(EditID);
        if (info == null)
            return;
       
        info.Title = mTitle.Text;
        info.Code = mCode.Text;
        //info.CountryID = int.Parse(mCountry.SelectedValue);
        info.Country = Helper.Instance.GetCountry(Tool.GetInt(mCountry.SelectedValue, Null.NullInteger));
        info.Enabled = mEnabled.Checked;
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Cities.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            mTitle.Text = mCode.Text = "";
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            City info = e.Row.DataItem as City;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Country.Title;

                if (Tool.GetInt( info.Code, Null.NullInteger) > 999 && !canDeleteEdit)
                    e.Row.Cells[e.Row.Cells.Count - 1].Text = "";
                else
                    JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        City info = Helper.Instance.GetCity(EditID);
        if (info == null)
            return;
        mTitle.Text = info.Title;
        mCode.Text = info.Code;
        mEnabled.Checked = info.Enabled;
        mTitle.Enabled = mCode.Enabled = (Tool.GetInt(info.Code, Null.NullInteger) < 1000 || canDeleteEdit);
        try
        {
            mCountry.SelectedValue = info.CountryID.ToString();
        }
        catch { }
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        City info = Helper.Instance.GetCity(id);
        if (info == null)
            return;
        if (Tool.GetInt(info.Code, Null.NullInteger) > 999 && !canDeleteEdit)
            return; 
        if (Helper.Instance.DeleteCity(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mEnabled.Checked = true;
        mTitle.Text = mCode.Text = "";
        mTitle.Enabled = mCode.Enabled = true;
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        list.PageIndex = 0;
        BindData();
    }
}
