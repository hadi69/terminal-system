﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Countries.aspx.cs" Inherits="Basics_Countries" Title="کشورها" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" Text="جديد INS" OnClick="doAdd_Click" CssClass="CB"
                            Width="50px"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        نام کشور
                    </td>
                    <td>
                        <asp:TextBox ID="sTitle" runat="server" Width="150px" >
                        </asp:TextBox>
                    </td>
                    <td>
                        کد کشور
                    </td>
                    <td>
                        <asp:TextBox ID="sCode" runat="server" Width="150px" >
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Title" HeaderText="نام کشور" />
                    <asp:BoundField DataField="Code" HeaderText="کد کشور" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate><br /><span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate><HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش کشور</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نام کشور
                            </td>
                            <td>
                                <asp:TextBox ID="mTitle" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="نام کشور الزامي است"
                                    ControlToValidate="mTitle" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد کشور
                            </td>
                            <td>
                                <asp:TextBox ID="mCode" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="کد کشور الزامي است"
                                    ControlToValidate="mCode" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
