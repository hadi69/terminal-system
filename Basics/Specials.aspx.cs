﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Basics_Specials : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Specials
                  select c;
        if (sTitle.Text.Trim().Length > 0)
            all = all.Where(c => c.Title.Contains(sTitle.Text.Trim()));
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Special info;
        if (Null.NullInteger == EditID)
            info = new Special();
        else
            info = Helper.Instance.GetSpecial(EditID);
        if (info == null)
            return;

        info.Title = mTitle.Text;
        //mError.Text = Helper.Instance.ValidateCountry(info);
        if (mError.Text.Length > 0)
        {
            puEx.Show();
            return;
        }
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Specials.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            mTitle.Text = "";
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Special info = e.Row.DataItem as Special;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Special info = Helper.Instance.GetSpecial(EditID);
        if (info == null)
            return;
        mTitle.Text = info.Title.ToString();
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteSpecial(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }

        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        //mEnabled.Checked = true;
        mTitle.Text = "";
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();

    }
}
