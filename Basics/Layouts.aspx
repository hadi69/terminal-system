﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Layouts.aspx.cs" Inherits="Basics_Layouts" Title="چيدمان صندلي" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" Width="50px" OnClick="doAdd_Click"
                            Text="جديد INS" />
                    </td>
                </tr>
                <tr>
                    <td>
                        عنوان
                    </td>
                    <td>
                        <asp:TextBox ID="sTitle" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        تعداد طبقه
                    </td>
                    <td>
                        <asp:TextBox ID="sFloors" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        تعداد ستون صندلي
                    </td>
                    <td>
                        <asp:TextBox ID="sColumns" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        تعداد رديف صندلي
                    </td>
                    <td>
                        <asp:TextBox ID="sRows" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        توضيحات
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="sComments" runat="server" Width="363px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Title" HeaderText="عنوان" />
                    <asp:BoundField DataField="Floors" HeaderText="طبقه" />
                    <asp:BoundField DataField="Columns" HeaderText="ستون صندلي" />
                    <asp:BoundField DataField="Rows" HeaderText="رديف صندلي" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:BoundField DataField="Comments" HeaderText="توضيحات" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش چيدمان صندلي</asp:Panel>
                <table border="0">
                    <tr>
                        <td class="N">
                            عنوان
                        </td>
                        <td>
                            <asp:TextBox ID="mTitle" runat="server">
                            </asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="عنوان الزامي است"
                                ControlToValidate="mTitle" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            تعداد طبقه
                        </td>
                        <td>
                            <asp:TextBox ID="mFloor" runat="server">
                            </asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="تعداد طبقه الزامي است"
                                ControlToValidate="mFloor" Display="Dynamic"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mFloor" />
                            <asp:RangeValidator ID="RangeValidator1" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mFloor" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 4 باشد"
                                MaximumValue="4" MinimumValue="1" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            تعداد ستون صندلي
                        </td>
                        <td>
                            <asp:TextBox ID="mColumns" runat="server">
                            </asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfv3" runat="server" ValidationGroup="addGroup" ErrorMessage="تعداد ستون صندلي الزامي است"
                                ControlToValidate="mColumns" Display="Dynamic"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mColumns" />
                            <asp:RangeValidator ID="RangeValidator2" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mColumns" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 20 باشد"
                                MaximumValue="20" MinimumValue="1" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            تعداد رديف صندلي
                        </td>
                        <td>
                            <asp:TextBox ID="mRows" runat="server">
                            </asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfv4" runat="server" ValidationGroup="addGroup" ErrorMessage="تعداد رديف صندلي الزامي است"
                                ControlToValidate="mRows" Display="Dynamic"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mRows" />
                            <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mRows" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 100 باشد"
                                MaximumValue="100" MinimumValue="1" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            فاصله پس از چند ستون
                        </td>
                        <td>
                            <asp:TextBox ID="mColumnSpace" runat="server">
                            </asp:TextBox>
                        </td>
                        <td>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mColumnSpace" />
                            <asp:RangeValidator ID="RangeValidator4" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mColumnSpace" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 20 باشد"
                                MaximumValue="20" MinimumValue="0" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            توضيحات
                        </td>
                        <td>
                            <asp:TextBox ID="mComment" runat="server">
                            </asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            <%--ضميمه--%>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            نمايش
                        </td>
                        <td>
                            <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            چيدمان
                        </td>
                        <td>
                            <asp:PlaceHolder ID="mLayout" runat="server" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                        </td>
                        <td>
                            <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                Text="تاييد F7" CssClass="CB" Width="80px"></asp:Button>&nbsp;<asp:Button ID="puOk2"
                                    ValidationGroup="addGroup" OnClick="puOk2_Click" runat="server" Text="تاييد و بستن"
                                    CssClass="CB" Width="80px"></asp:Button>&nbsp;
                            <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB" Width="80px">
                            </asp:Button>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="Err" colspan="3">
                            <asp:Label runat="server" ID="mError" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
