﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Basics_Insurances : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        List<InsuranceType> all = Helper.Instance.GetInsuranceTypes();
        mInsuranceType.DataValueField = "ID";
        mInsuranceType.DataTextField = "Title";
        mInsuranceType.DataSource = all;
        mInsuranceType.DataBind();

        InsuranceType none = new InsuranceType();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);

        cmboInsurance.DataValueField = "ID";
        cmboInsurance.DataTextField = "Title";
        cmboInsurance.DataSource = all;
        cmboInsurance.DataBind();     
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Insurances
                  select c;// new { c.ID, c.Enabled, c.Comments, c.HasInsurance, c.InsuranceTypeID, InsurancesTypeTitle = c.InsuranceType.Title };
        if (cmboInsurance.SelectedIndex > 0)
        {
            int insID = Tool.GetInt(cmboInsurance.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.InsuranceTypeID == insID);
        }
        list.DataSource = all;
        list.DataBind();
     
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Insurance info;
        if (Null.NullInteger == EditID)
            info = new Insurance();
        else
            info = Helper.Instance.GetInsurance(EditID);
        if (info == null)
            return;
       
        info.Enabled = mEnabled.Checked;
        info.InsuranceType = Helper.Instance.GetInsuranceType(Tool.GetInt(mInsuranceType.SelectedValue, Null.NullInteger));
        info.Comments = mComments.Text;
        info.HasInsurance = mHasInsurance.Checked;

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Insurances.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Insurance info = e.Row.DataItem as Insurance;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.InsuranceType.Title;

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Insurance info = Helper.Instance.GetInsurance(EditID);
        if (info == null)
            return;
        
        mEnabled.Enabled = info.Enabled;
        try
        {
            mInsuranceType.SelectedValue = info.InsuranceTypeID.ToString();
        }
        catch { }
        mComments.Text=info.Comments;
        mHasInsurance.Checked = info.HasInsurance;
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteInsurance(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mEnabled.Checked = true;
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
