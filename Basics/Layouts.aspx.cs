﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Basics_Layouts : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Layouts
                  select c;// new { c.ID, c.Title, c.Floors, c.Columns, c.Rows, c.Layout1, c.Attachment, c.Enabled, c.Comments };
        list.DataSource = all;
        list.DataBind();
    }
    private int GetNumChairs(Layout info)
    {
        int res = 0;
        for (int f = 0; f < info.Floors; f++)
            for (int i = 0; i < info.Rows; i++)
                for (int j = 0; j < info.Columns; j++)
                    if (info.Layout1 != null && info.Layout1.Contains(string.Format(";{0}_{1}_{2};", f, i, j)))
                        res++;
        return res;
    }
    Layout DoPrepareSave()
    {
        Layout info;
        if (Null.NullInteger == EditID)
            info = new Layout();
        else
            info = Helper.Instance.GetLayout(EditID);
        if (info == null)
            return null;

        info.Title = mTitle.Text;
        info.Enabled = mEnabled.Checked;
        info.Attachment = "";// mAttachment.Text;
        info.Comments = mComment.Text;
        info.Columns = Tool.GetInt(mColumns.Text, 1);
        info.ColumnSpace = Tool.GetInt(mColumnSpace.Text, 0);
        info.Rows = Tool.GetInt(mRows.Text, 1);
        //info.Layout1 = mLayout.Text;
        info.Floors = Tool.GetInt(mFloor.Text, 1);
        

        // Handle Layout
        StringBuilder builder = new StringBuilder();
        for (int f = 0; f < info.Floors; f++)
            for (int i = 0; i < info.Rows; i++)
                for (int j = 0; j < info.Columns; j++)
                {
                    string id = string.Format("cb{0}_{1}_{2}", f, i, j);
                    if (Null.NullInteger == EditID || Tool.IsChecked(Request, id))
                        builder.Append(string.Format(";{0}_{1}_{2};", f, i, j));
                }
        info.Layout1 = builder.ToString().Replace(";;", ";");
        info.NumChairs = GetNumChairs(info);

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Layouts.InsertOnSubmit(info);

        return info;
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Layout info = DoPrepareSave();
        if (info.NumChairs <= 25 && info.NumChairs >= 6)
        {
            if (!Tool.CanMini())
            {
                mError.Text = "بخش مینی بوس فعال نیست. تعداد صندلیها نمیتواند بین 6 تا 25 باشد";
                puEx.Show();
                return;
            }
        }
        if (info.NumChairs <= 5 && info.NumChairs >= 1)
        {
            if (!Tool.CanSewari())
            {
                mError.Text = "بخش سواری فعال نیست. تعداد صندلیها نمیتواند بین 1 تا 5 باشد";
                puEx.Show();
                return;
            }
        }
        if (info.NumChairs >= 25)
        {
            if (!Tool.CanBus())
            {
                mError.Text = "بخش اتوبوس فعال نیست. تعداد صندلیها نمیتواند بیشتر از 24 تا باشد";
                puEx.Show();
                return;
            }
        }
        if (Helper.Instance.Update())
            BindData();
        else
            mError.Text = Helper.Instance.LastException.Message;
         
        HandleLayout(info);
        EditID = info.ID;
        puEx.Show();
    }
    protected void puOk2_Click(object sender, EventArgs e)
    {
        Layout info = DoPrepareSave();
        if (info.NumChairs <= 24 && info.NumChairs >= 6)
        {
            if (!Tool.CanMini())
            {
                mError.Text = "بخش مینی بوس فعال نیست. تعداد صندلیها نمیتواند بین 6 تا 24 باشد";
                puEx.Show();
                return;
            }
        }
        if (info.NumChairs <= 5 && info.NumChairs >= 1)
        {
            if (!Tool.CanSewari())
            {
                mError.Text = "بخش سواری فعال نیست. تعداد صندلیها نمیتواند بین 1 تا 5 باشد";
                puEx.Show();
                return;
            }
        }
        if (info.NumChairs >= 25)
        {
            if (!Tool.CanBus())
            {
                mError.Text = "بخش اتوبوس فعال نیست. تعداد صندلیها نمیتواند بیشتر از 24 تا باشد";
                puEx.Show();
                return;
            }
        }
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
             HandleLayout(info);
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Layout info = e.Row.DataItem as Layout;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Layout info = Helper.Instance.GetLayout(EditID);
        if (info == null)
            return;

        mEnabled.Enabled = info.Enabled;
        mTitle.Text = info.Title;
        //info.Attachment = mAttachment.Text;
        mComment.Text = info.Comments;
        mColumns.Text = info.Columns.ToString();
        mColumnSpace.Text = info.ColumnSpace.ToString();
        mRows.Text = info.Rows.ToString();
        //info.Layout1 = mLayout.Text;
        mFloor.Text = info.Floors.ToString();

        HandleLayout(info);

        puEx.Show();
    }
    private void HandleLayout(Layout info)
    {
        // Handle Layout        
        for (int f = 0; f < info.Floors; f++)
        {
            mLayout.Controls.Add(new LiteralControl(string.Format("طبقه {0}:<br />", f + 1)));
            mLayout.Controls.Add(new LiteralControl("<table cellpadding=0>"));
            for (int i = 0; i < info.Rows; i++)
            {
                mLayout.Controls.Add(new LiteralControl("<tr>"));
                for (int j = 0; j < info.Columns; j++)
                {
                    CheckBox cb = new CheckBox();
                    cb.ID = string.Format("cb{0}_{1}_{2}", f, i, j);
                    cb.Checked = info.Layout1 != null && info.Layout1.Contains(string.Format(";{0}_{1}_{2};", f, i, j));
                    mLayout.Controls.Add(new LiteralControl("<td>"));
                    mLayout.Controls.Add(cb);
                    mLayout.Controls.Add(new LiteralControl("</td>"));
                    if (info.ColumnSpace == j + 1)
                        mLayout.Controls.Add(new LiteralControl("<td>&nbsp;-&nbsp;</td>"));
                }
                mLayout.Controls.Add(new LiteralControl("</tr>"));
            }
            mLayout.Controls.Add(new LiteralControl("</table>"));
        }

    }
    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteLayout(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mEnabled.Checked = true;
        mTitle.Text = mComment.Text = "";
        mColumns.Text = "4";
        mColumnSpace.Text = "2";
        mRows.Text = "11";
        mFloor.Text = "1";
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
