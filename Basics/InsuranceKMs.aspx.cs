﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Basics_InsuranceKMs : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            this.InsuranceID = Tool.GetInt(Request.QueryString["insuranceid"], Null.NullInteger);
            Insurance ins = Helper.Instance.GetInsurance(this.InsuranceID);
            if (ins == null)
                Response.Redirect("Insurances.aspx", true);
            mInsurance.Text = string.Format("{0} ({1})", ins.InsuranceType.Title, ins.Comments);
            BindData();
        }
    }
   
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.InsuranceKMs
                  where c.InsuranceID == this.InsuranceID
                  select c;
        //int costFrom = Tool.GetInt(sCostFrom.Text, Null.NullInteger);
        //if (costFrom != Null.NullInteger)
        //    all = all.Where(c => c.Cost >= costFrom);

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        InsuranceKM info;
        if (Null.NullInteger == EditID)
            info = new InsuranceKM();
        else
            info = Helper.Instance.GetInsuranceKM(EditID);
        if (info == null)
            return;

        info.InsuranceID = this.InsuranceID;
        info.StartKM = Tool.GetInt(mStartKM.Text, 0);
        info.EndKM = Tool.GetInt(mEndKM.Text, 0);
        info.Price = Tool.GetInt(mPrice.Text, 0);

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.InsuranceKMs.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            InsuranceKM info = e.Row.DataItem as InsuranceKM;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        InsuranceKM info = Helper.Instance.GetInsuranceKM(EditID);
        if (info == null)
            return;
        mStartKM.Text = info.StartKM.ToString();
        mEndKM.Text = info.EndKM.ToString();
        mPrice.Text = info.Price.ToString();
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteInsuranceKM(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected int InsuranceID
    {
        get
        {
            return Tool.GetInt(ViewState["InsuranceID"], Null.NullInteger);
        }
        set
        {
            ViewState["InsuranceID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mStartKM.Text = "";
        mEndKM.Text = "";
        mPrice.Text = "";
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
