﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Basics_CarTypes : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Layout> all = Helper.Instance.GetLayouts();
            mLayoutID.DataValueField = "ID";
            mLayoutID.DataTextField = "Title";
            mLayoutID.DataSource = all;
            mLayoutID.DataBind();

            Layout none = new Layout();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sLayout.DataValueField = "ID";
            sLayout.DataTextField = "Title";
            sLayout.DataSource = all;
            sLayout.DataBind();
        }
        {
            List<Special> all = Helper.Instance.GetSpecials();
            mSpecialID.DataValueField = "ID";
            mSpecialID.DataTextField = "Title";
            mSpecialID.DataSource = all;
            mSpecialID.DataBind();
        }
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.CarTypes
                  select c;
        if (sLayout.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sLayout.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.LayoutID == sID);
        }
        if (sCarTitle.Text.Trim().Length > 0)
            all = all.Where(c => c.Title.Contains(sCarTitle.Text.Trim()));
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        CarType info;
        if (Null.NullInteger == EditID)
            info = new CarType();
        else
            info = Helper.Instance.GetCarType(EditID);
        if (info == null)
            return;

        info.Title = mTitle.Text;
        info.Comments = mComments.Text;
        Layout lay = Helper.Instance.GetLayout(Tool.GetInt(mLayoutID.SelectedValue, Null.NullInteger));
        info.Layout = lay;
        int specialid = info.SpecialID;
        int mspecialid = Tool.GetInt(mSpecialID.SelectedValue, Null.NullInteger);

        info.Enabled = mEnabled.Checked;
        info.SpecialID = Tool.GetInt(mSpecialID.SelectedValue, Null.NullInteger);
        info.Special = Helper.Instance.GetSpecial(info.SpecialID);
        
        
        info.Code = Tool.GetInt(mCode2.SelectedValue, 0);
        if (info.Code <= 0)
        {
            mError.Text = "لطفا یک کد را انتخاب کنید";
            puEx.Show();
            return;
        }
        if (info.Code == Null.NullInteger)
            info.Code = null;
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.CarTypes.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            //gereftane service hai ke az in cartype estefade kardeand va agar tedade anha bozorgtar az 1 bood alert dahad//
            //**
            if (specialid != mspecialid)
            {
                List<Service> all = Helper.Instance.GetServicesBySpecial(specialid);
                foreach (var i in all)
                {
                    i.SpecialID = info.SpecialID;
                    Helper.Instance.Update();
                }
            }
            //**
            mTitle.Text = mComments.Text = "";
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CarType info = e.Row.DataItem as CarType;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[3].Text = info.Layout.Title;
                e.Row.Cells[5].Text = info.Special.Title;

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        CarType info = Helper.Instance.GetCarType(EditID);
        if (info == null)
            return;
        mTitle.Text = info.Title.ToString();
        mComments.Text = info.Comments.ToString();
        if (info.Code.HasValue)
            Tool.SetSelected(mCode2, info.Code.Value);
        else
            Tool.SetSelected(mCode2, 0);
        //mCode.Text = info.Code.HasValue ? info.Code.Value.ToString() : "";
        Tool.SetSelected( mSpecialID, info.SpecialID);
        mEnabled.Enabled = info.Enabled;
        Tool.SetSelected(mLayoutID, info.LayoutID);
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteCarType(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mEnabled.Checked = true;
        mComments.Text = mTitle.Text = "";
        Tool.SetSelected(mCode2, 0);
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
