﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Wages.aspx.cs" Inherits="Basics_Wages" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        نوع بیمه
                    </td>
                    <td>
                        <asp:DropDownList ID="sType" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" Text="جديد INS" OnClick="doAdd_Click" CssClass="CB"
                            Width="50px"></asp:Button>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                AllowPaging="true" OnPageIndexChanging="list_PageIndexChanging" PageSize="50"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Type" HeaderText="نوع بیمه" />
                    <asp:BoundField DataField="IsForeign" HeaderText="کشور" />
                    <asp:BoundField DataField="Auto" HeaderText="نوع اتوکار" />
                    <asp:BoundField DataField="KMFrom" HeaderText="مسافت از" />
                    <asp:BoundField DataField="KMTo" HeaderText="مسافت تا" />
                    <asp:BoundField DataField="Price" HeaderText="مبلغ" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش کارمزد</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نوع بیمه
                            </td>
                            <td>
                                <asp:DropDownList ID="mType" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کشور
                            </td>
                            <td>
                                <asp:CheckBox ID="mIsForeignNo" runat="server" Text="داخلی" />
                                <asp:CheckBox ID="mIsForeignYes" runat="server" Text="خارجی" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نوع اتوکار
                            </td>
                            <td>
                                <asp:DropDownList ID="mAuto" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مسافت از
                            </td>
                            <td>
                                <asp:TextBox ID="mKMFrom" runat="server" />
                            </td>
                            <td>
                             <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="اين مقدار الزامي است"
                                ControlToValidate="mKMFrom" Display="Dynamic"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mKMFrom" />
                            <asp:RangeValidator ID="RangeValidator1" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mKMFrom" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 1,000,000 باشد"
                                MaximumValue="1000000" MinimumValue="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مسافت تا
                            </td>
                            <td>
                                <asp:TextBox ID="mKMTo" runat="server" />
                            </td>
                            <td>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="addGroup" ErrorMessage="اين مقدار الزامي است"
                                ControlToValidate="mKMTo" Display="Dynamic"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mKMTo" />
                            <asp:RangeValidator ID="RangeValidator2" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mKMTo" ErrorMessage="اين مقدار بايد يک عدد بين 1 و 1,000,000 باشد"
                                MaximumValue="1000000" MinimumValue="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مبلغ
                            </td>
                            <td>
                                <asp:TextBox ID="mPrice" runat="server" />
                            </td>
                            <td>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="addGroup" ErrorMessage="اين مقدار الزامي است"
                                ControlToValidate="mPrice" Display="Dynamic"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                FilterType="Numbers" TargetControlID="mPrice" />
                            <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                                ValidationGroup="addGroup" ControlToValidate="mPrice" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 1,000,000 باشد"
                                MaximumValue="1000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
