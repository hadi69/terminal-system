﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Basics_Wages : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        sType.Items.Clear();
        sType.Items.Add(new ListItem("بیمه بدنه", "0"));
        sType.Items.Add(new ListItem("بیمه سرنشین", "1"));

        mType.Items.Clear();
        mType.Items.Add(new ListItem("بیمه بدنه", "0"));
        mType.Items.Add(new ListItem("بیمه سرنشین", "1"));

        mAuto.Items.Clear();
        mAuto.Items.Add(new ListItem("اتوبوس", "0"));
        mAuto.Items.Add(new ListItem("مینی بوس", "1"));
        mAuto.Items.Add(new ListItem("سواری", "2"));
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Wages
                  select c;// new { c.ID, c.Title, c.Code, c.Enabled, c.CountryID, CountryTitle = c.Country.Title };

        if (sType.SelectedIndex > 0)
        {
            int cntID = Tool.GetInt(sType.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.Type == cntID);
        }
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Wage info;
        if (Null.NullInteger == EditID)
            info = new Wage();
        else
            info = Helper.Instance.GetWage(EditID);
        if (info == null)
            return;

        info.Type = Tool.GetInt(mType.SelectedValue, Null.NullInteger);
        info.Auto = Tool.GetInt(mAuto.SelectedValue, Null.NullInteger);
        info.IsForeign = mIsForeignYes.Checked;
        info.KMFrom = Tool.GetInt(mKMFrom.Text, 0);
        info.KMTo = Tool.GetInt(mKMTo.Text, 0);
        info.Price = Tool.GetInt(mPrice.Text, 0);

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Wages.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Wage info = e.Row.DataItem as Wage;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Type == 0 ? "بیمه بدنه" : "بیمه سرنشین";
                e.Row.Cells[3].Text = info.IsForeign ? "خارجی" : "داخلی";
                e.Row.Cells[4].Text = info.Auto == 0 ? "اتوبوس" : info.Auto == 1 ? "مینی بوس" : "سواری";

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Wage info = Helper.Instance.GetWage(EditID);
        if (info == null)
            return;

        Tool.SetSelected(mType, info.Type);
        Tool.SetSelected(mAuto, info.Auto);
        mIsForeignYes.Checked = info.IsForeign;
        mIsForeignNo.Checked = !info.IsForeign;
        mKMFrom.Text = info.KMFrom.ToString();
        mKMTo.Text = info.KMTo.ToString();
        mPrice.Text = info.Price.ToString();

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteWage(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mKMFrom.Text = mKMTo.Text = mPrice.Text = "";
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        list.PageIndex = 0;
        BindData();
    }
}
