﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Services_Pathes : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        List<City> all = Helper.Instance.GetCities(true);
        mSrcCityID.DataValueField = mDestCityID.DataValueField = "ID";
        mSrcCityID.DataTextField = mDestCityID.DataTextField = "Title";
        mSrcCityID.DataSource = mDestCityID.DataSource = all;
        mSrcCityID.DataBind();
        mDestCityID.DataBind();

        City none = new City();
        none.ID = Null.NullInteger;
        none.Title = "همه";
        all.Insert(0, none);

        sSrcCityID.DataValueField = sDestCityID.DataValueField = "ID";
        sSrcCityID.DataTextField = sDestCityID.DataTextField = "Title";
        sSrcCityID.DataSource = sDestCityID.DataSource = all;
        sSrcCityID.DataBind();
        sDestCityID.DataBind();
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Paths
                  select c;// new { c.ID, c.Title, c.Code, c.Enabled, c.CountryID, CountryTitle = c.Country.Title };

        if (sSrcCityID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sSrcCityID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.SrcCityID == sID);
        }
        if (sDestCityID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestCityID.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.DestCityID == sID);
        }
        if (sCode.Text.Trim().Length > 0)
            all = all.Where(c => c.Code.Contains(sCode.Text.Trim()));

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Path info;
        if (Null.NullInteger == EditID)
            info = new Path();
        else
            info = Helper.Instance.GetPath(EditID);
        if (info == null)
            return;

        info.Comments = mComments.Text;
        info.KMs = Tool.GetInt(mKMs.Text, Null.NullInteger);
        info.Code = mCode.Text;
        info.SrcCityID = Tool.GetInt(mSrcCityID.SelectedValue, Null.NullInteger);
        info.DestCityID = Tool.GetInt(mDestCityID.SelectedValue, Null.NullInteger);
        info.Enabled = mEnabled.Checked;

        if (info.SrcCityID == info.DestCityID)
        {
            mError.Text = "شهر مبدا و مقصد نبايد يکسان باشند";
            puEx.Show();
            return;
        }
        Path old = Helper.Instance.GetPath(info.SrcCityID, info.DestCityID);
        if (old != null && old.ID != info.ID)
        {
            mError.Text = "مسیر تکراری است";
            puEx.Show();
            return;
        }
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Paths.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Path info = e.Row.DataItem as Path;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.City != null ? info.City.Title : "---";
                e.Row.Cells[3].Text = info.City1 != null ? info.City1.Title : "---";

                //e.Row.Cells[2].Text = info.City.Title ;
                //e.Row.Cells[3].Text = info.City1.Title;

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Path info = Helper.Instance.GetPath(EditID);
        if (info == null)
            return;
        mComments.Text = info.Comments;
        mCode.Text = info.Code;
        mKMs.Text = info.KMs.ToString();
        mEnabled.Checked = info.Enabled;
        Tool.SetSelected(mSrcCityID, info.SrcCityID);
        Tool.SetSelected(mDestCityID, info.DestCityID);
       
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeletePath(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.";// +Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mEnabled.Checked = true;
        mComments.Text = mCode.Text = "";
        City p = Helper.Instance.GetCity(Helper.Instance.GetSettingValue("DefaultSrcCity"));
        if (p != null)
            Tool.SetSelected(mSrcCityID, p.ID);
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
