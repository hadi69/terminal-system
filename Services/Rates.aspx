﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Rates.aspx.cs" Inherits="Services_Rates" Title="قیمت گذاری دوره ای" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        عنوان نرخ
                    </td>
                    <td>
                        <asp:TextBox ID="sTitle" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        تاريخ شروع
                    </td>
                    <td>
                        <uc1:DatePicker ID="sStartDate" runat="server" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" Width="50px" OnClick="doAdd_Click"
                            Text="جديد INS" />
                    </td>
                </tr>
                <tr>
                    <td>
                        توضيحات
                    </td>
                    <td>
                        <asp:TextBox ID="sComments" runat="server" Width="284px"></asp:TextBox>
                    </td>
                    <td>
                        تاريخ پايان
                    </td>
                    <td>
                        <uc1:DatePicker ID="sEndDate" runat="server" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                OnRowCommand="list_RowCommand">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:HyperLinkField HeaderText="عنوان نرخ" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="RateItems.aspx?rateid={0}"
                        DataTextField="Title" />
                    <asp:BoundField DataField="StartDate2" HeaderText="تاريخ شروع" />
                    <asp:BoundField DataField="EndDate2" HeaderText="تاريخ پايان" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:ButtonField CommandName="UpdateServices" Text="بروزرسانی سرویسها" />
                    <asp:ButtonField CommandName="GetPrices" Text="گرفتن قیمت از" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش قیمت گذاری دوره ای</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                عنوان نرخ
                            </td>
                            <td>
                                <asp:TextBox ID="mRate" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="عنوان نرخ الزامي است"
                                    ControlToValidate="mRate" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ شروع
                            </td>
                            <td>
                                <asp:TextBox ID="mStartDate" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="تاريخ شروع الزامي است"
                                    ControlToValidate="mStartDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:MaskedEditExtender ID="mee2" TargetControlID="mStartDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                تاريخ پايان
                            </td>
                            <td>
                                <asp:TextBox ID="mEndDate" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="refv2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="کدتاريخ پايان الزامي است" ControlToValidate="mEndDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:MaskedEditExtender ID="mee1" TargetControlID="mEndDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComment" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="puEx2" runat="server" TargetControlID="puDrag2" PopupDragHandleControlID="puDrag2"
                PopupControlID="puPanel2" OkControlID="puCancel2" DropShadow="true" CancelControlID="puCancel2"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel2" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag2" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    گرفتن قیمت گذاری دوره ای</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                عنوان نرخ
                            </td>
                            <td>
                                <asp:DropDownList ID="mRates2" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                عنوان قیمت
                            </td>
                            <td>
                                <asp:DropDownList ID="mPrice2" runat="server">
                                    <asp:ListItem Text='قیمت' Value='0' />
                                    <asp:ListItem Text='بیمه سرنشین' Value='1' />
                                    <asp:ListItem Text='هزینه اضافی' Value='2' />
                                    <asp:ListItem Text='پذیرایی' Value='3' />
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                درصد افزایش
                            </td>
                            <td>
                                <asp:TextBox ID="mPercent2" runat="server" />
                            </td>
                            <td>
                           مقدار جدید = (1+ درصد افزایش/100) * مقدار قدیم
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="addGroup2"
                                    ErrorMessage="درصد افزایش" ControlToValidate="mPercent2" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk2" ValidationGroup="addGroup2" OnClick="puOk2_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel2" runat="server" Text="انصراف" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mErr2" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
