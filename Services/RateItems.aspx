﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RateItems.aspx.cs" Inherits="Services_RateItems" Title="قيمتهاي مربوط به جدول نرخ" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <span class="Title">قيمتهاي مربوط به قیمت گذاری دوره ای
                <asp:Label ID="mRate" runat="server" />
                :</span>
            <table border="0" class="filter">
                <tr>
                    <td>
                        ویژه
                    </td>
                    <td>
                        <asp:DropDownList ID="sSpecial" runat="server" Width="150px" />
                    </td>
                    <td>
                        مبدا
                    </td>
                    <td>
                        <asp:DropDownList ID="sSrcCity" runat="server" Width="150px" />
                    </td>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestCity" runat="server" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" Width="50px" OnClick="doAdd_Click"
                            Text="جديد INS" />
                    </td>
                </tr>
                <tr>
                    <td>
                        قيمت از
                    </td>
                    <td>
                        <asp:TextBox ID="sCostFrom" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        قيمت تا
                    </td>
                    <td>
                        <asp:TextBox ID="sCostTo" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        <!--                    
                        نوع
                        !-->
                    </td>
                    <td>
                        <asp:TextBox ID="sType" runat="server" Width="150px" Visible="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="SpecialID" HeaderText="نوع سرویس دهی" />
                    <asp:BoundField DataField="SrcCityID" HeaderText="مبدا" />
                    <asp:BoundField DataField="DestCityID" HeaderText="مقصد" />
                    <asp:BoundField DataField="PureCost" HeaderText="کسورات اولیه" />
                    <asp:BoundField DataField="Cost" HeaderText="قيمت" />
                    <asp:BoundField DataField="ExtraCost" HeaderText="هزينه اضافي" />
                    <asp:BoundField DataField="ReceptionCost" HeaderText="پذيرايي" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="650px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش قيمت</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                نوع سرویس دهی
                            </td>
                            <td>
                                <asp:DropDownList ID="mSpecialID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv11" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="نوع سرویس دهی الزامي است" ControlToValidate="mSpecialID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مبدا
                            </td>
                            <td>
                                <asp:DropDownList ID="mSrcCityID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv21" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="مبدا الزامي است" ControlToValidate="mSrcCityID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مقصد
                            </td>
                            <td>
                                <asp:DropDownList ID="mDestCityID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="مقصد الزامي است"
                                    ControlToValidate="mDestCityID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کسورات اولیه
                            </td>
                            <td>
                                <asp:TextBox ID="mPureCost" runat="server" Visible="true" />
                            </td>
                            <td> به ازای هر مسافر
                                <asp:RequiredFieldValidator Enabled="false" ID="rfv2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="کسورات اولیه الزامي است" ControlToValidate="mPureCost" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender Enabled="false" ID="FilteredTextBoxExtender1" runat="server"
                                    FilterMode="ValidChars" FilterType="Numbers" TargetControlID="mPureCost" />
                                <asp:RangeValidator ID="RangeValidator1" Enabled="false" Type="Integer" runat="server"
                                    Display="Dynamic" ValidationGroup="addGroup" ControlToValidate="mPureCost" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                قيمت
                            </td>
                            <td>
                                <asp:TextBox ID="mCost" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv3" runat="server" ValidationGroup="addGroup" ErrorMessage="قيمت الزامي است"
                                    ControlToValidate="mCost" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mCost" />
                                <asp:RangeValidator ID="RangeValidator2" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mCost" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                قيمت صندلی جلو
                            </td>
                            <td>
                                <asp:TextBox ID="mCost2" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mCost2" />
                                <asp:RangeValidator ID="RangeValidator8" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mCost2" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                قیمت کمیسیون
                            </td>
                            <td>
                                <asp:TextBox ID="mCommissionPrice" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mReceptionCost" />
                                <asp:RangeValidator ID="RangeValidator9" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mCommissionPrice" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                پذيرايي
                            </td>
                            <td>
                                <asp:TextBox ID="mReceptionCost" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv4" runat="server" ValidationGroup="addGroup" ErrorMessage="پذيرايي الزامي است"
                                    ControlToValidate="mReceptionCost" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mReceptionCost" />
                                <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mReceptionCost" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                بيمه سرنشين
                            </td>
                            <td>
                                <asp:TextBox ID="mInsurance" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv5" runat="server" ValidationGroup="addGroup" ErrorMessage="بيمه سرنشين الزامي است"
                                    ControlToValidate="mInsurance" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mInsurance" />
                                <asp:RangeValidator ID="RangeValidator4" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                بيمه هر نفر
                            </td>
                            <td>
                                <asp:TextBox ID="mMaxInsurance" runat="server" Enabled="false">
                                </asp:TextBox>
                            </td>
                            <td>
                                تعداد صندلی اتوکار/بيمه سرنشين
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mMaxInsurance" />
                                <asp:RangeValidator ID="RangeValidator5" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mMaxInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                درصد عوارض
                            </td>
                            <td>
                                <asp:TextBox ID="mToll" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                %
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="عوارض الزامي است" ControlToValidate="mToll" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" ValidChars='.0123456789'
                                    TargetControlID="mToll" />
                                <asp:RangeValidator ID="RangeValidator6" Type="Double" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mToll" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                هزينه اضافي
                            </td>
                            <td>
                                <asp:TextBox ID="mExtraCost" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mExtraCost" />
                                <asp:RangeValidator ID="RangeValidator7" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mExtraCost" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                شرح هزينه اضافي
                            </td>
                            <td>
                                <asp:TextBox ID="mExtraCostComment" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                <!--
                                نوع
                                !-->
                            </td>
                            <td>
                                <asp:TextBox ID="mType" runat="server" Visible="false" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td colspan="2">
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="doClose" OnClick="doClose_Click" runat="server" Text="بستن" CssClass="CB">
                                </asp:Button>&nbsp;
                                <asp:Button ID="doUpdateServices" OnClick="doUpdateServices_Click" runat="server"
                                    Text="بروزرسانی سرویسها" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
