﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Services_Rates : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Rates
                  select c;
        if (sTitle.Text.Trim().Length > 0)
            all = all.Where(c => c.Title.Contains(sTitle.Text.Trim()));
        if (sComments.Text.Trim().Length > 0)
            all = all.Where(c => c.Comments.Contains(sComments.Text.Trim()));
        DateTime sd = sStartDate.SelectedDate;
        if (sd != Null.NullDate)
            all = all.Where(c => c.EndDate >= sd);
        DateTime ed = sEndDate.SelectedDate;
        if (ed != Null.NullDate)
            all = all.Where(c => c.StartDate <= ed);
        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Rate info;
        if (Null.NullInteger == EditID)
            info = new Rate();
        else
            info = Helper.Instance.GetRate(EditID);
        if (info == null)
            return;

        if (!Tool.ValidatePersianDate(mStartDate.Text))
        {
            mError.Text = "تاريخ شروع اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        if (!Tool.ValidatePersianDate(mEndDate.Text))
        {
            mError.Text = "تاريخ پايان اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        DateTime startDate = Tool.ParsePersianDate(mStartDate.Text, info.StartDate);
        DateTime endDate = Tool.ParsePersianDate(mEndDate.Text, info.EndDate);
        Rate old = Helper.Instance.GetRate(startDate);
        if (old != null && old.ID != info.ID)
        {
            mError.Text = "تاريخ شروع وارد شده با قیمتگذاریهای دیگر همپوشانی دارد.";
            puEx.Show();
            return;
        }
        old = Helper.Instance.GetRate(endDate);
        if (old != null && old.ID != info.ID)
        {
            mError.Text = "تاريخ پایان وارد شده با قیمتگذاریهای دیگر همپوشانی دارد.";
            puEx.Show();
            return;
        }

        info.Title = mRate.Text;
        info.Enabled = mEnabled.Checked;
        info.StartDate = Tool.ParsePersianDate(mStartDate.Text, info.StartDate);
        info.EndDate = Tool.ParsePersianDate(mEndDate.Text, info.EndDate);
        info.Comments = mComment.Text;
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Rates.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {

            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Rate info = e.Row.DataItem as Rate;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                LinkButton ctl = e.Row.Cells[6].Controls[0] as LinkButton;
                ctl.CommandArgument = info.ID.ToString();
                ctl = e.Row.Cells[7].Controls[0] as LinkButton;
                ctl.CommandArgument = info.ID.ToString();
                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Rate info = Helper.Instance.GetRate(EditID);
        if (info == null)
            return;
        mRate.Text = info.Title.ToString();
        mEnabled.Enabled = info.Enabled;
        mStartDate.Text = Tool.ToPersianDate(info.StartDate, "");
        mEndDate.Text = Tool.ToPersianDate(info.EndDate, "");
        mComment.Text = info.Comments;
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        List<RateItem> items = Helper.Instance.GetRateItems(id);
        if (null != items && items.Count > 0)
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد";
            return;
        }
        if (Helper.Instance.DeleteRate(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mRate.Text = mComment.Text = "";
        mEnabled.Checked = true;
        mStartDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        mEndDate.Text = Tool.ToPersianDate(DateTime.Now.AddMonths(1), "");
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void list_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == null)
            return;
        int id = Tool.GetInt(e.CommandArgument, Null.NullInteger);
        if (id == Null.NullInteger)
            return;
        if (e.CommandName.ToLower().Equals("updateservices"))
        {
            #region UpdateServices
            List<RateItem> items = Helper.Instance.GetRateItems(id);
            if (items == null || items.Count == 0)
            {
                mMsg.Text = "قیمت گذاری دارای آیتم نیست";
                return;
            }
            int count = 0;
            string errors = "";
            foreach (RateItem info in items)
            {
                List<Trip> trips = null;
                try
                {
                    var c = from cs in Helper.Instance.DB.Trips
                            where cs.Closed == false && (cs.Service.SpecialID == info.SpecialID)
                            && (cs.Service.Path.SrcCityID == info.SrcCityID) && (cs.Service.Path.DestCityID == info.DestCityID)
                            && info.Rate.StartDate <= cs.Date && cs.Date <= info.Rate.EndDate
                            select cs;
                    trips = c.ToList();
                }
                catch
                {
                }
                if (trips != null && trips.Count > 0)
                {
                    for (int i = 0; i < trips.Count; i++)
                    {
                        trips[i].Price = info.Cost;
                        trips[i].Toll = info.Toll;
                        trips[i].Insurance = info.Insurance;
                        trips[i].Reception = info.ReceptionCost;
                    }
                    if (Helper.Instance.Update())
                        count += trips.Count;

                    else
                        errors += Helper.Instance.LastException.Message + "<br />";
                }
            }
            mMsg.Text = string.Format("تعدا {0} سرویس باز بروزرسانی شد<br />{1}", count, errors);
            #endregion
        }
        else if (e.CommandName.ToLower().Equals("getprices"))
        {
            EditID = id;
            var all = from c in Helper.Instance.DB.Rates
                      select c;
            mRates2.DataTextField = "Title";
            mRates2.DataValueField = "ID";
            mRates2.DataSource = all;
            mRates2.DataBind();
            mPercent2.Text = "0";
            puEx2.Show();
        }
    }
    protected void puOk2_Click(object sender, EventArgs e)
    {
        int what = Tool.GetInt(mPrice2.SelectedValue, 0);
        float percent = Tool.GetFloat(mPercent2.Text, 0);
        percent = (1 + percent / 100);
        Rate from = Helper.Instance.GetRate(Tool.GetInt(mRates2.SelectedValue, Null.NullInteger));
        Rate info = Helper.Instance.GetRate(EditID);
        if (from == null || info == null)
            return;
        List<RateItem> items = Helper.Instance.GetRateItems(from.ID);
        if (items == null || items.Count == 0)
        {
            mMsg.Text = "قیمت گذاری مبدا انتخاب شده دارای آیتم نیست";
            return;
        }
        List<RateItem> toAdd = new List<RateItem>();
        for (int i = 0; i < items.Count; i++)
        {
            RateItem item = null;
            if (from != info)
            {
                // look for existance
                item = Helper.Instance.GetRateItemBySpecial(info.ID, items[i].SrcCityID, items[i].DestCityID, items[i].SpecialID, Null.NullDate);
                if (item == null)
                {
                    #region Add new Rate
                    item = new RateItem();
                    item.Rate = info;
                    item.RateID = info.ID;
                    item.City = items[i].City;
                    item.City1 = items[i].City1;
                    item.Comments = items[i].Comments;
                    item.Cost = items[i].Cost;
                    item.Cost2 = items[i].Cost2;
                    item.DestCityID = items[i].DestCityID;
                    item.Enabled = items[i].Enabled;
                    item.ExtraCost = items[i].ExtraCost;
                    item.ExtraCostComment = items[i].ExtraCostComment;
                    item.Insurance = items[i].Insurance;
                    item.PureCost = items[i].PureCost;
                    item.ReceptionCost = items[i].ReceptionCost;
                    item.Special = items[i].Special;
                    item.SpecialID = items[i].SpecialID;
                    item.SrcCityID = items[i].SrcCityID;
                    item.Toll = items[i].Toll;
                    item.Type = items[i].Type;

                    toAdd.Add(item);
                    #endregion
                }
            }
            else
                item = items[i];
            #region Update Value
            item.Cost = items[i].Cost;
            item.Cost2 = items[i].Cost2;
            item.ExtraCost = items[i].ExtraCost;
            item.ExtraCostComment = items[i].ExtraCostComment;
            item.Insurance = items[i].Insurance;
            item.PureCost = items[i].PureCost;
            item.ReceptionCost = items[i].ReceptionCost;
            switch (what)
            {
                case 0:
                    item.Cost = (int)(item.Cost * percent);
                    break;
                case 1:
                    item.Insurance = (int)(item.Insurance * percent);
                    break;
                case 2:
                    item.ExtraCost = (int)(item.ExtraCost * percent);
                    break;
                case 3:
                    item.ReceptionCost = (int)(item.ReceptionCost * percent);
                    break;
                default:
                    break;
            }
            #endregion
        }
        if (toAdd.Count > 0)
            Helper.Instance.DB.RateItems.InsertAllOnSubmit(toAdd);
        if (Helper.Instance.Update())
            mMsg.Text = "گرفتن قیمت با موفقیت انجام شد";
        else
            mMsg.Text = Helper.Instance.LastException.Message;
    }

}
