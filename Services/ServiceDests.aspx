﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ServiceDests.aspx.cs" Inherits="Services_ServiceDests" Title="مقاصد مربوط به سرويس" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <span class="Title">شهرهای بین راه مربوط به سرويس
                <asp:Label ID="mService" runat="server" />
                :</span>
            <asp:Button ID="doAdd" runat="server" CssClass="CB" Width="50px" OnClick="doAdd_Click"
                Text="جديد INS" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="CityID" HeaderText="شهر" />
                    <asp:BoundField DataField="Price" HeaderText="قيمت" />
                    <asp:BoundField DataField="Toll" HeaderText="درصد عوارض" />
                    <asp:BoundField DataField="Insurance" HeaderText="بيمه سرنشين" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش مقصد</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                شهر
                            </td>
                            <td>
                                <asp:DropDownList ID="mCityID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv11" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="شهر الزامي است" ControlToValidate="mCityID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                قيمت
                            </td>
                            <td>
                                <asp:TextBox ID="mPrice" runat="server" />
                            </td>
                            <td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mPrice" />
                                <asp:RangeValidator ID="RangeValidator1" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mPrice" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                درصد عوارض
                            </td>
                            <td>
                                <asp:TextBox ID="mToll" runat="server" Enabled="false" />
                            </td>
                            <td>
                                از "تعریف سرویس مقصد" خوانده میشود 
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                    TargetControlID="mToll" ValidChars='.0123456789' />
                                <asp:RangeValidator ID="RangeValidator2" Type="Double" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mToll" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                بيمه سرنشين
                            </td>
                            <td>
                                <asp:TextBox ID="mInsurance" runat="server" Enabled="false" />
                            </td>
                            <td>
                                از "تعریف سرویس مقصد" خوانده میشود 
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mInsurance" />
                                <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
