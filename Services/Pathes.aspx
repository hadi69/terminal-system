﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Pathes.aspx.cs" Inherits="Services_Pathes" Title="مسيرها" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                     <td>
                    </td>
                    <td>
                    </td>
                     <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        مبدا
                    </td>
                    <td>
                        <asp:DropDownList ID="sSrcCityID" runat="server" Width="150px" />
                    </td>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestCityID" runat="server" Width="150px" />
                    </td>
                    <td>
                        کد
                    </td>
                    <td>
                        <asp:TextBox ID="sCode" runat="server" Width="150px"></asp:TextBox>
                    </td>
                   
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="SrcCityID" HeaderText="مبدا" />
                    <asp:BoundField DataField="DestCityID" HeaderText="مقصد" />
                    <asp:BoundField DataField="Code" HeaderText="کد" />
                    <asp:BoundField DataField="KMs" HeaderText="کيلومتر" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="نمايش" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate><br /><span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate><HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش مسير (خط سير)</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                مبدا
                            </td>
                            <td>
                                <asp:DropDownList ID="mSrcCityID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="مبدا الزامي است" ControlToValidate="mSrcCityID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                مقصد
                            </td>
                            <td>
                                <asp:DropDownList ID="mDestCityID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="addGroup" ErrorMessage="مقصد الزامي است"
                                    ControlToValidate="mDestCityID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کد
                            </td>
                            <td>
                                <asp:TextBox ID="mCode" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfv2" runat="server" ValidationGroup="addGroup" ErrorMessage="کد الزامي است"
                                    ControlToValidate="mCode" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                کيلومتر
                            </td>
                            <td>
                                <asp:TextBox ID="mKMs" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvr2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="کيلومتر الزامي است" ControlToValidate="mKMs" Display="Dynamic"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender runat="server" FilterMode="ValidChars" FilterType="Numbers"
                                    TargetControlID="mKMs" />
                                <asp:RangeValidator Type="Integer" runat="server" Display="Dynamic" ValidationGroup="addGroup"
                                    ControlToValidate="mKMs" ErrorMessage="کيلومتر بايد يک عدد بين 1 و 999999 باشد"  MaximumValue="999999" MinimumValue="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                توضيحات
                            </td>
                            <td>
                                <asp:TextBox ID="mComments" runat="server">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                                نمايش
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true"></asp:CheckBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">
                            </td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
