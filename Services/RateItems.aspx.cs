﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Services_RateItems : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            this.RateID = Tool.GetInt(Request.QueryString["rateid"], Null.NullInteger);
            Rate rate = Helper.Instance.GetRate(this.RateID);
            if (rate == null)
                Response.Redirect("Rates.aspx", true);
            mRate.Text = string.Format("{0} ({1} - {2})", rate.Title, rate.StartDate2, rate.EndDate2);
            BindInitData();
            BindData();
        }
    }
    private void BindInitData()
    {
        {
            List<Special> all = Helper.Instance.GetSpecials();
            mSpecialID.DataValueField = "ID";
            mSpecialID.DataTextField = "Title";
            mSpecialID.DataSource = all;
            mSpecialID.DataBind();

            Special none = new Special();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sSpecial.DataValueField = "ID";
            sSpecial.DataTextField = "Title";
            sSpecial.DataSource = all;
            sSpecial.DataBind();
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            mSrcCityID.DataValueField = mDestCityID.DataValueField = "ID";
            mSrcCityID.DataTextField = mDestCityID.DataTextField = "Title";
            mSrcCityID.DataSource = mDestCityID.DataSource = all;
            mSrcCityID.DataBind();
            mDestCityID.DataBind();

            City none = new City();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sSrcCity.DataValueField = sDestCity.DataValueField = "ID";
            sSrcCity.DataTextField = sDestCity.DataTextField = "Title";
            sSrcCity.DataSource = sDestCity.DataSource = all;
            sSrcCity.DataBind();
            sDestCity.DataBind();
        }
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.RateItems
                  where c.RateID == this.RateID
                  select c;
        if (sSpecial.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sSpecial.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.SpecialID == sID);
        }
        if (sSrcCity.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sSrcCity.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.SrcCityID == sID);
        }
        if (sDestCity.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestCity.SelectedValue, Null.NullInteger);
            all = all.Where(c => c.DestCityID == sID);
        }
        int costFrom = Tool.GetInt(sCostFrom.Text, Null.NullInteger);
        if (costFrom != Null.NullInteger)
            all = all.Where(c => c.Cost >= costFrom);
        int costTo = Tool.GetInt(sCostTo.Text, Null.NullInteger);
        if (costTo != Null.NullInteger)
            all = all.Where(c => c.Cost >= costTo);

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        RateItem info;
        if (Null.NullInteger == EditID)
            info = new RateItem();
        else
            info = Helper.Instance.GetRateItem(EditID);
        if (info == null)
            return;

        info.RateID = this.RateID;

        info.SpecialID = Tool.GetInt(mSpecialID.SelectedValue, Null.NullInteger);
        info.Special = Helper.Instance.GetSpecial(info.SpecialID);

        info.SrcCityID = Tool.GetInt(mSrcCityID.SelectedValue, Null.NullInteger);
        info.DestCityID = Tool.GetInt(mDestCityID.SelectedValue, Null.NullInteger);
        info.ExtraCost = Tool.GetInt(mExtraCost.Text, Null.NullInteger);
        info.ExtraCostComment = mExtraCostComment.Text;
        info.PureCost = Tool.GetInt(mPureCost.Text, Null.NullInteger);
        info.Cost = Tool.GetInt(mCost.Text, Null.NullInteger);
        info.Cost2 = Tool.GetInt(mCost2.Text, Null.NullInteger);
        if (info.Cost2 == Null.NullInteger)
            info.Cost2 = null;
        info.ReceptionCost = Tool.GetInt(mReceptionCost.Text, Null.NullInteger);
        info.Type = mType.Text;
        info.Insurance = Tool.GetInt(mInsurance.Text, Null.NullInteger);
        //info.MaxInsurance = Tool.GetInt(mMaxInsurance.Text, Null.NullInteger);
        //CarType ct = Helper.Instance.GetCarType(info.CarTypeID);
        //if (ct != null)
        //    try
        //    {
        //        info.MaxInsurance = info.RoomerInsurance / ct.Layout.NumChairs;
        //    }
        //    catch { }
        info.Toll = Tool.GetDecimal(mToll.Text.Replace(",", "."), 0);
        info.Comments = mComments.Text;
        info.Enabled = mEnabled.Checked;
        info.CommissionPrice = Tool.GetInt(mCommissionPrice.Text);

        RateItem old = Helper.Instance.GetRateItemBySpecial(info.RateID, info.SrcCityID, info.DestCityID, info.SpecialID, Null.NullDate);
        if (old != null && old.ID != info.ID)
        {
            mError.Text = "قيمت برای این نوع اتوکار، مبدا و مقصد تعریف شده است.";
            puEx.Show();
            return;
        }
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.RateItems.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            BindData();
            if (EditID == Null.NullInteger)
                mError.Text = "قیمت با موفقیت اضافه شد";
            else
                mError.Text = "قیمت با موفقیت بروزرسانی شد";
            EditID = info.ID;
            puEx.Show();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RateItem info = e.Row.DataItem as RateItem;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Special != null ? info.Special.Title : "--خطا--";
                e.Row.Cells[3].Text = Helper.Instance.GetCityTitle(info.SrcCityID);
                e.Row.Cells[4].Text = Helper.Instance.GetCityTitle(info.DestCityID);

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        RateItem info = Helper.Instance.GetRateItem(EditID);
        if (info == null)
            return;

        Tool.SetSelected(mSpecialID, info.SpecialID);
        Tool.SetSelected(mSrcCityID, info.SrcCityID);
        Tool.SetSelected(mDestCityID, info.DestCityID);
        mExtraCost.Text = info.ExtraCost.ToString();
        mExtraCostComment.Text = info.ExtraCostComment.ToString();
        mPureCost.Text = info.PureCost.ToString();
        mCost.Text = info.Cost.ToString();
        mCost2.Text = info.Cost2 == null ? "" : info.Cost2.ToString();
        mReceptionCost.Text = info.ReceptionCost.ToString();
        mType.Text = info.Type;
        mInsurance.Text = info.Insurance.ToString();
        //mMaxInsurance.Text = info.MaxInsurance.ToString();
        mToll.Text = info.Toll.ToString().Replace(",", ".").Replace("،", ".");
        mComments.Text = info.Comments;
        mEnabled.Checked = info.Enabled;
        mCommissionPrice.Text = Tool.GetString(info.CommissionPrice);
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;

        if (Helper.Instance.DeleteRateItem(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected int RateID
    {
        get
        {
            return Tool.GetInt(ViewState["RateID"], Null.NullInteger);
        }
        set
        {
            ViewState["RateID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        //Tool.SetSelected(mCarTypeID, info.CarTypeID);
        //Tool.SetSelected(mSrcCityID, info.SrcCityID);
        //Tool.SetSelected(mDestCityID, info.DestCityID);
        mExtraCost.Text = mExtraCostComment.Text = mPureCost.Text = mCommissionPrice.Text="";
        mCost.Text = mCost2.Text = mReceptionCost.Text = mType.Text = mInsurance.Text = "";
        mMaxInsurance.Text = mToll.Text = mComments.Text = "";
        mEnabled.Checked = true;
        mPureCost.Text = "0";
        City p = Helper.Instance.GetCity(Helper.Instance.GetSettingValue("DefaultSrcCity"));
        if (p != null)
            Tool.SetSelected(mSrcCityID, p.ID);
        mToll.Text = "5";
        mReceptionCost.Text = "0";

        mReceptionCost.Text = Helper.Instance.GetSettingValue("ReceptionCost");
        if (mReceptionCost.Text == "")
            mReceptionCost.Text = "0";
        mToll.Text = Helper.Instance.GetSettingValue("Toll");
        if (mToll.Text == "")
            mToll.Text = "5";
        mExtraCost.Text = Helper.Instance.GetSettingValue("ExtraCost");
        if (mExtraCost.Text == "")
            mExtraCost.Text = "0";


        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void doClose_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        list.SelectedIndex = -1;
    }
    protected void doUpdateServices_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == EditID)
        {
            mMsg.Text = "قیمت گذاری ذخیره نشده است";
            puEx.Show();
            return;
        }
        RateItem info = Helper.Instance.GetRateItem(EditID);
        if (info == null)
            return;

        List<Trip> trips = null;
        try
        {
            var c = from cs in Helper.Instance.DB.Trips
                    where cs.Closed == false && (cs.Service.SpecialID == info.SpecialID)
                    && (cs.Service.Path.SrcCityID == info.SrcCityID) && (cs.Service.Path.DestCityID == info.DestCityID)
                    && info.Rate.StartDate <= cs.Date && cs.Date <= info.Rate.EndDate
                    select cs;
            trips = c.ToList();
        }
        catch
        {
        }
        if (trips != null && trips.Count > 0)
        {
            for (int i = 0; i < trips.Count; i++)
            {
                trips[i].Price = info.Cost;
                trips[i].Toll = info.Toll;
                trips[i].Insurance = info.Insurance;
                trips[i].Reception = info.ReceptionCost;
            }
            if (Helper.Instance.Update())
                mError.Text = string.Format("تعدا {0} سرویس باز بروزرسانی شد", trips.Count);
            else
                mError.Text = Helper.Instance.LastException.Message;
        }
        else
        {
            mError.Text = "هیچ سرویس بازی پیدا نشد";
        }
        puEx.Show();
    }
}
