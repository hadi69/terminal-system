﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Services_ServiceDests : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            this.ServiceID = Tool.GetInt(Request.QueryString["serviceid"], Null.NullInteger);
            Service ins = Helper.Instance.GetService(this.ServiceID);
            if (ins == null)
                Response.Redirect("Services.aspx", true);
            mService.Text = string.Format("{0} - {1} - {2}", ins.ServiceType2, ins.Special.Title, ins.Path.Title);
            BindData();
        }
    }
    private void BindInitData()
    {
        List<City> all = Helper.Instance.GetCities(true);
        mCityID.DataValueField = "ID";
        mCityID.DataTextField = "Title";
        mCityID.DataSource = all;
        mCityID.DataBind();
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.ServiceDests
                  where c.ServiceID == this.ServiceID
                  select c;

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        ServiceDest info;
        if (Null.NullInteger == EditID)
            info = new ServiceDest();
        else
            info = Helper.Instance.GetServiceDest(EditID);
        if (info == null)
            return;

        info.ServiceID = this.ServiceID;
        info.CityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
        info.Price = Tool.GetInt(mPrice.Text, 0);
        info.Toll = Tool.GetInt(mToll.Text, 0);

        Service ser = Helper.Instance.GetService(this.ServiceID);
        //try
        //{
        //    RateItem rate = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, ser.Path.SrcCityID, info.CityID, ser.CarType.SpecialID, ser.Date);
        //    if (rate != null)
        //        info.Toll = rate.Toll;
        //    else
        //    {
        //        mError.Text = "برای نوع اتوکار و مسیر 'تعریف سرویس' و این مقصد، در قیمت گذاری دوره ای، قیمت تعریف نشده است تا بتوان از روی آن درصد عوارض را خواند";
        //        puEx.Show();
        //        return;
        //    }
        //}
        //catch { }
        info.Insurance = Tool.GetInt(mInsurance.Text, 0);

        if (Null.NullInteger == EditID)
            Helper.Instance.DB.ServiceDests.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ServiceDest info = e.Row.DataItem as ServiceDest;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.City.Title;

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        ServiceDest info = Helper.Instance.GetServiceDest(EditID);
        if (info == null)
            return;
        Tool.SetSelected(mCityID, info.CityID);
        mPrice.Text = info.Price.ToString();
        mToll.Text = info.Toll.ToString();
        mInsurance.Text = info.Insurance.ToString();
        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteServiceDest(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected int ServiceID
    {
        get
        {
            return Tool.GetInt(ViewState["ServiceID"], Null.NullInteger);
        }
        set
        {
            ViewState["ServiceID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mPrice.Text = mToll.Text = mInsurance.Text = "";
        Service info = Helper.Instance.GetService(this.ServiceID);
        mInsurance.Text = info.Insurance.ToString();
        mToll.Text = info.Toll.ToString();
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
