﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;

public partial class Services_Services : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mMsg.Text = mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }
        mDepartureHour.Attributes.Add("onkeyup", "autoTab('SRC', 'DST', 2);"
            .Replace("SRC", mDepartureHour.ClientID)
            .Replace("DST", mDepartureMinute.ClientID));

        if (!IsPostBack)
        {
            if (Request.QueryString["do"] == "new")
            {
                FromTrips = true;
                doAdd_Click(null, null);
                puCancel.Enabled = false;
            }
        }

    }
    private void BindInitData()
    {
        {
            List<Path> all = Helper.Instance.GetPaths(true);
            mPathID.DataValueField = "ID";
            mPathID.DataTextField = "Title";
            mPathID.DataSource = all;
            mPathID.DataBind();

            Path none = new Path();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sPathID.DataValueField = "ID";
            sPathID.DataTextField = "Title";
            sPathID.DataSource = all;
            sPathID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            mCarTypeID.DataValueField = "ID";
            mCarTypeID.DataTextField = "Title";
            mCarTypeID.DataSource = all;
            mCarTypeID.DataBind();

            CarType none = new CarType();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sCarTypeID.DataValueField = "ID";
            sCarTypeID.DataTextField = "Title";
            sCarTypeID.DataSource = all;
            sCarTypeID.DataBind();
        }
        {
            mServiceType.Items.Clear();
            sServiceType.Items.Clear();
            sServiceType.Items.Add(new ListItem("همه", "-1"));
            Array all = Enum.GetValues(typeof(ServiceTypes));
            for (int i = 0; i < all.GetLength(0); i++)
            {
                ServiceTypes st = (ServiceTypes)all.GetValue(i);
                mServiceType.Items.Add(new ListItem(Tool.ToString(st), ((int)st).ToString()));
                sServiceType.Items.Add(new ListItem(Tool.ToString(st), ((int)st).ToString()));
            }
        }

        mDepartureHour.Text = DateTime.Now.Hour.ToString().PadLeft(2, '0');
        mDepartureMinute.Text = DateTime.Now.Minute.ToString().PadLeft(2, '0');
    }
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Services
                  where c.Path == null || c.Path.Enabled == true
                  orderby c.PathID, c.DepartureTime
                  select c;
        if (sKeinPath.Checked)
            all = from c in Helper.Instance.DB.Services
                  orderby c.DepartureTime
                  select c;


        List<Service> services = all.ToList();
        if (services == null || services.Count == 0)
        {
            list.DataSource = services;
            list.DataBind();
            return;
        }
        if (sKeinPath.Checked)
        {
            for (int i = 0; i < services.Count; i++)
            {
                if (services[i].Path != null)
                {
                    services.RemoveAt(i);
                    i--;
                }
            }
        }
        if (sPathID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sPathID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < services.Count; i++)
                if (services[i].PathID != sID)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            for (int i = 0; i < services.Count; i++)
                if (services[i].CarTypeID != sID)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        if (sServiceType.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceType.SelectedValue, Null.NullInteger);
            for (int i = 0; i < services.Count; i++)
                if (services[i].ServiceType != sID)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }

        if (sShowNormals.Checked == false && sServiceType.SelectedIndex == 0)
        {
            for (int i = 0; i < services.Count; i++)
                if (services[i].ServiceType == (int)ServiceTypes.Normal)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }

        if (sEnabled.Checked && !sEnabledNo.Checked)
        {
            for (int i = 0; i < services.Count; i++)
                if (services[i].Enabled != true)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        if (!sEnabled.Checked && sEnabledNo.Checked)
        {
            for (int i = 0; i < services.Count; i++)
                if (services[i].Enabled == true)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        //if (sPathID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sPathID.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.PathID == sID);
        //}
        //if (sCarTypeID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.CarTypeID == sID);
        //}
        //if (sServiceType.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sServiceType.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.ServiceType == sID);
        //}

        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        //Session["Closeds.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        //Session["Closeds.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        if (startDate != Null.NullDate || toDate != Null.NullDate)
        {
            startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
                : DateTime.Now.AddYears(-10);
            toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
                : DateTime.Now.AddYears(10);
            for (int i = 0; i < services.Count; i++)
            {
                if ((services[i].Date < startDate || services[i].Date > toDate))
                {
                    services.RemoveAt(i);
                    i--;
                }
            }
        }

        list.DataSource = services;
        list.DataBind();
    }
    protected void doClose_Click(object sender, EventArgs e)
    {
        if (FromTrips)
            Response.Redirect("~/Trips.aspx");
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        DateTime date = Tool.ParsePersianDate(mDate.Text, Null.NullDate);
        if (date == Null.NullDate)
        {
            mError.Text = "تاريخ اشتباه ميباشد. فرمت تاريخ بصورت 1386/12/01 ميباشد.";
            puEx.Show();
            return;
        }
        ServiceTypes sType = (ServiceTypes)Tool.GetInt(mServiceType.SelectedValue, Null.NullInteger);
        bool canDefineFixed = Tool.GetBool(Helper.Instance.GetSettingValue("CanDefineFixed"), false);
        if (!canDefineFixed)
        {
            if (sType == ServiceTypes.Fixed && Null.NullInteger == EditID)
            {
                mError.Text = "اجازه تعریف سرویس ثابت وجود ندارد.";
                puEx.Show();
                return;
            }
        }
        Path path = Helper.Instance.GetPath(Tool.GetInt(mPathID.SelectedValue, Null.NullInteger));
        int carTypeID = Tool.GetInt(mCarTypeID.SelectedValue, Null.NullInteger);
        if (carTypeID == Null.NullInteger)
        {
            mError.Text = "نوع اتوکار الزامی است.";
            puEx.Show();
            return;
        }
        int specialID = Helper.Instance.GetCarType(carTypeID).SpecialID;
        RateItem rateItem = null;
        if (path != null)//&& sType != ServiceTypes.Singular
        {
            #region Handle RateItem
            rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, path.SrcCityID, path.DestCityID, specialID, date);
            if (rateItem == null)
            {
                Rate rate = Helper.Instance.GetRate(date);
                if (null == rate)
                {
                    mError.Text = "برای این نوع اتوکار و مسیر، در قیمت گذاری دوره ای، قیمت تعریف نشده است و نمیتوان بصورت خودکار اضافه کرد.";
                    puEx.Show();
                    return;
                }
                else
                {
                    if (Tool.GetInt(mPrice.Text, 0) == 0 || Tool.GetInt(mInsurance.Text, 0) == 0 || Tool.GetInt(mToll.Text, 0) == 0)
                    {
                        mError.Text = "قیمت در قیمت گذاری تعریف نشده و باید 'قیمت'، 'بیمه سرنشین' و 'عوارض' صفر نباشد تا ذخیره نماید.";
                        puEx.Show();
                        return;
                    }
                    #region Add new RateItem
                    rateItem = new RateItem();
                    rateItem.Cost = Tool.GetInt(mPrice.Text, 0);
                    rateItem.Toll = Tool.GetInt(mToll.Text, 0);
                    rateItem.Insurance = Tool.GetInt(mInsurance.Text, 0);
                    rateItem.ReceptionCost = Tool.GetInt(mReception.Text, 0);
                    rateItem.RateID = rate.ID;
                    rateItem.ExtraCostComment = "";
                    rateItem.Type = "";
                    rateItem.Comments = "قیمت گذاری خودکار از روی تعریف سرویس";
                    rateItem.SrcCityID = path.SrcCityID;
                    rateItem.DestCityID = path.DestCityID;
                    rateItem.SpecialID = specialID;
                    Helper.Instance.DB.RateItems.InsertOnSubmit(rateItem);
                    mError.Text = "بصورت خودکار، قیمت گذاری جدید اضافه شد";
                    if (!Helper.Instance.Update())
                    {
                        mError.Text = "برای این نوع اتوکار و مسیر، در قیمت گذاری دوره ای، قیمت تعریف نشده است.<br />"
                            + Helper.Instance.LastException.Message;
                        puEx.Show();
                        return;
                    }
                    #endregion
                }
            }
            rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, path.SrcCityID, path.DestCityID, specialID, date);
            if (rateItem == null)
            {
                mError.Text = "برای این نوع اتوکار و مسیر، در قیمت گذاری دوره ای، قیمت تعریف نشده است.";
                puEx.Show();
                return;
            }
            #endregion
        }

        Service info;
        if (Null.NullInteger == EditID)
            info = new Service();
        else
            info = Helper.Instance.GetService(EditID);
        if (info == null)
            return;

        info.ServiceType = Tool.GetInt(mServiceType.SelectedValue, Null.NullInteger);
        info.CarTypeID = carTypeID;
        info.SpecialID = specialID;// Tool.GetInt(mSpecialID.SelectedValue, Null.NullInteger);
        info.Special = Helper.Instance.GetSpecial(info.SpecialID);
        info.PathID = Tool.GetInt(mPathID.SelectedValue, Null.NullInteger);
        info.DepartureTime = Tool.FixTime(string.Format("{0}:{1}", mDepartureHour.Text.PadLeft(2, '0'), mDepartureMinute.Text.PadLeft(2, '0')));
        info.Date = date;
        info.Days = GetDays();
        DayOfWeek oo = new System.Globalization.GregorianCalendar().GetDayOfWeek(info.Date);
        int day = 0;
        string dayStr = "";
        #region Find day
        switch (oo)
        {
            case DayOfWeek.Saturday:
                day = 0;
                dayStr = "شنبه";
                break;
            case DayOfWeek.Sunday:
                day = 1;
                dayStr = "یکشنبه";
                break;
            case DayOfWeek.Monday:
                day = 2;
                dayStr = "دوشنبه";
                break;
            case DayOfWeek.Tuesday:
                day = 3;
                dayStr = "سه شنبه";
                break;
            case DayOfWeek.Wednesday:
                day = 4;
                dayStr = "چهارشنبه";
                break;
            case DayOfWeek.Thursday:
                day = 5;
                dayStr = "پنجشنبه";
                break;
            case DayOfWeek.Friday:
                day = 6;
                dayStr = "جمعه";
                break;
            default:
                break;
        }
        #endregion
        if (info.ServiceType == (int)ServiceTypes.Fixed)
            if (string.IsNullOrEmpty(info.Days) || !info.Days.Contains(string.Format(";{0};", day)))
            {
                mError.Text = "تاریخ (" + dayStr + ") در روزهای ثابت انتخاب نشده است";
                puEx.Show();
                return;
            }
        info.Price = Tool.GetInt(mPrice.Text, 0);
        info.Toll = Tool.GetDecimal(mToll.Text, 0);
        info.Insurance = Tool.GetInt(mInsurance.Text, 0);
        info.Reception = Tool.GetInt(mReception.Text, 0);
        info.Enabled = mEnabled.Checked;
        info.HasAgency = mHasAgency.Checked;
        info.DefTickets = mDefTickets.Text;
        info.OnlineRows = mOnlineRows.Text;
        info.SMSMessage = mSMSMessage.Text;
        if (EditID == Null.NullInteger && rateItem != null && sType != ServiceTypes.Singular)
        {
            info.Price = rateItem.Cost;
            info.Toll = rateItem.Toll;
            info.Insurance = rateItem.Insurance;
            info.Reception = rateItem.ReceptionCost;
            mPrice.Text = info.Price.ToString();
            mToll.Text = info.Toll.ToString();
            mInsurance.Text = info.Insurance.ToString();
            mReception.Text = info.Reception.ToString();
        }

        else
        {
            if (info.Toll == 0)
                try
                {
                    info.Toll = (decimal)(info.Price * 5f / Tool.GetInt(mOriginalPrice.Text, info.Price));
                }
                catch { }
            mToll.Text = info.Toll.ToString();
        }
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Services.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            if (Null.NullInteger == EditID)
                mError.Text += "تعریف سرویس با موفقیت اضافه شد";
            else
                mError.Text += "تعریف سرویس با موفقیت بروزرسانی شد";
            list.SelectedIndex = -1;
            BindData();
            string error;
            switch ((ServiceTypes)info.ServiceType)
            {
                case ServiceTypes.Normal:
                    if (Null.NullInteger == EditID)
                    {
                        Helper.Instance.CreateTrip(out error, info.Date, info.ID, Null.NullInteger, Null.NullInteger);
                        mError.Text += " &nbsp;-&nbsp;برای این تعریف سرویس تکی یک سرویس ایجاد شد";
                    }
                    break;
                case ServiceTypes.Singular:
                    if (Null.NullInteger == EditID)
                    {
                        Helper.Instance.CreateTrip(out error, info.Date, info.ID, Null.NullInteger, Null.NullInteger);
                        mError.Text += " &nbsp;-&nbsp;برای این تعریف سرویس فوق العاده یک سرویس ایجاد شد";
                    }
                    break;
                case ServiceTypes.Fixed:
                    break;
                default:
                    break;
            }
            EditID = info.ID;
            mServiceType.Enabled = false;
            mDate.Enabled = info.ServiceType != (int)ServiceTypes.Normal;
            mTollPrice.Text = (info.Toll * info.Price / 100).ToString("N0");
            puEx.Show();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Service info = e.Row.DataItem as Service;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                try
                {
                    e.Row.Cells[3].Text = Helper.Instance.GetCarType(info.CarTypeID.Value).Title;
                }
                catch
                {
                    try
                    {
                        e.Row.Cells[3].Text = info.Special.Title;
                    }
                    catch { }
                }
                e.Row.Cells[4].Text = info.Path == null ? "" : info.Path.Title;
                try
                {
                    LinkButton ctl = e.Row.Cells[e.Row.Cells.Count - 3].Controls[0] as LinkButton;
                    ctl.CommandArgument = info.ID.ToString();
                    ctl.Text = info.Enabled ? "غیرفعال شود" : "فعال شود";
                }
                catch { }
                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Service info = Helper.Instance.GetService(EditID);
        if (info == null)
            return;

        Tool.SetSelected(mServiceType, info.ServiceType);
        Tool.SetSelected(mCarTypeID, info.CarTypeID);
        Tool.SetSelected(mPathID, info.PathID);
        string hm = Tool.FixTime(info.DepartureTime);
        mDepartureHour.Text = mDepartureMinute.Text = "";
        if (!string.IsNullOrEmpty(hm))
            if (hm.Contains(':'))
            {
                mDepartureHour.Text = hm.Split(':')[0];
                mDepartureMinute.Text = hm.Split(':')[1];
            }
        mDate.Text = Tool.ToPersianDate(info.Date, DateTime.Now);
        SetDays(info);
        mPrice.Text = info.Price.ToString();
        mToll.Text = info.Toll.ToString();
        mInsurance.Text = info.Insurance.ToString();
        mReception.Text = info.Reception.ToString();
        mEnabled.Enabled = info.Enabled;
        mHasAgency.Checked = Tool.GetBool(info.HasAgency, false);
        mDefTickets.Text = info.DefTickets;
        mOnlineRows.Text = info.OnlineRows;
        mServiceType.Enabled = false;
        mDate.Enabled = info.ServiceType != (int)ServiceTypes.Normal;
        mPriceReq.Enabled = mOriginalPriceReq.Enabled = mInsuranceReq.Enabled
            = mOriginalPrice.Enabled = info.ServiceType == (int)ServiceTypes.Singular;
        mSMSMessage.Text = info.SMSMessage;
        if (mOriginalPrice.Enabled)
        {
            try
            {
                mOriginalPrice.Text = (info.Price / 5 * info.Toll).ToString("N0");
            }
            catch { }
        }
        else
            mOriginalPrice.Text = "";
        mTollPrice.Text = (info.Toll * info.Price / 100).ToString("N0");

        puEx.Show();
    }
    private string GetDays()
    {
        StringBuilder builder = new StringBuilder();
        if (mDay0.Checked)
            builder.Append(";0;");
        if (mDay1.Checked)
            builder.Append(";1;");
        if (mDay2.Checked)
            builder.Append(";2;");
        if (mDay3.Checked)
            builder.Append(";3;");
        if (mDay4.Checked)
            builder.Append(";4;");
        if (mDay5.Checked)
            builder.Append(";5;");
        if (mDay6.Checked)
            builder.Append(";6;");
        return builder.ToString().Replace(";;", ";");
    }
    private void SetDays(Service info)
    {
        mDay0.Checked = info != null && info.Days != null && info.Days.Contains(";0;");
        mDay1.Checked = info != null && info.Days != null && info.Days.Contains(";1;");
        mDay2.Checked = info != null && info.Days != null && info.Days.Contains(";2;");
        mDay3.Checked = info != null && info.Days != null && info.Days.Contains(";3;");
        mDay4.Checked = info != null && info.Days != null && info.Days.Contains(";4;");
        mDay5.Checked = info != null && info.Days != null && info.Days.Contains(";5;");
        mDay6.Checked = info != null && info.Days != null && info.Days.Contains(";6;");
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        Service s = Helper.Instance.GetService(id);
        if (s.Trips != null && s.Trips.Count > 0)
        {
            if ((s.ServiceType == (int)ServiceTypes.Normal) && (s.Trips.Count == 1) && (s.Trips[0].Tickets.Count == 0))
            {
                Helper.Instance.DeleteTrip(s.Trips[0]);
            }
            else
            {
                mMsg.Text = "این تعریف سرویس در سرویسها استفاده شده است و نمیتوان آنرا حذف کرد";
                return;
            }
        }
        if (Helper.Instance.DeleteService(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
        //else
        //{
        //    mMsg.Text = Helper.Instance.LastException.Message;
        //}
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected bool FromTrips
    {
        get
        {
            return Tool.GetBool(ViewState["FromTrips"], false);
        }
        set
        {
            ViewState["FromTrips"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mEnabled.Checked = true;
        mHasAgency.Checked = false;
        mServiceType.Enabled = true;
        mDate.Enabled = true;
        //mDepartureHour.Text = "12";
        //mDepartureMinute.Text = "00";
        mDepartureHour.Text = DateTime.Now.Hour.ToString().PadLeft(2, '0');
        mDepartureMinute.Text = DateTime.Now.Minute.ToString().PadLeft(2, '0');
        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        mPrice.Text = mToll.Text = mInsurance.Text = mReception.Text = "0";
        mOriginalPrice.Text = "";
        SetDays(null);
        DayOfWeek oo = new System.Globalization.GregorianCalendar().GetDayOfWeek(DateTime.Now);
        #region Find day
        switch (oo)
        {
            case DayOfWeek.Saturday:
                mDay0.Checked = true;
                break;
            case DayOfWeek.Sunday:
                mDay1.Checked = true;
                break;
            case DayOfWeek.Monday:
                mDay2.Checked = true;
                break;
            case DayOfWeek.Tuesday:
                mDay3.Checked = true;
                break;
            case DayOfWeek.Wednesday:
                mDay4.Checked = true;
                break;
            case DayOfWeek.Thursday:
                mDay5.Checked = true;
                break;
            case DayOfWeek.Friday:
                mDay6.Checked = true;
                break;
            default:
                break;
        }
        #endregion
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        list.PageIndex = 0;
        BindData();
    }
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected void mServiceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ServiceTypes sType = (ServiceTypes)Tool.GetInt(mServiceType.SelectedValue, Null.NullInteger);
        mOriginalPrice.Enabled = sType == ServiceTypes.Singular;
        mPriceReq.Enabled = mOriginalPriceReq.Enabled = mInsuranceReq.Enabled
            = mOriginalPrice.Enabled = sType == ServiceTypes.Singular;
        puEx.Show();
    }


    protected void list_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == null)
            return;
        int id = Tool.GetInt(e.CommandArgument, Null.NullInteger);
        if (id == Null.NullInteger)
            return;
        if (e.CommandName.ToLower().Equals("enabledisable"))
        {
            Service info = Helper.Instance.GetService(id);
            if (info == null)
                return;
            info.Enabled = !info.Enabled;
            if (Helper.Instance.Update())
            {
                mMsg.Text = "تعریف سرویس با موفقیت بروزرسانی شد";
                list.SelectedIndex = -1;
                BindData();
            }
            else
            {
                mMsg.Text = "خطا در بروزرسانی<br>" + Helper.Instance.LastException.Message;
            }
        }
    }
}
