﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Services.aspx.cs" Inherits="Services_Services" Title="تعريف سرويس" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">

    <script language="javascript" type="text/javascript">
        function autoTab(src, dest, len) {
            if (event.keyCode == 9 || event.keyCode == 16)
                return;
            //alert(event.keyCode);
            if ($get(src).value.length == len) {
                $get(dest).focus();
                $get(dest).select();
            }
        }

    </script>

    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter">
                <tr>
                    <td>جستجو
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sKeinPath" Text='فقط سرویسهای بدون مسیر' Checked="false" />
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sShowNormals" Text='نمایش سرویسهای تکی' Checked="false" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabled" Text='نمایش فعالها' Checked="true" />
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="sEnabledNo" Text='نمایش غیرفعالها' />
                    </td>
                    <td>تاریخ از
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doAdd" runat="server" CssClass="CB" OnClick="doAdd_Click" Text="جديد INS"
                            Width="50px" />
                    </td>
                </tr>
                <tr>
                    <td>نوع سرويس
                    </td>
                    <td>
                        <asp:DropDownList ID="sServiceType" runat="server" Width="150px" />
                    </td>
                    <td>نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" Width="150px" />
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>مسير
                    </td>
                    <td>
                        <asp:DropDownList ID="sPathID" runat="server" Width="150px" />
                    </td>
                    <td>تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" OnSelectedIndexChanged="list_SelectedIndexChanged"
                Style="margin-top: 0px" AllowPaging="True" OnPageIndexChanging="list_PageIndexChanging"
                PageSize="20" OnRowCommand="list_RowCommand">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ServiceType2" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="CarTypeID" HeaderText="نوع اتوکار " />
                    <asp:BoundField DataField="PathID" HeaderText="مسير" />
                    <asp:BoundField DataField="DepartureTime" HeaderText="زمان حرکت" />
                    <asp:BoundField DataField="Days2" HeaderText="روزهاي ثابت" />
                    <asp:CheckBoxField DataField="Today" HeaderText="امروز" />
                    <asp:BoundField DataField="Price" HeaderText="قيمت" />
                    <asp:HyperLinkField HeaderText="شهرهای بین راه" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="ServiceDests.aspx?serviceid={0}"
                        Text="شهرهای بین راه" />
                    <asp:CheckBoxField DataField="HasAgency" HeaderText="دارای آژانس" />
                    <asp:CheckBoxField DataField="Enabled" HeaderText="فعال" />
                    <asp:ButtonField CommandName="EnableDisable" Text="غیرفعال شود" />
                    <asp:CommandField SelectText="ويرايش" ShowSelectButton="True" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span>
                </EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="Label1" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="550px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    اضافه نمودن / ويرايش تعريف سرويس
                </asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">نوع سرويس
                            </td>
                            <td>
                                <asp:DropDownList ID="mServiceType" runat="server" AutoPostBack="true" OnSelectedIndexChanged='mServiceType_SelectedIndexChanged' />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N">نوع اتوکار
                            </td>
                            <td>
                                <asp:DropDownList ID="mCarTypeID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="نوع اتوکار الزامي است" ControlToValidate="mCarTypeID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">مسير
                            </td>
                            <td>
                                <asp:DropDownList ID="mPathID" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="addGroup"
                                    ErrorMessage="مسير الزامي است" ControlToValidate="mPathID" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">زمان حرکت
                            </td>
                            <td>ساعت:
                                <asp:TextBox ID="mDepartureHour" runat="server" Width="20" MaxLength="2" />
                                <cc1:FilteredTextBoxExtender FilterMode="ValidChars" ValidChars="0123456789" runat="server"
                                    TargetControlID="mDepartureHour" ID="x2" />
                                دقیقه:
                                <asp:TextBox ID="mDepartureMinute" runat="server" Width="20" MaxLength="2" />
                                <cc1:FilteredTextBoxExtender ID="x1" FilterMode="ValidChars" ValidChars="0123456789"
                                    runat="server" TargetControlID="mDepartureMinute" />
                                <asp:RangeValidator ID="RangeValidator6" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mDepartureHour" ErrorMessage="ساعت بايد يک عدد بين 0 و 23 باشد"
                                    MaximumValue="23" MinimumValue="0" />
                                <asp:RangeValidator ID="RangeValidator7" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mDepartureMinute" ErrorMessage="ساعت بايد يک عدد بين 0 و 59 باشد"
                                    MaximumValue="59" MinimumValue="0" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N">تاريخ
                            </td>
                            <td>
                                <asp:TextBox ID="mDate" runat="server" />
                                <cc1:MaskedEditExtender ID="mee1" TargetControlID="mDate" runat="server" Mask="1399/99/99"
                                    ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                                <span dir="ltr">Mask=13--/--/--</span>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N">روزهاي ثابت
                            </td>
                            <td>
                                <asp:CheckBox runat="server" Text="ش" ID='mDay0' />
                                <asp:CheckBox runat="server" Text="1ش" ID='mDay1' />
                                <asp:CheckBox runat="server" Text="2ش" ID='mDay2' />
                                <asp:CheckBox runat="server" Text="3ش" ID='mDay3' />
                                <asp:CheckBox runat="server" Text="4ش" ID='mDay4' />
                                <asp:CheckBox runat="server" Text="5ش" ID='mDay5' />
                                <asp:CheckBox runat="server" Text="ج" ID='mDay6' />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N">قيمت
                            </td>
                            <td>
                                <asp:TextBox ID="mPrice" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator runat="server" ID="mPriceReq" ControlToValidate="mPrice"
                                    ErrorMessage='الزامی' Enabled="false" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mPrice" />
                                <asp:RangeValidator ID="RangeValidator1" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mPrice" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">قيمت اصلی
                            </td>
                            <td>
                                <asp:TextBox ID="mOriginalPrice" runat="server" Enabled="false" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator runat="server" ID="mOriginalPriceReq" ControlToValidate="mOriginalPrice"
                                    ErrorMessage='الزامی' Enabled="false" />
                                <asp:RangeValidator ID="RangeValidator5" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mOriginalPrice" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">درصد عوارض
                            </td>
                            <td>
                                <asp:TextBox ID="mToll" runat="server" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars"
                                    ValidChars='.0123456789' TargetControlID="mToll" />
                                <asp:RangeValidator ID="RangeValidator2" Type="Double" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mToll" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="mTollPrice" />

                            </td>
                        </tr>
                        <tr>
                            <td class="N"></td>
                            <td colspan="2">برای سرویس فوق العاده: اگر خالی باشد از روی مقادیر بالا با نسبت 5% محاسبه میشود
                            </td>
                        </tr>
                        <tr>
                            <td class="N">بيمه سرنشين
                            </td>
                            <td>
                                <asp:TextBox ID="mInsurance" runat="server" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator runat="server" ID="mInsuranceReq" ControlToValidate="mInsurance"
                                    ErrorMessage='الزامی' Enabled="false" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mInsurance" />
                                <asp:RangeValidator ID="RangeValidator3" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mInsurance" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N">پذيرايي
                            </td>
                            <td>
                                <asp:TextBox ID="mReception" runat="server" />
                            </td>
                            <td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers" TargetControlID="mReception" />
                                <asp:RangeValidator ID="RangeValidator4" Type="Integer" runat="server" Display="Dynamic"
                                    ValidationGroup="addGroup" ControlToValidate="mReception" ErrorMessage="اين مقدار بايد يک عدد بين 0 و 2.000.000.000 باشد"
                                    MaximumValue="2000000000" MinimumValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N" > ردیف های فعال خرید اینترنتی
                            </td>
                            <td>
                                <asp:TextBox ID="mOnlineRows" runat="server" Height="60px" Width="400px" TextMode="MultiLine" />
                            </td>
                            <td>ردیف ها با ; از هم جدا شوند
                            </td>
                        </tr>
                        <tr>
                            <td class="N">متن پیام سرویس</td>
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="mSMSMessage" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="N">داری آژانس
                            </td>
                            <td>
                                <asp:CheckBox ID="mHasAgency" runat="server" Checked="true" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N">فعال
                            </td>
                            <td>
                                <asp:CheckBox ID="mEnabled" runat="server" Checked="true" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="N" colspan="3">مسافرین پیشفرض - با کاما (،) یا دونقطه (:) از هم جدا شوند. علامت منها (-) به معنای
                                خانم میباشد
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="mDefTickets" runat="server" TextMode="MultiLine" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td class="N"></td>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="تاييد F7" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="doClose" ValidationGroup="addGroup" OnClick="doClose_Click" runat="server"
                                    Text="بستن" CssClass="CB"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
