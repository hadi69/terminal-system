﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;
using System.Text;

public partial class Closeds : System.Web.UI.Page
{
    string msg = "";
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        msg = DateTime.Now.ToString();
        DateTime dt1 = DateTime.Now;
        //list.EnableViewState = false;
        if (!IsPostBack)
        {
            //Helper.Instance.AutoDeleteTrips();
            BindInitData();
            //if (!Tool.ButtonIsClicked(Request, doSearch))
                BindData();
        }
        //ScriptManager manager = ScriptManager.GetCurrent(this);
        //if (!manager.IsInAsyncPostBack)
        //    BindData();
        StringBuilder sb = new StringBuilder();
        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function $d(id){$get('" + mIDToDelete.ClientID  + "').value=id;$find('puExBehaviorID').show();return false;}");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        Page.RegisterClientScriptBlock("Ask_Del", sb.ToString());
        msg += "<br> PageLoad: " + (DateTime.Now - dt1).ToString();
    }
    private void BindInitData()
    {
        {
            List<string> all = (from h in Helper.Instance.DB.Trips
                                select h.Series).ToList();
            all.Insert(0, "");

            sSeries.DataSource = all.Distinct();
            sSeries.DataBind();
        }

        {
            sServiceID.Enabled = false;
            //List<Service> all = Helper.Instance.GetServices();
            //Service none = new Service();
            //none.ID = Null.NullInteger;
            //all.Insert(0, none);

            //sServiceID.DataValueField = "ID";
            //sServiceID.DataTextField = "Title";
            //sServiceID.DataSource = all;
            //sServiceID.DataBind();
        }
        {
            List<Car> all = Helper.Instance.GetCars();
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarID.DataValueField = "ID";
            sCarID.DataTextField = "Title";
            sCarID.DataSource = all;
            sCarID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            CarType none = new CarType();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sCarTypeID.DataValueField = "ID";
            sCarTypeID.DataTextField = "Title";
            sCarTypeID.DataSource = all;
            sCarTypeID.DataBind();
        }
        {
            List<Driver> all = Helper.Instance.GetDrivers();
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDriverID1.DataValueField = "ID";
            sDriverID1.DataTextField = "Title";
            sDriverID1.DataSource = all;
            sDriverID1.DataBind();
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            City none = new City();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            sDestID.DataValueField = "ID";
            sDestID.DataTextField = "Title";
            sDestID.DataSource = all;
            sDestID.DataBind();
        }

        sDateStart.SelectedDate = DateTime.Now;
        sDateEnd.SelectedDate = DateTime.Now;

        sDateStart.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Closeds.sDateStart"]), DateTime.Now);
        sDateEnd.SelectedDate = Tool.ParsePersianDate(Tool.GetString(Session["Closeds.sDateEnd"]), DateTime.Now);

    }
    //private List<Trip> GetTrips()
    //{
    //    var all = from c in Helper.Instance.DB.Trips
    //              where c.Closed == true
    //              orderby c.Date, c.Service.DepartureTime, c.Service.ServiceType
    //              select c;

    //    List<Trip> trips = all.ToList();
    //    if (trips == null || trips.Count == 0)
    //        return trips;

    //    if (sServiceID.SelectedIndex > 0)
    //    {
    //        int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].ServiceID != sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (sCarTypeID.SelectedIndex > 0)
    //    {
    //        int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].CarTypeID != sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (sCarID.SelectedIndex > 0)
    //    {
    //        int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].CarID != sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (sDestID.SelectedIndex > 0)
    //    {
    //        int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].Service.Path.DestCityID != sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (sDriverID1.SelectedIndex > 0)
    //    {
    //        int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].DriverID1 != sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }

    //    if (sNo.Text.Trim().Length > 0)
    //    {
    //        int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].No < sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (sNo2.Text.Trim().Length > 0)
    //    {
    //        int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].No > sID)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }

    //    if (sSeries.Text.Trim().Length > 0)
    //    {
    //        string s = Tool.ParseSeries(sSeries.Text.Trim());
    //        for (int i = 0; i < trips.Count; i++)
    //            if (!trips[i].Series.Contains(s))
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (!mBus.Checked)
    //    {
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].CarType == null || trips[i].CarType.Layout.NumChairs > 25)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (!mMini.Checked)
    //    {
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].CarType == null || (trips[i].CarType.Layout.NumChairs > 6 && trips[i].CarType.Layout.NumChairs < 25))
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    if (!mSewari.Checked)
    //    {
    //        for (int i = 0; i < trips.Count; i++)
    //            if (trips[i].CarType == null || trips[i].CarType.Layout.NumChairs < 6)
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //    }
    //    DateTime startDate = sDateStart.SelectedDate;
    //    DateTime toDate = sDateEnd.SelectedDate;
    //    Session["Closeds.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
    //    Session["Closeds.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

    //    if (startDate != Null.NullDate || toDate != Null.NullDate)
    //    {
    //        startDate = startDate != Null.NullDate ? new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0)
    //            : DateTime.Now.AddYears(-10);
    //        toDate = toDate != Null.NullDate ? new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999)
    //            : DateTime.Now.AddYears(10);
    //        for (int i = 0; i < trips.Count; i++)
    //        {
    //            if ((trips[i].Date < startDate || trips[i].Date > toDate))
    //            {
    //                trips.RemoveAt(i);
    //                i--;
    //            }
    //        }
    //    }

    //    return trips;


    //}
    private void BindData()
    {
        DateTime dt1 = DateTime.Now;
        DateTime startDate = sDateStart.SelectedDate;
        DateTime toDate = sDateEnd.SelectedDate;
        if (startDate == Null.NullDate)
            startDate = DateTime.Now.AddYears(-2);
        if (toDate == Null.NullDate)
            toDate = DateTime.Now.AddYears(2);
        string qry = string.Format(@"SELECT Trips.* 
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2 
	, Services.ServiceType
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
	, (SELECT SUM(NumChairs) FROM Tickets
                    WHERE TripID = Trips.ID AND PaidBack = 0
                    AND (SaleType = 0 OR SaleType = 2)) AS NumTickets
	, Layouts.NumChairs
FROM Trips
INNER JOIN Services ON Services.ID=Trips.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
LEFT JOIN Cars ON Trips.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
WHERE Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59' AND Trips.Closed=1
"
            , startDate.Year, startDate.Month, startDate.Day
            , toDate.Year, toDate.Month, toDate.Day);
//         qry = string.Format(@"SELECT Trips.*
//    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2
//	, Services.ServiceType
//	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
//	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
//	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
//	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
//	, 0 AS NumTickets
//	, Layouts.NumChairs
//FROM Trips
//INNER JOIN Services ON Services.ID=Trips.ServiceID
//INNER JOIN Paths ON Services.PathID=Paths.ID
//LEFT JOIN Drivers ON Trips.DriverID1=Drivers.ID
//LEFT JOIN Cars ON Trips.CarID=Cars.ID
//INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
//INNER JOIN Layouts ON CarTypes.LayoutID=Layouts.ID
//WHERE Trips.Date>= '{0}/{1}/{2}' AND Trips.Date<='{3}/{4}/{5} 23:59:59'
//"
//           , startDate.Year, startDate.Month, startDate.Day
//           , toDate.Year, toDate.Month, toDate.Day);
        if (sServiceID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sServiceID.SelectedValue, Null.NullInteger);
            qry += " AND Trips.ServiceID=" + sID;
        }
        if (sCarTypeID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarTypeID.SelectedValue, Null.NullInteger);
            qry += " AND Trips.CarTypeID=" + sID;
        }
        if (sCarID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sCarID.SelectedValue, Null.NullInteger);
            qry += " AND Trips.CarID=" + sID;
        }
        if (sDestID.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
            qry += " AND Paths.DestCityID=" + sID;
        }
        if (sDriverID1.SelectedIndex > 0)
        {
            int sID = Tool.GetInt(sDriverID1.SelectedValue, Null.NullInteger);
            qry += " AND Trips.DriverID1=" + sID;
        }

        if (sNo.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo.Text, Null.NullInteger);
            qry += " AND Trips.No>=" + sID;
        }
        if (sNo2.Text.Trim().Length > 0)
        {
            int sID = Tool.GetInt(sNo2.Text, Null.NullInteger);
            qry += " AND Trips.No<=" + sID;
        }

        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY Trips.Date, Services.DepartureTime, Services.ServiceType");
        if (dt != null && dt.Rows.Count > 0)
        {
            if (sSeries.Text.Trim().Length > 0)
            {
                string s = Tool.ParseSeries(sSeries.Text.Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string series = Tool.GetString(dt.Rows[i]["Series"], "");
                    if (!series.Contains(s))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mBus.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"], 0);
                    if (numChairs > 25)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mMini.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"] , 0);
                    if (numChairs>= 6 && numChairs< 25)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (!mSewari.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int numChairs = Tool.GetInt(dt.Rows[i]["NumChairs"] , 0);
                    if (numChairs < 6)
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
            if (mOffilne.Checked != mHasCode.Checked)
            {
                if (mHasCode.Checked)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string trackCode = Tool.GetString(dt.Rows[i]["TrackCode"], "");
                        if (string.IsNullOrEmpty(trackCode))
                        {
                            dt.Rows.RemoveAt(i);
                            i--;
                        }
                    }
                }
                if (mOffilne.Checked)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string trackCode = Tool.GetString(dt.Rows[i]["TrackCode"], "");
                        if (!string.IsNullOrEmpty(trackCode))
                        {
                            dt.Rows.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
        }


        Session["Closeds.sDateStart"] = Tool.ToPersianDate(sDateStart.SelectedDate, "");
        Session["Closeds.sDateEnd"] = Tool.ToPersianDate(sDateEnd.SelectedDate, "");

        list.DataSource = dt;
        msg += "<br> GetTrips:" + (DateTime.Now - dt1).ToString();
        dt1 = DateTime.Now;
        list.DataBind();
        msg += "<br> Bind: " + (DateTime.Now - dt1).ToString();
       // mMsg.Text = msg;
    }


    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            DataRowView row = e.Row.DataItem as DataRowView;
            if (row != null)
            {
                e.Row.Cells[0].Text = row["ID"].ToString();
                e.Row.Cells[2].Text = Tool.ToPersianDate(row["Date"], "");
                e.Row.Cells[4].Text = Tool.ToString((ServiceTypes)Tool.GetInt(row["ServiceType"]));
                e.Row.Cells[5].Text = Tool.GetString(row["SrcCity"], "") + " -> " + Tool.GetString(row["DestCity"], "");
                //e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(info.CarID);
                //e.Row.Cells[7].Text = info.Driver != null ? info.Driver.Title : "";
                string series = Tool.GetString(row["Series"], "");
                if (series != null && series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                    string[] ss = series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                        b.AppendFormat("<td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td>", ss[0], ss[1], ss[2]);
                    b.Append("</tr></table>");
                    e.Row.Cells[9].Text = b.ToString();
                }
                //JsTools.HandleDeleteButton(e);
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = cc.Controls[0] as LinkButton;
                if (delete != null)
                    delete.Attributes.Add("OnClick", string.Format("$d({0});", row["ID"]));
            }
            else
            {
                Trip info = e.Row.DataItem as Trip;
                if (info != null)
                {
                    e.Row.Cells[0].Text = info.ID.ToString();
                    e.Row.Cells[2].Text = Tool.ToPersianDate(info.Date, "");
                    //e.Row.Cells[3].Text = info.Service.DepartureTime;
                    e.Row.Cells[4].Text = info.Service.ServiceType2;
                    e.Row.Cells[5].Text = info.Service.Path.Title;
                    e.Row.Cells[6].Text = Helper.Instance.GetCarTitle(info.CarID);
                    e.Row.Cells[7].Text = info.Driver != null ? info.Driver.Title : "";
                    if (info.Series != null && info.Series.Length > 0)
                    {
                        StringBuilder b = new StringBuilder();
                        b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl><tr>");
                        string[] ss = info.Series.Split(',');
                        if (ss.Length == 4)
                            b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                        if (ss.Length >= 3)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                b.AppendFormat("<td>{0}</td>", ss[i]);
                                if (i < 2)
                                    b.Append("<td>/</td>");
                            }
                        }
                        b.Append("</tr></table>");
                        e.Row.Cells[9].Text = b.ToString();
                    }
                    e.Row.Cells[10].Text = Helper.Instance.GetNumTickets(info.ID).ToString();

                    //JsTools.HandleDeleteButton(e);
                    TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                    LinkButton delete = cc.Controls[0] as LinkButton;
                    if (delete != null)
                        delete.Attributes.Add("OnClick", string.Format("$d({0});", info.ID));
                }
            }
        }
    }
    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        ToDeleteID = id;
        if (Null.NullInteger == id)
            return;
        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void puOk_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == ToDeleteID)
            return;

        Trip mTrip = Helper.Instance.GetTrip(ToDeleteID);
        if (mTrip == null)
            return;
        InValid v = new InValid();
        v.Date = mTrip.Date;
        v.Ticketless = mTrip.Ticketless;
        v.TicketlessPrice = mTrip.TicketlessPrice;
        v.TicketlessName = mTrip.TicketlessName;
        v.Stamp = mTrip.Stamp;
        v.Comission = mTrip.Comission;
        v.BodyInsurance = mTrip.BodyInsurance;
        v.ExtraCost = mTrip.ExtraCost;
        v.Closed = mTrip.Closed;
        v.Price = mTrip.Price;
        v.Toll = mTrip.Toll;
        v.Insurance = mTrip.Insurance;
        v.Reception = mTrip.Reception;
        v.TotalPrice = mTrip.TotalPrice;
        v.Comission2 = mTrip.Comission2;
        v.OtherDeficits = mTrip.OtherDeficits;
        v.SumDeficits = mTrip.SumDeficits;
        v.AutoShare = mTrip.AutoShare;
        v.Toll2 = mTrip.Toll2;
        v.Locked = mTrip.Locked;
        v.Others = mTrip.Others;
        v.TripID = mTrip.ID;
        v.No = mTrip.No;
        v.Series = mTrip.Series;
        v.CarID = mTrip.CarID;
        v.DriverID1 = mTrip.DriverID1;
        v.DriverID2 = mTrip.DriverID2;
        v.DriverID3 = mTrip.DriverID3;
        v.ServiceID = mTrip.ServiceID;

        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        
        //Add user log
        UserLog userLog=new UserLog();
        userLog.UserID = SiteSettings.UserID;
        userLog.ChangedTableName = "Trips";
        userLog.ChangedRecordID = mTrip.ID;
        userLog.Date = DateTime.Now;
        userLog.Comments = "حذف صورت وضعیت: مبدأ= " + mTrip.Service.Path.City.Title + ";مقصد= " +
                           mTrip.Service.Path.City1.Title + ";ساعت حرکت= " + mTrip.DepartureTime+";شماره صورت="+mTrip.No;
        Helper.Instance.DB.UserLogs.InsertOnSubmit(userLog);
        
        Helper.Instance.Update();

        mMsg.Text = "شماره صورت وضعیت باطل شد - ";


        if (Helper.Instance.DeleteTrip(ToDeleteID))
        {
            list.SelectedIndex = -1;
            BindData();
            mMsg.Text += "سرویس با موفقیت حذف شد";
        }
        else
        {
            mMsg.Text += "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
        ToDeleteID = Null.NullInteger;
    }
    protected void puNo_Click(object sender, EventArgs e)
    {
        if (Null.NullInteger == ToDeleteID)
            return;
        if (Helper.Instance.GetSettingValue("CanDeletePSurat") != "False")
        {
            Trip mTrip = Helper.Instance.GetTrip(ToDeleteID);
            //Add user log
            UserLog userLog = new UserLog();
            userLog.UserID = SiteSettings.UserID;
            userLog.ChangedTableName = "Trips";
            userLog.ChangedRecordID = mTrip.ID;
            userLog.Date = DateTime.Now;
            userLog.Comments = "حذف صورت وضعیت: مبدأ= " + mTrip.Service.Path.City.Title + ";مقصد= " +
                               mTrip.Service.Path.City1.Title + ";ساعت حرکت= " + mTrip.DepartureTime + ";شماره صورت=" + mTrip.No;
            Helper.Instance.DB.UserLogs.InsertOnSubmit(userLog);
            
            if (Helper.Instance.DeleteTrip(ToDeleteID))
            {
                list.SelectedIndex = -1;
                BindData();
                mMsg.Text = "سرویس با موفقیت حذف شد";
            }
            else
            {
                mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
            }
        }
        else
            mMsg.Text = "شما اجازه حذف صورت چاپ شده را ندارید";
        ToDeleteID = Null.NullInteger;
    }
    int ToDeleteID
    {
        get
        {
            return Tool.GetInt(mIDToDelete.Value, Null.NullInteger);
            return Tool.GetInt(ViewState["ToDeleteID"], Null.NullInteger);
        }
        set
        {
            mIDToDelete.Value = value.ToString();
            ViewState["ToDeleteID"] = value;
        }
    }
}
