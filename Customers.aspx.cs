﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Customers : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.BindDefaultHotKeysF(this, doAdd, puOk, puCancel);
        mError.Text = "";
        if (!IsPostBack)
            BindData();
    }

    private void BindData()
    {
        var all = from c in Helper.Instance.DB.Customers
                  select c;

        if (sName.Text.Trim().Length > 0)
            all = all.Where(c => c.Name.Contains(sName.Text.Trim()));
        if (sSurname.Text.Trim().Length > 0)
            all = all.Where(c => c.Surname.Contains(sSurname.Text.Trim()));
        if (sNo.Text.Trim().Length > 0)
            all = all.Where(c => c.No.Contains(sNo.Text.Trim()));
        if (sTel.Text.Trim().Length > 0)
            all = all.Where(c => c.Tel.Contains(sTel.Text.Trim()));

        list.DataSource = all;
        list.DataBind();
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        Customer info;
        if (Null.NullInteger == EditID)
            info = new Customer();
        else
            info = Helper.Instance.GetCustomer(EditID);
        if (info == null)
            return;

        Customer old = Helper.Instance.GetCustomer(mNo.Text);
        if (old != null && old.ID != info.ID)
        {
            mError.Text = "کد اشتراک تکراری است";
            puEx.Show();
            return;
        }

        info.Man = mManYes.Checked;
        info.No = mNo.Text;
        info.Name = mName.Text;
        info.Surname = mSurname.Text;
        info.Tel = mTel.Text;
        info.Fax = mFax.Text;
        info.CellNo = mCellNo.Text;
        info.Address = mAddress.Text;
        info.Email = mEmail.Text;
        info.Comments = mComments.Text;

        
        if (Null.NullInteger == EditID)
            Helper.Instance.DB.Customers.InsertOnSubmit(info);
        if (Helper.Instance.Update())
        {
            EditID = Null.NullInteger;
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            puEx.Show();
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Customer info = e.Row.DataItem as Customer;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();

                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        EditID = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == EditID)
            return;
        Customer info = Helper.Instance.GetCustomer(EditID);
        if (info == null)
            return;

        mManYes.Checked = info.Man;
        mManNo.Checked = !info.Man;
        mNo.Text = info.No;
        mName.Text = info.Name;
        mSurname.Text = info.Surname;
        mTel.Text = info.Tel;
        mFax.Text = info.Fax;
        mCellNo.Text = info.CellNo;
        mAddress.Text = info.Address;
        mEmail.Text = info.Email;
        mComments.Text = info.Comments;

        puEx.Show();
    }

    protected void list_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Tool.GetInt(list.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (Helper.Instance.DeleteCustomer(id))
        {
            list.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mMsg.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected int EditID
    {
        get
        {
            return Tool.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        EditID = Null.NullInteger;
        mManYes.Checked = true;
        mNo.Text = mName.Text = mSurname.Text = mTel.Text =
        mFax.Text = mCellNo.Text = mAddress.Text = mEmail.Text = mComments.Text = "";

        puEx.Show();
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}
