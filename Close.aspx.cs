﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using ir.rmto.soratonline;
using Button = System.Web.UI.WebControls.Button;
using TextBox = System.Web.UI.WebControls.TextBox;

public partial class Close : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    Trip mTrip = null;
    bool error = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            mError.Text = "";
            mWriteMsg.Text = "";
            if (!IsPostBack)
            {
                doSaveInSetting.Enabled = Tool.GetBool(Helper.Instance.GetSettingValue("AllowSaveSeries"), true);
                this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
                mTrip = Helper.Instance.GetTrip(this.TripID);
                if (mTrip == null)
                    Response.Redirect("Trips.aspx", true);
                mTripTitle.Text = string.Format("{0} ({1}) ({2} - {3}) شماره: {4}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDateRtl(mTrip.Date, ""), mTrip.DepartureTime2, mTrip.ID);
                //            mTripTitle.Text = string.Format("{0} ({1}) ({2}) شماره {3}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDate(mTrip.Date, ""), mTrip.ID);
                doTechnical.NavigateUrl = "Technical.aspx?tripid=" + mTrip.ID;

            }
            else
                mTrip = Helper.Instance.GetTrip(TripID);
            //mPrice.Enabled = mToll.Enabled = mInsurance.Enabled = mReception.Enabled
            //        = mDifference1.Enabled = mDifference2.Enabled
            //        = mComission.Enabled = mBodyInsurance.Enabled
            mToll.Enabled = mComission.Enabled = mBodyInsurance.Enabled = mInsurance.Enabled
                = mOthers.Enabled = mStamp.Enabled = mExtraCost.Enabled = mReception.Enabled
                = doChangeComission.Enabled = mTotalPrice.Enabled = mToll2.Enabled = mComission2.Enabled
                = mOtherDeficits.Enabled = mSumDeficits.Enabled = mAutoShare.Enabled
                = Tool.GetBool(Helper.Instance.GetSettingValue("ChangeCloseValues"), true);
            if (!IsPostBack)
            {
                BindInitData();
                BindData();
            }
            else
                CreateTripDestControls();

            if (!ButtonIsClicked(doJustSave))
                RegisterScripts();

            //JsTools.SetFocus(mCarIDlbl);
        }
        catch (Exception ex)
        {
            mError.Text += ex.Message + "<br>" + ex.StackTrace;
        }

        List<string> clientIDs = new List<string>();
        List<int> keyCodes = new List<int>();
        clientIDs.Add(doClose.ClientID);
        keyCodes.Add(118);
        clientIDs.Add(doPrint.ClientID);
        keyCodes.Add(120);
        //if (IsPostBack)
        JsTools.BindHotKeysF(Page, clientIDs, keyCodes);

        //else
        //    JsTools.BindHotKeysFocus(Page, clientIDs, keyCodes);
    }
    void RegisterScripts()
    {
        doChangeSeries_NoCode.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این صورت را ابطال کنید؟"));
        doChangeSeries.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این صورت را ابطال کنید؟"));
        doChangeSeries_Open.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این شماره صورت را ابطال کنید؟"));
        doDelete.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این سرویس را حذف کنید؟"));

        #region Auto
        string reg = @"
        <script language='javascript'>
function CheckAuto(){
    document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRB.OmnikeyRB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	 //کارت هوشمند 

    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseAuto.ClientID)
       .Replace("mSmartCard", mCar.ClientID);
        RegisterStartupScript("ReadClickScriptAuto", reg);
        doChooseAuto.Attributes.Add("OnClick", "CheckAuto()");
        #endregion

        #region Driver 1
        reg = @"
        <script language='javascript'>
function Check1(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseDriver1.ClientID)
       .Replace("mSmartCard", mDriver1.ClientID);
        RegisterStartupScript("ReadClickScript1", reg);
        doChooseDriver1.Attributes.Add("OnClick", "Check1()");
        #endregion

        #region Driver 2
        reg = @"
        <script language='javascript'>
function Check2(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseDriver2.ClientID)
     .Replace("mSmartCard", mDriver2.ClientID);
        RegisterStartupScript("ReadClickScript2", reg);
        doChooseDriver2.Attributes.Add("OnClick", "Check2()");
        #endregion

        #region Driver3
        reg = @"
        <script language='javascript'>
function Check3(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseDriver3.ClientID)
  .Replace("mSmartCard", mDriver3.ClientID);
        RegisterStartupScript("ReadClickScript3", reg);
        doChooseDriver3.Attributes.Add("OnClick", "Check3()");
        #endregion

        #region Write In Card

        reg = @"
        <script language='javascript1.2'>
var cardpath='c:\\SmartDb.udb';
function cardsabt() {
	alert('ثبت اطلاعات صورت در کارت\n\n لطفا کارت ماشین به شماره CARNO  را داخل کارت خوان قرار دهید');
	try{
	  var rd = new ActiveXObject('SmartRLWB.OmnikeyRLWB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById(button).disabled = false;	  
	  return 0;
	}
  var flag = rd.SCEstablish;
  flag = rd.SCConnect;
  if(!flag) alert(' اشکال در کارت');
  else {
	  flag = rd.SCSetPath(cardpath);
	  if(!flag) alert(' اشکال در مسیر فایل رمزگذاری شرکت');	  

	  else {
			var surat=Number('SURET');
			var type=Number('1');
			var date=Number('DATE'); //13880808
			var time=Number('TIME'); //2200
			var company=Number('COMPANYCODE');	//Code Sherket=73023
			var source=Number('SOURCE');//73310000
			var distin=Number('DEST');//11320000
			var drvcard1=Number('DRIVERCARD1');//1993116
			var drvcard2=Number('DRIVERCARD2');//2005171
			var pass=Number('PASSENGERS');//13
			var price=Number('PRICE');//730000
			var cardpsw='PASSWORD';		//Pass=xuw240
			var cardid=Number('USERID');		//UserNo=8
			var pswlen=cardpsw.length;						
			flag=rd.SCCheckUserIdPassword(cardid, cardpsw , pswlen) ;
			if(!flag) alert(' اشکال در کد کاربری و رمزعبور کارت ');	
			else {
				flag = rd.SCWriteSooratVazeat(surat, type, date, time, company, source, distin, drvcard1, drvcard2, pass, price, cardid, cardpsw, pswlen);
				if(!flag) 
                    alert(' اشکال در فایل ترابری');
                else
                    alert('صورت وضعیت با موفقیت ثبت شد.');
			}	
	}		 
  }  
 rd.SCDisconnect;
 rd.SCRelease;
}
function cardread() {
	try{
	  var rd = new ActiveXObject('CardReadInfo.CardRead');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById(button).disabled = false;	  
	  return 0;
	}
  var str = rd.ReadCardInfo(cardpath);
  if(str)  {
		str=str.split('@');
		var surat=str[0];
		var type=str[1];
		var date=str[2];
		var time=str[3];
		var company=str[4];
		var source=str[5];
		var distin=str[6];
		var drvcard1=str[7];
		var drvcard2=str[8];
		var pass=str[9];
		var price=str[10];
		date=String(date);
		date=date.substr(6,2)+'/'+date.substr(4,2)+'/'+date.substr(0,4);
		time=String(time);
		time=time.substr(0,2)+':'+time.substr(2,2);
		document.getElementById('surat').value=surat;
		document.getElementById('date').value=date;
		document.getElementById('time').value=time;
		document.getElementById('company').value=company;
		document.getElementById('source').value=source;
		document.getElementById('distin').value=distin;
		document.getElementById('pass').value=pass;
		document.getElementById('price').value=price;
	}		 
}
</script>
    ".Replace("button", doSaveInCard.ClientID)
     .Replace("CARNO", Helper.Instance.GetCarPlate(mTrip.CarID))
     .Replace("USERID", Helper.Instance.GetSettingValue("UserID"))
     .Replace("COMPANYCODE", Helper.Instance.GetSettingValue("CompanyCode"))
     .Replace("PASSWORD", Helper.Instance.GetSettingValue("Password"))
     .Replace("SURET", mTrip.No.ToString())
     .Replace("DATE", Tool.ToPersianDate(mTrip.Date, DateTime.Now).Replace("/", ""))
     .Replace("TIME", mTrip.DepartureTime2.Replace(":", ""))
     .Replace("SOURCE", Helper.Instance.GetCityCode(mTrip.Service.Path.SrcCityID))
     .Replace("DEST", Helper.Instance.GetCityCode(mTrip.Service.Path.DestCityID))
     .Replace("DRIVERCARD1", Helper.Instance.GetDriverSmartCard(mTrip.DriverID1))
     .Replace("DRIVERCARD2", Helper.Instance.GetDriverSmartCard(mTrip.DriverID2))
     .Replace("PASSENGERS", Helper.Instance.GetNumTickets(mTrip.ID).ToString())
     .Replace("PRICE", Helper.Instance.GetPrice(mTrip.ID).ToString());//987722

        //    var source=Number('');//73310000
        //    var distin=Number('');//11320000
        //    var drvcard1=Number('DRIVERCARD1');//1993116
        //    var drvcard2=Number('DRIVERCARD2');//2005171
        //    var pass=Number('');//13
        //    var price=Number('');//730000
        RegisterStartupScript("WriteClickScript", reg);
        doSaveInCard.Attributes.Add("OnClick", "cardsabt();cardread();");
        doReadFromCard.Attributes.Add("OnClick", "cardread();");
        #endregion
    }
    void RegisterAutoScript()
    {
        string reg = @"
        <script language='javascript'>
function CheckAuto(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRB.OmnikeyRB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	 //کارت هوشمند 

    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReaDCard').disabled = false;	
}

</script>".Replace("ReaDCard", doChooseAuto.ClientID)
         .Replace("mSmartCard", mCar.ClientID);

        RegisterStartupScript("ReadClickScriptAuto", reg);
        doChooseAuto.Attributes.Add("OnClick", "CheckAuto()");
    }
    private void CreateTripDestControls()
    {
        mTripDests.Controls.Clear();
        mTripDests.Controls.Add(new LiteralControl(string.Format("{0}&nbsp;:{1}&nbsp;,&nbsp;"
              , mTrip.Service.Path.City1.Title, Helper.Instance.GetNumTickets(mTrip.ID, mTrip.Service.Path.DestCityID))));
        mTripDestsLbl.Text = string.Format("{0}:{1}"
              , mTrip.Service.Path.City1.Title, Helper.Instance.GetNumTickets(mTrip.ID, mTrip.Service.Path.DestCityID));
        List<ServiceDest> dests = Helper.Instance.GetServiceDests(mTrip.ServiceID);
        if (dests == null)
            return;

        // --------------------------------------------
        for (int i = 0; i < dests.Count; i++)
        {
            mTripDests.Controls.Add(new LiteralControl(string.Format("{0}&nbsp;:{1}&nbsp;,&nbsp;"
                , dests[i].City.Title, Helper.Instance.GetNumTickets(mTrip.ID, dests[i].CityID))));
            mTripDestsLbl.Text = mTripDestsLbl.Text + string.Format(" - {0}:{1}"
                , dests[i].City.Title, Helper.Instance.GetNumTickets(mTrip.ID, dests[i].CityID));
        }
        // -------------------------------------------------
        #region oldCode
        //mTripDests.Controls.Add(new LiteralControl("<table>"));
        //mTripDests.Controls.Add(new LiteralControl("<tr><td>شهر</td><td>قیمت</td><td>عوارض</td><td> بیمه سرنشین</td></tr>"));
        //for (int i = 0; i < dests.Count; i++)
        //{
        //    mTripDests.Controls.Add(new LiteralControl(string.Format("<tr><td>{0}</td>", dests[i].City.Title)));
        //    TripDest dest = Helper.Instance.GetTripDest(mTrip.ID, dests[i].CityID);
        //    mTripDests.Controls.Add(new LiteralControl("<td>"));
        //    TextBox tx = new TextBox();
        //    tx.ID = "txp" + dests[i].ID.ToString();
        //    tx.Width = new Unit(50);
        //    if (!IsPostBack)
        //    {
        //        if (dest != null)
        //            tx.Text = dest.Price.ToString();
        //        else
        //            tx.Text = dests[i].Price.ToString();
        //    }
        //    mTripDests.Controls.Add(tx);
        //    mTripDests.Controls.Add(new LiteralControl("</td>"));

        //    mTripDests.Controls.Add(new LiteralControl("<td>"));
        //    tx = new TextBox();
        //    tx.ID = "txt" + dests[i].ID.ToString();
        //    tx.Width = new Unit(50);
        //    if (!IsPostBack)
        //    {
        //        if (dest != null)
        //            tx.Text = dest.Toll.ToString();
        //        else
        //            tx.Text = dests[i].Toll.ToString();
        //    }
        //    mTripDests.Controls.Add(tx);
        //    mTripDests.Controls.Add(new LiteralControl("</td>"));

        //    mTripDests.Controls.Add(new LiteralControl("<td>"));
        //    tx = new TextBox();
        //    tx.ID = "txi" + dests[i].ID.ToString();
        //    tx.Width = new Unit(50);
        //    if (!IsPostBack)
        //    {
        //        if (dest != null)
        //            tx.Text = dest.Insurance.ToString();
        //        else
        //            tx.Text = dests[i].Insurance.ToString();
        //    }
        //    mTripDests.Controls.Add(tx);
        //    mTripDests.Controls.Add(new LiteralControl("</td>"));
        //    mTripDests.Controls.Add(new LiteralControl("</tr>"));
        //}
        //mTripDests.Controls.Add(new LiteralControl("</table>"));
        #endregion
    }
    private void SaveTripDests()
    {
        return;
        List<ServiceDest> dests = Helper.Instance.GetServiceDests(mTrip.ServiceID);
        if (dests == null)
            return;
        for (int i = 0; i < dests.Count; i++)
        {
            TripDest dest = Helper.Instance.GetTripDest(mTrip.ID, dests[i].CityID);
            if (dest == null)
            {
                dest = new TripDest();
                dest.TripID = mTrip.ID;
                dest.Trip = mTrip;
                dest.CityID = dests[i].CityID;
                dest.City = dests[i].City;
                Helper.Instance.DB.TripDests.InsertOnSubmit(dest);
            }
            TextBox tx = mTripDests.FindControl("txp" + dests[i].ID.ToString()) as TextBox;
            dest.Price = Tool.GetInt(tx.Text, dest.Price);

            tx = mTripDests.FindControl("txt" + dests[i].ID.ToString()) as TextBox;
            dest.Toll = Tool.GetInt(tx.Text, dest.Toll);

            tx = mTripDests.FindControl("txi" + dests[i].ID.ToString()) as TextBox;
            dest.Insurance = Tool.GetInt(tx.Text, dest.Insurance);
            Helper.Instance.Update();
        }

    }

    private bool ButtonIsClicked(Button btn)
    {
        string clientID = btn.ClientID.Replace('_', '$');

        for (int i = 0; i < Request.Form.AllKeys.Length; i++)
        {
            if (Request.Form.AllKeys[i] == clientID)
                return true;
        }
        clientID = '$' + btn.ID;
        for (int i = 0; i < Request.Form.AllKeys.Length; i++)
        {
            if (Request.Form.AllKeys[i] != null && Request.Form.AllKeys[i].EndsWith(clientID))
                return true;
        }
        return false;
    }
    private void BindCars()
    {
        try
        {
            List<Car> all = mTrip.CarTypeID.HasValue ? Helper.Instance.GetCarsByCarType(mTrip.CarTypeID.Value)
                : Helper.Instance.GetCarsBySpecialID(mTrip.Service.SpecialID);
            for (int i = 0; i < all.Count; i++)
            {
                if (all[i].Enabled == true || all[i].ID == mTrip.CarID)
                    continue;
                else
                {
                    all.RemoveAt(i);
                    i--;
                }
            }
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            mCarID.DataValueField = "ID";
            mCarID.DataTextField = "Title";
            mCarID.DataSource = all;
            mCarID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += ex.Message + "<br>" + ex.StackTrace;
        }
    }
    private void BindInitData()
    {
        BindCars();
        try
        {
            List<Driver> all = Helper.Instance.GetEnableDrivers();
            for (int i = 0; i < all.Count; i++)
            {
                if (all[i].Enabled == true || all[i].ID == mTrip.DriverID1
                    || all[i].ID == mTrip.DriverID2 || all[i].ID == mTrip.DriverID3)
                    continue;
                else
                {
                    all.RemoveAt(i);
                    i--;
                }
            }
            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            mDriverID1.DataValueField = mDriverID2.DataValueField = mDriverID3.DataValueField = "ID";
            mDriverID1.DataTextField = mDriverID2.DataTextField = mDriverID3.DataTextField = "Title";
            mDriverID1.DataSource = mDriverID2.DataSource = mDriverID3.DataSource = all;
            mDriverID1.DataBind();
            mDriverID2.DataBind();
            mDriverID3.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += ex.Message + "<br>" + ex.StackTrace;
        }
    }
    private void BindData()
    {
        doInvalid.Enabled = !mTrip.Closed;
        doGetTrackingCode.Enabled = string.IsNullOrEmpty(mTrip.TrackCode);

        mServiceID.Text = mTrip.Service.ServiceType2;
        mPathID.Text = mTrip.Service.Path.Title;
        int numTickets = Helper.Instance.GetNumTickets(mTrip.ID);
        mNumTickets.Text = numTickets.ToString();

        mDate.SelectedDate = mTrip.Date;
        Tool.SetSelected(mCarID, mTrip.CarID);
        Tool.SetSelected(mDriverID1, mTrip.DriverID1);
        Tool.SetSelected(mDriverID2, mTrip.DriverID2);
        Tool.SetSelected(mDriverID3, mTrip.DriverID3);
        mNo.Text = mTrip.No.ToString();
        mSeriesNo4.Enabled = !mTrip.IsMini;
        if (!mSeriesNo4.Enabled)
            mSeriesNo4.BackColor = System.Drawing.Color.LightGray;
        try
        {
            string[] vals = mTrip.Series.Split(',');
            mSeriesNo1.Text = vals[0];
            mSeriesNo2.Text = vals[1];
            mSeriesNo3.Text = vals[2];

            mSeriesNo4.Text = vals[3];
        }
        catch { }

        try
        {
            //mTicketless.Text = (mTrip.Service.CarType.Layout.NumChairs - numTickets).ToString("N0");
        }
        catch { }
        if (!mTrip.Closed)
        {
            string series = mTrip.Series;
            if (string.IsNullOrEmpty(mTrip.Series) || mTrip.Series.Replace(",", "").Trim().Length == 0)
                try
                {
                    if (mTrip.IsMini)
                    {
                        mSeriesNo1.Text = Helper.Instance.GetSettingValue("SeriesNoMini1");
                        mSeriesNo2.Text = Helper.Instance.GetSettingValue("SeriesNoMini2");
                        mSeriesNo3.Text = Helper.Instance.GetSettingValue("SeriesNoMini3");
                        series = string.Format("{0},{1},{2}"
                               , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                               , Helper.Instance.GetSettingValue("SeriesNoMini3"));
                    }
                    else
                    {
                        mSeriesNo1.Text = Helper.Instance.GetSettingValue("SeriesNo1");
                        mSeriesNo2.Text = Helper.Instance.GetSettingValue("SeriesNo2");
                        mSeriesNo3.Text = Helper.Instance.GetSettingValue("SeriesNo3");
                        mSeriesNo4.Text = Helper.Instance.GetSettingValue("SeriesNo4");
                        series = string.Format("{0},{1},{2},{3}"
                               , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
                               , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
                    }
                }
                catch { }
            if (mTrip.No < 1)
                mNo.Text = Helper.Instance.GetNextSeriesStart(series, mTrip.IsMini).ToString();
        }

        mStamp.Text = mTrip.Stamp.ToString("N0");
        if (!mTrip.Closed)
        {
            int stamp = Tool.GetInt(Helper.Instance.GetSettingValue("Stamp"), Null.NullInteger);
            if (stamp != Null.NullInteger)
                mStamp.Text = stamp.ToString("N0");
        }
        if (mTrip.Comission >= 0 && mTrip.Closed)
            mComission.Text = mTrip.Comission.ToString("N0");
        mBodyInsurance.Text = mTrip.BodyInsurance.ToString("N0");
        mOthers.Text = mTrip.Others.HasValue ? mTrip.Others.Value.ToString("N0") : "0";
        if (mTrip.IsMini)
            mOthers.Text = "0";

        mExtraCost.Text = mTrip.ExtraCost.ToString();
        mExtraCost2.Text = (numTickets * Math.Abs(mTrip.ExtraCost)).ToString();

        mClosed.Checked = mTrip.Closed;

        mPrice.Text = mTrip.Price.ToString("N0");
        mToll.Text = mTrip.Toll.ToString();
        mInsurance.Text = mTrip.Insurance.ToString("N0");
        mReception.Text = mTrip.Reception.ToString("N0");

        mToll2.Text = mTrip.Toll2.HasValue ? mTrip.Toll2.Value.ToString("N0") : "0";
        mComission2.Text = mTrip.Comission2.HasValue ? mTrip.Comission2.Value.ToString("N0") : "0";
        mTotalPrice.Text = mTrip.TotalPrice.HasValue ? mTrip.TotalPrice.Value.ToString("N0") : "0";
        mAutoShare.Text = mTrip.AutoShare.HasValue ? mTrip.AutoShare.Value.ToString("N0") : "0";
        mOtherDeficits.Text = mTrip.OtherDeficits.HasValue ? mTrip.OtherDeficits.Value.ToString("N0") : "0";
        mSumDeficits.Text = mTrip.SumDeficits.HasValue ? mTrip.SumDeficits.Value.ToString("N0") : "0";
        mTotalReception.Text = mTrip.TotalReception.ToString("N0");


        if (!mTrip.Closed)
            doReturn.NavigateUrl = "Trips.aspx";
        else
            doReturn.NavigateUrl = "Closeds.aspx";

        if (mTrip.Closed)
        {
//
            //if (mTrip.CloseDate.Value.Date != DateTime.Now.Date)
            //    doClose.Enabled = false;
            doClose.Text = "ذخيره و بازکردن F7";
            doSendToTechnical.Enabled = false;
        }
        else
        {
            doClose.Text = "ذخيره و بستن F7";
        }

        CreateTripDestControls();
        try
        {
            mBodyInsuranceWage.Text = Helper.Instance.GetWagePrice(0, int.Parse(mTrip.Service.Path.City1.Country.Code) > 1 ? true : false, mTrip.CarType.Layout.NumChairs > 30 ? 0 : mTrip.CarType.Layout.NumChairs > 5 ? 1 : 2, mTrip.Service.Path.KMs).ToString();
            mInsuranceWage.Text = Helper.Instance.GetWagePrice(1, int.Parse(mTrip.Service.Path.City1.Country.Code) > 1 ? true : false, mTrip.CarType.Layout.NumChairs > 30 ? 0 : mTrip.CarType.Layout.NumChairs > 5 ? 1 : 2, mTrip.Service.Path.KMs).ToString();
        }
        catch { }

        bool enableClose = ValidateDates();
        //mTrip.Driver mHourDate

        try
        {
            mHour.Text = mTrip.DepartureTime2.Split(':')[0];
        }
        catch { }
        try
        {
            mMinute.Text = mTrip.DepartureTime2.Split(':')[1];
        }
        catch { }

        //doPrint.Enabled = true;
        if (mTrip.CarID == null)
            doPrint.Enabled = false;
        else if (mTrip.DriverID1 == null && mTrip.DriverID2 == null && mTrip.DriverID3 == null)
            doPrint.Enabled = false;

        int sFrom = -1, sTo = -1;
        if (mTrip.IsSewari || mTrip.IsMini)
        {
            sFrom = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartMiniFrom"), -1);
            sTo = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartMiniTo"), -1);
        }
        else
        {
            sFrom = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartFrom"), -1);
            sTo = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartTo"), -1);
        }
        if (sFrom == -1 || sTo == -1)
        {
            enableClose = false;
            mWarnings.Text += "در تنظیمات بازه سری صورت مشخص نشده است <br />";
        }
        int sCur = Tool.GetInt(mNo.Text, 0);
        if (sCur > sTo)
        {
            mNo.BackColor = System.Drawing.Color.FromArgb(255, 10, 10);
            enableClose = false;
            mWarnings.Text += "سری صورت خارج از  بازه سری صورت در تنظیمات است <br />";
            string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Manager/Settings.aspx');
}
window.open('Manager/Settings.aspx');
//window.onload=openprint;
</script>";
            Page.RegisterStartupScript("openprint", open);
        }
        else
            mNo.BackColor = System.Drawing.Color.White;

        if (enableClose)
        {
            doPrint.Enabled = doClose.Enabled = enableClose;
            //if(mTrip.Closed)

            if (mTrip.Insurance == 0)
            {
                doPrint.Enabled = false;
                mWarnings.Text += "مقدار بیمه سرنشین صفر است. چاپ غيرفعال شده است.";
            }

            if (mTrip.IsBusByCarType)
                if (mTrip.BodyInsurance <= 0)
                {
                    doPrint.Enabled = false;
                    mWarnings.Text += "بیمه بدنه برای اتوبوس باید بزرگتر از صفر باشد. چاپ غيرفعال شده است.";
                }
        }
        else
        {
            if (!mTrip.Closed)
                doClose.Enabled = false;
            doPrint.Enabled = enableClose;
            mWarnings.Text += "-----------------> بستن و چاپ غيرفعال شده است";
        }

        if (mTrip.Closed && mTrip.CloseDate.HasValue)
            mCloseDate.Text = string.Format(" {0}:{1}"
                , mTrip.CloseDate.Value.Hour.ToString().PadLeft(2, '0'), mTrip.CloseDate.Value.Minute.ToString().PadLeft(2, '0'))
                + " " + Tool.ToPersianDate(mTrip.CloseDate, "");
        else
            mCloseDate.Text = "-";

        mTrackCode.Text = mTrip.TrackCode;
        mTrackDate.Text = mTrip.TrackDate.HasValue ? Tool.ToPersianDate(mTrip.TrackDate.Value, "") : "";

        // ----------------------
        // Handle Invalidation + No
        if (mTrip.No < 1) // it has no No yet
        {
            mNo.BackColor = System.Drawing.Color.Aqua;
            doChangeSeries_NoCode.Enabled = false;
            doChangeSeries.Enabled = false;
            doChangeSeries_Open.Enabled = true;
            doDelete.Enabled = true;
        }
        else if (string.IsNullOrEmpty(mTrip.TrackCode))
        {
            doChangeSeries_NoCode.Enabled = true;
            doChangeSeries.Enabled = false;
            doChangeSeries_Open.Enabled = false;
            doDelete.Enabled = false;
        }
        else
        {
            doChangeSeries_NoCode.Enabled = false;
            doChangeSeries.Enabled = true;
            doChangeSeries_Open.Enabled = false;
            doDelete.Enabled = false;
        }

        if ((mTrip.TripStatus == (int)TripStatus.None || mTrip.TripStatus==null) && !mTrip.Closed && Tool.GetBool(Helper.Instance.GetSettingValue("Technical"), false))
        {
            doTechnical.Visible = false;
            doClose.Enabled = false;
            doPrint.Enabled = false;
        }
        if (mTrip.TripStatus != (int)TripStatus.None && mTrip.TripStatus != (int)TripStatus.Confirmed && mTrip.TripStatus!=null)
        {
            doClose.Enabled = false;
            doPrint.Enabled = false;
            if (mTrip.TripStatus == (int)TripStatus.Pending)
            {
                mError.Text = "صورت وضعیت توسط مدیر فنی در حال بررسی است. بستن و چاپ غیر فعال است. " + mTrip.Comments;
                doSendToTechnical.Enabled = false;
            }
            if (mTrip.TripStatus == (int)TripStatus.Rejected)
            {
                mError.Text = "صورت وضعیت توسط مدیر فنی رد شده است. بستن و چاپ غیر  فعال است. " + mTrip.Comments;
            }
            mError.Visible = true;
        }
        if (mTrip.TripStatus==(int)TripStatus.Confirmed)
        {
            doSendToTechnical.Enabled = false;
            doTechnical.Visible = true;
            doClose.Enabled = true;
            doPrint.Enabled = true;
        }

        if (!Tool.GetBool(Helper.Instance.GetSettingValue("Technical"),false))
        {
            doSendToTechnical.Visible = false;
            doCheckFani.Visible = false;
        }
    }
    bool ValidateDates()
    {
        mWarnings.Text = "";
        bool enableClose = true;
        #region Check Dates - Drivers
        if (null != mTrip.Driver)
        {
            int hDays = (int)(mTrip.Driver.HourDate - DateTime.Now).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 1 اعتبار ندارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 1 فقط {0} روز اعتبار دارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(mTrip.Driver.DrivingDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 1 اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 1 فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        if (null != mTrip.Driver1)
        {
            int hDays = (int)(mTrip.Driver1.HourDate - DateTime.Now).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 2 اعتبار ندارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 2 فقط {0} روز اعتبار دارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(mTrip.Driver1.DrivingDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 2 اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 2 فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        if (null != mTrip.Driver2)
        {
            int hDays = (int)(mTrip.Driver2.HourDate - DateTime.Now).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 3 اعتبار ندارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 3 فقط {0} روز اعتبار دارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(mTrip.Driver2.DrivingDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("تاريخ کارت سلامت راننده 3 اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 3 فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        #endregion

        #region CheckDates - Auto
        if (null != mTrip.CarID)
        {
            Car car = Helper.Instance.GetCar(mTrip.CarID);
            int vDays = (int)(car.VisitDate - DateTime.Now).TotalDays;
            if (vDays < 0)
            {
                mWarnings.Text += string.Format("معاینه فني اتوکار اعتبار ندارد<br>", vDays);
                if (vDays <= 1)
                    enableClose = false;
            }
            else if (vDays <= 5)
            {
                mWarnings.Text += string.Format("معاینه فني اتوکار فقط {0} روز اعتبار دارد<br>", vDays);
                if (vDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(car.ThirdPersonDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("بيمه شخص ثالث اتوکار اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("بيمه شخص ثالث اتوکار فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        #endregion
        return enableClose;
    }
    private void FillTrip()
    {
        mTrip.Date = mDate.SelectedDate == Null.NullDate ? mTrip.Date : mDate.SelectedDate;

        if (mDriverID1.SelectedIndex > 0)
            mTrip.DriverID1 = Tool.GetInt(mDriverID1.SelectedValue, null);
        else
            mTrip.DriverID1 = null;
        if (mDriverID2.SelectedIndex > 0)
            mTrip.DriverID2 = Tool.GetInt(mDriverID2.SelectedValue, null);
        else
            mTrip.DriverID2 = null;
        if (mDriverID3.SelectedIndex > 0)
            mTrip.DriverID3 = Tool.GetInt(mDriverID3.SelectedValue, null);
        else
            mTrip.DriverID3 = null;

        int? oldCarID = mTrip.CarID;
        mTrip.CarID = Tool.GetInt(mCarID.SelectedValue, Null.NullInteger);
        if (mTrip.CarID == Null.NullInteger && mTrip.DriverID1 != null)
        {
            Driver dr = Helper.Instance.GetDriver(mTrip.DriverID1.Value);
            if (dr != null && dr.CarID != null)
            {
                Car cr = Helper.Instance.GetCar(dr.CarID);
                if (cr != null)
                {
                    mTrip.CarType = cr.CarType;
                    mTrip.CarTypeID = cr.CarTypeID;
                    mTrip.CarID = cr.ID;
                }
            }
        }

        if (mTrip.CarID != Null.NullInteger)
        {
            #region Process Car
            Car c = Helper.Instance.GetCar(mTrip.CarID.Value);
            if (c != null)
            {
                Service s = Helper.Instance.GetService(mTrip.ServiceID);
                if (c.CarType.SpecialID != s.SpecialID)
                {
                    mError.Text += "نحوه سرویس دهی اتوکار انتخاب شده با نحوه سرویس دهی تعريف شده در سرويس يکي نيست.";
                    return;
                }
                if (true)
                {
                    mTrip.Comission = c.Commission;
                    if (string.IsNullOrEmpty(mComission.Text))
                    {
                        mComission.Text = mTrip.Comission.ToString("N0");
                    }
                    if (c.Insurance != null && c.Insurance.InsuranceKMs != null && c.Insurance.InsuranceKMs.Count > 0)
                    {
                        for (int i = 0; i < c.Insurance.InsuranceKMs.Count; i++)
                        {
                            if (c.Insurance.InsuranceKMs[i].StartKM <= s.Path.KMs && s.Path.KMs <= c.Insurance.InsuranceKMs[i].EndKM)
                            {
                                mTrip.BodyInsurance = c.Insurance.InsuranceKMs[i].Price;
                                mBodyInsurance.Text = mTrip.BodyInsurance.ToString("N0");
                                break;
                            }
                        }
                    }
                }
            }
            #endregion
        }
        else
            mTrip.CarID = null;


        if (mTrip.Closed)
        {
            mTrip.No = Tool.GetInt(mNo.Text, 0);
            if (mTrip.IsMini)
                mTrip.Series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
            else
                mTrip.Series = string.Format("{0},{1},{2},{3}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text, mSeriesNo4.Text);
        }

        mTrip.Toll = Tool.GetInt(mToll.Text, 0);
        mTrip.Comission = Tool.GetInt(mComission.Text, 0);
        mTrip.BodyInsurance = Tool.GetInt(mBodyInsurance.Text, mTrip.BodyInsurance);
        mTrip.Others = Tool.GetInt(mOthers.Text, 0);
        mTrip.Insurance = Tool.GetInt(mInsurance.Text, 0);
        mTrip.Stamp = Tool.GetInt(mStamp.Text, 0);
        mTrip.ExtraCost = Tool.GetInt(mExtraCost.Text, 0);
        mTrip.Reception = Tool.GetInt(mReception.Text, 0);
        mTrip.Price = Tool.GetInt(mPrice.Text, 0);

        mTrip.TotalPrice = Tool.GetInt(mTotalPrice.Text, 0);
        mTrip.Toll2 = Tool.GetInt(mToll2.Text, 0);
        mTrip.Comission2 = Tool.GetInt(mComission2.Text, 0);
        mTrip.OtherDeficits = Tool.GetInt(mOtherDeficits.Text, 0);
        mTrip.SumDeficits = Tool.GetInt(mSumDeficits.Text, 0);
        mTrip.AutoShare = Tool.GetInt(mAutoShare.Text, 0);
        mTrip.InsuranceWage = Tool.GetInt(mInsuranceWage.Text, 0);
        mTrip.BodyInsuranceWage = Tool.GetInt(mBodyInsuranceWage.Text, 0);
        mTrip.TotalReception = CalcTotalReception();

        int hour = Tool.GetInt(mHour.Text, Null.NullInteger);
        if (hour < 0 || hour > 23)
        {
            mError.Text += "ساعت باید بین 0 و 23 باشد.";
            return;
        }
        int minute = Tool.GetInt(mMinute.Text, Null.NullInteger);
        if (minute < 0 || minute > 59)
        {
            mError.Text += "دقیقه باید بین 0 و 59 باشد.";
            return;
        }
        mTrip.DepartureTime = Tool.FixTime(string.Format("{0}:{1}", hour.ToString().PadLeft(2, '0'), minute.ToString().PadLeft(2, '0')));
    }
    private void Calc()
    {
        FillTrip();

        RateItem rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, mTrip.Service.Path.SrcCityID, mTrip.Service.Path.DestCityID, mTrip.Service.SpecialID, mTrip.Date);
        decimal numTickets = Helper.Instance.GetNumTickets(mTrip.ID);
        decimal price = Helper.Instance.GetPrice(mTrip.ID) - numTickets * (rateItem == null ? 0 : rateItem.PureCost);

        decimal y = price - mTrip.Insurance - mTrip.BodyInsurance - (mTrip.ExtraCost * numTickets) - mTrip.TotalReception - mTrip.Stamp;
        decimal toll2 = FormulaEvaluater.Evaluate(mTrip.ID, Helper.Instance.GetFieldFormul("Toll"));
        y = y - toll2;
        decimal commision2 = FormulaEvaluater.Evaluate(mTrip.ID, Helper.Instance.GetFieldFormul("Commission"));


        mTotalPrice.Text = mTotalPricelbl.Text = (price - (price % 100)).ToString("N0");
        mToll2.Text = (toll2 - (toll2 % 100)).ToString("N0");
        mComission2.Text = (commision2 - (commision2 % 100)).ToString("N0");


        decimal otherDeficits = mTrip.BodyInsurance + mTrip.Insurance + (mTrip.ExtraCost * numTickets) + mTrip.Stamp;
        mOtherDeficits.Text = (otherDeficits - (otherDeficits % 100)).ToString("N0");
        decimal sums = toll2 + otherDeficits + commision2 + mTrip.TotalReception;
        mSumDeficits.Text = (sums - (sums % 100)).ToString("N0");

        decimal autoShare = price - (toll2 + otherDeficits + commision2 + mTrip.TotalReception);
        decimal others = mTrip.Others.HasValue ? mTrip.Others.Value : 0;
        autoShare = autoShare - others;
        mAutoShare.Text = (autoShare - (autoShare % 100)).ToString("N0");
    }
    protected void doChangeComission_Click(object sender, EventArgs e)
    {
        FillTrip();

        decimal numTickets = Helper.Instance.GetNumTickets(mTrip.ID);
        decimal price = Helper.Instance.GetPrice(mTrip.ID);
        decimal y = price - mTrip.Insurance - mTrip.BodyInsurance - (mTrip.ExtraCost * numTickets) - mTrip.TotalReception - mTrip.Stamp;
        decimal toll2 = y * mTrip.Toll / 100;
        y = y - toll2;
        decimal commision2 = Tool.GetInt(mComission2.Text, Null.NullInteger);// y * mTrip.Comission / 100;

        mTotalPrice.Text = mTotalPricelbl.Text = (price - (price % 100)).ToString("N0");
        mToll2.Text = (toll2 - (toll2 % 100)).ToString("N0");
        mComission2.Text = (commision2 - (commision2 % 100)).ToString("N0");

        decimal otherDeficits = mTrip.BodyInsurance + mTrip.Insurance + (mTrip.ExtraCost * numTickets) + mTrip.Stamp;
        mOtherDeficits.Text = (otherDeficits - (otherDeficits % 100)).ToString("N0");
        decimal sums = toll2 + otherDeficits + commision2 + mTrip.TotalReception;
        mSumDeficits.Text = (sums - (sums % 100)).ToString("N0");

        decimal autoShare = price - (toll2 + otherDeficits + commision2 + mTrip.TotalReception);
        decimal others = mTrip.Others.HasValue ? mTrip.Others.Value : 0;
        autoShare = autoShare - others;
        mAutoShare.Text = (autoShare - (autoShare % 100)).ToString("N0");
    }
    int GetNumTickets(Ticket t)
    {
        if (string.IsNullOrEmpty(t.Chairs))
            return 0;
        string ch = t.Chairs;
        if (ch.StartsWith(";"))
            ch = ch.Substring(1);
        if (ch.EndsWith(";"))
            ch = ch.Substring(0, ch.Length - 1);
        return ch.Split(';').Length;
    }
    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
    bool Save(bool close)
    {
        FillTrip();
        Car car = Helper.Instance.GetCar(mTrip.CarID);
        if (car != null)
            if (!CheckDateCar(car) && error == true)
            {
                return false;
            }
        Driver driver = Helper.Instance.GetDriver(mTrip.DriverID1);
        if (driver != null)
            if (!CheckDateDriver(driver) && error == true)
            {
                return false;
            }
        bool wasClosed = !mTrip.Closed;
        if (wasClosed == false && close)
        {
            if (!mTrip.CarID.HasValue)
            {
                mError.Text += "برای بستن حتما باید اتوکار انتخاب شده باشد";
                return false;
            }
        }
        if (wasClosed == false && close)
        {
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text += "برای بستن حتما باید راننده اول انتخاب شده باشد";
                return false;
            }
        }
        if (close)
        {
            if (!wasClosed && close)
            {
                mTrip.CloseDate = DateTime.Now;
                mTrip.CloseUserID = SiteSettings.UserID;
            }
            mTrip.Closed = doClose.Text == "ذخيره و بازکردن F7" ? false : true;
            if (mTrip.Closed)
            {
                if (string.IsNullOrEmpty(mTrip.Series) || mTrip.Series.Trim(',').Length == 0 /*|| !wasClosed*/)
                {
                    if (mTrip.IsMini)
                        mTrip.Series = string.Format("{0},{1},{2}"
                           , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                           , Helper.Instance.GetSettingValue("SeriesNoMini3"));
                    else
                        mTrip.Series = string.Format("{0},{1},{2},{3}"
                           , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
                           , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
                }
                else
                {
                    string[] ss = string.IsNullOrEmpty(mTrip.Series) ? null : mTrip.Series.Split(',');
                    if (mTrip.IsMini)
                    {
                        if (ss == null || ss.Length < 3 || ss[0].Trim().Length == 0
                            || ss[1].Trim().Length == 0 || ss[2].Trim().Length == 0)
                            mTrip.Series = string.Format("{0},{1},{2}"
                                   , mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
                    }
                    else
                    {
                        if (ss == null || ss.Length < 4 || ss[0].Trim().Length == 0
                            || ss[1].Trim().Length == 0 || ss[2].Trim().Length == 0 || ss[3].Trim().Length == 0)
                            mTrip.Series = string.Format("{0},{1},{2},{3}"
                                   , mSeriesNo1.Text, mSeriesNo2.Text
                                   , mSeriesNo3.Text, mSeriesNo4.Text);
                    }
                }
                if (mTrip.No < 1 /*|| !wasClosed*/)
                    mTrip.No = Helper.Instance.GetNextSeriesStart(mTrip.Series, mTrip.IsMini);

                mNo.Text = mTrip.No.ToString();
                try
                {
                    string[] vals = mTrip.Series.Split(',');
                    mSeriesNo1.Text = vals[0];
                    mSeriesNo2.Text = vals[1];
                    mSeriesNo3.Text = vals[2];
                    if (!mTrip.IsMini)
                        mSeriesNo4.Text = vals[3];
                }
                catch { }
            }
        }

        // double check the SeriesNo
        if (mTrip.Closed)
        {
            try
            {
                string[] vals = mTrip.Series.Split(',');
                if (mTrip.IsMini)
                {
                    if (vals.Length < 3 || string.IsNullOrEmpty(vals[0].Trim()) || string.IsNullOrEmpty(vals[1].Trim())
                        || string.IsNullOrEmpty(vals[2].Trim()))
                    {
                        mError.Text += "سری صورت اشکال دارد. همه بخشهای سری صورت باید پر باشند";
                        return false;
                    }
                    mTrip.Series = mTrip.Series.Trim(',');
                }
                else
                {
                    if (vals.Length != 4 || string.IsNullOrEmpty(vals[0].Trim()) || string.IsNullOrEmpty(vals[1].Trim())
                        || string.IsNullOrEmpty(vals[2].Trim()) || string.IsNullOrEmpty(vals[3].Trim()))
                    {
                        mError.Text += "سری صورت اشکال دارد. همه بخشهای سری صورت باید پر باشند";
                        return false;
                    }
                }
            }
            catch { }
            try
            {
                var v = from t in Helper.Instance.DB.Trips
                        where t.ID != mTrip.ID && t.Series == mTrip.Series && t.No == mTrip.No
                        select t;
                if (v.Count() > 0)
                {
                    mError.Text += "هشدار : سری صورت تکراری است . این شماره در سرویسهای زیر استفاده شده است: <br />";
                    List<Trip> olds = v.ToList();
                    for (int i = 0; i < olds.Count; i++)
                    {
                        mError.Text += string.Format("<br />{0} ({1}) ({2} - {3}) شماره: {4}", olds[i].Service.Path.Title, olds[i].Service.ServiceType2, Tool.ToPersianDateRtl(olds[i].Date, ""), olds[i].DepartureTime2, olds[i].No);
                    }
                    mError.Text += "<br />";
                    //return false;
                }
            }
            catch { }
        }
        //try
        //{
        //    if (mTrip.Closed)
        //        Helper.Instance.RequestTrackCode(mTrip);
        //}
        //catch (Exception ex)
        //{
        //    mError.Text += "خطا در دریافت کد رهگیری صورت وضعیت: " + ex.Message + "<br />";
        //}

        SaveTripDests();
        if (Helper.Instance.Update())
        {
            if (wasClosed == false && mTrip.Closed)
                UpdateSettings();

            BindCars();
            Tool.SetSelected(mCarID, mTrip.CarID);
            // Delete Extra Tickets
            if (mTrip.CarType != null)
            {
                int tickets = Helper.Instance.GetNumTickets(mTrip.ID);
                if (tickets > mTrip.CarType.Layout.NumChairs)
                {
                    int toDel = tickets - mTrip.CarType.Layout.NumChairs.Value;
                    List<Ticket> ts = mTrip.Tickets.ToList();
                    List<Ticket> delets = new List<Ticket>();
                    while (true)
                    {
                        Ticket t = ts[ts.Count - 1];
                        if (t.NumChairs.HasValue == false || t.NumChairs <= toDel)
                        {
                            ts.RemoveAt(ts.Count - 1);
                            Helper.Instance.DeleteTicket(t);
                            tickets = Helper.Instance.GetNumTickets(mTrip.ID);
                            toDel = tickets - mTrip.CarType.Layout.NumChairs.Value;
                        }
                        else
                        {
                            t.NumChairs -= toDel;
                            int index = 1;
                            {
                                t.Chairs = ";m" + index++ + ";";
                                for (int x = 0; x < t.NumChairs.Value - 1; x++)
                                    t.Chairs = string.Format("{0}m{1};", t.Chairs, index++);
                            }
                            Helper.Instance.Update();
                            tickets = Helper.Instance.GetNumTickets(mTrip.ID);
                            toDel = tickets - mTrip.CarType.Layout.NumChairs.Value;
                        }
                        if (toDel <= 0)
                            break;
                    }
                }
            }
            return true;
        }
        return false;
    }
    protected void doSave_Click(object sender, EventArgs e)
    {

        if (Save(false))
        {
            //if (mTrip.Closed)
            //    Response.Redirect("Closeds.aspx");
            //else
            //    Response.Redirect("Sale.aspx");
            BindData();
            mError.Text += "بروز رساني با موفقیت انجام شد";
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
        }
    }
    protected void doClose_Click(object sender, EventArgs e)
    {
        if (doClose.Text == "ذخيره و بستن F7")
            mTrip.Closed = true;
        else
            mTrip.Closed = false;
        int numReserves = Helper.Instance.GetNumReserves(mTrip.ID);
        if (numReserves > 0)
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserveClose"), true) == false)
            {
                mError.Text += string.Format("این سرویس تعداد {0} بلیط رزروی دارد و نمیتوان آنرا بست.", numReserves);
                return;
            }

        Calc();
        //if (mTrip.BodyInsurance == 0)
        //{
        //    mError.Text += "مقدار بیمه بدنه صفر است. نمیتوان صورت را  بست.";
        //    return;
        //}
        if (mTrip.Insurance == 0)
        {
            mError.Text += "مقدار بیمه سرنشین صفر است. نمیتوان صورت را  بست.";
            return;
        }
        // 90-08-10 why?
        //if (mTrip.No == 0)
        //{
        //    mError.Text += "شماره صورت صفر یا خالی است. نمیتوان صورت را  بست.";
        //    return;
        //}
        if (Helper.Instance.GetNumTickets(mTrip.ID) == 0)
        {
            mError.Text += "تعداد بلیط صفر است. نمیتوان صورت را  بست.";
            return;
        }
        if (Tool.GetFloat(mTotalPricelbl.Text, 0) <= 0)
        {
            mError.Text += "کل کرايه صفر یا منفی است. نمیتوان صورت را  بست.";
            return;
        }
        if (Tool.GetFloat(mAutoShare.Text, 0) <= 0)
        {
            mError.Text += "سهم اتوکار صفر یا منفی است. نمیتوان صورت را  بست.";
            return;
        }

        //if (mTrip.IsMini == false && mTrip.IsSewari == false)
        if (mTrip.IsBusByCarType)
            if (mTrip.BodyInsurance <= 0)
            {
                mError.Text += "بیمه بدنه برای اتوبوس باید بزرگتر از صفر باشد. نمیتوان صورت را بست.";
                return;
            }
        if (string.IsNullOrEmpty(mTrip.TrackCode))
            try
            {
                if (!mTrip.CarID.HasValue)
                {
                    mError.Text += "برای دریافت کد رهگیری حتما باید اتوکار انتخاب شده باشد";
                }
                if (!mTrip.DriverID1.HasValue)
                {
                    mError.Text += "برای دریافت کد رهگیری حتما باید راننده اول انتخاب شده باشد";
                }
                if (ValidateDates())
                    Helper.Instance.RequestTrackCode(mTrip);
            }
            catch (Exception ex)
            {
                mError.Text += "خطا در دریافت کد رهگیری صورت وضعیت: " + ex.Message;
            }
        if (Save(true))
        {
            BindData();
            mError.Text += "بروز رساني با موفقیت انجام شد";
            if (mTrip.Closed && string.IsNullOrEmpty(mTrip.TrackCode))
                mWarnings.Text += "<br />" + "وضعیت صورت آفلاین است";
            if (mTrip.SentToInsurance != true)
            {
                try
                {
                    FillTrip();
                    if (!mTrip.CarID.HasValue)
                    {
                        mError.Text += "برای ارسال به بیمه حتما باید اتوکار انتخاب شده باشد";
                        return;
                    }
                    if (!mTrip.DriverID1.HasValue)
                    {
                        mError.Text += "برای ارسال به بیمه حتما باید راننده اول انتخاب شده باشد";
                        return;
                    }
                    if (!ValidateDates())
                        return;
                    string result = Helper.Instance.RequestInsurance(mTrip);
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(result);
                    XmlNodeList msg = xml.GetElementsByTagName("Message");

                    XmlNodeList status = xml.GetElementsByTagName("Status");
                    XmlNodeList trackCode = xml.GetElementsByTagName("TrackingCode");
                    XmlNodeList errorCode = xml.GetElementsByTagName("ErrorCode");
                    if (status.Item(0).InnerText == "OK" || errorCode.Item(0).InnerText == "0" ||
                        errorCode.Item(0).InnerText == "-10")
                    {
                        mTrip.SentToInsurance = true;
                        mTrip.InsuranceTrackCode = trackCode.Item(0).InnerText;
                    }

                    Save(false);
                    BindData();
                    mError.Text += msg.Item(0).InnerText + ". شماره رهگیری بیمه: "+mTrip.InsuranceTrackCode;
                    mError.Visible = true;
                }
                catch (Exception ex)
                {
                    mError.Text += "خطا در ارسال اطلاعات برای بیمه:" + ex.Message;
                }
                mError.Visible = true;
            }
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
        }
    }
    protected void doJustSave_Click(object sender, EventArgs e)
    {
        Calc();
        if (string.IsNullOrEmpty(mTrip.TrackCode))
            try
            {
                if (!mTrip.CarID.HasValue)
                {
                    mError.Text += "برای دریافت کد رهگیری حتما باید اتوکار انتخاب شده باشد";
                }
                if (!mTrip.DriverID1.HasValue)
                {
                    mError.Text += "برای دریافت کد رهگیری حتما باید راننده اول انتخاب شده باشد";
                }
                if (ValidateDates())
                    Helper.Instance.RequestTrackCode(mTrip);
            }
            catch (Exception ex)
            {
                mError.Text += "خطا در دریافت کد رهگیری صورت وضعیت: " + ex.Message;
            }
        if (Save(false))
        {
            mError.Text += "<br />با موفقیت ذخیره شد";
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
        }
        BindData();
        RegisterScripts();
    }
    protected void doChangeSeries_Click(object sender, EventArgs e)
    {
        ChangeSeries(true);
    }
    protected void doChangeSeries_NoCode_Click(object sender, EventArgs e)
    {
        ChangeSeries(false);
    }
    protected void doChangeSeries_Open_Click(object sender, EventArgs e)
    {
        if (mTrip.Closed)
        {
            // it should not happen
            mError.Text += "صورت وضعیت بسته است و نمیتوان سری و شماره صورت را تغییر داد";
            return;
        }
        int no = Tool.GetInt(mNo.Text, 0);
        if (no == 0)
        {
            mError.Text += "شماره صورت اشتباه است";
            return;
        }
        mTrip.No = 0;
        InValid v = new InValid();
        v.Date = mTrip.Date;
        v.Ticketless = 0;
        v.TicketlessPrice = 0;
        v.TicketlessName = "";
        v.Stamp = 0;
        v.Comission = 0;
        v.BodyInsurance = 0;
        v.ExtraCost = 0;
        v.Closed = false;
        v.Price = 0;
        v.Toll = 0;
        v.Insurance = 0;
        v.Reception = 0;
        v.TotalPrice = 0;
        v.Comission2 = 0;
        v.OtherDeficits = 0;
        v.SumDeficits = 0;
        v.AutoShare = 0;
        v.Toll2 = 0;
        v.Locked = false;
        v.Others = 0;
        //v.TripID = mTrip.ID;
        v.No = no;
        v.Series = string.Format("{0},{1},{2},{3}"
                           , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
                           , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
        //v.CarID = mTrip.CarID;
        //v.DriverID1 = mTrip.DriverID1;
        //v.DriverID2 = mTrip.DriverID2;
        //v.DriverID3 = mTrip.DriverID3;
        v.ServiceID = mTrip.ServiceID;
        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        Helper.Instance.Update();

        if (mTrip.IsMini)
            Helper.Instance.UpdateSetting("SeriesStartMini", no.ToString());
        //else
        //    Helper.Instance.UpdateSetting("SeriesStart", no.ToString());

        BindData();
        mError.Text += "شماره صورت وضعیت باطل شد - تنظیمات بروزرسانی شد";
    }
    void ChangeSeries(bool withCode)
    {
        try
        {
            Helper.Instance.ChangeSeries(withCode, mTrip);
            mError.Text += "صورت با موفقیت ابطال شد.";
            BindData();
        }
        catch (Exception ex)
        {
            mError.Text += ex.Message;
        }
        //Response.Redirect("Closeds.aspx");
    }
    protected void doChooseAuto_Click(object sender, EventArgs e)
    {
        Car d = Helper.Instance.GetCar(mCar.Value);
        if (d == null)
        {
            mError.Text = "کد اتوکار زیر از کارت خوانده شد که در لیست همکاران موجود نیست. لطفا به صورت دستی اضافه شود:<br />" + mCar.Value;
            return;
        }
        mError.Text = "کد اتوکار از کارت خوانده شد.";
        Tool.SetSelected(mCarID, d.ID);
    }
    protected void doChooseDriver1_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver1.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver1.Value);
            }
            catch
            {
                mError.Text = "کارت هوشمند راننده اول تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mError.Text = "کارت هوشمند راننده اول تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                Tool.SetSelected(mDriverID1, d.ID);
            }
            return;
        }
        mError.Text = "کد راننده اول از کارت خوانده شد.";
        Tool.SetSelected(mDriverID1, d.ID);
    }
    protected void doChooseDriver2_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver2.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver2.Value);
            }
            catch
            {
                mError.Text = "کارت هوشمند راننده دوم تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mError.Text = "کارت هوشمند راننده دوم تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                Tool.SetSelected(mDriverID2, d.ID);
            }
            return;
        }
        mError.Text = "کد راننده دوم از کارت خوانده شد.";
        Tool.SetSelected(mDriverID2, d.ID);
    }
    protected void doChooseDriver3_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver3.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver3.Value);
            }
            catch
            {
                mError.Text = "کارت هوشمند راننده سوم تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mError.Text = "کارت هوشمند راننده سوم تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                Tool.SetSelected(mDriverID3, d.ID);
            }
            return;
        }
        mError.Text = "کد راننده سوم از کارت خوانده شد.";
        Tool.SetSelected(mDriverID3, d.ID);
    }

    protected void doSaveInCard_Click(object sender, EventArgs e)
    {
        puEx.Show();
    }
    protected void doPrint_Click(object sender, EventArgs e)
    {
        if (mTrip.Closed == false)
        {
            int numReserves = Helper.Instance.GetNumReserves(mTrip.ID);
            if (numReserves > 0)
                if (Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserveClose"), true) == false)
                {
                    mError.Text += string.Format("این سرویس تعداد {0} بلیط رزروی دارد و نمیتوان آنرا بست.", numReserves);
                    return;
                }
        }
        //Calc();
        if (Save(!mTrip.Closed))
        {
            BindData();
            mError.Text += "بروز رساني با موفقیت انجام شد";
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
            return;
        }
        if (mTrip.Insurance == 0)
        {
            doPrint.Enabled = false;
            //mWarnings.Text += "مقدار بیمه سرنشین صفر است. چاپ غيرفعال شده است.";
            return;
        }

        if (mTrip.IsBusByCarType)
            if (mTrip.BodyInsurance <= 0)
            {
                doPrint.Enabled = false;
                //mWarnings.Text += "بیمه بدنه برای اتوبوس باید بزرگتر از صفر باشد. چاپ غيرفعال شده است.";
                return;
            }
        Trip trp = Helper.Instance.GetTrip(TripID);
        if (trp.CarType.Layout.NumChairs >= 25)
            Response.Redirect(string.Format("Reports/Report.aspx?id={0}&cc={3}&repname={1}&otherDests={2}", TripID.ToString(), "SuratVaziat", mTripDestsLbl.Text, Helper.Instance.GetSettingValue("CompanyCode")), true);
        else
            Response.Redirect(string.Format("Reports/Report.aspx?id={0}&cc={2}&repname={1}", TripID.ToString(), "SuratVaziatM", Helper.Instance.GetSettingValue("CompanyCode")), true);
    }
    private void UpdateSettings()
    {
        bool ok = false;
        if (mTrip.No > 0)
        {
            //int no = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStart"), 1);
            //no = Math.Max(no, mTrip.No);
            int no = mTrip.No;// +1;
            if (mTrip.IsMini)
                ok = Helper.Instance.UpdateSetting("SeriesStartMini", no.ToString());
            else
                ok = Helper.Instance.UpdateSetting("SeriesStart", no.ToString());
        }
        if (!string.IsNullOrEmpty(mTrip.Series) && mTrip.Series.Trim(',').Length > 0)
        {
            if (mTrip.IsMini)
            {
                if (mSeriesNo1.Text.Trim().Length > 0 && mSeriesNo2.Text.Trim().Length > 0
                    && mSeriesNo3.Text.Trim().Length > 0)
                {
                    ok = Helper.Instance.UpdateSetting("SeriesNoMini1", mSeriesNo1.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNoMini2", mSeriesNo2.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNoMini3", mSeriesNo3.Text);
                }
            }
            else
            {
                if (mSeriesNo1.Text.Trim().Length > 0 && mSeriesNo2.Text.Trim().Length > 0
                    && mSeriesNo3.Text.Trim().Length > 0 && mSeriesNo4.Text.Trim().Length > 0)
                {
                    ok = Helper.Instance.UpdateSetting("SeriesNo1", mSeriesNo1.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNo2", mSeriesNo2.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNo3", mSeriesNo3.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNo4", mSeriesNo4.Text);
                }
            }
        }
    }
    protected void doSaveInSetting_Click(object sender, EventArgs e)
    {
        bool ok = false;
        int no = Tool.GetInt(mNo.Text, 0);
        string series = "";
        if (mTrip.IsMini)
            series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
        else
            series = string.Format("{0},{1},{2},{3}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text, mSeriesNo4.Text);

        if (no <= 0 || string.IsNullOrEmpty(series))
        {
            mError.Text += "شماره باید بزرگتر از صفر باشد و همه بخشهای سری پر شود";
            return;
        }

        if (!string.IsNullOrEmpty(mTrip.Series) && mTrip.Series.Trim(',').Length > 0)
        {
            if (mTrip.IsMini)
            {
                if (mSeriesNo1.Text.Trim().Length > 0 && mSeriesNo2.Text.Trim().Length > 0
                    && mSeriesNo3.Text.Trim().Length > 0)
                {
                    ok = Helper.Instance.UpdateSetting("SeriesNoMini1", mSeriesNo1.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNoMini2", mSeriesNo2.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNoMini3", mSeriesNo3.Text);
                }
                else
                {
                    mError.Text += "شماره باید بزرگتر از صفر باشد و همه بخشهای سری پر شود";
                    return;
                }
            }
            else
            {
                if (mSeriesNo1.Text.Trim().Length > 0 && mSeriesNo2.Text.Trim().Length > 0
                    && mSeriesNo3.Text.Trim().Length > 0 && mSeriesNo4.Text.Trim().Length > 0)
                {
                    ok = Helper.Instance.UpdateSetting("SeriesNo1", mSeriesNo1.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNo2", mSeriesNo2.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNo3", mSeriesNo3.Text);
                    ok = Helper.Instance.UpdateSetting("SeriesNo4", mSeriesNo4.Text);
                }
                else
                {
                    mError.Text += "شماره باید بزرگتر از صفر باشد و همه بخشهای سری پر شود";
                    return;
                }
            }
        }
        if (mTrip.IsMini)
            ok = Helper.Instance.UpdateSetting("SeriesStartMini", no.ToString());
        else
            ok = Helper.Instance.UpdateSetting("SeriesStart", no.ToString());

        if (ok)
        {
            mError.Text += "بروز رساني تنظیمات با موفقیت انجام شد";
        }
        else
        {
            mError.Text += "<br />بروز رساني انجام نشد";
        }
    }
    protected void doInvalid_Click(object sender, EventArgs e)
    {
        if (mTrip.Closed)
        {
            mError.Text += "صورت وضعیت بسته است و نمیتوان سری و شماره صورت را تغییر داد";
            return;
        }
        int no = Tool.GetInt(mNo.Text, 0);
        if (no == 0)
        {
            mError.Text += "شماره صورت اشتباه است";
            return;
        }
        mTrip.No = 0;
        InValid v = new InValid();
        v.Date = mTrip.Date;
        v.Ticketless = mTrip.Ticketless;
        v.TicketlessPrice = mTrip.TicketlessPrice;
        v.TicketlessName = mTrip.TicketlessName;
        v.Stamp = mTrip.Stamp;
        v.Comission = mTrip.Comission;
        v.BodyInsurance = mTrip.BodyInsurance;
        v.ExtraCost = mTrip.ExtraCost;
        v.Closed = mTrip.Closed;
        v.Price = mTrip.Price;
        v.Toll = mTrip.Toll;
        v.Insurance = mTrip.Insurance;
        v.Reception = mTrip.Reception;
        v.TotalPrice = mTrip.TotalPrice;
        v.Comission2 = mTrip.Comission2;
        v.OtherDeficits = mTrip.OtherDeficits;
        v.SumDeficits = mTrip.SumDeficits;
        v.AutoShare = mTrip.AutoShare;
        v.Toll2 = mTrip.Toll2;
        v.Locked = mTrip.Locked;
        v.Others = mTrip.Others;
        v.TripID = mTrip.ID;
        v.No = no;
        v.Series = string.Format("{0},{1},{2},{3}"
                           , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
                           , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
        v.CarID = mTrip.CarID;
        v.DriverID1 = mTrip.DriverID1;
        v.DriverID2 = mTrip.DriverID2;
        v.DriverID3 = mTrip.DriverID3;
        v.ServiceID = mTrip.ServiceID;
        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        Helper.Instance.Update();

        if (mTrip.IsMini)
            Helper.Instance.UpdateSetting("SeriesStartMini", no.ToString());
        //else
        //    Helper.Instance.UpdateSetting("SeriesStart", no.ToString());

        BindData();
        mError.Text += "شماره صورت وضعیت باطل شد - تنظیمات بروزرسانی شد";


    }
    protected void doGetTrackingCode_Click(object sender, EventArgs e)
    {
        try
        {
            FillTrip();
            if (!mTrip.CarID.HasValue)
            {
                mError.Text += "برای دریافت کد رهگیری حتما باید اتوکار انتخاب شده باشد";
                return;
            }
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text += "برای دریافت کد رهگیری حتما باید راننده اول انتخاب شده باشد";
                return;
            }
            if (!ValidateDates())
                return;
            if (!Helper.Instance.RequestTrackCode(mTrip))
                return;
            Save(false);
            BindData();
        }
        catch (Exception ex)
        {
            mError.Text += "خطا در دریافت کد رهگیری صورت وضعیت: " + ex.Message;
            return;
        }
    }
    protected void doDelete_Click(object sender, EventArgs e)
    {
        if (mTrip.Closed || mTrip.No > 0)
        {
            // it should not happen
            mError.Text += "صورت وضعیت بسته یا دارای شماره صورت است و نمیتوان آنرا حذف کرد";
            return;
        }
        try
        {
            Helper.Instance.DB.Trips.DeleteOnSubmit(mTrip);
            if (!Helper.Instance.Update())
                throw new Exception("اشکال در حذف سرویس");
        }
        catch (Exception ex)
        {
            BindData();
            mError.Text += ex.Message;
            return;
        }
        Response.Redirect("Closeds.aspx");
    }

    protected void do_CarEdit_Click(object sender, EventArgs e)
    {
        if (mCarID.SelectedValue == "-1")
        {
            mError.Text = "ابتدا اتوکار را انتخاب کنید";
            return;
        }
        Response.Redirect("~/Staff/Cars.aspx?do=Edit&CarID=" + Tool.GetInt(mCarID.SelectedValue, Null.NullInteger) + "&TripID=" + mTrip.ID);
    }
    protected void do_Driver1Edit_Click(object sender, EventArgs e)
    {
        if (mDriverID1.SelectedValue == "-1")
        {
            mError.Text = "ابتدا راننده اول را انتخاب کنید";
            return;
        }
        Response.Redirect("~/Staff/Drivers.aspx?do=Edit&DriverID=" + Tool.GetInt(mDriverID1.SelectedValue, Null.NullInteger) + "&TripID=" + mTrip.ID);
    }
    protected void do_Driver2Edit_Click(object sender, EventArgs e)
    {
        if (mDriverID2.SelectedValue == "-1")
        {
            mError.Text = "ابتدا راننده2 را انتخاب کنید";
            return;
        }
        Response.Redirect("~/Staff/Drivers.aspx?do=Edit&DriverID=" + Tool.GetInt(mDriverID2.SelectedValue, Null.NullInteger) + "&TripID=" + mTrip.ID);
    }
    protected void do_Driver3Edit_Click(object sender, EventArgs e)
    {
        if (mDriverID3.SelectedValue == "-1")
        {
            mError.Text = "ابتدا راننده3 را انتخاب کنید";
            return;
        }
        Response.Redirect("~/Staff/Drivers.aspx?do=Edit&DriverID=" + Tool.GetInt(mDriverID3.SelectedValue, Null.NullInteger) + "&TripID=" + mTrip.ID);
    }
    bool CheckDateCar(Car car)
    {
        if (mError.Text == "")
        {
            int vDays = (int)(car.VisitDate - DateTime.Now).TotalDays;
            if (vDays < 0)
            {
                mError.Text += string.Format("معاینه فني اتوکار اعتبار ندارد<br>", vDays);
                error = true;
                //
                if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
                {
                    string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
}
window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
//window.onload=openprint;
</script>";
                    Page.RegisterStartupScript("openprint", open);
                }
            }
            int dDays = (int)(car.ThirdPersonDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mError.Text += string.Format("بيمه شخص ثالث اتوکار اعتبار ندارد<br>", dDays);
                error = true;
                //
                if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
                {
                    string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
}
window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
//window.onload=openprint;
</script>";
                    Page.RegisterStartupScript("openprint", open);
                }
            }
        }
        return false;
    }
    bool CheckDateDriver(Driver driver)
    {

        mError.Text = "";
        int hDays = (int)(driver.HourDate - DateTime.Now).TotalDays;
        if (hDays < 0)
        {
            mError.Text += string.Format("دفترچه ساعت راننده اعتبار ندارد<br>", hDays);
            error = true;
            //
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
        }
        int sDays = (int)((DateTime)driver.SmartCardDate - DateTime.Now).TotalDays;
        if (sDays < 0)
        {
            mError.Text += string.Format("کارت هوشمند راننده اعتبار ندارد<br>", sDays);
            error = true;
            //
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
        }
        int dDays = (int)(driver.DrivingDate - DateTime.Now).TotalDays;
        if (dDays < 0)
        {
            mError.Text += string.Format("کارت سلامت راننده اعتبار ندارد<br>", dDays);
            error = true;
            //
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
        }
        return false;
    }
    protected void doRequestInsurance_Click(object sender, EventArgs e)
    {
        if (mTrip == null || !mTrip.Closed)
        {
            mError.Text = "صورت وضعیت بسته نشده است. نمیتوان صورت را برای بیمه ارسال کرد";
            return;
        }
        try
        {
            FillTrip();
            if (!mTrip.CarID.HasValue)
            {
                mError.Text += "برای ارسال به بیمه حتما باید اتوکار انتخاب شده باشد";
                return;
            }
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text += "برای ارسال به بیمه حتما باید راننده اول انتخاب شده باشد";
                return;
            }
            if (!ValidateDates())
                return;
            string result = Helper.Instance.RequestInsurance(mTrip);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);
            XmlNodeList msg = xml.GetElementsByTagName("Message");

            XmlNodeList status = xml.GetElementsByTagName("Status");
            XmlNodeList trackCode = xml.GetElementsByTagName("TrackingCode");
            XmlNodeList errorCode = xml.GetElementsByTagName("ErrorCode");
            if (status.Item(0).InnerText == "OK" || errorCode.Item(0).InnerText == "0" || errorCode.Item(0).InnerText == "-10")
            {
                mTrip.SentToInsurance = true;
                mTrip.InsuranceTrackCode = trackCode.Item(0).InnerText;
            }

            Save(false);
            BindData();
            mError.Text = msg.Item(0).InnerText + "<br />";
            mError.Visible = true;
        }
        catch (Exception ex)
        {
            mError.Text = "خطا در ارسال اطلاعات برای بیمه:" + ex.Message;
        }
        mError.Visible = true;
    }

    int CalcTotalReception()
    {
        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        int MinForReception = Tool.GetInt(Helper.Instance.GetSettingValue("MinForReception"), 0);

        int TotalReception = 0;
        bool extra = false;

        for (int i = 0; i < tickets.Count; i++)
        {
            int numChairs = tickets[i].Chairs.Trim(';').Split(';').Length;
            if (tickets[i].Price - tickets[i].Discount >= MinForReception)
                TotalReception = TotalReception + (mTrip.Reception * numChairs);
            else
                extra = true;
        }
        if (extra)
            TotalReception = TotalReception + mTrip.Reception;
        return TotalReception;
    }

    protected void doSendToTechnical_Click(object sender, EventArgs e)
    {
        if (!mTrip.CarID.HasValue)
        {
            mError.Text += "برای ارسال به مدیر فنی حتما باید اتوکار انتخاب شده باشد";
            return;
        }
        if (!mTrip.DriverID1.HasValue)
        {
            mError.Text += "برای ارسال به بیمه حتما باید راننده اول انتخاب شده باشد";
            return;
        }
        string cntStr =
            System.Configuration.ConfigurationManager.ConnectionStrings["TechnicalConnectionString"].ConnectionString;
        CompanyTrips companyTrips = new CompanyTrips();
        int companyId = Helper.Instance.GetCompanyId(Helper.Instance.GetSettingValue("CompanyCode"), cntStr);
        if (companyId > 0)
        {
            string plate = Helper.Instance.GetCar(mTrip.CarID).Plate;
            companyTrips.CompanyID = companyId;
            companyTrips.TripID = mTrip.ID;
            companyTrips.CarNo = plate;
            companyTrips.UserID = -1;
            companyTrips.Driver = mTrip.Driver.Name + ' ' + mTrip.Driver.Surname;
            if (mTrip.IsMini)
                companyTrips.TripSeries = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
            else
                companyTrips.TripSeries = string.Format("{0},{1},{2},{3}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text, mSeriesNo4.Text);
            companyTrips.TripNo = mTrip.No;
            companyTrips.Status = (int)Status.NoCheck;
            if (Helper.Instance.AddCompanyTrips(companyTrips))
            {
                CompanyTripImpl companyTripImpl = new CompanyTripImpl();
                companyTripImpl.CompanyID = companyId;
                companyTripImpl.TripID = mTrip.ID;
                companyTripImpl.CarNo = plate;
                companyTripImpl.UserID = -1;
                companyTripImpl.Status = (int)Status.NoCheck;
                companyTripImpl.Date = DateTime.Now;
                companyTripImpl.Driver = mTrip.Driver.Name + ' ' + mTrip.Driver.Surname;
                if (mTrip.IsMini)
                    companyTripImpl.TripSeries = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
                else
                    companyTripImpl.TripSeries = string.Format("{0},{1},{2},{3}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text, mSeriesNo4.Text);
                companyTripImpl.TripNo = Tool.GetInt(mNo.Text,0);
                Helper.Instance.AddCompanyTripImpl(companyTripImpl);

                mTrip.TripStatus = (int)TripStatus.Pending;
                mTrip.CompanyUser = SiteSettings.User.FullName;
                if (Helper.Instance.Update())
                {
                    mError.Text = "صورت وضعیت برای مدیر فنی ارسال گردید.";
                    mError.Visible = true;
                    doSendToTechnical.Enabled = false;
                    doClose.Enabled = false;
                    doPrint.Enabled = false;
                }
            }
        }
        else
        {
            mError.Text = "این تعاونی در سیستم مدیر فنی تعریف نشده است";
            mError.Visible = true;
        }
    }
    protected void doCheckFani_Click(object sender, EventArgs e)
    {
        if (!mTrip.CarID.HasValue)
        {
            mError.Text += "برای استعلام فنی حتما باید اتوکار انتخاب شده باشد";
            return;
        }
        string cntStr =
            System.Configuration.ConfigurationManager.ConnectionStrings["TechnicalConnectionString"].ConnectionString;
        Car car = Helper.Instance.GetCar(mTrip.CarID);
        CarTechnical carTechnical = Helper.Instance.GetCarTechnical(cntStr, car.SmartCard, car.Plate,
            Tool.ToPersianDate(DateTime.Now, DateTime.Now));
        if (carTechnical!=null)
        {
            mTrip.TechnicalUser = carTechnical.UserName;
            mTrip.CompanyUser = SiteSettings.User.FullName;
            mTrip.TripStatus = (int) carTechnical.TechnicalStatus;
            mTrip.FaniDetail = carTechnical.FaniDetail;
            mTrip.Comments = carTechnical.Comment;
            if (Helper.Instance.Update())
            {
                if (mTrip.TripStatus == (int)TripStatus.Confirmed)
                {
                    mError.Text = "اتوکار امروز توسط مدیر فنی تأیید شده است";
                    mError.Visible = true;
                }
                if (mTrip.TripStatus == (int)TripStatus.Rejected)
                {
                    mError.Text = "اتوکار امروز توسط مدیر فنی رد شده است";
                    mError.Visible = true;
                }
                BindData();
            }
        }
        else
        {
            mError.Text = "اتوکار امروز توسط مدیر فنی بررسی نشده است";
            mError.Visible = true;
        }
    }
}
