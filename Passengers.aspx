﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Passengers.aspx.cs" Inherits="Passengers" Title="ليست مسافران" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0" width="90%">
        <tr>
            <td class="Title" colspan="6">
                ليست مسافران مربوط به سرويس
                <asp:Label ID="mTripTitle" runat="server" />&nbsp;&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx'
                    runat="server" ID="doReturn">برگشت</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:PlaceHolder runat="server" ID="mLayout" />
            </td>
        </tr>
    </table>
</asp:Content>
