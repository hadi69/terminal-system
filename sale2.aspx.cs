﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;

public partial class sale2 : System.Web.UI.Page
{
    // ++++++++++++++++++
    Trip mTrip = null;
    bool error = false;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        (this.Master as MasterPage).BindHotKeys = false;
        mError.Text = "";
        mWarnings.Text = "";
        doTechnical.Visible = false;
        if (!IsPostBack)
        {
            BindInitData();
            this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
            if (this.TripID == Null.NullInteger)
            {
                int pathID = Tool.GetInt(Request.QueryString["pathid"], Null.NullInteger);
                int carTypeID = Tool.GetInt(Request.QueryString["cartypeid"], Null.NullInteger);
                if (pathID != Null.NullInteger && carTypeID != Null.NullInteger)
                {
                    if (!CreateTrip(pathID, carTypeID))
                        Response.Redirect("Trips.aspx", true);
                }
            }
            else
                mTrip = Helper.Instance.GetTrip(TripID);
        }
        else
            mTrip = Helper.Instance.GetTrip(TripID);
        if (!IsPostBack)
        {

            BindData();
            if (mTrip != null)
            {
                mPrice.Text = mTrip.Service.Price.ToString(); //GetPrice(mTrip.Tickets.Count > 0).ToString();
                doClose.NavigateUrl = "Close.aspx?tripid=" + this.TripID;
                doRefresh.NavigateUrl = "sale2.aspx?tripid=" + this.TripID;
            }
        }
        else if (mTrip != null)
        {
            Car car = Helper.Instance.GetCar(mTrip.CarID);
            List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
            CheckDates(car, tickets);
        }

        if (!Tool.ButtonIsClicked(Request, doSetPath) && !Tool.ButtonIsClicked(Request, doSetDriver)
            && !Tool.ButtonIsClicked(Request, doSetCar))
        {
            if (mTrip != null)
            {
                mName.Enabled = true;
                JsTools.SetFocus(mName);
            }
            else
                JsTools.SetFocus(mPathCode);
        }

        //JsTools.BindHotKeysF(Page, clientIDs, keyCodes);
        BindHotKeys();
    }
    void BindHotKeys()
    {
        List<string> clientIDs = new List<string>();
        List<int> keyCodes = new List<int>();
        clientIDs.Add(doNew.ClientID);
        keyCodes.Add(45);
        clientIDs.Add(doSale2.ClientID);
        keyCodes.Add(113);
        clientIDs.Add(doClose.ClientID);
        keyCodes.Add(115);
        clientIDs.Add(doAutoFill.ClientID);
        keyCodes.Add(123);
        if (mTrip != null && mTrip.Tickets.Count > 0)
        {
            clientIDs.Add(doPrint.ClientID);
            keyCodes.Add(120);
        }

        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function keyPressHand(event) { \n");
        builder.Append("    var e = event;\n");
        builder.Append("    if (!window.getSelection) e = window.event;\n");
        builder.Append("    key = e.keyCode;\n");
        builder.Append("    if (key == 119)\n");
        builder.Append("        window.location = 'QQTrips.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        //builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.click();\n");
            builder.AppendFormat("window.event.keyCode=0;\n");
            builder.AppendFormat("break;\n");
        }
        // ----------------------------
        builder.AppendFormat("case {0}:\n", 118);
        builder.AppendFormat("var b = $get('{0}');\n", mName.ClientID);
        builder.AppendFormat("b.focus();\n");
        builder.AppendFormat("window.event.keyCode=0;\n");
        builder.AppendFormat("break;\n");

        builder.Append("    }\n");
        builder.Append("return;\n");
        builder.Append("}\n");
        builder.Append("document.onkeydown=keyPressHand;\n");
        builder.Append("    </script>\n");
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "HotKeyScript", builder.ToString());

        #region Name.KeyDown
        mName.Attributes.Add("onkeydown", "DoMoveToNext();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function DoMoveToNext() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", mNum.ClientID);
        builder.Append("b.focus();b.select();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("DoMoveToNextScript", builder.ToString());
        #endregion

        #region mNum.KeyDown
        mNum.Attributes.Add("onkeydown", "DoMoveToNextNum();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function DoMoveToNextNum() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", doSale2.ClientID);
        builder.Append("b.click();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("DoMoveToNextNumScript", builder.ToString());
        #endregion

        #region doSale.KeyDown
        //        doSale2.Attributes.Add("onclick", "DoDisableSale2();");
        //        builder = new StringBuilder();
        //        builder.Append("<script language='javascript' type='text/javascript'>\n");
        //        builder.Append("<!--\n");
        //        builder.Append("function DoDisableSale2() {\n");
        //        builder.AppendFormat("var b = $get('{0}');\n", doSale2.ClientID);
        //        builder.Append("b.click() ;\n");
        ////        builder.Append("b.disabled= 'disabled' ;\n");
        //        builder.Append("}\n");
        //        builder.Append("-->\n");
        //        builder.Append("    </script>\n");
        //        Page.RegisterClientScriptBlock("DoDisableSale2Script", builder.ToString());
        #endregion

        #region mHour.KeyDown
        mHour.Attributes.Add("onkeydown", "mHourDoMoveToNext();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function mHourDoMoveToNext() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", mMinute.ClientID);
        builder.Append("b.focus();b.select();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("mHourDoMoveToNextScript", builder.ToString());
        #endregion

        #region mMinute.KeyDown
        mMinute.Attributes.Add("onkeydown", "mMinuteDoMoveToNext();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function mMinuteDoMoveToNext() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", mDate.ClientID);
        builder.Append("b.focus();b.select();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("mMinuteDoMoveToNextScript", builder.ToString());
        #endregion

        #region mDate.KeyDown
        mDate.Attributes.Add("onkeydown", "mDateDoMoveToNext();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function mDateDoMoveToNext() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", doSave.ClientID);
        builder.Append("b.focus();b.select();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("mDateDoMoveToNextScript", builder.ToString());
        #endregion
        #region Keys for Codes
        // ------------------------------
        // -------------------------------
        mPathCode.Attributes.Add("onkeydown", "DoPostBackPath();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function DoPostBackPath() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        //builder.Append("    alert(key);\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", doSetPath.ClientID);
        builder.Append("b.click();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("DoPostBackScriptPath", builder.ToString());

        // ------------------------------
        // -------------------------------
        mCarCode.Attributes.Add("onkeydown", "DoPostBackCar();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function DoPostBackCar() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", doSetCar.ClientID);
        builder.Append("b.click();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("DoPostBackScriptCar", builder.ToString());

        // ------------------------------
        // -------------------------------
        mDriverCode.Attributes.Add("onkeydown", "DoPostBackDriver();");
        builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function DoPostBackDriver() {\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    if (key == 13) {\n");
        builder.Append("     event.returnValue=false;\n");
        builder.Append("     event.cancel = true;\n");
        builder.AppendFormat("var b = $get('{0}');\n", doSetDriver.ClientID);
        builder.Append("b.click();\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        Page.RegisterClientScriptBlock("DoPostBackScriptDriver", builder.ToString());
        #endregion

        #region Auto
        string reg = @"
        <script language='javascript'>
function CheckAuto(){
    document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRB.OmnikeyRB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0;
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	 //کارت هوشمند 

    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseAuto.ClientID)
       .Replace("mSmartCard", mCar.ClientID);
        RegisterStartupScript("ReadClickScriptAuto", reg);
        doChooseAuto.Attributes.Add("OnClick", "CheckAuto()");
        #endregion

        #region Driver 1
        reg = @"
        <script language='javascript'>
function Check1(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0;
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseDriver.ClientID)
       .Replace("mSmartCard", mDriver.ClientID);
        RegisterStartupScript("ReadClickScript1", reg);
        doChooseDriver.Attributes.Add("OnClick", "Check1()");
        #endregion

        #region Write In Card
        if (mTrip != null)
        {
            reg = @"
        <script language='javascript1.2'>
var cardpath='c:\\SmartDb.udb';
function cardsabt() {
	alert('ثبت اطلاعات صورت در کارت\n\n لطفا کارت ماشین به شماره CARNO  را داخل کارت خوان قرار دهید');
	try{
	  var rd = new ActiveXObject('SmartRLWB.OmnikeyRLWB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById(button).disabled = false;	  
	  return 0;
	}
  var flag = rd.SCEstablish;
  flag = rd.SCConnect;
  if(!flag) alert(' اشکال در کارت');
  else {
	  flag = rd.SCSetPath(cardpath);
	  if(!flag) alert(' اشکال در مسیر فایل رمزگذاری شرکت');	  

	  else {
			var surat=Number('SURET');
			var type=Number('1');
			var date=Number('DATE'); //13880808
			var time=Number('TIME'); //2200
			var company=Number('COMPANYCODE');	//Code Sherket=73023
			var source=Number('SOURCE');//73310000
			var distin=Number('DEST');//11320000
			var drvcard1=Number('DRIVERCARD1');//1993116
			var drvcard2=Number('DRIVERCARD2');//2005171
			var pass=Number('PASSENGERS');//13
			var price=Number('PRICE');//730000
			var cardpsw='PASSWORD';		//Pass=xuw240
			var cardid=Number('USERID');		//UserNo=8
			var pswlen=cardpsw.length;						
			flag=rd.SCCheckUserIdPassword(cardid, cardpsw , pswlen) ;
			if(!flag) alert(' اشکال در کد کاربری و رمزعبور کارت ');	
			else {
				flag = rd.SCWriteSooratVazeat(surat, type, date, time, company, source, distin, drvcard1, drvcard2, pass, price, cardid, cardpsw, pswlen);
				if(!flag) 
                    alert(' اشکال در فایل ترابری');
                else
                    alert('صورت وضعیت با موفقیت ثبت شد.');
			}	
	}		 
  }  
 rd.SCDisconnect;
 rd.SCRelease;
}
function cardread() {
	try{
	  var rd = new ActiveXObject('CardReadInfo.CardRead');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById(button).disabled = false;	  
	  return 0;
	}
  var str = rd.ReadCardInfo(cardpath);
  if(str)  {
		str=str.split('@');
		var surat=str[0];
		var type=str[1];
		var date=str[2];
		var time=str[3];
		var company=str[4];
		var source=str[5];
		var distin=str[6];
		var drvcard1=str[7];
		var drvcard2=str[8];
		var pass=str[9];
		var price=str[10];
		date=String(date);
		date=date.substr(6,2)+'/'+date.substr(4,2)+'/'+date.substr(0,4);
		time=String(time);
		time=time.substr(0,2)+':'+time.substr(2,2);
		document.getElementById('surat').value=surat;
		document.getElementById('date').value=date;
		document.getElementById('time').value=time;
		document.getElementById('company').value=company;
		document.getElementById('source').value=source;
		document.getElementById('distin').value=distin;
		document.getElementById('pass').value=pass;
		document.getElementById('price').value=price;
	}		 
}
</script>
    ".Replace("button", doSaveInCard.ClientID)
         .Replace("CARNO", Helper.Instance.GetCarPlate(mTrip.CarID))
         .Replace("USERID", Helper.Instance.GetSettingValue("UserID"))
         .Replace("COMPANYCODE", Helper.Instance.GetSettingValue("CompanyCode"))
         .Replace("PASSWORD", Helper.Instance.GetSettingValue("Password"))
         .Replace("SURET", mTrip.No.ToString())
         .Replace("DATE", Tool.ToPersianDate(mTrip.Date, DateTime.Now).Replace("/", ""))
         .Replace("TIME", mTrip.DepartureTime2.Replace(":", ""))
         .Replace("SOURCE", Helper.Instance.GetCityCode(mTrip.Service.Path.SrcCityID))
         .Replace("DEST", Helper.Instance.GetCityCode(mTrip.Service.Path.DestCityID))
         .Replace("DRIVERCARD1", Helper.Instance.GetDriverSmartCard(mTrip.DriverID1))
         .Replace("DRIVERCARD2", Helper.Instance.GetDriverSmartCard(mTrip.DriverID2))
         .Replace("PASSENGERS", Helper.Instance.GetNumTickets(mTrip.ID).ToString())
         .Replace("PRICE", Helper.Instance.GetPrice(mTrip.ID).ToString());//987722

            //    var source=Number('');//73310000
            //    var distin=Number('');//11320000
            //    var drvcard1=Number('DRIVERCARD1');//1993116
            //    var drvcard2=Number('DRIVERCARD2');//2005171
            //    var pass=Number('');//13
            //    var price=Number('');//730000
            RegisterStartupScript("WriteClickScript", reg);
            doSaveInCard.Attributes.Add("OnClick", "cardsabt();cardread();");
            doReadFromCard.Attributes.Add("OnClick", "cardread();");
        }
        #endregion
    }
    private void BindInitData()
    {
        mDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        mHour.Text = DateTime.Now.Hour.ToString().PadLeft(2, '0');
        mMinute.Text = DateTime.Now.Minute.ToString().PadLeft(2, '0');
        mShowDate.Checked = false;
        mShowDate_CheckedChanged(null, null);
        try
        {
            List<Fund> all = Helper.Instance.GetFunds();
            if (SiteSettings.User != null && SiteSettings.User.IsAdmin != true)
            {
                all = new List<Fund>();
                Fund f = Helper.Instance.GetFund(SiteSettings.User.FundID);
                if (f != null)
                    all.Add(f);
            }
            mFundID.DataValueField = "ID";
            mFundID.DataTextField = "Title";
            mFundID.DataSource = all;
            mFundID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        try
        {
            List<Branch> all = Helper.Instance.GetBranches();
            if (SiteSettings.User != null && SiteSettings.User.IsAdmin != true)
            {
                all = new List<Branch>();
                Branch f = SiteSettings.User.BranchID.HasValue ? Helper.Instance.GetBranch(SiteSettings.User.BranchID.Value) : null;
                if (f != null)
                    all.Add(f);
            }
            mBranchID.DataValueField = "ID";
            mBranchID.DataTextField = "Title";
            mBranchID.DataSource = all;
            mBranchID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }

        try
        {
            List<PassengerList> all = Helper.Instance.GetPassengerLists();
            mPassengerList.DataValueField = "ID";
            mPassengerList.DataTextField = "Title";
            mPassengerList.DataSource = all;
            mPassengerList.DataBind();
            mPassengerList.Items.Insert(0, "");
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
    }

    int GetSumDiscount()
    {
        try
        {
            var v = from t in Helper.Instance.DB.Tickets
                    where t.TripID == TripID
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    && t.PaidBack == 0
                    select t.Discount * t.NumChairs;
            int? res = v.Sum();
            if (res.HasValue)
                return res.Value;
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }

    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
    protected void doSale_Click(object sender, EventArgs e)
    {
        if (mTrip.IsSewari && mTrip.Tickets.Count == 0 && Tool.GetInt(mNum.Text, 0) > 1)
        {
            int num = Tool.GetInt(mNum.Text, 0);
            string name = mName.Text;
            mNum.Text = "1";
            SaleTickts(false);
            mName.Text = name;
            mNum.Text = (num - 1).ToString();
            SaleTickts(true);
        }
        else
            SaleTickts(true);
    }
    protected void doSale2_Click(object sender, EventArgs e)
    {
        if (mTrip.IsSewari && mTrip.Tickets.Count == 0 && Tool.GetInt(mNum.Text, 0) > 1)
        {
            int num = Tool.GetInt(mNum.Text, 0);
            string name = mName.Text;
            mNum.Text = "1";
            mPrice.Text = mTrip.Service.Price.ToString(); //GetPrice(false).ToString();
            SaleTickts(false);
            mPrice.Text = mTrip.Service.Price.ToString(); // GetPrice(true).ToString();
            mName.Text = name;
            mNum.Text = (num - 1).ToString();
            SaleTickts(false);
        }
        else
            SaleTickts(false);
    }

    protected void SaleTickts(bool print)
    {
        if (mName.Text.Trim().Length == 0)
        {
            mError.Text = "نام مسافر وارد نشده است";
            return;
        }
        Ticket t = new Ticket();
        t.TripID = this.TripID;
        t.SaleUserID = SiteSettings.UserID;
        t.SaleDate = DateTime.Now;

        t.Fullname = mName.Text;
        t.Man = true;
        t.No = Helper.Instance.GetNewTicketNo(mTrip.ID);
        t.NumChairs = Tool.GetInt(mNum.Text, Null.NullInteger);
        int numEmpty = Tool.GetInt(mNumEmpty.Text, 0);
        if (t.NumChairs.Value > numEmpty)
            t.NumChairs = numEmpty;
        if (t.NumChairs.Value <= 0)
        {
            mError.Text = "تعداد صندلی باید بیشتر از صفر باشد";
            return;
        }

        if (t.NumChairs.Value > Tool.GetInt(mNumEmpty.Text, 0))
        {
            mError.Text = string.Format("تعداد صندلی باید کمتر از {0} باشد", mNumEmpty.Text);
            return;
        }
        t.Chairs = "m0";
        for (int i = 0; i < t.NumChairs.Value - 1; i++)
            t.Chairs += ";m0";


        t.CityID = mTrip.Service.Path.DestCityID;
        t.SaleType = (int)SaleTypes.Sale;
        t.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        if (t.FundID == Null.NullInteger)
        {
            mError.Text = "صندوق انتخاب نشده است";
            return;
        }
        t.Expire = null;
        if (mBranchID.SelectedIndex >= 0)
        {
            t.SaleType = (int)SaleTypes.OfficeSale;
            t.BranchID = Tool.GetInt(mBranchID.SelectedValue, Null.NullInteger);
        }
        if (t.BranchID == null || t.BranchID.Value <= 0)
        {
            mError.Text = "دفتر انتخاب نشده است";
            return;
        }
        t.Tel = "";
        t.Price = Tool.GetInt(mPrice.Text, 0);// mTrip.Price;
        t.Discount = 0;// Tool.GetInt(mDiscount.Text, 0);

        bool done = false;
        Helper.Instance.DB.Tickets.InsertOnSubmit(t);
        if (!Helper.Instance.Update())
            mError.Text = "خطا در فروش " + Helper.Instance.LastException.Message;
        else
        {
            done = true;
            mPrice.Text = mTrip.Service.Price.ToString(); //GetPrice(true).ToString();
            mError.Text = "فروش با موفقیت انجام شد";
            mError.Text = string.Format("بلیط با شماره {0} و جمع {1} فروخته شد. <a href='Reports/MiniTicket.aspx?id={2}' target=_blank>چاپ</>", t.No, (t.Price - t.Discount) * t.NumChairs, t.ID);
            mName.Text = /*mCustomerNo.Text = mDiscount.Text = mTel.Text =*/ "";
            if (print)
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=300,height=550,width=430');
}
window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=50,height=550,width=900');
//window.onload=openprint;
</script>".Replace("IDID", t.ID.ToString());
                Page.RegisterStartupScript("openprint", open);
            }
        }
        UpdateAllChairs();
        BindData();
        if (done)
            mPrice.Text = mTrip.Service.Price.ToString(); //GetPrice(true).ToString();
        if (Tool.GetBool(Helper.Instance.GetSettingValue("MiniResetNum"), false) == true)
        {
            mNum.Text = "1";
        }
    }
    void UpdateAllChairs()
    {
        List<Ticket> tickets = Helper.Instance.GetTickets(TripID);
        if (tickets == null || tickets.Count == 0)
            return;

        int index = 1;

        for (int i = 0; i < tickets.Count; i++)
            if (tickets[i].NumChairs.HasValue && tickets[i].NumChairs.Value > 0)
            {
                tickets[i].Chairs = ";m" + index++ + ";";
                for (int x = 0; x < tickets[i].NumChairs.Value - 1; x++)
                    tickets[i].Chairs = string.Format("{0}m{1};", tickets[i].Chairs, index++);
            }
        Helper.Instance.Update();

    }
    //++++++++++++++++++++
    protected void doReturn_Click(object sender, EventArgs e)
    {
        if (Tool.ButtonIsClicked(Request, doSetPath) || Tool.ButtonIsClicked(Request, doSetDriver) || Tool.ButtonIsClicked(Request, doSetCar))
            return;
        if (!Tool.ButtonIsClicked(Request, doReturn))
            return;
        if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoDeleteMini"), false) == true)
            if (mTrip != null && mTrip.Tickets.Count == 0)
            {
                Helper.Instance.DeleteTrip(mTrip.ID);
                Helper.Instance.DeleteService(mTrip.ServiceID);
            }
        //Helper.Instance.AutoDeleteTrips();
        Response.Redirect("~/Trips.aspx");
    }
    private void BindData()
    {
        // mWarnings.Text = "";
        doGetTrackingCode.Enabled = mTrip != null && string.IsNullOrEmpty(mTrip.TrackCode);
        doSaveClose.Enabled = mTrip != null;

        if (mTrip == null)
        {
            if (!Tool.GetBool(Helper.Instance.GetSettingValue("Technical"), false))
            {
                doSendToTechnical.Visible = false;
                doCheckFani.Visible = false;
            }
            doAutoFill.Enabled = doPrint.Enabled = doPrint2.Enabled = doSale.Enabled = doSale2.Enabled =doCheckFani.Enabled= false;
            doSendToTechnical.Enabled = false;
            mPathCode.Enabled = true;
            mName.Enabled = mNum.Enabled = false;
            mDate.Text = Tool.ToPersianDate(DateTime.Now, "");
            mHour.Text = DateTime.Now.Hour.ToString().PadLeft(2, '0');
            mMinute.Text = DateTime.Now.Minute.ToString().PadLeft(2, '0');
            mGrid.DataSource = null;
            mGrid.DataBind();

            string series1 = string.Format("{0},{1},{2}"
                       , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                       , Helper.Instance.GetSettingValue("SeriesNoMini3"));
            mNo.Text = Helper.Instance.GetNextSeriesStart(series1, true).ToString();
            mSeriesNo1.Text = Helper.Instance.GetSettingValue("SeriesNoMini1");
            mSeriesNo2.Text = Helper.Instance.GetSettingValue("SeriesNoMini2");
            mSeriesNo3.Text = Helper.Instance.GetSettingValue("SeriesNoMini3");
            //
            int seFrom = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartMiniFrom"), -1);
            int seTo = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartMiniTo"), -1);
            if (seFrom == -1 || seTo == -1)
            {
                mWarnings.Text += "در تنظیمات بازه سری صورت مشخص نشده است <br />";
            }
            int seCur = Tool.GetInt(mNo.Text, 0);
            if (seCur > seTo)
            {
                mNo.BackColor = System.Drawing.Color.FromArgb(255, 10, 10);
                mWarnings.Text += "سری صورت خارج از  بازه سری صورت در تنظیمات است <br />";
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Manager/Settings.aspx');
}
window.open('Manager/Settings.aspx');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
            return;
        }
        mPathCode.Enabled = false;
        mName.Enabled = mNum.Enabled = true;
        doAutoFill.Enabled = doPrint.Enabled = doPrint2.Enabled = doSale.Enabled = doSale2.Enabled =doCheckFani.Enabled= true;
        Car car = Helper.Instance.GetCar(mTrip.CarID);
        mCarCode.Text = car == null ? "" : car.Code;
        mPathCode.Text = mTrip.Service.Path.Code;
        mDriverCode.Text = mTrip.Driver == null ? "" : mTrip.Driver.Code;
        mCarTitle.Text = car == null ? "" : car.Title;
        mPathTitle.Text = mTrip.Service.Path.Title;
        mDriverTitle.Text = mTrip.Driver == null ? "" : mTrip.Driver.Title;

        int total = mTrip.CarType.Layout.NumChairs.HasValue ? mTrip.CarType.Layout.NumChairs.Value : 0;
        int full = Helper.Instance.GetNumTickets(mTrip.ID);

        mAll.Text = total.ToString();
        mFull.Text = full.ToString();
        mNumEmpty.Text = (total - full).ToString();
        //mSumDiscount.Text = GetSumDiscount().ToString();

        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        mGrid.DataSource = tickets;
        mGrid.DataBind();

        doPrint.Enabled = doPrint2.Enabled = tickets != null && tickets.Count > 0;
        CheckDates(car, tickets);
        mDate.Text = Tool.ToPersianDate(mTrip.Service.Date, "");
        try
        {
            mHour.Text = mTrip.DepartureTime2.Split(':')[0];
        }
        catch { }
        try
        {
            mMinute.Text = mTrip.DepartureTime2.Split(':')[1];
        }
        catch { }
        mTripTitle.Text = string.Format("{0} ({1}) ({2} - {3}) شماره: {4}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDateRtl(mTrip.Date, ""), mTrip.DepartureTime2, mTrip.ID);


        string series = mTrip.Series;
        if (string.IsNullOrEmpty(mTrip.Series) || mTrip.Series.Trim(',').Length == 0)
        {
            series = string.Format("{0},{1},{2}"
                   , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                   , Helper.Instance.GetSettingValue("SeriesNoMini3"));
        }

        if (mTrip.No < 1)
            mNo.Text = Helper.Instance.GetNextSeriesStart(series, mTrip.IsMini).ToString();
        else
            mNo.Text = mTrip.No.ToString();

        try
        {
            string[] vals = series.Split(',');
            mSeriesNo1.Text = vals[0];
            mSeriesNo2.Text = vals[1];
            mSeriesNo3.Text = vals[2];
        }
        catch { }
        bool enableClose = true;
        int sFrom = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartMiniFrom"), -1);
        int sTo = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartMiniTo"), -1);
        if (sFrom == -1 || sTo == -1)
        {
            enableClose = false;
            mWarnings.Text += "در تنظیمات بازه سری صورت مشخص نشده است <br />";
        }
        int sCur = Tool.GetInt(mNo.Text, 0);
        if (sCur > sTo)
        {
            mNo.BackColor = System.Drawing.Color.FromArgb(255, 10, 10);
            enableClose = false;
            mWarnings.Text += "سری صورت خارج از  بازه سری صورت در تنظیمات است <br />";
            string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Manager/Settings.aspx');
}
window.open('Manager/Settings.aspx');
//window.onload=openprint;
</script>";
            Page.RegisterStartupScript("openprint", open);
        }
        else
            mNo.BackColor = System.Drawing.Color.White;

        mPrice.Text = mTrip.Service.Price.ToString();// GetPrice(mTrip.Tickets.Count > 0).ToString();

        if (enableClose)
        {
            doPrint.Enabled = doPrint2.Enabled = enableClose;
        }
        else
        {
            if (!mTrip.Closed)
                doClose.Enabled = false;
            doPrint.Enabled = doPrint2.Enabled = enableClose;
            mWarnings.Text += "-----------------> بستن و چاپ غيرفعال شده است";
        }

        mCloseDate.Text = mTrip.CloseDate.HasValue ? Tool.ToPersianDate(mTrip.CloseDate.Value, DateTime.Now) : "";
        doPrint.Enabled = doPrint2.Enabled = doPrint.Enabled;

        mTrackCode.Text = mTrip.TrackCode;
        mTrackDate.Text = mTrip.TrackDate.HasValue ? Tool.ToPersianDate(mTrip.TrackDate.Value, "") : "";
        if (!Tool.GetBool(Helper.Instance.GetSettingValue("Technical"), false))
        {
            doTechnical.Visible = true;
        }
        if (mTrip.TripStatus == (int)TripStatus.None && !mTrip.Closed && Tool.GetBool(Helper.Instance.GetSettingValue("Technical"), false))
        {
            doClose.Enabled = false;
            doPrint.Enabled = false;
            doPrint2.Enabled = false;
            doSaveClose.Enabled = false;
            doSendToTechnical.Enabled = true;
        }
        if (mTrip.TripStatus != (int)TripStatus.None && mTrip.TripStatus != (int)TripStatus.Confirmed && mTrip.TripStatus != null)
        {
            doClose.Enabled = false;
            doPrint.Enabled = false;
            doSaveClose.Enabled = false;
            doGetTrackingCode.Enabled = false;
            doPrint2.Enabled = false;
            if (mTrip.TripStatus == (int)TripStatus.Pending)
            {
                mError.Text = "صورت وضعیت توسط مدیر فنی در حال بررسی است. بستن و چاپ غیر فعال است. " + mTrip.Comments;
            }
            if (mTrip.TripStatus == (int)TripStatus.Rejected)
            {
                mError.Text = "صورت وضعیت توسط مدیر فنی رد شده است. بستن و چاپ غیر  فعال است. " + mTrip.Comments;
            }
            mError.Visible = true;
        }
        if (mTrip.TripStatus==(int)TripStatus.Confirmed)
        {
            doTechnical.NavigateUrl = "Technical.aspx?tripid=" + mTrip.ID;
            doTechnical.Visible = true;
            doSendToTechnical.Enabled = false;
            doClose.Enabled = true;
            doPrint.Enabled = true;
            doPrint2.Enabled = true;
            doSaveClose.Enabled = true;
        }
        if (!Tool.GetBool(Helper.Instance.GetSettingValue("Technical"), false))
        {
            doSendToTechnical.Visible = false;
            doCheckFani.Visible = false;
        }
    }

    protected void mGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (e.RowIndex < 0)
            return;

        int id = Tool.GetInt(mGrid.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        TextBox name = Tool.FindControl(mGrid.Rows[e.RowIndex].Cells[1].Controls, typeof(TextBox)) as TextBox;
        TextBox num = Tool.FindControl(mGrid.Rows[e.RowIndex].Cells[2].Controls, typeof(TextBox)) as TextBox;
        TextBox price = Tool.FindControl(mGrid.Rows[e.RowIndex].Cells[3].Controls, typeof(TextBox)) as TextBox;
        if (Null.NullInteger == id || name == null || num == null || price == null)
            return;

        Ticket t = Helper.Instance.GetTicket(id);
        if (t == null)
        {
            mGrid.EditIndex = -1;
            BindData();
            return;
        }
        t.Fullname = name.Text.Normalize();
        int numChairs = Tool.GetInt(num.Text, 0);
        if (numChairs == Null.NullInteger)
        {
            mError.Text = "تعداد صندلی باید بیشتر از صفر باشد";
            return;
        }
        if (numChairs > (Tool.GetInt(mNumEmpty.Text, 0) + t.NumChairs.Value))
        {
            mError.Text = string.Format("تعداد صندلی باید کمتر از {0} باشد", (1 + Tool.GetInt(mNumEmpty.Text, 0) + t.NumChairs.Value));
            return;
        }
        int pr = Tool.GetInt(price.Text, Null.NullInteger);
        if (pr == Null.NullInteger)
        {
            mError.Text = "مقدار قیمت اشتباه است";
            return;
        }
        t.NumChairs = numChairs;
        t.Chairs = "m0";
        t.Price = pr;
        for (int i = 0; i < t.NumChairs.Value; i++)
            t.Chairs += ";m0";


        if (Helper.Instance.Update())
        {
            mGrid.EditIndex = -1;
            UpdateAllChairs();
            BindData();
            mError.Text = "بلیط با موفقیت بروزرسانی شد ";
        }
        else
        {
            mError.Text = "خطا در بروزرسانی بلیط :<br>" + Helper.Instance.LastException.Message;
        }
    }

    protected void mGrid_RowEditing(object sender, GridViewEditEventArgs e)
    {
        mGrid.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void mGrid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        mGrid.EditIndex = -1;
        BindData();
    }
    protected void mGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex < 0)
            return;
        int id = Tool.GetInt(mGrid.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;

        if (Helper.Instance.DeleteTicket(id))
        {
            mGrid.SelectedIndex = -1;
            BindData();
        }
        else
        {
            mError.Text = "این آیتم استفاده شده است و نمیتوان آنرا حذف کرد.\r\n" + Helper.Instance.LastException.Message;
        }
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Ticket t = e.Row.DataItem as Ticket;
            if (t != null)
            {
                e.Row.Cells[0].Text = t.ID.ToString();

                e.Row.Cells[4].Text = t.Fund.Title;
                e.Row.Cells[5].Text = t.Branch.Title;
                JsTools.HandleDeleteButton(e);
            }
        }
    }
    protected void doAutoFill_Click(object sender, EventArgs e)
    {
        if (mTrip.CarType == null)
        {
            mError.Text = "نوع اتوکار انتخاب نشده است";
            return;
        }
        if (mTrip.CarType.Layout.NumChairs == null)
        {
            mError.Text = "چیدمان صندلی ندارد";
            return;
        }
        int remain = mTrip.CarType.Layout.NumChairs.Value - Helper.Instance.GetNumTickets(mTrip.ID);
        if (remain <= 0)
        {
            mError.Text = "همه صندلیها فروخته شده اند";
            return;
        }
        int fundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        if (fundID == Null.NullInteger)
        {
            mError.Text = "صندوق انتخاب نشده است";
            return;
        }
        int branchID = Tool.GetInt(mBranchID.SelectedValue, Null.NullInteger);
        if (branchID == Null.NullInteger)
        {
            mError.Text = "دفتر انتخاب نشده است";
            return;
        }
        string[] names = null;
        if (!string.IsNullOrEmpty(mTrip.Service.DefTickets))
            names = mTrip.Service.DefTickets.Split(',', '،', ':');
        PassengerList passengerList = Helper.Instance.GetPassengerList(Tool.GetInt(mPassengerList.SelectedValue, 0));
        if (passengerList != null)
        {
            if (!string.IsNullOrEmpty(passengerList.List))
                names = Helper.Instance.GetPassengerList(Tool.GetInt(mPassengerList.SelectedValue, 0))
                    .List.Split(',', '،', ':');
        }
        if (names == null || names.Length == 0 || (names.Length == 1 && names[0].Trim().Length == 0))
            names = Tool.GetString(Helper.Instance.GetSettingValue("AutoFillNames"), "").Split(',', '،', ':');
        if (names == null || names.Length == 0 || (names.Length == 1 && names[0].Trim().Length == 0))
        {
            mError.Text = "ليست نامهای پر کردن خودکار سرویس در تنظیمات پر نشده است";
            return;
        }
        Random rnd = new Random();
        for (int f = 0; f < remain; f++)
        {
            Ticket t = new Ticket();
            t.TripID = this.TripID;
            t.SaleUserID = SiteSettings.UserID;
            t.SaleDate = DateTime.Now;

            t.Man = true;
            string name = names[rnd.Next(0, names.Length)].Trim();
            if (name.StartsWith("-"))
            {
                name = name.Substring(1);
                t.Man = false;
            }
            t.Fullname = name;
            t.No = Helper.Instance.GetNewTicketNo(mTrip.ID);
            t.NumChairs = 1;
            t.Chairs = ";m0;";
            t.CityID = mTrip.Service.Path.DestCityID;
            t.SaleType = (int)SaleTypes.Sale;
            t.FundID = fundID;
            t.Expire = null;
            t.SaleType = (int)SaleTypes.OfficeSale;
            t.BranchID = branchID;
            t.Tel = "";
            t.Price = mTrip.Price;
            t.Discount = 0;// Tool.GetInt(mDiscount.Text, 0);

            Helper.Instance.DB.Tickets.InsertOnSubmit(t);
            if (!Helper.Instance.Update())
            {
                mError.Text = "خطا در فروش " + Helper.Instance.LastException.Message;
                break;
            }
        }

        UpdateAllChairs();
        BindData();
    }
    private bool CreateTrip(int pathID, int carTypeID)
    {
        DateTime date = Tool.ParsePersianDate(mDate.Text, Null.NullDate);
        if (date == Null.NullDate)
        {
            mError.Text = "تاریخ اشتباه است.";
            return false;
        }
        int hour = Tool.GetInt(mHour.Text, Null.NullInteger);
        if (hour < 0 || hour > 23)
        {
            mError.Text = "ساعت باید بین 0 و 23 باشد.";
            return false;
        }
        int minute = Tool.GetInt(mMinute.Text, Null.NullInteger);
        if (minute < 0 || minute > 59)
        {
            mError.Text = "دقیقه باید بین 0 و 59 باشد.";
            return false;
        }
        ServiceTypes sType = ServiceTypes.Normal;
        Path path = Helper.Instance.GetPath(pathID);
        int specialID = Helper.Instance.GetCarType(carTypeID).SpecialID;
        RateItem rateItem = null;
        #region Handle RateItem
        //rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, path.SrcCityID, path.DestCityID, specialID, date);
        rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, path.SrcCityID, path.DestCityID, specialID, date);
        if (rateItem == null)
        {
            mError.Text = "برای این نوع اتوکار و مسیر، در قیمت گذاری دوره ای، قیمت تعریف نشده است.";
            doSale2.Enabled = doSale.Enabled = false;
            return false;
        }
        #endregion

        #region Create Service
        Service info = new Service();
        info.ServiceType = (int)sType;
        info.CarTypeID = carTypeID;
        info.SpecialID = specialID;// Tool.GetInt(mSpecialID.SelectedValue, Null.NullInteger);
        info.Special = Helper.Instance.GetSpecial(info.SpecialID);
        info.PathID = pathID;
        info.DepartureTime = Tool.FixTime(string.Format("{0}:{1}", hour.ToString().PadLeft(2, '0'), minute.ToString().PadLeft(2, '0')));
        info.Date = date;
        info.Price = rateItem.Cost;
        info.Toll = rateItem.Toll;
        info.Insurance = rateItem.Insurance;
        info.Reception = rateItem.ReceptionCost;
        info.Days = "";// GetDays();
        info.Enabled = true;
        Helper.Instance.DB.Services.InsertOnSubmit(info);
        #endregion

        if (Helper.Instance.Update())
        {
            mError.Text += "تعریف سرویس با موفقیت اضافه شد";
            string error;
            Trip tr = Helper.Instance.CreateTrip(out error, info.Date, info.ID, Null.NullInteger, Null.NullInteger);
            if (tr != null)
            {
                mError.Text += " &nbsp;-&nbsp;برای این تعریف سرویس تکی یک سرویس ایجاد شد";
                TripID = tr.ID;
            }
        }
        else
        {
            mError.Text = Helper.Instance.LastException.Message;
            doSale2.Enabled = doSale.Enabled = false;
            return false;
        }


        mTrip = Helper.Instance.GetTrip(this.TripID);
        if (mTrip == null)
            return false;
        doSale2.Enabled = doSale.Enabled = true;
        mTripTitle.Text = string.Format("{0} ({1}) ({2} - {3}) شماره: {4}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDateRtl(mTrip.Date, ""), mTrip.Service.DepartureTime, mTrip.ID);
        doClose.NavigateUrl = "Close.aspx?tripid=" + this.TripID;
        doRefresh.NavigateUrl = "sale2.aspx?tripid=" + this.TripID;

        #region Pre
        Trip pre = Helper.Instance.GetPreTrip(mTrip);
        if (pre == null || pre.ID == mTrip.ID || pre.Closed)
        {
            doShowPre.Text = "قبلی";
            doShowPre.NavigateUrl = "";
            doShowPre.Enabled = false;
        }
        else
        {
            doShowPre.Text = string.Format("قبلی({0})", string.Format("{0} ({1}) ({2} - {3})", pre.Service.Path.Title, pre.Service.ServiceType2, Tool.ToPersianDateRtl(pre.Date, ""), pre.Service.DepartureTime, pre.ID));
            doShowPre.NavigateUrl = "Sale.aspx?tripid=" + pre.ID;
            doShowPre.Enabled = true;
        }
        #endregion
        #region Next
        Trip next = Helper.Instance.GetNextTrip(mTrip);
        if (next == null || next.ID == mTrip.ID || next.Closed)
        {
            doShowNext.Text = "بعدی";
            doShowNext.NavigateUrl = "";
            doShowNext.Enabled = false;
        }
        else
        {
            doShowNext.Text = string.Format("بعدی({0})", string.Format("{0} ({1}) ({2} - {3})", next.Service.Path.Title, next.Service.ServiceType2, Tool.ToPersianDateRtl(next.Date, ""), next.Service.DepartureTime, next.ID));
            doShowNext.NavigateUrl = "Sale.aspx?tripid=" + next.ID;
            doShowNext.Enabled = true;
        }
        #endregion
        return true;
    }
    protected void doSave_Click(object sender, EventArgs e)
    {
        if (Save(false))
        {
            BindData();
            mPrice.Text = mTrip.Service.Price.ToString(); //GetPrice(mTrip.Tickets.Count > 0).ToString();
            mError.Text += "بروز رساني با موفقیت انجام شد";
            JsTools.SetFocus(mName);
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
        }
    }
    protected void doSaveClose_Click(object sender, EventArgs e)
    {
        Calc();
        if (Save(true))
        {
            BindData();
            mError.Text += "<br />بروز رساني با موفقیت انجام شد";
            if (string.IsNullOrEmpty(mTrip.TrackCode))
                mError.Text += "<br />" + "وضعیت صورت آفلاین است";
            if (mTrip.SentToInsurance != true)
            {
                try
                {
                    FillTrip();
                    if (!mTrip.CarID.HasValue)
                    {
                        mError.Text += "برای ارسال به بیمه حتما باید اتوکار انتخاب شده باشد";
                        return;
                    }
                    if (!mTrip.DriverID1.HasValue)
                    {
                        mError.Text += "برای ارسال به بیمه حتما باید راننده اول انتخاب شده باشد";
                        return;
                    }
                    string result = Helper.Instance.RequestInsurance(mTrip);
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(result);
                    XmlNodeList msg = xml.GetElementsByTagName("Message");
                    XmlNodeList status = xml.GetElementsByTagName("Status");
                    XmlNodeList trackCode = xml.GetElementsByTagName("TrackingCode");
                    XmlNodeList errorCode = xml.GetElementsByTagName("ErrorCode");
                    if (status.Item(0).InnerText == "OK" || errorCode.Item(0).InnerText == "0" ||
                        errorCode.Item(0).InnerText == "-10")
                    {
                        mTrip.SentToInsurance = true;
                        mTrip.InsuranceTrackCode = trackCode.Item(0).InnerText;
                    }

                    Save(false);
                    BindData();
                    mError.Text += msg.Item(0).InnerText + ". شماره رهگیری بیمه: " + mTrip.InsuranceTrackCode;
                    mError.Visible = true;
                }
                catch (Exception ex)
                {
                    mError.Text += "خطا در ارسال اطلاعات برای بیمه:" + ex.Message;
                    mError.Visible = true;
                }
            }
            JsTools.SetFocus(mName);
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
        }
    }
    int GetPrice(bool anySold)
    {
        if (mTrip == null)
            return 0;

        int num = mTrip.CarType.Layout.NumChairs.HasValue ? mTrip.CarType.Layout.NumChairs.Value : 0;
        if (num > 5)
            return mTrip.Price;

        RateItem rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, mTrip.Service.Path.SrcCityID, mTrip.Service.Path.DestCityID, mTrip.Service.SpecialID, mTrip.Date);
        if (rateItem != null && rateItem.Cost2.HasValue && rateItem.Cost2.Value > 0)
        {
            if (anySold) // it is the second or more chair
                return (mTrip.Price * 4 - rateItem.Cost2.Value) / 3;
            return rateItem.Cost2.Value; // it is the first chair
        }
        // The cost2 is not defined, use the old algorithm
        int total = mTrip.Price * num;
        int dif = (mTrip.Price > 20000 ? total % 40000 : total % 4000);
        int price = (total - dif) / num;
        if (anySold)
            return price;
        return price + dif;
    }
    protected void doPrint_Click(object sender, EventArgs e)
    {
        if (doPrint.Enabled == false)
            return;
        //Calc();
        if (Save(!mTrip.Closed))
        {
            BindData();
            mError.Text = "بروز رساني با موفقیت انجام شد";
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
            return;
        }

        //Trip trp = Helper.Instance.GetTrip(TripID);
        Response.Redirect(string.Format("Reports/Report.aspx?id={0}&cc={1}&repname={2}&secdest={3}&warning={4}", TripID.ToString(), Helper.Instance.GetSettingValue("CompanyCode"), "SuratVaziatM", mSecondDistCity.Text, mWarnings.Text.Replace("<br>", " ; ")), true);

        //        string open = @"
        //<script language='javascript' type='text/javascript'>
        //function openprint()
        //{
        //    window.open('Reports/Report.aspx?id=IDID&cc=CompanyCode&repname=SuratVaziatM','Surat', 'location=no,toolbar=no,scrollbars=yes,menubar=no,status=no,top=50,left=300,height=550,width=900');
        //}
        //window.open('Reports/Report.aspx?id=IDID&cc=CompanyCode&repname=SuratVaziatM','Surat', 'location=no,toolbar=no,scrollbars=yes,menubar=no,status=no,top=50,left=50,height=550,width=900');
        ////window.onload=openprint;
        //</script>".Replace("IDID", TripID.ToString()).Replace("CompanyCode",Helper.Instance.GetSettingValue("CompanyCode"));
        //        Page.RegisterStartupScript("openprint", open);
    }
    protected void doPrint2_Click(object sender, EventArgs e)
    {
        if (doPrint2.Enabled == false)
            return;
        //Calc();
        if (Save(!mTrip.Closed))
        {
            BindData();
            mError.Text = "بروز رساني با موفقیت انجام شد";
        }
        else
        {
            if (Helper.Instance.LastException != null)
                mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
            else
                mError.Text += "<br />بروز رساني انجام نشد";
            return;
        }

        //Trip trp = Helper.Instance.GetTrip(TripID);
        //Response.Redirect(string.Format("Reports/Report.aspx?id={0}&repname={1}", TripID.ToString(), "SuratVaziatM"), true);

        string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Reports/Report.aspx?id=IDID&repname=SuratVaziatMD','Surat', 'location=no,toolbar=no,scrollbars=yes,menubar=no,status=no,top=50,left=300,height=550,width=900');
}
window.open('Reports/Report.aspx?id=IDID&repname=SuratVaziatMD','Surat', 'location=no,toolbar=no,scrollbars=yes,menubar=no,status=no,top=50,left=50,height=550,width=900');
//window.onload=openprint;
</script>".Replace("IDID", TripID.ToString());
        Page.RegisterStartupScript("openprint", open);
    }
    private void Calc()
    {
        FillTrip();

        RateItem rateItem = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, mTrip.Service.Path.SrcCityID, mTrip.Service.Path.DestCityID, mTrip.Service.SpecialID, mTrip.Date);
        decimal numTickets = Helper.Instance.GetNumTickets(mTrip.ID);
        decimal price = Helper.Instance.GetPrice(mTrip.ID) - numTickets * (rateItem == null ? 0 : rateItem.PureCost);

        decimal y = price - mTrip.Insurance - (mTrip.Reception + mTrip.ExtraCost) * numTickets - mTrip.Stamp;
        decimal toll2 = FormulaEvaluater.Evaluate(mTrip.ID, Helper.Instance.GetFieldFormul("Toll"));
        y = y - toll2;
        decimal commision2 = FormulaEvaluater.Evaluate(mTrip.ID, Helper.Instance.GetFieldFormul("Commission"));


        mTrip.TotalPrice = Helper.Instance.GetPrice(mTrip.ID);
        mTrip.Toll2 = (int)(toll2 - (toll2 % 100));
        mTrip.Comission2 = (int)(commision2 - (commision2 % 100));

        decimal otherDeficits = mTrip.BodyInsurance + mTrip.Insurance + (mTrip.ExtraCost + mTrip.Reception) * numTickets + mTrip.Stamp;
        mTrip.OtherDeficits = (int)(otherDeficits - (otherDeficits % 100));
        decimal sums = toll2 + otherDeficits + commision2;
        mTrip.SumDeficits = (int)(sums - (sums % 100));

        decimal autoShare = price - (toll2 + otherDeficits + commision2);
        decimal others = mTrip.Others.HasValue ? mTrip.Others.Value : 0;
        autoShare = autoShare - others;
        mTrip.AutoShare = (int)(autoShare - (autoShare % 100));

        // --------------------
        //mTrip.Toll = Tool.GetInt(mToll.Text, 0);
        //mTrip.Comission = Tool.GetInt(mComission.Text, 0);
        //mTrip.BodyInsurance = Tool.GetInt(mBodyInsurance.Text, 0);
        //mTrip.Others = Tool.GetInt(mOthers.Text, 0);
        //mTrip.Insurance = Tool.GetInt(mInsurance.Text, 0);
        //mTrip.Stamp = Tool.GetInt(mStamp.Text, 0);
        //mTrip.ExtraCost = Tool.GetInt(mExtraCost.Text, 0);
        //mTrip.Reception = Tool.GetInt(mReception.Text, 0);
        //mTrip.Price = Tool.GetInt(mPrice.Text, 0);
        //mTrip.InsuranceWage = Tool.GetInt(mInsuranceWage.Text, 0);
        //mTrip.BodyInsuranceWage = Tool.GetInt(mBodyInsuranceWage.Text, 0);

        mTrip.BodyInsuranceWage = Helper.Instance.GetWagePrice(0, int.Parse(mTrip.Service.Path.City1.Country.Code) > 1 ? true : false, mTrip.CarType.Layout.NumChairs > 30 ? 0 : mTrip.CarType.Layout.NumChairs > 5 ? 1 : 2, mTrip.Service.Path.KMs);
        mTrip.InsuranceWage = Helper.Instance.GetWagePrice(1, int.Parse(mTrip.Service.Path.City1.Country.Code) > 1 ? true : false, mTrip.CarType.Layout.NumChairs > 30 ? 0 : mTrip.CarType.Layout.NumChairs > 5 ? 1 : 2, mTrip.Service.Path.KMs);
    }
    private void FillTrip()
    {
        if (mTrip == null)
            return;
        Car car = Helper.Instance.GetCarByCode(mCarCode.Text, true);
        if (car == null)
        {
            car = Helper.Instance.GetCar(mTrip.CarID);
            if (car == null || car.Code != mCarCode.Text)
                car = null;
        }
        Driver driver = Helper.Instance.GetDriverByCode(mDriverCode.Text, true);
        if (driver == null && mTrip.DriverID1.HasValue)
        {
            driver = Helper.Instance.GetDriver(mTrip.DriverID1.Value);
            if (driver == null || driver.Code != mDriverCode.Text)
                driver = null;
        }
        if (driver != null)
            mTrip.DriverID1 = driver.ID;
        else
            mTrip.DriverID1 = null;

        int? oldCarID = mTrip.CarID;
        if (car == null)
        {
            mTrip.CarID = Null.NullInteger;
        }
        else
        {
            mTrip.CarID = car.ID;
            mTrip.CarType = car.CarType;
            mTrip.CarTypeID = car.CarTypeID;
        }
        if (mTrip.CarID == Null.NullInteger && mTrip.DriverID1 != null)
        {
            Driver dr = Helper.Instance.GetDriver(mTrip.DriverID1.Value);
            if (dr != null && dr.CarID != null)
            {
                Car cr = Helper.Instance.GetCar(dr.CarID);
                if (cr != null && cr.CarType.SpecialID == mTrip.Service.SpecialID)
                {
                    mTrip.CarType = cr.CarType;
                    mTrip.CarTypeID = cr.CarTypeID;
                    mTrip.CarID = cr.ID;
                }
            }
        }

        if (mTrip.CarID != Null.NullInteger)
        {
            #region Process Car
            Car c = Helper.Instance.GetCar(mTrip.CarID.Value);
            if (c != null)
            {
                Service s = Helper.Instance.GetService(mTrip.ServiceID);
                if (c.CarType.SpecialID != s.SpecialID)
                {
                    mError.Text = "نحوه سرویس دهی اتوکار انتخاب شده با نحوه سرویس دهی تعريف شده در سرويس يکي نيست.";
                    return;
                }
                if (oldCarID == null || oldCarID.Value != mTrip.CarID)
                {
                    mTrip.Comission = c.Commission;
                    if (c.Insurance != null && c.Insurance.InsuranceKMs != null && c.Insurance.InsuranceKMs.Count > 0)
                    {
                        for (int i = 0; i < c.Insurance.InsuranceKMs.Count; i++)
                        {
                            if (c.Insurance.InsuranceKMs[i].StartKM <= s.Path.KMs && s.Path.KMs <= c.Insurance.InsuranceKMs[i].EndKM)
                            {
                                mTrip.BodyInsurance = c.Insurance.InsuranceKMs[i].Price;
                                break;
                            }
                        }
                    }
                }
            }
            #endregion
        }
        else
            mTrip.CarID = null;

        if (mTrip.Stamp <= 0)
        {
            int stamp = Tool.GetInt(Helper.Instance.GetSettingValue("Stamp"), Null.NullInteger);
            if (stamp != Null.NullInteger)
                mTrip.Stamp = stamp;
        }
    }
    private void CheckDates(Car car, List<Ticket> tickets)
    {
        if (car == null || tickets == null || tickets.Count == 0)
        {
            doPrint.Enabled = doPrint2.Enabled = false;
            return;
        }
        mWarnings.Text = "";
        bool enableClose = true;
        int vDays = (int)(car.VisitDate - DateTime.Today.AddDays(1)).TotalDays;
        if (vDays < 0)
        {
            mWarnings.Text += string.Format("معاینه فني اتوکار اعتبار ندارد<br>", vDays);
            enableClose = false;
        }
        else if (vDays <= 5)
        {
            mWarnings.Text += string.Format("معاینه فني اتوکار فقط {0} روز اعتبار دارد<br>", vDays);
            //if (vDays <= 2)
            //    enableClose = false;
        }
        int dDays = (int)(car.ThirdPersonDate - DateTime.Today.AddDays(1)).TotalDays;
        if (dDays < 0)
        {
            mWarnings.Text += string.Format("بيمه شخص ثالث اتوکار اعتبار ندارد<br>", dDays);
            enableClose = false;
        }
        else if (dDays <= 5)
        {
            mWarnings.Text += string.Format("بيمه شخص ثالث اتوکار فقط {0} روز اعتبار دارد<br>", dDays);
            //if (dDays <= 2)
            //    enableClose = false;
        }
        if (mTrip != null && mTrip.Driver != null)
        {
            int sDays = (int)((DateTime)mTrip.Driver.SmartCardDate - DateTime.Today.AddDays(1)).TotalDays;
            if (sDays < 0)
            {
                mWarnings.Text += string.Format("تاریخ اعتبار کارت هوشمند راننده اعتبار ندارد<br>", sDays);
                enableClose = false;
            }
            else if (sDays <= 5)
            {
                mWarnings.Text += string.Format("تاریخ اعتبار کارت هوشمند راننده 1 فقط {0} روز اعتبار دارد<br>", sDays);
                //if (hDays <= 2)
                //    enableClose = false;
            }
            int hDays = (int)(mTrip.Driver.HourDate - DateTime.Today.AddDays(1)).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده اعتبار ندارد<br>", hDays);
                enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 1 فقط {0} روز اعتبار دارد<br>", hDays);
                //if (hDays <= 2)
                //    enableClose = false;
            }
            int ddDays = (int)(mTrip.Driver.DrivingDate - DateTime.Today.AddDays(1)).TotalDays;
            if (ddDays < 0)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 1 اعتبار ندارد<br>", ddDays);
                enableClose = false;
            }
            else if (ddDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 1 فقط {0} روز اعتبار دارد<br>", ddDays);
                //if (hDays <= 2)
                //    enableClose = false;
            }
        }
        doPrint.Enabled = doPrint2.Enabled = enableClose;
    }
    private bool Save(bool close)
    {
        if (!PrepareSave())
            return false;

        // ---------------------
        bool wasClosed = mTrip.Closed;
        if (wasClosed == false && close)
        {
            if (!mTrip.CarID.HasValue)
            {
                mError.Text = "برای بستن حتما باید اتوکار انتخاب شده باشد";
                return false;
            }
        }
        if (wasClosed == false && close)
        {
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text = "برای بستن حتما باید راننده اول انتخاب شده باشد";
                return false;
            }
        }
        if (wasClosed == false && close)
        {
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text = "برای بستن حتما باید راننده اول انتخاب شده باشد";
                return false;
            }
        }
        if (wasClosed == false && close)
        {
            if (mTrip.Tickets.Count == 0)
            {
                mError.Text = "برای بستن حتما باید حداقل یک بلیط فروخته شده باشد";
                return false;
            }
        }
        if (!wasClosed && close)
        {
            mTrip.CloseDate = DateTime.Now;
            mTrip.CloseUserID = SiteSettings.UserID;
        }
        mTrip.Series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
        if (close)
        {
            mTrip.Closed = true;
            if (string.IsNullOrEmpty(mTrip.Series) || mTrip.Series.Trim(',').Length == 0 /*|| !wasClosed*/)
            {
                mTrip.Series = string.Format("{0},{1},{2}"
                       , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                       , Helper.Instance.GetSettingValue("SeriesNoMini3"));
            }
            else
            {
                string[] ss = string.IsNullOrEmpty(mTrip.Series) ? null : mTrip.Series.Split(',');
                if (ss == null || ss.Length < 3 || ss[0].Trim().Length == 0
                      || ss[1].Trim().Length == 0 || ss[2].Trim().Length == 0)
                    mTrip.Series = string.Format("{0},{1},{2}"
                           , Helper.Instance.GetSettingValue("SeriesNoMini1"), Helper.Instance.GetSettingValue("SeriesNoMini2")
                           , Helper.Instance.GetSettingValue("SeriesNoMini3"));
            }
            //if (mTrip.No < 1 /*|| !wasClosed*/)
        }


        // double check the SeriesNo
        if (mTrip.Closed)
        {
            try
            {
                mTrip.No = Tool.GetInt(mNo.Text, Helper.Instance.GetNextSeriesStart(mTrip.Series, mTrip.IsMini));
                string[] vals = mTrip.Series.Split(',');
                if (vals.Length < 3 || string.IsNullOrEmpty(vals[0].Trim()) || string.IsNullOrEmpty(vals[1].Trim())
                        || string.IsNullOrEmpty(vals[2].Trim()))
                {
                    mError.Text = "سری صورت اشکال دارد. همه بخشهای سری صورت باید پر باشند";
                    return false;
                }
                mTrip.Series = mTrip.Series.Trim(',');
            }
            catch { }
        }
        if (Helper.Instance.Update())
        {
            if (wasClosed == false && mTrip.Closed)
                UpdateSettings();

            // Delete Extra Tickets
            if (mTrip.CarType != null)
            {
                #region Delete Extra Tickets
                int tickets = Helper.Instance.GetNumTickets(mTrip.ID);
                if (tickets > mTrip.CarType.Layout.NumChairs)
                {
                    int toDel = tickets - mTrip.CarType.Layout.NumChairs.Value;
                    List<Ticket> ts = mTrip.Tickets.ToList();
                    List<Ticket> delets = new List<Ticket>();
                    while (true)
                    {
                        Ticket t = ts[ts.Count - 1];
                        if (t.NumChairs.HasValue == false || t.NumChairs <= toDel)
                        {
                            ts.RemoveAt(ts.Count - 1);
                            Helper.Instance.DeleteTicket(t);
                            tickets = Helper.Instance.GetNumTickets(mTrip.ID);
                            toDel = tickets - mTrip.CarType.Layout.NumChairs.Value;
                        }
                        else
                        {
                            t.NumChairs -= toDel;
                            int index = 1;
                            {
                                t.Chairs = ";m" + index++ + ";";
                                for (int x = 0; x < t.NumChairs.Value - 1; x++)
                                    t.Chairs = string.Format("{0}m{1};", t.Chairs, index++);
                            }
                            Helper.Instance.Update();
                            tickets = Helper.Instance.GetNumTickets(mTrip.ID);
                            toDel = tickets - mTrip.CarType.Layout.NumChairs.Value;
                        }
                        if (toDel <= 0)
                            break;
                    }
                }
                #endregion
            }
            return true;
        }
        return false;

    }
    private bool PrepareSave()
    {
        Car car = Helper.Instance.GetCarByCode(mCarCode.Text, true);
        Driver driver = Helper.Instance.GetDriverByCode(mDriverCode.Text);
        if (mTrip == null)
        {
            Path path = Helper.Instance.GetPathByCode(mPathCode.Text);
            if (path == null)
            {
                mError.Text = "مسیر اشتباه است";
                return false;
            }
            if (car == null)
            {
                mError.Text = "اتوکار اشتباه است";
                return false;
            }

            // -------------------------------------------
            mError.Text = "";
            int vDays = (int)(car.VisitDate - DateTime.Today.AddDays(1)).TotalDays;
            if (vDays < 0)
                mError.Text += string.Format("معاینه فني اتوکار اعتبار ندارد<br>", vDays);
            int dDays = (int)(car.ThirdPersonDate - DateTime.Today.AddDays(1)).TotalDays;
            if (dDays < 0)
                mError.Text += string.Format("بيمه شخص ثالث اتوکار اعتبار ندارد<br>", dDays);

            if (driver != null)
            {
                int hDays = (int)(driver.HourDate - DateTime.Today.AddDays(1)).TotalDays;
                if (hDays < 0)
                    mError.Text += string.Format("دفترچه ساعت راننده 1 اعتبار ندارد<br>", hDays);
                if (driver.SmartCardDate == null)
                    mError.Text += "تاریخ اعتبار کارت هوشمند مقداری ندارد";
                else
                {
                    int sDays = (int)((DateTime)driver.SmartCardDate - DateTime.Today.AddDays(1)).TotalDays;
                    if (sDays < 0)
                        mError.Text += string.Format("تاریخ اعتبار کارت هوشمند راننده 1 اعتبار ندارد<br>", sDays);
                }
            }
            if (mError.Text.Length > 0)
                return false;
            // -------------------------------------------
            if (!CreateTrip(path.ID, car.CarTypeID))
            {
                mError.Text = "خطا در ایجاد تعریف سرویس و سرویس :<br>" + mError.Text;
                return false;
            }
        }
        else
        {
            if (car == null)
            {
                car = Helper.Instance.GetCar(mTrip.CarID);
                if (car == null || car.Code != mCarCode.Text)
                    car = null;
            }
            if (driver == null && mTrip.DriverID1.HasValue)
            {
                driver = Helper.Instance.GetDriver(mTrip.DriverID1.Value);
                if (driver == null || driver.Code != mDriverCode.Text)
                    driver = null;
            }
            DateTime date = Tool.ParsePersianDate(mDate.Text, Null.NullDate);
            if (date == Null.NullDate)
            {
                mError.Text = "تاریخ اشتباه است.";
                return false;
            }
            int hour = Tool.GetInt(mHour.Text, Null.NullInteger);
            if (hour < 0 || hour > 23)
            {
                mError.Text = "ساعت باید بین 0 و 23 باشد.";
                return false;
            }
            int minute = Tool.GetInt(mMinute.Text, Null.NullInteger);
            if (minute < 0 || minute > 59)
            {
                mError.Text = "دقیقه باید بین 0 و 59 باشد.";
                return false;
            }
            bool updateService = false;
            if (Tool.ToPersianDate(mTrip.Service.Date, "") != mDate.Text)
                updateService = true;
            else if (mTrip.Service.DepartureTime != Tool.FixTime(string.Format("{0}:{1}", hour.ToString().PadLeft(2, '0'), minute.ToString().PadLeft(2, '0'))))
                updateService = true;
            if (updateService)
            {
                mTrip.Service.Date = date;
                mTrip.Service.DepartureTime = Tool.FixTime(string.Format("{0}:{1}", hour.ToString().PadLeft(2, '0'), minute.ToString().PadLeft(2, '0')));
                Helper.Instance.Update();
            }
            mTrip.Date = date;
        }
        mTrip.Others = 0;
        mTrip.DepartureTime = mTrip.Service.DepartureTime;
        if (driver != null)
        {
            mTrip.Driver = driver;
            mTrip.DriverID1 = driver.ID;
        }
        else
        {
            mTrip.Driver = null;
            mTrip.DriverID1 = null;
        }

        int? oldCarID = mTrip.CarID;
        if (car == null)
        {
            mTrip.CarID = Null.NullInteger;
        }
        else
        {
            //mTrip.Car = car;
            mTrip.CarID = car.ID;
            mTrip.CarType = car.CarType;
            mTrip.CarTypeID = car.CarTypeID;
        }
        CheckDates(car, mTrip.Tickets.ToList());
        if (mTrip.CarID == Null.NullInteger && mTrip.DriverID1 != null)
        {
            Driver dr = Helper.Instance.GetDriver(mTrip.DriverID1.Value);
            if (dr != null && dr.CarID != null)
            {
                Car cr = Helper.Instance.GetCar(dr.CarID);
                if (cr != null && cr.CarType.SpecialID == mTrip.Service.SpecialID)
                {
                    mTrip.CarType = cr.CarType;
                    mTrip.CarTypeID = cr.CarTypeID;
                    mTrip.CarID = cr.ID;
                }
            }
        }

        if (mTrip.CarID != Null.NullInteger)
        {
            #region Process Car
            Car c = Helper.Instance.GetCar(mTrip.CarID.Value);
            if (c != null)
            {
                Service s = Helper.Instance.GetService(mTrip.ServiceID);
                if (c.CarType.SpecialID != s.SpecialID)
                {
                    mError.Text = "نحوه سرویس دهی اتوکار انتخاب شده با نحوه سرویس دهی تعريف شده در سرويس يکي نيست.";
                    return false;
                }
                if (oldCarID == null || oldCarID.Value != mTrip.CarID)
                {
                    mTrip.Comission = c.Commission;
                    if (c.Insurance != null && c.Insurance.InsuranceKMs != null && c.Insurance.InsuranceKMs.Count > 0)
                    {
                        for (int i = 0; i < c.Insurance.InsuranceKMs.Count; i++)
                        {
                            if (c.Insurance.InsuranceKMs[i].StartKM <= s.Path.KMs && s.Path.KMs <= c.Insurance.InsuranceKMs[i].EndKM)
                            {
                                mTrip.BodyInsurance = c.Insurance.InsuranceKMs[i].Price;
                                break;
                            }
                        }
                    }
                }
            }
            #endregion
        }
        else
            mTrip.CarID = null;

        return true;
    }
    private void UpdateSettings()
    {
        bool ok = false;
        if (mTrip.No > 0)
        {
            //int no = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStart"), 1);
            //no = Math.Max(no, mTrip.No);
            int no = mTrip.No;// +1;
            ok = Helper.Instance.UpdateSetting("SeriesStartMini", no.ToString());
        }
        if (!string.IsNullOrEmpty(mTrip.Series) && mTrip.Series.Trim(',').Length > 0)
        {
            string[] ss = mTrip.Series.Split(',');
            if (ss.Length == 3 && ss[0].Trim().Length > 0 && ss[1].Trim().Length > 0
                    && ss[2].Trim().Length > 0)
            {
                ok = Helper.Instance.UpdateSetting("SeriesNoMini1", ss[0].Trim());
                ok = Helper.Instance.UpdateSetting("SeriesNoMini2", ss[1].Trim());
                ok = Helper.Instance.UpdateSetting("SeriesNoMini3", ss[2].Trim());
            }
        }
    }
    private void doSet_Click()
    {
        //changed in 20-09-92
        if (mTrip == null && mPathCode.Text.Trim().Length > 0)
        {
            Path path = Helper.Instance.GetPathByCode(mPathCode.Text);
            if (path == null)
                mError.Text = "کد مسیر اشتباه است<br>";
            else
                mPathTitle.Text = path.Title;
        }
        Car car = mCarCode.Text.Trim().Length > 0 ? Helper.Instance.GetCarByCode(mCarCode.Text, true) : null;
        Driver driver = mDriverCode.Text.Trim().Length > 0 ? Helper.Instance.GetDriverByCode(mDriverCode.Text, true) : null;
        if (driver != null && car == null && driver.CarID != null)
            car = Helper.Instance.GetCar(driver.CarID.Value);
        if (mDriverCode.Text.Trim().Length > 0 && driver == null)
            mError.Text += "کد راننده اشتباه است<br>";
        if (mCarCode.Text.Trim().Length > 0 && car == null)
            mError.Text += "کد اتوکار اشتباه است<br>";
        if (driver != null)
        {
            if (!CheckDateDriver(driver))
            {
                error = true;
            }
            else
            {
                error = false;
            }
        }
        if (car != null)
        {
            if (!CheckDateCar(car))
            {
                error = true;
            }
            else
            {
                error = false;
            }
        }
    }
    protected void doSetPath_Click(object sender, EventArgs e)
    {
        doSet_Click();
        JsTools.SetFocusSelect(mDriverCode);
    }
    protected void doSetDriver_Click(object sender, EventArgs e)
    {
        doSet_Click();
        if (!error)
        {
            JsTools.SetFocusSelect(mCarCode);
        }
        else
        {
            JsTools.SetFocusSelect(mDriverCode);
        }
    }
    protected void doSetCar_Click(object sender, EventArgs e)
    {
        doSet_Click();
        if (!error)
        {
            if (mHour.Visible)
                JsTools.SetFocusSelect(mHour);
            else
                JsTools.SetFocusSelect(doSave);
        }
        else
        {
            JsTools.SetFocusSelect(mCarCode);
        }
    }
    protected void doNew_Click(object sender, EventArgs e)
    {
        if (Tool.ButtonIsClicked(Request, doSetPath) || Tool.ButtonIsClicked(Request, doSetDriver) || Tool.ButtonIsClicked(Request, doSetCar))
            return;
        if (!Tool.ButtonIsClicked(Request, doNew))
            return;
        if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoDeleteMini"), false) == true)
            if (mTrip != null && mTrip.Tickets.Count == 0)
            {
                Helper.Instance.DeleteTrip(mTrip.ID);
                Helper.Instance.DeleteService(mTrip.ServiceID);
            }

        mPathCode.Text = mCarCode.Text = mDriverCode.Text = "";
        mPathTitle.Text = mCarTitle.Text = mDriverTitle.Text = "";
        this.TripID = Null.NullInteger;
        int pathID = Tool.GetInt(Request.QueryString["pathid"], Null.NullInteger);
        int carTypeID = Tool.GetInt(Request.QueryString["cartypeid"], Null.NullInteger);
        mTrip = null;
        mTripTitle.Text = "جدید";
        BindData();
        mShowDate.Checked = false;
        mShowDate_CheckedChanged(null, null);
        JsTools.SetFocus(mPathCode);
    }
    protected void doChooseAuto_Click(object sender, EventArgs e)
    {
        Car d = Helper.Instance.GetCar(mCar.Value);
        if (d == null)
        {
            mError.Text = "کد اتوکار زیر از کارت خوانده شد که در لیست همکاران موجود نیست. لطفا به صورت دستی اضافه شود:<br />" + mCar.Value;
            JsTools.SetFocusSelect(mCarCode);
            return;
        }
        mError.Text = "کد اتوکار از کارت خوانده شد.";
        mCarCode.Text = d.Code;
        doSet_Click();
        JsTools.SetFocusSelect(mHour);
    }
    protected void doChooseDriver_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver.Value);
            }
            catch
            {
                mError.Text = "کارت هوشمند راننده تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mError.Text = "کارت هوشمند راننده تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                mDriverCode.Text = d.Code;
                doSet_Click();
            }
            JsTools.SetFocusSelect(mDriverCode);
            return;
        }
        mError.Text = "کد راننده از کارت خوانده شد.";
        mDriverCode.Text = d.Code;
        doSet_Click();
        JsTools.SetFocusSelect(mCarCode);
    }
    protected void mShowDate_CheckedChanged(object sender, EventArgs e)
    {
        lTime.Visible = lDate.Visible = lHour.Visible = lMin.Visible = mShowDate.Checked;
        mDate.Visible = mMinute.Visible = mHour.Visible = mShowDate.Checked;

    }
    protected void doSaveInCard_Click(object sender, EventArgs e)
    {
        if (mTrip != null)
            puEx.Show();
    }
    protected void doGetTrackingCode_Click(object sender, EventArgs e)
    {
        if (mTrip == null)
            return;
        try
        {
            Calc();

            if (!PrepareSave())
                return;
            if (Save(!mTrip.Closed))
            {
                BindData();
                mError.Text += "<br />بروز رساني با موفقیت انجام شد";
            }
            // ---------------------
            if (!mTrip.CarID.HasValue)
            {
                mError.Text = "برای دریافت کد رهگیری حتما باید اتوکار انتخاب شده باشد";
                return;
            }
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text = "برای دریافت کد رهگیری حتما باید راننده اول انتخاب شده باشد";
                return;
            }

            if (mTrip.Tickets.Count == 0)
            {
                mError.Text = "برای دریافت کد رهگیری حتما باید حداقل یک بلیط فروخته شده باشد";
                return;
            }
            //if (!ValidateDates())
            //    return;
            if (!Helper.Instance.RequestTrackCode(mTrip))
                return;
            Save(false);
            BindData();
        }
        catch (Exception ex)
        {
            mError.Text += "<br />خطا در دریافت کد رهگیری صورت وضعیت: " + ex.Message;
            return;
        }
    }
    //added by hadi
    protected void do_DriverEdit_Click(object sender, EventArgs e)
    {
        Driver driver = Helper.Instance.GetDriverByCode(mDriverCode.Text, true);
        if (driver == null)
        {
            mError.Text = "راننده ای با این کد وجود ندارد";
            return;
        }
        if (mDriverCode.Text == "")
        {
            mError.Text = "ابتدا راننده را انتخاب کنید";
            return;
        }
        // Response.Redirect("~/Staff/Drivers.aspx?do=SaleEdit&DriverID=" + driver.ID);
        string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
        Page.RegisterStartupScript("openprint", open);
        JsTools.SetFocusSelect(mDriverCode);
    }

    protected void do_CarEdit_Click(object sender, EventArgs e)
    {
        Car car = Helper.Instance.GetCar(Helper.Instance.GetDriverByCode(mDriverCode.Text).CarID);
        if (car == null)
        {
            car = Helper.Instance.GetCarByCode(mCarCode.Text, true);
            if (car == null)
            {
                mError.Text = "اتوکاری با کد فوق وجود ندارد";
                return;
            }
            if (mCarCode.Text == "")
            {
                mError.Text = "ابتدا اتوکار را انتخاب کنید";
                return;
            }
        }
        //Response.Redirect("~/Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID );
        string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
}
window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
//window.onload=openprint;
</script>";
        Page.RegisterStartupScript("openprint", open);
        JsTools.SetFocusSelect(mDriverCode);
    }
    //added by hadi in 20-9-92
    bool CheckDateCar(Car car)
    {
        if (mError.Text == "")
        {
            int vDays = (int)(car.VisitDate - DateTime.Today.AddDays(1)).TotalDays;
            if (vDays < 0)
            {
                mError.Text += string.Format("معاینه فني اتوکار اعتبار ندارد<br>", vDays);
                error = true;
                //
                if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
                {
                    string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
}
window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
//window.onload=openprint;
</script>";
                    Page.RegisterStartupScript("openprint", open);
                }
            }
            int dDays = (int)(car.ThirdPersonDate - DateTime.Today.AddDays(1)).TotalDays;
            if (dDays < 0)
            {
                mError.Text += string.Format("بيمه شخص ثالث اتوکار اعتبار ندارد<br>", dDays);
                error = true;
                //
                if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
                {
                    string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
}
window.open('Staff/Cars.aspx?do=SaleEdit&CarID=" + car.ID + @"');
//window.onload=openprint;
</script>";
                    Page.RegisterStartupScript("openprint", open);
                }
            }
        }
        if (mError.Text == "")
        {
            mCarCode.Text = car.Code;
            mCarTitle.Text = car.Title;
            return true;
        }
        return false;
    }
    bool CheckDateDriver(Driver driver)
    {

        mError.Text = "";
        int hDays = (int)(driver.HourDate - DateTime.Today.AddDays(1)).TotalDays;
        if (hDays < 0)
        {
            mError.Text += string.Format("دفترچه ساعت راننده اعتبار ندارد<br>", hDays);
            error = true;
            //
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
        }
        int sDays = (int)((DateTime)driver.SmartCardDate - DateTime.Today.AddDays(1)).TotalDays;
        if (sDays < 0)
        {
            mError.Text += string.Format("کارت هوشمند راننده اعتبار ندارد<br>", sDays);
            error = true;
            //
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
        }
        int dDays = (int)(driver.DrivingDate - DateTime.Now).TotalDays;
        if (dDays < 0)
        {
            mError.Text += string.Format("کارت سلامت راننده اعتبار ندارد<br>", dDays);
            error = true;
            //
            if (Tool.GetBool(Helper.Instance.GetSettingValue("AutoOpen"), false))
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
}
window.open('Staff/Drivers.aspx?do=SaleEdit&Close=1&DriverID=" + driver.ID + @"');
//window.onload=openprint;
</script>";
                Page.RegisterStartupScript("openprint", open);
            }
        }
        if (mError.Text == "")
        {
            mDriverTitle.Text = driver.Title;
            return true;
        }
        return false;
    }

    protected void mSecondDistCity_TextChanged(object sender, EventArgs e)
    {

    }

    protected void doRequestInsurance_Click(object sender, EventArgs e)
    {
        try
        {
            FillTrip();
            if (!mTrip.CarID.HasValue)
            {
                mError.Text += "برای ارسال به بیمه حتما باید اتوکار انتخاب شده باشد";
                return;
            }
            if (!mTrip.DriverID1.HasValue)
            {
                mError.Text += "برای ارسال به بیمه حتما باید راننده انتخاب شده باشد";
                return;
            }
            string result = Helper.Instance.RequestInsurance(mTrip);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);
            XmlNodeList msg = xml.GetElementsByTagName("Message");
            XmlNodeList status = xml.GetElementsByTagName("Status");
            XmlNodeList trackCode = xml.GetElementsByTagName("TrackingCode");
            XmlNodeList errorCode = xml.GetElementsByTagName("ErrorCode");
            if (status.Item(0).InnerText == "OK" || errorCode.Item(0).InnerText == "0" || errorCode.Item(0).InnerText == "-10")
            {
                mTrip.SentToInsurance = true;
                mTrip.InsuranceTrackCode = trackCode.Item(0).InnerText;
            }

            Save(false);
            BindData();
            mError.Text = msg.Item(0).InnerText + "<br />";
            mError.Visible = true;
        }
        catch (Exception ex)
        {
            mError.Text = "خطا در ارسال اطلاعات برای بیمه:" + ex.Message;
        }
        mError.Visible = true;
    }

    protected void doSendToTechnical_Click(object sender, EventArgs e)
    {
        if (!mTrip.CarID.HasValue)
        {
            mError.Text += "برای ارسال به مدیر فنی حتما باید اتوکار انتخاب شده باشد";
            return;
        }
        if (!mTrip.DriverID1.HasValue)
        {
            mError.Text += "برای ارسال به بیمه حتما باید راننده انتخاب شده باشد";
            return;
        }
        string cntStr =
            System.Configuration.ConfigurationManager.ConnectionStrings["TechnicalConnectionString"].ConnectionString;
        CompanyTrips companyTrips = new CompanyTrips();
        int companyId = Helper.Instance.GetCompanyId(Helper.Instance.GetSettingValue("CompanyCode"), cntStr);
        if (companyId > 0)
        {
            int no = -1;
            string series = "";
            string plate = Helper.Instance.GetCar(mTrip.CarID).Plate;
            companyTrips.CompanyID = companyId;
            companyTrips.TripID = mTrip.ID;
            companyTrips.CarNo = plate;
            companyTrips.UserID = -1;
            companyTrips.Driver = mTrip.Driver.Name + ' ' + mTrip.Driver.Surname;
            try
            {
                no = Tool.GetInt(mNo.Text, Helper.Instance.GetNextSeriesStart(mTrip.Series, mTrip.IsMini));
                string[] vals = mTrip.Series.Split(',');
                if (vals.Length < 3 || string.IsNullOrEmpty(vals[0].Trim()) || string.IsNullOrEmpty(vals[1].Trim())
                        || string.IsNullOrEmpty(vals[2].Trim()))
                {
                    mError.Text = "سری صورت اشکال دارد. همه بخشهای سری صورت باید پر باشند";
                    return;
                }
                series = mTrip.Series.Trim(',');
            }
            catch { }
            companyTrips.TripNo = no;
            companyTrips.TripSeries = series;
            companyTrips.Status = (int)Status.NoCheck;
            if (Helper.Instance.AddCompanyTrips(companyTrips))
            {
                CompanyTripImpl companyTripImpl = new CompanyTripImpl();
                companyTripImpl.CompanyID = companyId;
                companyTripImpl.TripID = mTrip.ID;
                companyTripImpl.CarNo = plate;
                companyTripImpl.UserID = -1;
                companyTripImpl.Status = (int)Status.NoCheck;
                companyTripImpl.Date = DateTime.Now;
                companyTripImpl.Driver = mTrip.Driver.Name + ' ' + mTrip.Driver.Surname;
                companyTripImpl.TripSeries = series;
                companyTripImpl.TripNo = no;
                Helper.Instance.AddCompanyTripImpl(companyTripImpl);

                mTrip.CompanyUser = SiteSettings.User.FullName;
                mTrip.TripStatus = (int)TripStatus.Pending;
                if (Helper.Instance.Update())
                {
                    mError.Text = "صورت وضعیت برای مدیر فنی ارسال گردید.";
                    mError.Visible = true;
                    doSendToTechnical.Enabled = false;
                    doClose.Enabled = false;
                    doPrint.Enabled = false;
                    doSaveClose.Enabled = false;
                    doPrint2.Enabled = false;
                    doGetTrackingCode.Enabled = false;

                }
            }
        }
        else
        {
            mError.Text = "این تعاونی در سیستم مدیر فنی تعریف نشده است";
            mError.Visible = true;
        }
    }

    protected void doCheckFani_Click(object sender, EventArgs e)
    {
        if (!mTrip.CarID.HasValue)
        {
            mError.Text += "برای استعلام فنی حتما باید اتوکار انتخاب شده باشد";
            return;
        }
        string cntStr =
            System.Configuration.ConfigurationManager.ConnectionStrings["TechnicalConnectionString"].ConnectionString;
        Car car = Helper.Instance.GetCar(mTrip.CarID);
        CarTechnical carTechnical = Helper.Instance.GetCarTechnical(cntStr, car.SmartCard, car.Plate,
            Tool.ToPersianDate(DateTime.Now, DateTime.Now));
        if (carTechnical != null)
        {
            mTrip.TechnicalUser = carTechnical.UserName;
            mTrip.CompanyUser = SiteSettings.User.FullName;
            mTrip.TripStatus = (int)carTechnical.TechnicalStatus;
            mTrip.FaniDetail = carTechnical.FaniDetail;
            mTrip.Comments = carTechnical.Comment;
            if (Helper.Instance.Update())
            {
                if (mTrip.TripStatus==(int)TripStatus.Confirmed)
                {
                    mError.Text = "اتوکار امروز توسط مدیر فنی تأیید شده است";
                    mError.Visible = true;
                }
                if (mTrip.TripStatus==(int)TripStatus.Rejected)
                {
                    mError.Text = "اتوکار امروز توسط مدیر فنی رد شده است";
                    mError.Visible = true;
                }
                BindData();
            }

        }
        else
        {
            mError.Text = "اتوکار امروز توسط مدیر فنی بررسی نشده است";
            mError.Visible = true;
        }
    }
}


