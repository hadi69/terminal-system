﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TrackingCode : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
    }
    protected void doRetrive_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(sTrackCode.Text.Trim()))
        {
            mMsg.Text = "لطفا 'کد رهگیری' را وارد کنید.";
            return;
        }
        try
        {
            string userID = Helper.Instance.GetSettingValue("User_Id");
            string passwordID = Helper.Instance.GetSettingValue("Password_Id");
            int identityNo = Tool.GetInt(Helper.Instance.GetSettingValue("Identity_id"), 0);
            int companyCode = Tool.GetInt(Helper.Instance.GetSettingValue("CompanyCode"), 0);

            ir.rmto.soratonline.Statement statement = new ir.rmto.soratonline.Statement();
            string value = statement.Request_Info_Statement(userID, passwordID, identityNo, sTrackCode.Text, companyCode);
            if (string.IsNullOrEmpty(value))
                throw new Exception("");
            string[] words = value.Split(';');
            if (words.Length < 21)
            {
                //int errorCode = Tool.GetInt(words[0], -1);
                //if (errorCode != 0)
                    throw new Exception(words[0]);
            }
            int i = 0;
            tripNo.Text = words[i++];
            tripSeries.Text = words[i++];
            tripIsMini.Text  = words[i++] == "2" ? "سواری" : "اتوبوس";
            // Drivers
            driver1SmartCard.Text = words[i++]; driver1.Text = Helper.Instance.GetDriverFullNameBySmartCard(driver1SmartCard.Text);
            driver2SmartCard.Text = words[i++]; driver2.Text = Helper.Instance.GetDriverFullNameBySmartCard(driver2SmartCard.Text);
            driver3SmartCard.Text = words[i++]; driver3.Text = Helper.Instance.GetDriverFullNameBySmartCard(driver3SmartCard.Text);
            carSmartCard.Text = words[i++];
            // Prices
            tripTotalPrice.Text = words[i++];
            servicePathKMs.Text = words[i++];
            tripBodyInsurance.Text = words[i++];
            serviceInsurance.Text = words[i++];
            //// Cities
            //srcCityCode.Text = words[i++];
            //destCityCode.Text = words[i++];
            // Date
            tripDate.Text = words[i++];
            tripDepartureTime2.Text = words[i++];
            numTickets.Text = words[i++];
            // Car Type
            tripCarTypeCode.Text = words[i++]; tripCarTypeCode0.Text = Helper.Instance.GetCarTypeTitle(Tool.GetInt(tripCarTypeCode.Text, -1));
           // companyCode.Text = words[i++];
            numChairs.Text = words[i++];
            // Plaque
            plateNo.Text = words[i++];
            plateSeries.Text = words[i++];
            tripClosed.Text = words[i++];
            sentType.Text = words[i++];

            if (words.Length == 13)
            {
                string msg = "سری پلاک: " + words[1];
                msg += "<br />شماره پلاک: " + words[0];
                msg += "<br />سال تولید: " + words[2];
                msg += "<br />وضعیت: " + (words[3] == "1" ? "فعال" : "غیر فعال");
                msg += "<br />شماره کارت هوشمند: " + words[5];

                mMsg.Text = msg;
            }
        }
        catch (Exception ex)
        {
            mMsg.Text = "خطا در دریافت اطلاعات از سرور: " + ex.Message;
        }
    }
}
