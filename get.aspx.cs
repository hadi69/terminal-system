﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class get : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string op = Request.QueryString["op"];
        if (op != null)
        {
            op = op.ToLower();
            if (op == "GetCartable".ToLower())
            {
                Response.Clear();
                Response.Write(Helper.Instance.CheckCartable());
            }
        }
    }
}