﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TrackingCode.aspx.cs" Inherits="TrackingCode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0" class="filter">
        <tr>
            <td colspan="8">
                استعلام کد رهگیری
            </td>
        </tr>
        <tr>
            <td>
                کد رهگیری:
            </td>
            <td>
                <asp:TextBox ID="sTrackCode" runat="server" />
            </td>
            <td>
                <asp:Button ID="doRetrive" runat="server" CssClass="CB" OnClick="doRetrive_Click"
                    Text="استعلام" Width="100px" />&nbsp;<a href='#' class="CB" id="printRetrive" onclick='showHtml(event)'
                                    style='color: #fff'>&nbsp;&nbsp;چاپ&nbsp;&nbsp;</a>
                            <input type="hidden" runat="server" id="prinQry" class="printQry" />
            </td>
        </tr>
    </table>
    <asp:Label runat="server" ID="mMsg" class="Err" />
    <table border="0" cellpadding="4">
        <tr>
            <td><b>شماره صورت
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripNo"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>سری صورت
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripSeries"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>نوع اتوکار
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripIsMini"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>راننده 1
            </b>
            </td>
            <td>
                کارت هوشمند:</td>
            <td>
                <asp:Label runat="server" ID="driver1SmartCard"></asp:Label>
            </td>
            <td>
                نام:</td>
            <td>
                <asp:Label runat="server" ID="driver1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><b>راننده 2
            </b>
            </td>
            <td>
                کارت هوشمند:</td>
            <td>
                <asp:Label runat="server" ID="driver2SmartCard"></asp:Label>
            </td>
            <td>
                نام:</td>
            <td>
                <asp:Label runat="server" ID="driver2"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><b>راننده 3
            </b>
            </td>
            <td>
                کارت هوشمند:</td>
            <td>
                <asp:Label runat="server" ID="driver3SmartCard"></asp:Label>
            </td>
            <td>
                نام:</td>
            <td>
                <asp:Label runat="server" ID="driver3"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><b>اتوکار
            </b>
            </td>
            <td>
                کارت هوشمند:</td>
            <td>
                <asp:Label runat="server" ID="carSmartCard"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>کل کرايه
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripTotalPrice"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>مسافت
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="servicePathKMs"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>بیمه بدنه
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripBodyInsurance"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>بیمه سرنشین
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="serviceInsurance"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>شهر مبدا
            </b>
            </td>
            <td>
                کد:</td>
            <td>
                <asp:Label runat="server" ID="srcCityCode"></asp:Label>
            </td>
            <td>
                نام:</td>
            <td>
                <asp:Label runat="server" ID="srcCity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><b>شهر مقصد
            </b>
            </td>
            <td>
                کد:</td>
            <td>
                <asp:Label runat="server" ID="destCityCode"></asp:Label>
            </td>
            <td>
                نام:</td>
            <td>
                <asp:Label runat="server" ID="destCity"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><b>تاریخ سرویس
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripDate"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>ساعت سرویس
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripDepartureTime2"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>تعداد بلیط
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="numTickets"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>نوع اتوکار
            </b>
            </td>
            <td>
                کد:</td>
            <td>
                <asp:Label runat="server" ID="tripCarTypeCode"></asp:Label>
            </td>
            <td>
                نام:</td>
            <td>
                <asp:Label runat="server" ID="tripCarTypeCode0"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><b>تعداد صندلی
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="numChairs"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>شماره پلاک
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="plateNo"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>سری پلاک
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="plateSeries"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td><b>نوع صورت
            </b>
            </td>
            <td>
                <asp:Label runat="server" ID="tripClosed"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            
            <td>
                <b>وضعیت ارسال</b></td>
            <td>
                <asp:Label runat="server" ID="sentType"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function showHtml(event) {
            //event.preventDefault();
            //alert(2);
            var url = "invalidshtml.aspx?" + $('.printQry').val();
            //alert(url);
            window.open(url, "Zweitfenster", "location=1,status=1,scrollbars=1,width=1000,height=600,left=10,top=10")
            return false;
        }
    </script>
</asp:Content>
