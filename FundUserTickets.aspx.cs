﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FundUserTickets : System.Web.UI.Page
{
    Trip mTrip = null;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        if (!IsPostBack)
        {
            this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
            mTrip = Helper.Instance.GetTrip(this.TripID);
            if (mTrip == null)
                Response.Redirect("Trips.aspx", true);
            int userID = Tool.GetInt(Request.QueryString["userid"], Null.NullInteger);
            User u = Helper.Instance.GetUser(userID);
            if (u == null)
                mTripTitle.Text = string.Format("{0} ({1}) ({2}) شماره: {3}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDate(mTrip.Date, ""), mTrip.ID);
            else
            {
                mTripTitle.Text = string.Format("{0} ({1}) ({2}) شماره: {3} کاربر {4}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDate(mTrip.Date, ""), mTrip.ID, u.FullName);
            }
        }
        //else
        //    mTrip = Helper.Instance.GetTrip(TripID);
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        int userID = Tool.GetInt(Request.QueryString["userid"], Null.NullInteger);
        System.Collections.Generic.List<Ticket> all = null;// Helper.Instance.GetTickets(TripID, userID);
        try
        {
            if (userID == Null.NullInteger)
                all = Helper.Instance.DB.Tickets.Where(t => t.TripID == TripID).OrderBy(t => t.PaidBack).ToList();// GetTickets(tripID);
            else
                all = Helper.Instance.DB.Tickets.Where(t => t.TripID == TripID && t.FundUserID == userID).OrderBy(t => t.PaidBack).ToList();
        }
        catch
        {
        }
        list.DataSource = all;
        list.DataBind();


        var allHistory = Helper.Instance.GetTicketHistories(TripID, userID);
        listHistory.DataSource = allHistory;
        listHistory.DataBind();

        if (!mTrip.Closed)
            doReturn.NavigateUrl = "Trips.aspx";
        else
            doReturn.NavigateUrl = "Closeds.aspx";

        if (userID != Null.NullInteger)
            doReturn.NavigateUrl = "~/Reports/UserActivity.aspx";
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Ticket info = e.Row.DataItem as Ticket;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.No == "-1" ? info.No : info.No.Substring(mTrip.ID.ToString().Length + 1);
                e.Row.Cells[3].Text = info.Chairs == null ? "" : info.Chairs.Replace("f", "").Replace("m", "").Replace(";", "-").Trim('-');
                e.Row.Cells[6].Text = (info.SaleType == (int)SaleTypes.Reserve) ? "0" : info.Price.ToString(); //Show 0 For Reservs
                int num = info.GetNumTickets();
                e.Row.Cells[7].Text = num.ToString();
                e.Row.Cells[8].Text = (info.SaleType == (int)SaleTypes.Reserve) ? "0" : ((info.Price - info.Discount) * num).ToString();
                e.Row.Cells[9].Text = info.City.Title;
                e.Row.Cells[10].Text = (info.SaleType == (int)SaleTypes.Reserve) ? "رزروی" : info.Branch == null ? "" : info.Branch.Title;
                e.Row.Cells[11].Text = Helper.Instance.GetUserFullName(info.SaleUserID);
                e.Row.Cells[12].Text = Tool.ToPersianDate(Helper.Instance.GetTrip(info.TripID).Date, null);
                e.Row.Cells[13].Text = Tool.ToPersianDate(info.SaleDate, null);
                if (info.SaleDate.HasValue)
                    e.Row.Cells[14].Text = string.Format("{0}:{1}"
                        , info.SaleDate.Value.Hour.ToString().PadLeft(2, '0')
                        , info.SaleDate.Value.Minute.ToString().PadLeft(2, '0'));
                else
                    e.Row.Cells[14].Text = "";
                if (mTrip.Closed && info.PaidBack == 0)
                    e.Row.Cells[15].Text = "";
                else if (info.PaidBack != 0)
                    e.Row.Cells[15].Text = "استرداد شده";
            }
        }
    }
    protected void listHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TicketHistory info = e.Row.DataItem as TicketHistory;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Type == 0 ? "کاهش" : "افزایش";
                e.Row.Cells[3].Text = info.Ticket.No.Substring(mTrip.ID.ToString().Length + 1);
                e.Row.Cells[4].Text = info.Ticket.Fullname;
                e.Row.Cells[5].Text = info.Ticket.Tel;
                e.Row.Cells[6].Text = (info.Ticket.SaleType == (int)SaleTypes.Reserve) ? "0" : info.Ticket.Price.ToString(); //Show 0 For Reservs
                int num = (int)info.NumChairs;
                e.Row.Cells[7].Text = num.ToString();
                e.Row.Cells[8].Text = (info.Ticket.SaleType == (int)SaleTypes.Reserve) ? "0" : ((info.Ticket.Price - info.Ticket.Discount) * num).ToString();
                e.Row.Cells[9].Text = info.Ticket.City.Title;
                e.Row.Cells[10].Text = (info.Ticket.SaleType == (int)SaleTypes.Reserve) ? "رزروی" : info.Ticket.Branch == null ? "" : info.Ticket.Branch.Title;
                e.Row.Cells[11].Text = Helper.Instance.GetUserFullName(info.Ticket.FundUserID);
                e.Row.Cells[12].Text = Helper.Instance.GetUserFullName(info.SaleUserID);
            }
        }
    }

    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
    protected void doPrint_Click(object sender, EventArgs e)
    {
        //<a href='ReportSale.aspx?tripid={0}&userid={1}&repname=UserSale'>{2}</a>
        Response.Redirect(string.Format("Reports/ReportSale.aspx?tripid={0}&userid=0&repname={1}", TripID.ToString(), "UsersSale"), true);
    }
}