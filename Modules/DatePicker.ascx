﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePicker.ascx.cs" Inherits="Modules_DatePicker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:TextBox ID="mSelectedDateText" runat="server" Style="direction: ltr" Text=''
                        Width="80px" CssClass='Text'>
                    </asp:TextBox>
                    <cc1:MaskedEditExtender ID="mee1" TargetControlID="mSelectedDateText" runat="server"
                        Mask="1399/99/99" ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                </td>
                <td>
                    <asp:Button ID="doSelectDate" runat="server" Text="..." CssClass="CB" />
                </td>
            </tr>
        </table>
        <cc1:ModalPopupExtender ID="pEx" runat="server" TargetControlID="doSelectDate" PopupControlID='popupSelDatePanel'
            PopupDragHandleControlID='popupSelDateDrag' RepositionMode="RepositionOnWindowResize"
            CancelControlID='popupSelDateCancel' OkControlID='popupSelDateCancel' BackgroundCssClass='modalBackground'>
        </cc1:ModalPopupExtender>
        <asp:Panel ID="popupSelDatePanel" runat="server" Height="" Width="225px" CssClass='modalPopup'
            Style="display: none">
            <asp:Panel ID="popupSelDateDrag" runat="server" Height="20px" Width="100%" CssClass='mdlDrg'>
            </asp:Panel>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left">
                                    <asp:Button ID="movePrior" Text="قبل" runat="server" OnClick="movePrior_Click" CssClass="Button">
                                    </asp:Button>
                                </td>
                                <td>
                                    <asp:Label ID='mCurDate' runat="server" Visible="False"></asp:Label>&nbsp;
                                    <asp:DropDownList ID="mMonthList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="mMonthList_SelectedIndexChanged">
                                        <asp:ListItem Value="1">فروردین</asp:ListItem>
                                        <asp:ListItem Value="2">اردیبهشت</asp:ListItem>
                                        <asp:ListItem Value="3">خرداد</asp:ListItem>
                                        <asp:ListItem Value="4">تیر</asp:ListItem>
                                        <asp:ListItem Value="5">مرداد</asp:ListItem>
                                        <asp:ListItem Value="6">شهریور</asp:ListItem>
                                        <asp:ListItem Value="7">مهر</asp:ListItem>
                                        <asp:ListItem Value="8">آبان</asp:ListItem>
                                        <asp:ListItem Value="9">آذر</asp:ListItem>
                                        <asp:ListItem Value="10">دی</asp:ListItem>
                                        <asp:ListItem Value="11">بهمن</asp:ListItem>
                                        <asp:ListItem Value="12">اسفند</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="mYearList" runat="server" AutoPostBack="True" CssClass="DD"
                                        OnSelectedIndexChanged="mYearList_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: right">
                                    <asp:Button ID="moveNext" Text="بعد" runat="server" OnClick="moveNext_Click" CssClass="CB">
                                    </asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:PlaceHolder runat='server' ID='calendarHolder'></asp:PlaceHolder>
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px" align="right">
                       <asp:Button ID="todayDate" Text="بعد" runat="server" OnClick="todayDate_Click" CssClass="CB">
                                    </asp:Button>
                    </td>
                    <td align="left">
                        <asp:Button ID="popupSelDateCancel" runat="server" Text="انصراف" CssClass="Button" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
