﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhoneBook.ascx.cs" Inherits="Modules_PhoneBook" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
    PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
    BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
</cc1:ModalPopupExtender>
<asp:Panel Style="display: none" ID="puPanel" runat="server" Width="50px" CssClass="modalPopup">
    <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
        جستجوی دفترچه تلفن</asp:Panel>
     <table border="0" class="filter">
                <tr>
                    <td>
                        نام
                    </td>
                    <td>
                        <asp:TextBox ID="mName" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        نام خانوادگی
                    </td>
                    <td>
                        <asp:TextBox ID="mSurname" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        تلفن
                    </td>
                    <td>
                        <asp:TextBox ID="mTel" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        موبایل
                    </td>
                    <td>
                        <asp:TextBox ID="mMobile" runat="server" Width="150px"> </asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        آدرس
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="mAddress" runat="server" Width="369px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" CssClass="CB" OnClick="doSearch_Click" Text="جستجو"
                            Width="50px" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" AllowPaging="True" OnPageIndexChanging="list_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="ID" HeaderText="نام" />
                    <asp:BoundField DataField="Title" HeaderText="نام خانوادگی" />
                    <asp:BoundField DataField="Code" HeaderText="تلفن" />
                    <asp:BoundField DataField="Code" HeaderText="موبایل" />
                    <asp:BoundField DataField="Code" HeaderText="آدرس" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
</asp:Panel>
