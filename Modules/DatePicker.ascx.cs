﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

public partial class Modules_DatePicker : System.Web.UI.UserControl
{
    public event EventHandler DataSelected;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (mSelectedDateText.Text.Trim().Length > 0)
        {
            DateTime date = Tool.ParsePersianDate(mSelectedDateText.Text, new DateTime());
            if (!date.Equals(new DateTime()) && this.SelectedDate != date)
                this.SelectedDate = date;
        }
        else
        {
            string dateStr = FindPostBack();
            DateTime date = Tool.ParsePersianDate(dateStr, new DateTime());
            if (!date.Equals(new DateTime()))
                this.SelectedDate = date;
        }
        InitCalendar();

        if (mYearList.Items.Count == 0)
        {
            int year = mYear;
            mYearList.Items.Clear();
            for (int i = year - 10; i < year + 10; i++)
                mYearList.Items.Add(new ListItem(i.ToString(), i.ToString()));
            mYearList.SelectedValue = year.ToString();

            mMonthList.SelectedValue = mMonth.ToString();
        }
        RenderToHolder();

        if (Null.NullInteger != PopupX)
            pEx.X = PopupX;
        if (Null.NullInteger != PopupY)
            pEx.Y = PopupY;
    }
    private string FindPostBack()
    {
        if (Request.Form.Keys == null)
            return "";
        string id = string.Format("{0}${1}", this.ID, this.mSelectedDateText.ID);
        IEnumerator enu = Request.Form.Keys.GetEnumerator();
        if (enu != null)
            while (enu.MoveNext())
            {
                if (enu.Current != null && enu.Current.ToString().EndsWith(id))
                    return Request.Form[enu.Current.ToString()];
            }
        return "";
    }
    #region Private Fields
    /// <summary>
    /// Array of DayControls 
    /// Dimension : NumWeekShown * DaysInWeek
    /// </summary>
    private DayControl[,] mDayControls;

    /// <summary>
    /// Calendar Object Used In Date Calculation
    /// It may be any valid class derived from System.Globalization.Calendar
    /// </summary>
    private System.Globalization.Calendar mCalendar = new PersianCalendar();

    /// <summary>
    /// Direction of MonthCalendar
    /// It affects dayControl Ordering And font visualization (English vs Arabic View)
    /// </summary>
    private bool mRightToLeft;

    /// <summary>
    /// Names Of Days in week
    /// </summary>
    private DayOfWeekNames mDayNames;

    /// <summary>
    /// Today Date
    /// By Default , this class gets the System Date (DateTime.Today)
    /// </summary>
    private DateTime mTodayDate = DateTime.Today;

    /// <summary>
    /// Indicates of the TodayDay set by user or not
    /// </summary>
    private bool mTodayDateSet = false;

    /// <summary>
    /// List of Annualy Bold Dates
    /// Any AnnualyBoldDate Affects all dates with same day and month
    /// This Dates are showed as Bold (using showBold=true in DayControl.paint method)
    /// Several methods is written to maniuplate this list
    /// </summary>
    private ArrayList mAnnuallyBoldedDates;

    /// <summary>
    /// List of Monthly Bold Dates
    /// Any MonthlyBoldDate Affects all dates with same day 
    /// This Dates are showed as Bold (using showBold=true in DayControl.paint method)
    /// Several methods is written to maniuplate this list
    /// </summary>
    private ArrayList mMonthlyBoldedDates;

    /// <summary>
    /// List of Bold Dates
    /// Any BoldDate Affects all dates with same date (equal day and month and year)
    /// This Dates are showed as Bold (using showBold=true in DayControl.paint method)
    /// Several methods is written to maniuplate this list
    /// </summary>
    private ArrayList mBoldedDates;

    /// <summary>
    /// First Day Of Week
    /// By default it is saturday (for default ShamsiCalendar)
    /// </summary>
    private System.Windows.Forms.Day mFirstDayOfWeek;

    /// <summary>
    /// Show Today or not
    /// </summary>
    private bool mShowToday;

    /// <summary>
    /// Show Week Numbers Or Not
    /// First Week of a year is a week which contains first four days
    /// </summary>
    private bool mShowWeekNumbers;

    /// <summary>
    /// Num Of Days in A Week
    /// </summary>
    public static readonly int DaysInWeek = 7;

    /// <summary>
    /// Num Of Week shown
    /// </summary>
    public static readonly int NumWeeksShown = 6;

    /// <summary>
    /// Current Year/Month Showing
    /// </summary>
    //private int mYear, mMonth;
    #endregion

    private int mMonth
    {
        get
        {
            if (ViewState["SaveMonth"] != null)
                return Convert.ToInt32(ViewState["SaveMonth"]);
            //Default Year & Month
            return mMonth = mCalendar.GetMonth(mTodayDate);
        }
        set
        {
            ViewState["SaveMonth"] = value;
        }
    }
    private int mYear
    {
        get
        {
            if (ViewState["SavedYear"] != null)
                return Convert.ToInt32(ViewState["SavedYear"]);
            return mYear = mCalendar.GetYear(mTodayDate);
        }
        set
        {
            ViewState["SavedYear"] = value;
        }
    }

    /// <summary> 
    /// Required designer variable.
    /// Default Constructor:
    /// This Constructor , Initiates a new ShamsiMonthCalendar with default properties
    ///		- TodayDate : System TodayDate
    ///		- SelectionStart = SelectionEnd = TodayDate
    ///		- Font = Arial 10
    ///		- Calendar : An Instance of ShamsiCalendar
    ///		- DayNames : An Instance of DayOfWeekNames with default properties
    ///		- Visibility:
    ///				- Show Today & Today Circle
    ///				- Don't Show WeekNumbers
    /// </summary>
    private void InitCalendar()
    {
        // Constructiong mMonthCalendar
        mTodayDate = DateTime.Today;
        //if (Null.IsNull(SelectedDate))
        //    SelectedDate = DateTime.Today;

        //Constructing mCalendar ; Default As ShamsiCalendar
        // This is used during this class life to converting DateTimes
        mCalendar = new PersianCalendar();
        mRightToLeft = false;

        mFirstDayOfWeek = System.Windows.Forms.Day.Saturday;
        mDayNames = new DayOfWeekNames();

        mShowToday = true;

        //innerPadding = 10;
        mAnnuallyBoldedDates = new ArrayList();
        mMonthlyBoldedDates = new ArrayList();
        mBoldedDates = new ArrayList();

        //Create DayControls
        mDayControls = new DayControl[NumWeeksShown, DaysInWeek];
        for (int i = 0; i < NumWeeksShown; i++)
            for (int j = 0; j < DaysInWeek; j++)
                mDayControls[i, j] = new DayControl(1000, 1, 1);

        RefineDayControls();
    }

    private void RefineDayControls()
    {
        if (mDayControls == null)
            InitCalendar();
        DateTime firstDayDate = new DateTime(mYear, mMonth, 1, mCalendar);
        DayOfWeek firstDay = mCalendar.GetDayOfWeek(firstDayDate);
        int daysInMonth = mCalendar.GetDaysInMonth(mYear, mMonth);

        int start = (DayOfWeek.Sunday.GetHashCode() - ((mFirstDayOfWeek.GetHashCode() + 1) % DaysInWeek)
            + DaysInWeek + firstDay.GetHashCode()) % DaysInWeek;
        if (start == 0)
            start = 7;

        //Get Days of Previous Month
        for (int i = 0; i < start; i++)
        {
            mDayControls[0, i].Day = mCalendar.GetDayOfMonth(mCalendar.AddDays(firstDayDate, -(start - i)));
            mDayControls[0, i].Month = mCalendar.GetMonth(mCalendar.AddDays(firstDayDate, -(start - i)));
            mDayControls[0, i].Year = mCalendar.GetYear(mCalendar.AddDays(firstDayDate, -(start - i)));
            mDayControls[0, i].IsInRightMonth = false;
        }
        //Get Days of This Month
        for (int i = 0; i < daysInMonth; i++)
        {
            mDayControls[(i + start) / DaysInWeek, (i + start) % DaysInWeek].Day = i + 1;
            mDayControls[(i + start) / DaysInWeek, (i + start) % DaysInWeek].Month = mMonth;
            mDayControls[(i + start) / DaysInWeek, (i + start) % DaysInWeek].Year = mYear;
            mDayControls[(i + start) / DaysInWeek, (i + start) % DaysInWeek].IsInRightMonth = true;
        }
        //Get Days of Next Month
        for (int i = start + daysInMonth; i < 42; i++)
        {
            mDayControls[i / DaysInWeek, i % DaysInWeek].Day = (i + 1) - (start + daysInMonth);
            mDayControls[i / DaysInWeek, i % DaysInWeek].Month = mCalendar.GetMonth(mCalendar.AddMonths(firstDayDate, 1));
            mDayControls[i / DaysInWeek, i % DaysInWeek].Year = mCalendar.GetYear(mCalendar.AddMonths(firstDayDate, 1));
            mDayControls[i / DaysInWeek, i % DaysInWeek].IsInRightMonth = false;
        }
    }
    protected void DrawDayNames(System.Web.UI.HtmlTextWriter writer)
    {
        //For Mapping mFirstDayOfWeek To mDayOfWeekNames Correct Index
        //Add Following value to itterator i
        //mFirstDayOfWeek type is Day not DayOfWeek so ((mFirstDayOfWeek.GetHashCode() + 1) % DaysInWeek) converts
        //its hashCode to coressponding hashCode in DayOfWeek
        int start = ((mFirstDayOfWeek.GetHashCode() + 1) % DaysInWeek) - DayOfWeek.Sunday.GetHashCode();
        writer.Write("<tr>");
        for (int i = 0; i < DaysInWeek; i++)
            writer.Write(string.Format("<td class=DayName>{0}</td>", mDayNames[(i + start) % DaysInWeek]));
        writer.Write("</tr>");
    }
    private void AddDrawDayNames()
    {
        //For Mapping mFirstDayOfWeek To mDayOfWeekNames Correct Index
        //Add Following value to itterator i
        //mFirstDayOfWeek type is Day not DayOfWeek so ((mFirstDayOfWeek.GetHashCode() + 1) % DaysInWeek) converts
        //its hashCode to coressponding hashCode in DayOfWeek
        int start = ((mFirstDayOfWeek.GetHashCode() + 1) % DaysInWeek) - DayOfWeek.Sunday.GetHashCode();
        calendarHolder.Controls.Add(new LiteralControl("<tr>"));
        for (int i = 0; i < DaysInWeek; i++)
            calendarHolder.Controls.Add(new LiteralControl(string.Format("<td class=DayName>{0}</td>", mDayNames[(i + start) % DaysInWeek])));
        calendarHolder.Controls.Add(new LiteralControl("</tr>"));
    }

    protected bool IsDateInAnnuallyBoldDates(DateTime date)
    {
        foreach (DateTime dt in mAnnuallyBoldedDates)
            if ((dt.Month == date.Month) &&
                (dt.Day == date.Day))
                return true;
        return false;
    }

    /// <summary>
    /// Determine if a date is in MonthlyBoldDates List
    /// Check only for Day (not year nor month) members of dates
    /// </summary>
    /// <param name="date">Date to compare</param>
    /// <returns>True if the date is in the list</returns>
    protected bool IsDateInMonthlyBoldedDates(DateTime date)
    {
        foreach (DateTime dt in mMonthlyBoldedDates)
            if (dt.Day == date.Day)
                return true;
        return false;
    }

    /// <summary>
    /// Determine if a date is in BoldDates List
    /// Check for Day  and month and year  members of dates
    /// </summary>
    /// <param name="date">Date to compare</param>
    /// <returns>True if the date is in the list</returns>
    protected bool IsDateInBoldDates(DateTime date)
    {
        foreach (DateTime dt in mBoldedDates)
            if ((dt.Year == date.Year) &&
                (dt.Month == date.Month) &&
                (dt.Day == date.Day))
                return true;
        return false;
    }

    #region Public Methods
    /// <summary>
    /// Set Calendar Object Used In Date Calculation
    /// It may be any valid class derived from System.Globalization.Calendar
    /// </summary>
    /// <param name="calendar"></param>
    public void SetCalendar(System.Globalization.Calendar calendar)
    {
        if (calendar != null)
        {
            mCalendar = calendar;
            mYear = mCalendar.GetYear(new DateTime(mYear, mMonth, 1, mCalendar));
            mMonth = mCalendar.GetMonth(new DateTime(mYear, mMonth, 1, mCalendar));
            RefineDayControls();
        }
    }
    public void SetYearMonth(int year, int month)
    {
        mYear = year;
        mMonth = month;
        mMonthList.SelectedValue = mMonth.ToString();
        mYearList.Items.Clear();
        for (int i = year - 70; i < year + 70; i++)
            mYearList.Items.Add(new ListItem(i.ToString(), i.ToString()));
        mYearList.SelectedValue = year.ToString();
        RefineDayControls();
    }
    public void SetDate(DateTime date)
    {
        mTodayDate = date;
        mTodayDateSet = true;
    }

    #endregion

    #region properties
    /// <summary>
    /// Calendar Object Used In Date Calculation
    /// It may be any valid class derived from System.Globalization.Calendar
    /// </summary>
    public System.Globalization.Calendar Calendar
    {
        get
        {
            return mCalendar;
        }
        set
        {
            SetCalendar(value);
        }
    }


    /// <summary>
    /// List of Annualy Bold Dates
    /// Any AnnualyBoldDate Affects all dates with same day and month
    /// This Dates are showed as Bold (using showBold=true in DayControl.paint method)
    /// Several methods is written to maniuplate this list
    /// </summary>
    public System.DateTime[] AnnuallyBoldedDates
    {
        get
        {
            DateTime[] dates = new DateTime[mAnnuallyBoldedDates.Count];
            mAnnuallyBoldedDates.CopyTo(dates, 0);
            return dates;
        }
        set
        {
            if (null != value)
                mAnnuallyBoldedDates = new ArrayList(value);
        }
    }


    /// <summary>
    /// List of Bold Dates
    /// Any BoldDate Affects all dates with same date (equal day and month and year)
    /// This Dates are showed as Bold (using showBold=true in DayControl.paint method)
    /// Several methods is written to maniuplate this list
    /// </summary>
    public System.DateTime[] BoldedDates
    {
        get
        {
            DateTime[] dates = new DateTime[mBoldedDates.Count];
            mBoldedDates.CopyTo(dates, 0);
            return dates;
        }
        set
        {
            if (null != value)
                mBoldedDates = new ArrayList(value);
        }
    }


    /// <summary>
    /// List of Monthly Bold Dates
    /// Any MonthlyBoldDate Affects all dates with same day 
    /// This Dates are showed as Bold (using showBold=true in DayControl.paint method)
    /// Several methods is written to maniuplate this list
    /// </summary>
    public System.DateTime[] MonthlyBoldedDates
    {
        get
        {
            DateTime[] dates = new DateTime[mMonthlyBoldedDates.Count];
            mMonthlyBoldedDates.CopyTo(dates, 0);
            return dates;
        }
        set
        {
            if (null != value)
                mMonthlyBoldedDates = new ArrayList(value);
        }
    }


    /// <summary>
    /// First Day Of Week
    /// By default it is saturday (for default ShamsiCalendar)
    /// </summary>
    public System.Windows.Forms.Day FirstDayOfWeek
    {
        get
        {
            return mFirstDayOfWeek;
        }
        set
        {
            if (value != mFirstDayOfWeek)
            {
                mFirstDayOfWeek = value;
            }
        }
    }


    public System.DateTime SelectedDate
    {
        get
        {
            if (ViewState["SelectedDate"] == null)
                return Null.NullDate;
            return (DateTime)ViewState["SelectedDate"];
        }
        set
        {
            ViewState["SelectedDate"] = new DateTime(value.Year, value.Month, value.Day, 12, 0, 0, 0);
            //if (mCalendar != null && !Null.IsNull(value))
            //{
            //    mYear = mCalendar.GetYear( value);
            //    mMonth = mCalendar.GetMonth( value);
            //}

            mSelectedDateText.Text = Tool.ToPersianDate(value, "");
            if (!Null.IsNull(value))
            {
                SetYearMonth(mCalendar.GetYear(value), mCalendar.GetMonth(value));
                RenderToHolder();
            }
        }
    }


    /// <summary>
    /// Show Today or not
    /// </summary>
    public bool ShowToday
    {
        get
        {
            return mShowToday;
        }
        set
        {
            mShowToday = value;
        }
    }

    /// <summary>
    /// Show Week Numbers Or Not
    /// First Week of a year is a week which contains first four days
    /// </summary>
    public bool ShowWeekNumbers
    {
        get
        {
            return mShowWeekNumbers;
        }
        set
        {
            if (value == mShowWeekNumbers)
                return;
            mShowWeekNumbers = value;
        }
    }


    /// <summary>
    /// Today Date
    /// By Default , this class gets the System Date (DateTime.Today)
    /// </summary>
    public DateTime TodayDate
    {
        get
        {
            return mTodayDate;
        }
        set
        {
            mTodayDate = value;
            mTodayDateSet = true;
        }
    }


    /// <summary>
    /// Indicates if the date is set by the user or not
    /// </summary>
    public bool TodayDateSet
    {
        get
        {
            return mTodayDateSet;
        }
    }


    /// <summary>
    /// Direction of MonthCalendar
    /// It affects dayControl Ordering And font visualization (English vs Arabic View)
    /// </summary>
    public bool RightToLeft
    {
        get
        {
            return mRightToLeft;
        }
        set
        {
            mRightToLeft = value;
        }
    }
    #endregion

    #region Nested Classes and Structs
    private struct DayControl
    {
        private int mDay, mMonth, mYear;
        private bool mIsInRightMonth;
        public DayControl(int year, int month, int day)
        {
            if ((month < 1) || (month > 12))
                throw new Exception("Out Of Range Exception : Month Must Between 1-12");
            // Check validtation of Day For This Month
            PersianCalendar calendar = new PersianCalendar();
            if ((day < 1) || (day > calendar.GetDaysInMonth(year, month)))
                throw new Exception("Out Of Range Exception : Day Must Between 1-31");
            mYear = year;
            mMonth = month;
            mDay = day;
            mIsInRightMonth = true;
        }

        public int Year
        {
            get
            {
                return mYear;
            }
            set
            {
                mYear = value;
            }
        }
        public int Month
        {
            get
            {
                return mMonth;
            }
            set
            {
                mMonth = value;
            }
        }
        public int Day
        {
            get
            {
                return mDay;
            }
            set
            {
                mDay = value;
            }
        }
        public bool IsInRightMonth
        {
            get
            {
                return mIsInRightMonth;
            }
            set
            {
                mIsInRightMonth = value;
            }
        }

    }
    public class DayOfWeekNames
    {
        private string[] mDayNames; // Start From Sunday
        /// <summary>
        /// Constructor:
        /// This constructor , initiate a new DayOfWeekNames instance with 
        /// default names.
        /// </summary>
        public DayOfWeekNames()
        {
            //Default DayNames
            mDayNames = new String[7];
            mDayNames[0] = "1شنبه";
            mDayNames[1] = "2شنبه";
            mDayNames[2] = "3شنبه";
            mDayNames[3] = "4شنبه";
            mDayNames[4] = "5شنبه";
            mDayNames[5] = "جمعه";
            mDayNames[6] = "شنبه";
        }
        /// <summary>
        /// Constructor:
        /// This constructor , initiate a new DayOfWeekNames instance with 
        /// desired names.	
        /// </summary>
        /// <param name="sunday">Desired Name For Sunday</param>
        /// <param name="monday">Desired Name For Monday</param>
        /// <param name="tuesday">Desired Name For Tuesday</param>
        /// <param name="wednesday">Desired Name For Wednesday</param>
        /// <param name="thursday">Desired Name For Thursday</param>
        /// <param name="friday">Desired Name For Friday</param>
        /// <param name="saturday">Desired Name For Saturday</param>	
        public DayOfWeekNames(string sunday, string monday, string tuesday,
            string wednesday, string thursday, string friday, string saturday)
        {

            mDayNames = new String[7];
            mDayNames[0] = sunday;
            mDayNames[1] = monday;
            mDayNames[2] = tuesday;
            mDayNames[3] = wednesday;
            mDayNames[4] = thursday;
            mDayNames[5] = friday;
            mDayNames[6] = saturday;
        }
        /// <summary>
        /// Default Indexer:
        /// Return/Sets the Name of index-th Day , starting from sunday
        /// </summary>
        public string this[int index]
        {
            get
            {
                if ((index >= 0) && (index < 7))
                    return mDayNames[index];
                else
                    return null;
            }
            set
            {
                if ((index >= 0) && (index < 7))
                    if (null != value)
                        mDayNames[index] = value;
            }
        }
    }
    #endregion
    private void RenderToHolder()
    {
        calendarHolder.Controls.Clear();
        string dir = mRightToLeft ? "dir=ltr" : "dir=rtl";

        calendarHolder.Controls.Add(new LiteralControl(string.Format("<table border=1 cellpadding=0 cellspacing=0 {0}>", dir)));

        // ------------------
        // Write Year-Month
        mCurDate.Text = string.Format("{0}  {1}", GetMonthStr(mMonth), mYear);

        // Calculate DayControl Dimension and Paint Them
        AddDrawDayNames();

        for (int i = 0; i < NumWeeksShown; i++)
        {
            calendarHolder.Controls.Add(new LiteralControl("<tr>"));

            // If Show Week Numbers --> So Show Them
            if (mShowWeekNumbers)
            {
                DateTime firstDayInRow = new DateTime(mDayControls[i, 0].Year, mDayControls[i, 0].Month, mDayControls[i, 0].Day, mCalendar);
                int weekNum = mCalendar.GetWeekOfYear(firstDayInRow, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Saturday);
                calendarHolder.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", weekNum)));
            }
            for (int j = 0; j < DaysInWeek; j++)
            {
                DateTime dt = new DateTime(mDayControls[i, j].Year, mDayControls[i, j].Month, mDayControls[i, j].Day, mCalendar);
                #region Find CssClass
                string cssClass = "";
                if (!mDayControls[i, j].IsInRightMonth)
                    cssClass = "calOutMonth";
                else if (dt == this.SelectedDate)
                    cssClass = "calSelected";
                else if (dt == mTodayDate)
                    cssClass = "calToday";
                else if (IsDateInAnnuallyBoldDates(dt) || IsDateInMonthlyBoldedDates(dt) || IsDateInBoldDates(dt))
                    cssClass = "calBold";
                else
                    cssClass = "calNone";
                #endregion

                calendarHolder.Controls.Add(new LiteralControl("<td>"));
                if (mDayControls[i, j].IsInRightMonth)
                {
                    LinkButton button = new LinkButton();
                    button.ID = string.Format("sel_{0}_{1}_{2}", mDayControls[i, j].Year, mDayControls[i, j].Month, mDayControls[i, j].Day);
                    button.CssClass = cssClass;
                    button.Text = mDayControls[i, j].Day.ToString();
                    button.Click += new EventHandler(button_Click);
                    calendarHolder.Controls.Add(button);
                }
                else
                    calendarHolder.Controls.Add(new LiteralControl("&nbsp;"));
                calendarHolder.Controls.Add(new LiteralControl("</td>"));
            }
            calendarHolder.Controls.Add(new LiteralControl("</tr>"));
        }
        //Show Today Date
        if (mShowToday)
        {
            //calendarHolder.Controls.Add(new LiteralControl(string.Format("<tr><td colspan=8 align=center class=Title>امروز: {2}/{1}/{0}</td></tr>",
            //    mCalendar.GetYear(mTodayDate), mCalendar.GetMonth(mTodayDate), mCalendar.GetDayOfMonth(mTodayDate))));
            todayDate.Text = string.Format("امروز: {2}/{1}/{0}", mCalendar.GetYear(mTodayDate), mCalendar.GetMonth(mTodayDate), mCalendar.GetDayOfMonth(mTodayDate));
        }

        calendarHolder.Controls.Add(new LiteralControl("</table>"));
    }

    void button_Click(object sender, EventArgs e)
    {
        string id = (sender as LinkButton).ID;
        string[] str = id.Split('_');
        mSelectedDateText.Text = string.Format("{0}/{1}/{2}", str[1], str[2], str[3]);
        //PersianCalendar cal = new PersianCalendar();
        SelectedDate = mCalendar.ToDateTime(Convert.ToInt32(str[1]), Convert.ToInt32(str[2]), Convert.ToInt32(str[3]), 0, 0, 0, 0);
        RenderToHolder();

        if (null != DataSelected)
            DataSelected(this, null);
    }
    public string GetMonthStr(int month)
    {
        string Retval = "";
        switch (month)
        {
            case 1:
                Retval = "فروردين";
                break;
            case 2:
                Retval = "ارديبهشت";
                break;
            case 3:
                Retval = "خرداد";
                break;
            case 4:
                Retval = "تير";
                break;
            case 5:
                Retval = "مرداد";
                break;
            case 6:
                Retval = "شهريور";
                break;
            case 7:
                Retval = "مهر";
                break;
            case 8:
                Retval = "آبان";
                break;
            case 9:
                Retval = "آذر";
                break;
            case 10:
                Retval = "دي";
                break;
            case 11:
                Retval = "بهمن";
                break;
            case 12:
                Retval = "اسفند";
                break;

        }
        return Retval;
    }

    protected void moveNext_Click(object sender, EventArgs e)
    {
        if (mMonth == 12)
            SetYearMonth(mYear + 1, 1);
        else
            SetYearMonth(mYear, mMonth + 1);
        RenderToHolder();
        pEx.Show();
    }
    protected void movePrior_Click(object sender, EventArgs e)
    {
        if (mMonth == 1)
            SetYearMonth(mYear - 1, 12);
        else
            SetYearMonth(mYear, mMonth - 1);
        RenderToHolder();
        pEx.Show();
    }
    protected void mMonthList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int month = Tool.GetInt(mMonthList.SelectedValue, Null.NullInteger);
        if (month != Null.NullInteger && month != mMonth)
            SetYearMonth(mYear, month);
        RenderToHolder();
        pEx.Show();
    }
    protected void mYearList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int year = Tool.GetInt(mYearList.SelectedValue, Null.NullInteger);
        if (year != Null.NullInteger && year != mYear)
            SetYearMonth(year, mMonth);
        RenderToHolder();
        pEx.Show();
    }
    protected void todayDate_Click(object sender, EventArgs e)
    {
        this.SelectedDate = DateTime.Now;
    }
    public bool Enabled
    {
        get
        {
            return mSelectedDateText.Enabled;
        }
        set
        {
            mSelectedDateText.Enabled = value;
            pEx.Enabled = value;
            popupSelDatePanel.Visible = value;
            doSelectDate.Visible = value;
        }
    }
    public int PopupX
    {
        get
        {
            return Tool.GetInt(ViewState["PopupX"], Null.NullInteger);
        }
        set
        {
            ViewState["PopupX"] = value;
            if (null != pEx)
                pEx.X = value;
        }
    }
    public int PopupY
    {
        get
        {
            return Tool.GetInt(ViewState["PopupY"], Null.NullInteger);
        }
        set
        {
            ViewState["PopupY"] = value;
            if (null != pEx)
                pEx.Y = value;
        }
    }
}
