﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageBox.ascx.cs" Inherits="Modules_MessageBox" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="popupMsgExtender" runat="server" BackgroundCssClass="mdlBck"
    CancelControlID="popupMsgCancel" OkControlID="popupMsgCancel" PopupControlID="popupMsgPanel"
    PopupDragHandleControlID="popupMsgDrag" TargetControlID="popupMsgDrag">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="popupMsgPanel" runat="server" CssClass="modalPopup" Style="display: none"
    Width="425px">
    <asp:Panel ID="popupMsgDrag" runat="server" CssClass="modalPopupDrag" Height="20px"
        Width="100%">
        <asp:Label ID="popupMsgTitle" runat="server" Text="Label"></asp:Label></asp:Panel>
    <table border="0" width="100%">
        <tr>
            <td>
                <asp:Label ID="popupMsgMessage" runat="server" Text="..." CssClass="V"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Button ID="popupMsgCancel" runat="server" Text="بستن پنجره" CssClass="CB" Width="70px" />
                <asp:Button ID="popupMsgOk" runat="server" CssClass="CB" OnClick="popupMsgOk_Click"
                    Text="تاييد" Visible="False" Width="70px" /></td>
        </tr>
    </table>
</asp:Panel>