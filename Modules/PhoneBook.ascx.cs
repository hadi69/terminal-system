﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Modules_PhoneBook : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
   
    private void BindData()
    {
        var all = from c in Helper.Instance.DB.TelEntries
                  select c;

        if (mName.Text.Trim().Length > 0)
            all = all.Where(c => c.Name.Contains(mName.Text.Trim()));
        if (mSurname.Text.Trim().Length > 0)
            all = all.Where(c => c.Surname.Contains(mSurname.Text.Trim()));
        if (mTel.Text.Trim().Length > 0)
            all = all.Where(c => c.Tel.Contains(mTel.Text.Trim()));
        if (mMobile.Text.Trim().Length > 0)
            all = all.Where(c => c.Mobile.Contains(mMobile.Text.Trim()));
        if (mAddress.Text.Trim().Length > 0)
            all = all.Where(c => c.Address.Contains(mAddress.Text.Trim()));
        list.DataSource = all;
        list.DataBind();
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TelEntry info = e.Row.DataItem as TelEntry;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        list.PageIndex = 0;
        BindData();
    }
  
    protected void list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        list.PageIndex = e.NewPageIndex;
        BindData();
    }
    public void Show()
    {
        puEx.Show();
    }
    public string TargetControlID
    {
        get
        {
            return puEx.TargetControlID;
        }
        set
        {
            puEx.TargetControlID = value;
        }
    }
}
