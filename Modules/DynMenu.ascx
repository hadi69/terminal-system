﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DynMenu.ascx.cs" Inherits="Modules_DynMenu" %>
<style type="text/css">

#dropmenudiv{
position:absolute;
border:1px solid black;
border-bottom-width: 0;
font:normal 12px Verdana;
line-height:18px;
z-index:100;
color:white;
}

#dropmenudiv a{
width: 100%;
display: block;
text-indent: 3px;
border-bottom: 1px solid black;
padding: 1px 0;
text-decoration: none;
font-weight: bold;
color:white;
}

#dropmenudiv a:hover{ /*hover background color*/
background-color: #003366;
color:yellow;
}
#dropmenudiv a:visited{
color:white;
}

</style>

<script type="text/javascript">
/***********************************************
* AnyLink Drop Down Menu- © Dynamic Drive (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

//Contents for menu 1
//var menu1=new Array()
//menu1[0]='<a href="http://www.javascriptkit.com">JavaScript Kit</a>'
//menu1[1]='<a href="http://www.freewarejava.com">Freewarejava.com</a>'
//menu1[2]='<a href="http://codingforums.com">Coding Forums</a>'
//menu1[3]='<a href="http://www.cssdrive.com">CSS Drive</a>'

////Contents for menu 2, and so on
//var menu2=new Array()
//menu2[0]='<a href="http://cnn.com">CNN</a>'
//menu2[1]='<a href="http://msnbc.com">MSNBC</a>'
//menu2[2]='<a href="http://news.bbc.co.uk">BBC News</a>'
		
var menuwidth='165px'; //default menu width
var menubgcolor= '#315599';//'lightyellow'  //menu bgcolor
var disappeardelay=250;  //menu disappear speed onMouseout (in miliseconds)
var hidemenu_onclick="yes"; //hide menu when user clicks within menu?

/////No further editting needed

var ie4=document.all
var ns6=document.getElementById&&!document.all

if (ie4||ns6)
    document.write('<div id="dropmenudiv" style="visibility:hidden;width:'+menuwidth+';background-color:'+menubgcolor+'" onMouseover="clearhidemenu()" onMouseout="dynamichide(event)"></div>')

function getposOffset(what, offsettype){
    var totaloffset=(offsettype=="left")? what.offsetLeft - 80: what.offsetTop;
    var parentEl=what.offsetParent;
    while (parentEl!=null){
        totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
        parentEl=parentEl.offsetParent;
    }
    return totaloffset;
}


function showhide(obj, e, visible, hidden, menuwidth){
    if (ie4||ns6)
    dropmenuobj.style.left=dropmenuobj.style.top="-500px"
    if (menuwidth!=""){
        dropmenuobj.widthobj=dropmenuobj.style
        dropmenuobj.widthobj.width=menuwidth
    }
    if (e.type=="click" && obj.visibility==hidden || e.type=="mouseover")
        obj.visibility=visible
    else if (e.type=="click")
        obj.visibility=hidden
}

function iecompattest(){
    return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function clearbrowseredge(obj, whichedge){
    var edgeoffset=0
    if (whichedge=="rightedge"){
        var windowedge=ie4 && !window.opera? iecompattest().scrollLeft+iecompattest().clientWidth-15 : window.pageXOffset+window.innerWidth-15
        dropmenuobj.contentmeasure=dropmenuobj.offsetWidth
        if (windowedge-dropmenuobj.x < dropmenuobj.contentmeasure)
        edgeoffset=dropmenuobj.contentmeasure-obj.offsetWidth
    }
    else{
        var topedge=ie4 && !window.opera? iecompattest().scrollTop : window.pageYOffset
        var windowedge=ie4 && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
        dropmenuobj.contentmeasure=dropmenuobj.offsetHeight
        if (windowedge-dropmenuobj.y < dropmenuobj.contentmeasure){ //move up?
        edgeoffset=dropmenuobj.contentmeasure+obj.offsetHeight
        if ((dropmenuobj.y-topedge)<dropmenuobj.contentmeasure) //up no good either?
        edgeoffset=dropmenuobj.y+obj.offsetHeight-topedge
}
}
return edgeoffset
}

function populatemenu(what){
    if (ie4||ns6)
    dropmenuobj.innerHTML=what.join("")
}


function dropdownmenu(obj, e, menucontents, menuwidth){
    if (window.event) event.cancelBubble=true
    else if (e.stopPropagation) e.stopPropagation()
      clearhidemenu()
    dropmenuobj=document.getElementById? document.getElementById("dropmenudiv") : dropmenudiv
    populatemenu(menucontents)

    if (ie4||ns6){
        showhide(dropmenuobj.style, e, "visible", "hidden", menuwidth)
        dropmenuobj.x=getposOffset(obj, "left")
        dropmenuobj.y=getposOffset(obj, "top")
        dropmenuobj.style.left=dropmenuobj.x-clearbrowseredge(obj, "rightedge")+"px"
        dropmenuobj.style.top=dropmenuobj.y-clearbrowseredge(obj, "bottomedge")+obj.offsetHeight+"px"
    }

    return clickreturnvalue()
}

function clickreturnvalue(){
    if (ie4||ns6) return false
    else return true
}

function contains_ns6(a, b) {
    while (b.parentNode)
        if ((b = b.parentNode) == a)
           return true;
    return false;
}

function dynamichide(e){
    if (ie4&&!dropmenuobj.contains(e.toElement))
      delayhidemenu()
    else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
      delayhidemenu()
}

function hidemenu(e){
    if (typeof dropmenuobj!="undefined"){
        if (ie4||ns6)
        dropmenuobj.style.visibility="hidden"
    }
}

function delayhidemenu(){
    if (ie4||ns6)
     delayhide=setTimeout("hidemenu()",disappeardelay)
}

function clearhidemenu(){
    if (typeof delayhide!="undefined")
       clearTimeout(delayhide)
}

if (hidemenu_onclick=="yes")
    document.onclick=hidemenu

</script>
<style type="text/css"> table { font-family:Verdana, Helvetica, sans-serif; font-size:20px; color:#040404; text-decoration:none; text-transform:none; }
	#menu_DE { position:absolute; top: 15px; left: -25px; z-index:100; width:666px; }
	#menu_DE dd { height:25px; display: none; position:absolute; left:50px; top:20px; width:666px; padding-top:1px; }
	#menu_DEq dd a { margin-top:5px; color:red; font-weight:bold; margin-right:5px; }
	#menus_DE dd a:hover { color:blue; text-decoration:none; }
	#menu_DE li { display:inline; color:#a4acb1; margin:0; }
	#menu_DE dl { float:left; display:inline; margin:0; margin-left:40px; }
	#menu_DE dt a { display:block; height: 20px; border:none; background-repeat:no-repeat; background-position:0 0; }
	.MenuTable {direction:rtl; background-color:#FF3300;border-width:0px;width:100%;background-image: url(/images/MainMenuBack.gif)}
	.MainMenuTable {color:white;font-family:tahoma;text-decoration:none;direction:rtl; height:25px}
	.MainMenuSelected {color:black;}
	.MainMenuSelected:visited {color:black;}
	.MyMenu { color:white; font-family:tahoma; background-image: url(/image/home_g.gif);font-weight:bold ;margin-right:5px;margin-left:5px;text-decoration:none}
	.MyMenu:hover { color:black; text-decoration:none}
	.MyMenu:visited { color:white}
	.MyMenu:visited:hover { color:#FFAA00}
	.MySubMenu { color:black; font-family:tahoma;text-decoration:none;z-index:101; }
	.MySubMenu:visited { color:black}
	.MySubMenu:hover { color:blue; }
	.MySubMenuSelected { color:blue; font-family:tahoma;text-decoration:none; }
	.MySubMenuSelected :hover{ color:red; font-family:tahoma;text-decoration:none; }
	.MySubMenuSelected:visited { color:blue; font-family:tahoma;text-decoration:none; }
	.MySubMenuSelected:visited:hover{ color:red; font-family:tahoma;text-decoration:none; }
	.MySubMenuTD {background-color:#FFFF99;color:white;direction:rtl;text-align:right;height:20px}
	.MySubMenu:hover { color:red; font-family:tahoma; text-decoration:none; }
</style>
