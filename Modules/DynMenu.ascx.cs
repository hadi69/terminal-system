﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;

public partial class Modules_DynMenu : System.Web.UI.UserControl
{

    private void Page_Load(object sender, System.EventArgs e)
    {
        // Put user code to initialize the page here
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);

    }
    #endregion

    private class MenuItem
    {
        public string Title, Url, CssClass, SubStyle, Style, Separator, Access,SoftwareType;
        public bool ShowChildsAsMenu = false;
        //public int[] GroupIDs;
        public MenuItem()
        {
        }
        public string GetCssClass()
        {
            if (CssClass == null || CssClass.Trim().Length == 0)
                return "";
            return string.Concat("class='", CssClass, "'");
        }
        public string GetStyle()
        {
            if (Style == null || Style.Trim().Length == 0)
                return "";
            return string.Concat("style='{", Style, "}'");
        }
        public string GetSubStyle()
        {
            if (SubStyle == null || SubStyle.Trim().Length == 0)
                return "";
            return string.Concat("style='{", SubStyle, "}'");
        }
    }
    private MenuItem GetItem(XmlNode node)
    {
        MenuItem item = new MenuItem();
        item.Title = (node.Attributes["title"] != null) ? node.Attributes["title"].Value : "";
        item.Url = (node.Attributes["url"] != null) ? node.Attributes["url"].Value : "";
        item.Style = (node.Attributes["style"] != null) ? node.Attributes["style"].Value : "";
        item.CssClass = node.Attributes["cssclass"] != null ? node.Attributes["cssclass"].Value : "";
        item.SubStyle = node.Attributes["substyle"] != null ? node.Attributes["substyle"].Value : "";
        item.Separator = (node.Attributes["separator"] != null) ? node.Attributes["separator"].Value : "";
        item.Access = (node.Attributes["Access"] != null) ? node.Attributes["Access"].Value : "";
        item.SoftwareType = (node.Attributes["SoftType"] != null) ? node.Attributes["SoftType"].Value : "";
        item.ShowChildsAsMenu = (node.Attributes["ShowChildsAsMenu"] != null) ? Convert.ToBoolean(node.Attributes["ShowChildsAsMenu"].Value) : false;
        return item;
    }

    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
        base.Render(writer);
        // ------------------------------------------------------
        // Start of Menu
        writer.WriteLine("<table class='MenuTable' cellpadding=0 cellspacing=0><tr><td>");
        writer.WriteLine("	<table class='MainMenuTable'><tr>");
        // ------------------------------------------------------
        // Menu
        XmlDocument doc = this.MenuDefinition;
        XmlNode mainMenus = doc.DocumentElement.SelectSingleNode("MenuItems");
        int selectedMainMenuIndex = -1;
        for (int i = 0; i < mainMenus.ChildNodes.Count; i++)
        {
            Uri url = Request.Url;
            MenuItem item = GetItem(mainMenus.ChildNodes[i]);
            if (item.Url.ToLower().EndsWith("sale2.aspx"))
                if (!Tool.CanMini() && !Tool.CanSewari())
                    continue;
            bool hasAtLeastOneAccess = false;
            bool hasAccessToDefaultUrl = true;
            // -------------------------------------
            // Check User Access
            #region Check UserAccess
            if (mainMenus.ChildNodes[i].ChildNodes.Count == 0)
            {
                if (!string.IsNullOrEmpty(item.Access))
                    if (!UserDataEngine.UserHasAccess(item.SoftwareType, item.Access))
                        continue;
                hasAtLeastOneAccess = true;
            }
            else
                for (int j = 0; j < mainMenus.ChildNodes[i].ChildNodes.Count; j++)
                {
                    MenuItem subMenu = GetItem(mainMenus.ChildNodes[i].ChildNodes[j]);
                    bool hasAccess = true;
                    if (!string.IsNullOrEmpty(subMenu.Access))
                        hasAccess = UserDataEngine.UserHasAccess(subMenu.SoftwareType, subMenu.Access);
                    hasAtLeastOneAccess = hasAtLeastOneAccess || hasAccess;
                    if (!hasAccess)
                    {
                        if (subMenu.Url == item.Url)
                            hasAccessToDefaultUrl = false;
                        // if this page is requested : dont allow access
                        //if (url.AbsoluteUri.ToLower().IndexOf(subMenu.Url.ToLower()) >= 0)
                        //    Response.Redirect(SiteSettings.FirstPage);
                    }
                }
            if (!hasAtLeastOneAccess)
            {
                // if this page is requested : dont allow access
                if (url.AbsoluteUri.ToLower().IndexOf(item.Url.ToLower()) >= 0)
                    Response.Redirect(SiteSettings.FirstPage);
                continue;
            }
            if (!hasAccessToDefaultUrl)
                for (int j = 0; j < mainMenus.ChildNodes[i].ChildNodes.Count; j++)
                {
                    MenuItem subMenu = GetItem(mainMenus.ChildNodes[i].ChildNodes[j]);
                    if (string.IsNullOrEmpty(subMenu.Access) || UserDataEngine.UserHasAccess(subMenu.SoftwareType,subMenu.Access))
                        if (subMenu.Url != item.Url)
                        {
                            item.Url = subMenu.Url;
                            break;
                        }
                }
            #endregion
            if (selectedMainMenuIndex == -1)
            {
                #region Check if this is the SelectedMainMenu
                if (url.AbsoluteUri.ToLower().IndexOf(item.Url.ToLower()) >= 0)
                    selectedMainMenuIndex = i;
                else
                    for (int j = 0; j < mainMenus.ChildNodes[i].ChildNodes.Count; j++)
                    {
                        MenuItem subMenu = GetItem(mainMenus.ChildNodes[i].ChildNodes[j]);
                        if (url.AbsoluteUri.ToLower().IndexOf(subMenu.Url.ToLower()) >= 0)
                        {
                            selectedMainMenuIndex = i;
                            break;
                        }
                        for (int k = 0; k < mainMenus.ChildNodes[i].ChildNodes[j].ChildNodes.Count; k++)
                        {
                            MenuItem ChildItem = GetItem(mainMenus.ChildNodes[i].ChildNodes[j].ChildNodes[k]);
                            if (url.AbsoluteUri.ToLower().IndexOf(ChildItem.Url.ToLower()) >= 0)
                            {
                                selectedMainMenuIndex = i;
                                break;
                            }
                        }
                    }
                #endregion
            }
            if (selectedMainMenuIndex == i)
                writer.WriteLine("<td class='MainMenuSelected'>");
            else
                writer.WriteLine("<td>");
            writer.WriteLine(string.Format("	<a id='menuItem{0}' href='{1}' style='' {2} {3}>{4}</a></td>", i, item.Url, selectedMainMenuIndex == i ? " class='MainMenuSelected'" : item.GetCssClass(), item.GetStyle(), item.Title));
            writer.WriteLine("</td>");
        }
        // ------------------------------------------------------
        // End of Menu
        StringBuilder dropDownMenus = new StringBuilder();

        writer.WriteLine("	</tr></table>");
        writer.WriteLine("</td></tr>");
        #region SubMenus
        if (selectedMainMenuIndex != -1)
        {
            XmlNode selectedMainMenu = mainMenus.ChildNodes[selectedMainMenuIndex];
            for (int j = 0; j < selectedMainMenu.ChildNodes.Count; j++)
            {
                if (j == 0)
                    writer.WriteLine("<tr><td class='MySubMenuTD'>");
                MenuItem subMenu = GetItem(selectedMainMenu.ChildNodes[j]);
                // -------------------------------------
                // Check User Access
                if (!string.IsNullOrEmpty(subMenu.Access))
                    if (!UserDataEngine.UserHasAccess(subMenu.SoftwareType, subMenu.Access))
                        continue;
                if (subMenu.ShowChildsAsMenu)
                {
                    dropDownMenus.AppendFormat(" var menu{0}=new Array();\n", j);
                    for (int k = 0; k < selectedMainMenu.ChildNodes[j].ChildNodes.Count; k++)
                    {
                        MenuItem childItem = GetItem(selectedMainMenu.ChildNodes[j].ChildNodes[k]);
                        dropDownMenus.AppendFormat(" menu{0}[{1}]='<a href=\"{2}\">{3}</a>';\n", j, k, childItem.Url, childItem.Title);
                    }
                }
                bool isSelectedSubMenu = Request.Url.AbsoluteUri.ToLower().IndexOf(subMenu.Url.ToLower()) >= 0;
                if (!isSelectedSubMenu)
                    #region See if we are in one of its ChildItems
                    for (int k = 0; k < selectedMainMenu.ChildNodes[j].ChildNodes.Count; k++)
                    {
                        MenuItem ChildItem = GetItem(selectedMainMenu.ChildNodes[j].ChildNodes[k]);
                        if (Request.Url.AbsoluteUri.ToLower().IndexOf(ChildItem.Url.ToLower()) >= 0)
                        {
                            isSelectedSubMenu = true;
                            break;
                        }
                    }
                    #endregion
                string cssClassAtrib = isSelectedSubMenu ? "class='MySubMenuSelected'" : subMenu.GetCssClass();
                if (subMenu.ShowChildsAsMenu)
                    writer.WriteLine(string.Format("<a href='{0}' {1} {2} onclick=\"return dropdownmenu(this, event, menu{3}, '150px')\" onmouseout=\"delayhidemenu()\">{4}</a>{5}"
                    , subMenu.Url, subMenu.GetStyle(), cssClassAtrib, j, subMenu.Title, j < selectedMainMenu.ChildNodes.Count - 1 ? GetItem(selectedMainMenu).Separator : ""));
                else
                    writer.WriteLine(string.Format("	<a href='{0}' {1} {2}>{3}</a>{4}", subMenu.Url, subMenu.GetStyle(), cssClassAtrib, subMenu.Title, j < selectedMainMenu.ChildNodes.Count - 1 ? GetItem(selectedMainMenu).Separator : ""));
                if (j == selectedMainMenu.ChildNodes.Count - 1)
                    writer.WriteLine("</td></tr>");
            }
        }
        #endregion
        writer.WriteLine("</table>");
        if (dropDownMenus.Length > 0)
        {
            writer.WriteLine("<script type='text/javascript'>");
            writer.WriteLine(dropDownMenus.ToString());
            writer.WriteLine("</script>");
        }
    }
    public string MenuDefinitionFile
    {
        get
        {
            return Tool.GetString(ViewState["MenuDefinitionFile"], "MenuDefinition.Xml");
        }
        set
        {
            if (value != MenuDefinitionFile)
            {
                ViewState["MenuDefinitionFile"] = value;
                MenuDefinition = null;
            }
        }
    }
    private XmlDocument MenuDefinition
    {
        get
        {
            if (ViewState["MenuDefinition"] == null)
            {
                string menuDefinitionPath = System.IO.Path.Combine(Server.MapPath("~/"), MenuDefinitionFile);
                if (System.IO.File.Exists(menuDefinitionPath))
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(menuDefinitionPath);
                        return (ViewState["MenuDefinition"] = doc) as XmlDocument;
                    }
                    catch { }
                return null;
            }
            return ViewState["MenuDefinition"] as XmlDocument;
        }
        set
        {
            ViewState["MenuDefinition"] = value;
        }
    }
}
