﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Inquiry : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        //if (!IsPostBack)
        //    this.TicketID = Tool.GetInt(Request.QueryString["ticketid"], Null.NullInteger);
        if (!IsPostBack)
        {
            sDateStart.SelectedDate = DateTime.Now.AddDays(-3);
            sDateEnd.SelectedDate = DateTime.Now.AddDays(3);
        }
    }
    protected int TicketID
    {
        get
        {
            return Tool.GetInt(ViewState["TicketID"], Null.NullInteger);
        }
        set
        {
            ViewState["TicketID"] = value;
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Ticket info = e.Row.DataItem as Ticket;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
               // e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                int num = info.GetNumTickets();
                e.Row.Cells[5].Text = num.ToString();
                e.Row.Cells[6].Text = (info.Price * num).ToString();
                e.Row.Cells[7].Text = info.City.Title;
                e.Row.Cells[8].Text = info.Trip.DepartureTime2;
                e.Row.Cells[9].Text = Helper.Instance.GetDriverFullName(info.Trip.DriverID1);
                e.Row.Cells[10].Text = info.Trip.No.ToString();
                e.Row.Cells[11].Text = info.Trip.Closed ? "بسته" : "باز";
                e.Row.Cells[12].Text = Tool.ToPersianDate(info.Trip.Date, "");
                if (info.Trip.Closed)
                    e.Row.Cells[13].Text = "";
                else if (info.PaidBack != 0)
                    e.Row.Cells[13].Text = "استرداد شده";
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {

        var all = from c in Helper.Instance.DB.Tickets
                  where c.No.Contains(sNo.Text) && c.PaidBack == 0
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
            all = all.Where(c => c.Trip.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            all = all.Where(c => c.Trip.Date <= toDate);
        try
        {
            list.DataSource = all;
        }
        catch
        {
            list.DataSource = Helper.Instance.GetTickets(sNo.Text);
        }
        list.DataBind();        
    }
    protected void doSearch2_Click(object sender, EventArgs e)
    {
        var all = from c in Helper.Instance.DB.Tickets
                  where c.Fullname.Contains(sFullname.Text)
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
            all = all.Where(c => c.Trip.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            all = all.Where(c => c.Trip.Date <= toDate);
        try
        {
            list.DataSource = all;
        }
        catch
        {
            list.DataSource = Helper.Instance.GetTicketsByFullname(sFullname.Text);
        }
        list.DataBind();
    }
}
