﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mUserError.Visible = false;
        mDBError.Visible = false;
        mLockError.Visible = false;
        if (!IsPostBack)
        {
            if (Request.QueryString["do"] == "logout")
            {
                UserDataEngine.Logout();
            }

            mCompanyName.Text = Helper.Instance.GetSettingValue("CompanyName");
            BindAccess();
        }

    }
    private void BindAccess()
    {
        User info = SiteSettings.User;
        if (info == null)
        {
            logined.Visible = false;
            notLogined.Visible = true;
            if (userName.Text.Length > 0 && null != Helper.Instance.GetUser(userName.Text))
                JsTools.SetFocus(password);
            else
                JsTools.SetFocus(userName);
        }
        else
        {
            logined.Visible = true;
            notLogined.Visible = false;
            userDisplay.Text = info.FullName;
            Helper.Instance.DeleteOnlineUser(info.ID);
            Helper.Instance.SetUserOnline(userName.Text,info.ID);
            
        }
        (this.Master as MasterPage).ShowMenu();
    }
    //static TINYLib.TinyClass tiny;
    protected void doLogin_Click(object sender, EventArgs e)
    {
        try
        {
            if (!UserDataEngine.Login(userName.Text, password.Text))
                mUserError.Visible = true;
            else
            {
                if (Helper.Instance.GetSettingValue("CompanyUser") == "")
                {
                    try
                    {
                        Helper.Instance.UpdateSetting("CompanyUser", "mohsen");
                        Helper.Instance.UpdateSetting("CompanyPassword", "mohsen");
                    }
                    catch { }
                }
                User adminUser=new User();
                if (Helper.Instance.GetUser(Helper.Instance.GetSettingValue("CompanyUser")) == null)
                {
                        adminUser.UserName = "mohsen";
                        adminUser.Password = "mohsen";
                        adminUser.FullName = "کاربر اصلی";
                        adminUser.RoleID = 1;
                        adminUser.IsAdmin = true;
                        Helper.Instance.DB.Users.InsertOnSubmit(adminUser);
                        Helper.Instance.Update();
                }
                BindAccess();
                return;
                #region Check Lock
                #endregion
            }
        }
        catch
        {
            mDBError.Visible = true;
        }
        BindAccess();
    }
    
}
