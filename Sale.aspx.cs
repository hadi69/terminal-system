﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using SMSService;

public partial class Sale : System.Web.UI.Page
{
    Trip mTrip = null;
    int TicketID = -1;
    bool ShowPayback = false;
    bool Simple = false;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        (this.Master as MasterPage).BindHotKeys = false;
        mError.Text = "";
        mMsg.Text = mMsg2.Text = "";
        if (!IsPostBack)
        {
            this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);

            mTrip = Helper.Instance.GetTrip(this.TripID);
            if (mTrip == null)
                Response.Redirect("Trips.aspx", true);
            if (mTrip.IsMini)
                if (Tool.CanMini() || Tool.CanSewari())
                    if (Tool.GetBool(Helper.Instance.GetSettingValue("MiniAsBus"), false) == false)
                        Response.Redirect("sale2.aspx?tripid=" + this.TripID, true);
            mTripTitle.Text = string.Format("<span class='PathTitle'>{0}</span> ({1}) ({2} - {3}) شماره: {4}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDateRtl(mTrip.Date, ""), mTrip.Service.DepartureTime, mTrip.ID);
            doClose.NavigateUrl = "Close.aspx?tripid=" + this.TripID;
            doRefresh.NavigateUrl = "sale.aspx?tripid=" + this.TripID;
            mExpireDate.SelectedDate = mTrip.Date;
            mExpireTime.Text = Tool.FixTime(mTrip.Service.DepartureTime);
            SetChangeMode(false);

            doAutoFill.Visible = doAutoFill.Enabled = Tool.GetBool(Helper.Instance.GetSettingValue("AutoFill"), false);

            #region Pre
            Trip pre = Helper.Instance.GetPreTrip(mTrip);
            if (pre == null || pre.ID == mTrip.ID || pre.Closed)
            {
                doShowPre.Text = "قبلی";
                doShowPre.NavigateUrl = "";
                doShowPre.Enabled = false;
            }
            else
            {
                doShowPre.Text = string.Format("قبلی({0})", string.Format("{0} ({1}) ({2} - {3})", pre.Service.Path.Title, pre.Service.ServiceType2, Tool.ToPersianDateRtl(pre.Date, ""), pre.Service.DepartureTime, pre.ID));
                doShowPre.NavigateUrl = "Sale.aspx?tripid=" + pre.ID;
                doShowPre.Enabled = true;
            }
            #endregion
            #region Next
            Trip next = Helper.Instance.GetNextTrip(mTrip);
            if (next == null || next.ID == mTrip.ID || next.Closed)
            {
                doShowNext.Text = "بعدی";
                doShowNext.NavigateUrl = "";
                doShowNext.Enabled = false;
            }
            else
            {
                doShowNext.Text = string.Format("بعدی({0})", string.Format("{0} ({1}) ({2} - {3})", next.Service.Path.Title, next.Service.ServiceType2, Tool.ToPersianDateRtl(next.Date, ""), next.Service.DepartureTime, next.ID));
                doShowNext.NavigateUrl = "Sale.aspx?tripid=" + next.ID;
                doShowNext.Enabled = true;
            }
            #endregion

            if (Tool.ToPersianDate(DateTime.Now, "") != Tool.ToPersianDate(mTrip.Date, ""))
                mFilter.BgColor = "#AAF0F0";
            else
                mFilter.BgColor = "#dcdcdc";
        }
        else
            mTrip = Helper.Instance.GetTrip(TripID);
        ShowPayback = Request.QueryString["do"] == "payback";
        doReturn.NavigateUrl = "sale.aspx?tripid=" + this.TripID;
        if (!IsPostBack || ShowPayback)
        {
            BindInitData();
            BindData(true);
        }

        //BindHotKeys();
        JsTools.SetFocus(mName);

        if (!Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserve"), true))
        {
            doReserve.Enabled = doCancel.Enabled = doCancelAll.Enabled = doConfirmReserve.Enabled = false;
            doReserve.CssClass = doCancel.CssClass = doCancelAll.CssClass = doConfirmReserve.CssClass = "CBBD";
        }
        hiddentripID.Value = this.TripID.ToString();

    }
    void BindHotKeys(string firsStr)
    {
        List<string> clientIDs = new List<string>();
        List<int> keyCodes = new List<int>();
        clientIDs.Add(doSale.ClientID);
        clientIDs.Add(doClose.ClientID);
        clientIDs.Add(doShowCustomer.ClientID);
        keyCodes.Add(113);
        keyCodes.Add(115);
        keyCodes.Add(123);

        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function keyPressHand(event) { \n");
        builder.Append("    var e = event;\n");
        builder.Append("    if (!window.getSelection) e = window.event;\n");
        builder.Append("    key = e.keyCode;\n");
        builder.Append("    if (key == 119)\n");
        builder.Append("        window.location = 'QQTrips.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        if (Tool.CanMini() || Tool.CanSewari())
        {
            builder.Append("    if (key == 117)\n");
            builder.Append("        window.location = 'QQSale2.aspx';\n".Replace("QQ", SiteSettings.GetValue("BaseUrl")));
        }
        //builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.click();\n");
            builder.AppendFormat("window.event.keyCode=0;\n");
            builder.AppendFormat("break;\n");
        }
        // ----------------------------
        builder.AppendFormat("case {0}:\n", 120);
        builder.AppendFormat("var b = $get('{0}');\n", mName.ClientID);
        builder.AppendFormat("b.focus();\n");
        builder.AppendFormat("window.event.keyCode=0;\n");
        builder.AppendFormat("break;\n");
        // ----------------------------
        builder.AppendFormat("case {0}:\n", 118);
        builder.AppendFormat("var b = $get('{0}');\n", firsStr);
        builder.AppendFormat("b.focus();\n");
        builder.AppendFormat("window.event.keyCode=0;\n");
        builder.AppendFormat("break;\n");

        builder.Append("    }\n");
        builder.Append("return;\n");
        builder.Append("}\n");
        builder.Append("document.onkeydown=keyPressHand;\n");
        builder.Append("    </script>\n");
        //Page.RegisterStartupScript(Guid.NewGuid().ToString(), builder.ToString());
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "HotKeyScript", builder.ToString());
        //if (!Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "NMY0"))
        //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "NMY0", builder.ToString());
        //else if (!Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "NiceToMeetYou"))
        //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "NMY1", builder.ToString());
        //else
        //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "NMY2", builder.ToString());
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), builder.ToString(), true);
    }
    private void BindInitData()
    {
        try
        {
            List<City> all = Helper.Instance.GetCities(mTrip.Service);
            mCityID.DataValueField = "ID";
            mCityID.DataTextField = "Title";
            mCityID.DataSource = all;
            mCityID.DataBind();
            if (mCityID.Items.Count > 0)
            {
                mCityID.SelectedIndex = mCityID.Items.Count - 1;
                mCityID_SelectedIndexChanged(null, null);
            }
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        try
        {
            List<Fund> all = Helper.Instance.GetFunds();
            if (SiteSettings.User != null && SiteSettings.User.IsAdmin != true)
            {
                all = new List<Fund>();
                Fund f = Helper.Instance.GetFund(SiteSettings.User.FundID);
                if (f != null)
                    all.Add(f);
            }
            mFundID.DataValueField = "ID";
            mFundID.DataTextField = "Title";
            mFundID.DataSource = all;
            mFundID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        try
        {
            List<Branch> all = Helper.Instance.GetBranches();
            if (SiteSettings.User != null && SiteSettings.User.IsAdmin != true)
            {
                all = new List<Branch>();
                Branch f = SiteSettings.User.BranchID.HasValue ? Helper.Instance.GetBranch(SiteSettings.User.BranchID.Value) : null;
                if (f != null)
                    all.Add(f);
            }
            mBranchID.DataValueField = "ID";
            mBranchID.DataTextField = "Title";
            mBranchID.DataSource = all;
            mBranchID.DataBind();
        }
        catch (Exception ex)
        {
            mError.Text += "--" + ex.Message + "<br />";
        }
        {
            mTitle.Items.Clear();
            mTitle.Items.Add("آقا");
            mTitle.Items.Add("خانم");
            //<asp:ListItem Text="خانم" Value="0" />
        }

    }
    private void BindData(bool bindCombos)
    {
        BindData(bindCombos, false, false);
    }
    private void BindData(bool bindCombos, bool selectAll, bool deselectAll)
    {
        User webUser = Helper.Instance.GetUser("web");
        User postbankUser = Helper.Instance.GetUser("postbank");
        // --------------------------------------
        mTripDests.Controls.Clear();
        try
        {
            mTripDests.Controls.Add(new LiteralControl(string.Format("{0}&nbsp;:{1}&nbsp;,&nbsp;"
                  , mTrip.Service.Path.City1.Title, Helper.Instance.GetNumTickets(mTrip.ID, mTrip.Service.Path.DestCityID))));
            List<ServiceDest> dests = Helper.Instance.GetServiceDests(mTrip.ServiceID);
            if (dests != null)
            {
                for (int i = 0; i < dests.Count; i++)
                    mTripDests.Controls.Add(new LiteralControl(string.Format("{0}&nbsp;:{1}&nbsp;,&nbsp;"
                        , dests[i].City.Title, Helper.Instance.GetNumTickets(mTrip.ID, dests[i].CityID))));
            }
        }
        catch (Exception ex)
        {
        }
        // --------------------------------------
        int total = mTrip.CarType.Layout.NumChairs.HasValue ? mTrip.CarType.Layout.NumChairs.Value : 0;
        int full = Helper.Instance.GetNumTickets(mTrip.ID);
        int numReserves = Helper.Instance.GetNumReserves(mTrip.ID);

        mAll.Text = total.ToString();
        mFull.Text = full.ToString();
        mNumReserves.Text = numReserves.ToString();
        mNumEmpty.Text = (total - full - numReserves).ToString();
        mSumDiscount.Text = GetSumDiscount().ToString();
        mLayout.Controls.Clear();

        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        if (bindCombos)
        {
            mReserves.DataValueField = mTickets.DataValueField = "ID";
            mReserves.DataTextField = mTickets.DataTextField = "NoPlusName";
            mTickets.DataSource = tickets;
            mReserves.DataSource = Helper.Instance.GetReserves(mTrip.ID);
            mTickets.DataBind();
            mReserves.DataBind();
        }

        Layout layout = mTrip.CarType.Layout;
        if (string.IsNullOrEmpty(layout.Layout1))
            return;
        int index = 1;
        bool isFirst = true;
        string lastStr = "", firsStr = ""; ;
        for (int f = 0; f < layout.Floors; f++)
        {
            for (int i = 0; i < layout.Rows; i++)
            {
                for (int j = 0; j < layout.Columns; j++)
                {
                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                    if (layout.Layout1.Contains(key))
                    {
                        bool man;
                        Ticket ticket = GetTicket(tickets, index, out man);
                        if (ticket == null)
                            lastStr = "cbINDX".Replace("INDX", index.ToString());
                        index++;
                    }
                }
            }
        }

        index = 1;
        for (int f = 0; f < layout.Floors; f++)
        {
            mLayout.Controls.Add(new LiteralControl(string.Format("طبقه {0}:<br />", f + 1)));
            mLayout.Controls.Add(new LiteralControl("<table cellpadding=0 cellspacing=0 border=1>"));
            for (int i = 0; i < layout.Rows; i++)
            {
                mLayout.Controls.Add(new LiteralControl("<tr>"));
                for (int j = 0; j < layout.Columns; j++)
                {
                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                    if (layout.Layout1.Contains(key))
                    {
                        #region Script
                        string code = @"
<span  onMouseOver='setColor(INDX);setText(PASSNAME)' 
	onClick='setCheck(INDX);
(INDX);' 
	onMouseOut='resetColor(INDX)'  title='TOOLTIP' BACK>
		<span id=select_spanINDX>
			&nbsp;&nbsp;
				<input name=cbSexINDX type='hidden' id=cbSexINDX value=MAVVAL size='2'>
				<img id=cbPicINDX src='images/MAN.gif' onclick='changeSex(INDX);setCheck(INDX);' >
				<span id=spanINDX>
					<input name=cbINDX type='checkbox' id=cbINDX value='0' tabindex=19  DISABLED CHECKED LASTCHECK
						onClick='setCheck(INDX)' onfocus='setColor(INDX);setText(PASSNAME)'  onBlur='resetColor(INDX);' 
						onkeypress='if(event.keyCode==13) {setCheck(INDX); setColor(INDX)}'>
				</span>
				INDX
		</span>
</span>".Replace("INDX", index.ToString());
                        #endregion

                        bool man;
                        string[] rows = null;
                        if (!string.IsNullOrEmpty(mTrip.Service.OnlineRows))
                        {
                            rows = mTrip.Service.OnlineRows.Split(';');
                        }
                        Ticket ticket = GetTicket(tickets, index, out man);
                        if (ticket == null)
                        {
                            #region Free

                            if (rows != null && Tool.GetBool(Helper.Instance.GetSettingValue("OnlineSale"), false))
                            {
                                for (int k = 0; k < rows.Length; k++)
                                {
                                    if (Tool.GetInt(rows[k]) == i + 1)
                                    {
                                        code = code.Replace("DISABLED", "disabled='disabled'");
                                        code = code.Replace("onclick='changeSex(INDX);setCheck(INDX);'".Replace("INDX", index.ToString()), "");
                                        code = code.Replace("CHECKED", "");
                                        break;
                                    }
                                }

                                code = code.Replace("DISABLED", "");
                                code = code.Replace("TOOLTIP", "Free");
                                if (mTitle.SelectedIndex == 0)
                                {
                                    code = code.Replace("MAN", "man");
                                    code = code.Replace("MAVVAL", "1");
                                }
                                else
                                {
                                    code = code.Replace("MAN", "woman");
                                    code = code.Replace("MAVVAL", "0");
                                }
                                code = code.Replace("BACK", "style='cursor:pointer'");
                                if (selectAll)
                                    code = code.Replace("CHECKED", "checked='checked'");
                                else if (deselectAll)
                                    code = code.Replace("CHECKED", "");
                                else if (Tool.IsChecked(Request, "cb" + index))
                                    code = code.Replace("CHECKED", "checked='checked'");
                                else
                                    code = code.Replace("CHECKED", "");
                                code = code.Replace("PASSNAME", "\"خالی\"");
                            }
                            else
                            {
                                code = code.Replace("DISABLED", "");
                                code = code.Replace("TOOLTIP", "Free");
                                if (mTitle.SelectedIndex == 0)
                                {
                                    code = code.Replace("MAN", "man");
                                    code = code.Replace("MAVVAL", "1");
                                }
                                else
                                {
                                    code = code.Replace("MAN", "woman");
                                    code = code.Replace("MAVVAL", "0");
                                }
                                code = code.Replace("BACK", "style='cursor:pointer'");
                                if (selectAll)
                                    code = code.Replace("CHECKED", "checked='checked'");
                                else if (deselectAll)
                                    code = code.Replace("CHECKED", "");
                                else if (Tool.IsChecked(Request, "cb" + index))
                                    code = code.Replace("CHECKED", "checked='checked'");
                                else
                                    code = code.Replace("CHECKED", "");
                                code = code.Replace("PASSNAME", "\"خالی\"");
                            }

                            #endregion
                            if (isFirst)
                            {
                                isFirst = false;
                                firsStr = "cbINDX".Replace("INDX", index.ToString());
                                mDiscount.Attributes.Add("onkeydown"
                                    , " if (event.keyCode==9){$get('cbINDX').focus();return false;}".Replace("INDX", index.ToString()));
                            }
                            if (lastStr == "cbINDX".Replace("INDX", index.ToString()))
                                code = code.Replace("LASTCHECK", "onkeydown=\"if (event.keyCode==9){$get('NAME').focus();return false;}\"".Replace("NAME", mName.ClientID));
                        }
                        else
                        {
                            if (ShowPayback)
                                if (index.ToString() == Request.QueryString["who"])
                                {
                                    TicketID = ticket.ID;
                                    pbFullname.Text = ticket.Fullname;
                                    pbChairs.Text = ticket.Chairs.Replace(";", ",").Replace("m", "").Replace("f", "");
                                    doSubtract.Enabled = doPayback.Enabled = true;
                                    mNoTR.Visible = false;
                                    // Check if it is soled on WEB
                                    mIsWeb.Visible = webUser != null && webUser.ID == ticket.SaleUserID;
                                    mIsPost.Visible = postbankUser != null && postbankUser.ID == ticket.SaleUserID;
                                    if ((webUser != null && ticket.SaleUserID == webUser.ID) || (postbankUser != null && ticket.SaleUserID == postbankUser.ID))
                                    {
                                        // only CompanyAdmin can change it
                                        if (SiteSettings.User.IsCompanyAdmin != true)
                                        {
                                            mMsg2.Text = "این بلیط از طریق اینترنت فروخته شده است و فقط کاربر اصلی میتواند آنرا استرداد کند";
                                            doSubtract.Enabled = doPayback.Enabled = false;
                                        }
                                        else
                                            mNoTR.Visible = true;
                                    }
                                    puEx.Show();
                                }
                            #region Has Ticket
                            code = code.Replace("CHECKED", "checked='checked'");
                            if (ticket.ID != CurTicketID)
                                code = code.Replace("DISABLED", "disabled='disabled'");
                            else
                            {
                                code = code.Replace("DISABLED", "");
                                code = code.Replace("BACK", "style='background-color:red;cursor:pointer'");
                            }
                            code = code.Replace("TOOLTIP", string.Format("{0} ({1}) \r\nصندلیها:{2}\r\nتعداد: {3}\r\nتخفیف: {4}\r\nجمع: {5}"
                                , ticket.Fullname, ticket.No == "-1" ? "0" : ticket.No.Substring(mTrip.ID.ToString().Length + 1)
                                , ticket.Chairs == null ? "" : ticket.Chairs.Replace("f", "").Replace("m", "").Replace(";", "-").Trim('-')
                                , ticket.GetNumTickets()
                                , ticket.Discount
                                , ((ticket.Price - ticket.Discount) * ticket.GetNumTickets())));
                            if (man)
                            {
                                code = code.Replace("MAN", "man_disable");
                                code = code.Replace("MAVVAL", "1");
                            }
                            else
                            {
                                code = code.Replace("MAN", "woman_disable");
                                code = code.Replace("MAVVAL", "0");
                            }
                            if (ticket.SaleType == (int)SaleTypes.Reserve)
                                code = code.Replace("BACK", "style='background-color:#6666FF;cursor:pointer'");
                            else if (ticket.SaleType == (int)SaleTypes.OfficeSale || ticket.SaleType == (int)SaleTypes.Sale)
                            {
                                string color = "";
                                int colorID = Tool.GetInt(Helper.Instance.GetSettingValue("UserColor" + ticket.SaleUserID), 1);
                                if (colorID == 1)
                                    color = "#003366";
                                else if (colorID == 2)
                                    color = "#FFFF88";
                                else if (colorID == 3)
                                    color = "#663300";
                                else if (colorID == 4)
                                    color = "#330066";
                                else if (colorID == 5)
                                    color = "#FF1415";
                                else if (colorID == 6)
                                    color = "#00CC66";
                                else if (colorID == 7)
                                    color = "#da70d6";
                                else if (colorID == 8)
                                    color = "#daa520";
                                else if (colorID == 9)
                                    color = "#00ffff";
                                else if (colorID == 10)
                                    color = "#bdb76b";

                                code = code.Replace("BACK", string.Format("style='background-color:{0};cursor:pointer'", color));
                            }
                            else
                                code = code.Replace("BACK", "");
                            code = code.Replace("PASSNAME", "\"" + ticket.Fullname + "\"");
                            #endregion
                        }
                        code = code.Replace("LASTCHECK", "");

                        mLayout.Controls.Add(new LiteralControl(string.Format("<td style='width:70px'>{0}</td>", code)));
                        index++;
                    }
                    else
                        mLayout.Controls.Add(new LiteralControl("<td style='{width:40px}'>&nbsp;-&nbsp;</td>"));
                    if (layout.ColumnSpace == j + 1)
                        mLayout.Controls.Add(new LiteralControl("<td style='{width:40px}'>&nbsp;-&nbsp;</td>"));
                }
                mLayout.Controls.Add(new LiteralControl("</tr>"));
            }
            mLayout.Controls.Add(new LiteralControl("</table>"));
            //CheckBox cb = new CheckBox();
            //cb.Checked = true;
            //cb.Text = "FCK";
            //mLayout.Controls.Add(cb);
        }


        BindHotKeys(firsStr);
    }
    int GetSumDiscount()
    {
        try
        {
            var v = from t in Helper.Instance.DB.Tickets
                    where t.TripID == TripID
                    && (t.SaleType == (int)SaleTypes.Sale || t.SaleType == (int)SaleTypes.OfficeSale)
                    && t.PaidBack == 0
                    select t.Discount * t.NumChairs;
            int? res = v.Sum();
            if (res.HasValue)
                return res.Value;
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
        return 0;
    }
    Ticket GetTicket(List<Ticket> tickets, int chairIndex, out bool man)
    {
        man = true;
        string mKey = string.Format(";m{0};", chairIndex);
        string fKey = string.Format(";f{0};", chairIndex);
        for (int i = 0; i < tickets.Count; i++)
            if (tickets[i].Chairs.Contains(mKey))
                return tickets[i];
            else if (tickets[i].Chairs.Contains(fKey))
            {
                man = false;
                return tickets[i];
            }
        return null;
    }
    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
    protected int CurTicketID
    {
        get
        {
            return Tool.GetInt(ViewState["CurTicketID"], Null.NullInteger);
        }
        set
        {
            ViewState["CurTicketID"] = value;
        }
    }
    private string GetChairs()
    {
        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        Layout layout = mTrip.CarType.Layout;
        if (string.IsNullOrEmpty(layout.Layout1))
            return "";
        StringBuilder res = new StringBuilder();
        StringBuilder sold = new StringBuilder();
        int index = 1;
        for (int f = 0; f < layout.Floors; f++)
        {
            for (int i = 0; i < layout.Rows; i++)
            {
                for (int j = 0; j < layout.Columns; j++)
                {
                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                    if (layout.Layout1.Contains(key))
                    {
                        {
                            //bool man;
                            //Ticket ticket = GetTicket(tickets, index, out man);
                            //if (ticket == null || ticket.ID == CurTicketID)
                            {
                                if (Tool.IsChecked(Request, "cb" + index))
                                {
                                    bool man;
                                    Ticket ticket = GetTicket(tickets, index, out man);
                                    if (ticket == null || ticket.ID == CurTicketID)
                                    {
                                        string isMan = Tool.GetValue(Request, "cbSex" + index);
                                        res.Append(string.Format(";{0}{1};", isMan == "1" ? "m" : "f", index));
                                    }
                                    else if (CurTicketID == Null.NullInteger)
                                    {
                                        sold.AppendFormat("{0}, ", index);
                                    }
                                }
                            }
                            index++;
                        }
                    }
                }
            }
        }
        if (sold.Length > 0)
            throw new Exception("این صندلیها قبلا فروخته شده اند:" + sold.ToString(0, sold.Length - 2));
        return res.ToString().Replace(";;", ";");
    }
    protected void mTitle_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData(false);
    }
    protected void doSale_Click(object sender, EventArgs e)
    {
        SaleTickts(mAutoPrint.Checked);
    }
    protected void doSale2_Click(object sender, EventArgs e)
    {
        SaleTickts(false);
    }
    protected void SaleTickts(bool print)
    {
        Ticket t = new Ticket();
        t.TripID = this.TripID;
        t.SaleUserID = SiteSettings.UserID;
        t.SaleDate = DateTime.Now;
        Customer cust = Helper.Instance.GetCustomer(mCustomerNo.Text);
        if (cust != null)
        {
            t.CustomerID = cust.ID;
            t.Fullname = cust.Name + " " + cust.Surname;
        }
        else
            t.Fullname = mName.Text;
        if (t.Fullname.Trim().Length == 0)
        {
            mError.Text = "نام مسافر وارد نشده است";
            BindData(false);
            return;
        }
        
        t.Man = mTitle.SelectedIndex == 0;
        t.No = Helper.Instance.GetNewTicketNo(mTrip.ID);
        t.Tel = mTel.Text;
        try
        {
            t.Chairs = GetChairs();
        }
        catch (Exception ex)
        {
            mError.Text = ex.Message;
            BindData(false);
            return;
        }

        if (string.IsNullOrEmpty(t.Chairs))
        {
            mError.Text = "صندلی انتخاب نشده است";
            BindData(false);
            return;
        }
        if (t.Tel.Trim().Length == 0 && Tool.GetBool(Helper.Instance.GetSettingValue("Mobile"), false) && t.Chairs.Contains('m'))
        {
            mError.Text = "شماره موبایل وارد نشده است";
            BindData(false);
            return;
        }
        else if (!t.Tel.StartsWith("090") && !t.Tel.StartsWith("091") && !t.Tel.StartsWith("092") && !t.Tel.StartsWith("093") && t.Tel.Length != 11 && t.Tel.Trim().Length > 0)
        {
            mError.Text = "شماره موبایل صحیح نمی باشد";
            BindData(false);
            return;
        }
        if (t.Chairs.Contains('f') && !t.Chairs.Contains('m'))
        {
            if (!t.Fullname.StartsWith("خانم"))
                t.Fullname = "خانم " + t.Fullname;
        }
        else if (t.Chairs.Contains('m') && !t.Chairs.Contains('f'))
        {
            if (!t.Fullname.StartsWith("آقا"))
                t.Fullname = "آقای " + t.Fullname;
        }
        t.NumChairs = t.Chairs.Trim().Trim(';').Split(';').Length;
        t.CityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
        t.SaleType = (int)SaleTypes.Sale;
        t.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        if (t.FundID == Null.NullInteger)
        {
            mError.Text = "صندوق انتخاب نشده است";
            BindData(false);
            return;
        }
        t.Expire = null;
        if (mBranchID.SelectedIndex >= 0)
        {
            t.SaleType = (int)SaleTypes.OfficeSale;
            t.BranchID = Tool.GetInt(mBranchID.SelectedValue, Null.NullInteger);
        }
        if (t.BranchID == null || t.BranchID.Value <= 0)
        {
            mError.Text = "دفتر انتخاب نشده است";
            BindData(false);
            return;
        }
        
        t.Price = Tool.GetInt(mPrice.Text, 0);
        t.Discount = Tool.GetInt(mDiscount.Text, 0);
        t.Printed = false;
        t.Address = mAddress.Text;
        Helper.Instance.DB.Tickets.InsertOnSubmit(t);
        if (!Helper.Instance.Update())
            mError.Text = "خطا در فروش " + Helper.Instance.LastException.Message;
        else
        {
            mError.Text = "فروش با موفقیت انجام شد";
            mError.Text = string.Format("بلیط با شماره {0} و جمع {1} فروخته شد. <a href='Reports/Ticket.aspx?id={2}' target=_blank>چاپ</a><br />", t.No, (t.Price - t.Discount) * t.NumChairs, t.ID);
            string smsUser = Helper.Instance.GetSettingValue("SMSUserName");
            string smsPass = Helper.Instance.GetSettingValue("SMSPassword");
            if (Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSSale"), false) && !string.IsNullOrEmpty(t.Tel) && !string.IsNullOrEmpty(smsUser) && !string.IsNullOrEmpty(smsPass))
            {
                service sms = new service();
                sms.AddSms(smsUser, smsPass, t.Fullname, t.Tel, Helper.Instance.GetSettingValue("SMSMessageSale")+"\n\r"+mTrip.Service.SMSMessage);
                //int result = sms.Send(t.Tel, Helper.Instance.GetSettingValue("SMSMessage"));
                //if (result == 0)
                //    mError.Text += "پیام ارسال شد.";
            }
            if (Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSSale"), false) && string.IsNullOrEmpty(smsUser) && string.IsNullOrEmpty(smsPass))
            {
                mError.Text += @"نام کاربری و رمز عبور برای ارسال پیام کوتاه وارد نشده است<br />";
            }
            if (string.IsNullOrEmpty(t.Tel) && Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSSale"), false))
            {
                mError.Text += @"به دلیل وارد نکردن شماره موبایل برای مسافر پیام کوتاهی ارسال نخواهد شد";
            }
            mName.Text = mCustomerNo.Text = mDiscount.Text = mTel.Text = "";
            if (print)
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=300,height=550,width=430');
}
window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=50,height=550,width=900');
//window.onload=openprint;
</script>".Replace("IDID", t.ID.ToString());
                Page.RegisterStartupScript("openprint", open);
            }
            if (Simple)
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Reports/AutoTicket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=300,height=550,width=430');
}
window.open('Reports/AutoTicket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=50,height=550,width=900');
//window.onload=openprint;
</script>".Replace("IDID", t.ID.ToString());
                Page.RegisterStartupScript("openprint", open);
            }
            //mMsg.Text = string.Format("بلیط با شماره {0} فروخته شد. <a href='Reports/Ticket.aspx?id={1}' target=_blank>چاپ</>", t.No, t.ID);

            if (mCityID.Items.Count > 0)
            {
                mCityID.SelectedIndex = mCityID.Items.Count - 1;
                //mCityID_SelectedIndexChanged(null, null);
                try
                {
                    int cityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
                    if (cityID == mTrip.Service.Path.DestCityID)
                        mPrice.Text = mTrip.Price.ToString(); //mTrip.Service.Price.ToString();
                    else
                    {
                        ServiceDest sd = Helper.Instance.GetServiceDest(mTrip.ServiceID, cityID);
                        mPrice.Text = sd == null ? mTrip.Price.ToString() : sd.Price.ToString();
                    }
                }
                catch (Exception ex)
                {
                    mError.Text = ex.Message;
                }
            }
        }
        BindData(true);
    }
    protected void doReserve_Click(object sender, EventArgs e)
    {
        Ticket t = new Ticket();
        t.TripID = this.TripID;
        t.SaleUserID = SiteSettings.UserID;
        t.SaleDate = DateTime.Now;
        Customer cust = Helper.Instance.GetCustomer(mCustomerNo.Text);
        if (cust != null)
        {
            t.CustomerID = cust.ID;
            t.Fullname = cust.Name + " " + cust.Surname;
        }
        else
            t.Fullname = mName.Text;
        if (t.Fullname.Trim().Length == 0)
        {
            mError.Text = "نام مسافر وارد نشده است";
            BindData(false);
            return;
        }
        t.Man = mTitle.SelectedIndex == 0;
        t.No = Helper.Instance.GetNewTicketNo(mTrip.ID);
        try
        {
            t.Chairs = GetChairs();
        }
        catch (Exception ex)
        {
            mError.Text = ex.Message;
            BindData(false);
            return;
        }
        if (string.IsNullOrEmpty(t.Chairs))
        {
            mError.Text = "صندلی انتخاب نشده است";
            BindData(false);
            return;
        }
        t.NumChairs = t.Chairs.Trim().Trim(';').Split(';').Length;
        t.CityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
        t.SaleType = (int)SaleTypes.Reserve;
        t.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        t.Expire = mExpireDate.SelectedDate;
        if (t.Expire == new DateTime())
        {
            mError.Text = "تاریخ یا زمان انقضای انتخاب شده اشتباه است";
            BindData(false);
            return;
        }
        if (mBranchID.SelectedIndex > 0)
            t.BranchID = Tool.GetInt(mBranchID.SelectedValue, Null.NullInteger);
        t.Tel = mTel.Text;
        t.Price = Tool.GetInt(mPrice.Text, 0);
        t.Discount = Tool.GetInt(mDiscount.Text, 0);
        t.Address = mAddress.Text;
        if (t.Tel.Trim().Length == 0 && Tool.GetBool(Helper.Instance.GetSettingValue("Mobile"), false) && t.Chairs.Contains('m'))
        {
            mError.Text = "شماره موبایل وارد نشده است";
            BindData(false);
            return;
        }
        else if (!t.Tel.StartsWith("090") && !t.Tel.StartsWith("091") && !t.Tel.StartsWith("092") && !t.Tel.StartsWith("093") && t.Tel.Length != 11 && t.Tel.Trim().Length>0)
        {
            mError.Text = "شماره موبایل صحیح نمی باشد";
            BindData(false);
            return;
        }
        Helper.Instance.DB.Tickets.InsertOnSubmit(t);
        if (!Helper.Instance.Update())
            mError.Text = "خطا در رزرو " + Helper.Instance.LastException.Message;
        else
        {
            mError.Text = "رزرو با موفقیت انجام شد";
            mName.Text = mCustomerNo.Text = mDiscount.Text = mTel.Text = "";

            mError.Text = string.Format("بلیط با شماره {0} و جمع {1} رزرو شد. <a href='Reports/Ticket.aspx?id={2}' target=_blank>چاپ</a><br />", t.No, (t.Price - t.Discount) * t.NumChairs, t.ID);
            string smsUser = Helper.Instance.GetSettingValue("SMSUserName");
            string smsPass = Helper.Instance.GetSettingValue("SMSPassword");
            if (Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSReserve"), false) && !string.IsNullOrEmpty(t.Tel) && !string.IsNullOrEmpty(smsUser) && !string.IsNullOrEmpty(smsPass))
            {
                service sms=new service();
                sms.AddSms(smsUser,smsPass,t.Fullname,t.Tel, Helper.Instance.GetSettingValue("SMSMessage"));
                //int result = sms.Send(t.Tel, Helper.Instance.GetSettingValue("SMSMessage"));
                //if (result == 0)
                //    mError.Text += "پیام ارسال شد.";
            }
            if (Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSReserve"), false) && string.IsNullOrEmpty(smsUser) && string.IsNullOrEmpty(smsPass))
            {
                mError.Text += @"نام کاربری و رمز عبور برای ارسال پیام کوتاه وارد نشده است<br />";
            }
            if (string.IsNullOrEmpty(t.Tel) && Tool.GetBool(Helper.Instance.GetSettingValue("SendSMSReserve"), false))
            {
                mError.Text += @"به دلیل وارد نکردن شماره موبایل برای مسافر پیام کوتاهی ارسال نخواهد شد";
            }
            if (false && mAutoPrint.Checked)
            {
                string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=300,height=550,width=430');
}
window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=50,height=550,width=900');
//window.onload=openprint;
</script>".Replace("IDID", t.ID.ToString());
                Page.RegisterStartupScript("openprint", open);
            }
        }
        BindData(true);
    }
    protected void doCancel_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(Tool.GetInt(mReserves.SelectedValue, Null.NullInteger));
        if (t != null)
            if (!Helper.Instance.DeleteTicket(t))
            {
                //todo: showerror message
            }
        BindData(true);
    }
    protected void doCancelAll_Click(object sender, EventArgs e)
    {
        List<Ticket> ts = Helper.Instance.GetReserves(mTrip.ID);
        if (ts != null)
            for (int i = 0; i < ts.Count; i++)
            {
                if (!Helper.Instance.DeleteTicket(ts[i]))
                {
                    //todo: showerror message
                }
            }

        BindData(true);
    }
    protected void doConfirmReserve_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(Tool.GetInt(mReserves.SelectedValue, Null.NullInteger));
        if (t != null)
        {
            t.SaleType = (int)SaleTypes.Sale;
            t.SaleUserID = SiteSettings.UserID;
            t.SaleDate = DateTime.Now;
            t.NumChairs = t.Chairs.Trim().Trim(';').Split(';').Length;
            if (t.Tel.Trim().Length == 0 && Tool.GetBool(Helper.Instance.GetSettingValue("Mobile"), false) && t.Chairs.Contains('m'))
            {
                mError.Text = "شماره موبایل وارد نشده است";
                BindData(false);
                return;
            }
            else if (!t.Tel.StartsWith("090") && !t.Tel.StartsWith("091") && !t.Tel.StartsWith("092") && !t.Tel.StartsWith("093") && t.Tel.Length != 11 && t.Tel.Trim().Length > 0)
            {
                mError.Text = "شماره موبایل صحیح نمی باشد";
                BindData(false);
                return;
            }
            if (!Helper.Instance.Update())
            {
                mError.Text = "خطا در فروش رزرو:" + Helper.Instance.LastException.Message;
                BindData(false);
                return;
            }
            else
            {
                mError.Text = string.Format("بلیط رزرو با شماره {0} و جمع {1} فروش شد. <a href='Reports/Ticket.aspx?id={2}' target=_blank>چاپ</>", t.No, (t.Price - t.Discount) * t.NumChairs, t.ID);
                BindData(true);
            }
        }
        else
        {
            mError.Text = "رزرو انتخاب نشده است.";
            BindData(false);
        }
    }
    protected void doChange_Click(object sender, EventArgs e)
    {
        int ticketID = Tool.GetInt(mTickets.SelectedValue, Null.NullInteger);
        Ticket t = Helper.Instance.GetTicket(ticketID);

        if (t != null)
        {
            // Check if it is soled on WEB
            User webUser = Helper.Instance.GetUser("web");
            User postbankUser = Helper.Instance.GetUser("postbank");
            if ((webUser != null && t.SaleUserID == webUser.ID) || (postbankUser != null && t.SaleUserID == postbankUser.ID))
            {
                // only admin can change it
                if (SiteSettings.User.IsCompanyAdmin != true)
                {
                    mError.Text = "این بلیط از طریق اینترنت فروخته شده است و فقط کاربر اصلی میتواند آنرا اصلاح  کند";
                    BindData(false);
                    return;
                }
            }
            CurTicketID = ticketID;
            SetChangeMode(true);

            mName.Text = t.Fullname;
            mDiscount.Text = t.Discount.HasValue ? t.Discount.Value.ToString() : "";
            Tool.SetSelected(mCityID, t.CityID);
            int cityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
            if (cityID == mTrip.Service.Path.DestCityID)
                mPrice.Text = mTrip.Price.ToString();
            else
            {
                ServiceDest sd = Helper.Instance.GetServiceDest(mTrip.ServiceID, cityID);
                mPrice.Text = sd == null ? "?" : sd.Price.ToString();
            }


            mIsWeb1.Visible = webUser != null && t.SaleUserID == webUser.ID;
            mIsPost1.Visible = postbankUser != null && t.SaleUserID == postbankUser.ID;
        }
        BindData(false);
    }
    protected void doChangeOk_Click(object sender, EventArgs e)
    {
        mIsWeb1.Visible = false;
        mIsPost1.Visible = false;
        if (CurTicketID != Null.NullInteger)
        {
            Ticket t = Helper.Instance.GetTicket(CurTicketID);
            if (t != null)
            {
                try
                {
                    t.Chairs = GetChairs();
                }
                catch (Exception ex)
                {
                    mError.Text = ex.Message;
                    BindData(false);
                    return;
                }
                if (string.IsNullOrEmpty(t.Chairs))
                {
                    mError.Text = "صندلی انتخاب نشده است";
                    BindData(false);
                    return;
                }
                t.NumChairs = t.Chairs.Trim().Trim(';').Split(';').Length;
                t.Fullname = mName.Text;
                t.Discount = Tool.GetInt(mDiscount.Text, 0);
                t.CityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
                t.City = Helper.Instance.GetCity(Tool.GetInt(mCityID.SelectedValue, Null.NullInteger));
                t.Price = Tool.GetInt(mPrice.Text, 0);
                t.Address = mAddress.Text;
                t.SaleUserID = SiteSettings.UserID;
                if (!Helper.Instance.Update())
                {
                    //todo: showerror message
                    mError.Text = "خطا در ویرایش" + Helper.Instance.LastException.Message;
                    return;
                }
                mError.Text = string.Format("بلیط با شماره {0} و جمع {1} اصلاح شد. <a href='Reports/Ticket.aspx?id={2}' target=_blank>چاپ</>", t.No, (t.Price - t.Discount) * t.NumChairs, t.ID);
            }
        }
        SetChangeMode(false);
        CurTicketID = Null.NullInteger;
        BindData(true);
        mDiscount.Text = mName.Text = mCustomerNo.Text = "";
    }
    void SetChangeMode(bool value)
    {
        doSale2.Enabled = doSale.Enabled = doReserve.Enabled = doCancel.Enabled = doChange.Enabled = doConfirmReserve.Enabled = doCancelAll.Enabled = !value;
        doChangeCancel.Enabled = doChangeOk.Enabled = value;

        if (!value)
        {
            doSale3.CssClass = doSale2.CssClass = doSale.CssClass = doReserve.CssClass = doCancel.CssClass = doChange.CssClass = doConfirmReserve.CssClass = doCancelAll.CssClass = "CBBJ";
            doChangeCancel.CssClass = doChangeOk.CssClass = "CBBD";
        }
        else
        {
            doSale3.CssClass = doSale2.CssClass = doSale.CssClass = doReserve.CssClass = doCancel.CssClass = doChange.CssClass = doConfirmReserve.CssClass = doCancelAll.CssClass = "CBBD";
            doChangeCancel.CssClass = doChangeOk.CssClass = "CBBJ";
        }

        doReserve.Enabled = Tool.GetBool(Helper.Instance.GetSettingValue("AllowReserve"), true);
    }
    protected void doChangeCancel_Click(object sender, EventArgs e)
    {
        mIsWeb1.Visible = false;
        mIsPost1.Visible = false;
        mDiscount.Text = mName.Text = mCustomerNo.Text = "";
        SetChangeMode(false);
        CurTicketID = Null.NullInteger;
        BindData(false);
    }
    protected void mCityID_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int cityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
            if (cityID == mTrip.Service.Path.DestCityID)
                mPrice.Text = mTrip.Price.ToString(); //mTrip.Service.Price.ToString();
            else
            {
                ServiceDest sd = Helper.Instance.GetServiceDest(mTrip.ServiceID, cityID);
                mPrice.Text = sd == null ? mTrip.Price.ToString() : sd.Price.ToString();
            }
        }
        catch (Exception ex)
        {
            mError.Text = ex.Message;
        }
        BindData(false);
    }
    protected void doShowCustomer_Click(object sender, EventArgs e)
    {
        Customer cust = Helper.Instance.GetCustomer(mCustomerNo.Text);
        if (cust != null)
        {
            mError.Text = string.Format("کد {0} : {1} {2} {3} تلفن:{4}"
                , cust.No, cust.Man ? "آقای" : "خانم", cust.Name, cust.Surname, cust.Tel);
                //"مشتری با کد داده شده پیدا نشد";
            mName.Text = cust.Name + ' ' + cust.Surname;
            mTel.Text = cust.Tel;
            mAddress.Text = cust.Address;
        }
        else
            mError.Text = "مشترک با کد داده شده پیدا نشد";
        BindData(false);
    }

    protected void doSelectAll_Click(object sender, EventArgs e)
    {
        BindData(false, true, false);
    }
    protected void doDeSelectAll_Click(object sender, EventArgs e)
    {
        BindData(false, false, true);
    }

    protected void doAutoFill_Click(object sender, EventArgs e)
    {
        // -------------------------------
        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        Layout layout = mTrip.CarType.Layout;
        if (string.IsNullOrEmpty(layout.Layout1))
        {
            mError.Text = "نوع اتوکار، چیدمان ندارد";
            return;
        }
        int fundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        if (fundID == Null.NullInteger)
        {
            mError.Text = "صندوق انتخاب نشده است";
            BindData(false);
            return;
        }
        int branchID = Tool.GetInt(mBranchID.SelectedValue, Null.NullInteger);
        if (branchID == Null.NullInteger)
        {
            mError.Text = "دفتر انتخاب نشده است";
            BindData(false);
            return;
        }
        string[] names = null;
        if (!string.IsNullOrEmpty(mTrip.Service.DefTickets))
            names = mTrip.Service.DefTickets.Split(',', '،', ':');
        if (names == null || names.Length == 0 || (names.Length == 1 && names[0].Trim().Length == 0))
            names = Tool.GetString(Helper.Instance.GetSettingValue("AutoFillNames"), "").Split(',', '،', ':');
        if (names == null || names.Length == 0 || (names.Length == 1 && names[0].Trim().Length == 0))
        {
            mError.Text = "ليست نامهای پر کردن خودکار سرویس در تنظیمات یا در تعریف سرویس پر نشده است";
            BindData(false);
            return;
        }
        Random rnd = new Random();
        List<Ticket> newTickets = new List<Ticket>();
        int index = 1;
        for (int f = 0; f < layout.Floors; f++)
        {
            for (int i = 0; i < layout.Rows; i++)
            {
                for (int j = 0; j < layout.Columns; j++)
                {
                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                    if (layout.Layout1.Contains(key))
                    {
                        bool man;
                        Ticket ticket = GetTicket(tickets, index, out man);
                        if (ticket == null)
                        {
                            Ticket t = new Ticket();
                            #region Fill Ticket
                            t.TripID = this.TripID;
                            t.SaleUserID = SiteSettings.UserID;
                            t.SaleDate = DateTime.Now;
                            t.Man = true;
                            string name = names[rnd.Next(0, names.Length)].Trim();
                            if (name.StartsWith("-"))
                            {
                                name = name.Substring(1);
                                t.Man = false;
                            }
                            t.Fullname = name;

                            t.No = Helper.Instance.GetNewTicketNo(mTrip.ID);
                            t.Chairs = string.Format(";{0}{1};", t.Man ? "m" : "f", index);
                            t.NumChairs = t.Chairs.Trim().Trim(';').Split(';').Length;
                            t.CityID = Tool.GetInt(mCityID.SelectedValue, Null.NullInteger);
                            t.SaleType = (int)SaleTypes.Sale;
                            t.FundID = fundID;

                            t.Expire = null;
                            t.SaleType = (int)SaleTypes.OfficeSale;
                            t.BranchID = branchID;
                            t.Tel = mTel.Text;
                            t.Price = Tool.GetInt(mPrice.Text, 0);
                            t.Discount = Tool.GetInt(mDiscount.Text, 0);
                            #endregion
                            newTickets.Add(t);
                        }
                        index++;
                    }
                }
            }
        }


        // ===============================================+++++++++++++++++
        Helper.Instance.DB.Tickets.InsertAllOnSubmit(newTickets);
        if (!Helper.Instance.Update())
            mError.Text = "خطا در فروش " + Helper.Instance.LastException.Message;
        else
        {
            mError.Text = newTickets.Count + " بلیط با موفقیت فروخته شد";
            mName.Text = mCustomerNo.Text = mDiscount.Text = mTel.Text = "";
        }
        BindData(true);
    }

    protected void doSubtract_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(this.TicketID);
        if (t == null)
            return;
        if (t.Trip.Closed)
        {
            mMsg2.Text = "سرویس مرتبط بسته شده است و نمیتوان این بلیط را استرداد کرد.";
            puEx.Show();
            return;
        }
        // Check if it is soled on WEB
        User webUser = Helper.Instance.GetUser("web");
        User postbankUser = Helper.Instance.GetUser("postbank");
        if ((webUser != null && t.SaleUserID == webUser.ID) || (postbankUser != null && t.SaleUserID == postbankUser.ID))
        {
            // only admin can change it
            if (SiteSettings.User.IsCompanyAdmin != true)
            {
                mMsg2.Text = "این بلیط از طریق اینترنت فروخته شده است و فقط کاربر اصلی میتواند آنرا استرداد کند";
                puEx.Show();
                return;
            }
            //  check for Tracking No:
            Ticket other = Helper.Instance.GetTicket(mTicketNo.Text);
            if (other == null || other.ID != t.ID)
            {
                mMsg2.Text = "شماره بليط وارد شده صحیح نمیباشد.";
                puEx.Show();
                return;
            }
        }

        if (t.SaleType == (int)SaleTypes.Sale)
            t.PaidBack = (int)PaidBacks.FromFund;
        else
            t.PaidBack = mFromSale.Checked ? (int)PaidBacks.FromFund : (int)PaidBacks.FromBranch;
        t.PaidBackPercent = t.Trip.Closed ? (int)Helper.Instance.GetSettingValue("PaidBackPercentClosed", 0)
            : (int)Helper.Instance.GetSettingValue("PaidBackPercentOpen", 0);
        doPayback.Enabled = doSubtract.Enabled = true;
        t.ReturnUserID = SiteSettings.UserID;
        if (Helper.Instance.Update())
        {
            mMsg2.Text = "استرداد با کسر انجام شد";
            Response.Redirect("sale.aspx?msg=paidback1&tripid=" + this.TripID);
        }
        else
        {
            mMsg2.Text = "اشکال در استرداد";
            puEx.Show();
        }
    }
    protected void doPayback_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(this.TicketID);
        if (t == null)
            return;
        if (t.Trip.Closed)
        {
            mMsg2.Text = "سرویس مرتبط بسته شده است و نمیتوان این بلیط را استرداد کرد.";
            puEx.Show();
            return;
        }

        // Check if it is soled on WEB
        User webUser = Helper.Instance.GetUser("web");
        User postbankUser = Helper.Instance.GetUser("postbank");
        if ((webUser != null && t.SaleUserID == webUser.ID) || (postbankUser != null && t.SaleUserID == postbankUser.ID))
        {
            // only admin can change it
            if (SiteSettings.User.IsCompanyAdmin != true)
            {
                mMsg2.Text = "این بلیط از طریق اینترنت فروخته شده است و فقط کاربر اصلی میتواند آنرا استرداد کند";
                puEx.Show();
                return;
            }
            //  check for Tracking No:
            Ticket other = Helper.Instance.GetTicket(mTicketNo.Text);
            if (other == null || other.ID != t.ID)
            {
                mMsg2.Text = "شماره بليط وارد شده صحیح نمیباشد.";
                puEx.Show();
                return;
            }
        }

        if (t.SaleType == (int)SaleTypes.Sale)
            t.PaidBack = (int)PaidBacks.FromFund;
        else
            t.PaidBack = mFromSale.Checked ? (int)PaidBacks.FromFund : (int)PaidBacks.FromBranch;
        t.PaidBackPercent = 0;
        doPayback.Enabled = doSubtract.Enabled = true;
        t.ReturnUserID = SiteSettings.UserID;
        t.PaidBackDate = DateTime.Now;
        if (Helper.Instance.Update())
        {
            //Payment pay = new Payment();
            //pay.Type = (int)PaymentTypes.Payback;
            //pay.Amount 
            mMsg2.Text = "استرداد بي کسر انجام شد";
            Response.Redirect("sale.aspx?msg=paidback2&tripid=" + this.TripID);
        }
        else
        {
            mMsg2.Text = "اشکال در استرداد";
            puEx.Show();
        }
    }

    protected void doSale3_Click(object sender, EventArgs e)
    {
        Simple = true;
        SaleTickts(false);
    }
}
