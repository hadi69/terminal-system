﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class CreateTrip : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    //protected Modules_DatePicker mDateStart;
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        if (!IsPostBack)
        {
            BindInitData();
            if (Request.QueryString["do"] == "sibey")
            {
                sDateStart.SelectedDate = DateTime.Now.AddDays(1);
                sDateEnd.SelectedDate = DateTime.Now.AddDays(1);
            }
            BindData();
            mNewDate.Text = Tool.ToPersianDate(DateTime.Now, "");
        }
    }
    private void BindInitData()
    {
        {
            List<Rate> all = Helper.Instance.GetRates();
            sRates.DataValueField = "ID";
            sRates.DataTextField = "Title";
            sRates.DataSource = all;
            sRates.DataBind();
        }
        {
            List<City> all = Helper.Instance.GetCities(true);
            City none = new City();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sDestID.DataValueField = "ID";
            sDestID.DataTextField = "Title";
            sDestID.DataSource = all;
            sDestID.DataBind();
        }
        {
            List<CarType> all = Helper.Instance.GetCarTypes();
            CarType none = new CarType();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sCarTypes.DataValueField = "ID";
            sCarTypes.DataTextField = "Title";
            sCarTypes.DataSource = all;
            sCarTypes.DataBind();
        }
        {
            List<Special> all = Helper.Instance.GetSpecials();
            Special none = new Special();
            none.ID = Null.NullInteger;
            none.Title = "همه";
            all.Insert(0, none);

            sSpecials.DataValueField = "ID";
            sSpecials.DataTextField = "Title";
            sSpecials.DataSource = all;
            sSpecials.DataBind();
        }
        sDateStart.SelectedDate = DateTime.Now;// = Tool.ToPersianDate(DateTime.Now, "");
        sDateEnd.SelectedDate = DateTime.Now.AddDays(5);// Tool.ToPersianDate(DateTime.Now.AddMonths(1), "");
    }
    private void BindData()
    {
        List<Trip> all = GetTrips();

        list.DataSource = all;
        list.DataBind();
    }
    List<Trip> GetTrips()
    {
        mError.Text = "";
        DateTime fromDate = sDateStart.SelectedDate;// Tool.ParsePersianDate(mDateStart.Text, Null.NullDate);
        if (fromDate == Null.NullDate)
        {
            mError.Text = "لطفا تاریخ شروع را وارد کنید";
            return null;
        }
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate == Null.NullDate)
        {
            mError.Text = "لطفا تاریخ پایان را وارد کنید";
            return null;
        }
        if (fromDate > toDate)
        {
            mError.Text = "تاریخ شروع از تاریخ پایان بزرگتر است";
            return null;
        }
        //string series = string.Format("{0},{1},{2},{3}"
        //                , Helper.Instance.GetSettingValue("SeriesNo1"), Helper.Instance.GetSettingValue("SeriesNo2")
        //                , Helper.Instance.GetSettingValue("SeriesNo3"), Helper.Instance.GetSettingValue("SeriesNo4"));
        //int no = Helper.Instance.GetNextSeriesStart(series);
        List<Service> services = Helper.Instance.GetServices(true, Tool.GetInt(sDestID.SelectedValue, Null.NullInteger));
        if (sCarTypes.SelectedIndex > 0)
        {
            int sID1 = Tool.GetInt(sCarTypes.SelectedValue, Null.NullInteger);
            for (int i = 0; i < services.Count; i++)
                if (services[i].CarTypeID != sID1)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        if (sSpecials.SelectedIndex > 0)
        {
            int sID1 = Tool.GetInt(sSpecials.SelectedValue, Null.NullInteger);
            for (int i = 0; i < services.Count; i++)
                if (services[i].SpecialID != sID1)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        if (sHour.Text.Length > 0 && sMinute.Text.Length > 0)
        {
            int hour = Tool.GetInt(sHour.Text, Null.NullInteger);
            if (hour < 0 || hour > 23)
            {
                mError.Text = "ساعت باید بین 0 و 23 باشد.";
                return null;
            }
            int minute = Tool.GetInt(sMinute.Text, Null.NullInteger);
            if (minute < 0 || minute > 59)
            {
                mError.Text = "دقیقه باید بین 0 و 59 باشد.";
                return null;
            }
            string departureTime = Tool.FixTime(string.Format("{0}:{1}", hour.ToString().PadLeft(2, '0'), minute.ToString().PadLeft(2, '0')));
            for (int i = 0; i < services.Count; i++)
                if (services[i].DepartureTime != departureTime)
                {
                    services.RemoveAt(i);
                    i--;
                }
        }
        List<Trip> res = new List<Trip>();

        int stamp = Tool.GetInt(Helper.Instance.GetSettingValue("Stamp"), 0);
        int others = Tool.GetInt(Helper.Instance.GetSettingValue("Others"), 0);
        DateTime date = fromDate;
        List<int> byPass = new List<int>();
        while (date <= toDate)
        {
            for (int i = 0; i < services.Count; i++)
            {
                Service s = services[i];
                // only create trips for Fixed and AnotherDay services
                if (s.ServiceType == (int)ServiceTypes.Normal || s.ServiceType == (int)ServiceTypes.Singular)
                    continue;
                if (s.IsInDate(date))
                {
                    if (!Helper.Instance.TripExists(s.ID, date))
                    {
                        #region Create Trip
                        Trip info = new Trip();
                        info.Series = "";
                        info.TicketlessName = "";
                        info.Others = others;
                        if (info.IsMini)
                            info.Others = 0;

                        info.Series = "";// series;
                        info.No = 0;// no++;
                        info.ServiceID = s.ID;
                        info.Service = s;
                        info.Date = date;
                        info.Price = s.Price;
                        info.Toll = s.Toll;
                        info.Insurance = s.Insurance;
                        info.Reception = s.Reception;
                        info.Stamp = stamp;
                        info.CarTypeID = s.CarTypeID;
                        info.DepartureTime = s.DepartureTime;
                        if (s.Path == null)
                        {
                            Path path = Helper.Instance.GetPath(s.PathID);
                            if (!byPass.Contains(s.ID))
                            {
                                byPass.Add(s.ID);
                                mError.Text += string.Format("این سرویس، مسیر ندارد: {0}-{1}-{2}-{3}", s.Title, s.ServiceType2, s.Special.Title, s.DepartureTime);
                            }
                            continue;
                        }
                        RateItem rate = Helper.Instance.GetRateItemBySpecial(Null.NullInteger, s.Path.SrcCityID, s.Path.DestCityID, s.SpecialID, info.Date);
                        if (rate != null)
                        {
                            info.ExtraCost = rate.ExtraCost;
                            info.Insurance = rate.Insurance;
                            info.Toll = rate.Toll;
                            info.Reception = rate.ReceptionCost;
                        }

                        info.CarID = null;
                        info.DriverID1 = null;

                        res.Add(info);
                        #endregion
                    }

                }
            }
            date = date.AddDays(1);
        }

        return res;
        //var all = from c in Helper.Instance.DB.Trips
        //          where c.Closed == true
        //          select c;

        //DateTime fromDate = Tool.ParsePersianDate(mDateStart.Text, new DateTime());
        //if (fromDate != new DateTime())
        //    all = all.Where(c => c.Date >= fromDate);
        //DateTime toDate = Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        //if (toDate != new DateTime())
        //    all = all.Where(c => c.Date <= toDate);
        //if (sDestID.SelectedIndex > 0)
        //{
        //    int sID = Tool.GetInt(sDestID.SelectedValue, Null.NullInteger);
        //    all = all.Where(c => c.Service.Path.City.ID == sID);
        //}
        //return all.ToList();
    }

    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Trip info = e.Row.DataItem as Trip;
            if (info != null)
            {
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                e.Row.Cells[2].Text = info.Service.ServiceType2;
                e.Row.Cells[3].Text = info.Service.Path.Title;
                e.Row.Cells[4].Text = info.Service.Special != null ? info.Service.Special.Title : "--خطا--";//.CarType != null ? info.CarType.Title : "";
                if (info.Service.CarTypeID.HasValue)
                {
                    CarType ct = Helper.Instance.GetCarType(info.Service.CarTypeID.Value);
                    e.Row.Cells[4].Text = ct != null ? ct.Title : "";
                }
                else
                    e.Row.Cells[4].Text = "";
                e.Row.Cells[6].Text = info.Driver != null ? info.Driver.Title : "";
                e.Row.Cells[7].Text = Tool.ToPersianDate(info.Date, "");
            }
        }
    }

    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void doMorgen_Click(object sender, EventArgs e)
    {
        sDateStart.SelectedDate = DateTime.Now.AddDays(1);// = Tool.ToPersianDate(DateTime.Now, "");
        sDateEnd.SelectedDate = DateTime.Now.AddDays(1);// Tool.ToPersianDate(DateTime.Now.AddMonths(1), "");
        BindData();
    }
    protected void doCreate_Click(object sender, EventArgs e)
    {
        Rate rate = Helper.Instance.GetRate(Tool.GetInt(sRates.SelectedValue, Null.NullInteger));
        List<Trip> all = GetTrips();
        if (all == null || all.Count == 0)
        {
            mError.Text = "سرویسی برای ایجاد وجود ندارد.";
            return;
        }
        mError.Text = "";
        int done = 0;
        int rateID = Null.NullInteger;// rate == null ? Null.NullInteger : rate.ID;
        Trip lastDone = null;
        for (int i = 0; i < all.Count; i++)
        {
            Trip info = all[i];
            RateItem rateItem = Helper.Instance.GetRateItemBySpecial(rateID, info.Service.Path.SrcCityID, info.Service.Path.DestCityID, info.Service.SpecialID, info.Date);
            if (null != rateItem)
            {
                info.Price = rateItem.Cost;
                info.Toll = rateItem.Toll;
                info.Insurance = rateItem.Insurance;
                info.Reception = rateItem.ReceptionCost;
            }

        }
        try
        {
            Helper.Instance.DB.Trips.InsertAllOnSubmit(all);
            if (!Helper.Instance.Update())
                mError.Text += "<br>" + Helper.Instance.LastException.Message;
            else
            {
                lastDone = all[all.Count - 1];
                done++;
                //  Helper.Instance.Init();
            }
        }
        catch (Exception ex)
        {
            mError.Text += "<br>" + ex.Message;
        }
        mError.Text = string.Format("تعداد {0} سرویس از کل  {1} سرویس ایجاد شد. <br>{2}", all.Count, all.Count, mError.Text);
        if (lastDone != null)
        {
            //bool ok = Helper.Instance.UpdateSetting("SeriesStart", lastDone.No.ToString());
            //try
            //{
            //    string[] vals = lastDone.Series.Split(',');
            //    ok = Helper.Instance.UpdateSetting("SeriesNo1", vals[0]);
            //    ok = Helper.Instance.UpdateSetting("SeriesNo2", vals[1]);
            //    ok = Helper.Instance.UpdateSetting("SeriesNo3", vals[2]);
            //    ok = Helper.Instance.UpdateSetting("SeriesNo4", vals[3]);
            //}
            //catch { }

        }
    }
}
