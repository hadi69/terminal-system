﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Exclusives.aspx.cs" Inherits="Exclusives" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sPathID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        وضعیت کد رهگیری
                        <asp:CheckBox ID="mHasCode" runat="server" CssClass="T" Text='دارای کد' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mOffilne" runat="server" CssClass="T" Text='آفلاین' Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:TextBox ID="sPath2" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        راننده اول
                    </td>
                    <td>
                        <asp:DropDownList ID="sDriverID1" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="50px"
                            CssClass="CB"></asp:Button>&nbsp;<asp:Button ID="doNew" runat="server" Text="جدید"
                                OnClick="doNew_Click" Width="50px" CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <input type="hidden" runat="server" id="mIDToDelete" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                OnRowDeleting="list_RowDeleting" Style="margin-top: 0px" Width="95%" AllowPaging="True"
                PagerSettings-Mode="Numeric" OnPageIndexChanging="list_PageIndexChanging" PageSize="20">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="DepartureDate" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime" HeaderText="ساعت" />
                    <asp:BoundField DataField="PathID" HeaderText="مسير" />
                    <asp:BoundField DataField="CarID" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverID1" HeaderText="راننده1" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                    <asp:BoundField DataField="NumPassangers" HeaderText="ت مسافر" />
                    <asp:BoundField DataField="TrackCode" HeaderText="کد رهگیری" />
                    <asp:HyperLinkField HeaderText="ويرايش" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="EditExclusive.aspx?id={0}"
                        Text="ويرايش" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" Visible=false />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <cc1:ModalPopupExtender ID="puEx" BehaviorID="puExBehaviorID" runat="server" TargetControlID="puDrag"
                PopupDragHandleControlID="puDrag" PopupControlID="puPanel" OkControlID="puCancel"
                DropShadow="true" CancelControlID="puCancel" BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    حذف صورت وضعیت</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                آیا میخواهید علاوه بر حذف، شماره صورت هم باطل شود
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="بله" CssClass="CB" Width="60"></asp:Button>&nbsp;
                                <asp:Button ID="puNo" ValidationGroup="addGroup" OnClick="puNo_Click" runat="server"
                                    Text="خیر" CssClass="CB" Width="60"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف" CssClass="CB" Width="60">
                                </asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label class="Err" runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
