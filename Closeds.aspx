﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Closeds.aspx.cs" Inherits="Closeds" Title="صورت وضعيت ها" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cp1">
    <asp:UpdatePanel ID="up2" runat="server">
        <ContentTemplate>
            <table border="0" class="filter" width="95%">
                <tr>
                    <td>
                        جستجو
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" style="text-align: left">
                        <asp:UpdateProgress ID="up2a" runat="Server" AssociatedUpdatePanelID="up2">
                            <ProgressTemplate>
                                <span class="wait">لطفا صبر کنيد ...</span>
                                <img src="wait.gif" width="20" alt="Please wait" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        سري صورت
                    </td>
                    <td>
                        <asp:DropDownList ID="sSeries" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        از شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        تا شماره صورت
                    </td>
                    <td>
                        <asp:TextBox ID="sNo2" runat="server" CssClass="T" Width="150px" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        مقصد
                    </td>
                    <td>
                        <asp:DropDownList ID="sDestID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع
                    </td>
                    <td>
                        <asp:CheckBox ID="mBus" runat="server" CssClass="T" Text='اتوبوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mMini" runat="server" CssClass="T" Text='مینی بوس' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mSewari" runat="server" CssClass="T" Text='سواری' Checked="true" />
                    </td>
                    <td>
                       وضعیت کد رهگیری
                    </td>
                    <td>
                        <asp:CheckBox ID="mHasCode" runat="server" CssClass="T" Text='دارای کد' Checked="true" />&nbsp;
                        <asp:CheckBox ID="mOffilne" runat="server" CssClass="T" Text='آفلاین' Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        از تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateStart" runat="server" Width="150px" />
                    </td>
                    <td>
                        تا تاريخ
                    </td>
                    <td>
                        <uc1:DatePicker ID="sDateEnd" runat="server" Width="150px" />
                    </td>
                    <td>
                        سرويس
                    </td>
                    <td>
                        <asp:DropDownList ID="sServiceID" runat="server" CssClass="DD" Width="250px" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        راننده اول
                    </td>
                    <td>
                        <asp:DropDownList ID="sDriverID1" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        نوع اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarTypeID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                    <td>
                        اتوکار
                    </td>
                    <td>
                        <asp:DropDownList ID="sCarID" runat="server" CssClass="DD" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left" colspan="6">
                        <asp:Button ID="doSearch" runat="server" Text="جستجو" OnClick="doSearch_Click" Width="60px"
                            CssClass="CB"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Label runat="server" ID="mMsg" class="Err" />
            <input type="hidden" runat="server" id="mIDToDelete" />
            <asp:GridView ID="list" runat="server" AutoGenerateColumns="False" OnRowDataBound="list_RowDataBound"
                Style="margin-top: 0px" Width="95%" OnRowDeleting="list_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="ID" Visible="False" />
                    <asp:BoundField DataField="ID" HeaderText="رديف" />
                    <asp:BoundField DataField="Date" HeaderText="تاریخ" />
                    <asp:BoundField DataField="DepartureTime2" HeaderText="ساعت" />
                    <asp:BoundField DataField="ServiceType" HeaderText="نوع سرويس" />
                    <asp:BoundField DataField="SrcCity" HeaderText="مسير" />
                    <asp:BoundField DataField="CarTitle" HeaderText="ماشين" />
                    <asp:BoundField DataField="DriverTitle" HeaderText="راننده1" />
                    <asp:BoundField DataField="No" HeaderText="شماره صورت" />
                    <asp:BoundField DataField="Series" HeaderText="سري صورت " />
                    <asp:BoundField DataField="NumTickets" HeaderText="ت بليط" />
                    <asp:BoundField DataField="TrackCode" HeaderText="کد رهگیری" />
                    <asp:HyperLinkField HeaderText=" مسافرين " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Passengers.aspx?tripid={0}"
                        Text="مسافرين" />
                    <asp:HyperLinkField HeaderText=" بليطها " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Tickets.aspx?tripid={0}"
                        Text="بليطها" />
                    <asp:HyperLinkField HeaderText="صورت وضعيت" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="Close.aspx?tripid={0}"
                        Text="صورت وضعيت" />
                    <asp:HyperLinkField HeaderText=" راهنما " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="PrintHelp.aspx?tripid={0}"
                        Text="راهنما" Target="_blank" />
                    <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    <br />
                    <span class="BErr">براي جستجو موردي وجود ندارد</span></EmptyDataTemplate>
                <HeaderStyle CssClass="GH" />
                <RowStyle CssClass="GR" />
                <AlternatingRowStyle CssClass="GAR" />
            </asp:GridView>
            <div style="display: none">
                <asp:Button ID="dummyShowUpdate" runat="server" Text="Button"></asp:Button></div>
            <cc1:ModalPopupExtender ID="puEx"  BehaviorID="puExBehaviorID"  runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
                PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
                BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
            </cc1:ModalPopupExtender>
            <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
                <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
                    حذف صورت وضعیت</asp:Panel>
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="N">
                                آیا میخواهید علاوه بر حذف، شماره صورت هم باطل شود
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="puOk" ValidationGroup="addGroup" OnClick="puOk_Click" runat="server"
                                    Text="بله" CssClass="CB" Width="60"></asp:Button>&nbsp;
                                <asp:Button ID="puNo" ValidationGroup="addGroup" OnClick="puNo_Click" runat="server"
                                    Text="خیر" CssClass="CB" Width="60"></asp:Button>&nbsp;
                                <asp:Button ID="puCancel" runat="server" Text="انصراف" CssClass="CB" Width="60">
                                </asp:Button>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Err" colspan="3">
                                <asp:Label class="Err" runat="server" ID="mError" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
