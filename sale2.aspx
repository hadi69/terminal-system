﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="sale2.aspx.cs" Inherits="sale2" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Modules/DatePicker.ascx" TagName="DatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" runat="Server">
    <table border="0">
        <tr>
            <td class="Title" colspan="11">فروش سرويس
                <asp:Label ID="mTripTitle" runat="server" />
                &nbsp;-&nbsp;<asp:HyperLink ID="doClose" runat="server" Text='صورت وضعيت F4' />&nbsp;-&nbsp;<asp:HyperLink
                    ID="doRefresh" runat="server" Text='فراخوانی' />&nbsp;-&nbsp;<asp:HyperLink NavigateUrl='Trips.aspx'
                        runat="server" ID="doTechnical" Target="_blank">فهرست کنترل اجزاء فنی</asp:HyperLink></td>
        </tr>
        <tr>
            <td>نام
            </td>
            <td>
                <asp:TextBox ID="mName" runat="server" CssClass="BT" Width="171px" />
            </td>
            <td>تعداد
            </td>
            <td>
                <asp:TextBox ID="mNum" runat="server" CssClass="BT" Width="40px" Text="1" />
            </td>
            <td>قیمت &nbsp;
            </td>
            <td>
                <asp:TextBox ID="mPrice" runat="server" CssClass="BT" Width="80px" />
                <asp:DropDownList runat="server" ID="mFundID" CssClass="DD" Width="50px" Visible="False" />
            </td>
            <td style="text-align: left">
                <asp:DropDownList runat="server" ID="mBranchID" CssClass="DD" Width="50px" Visible="False" />
            </td>
            <td>لیست مسافرین &nbsp;
            </td>
            <td>
                <asp:DropDownList runat="server" ID="mPassengerList" CssClass="DD" Width="200px" />
            </td>
            <td colspan="2">
                <asp:Button runat="server" ID="doAutoFill" Text='تکمیل سرویس F12' Width="125px" CssClass="CBBJ"
                    OnClick="doAutoFill_Click" />
                <asp:Button ID="doSale2" runat="server" CssClass="CBBJ" OnClick="doSale2_Click" Text="فروش  F2"
                    Width="100px" />
                <asp:Button runat="server" ID="doSale" Text='فروش با چاپ' Width="10px" CssClass="CBBJ"
                    OnClick="doSale_Click" Visible="False" />
                <asp:Button ID="doReturn" runat="server" Text='برگشت' OnClick='doReturn_Click' Width="100px"
                    CssClass="CBBJ" />
                <asp:Button ID="doNew" runat="server" Text='جدید INS' OnClick='doNew_Click' Width="100px"
                    CssClass="CBBJ" />
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="11" valign="top">
                <p style="margin-bottom: 0cm; text-align: left; font-size: 12px; font-weight: bold; font-family: tahoma">
                    <span style="font-weight: normal">F7=نام، F9=بستن و چاپ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>مسافر:
                    <span id="passName"></span>
                    <br />
                    <span style="color: #009933">کل:
                        <asp:Label ID="mAll" runat="server" />
                    </span>&nbsp;&nbsp; <span style="color: #FF0000">پر:
                        <asp:Label ID="mFull" runat="server" />
                    </span>&nbsp;&nbsp; خالی:
                    <asp:Label runat="server" ID="mNumEmpty" />
                </p>
            </td>
        </tr>
        <tr>
            <td class="BErr" colspan="9">
                <asp:Label ID="mWarnings" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="6" class="BErr">
                <asp:GridView ID="mGrid" runat="server" AutoGenerateColumns="False" OnRowUpdating="mGrid_RowUpdating"
                    OnRowEditing="mGrid_RowEditing" OnRowDeleting="mGrid_RowDeleting" OnRowDataBound="mGrid_RowDataBound"
                    CellPadding="4" OnRowCancelingEdit="mGrid_RowCancelingEdit">
                    <Columns>
                        <asp:BoundField DataField="ID" Visible="False" />
                        <asp:BoundField DataField="Fullname" HeaderText="نام" />
                        <asp:BoundField DataField="NumChairs" HeaderText="تعداد" />
                        <asp:BoundField DataField="Price" HeaderText="قیمت" />
                        <asp:BoundField DataField="ID" ReadOnly="true" HeaderText="صندوق" />
                        <asp:BoundField DataField="ID" ReadOnly="true" HeaderText="دفتر" />
                        <asp:HyperLinkField HeaderText=" چاپ " DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Reports/MiniTicket.aspx?id={0}"
                            Target="_blank" Text="چاپ" />
                        <asp:CommandField CancelText="انصراف" EditText="ویرایش" ShowEditButton="True" UpdateText="تایید" />
                        <asp:CommandField DeleteText="حذف" ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        <br />
                        <span class="BErr">هیچ بلیطی فروخته نشده است</span>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="GH" />
                    <RowStyle CssClass="GR" />
                    <AlternatingRowStyle CssClass="GAR" />
                </asp:GridView>
            </td>
            <td valign="top" colspan="4">
                <table border="0">
                    <tr>
                        <td class="N">کد مسیر
                        </td>
                        <td>
                            <asp:TextBox ID="mPathCode" runat="server" CssClass="BT" Width="60px" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="mPathTitle" CssClass="Value" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">کد راننده
                        </td>
                        <td>
                            <asp:TextBox ID="mDriverCode" runat="server" CssClass="BT" Width="60px" />
                            <asp:Button ID="doChooseDriver" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                                Width="100px" OnClick="doChooseDriver_Click" />
                            <asp:Button ID="do_DriverEdit" runat="server" CssClass="CB"
                                OnClick="do_DriverEdit_Click" Text="..." />
                            <asp:HiddenField runat="server" ID="mDriver" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="mDriverTitle" CssClass="Value" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">کد اتوکار
                        </td>
                        <td>
                            <asp:TextBox ID="mCarCode" runat="server" CssClass="BT" Width="60px" />
                            <asp:Button ID="doChooseAuto" runat="server" CssClass="CB" Height="20px" Text="خواندن از کارت"
                                Width="100px" OnClick="doChooseAuto_Click" />
                            <asp:Button ID="do_CarEdit" runat="server" CssClass="CB" Text="..."
                                OnClick="do_CarEdit_Click" />&nbsp;
                            <asp:Button ID="doCheckFani" runat="server" CssClass="CB" Text="استعلام فنی" OnClick="doCheckFani_Click" />
                            <asp:HiddenField runat="server" ID="mCar" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="mCarTitle" CssClass="Value" />
                        </td>
                    </tr>
                    <tr>
                        <td class="N">
                            <asp:Label runat="server" ID="lTime">
                                    زمان حرکت</asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lHour">ساعت</asp:Label>
                            <asp:TextBox ID="mHour" runat="server" CssClass="BT" Width="30px" />
                            <asp:Label runat="server" ID="lMin">دقیقه</asp:Label>
                            <asp:TextBox ID="mMinute" runat="server" CssClass="BT" Width="30px" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="N">
                            <asp:Label runat="server" ID="lDate">تاریخ حرکت</asp:Label>
                        </td>
                        <td>
                            <cc1:MaskedEditExtender ID="meeDate" TargetControlID="mDate" runat="server" Mask="1399/99/99"
                                ClearMaskOnLostFocus="false" AcceptAMPM="false" />
                            <asp:TextBox ID="mDate" runat="server" CssClass="BT" Width="120px" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>شماره صورت
                        </td>
                        <td colspan="2">
                            <table border="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="mNo" runat="server" CssClass="T" Enabled="true" Width="80" />
                                    </td>
                                    <td>سري صورت
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo1" Width="30" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs1" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo1" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>/
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo2" Width="30" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs2" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo2" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>/
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="mSeriesNo3" Width="30" MaxLength="3" CssClass="T" />
                                        <asp:RequiredFieldValidator ID="rfs3" runat="server" ValidationGroup="auG" ErrorMessage="*"
                                            ControlToValidate="mSeriesNo3" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="display: none">&nbsp; تاریخ و ساعت بستن<asp:Label ID="mCloseDate" runat="server" CssClass="BT" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0">
                                <tr>
                                    <td class="N">کد رهگیری:
                                    </td>
                                    <td>
                                        <asp:Label ID="mTrackCode" runat="server" CssClass="BT" />
                                    </td>
                                    <td class="N" style="display: none">تاریخ کد رهگیری:
                                    </td>
                                    <td style="display: none">
                                        <asp:Label ID="mTrackDate" runat="server" CssClass="BT" />
                                    </td>
                                    <td style="display: none">مقصد دوم: </td>
                                    <td style="display: none">

                                        <asp:TextBox ID="mSecondDistCity" runat="server"
                                            OnTextChanged="mSecondDistCity_TextChanged"></asp:TextBox>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: left">
                            <asp:Button runat="server" ID="doSave" Text='ذخیره' CssClass="CB" OnClick="doSave_Click"
                                Width="50px" OnClientClick='return disableMe(this);' />&nbsp;
                                <asp:Button runat="server" ID="doSaveClose" Text='ذخیره و بستن F7' CssClass="CB" OnClick="doSaveClose_Click" Width="100px" OnClientClick='return disableMe(this);' />&nbsp;
                            <asp:Button runat="server" ID="doSendToTechnical" Text="ارسال به مدیر فنی" CssClass="CB" Width="120px" OnClick="doSendToTechnical_Click" Visible="False" />
                            &nbsp;
                            <asp:Button runat="server" ID="doGetTrackingCode" Text="دریافت کد رهگیری" CssClass="CB" Width="120px" OnClick="doGetTrackingCode_Click" />
                            &nbsp;
                            <asp:Button runat="server" ID="doRequestInsurance" Text='ارسال به بیمه' CssClass="CB" Width="100px" OnClick="doRequestInsurance_Click" Visible="False" />
                            &nbsp;
                            <asp:Button runat="server" ID="doPrint" Text='بستن و چاپ F9' CssClass="CB" Width="100px" OnClick="doPrint_Click" />
                            &nbsp;
                            <asp:Button runat="server" ID="doPrint2" Text='بستن و چاپ' CssClass="CB" Width="90px" OnClick="doPrint2_Click" /><asp:CheckBox runat="server" ID="mShowDate" AutoPostBack="true" OnCheckedChanged="mShowDate_CheckedChanged" />
                            <asp:Button runat="server" ID="doSaveInCard" Text="نوشتن در کارت" CssClass="CB" Width="90px"
                                ValidationGroup="addGroup1" OnClick="doSaveInCard_Click" Visible="false" />
                            &nbsp;
                            <asp:Button runat="server" ID="doReadFromCard" Text="مشاهده کارت" CssClass="CB" Width="90px"
                                ValidationGroup="addGroup1" OnClick="doSaveInCard_Click" Visible="false" />
                            <div style="visibility: hidden">
                                <asp:Button runat="server" ID="doSetPath" Text='' CssClass="CB" OnClick="doSetPath_Click"
                                    Width="5px" />
                                <asp:Button runat="server" ID="doSetDriver" Text='' CssClass="CB" OnClick="doSetDriver_Click"
                                    Width="5px" />
                                <asp:Button runat="server" ID="doSetCar" Text='' CssClass="CB" OnClick="doSetCar_Click"
                                    Width="5px" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="10" class="Err">
                <asp:Label ID="mError" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="10">&nbsp;
                <asp:HyperLink ID="doShowPre" runat="server">قبلی</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="10">&nbsp;
                <asp:HyperLink ID="doShowNext" runat="server">بعدی</asp:HyperLink>
            </td>
        </tr>
    </table>
    <cc1:ModalPopupExtender ID="puEx" runat="server" TargetControlID="puDrag" PopupDragHandleControlID="puDrag"
        PopupControlID="puPanel" OkControlID="puCancel" DropShadow="true" CancelControlID="puCancel"
        BackgroundCssClass="mdlBck" RepositionMode="RepositionOnWindowResize">
    </cc1:ModalPopupExtender>
    <asp:Panel Style="display: none" ID="puPanel" runat="server" Width="450px" CssClass="modalPopup">
        <asp:Panel ID="puDrag" runat="server" Width="100%" CssClass="mdlDrg" Height="20px">
            اطلاعات ثبت شده روی کارت
        </asp:Panel>
        <table border="0">
            <tbody>
                <tr>
                    <td class="N">کد شرکت
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="company">
                    </td>
                </tr>
                <tr>
                    <td class="N">شماره صورت
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="surat">
                    </td>
                </tr>
                <tr>
                    <td class="N">تاریخ
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="date">
                    </td>
                </tr>
                <tr>
                    <td class="N">ساعت
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="time">
                    </td>
                </tr>
                <tr>
                    <td class="N">کد مبدا
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="source">
                    </td>
                </tr>
                <tr>
                    <td class="N">کد مقصد
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="distin">
                    </td>
                </tr>
                <tr>
                    <td class="N">مسافر
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="pass">
                    </td>
                </tr>
                <tr>
                    <td class="N">مبلغ
                    </td>
                    <td>
                        <input class="T" type="text" readonly="true" value="0" id="price" />
                    </td>
                </tr>
                <tr>
                    <td class="N"></td>
                    <td>
                        <asp:Button ID="puCancel" runat="server" Text="انصراف ESC" CssClass="CB"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td class="N" colspan="2">
                        <asp:Label ID="mWriteMsg" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
</asp:Content>
