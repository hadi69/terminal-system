﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Passengers : System.Web.UI.Page
{
    Trip mTrip = null;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        if (!IsPostBack)
        {
            this.TripID = Tool.GetInt(Request.QueryString["tripid"], Null.NullInteger);
            mTrip = Helper.Instance.GetTrip(this.TripID);
            if (mTrip == null)
                Response.Redirect("Trips.aspx", true);
            mTripTitle.Text = string.Format("{0} ({1}) ({2}) شماره: {3}", mTrip.Service.Path.Title, mTrip.Service.ServiceType2, Tool.ToPersianDate(mTrip.Date, ""), mTrip.ID);
        }
        else
            mTrip = Helper.Instance.GetTrip(TripID);
        if (!IsPostBack)
            BindData();
    }
    private void BindData()
    {
        if (!mTrip.Closed)
            doReturn.NavigateUrl = "Trips.aspx";
        else
            doReturn.NavigateUrl = "Closeds.aspx";

        List<Ticket> tickets = Helper.Instance.GetTickets(mTrip.ID);
        Layout layout = mTrip.CarType.Layout;
        if (string.IsNullOrEmpty(layout.Layout1))
            return;
        int index = 1;
        int len = mTrip.ID.ToString().Length + 1;
        for (int f = 0; f < layout.Floors; f++)
        {
            mLayout.Controls.Add(new LiteralControl(string.Format("طبقه {0}:<br />", f + 1)));
            mLayout.Controls.Add(new LiteralControl("<table cellpadding=5 cellspacing=0 border=1 class='PrintHelp'>"));
            for (int i = 0; i < layout.Rows; i++)
            {
                mLayout.Controls.Add(new LiteralControl("<tr>"));
                for (int j = 0; j < layout.Columns; j++)
                {
                    string key = string.Format(";{0}_{1}_{2};", f, i, j);
                    if (layout.Layout1.Contains(key))
                    {
                        string code = @"
<span  style='cursor:pointer' title='TOOLTIP' BACK>
			&nbsp;&nbsp;
				<img id=cbPicINDX src='images/MAN.gif' >
				INDX TEXT
</span>".Replace("INDX", index.ToString());

                        bool man;
                        Ticket ticket = GetTicket(tickets, index, out man);
                        if (ticket == null)
                        {
                            #region Free
                            code = code.Replace("TOOLTIP", "Free");
                            code = code.Replace("MAN", "man");
                            code = code.Replace("BACK", "");
                            code = code.Replace("TEXT", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                            #endregion
                        }
                        else
                        {
                            #region Occupied
                            code = code.Replace("TEXT", string.Format("{0} ({1})", ticket.Fullname, ticket.No.Length>len?ticket.No.Substring(len):ticket.No));
                            code = code.Replace("TOOLTIP", string.Format("{0}\r\n شماره بلیط : {1}\r\nتاریخ : {2}\r\nساعت : {3}\r\nتوسط : {4}"
                                , ticket.Fullname, ticket.No.Substring(len)
                                , ticket.SaleDate.HasValue ? Tool.ToPersianDate(ticket.SaleDate, "") : ""
                                , ticket.SaleDate.HasValue ? ticket.SaleDate.Value.ToShortTimeString() : ""
                                , Helper.Instance.GetUserFullName(ticket.SaleUserID)
                                ));
                            if (man)
                                code = code.Replace("MAN", "man_disable");
                            else
                                code = code.Replace("MAN", "woman_disable");
                            if (ticket.SaleType == (int)SaleTypes.Reserve)
                                code = code.Replace("BACK", "style='background-color:#6666FF'");
                            else if (ticket.SaleType == (int)SaleTypes.OfficeSale || ticket.SaleType == (int)SaleTypes.Sale)
                                code = code.Replace("BACK", "style='background-color:#FFFF88'");
                            else
                                code = code.Replace("BACK", "");
                            #endregion
                        }

                        mLayout.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", code)));
                        index++;
                    }
                    else
                        mLayout.Controls.Add(new LiteralControl("<td>&nbsp;-&nbsp;</td>"));//style='{width:69px}'
                    if (layout.ColumnSpace == j + 1)
                        mLayout.Controls.Add(new LiteralControl("<td>&nbsp;-&nbsp;</td>"));
                }
                mLayout.Controls.Add(new LiteralControl("</tr>"));
            }
            mLayout.Controls.Add(new LiteralControl("</table>"));
        }
    }
    Ticket GetTicket(List<Ticket> tickets, int chairIndex, out bool man)
    {
        man = true;
        string mKey = string.Format(";m{0};", chairIndex);
        string fKey = string.Format(";f{0};", chairIndex);
        for (int i = 0; i < tickets.Count; i++)
            if (tickets[i].Chairs.Contains(mKey))
                return tickets[i];
            else if (tickets[i].Chairs.Contains(fKey))
            {
                man = false;
                return tickets[i];
            }
        return null;
    }
    protected int TripID
    {
        get
        {
            return Tool.GetInt(ViewState["TripID"], Null.NullInteger);
        }
        set
        {
            ViewState["TripID"] = value;
        }
    }
}
