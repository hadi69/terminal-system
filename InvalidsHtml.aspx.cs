﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class InvalidsHtml : System.Web.UI.Page
{
    const int rowsPerPage = 26;
    protected void Page_Load(object sender, EventArgs e)
    {

        Helper.Instance.Init();
        Helper.Instance.LastException = null;
        DataTable dt = GetTripsDT();
        if (dt == null && Helper.Instance.LastException != null)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + Helper.Instance.LastException.Message + "<br />StackTrace: " + Helper.Instance.LastException.StackTrace));
            return;
        }
        if (dt == null || dt.Rows.Count == 0)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("براي جستجو موردي وجود ندارد"));
            return;
        }
        try
        {
            StringBuilder builder = new StringBuilder();

            int i = 0, page = 1;
            bool closed = true;
            for (; i < dt.Rows.Count; i++)
            {
                if (i % rowsPerPage == 0)
                {
                    if (builder.Length > 0)
                    {
                        // Close Table
                        //builder.AppendFormat("<tr hight='15'><td  dir='rtl' align='middle' width='60' hight='15'>{0}<br>{1}<br>{2}</td><td  dir='rtl' align='middle' width='60' hight='15'>{3}<br>{4}<br>{5}</td><td align='middle' colspan='6'>&nbsp;</td></tr>\r\n"
                        //    , sumBodyInsurance, sumBodyInsuranceWage, sumBodyInsurance - sumBodyInsuranceWage
                        //    , sumInsurance, sumInsuranceWage, sumInsurance - sumInsuranceWage);
                        builder.AppendLine("</tbody></table>");
                        closed = true;
                    }
                    // Start New
                    closed = false;
                    if (dt.Rows.Count - i <= rowsPerPage)
                        builder.AppendLine("<table width='580' align='center' border=1 cellspacing=0  dir='rtl'><tbody>");
                    else
                        builder.AppendLine("<table style='page-break-after: always' width='580' align='center' cellspacing=0 border=1  dir='rtl'><tbody>");
                    builder.AppendLine("<tr><td colspan='10'>&nbsp;</td></tr>");
                    builder.AppendFormat("<tr><td colspan='10'><div align='left'><strong>{0}</strong></div></td></tr>", Helper.Instance.GetSettingValue("CompanyName"));
                    builder.AppendFormat("<tr><td dir='rtl' align='middle' colspan='10'>صفحه {0}</td></tr>", page++);
                    builder.AppendLine("<tr><td colspan='10' height='50'>&nbsp;</td></tr>");
                    builder.Append("<tr><td>رديف</td><td>تاریخ</td><td>ساعت</td><td>نوع سرويس</td><td>مسير</td><td>ماشين</td><td>راننده1</td><td>شماره صورت</td><td>سري صورت </td><td>دربستی</td></tr>");
                }

                string series = Tool.GetString(dt.Rows[i]["Series"], "");
                if (series != null && series.Length > 0)
                {
                    StringBuilder b = new StringBuilder();
                    b.Append("<table border=0 cellpading=0 cellspacing=0 dir=rtl class=tt><tr>");
                    string[] ss = series.Split(',');
                    if (ss.Length == 4)
                        b.AppendFormat("<td>{0}</td><td>-</td>", ss[3]);
                    if (ss.Length >= 3)
                        b.AppendFormat("<td>{0}</td><td>/</td><td>{1}</td><td>/</td><td>{2}</td>", ss[0], ss[1], ss[2]);
                    b.Append("</tr></table>");
                    series = b.ToString();
                }

                builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td></tr>"
                    , i + 1
                    , Tool.ToPersianDate(dt.Rows[i]["Date"], "")
                    , dt.Rows[i]["DepartureTime"]
                    , Tool.ToString((ServiceTypes)Tool.GetInt(dt.Rows[i]["ServiceType"]))
                    , Tool.GetString(dt.Rows[i]["SrcCity"], "") + " -> " + Tool.GetString(dt.Rows[i]["DestCity"], "")
                    , dt.Rows[i]["CarTitle"]
                    , dt.Rows[i]["DriverTitle"]
                    , dt.Rows[i]["No"]
                    , series
                    , Tool.GetBool(dt.Rows[i]["Exclusive"], false) ? "X" : "");
            }
            if (!closed)
            {
                builder.AppendLine("</tbody></table><table border=0 cellspacing=0>");
                while (i++ % rowsPerPage != 0)
                    builder.Append(" <tr dir=rtl><td class=rowNoLine align=middle colSpan=10>&nbsp;</td></tr>");
                // Close Table
                //builder.AppendFormat("<tr hight='15'><td  dir='rtl' align='middle' width='60' hight='15'>{0}<br>{1}<br>{2}</td><td  dir='rtl' align='middle' width='60' hight='15'>{3}<br>{4}<br>{5}</td><td align='middle' colspan='6'>&nbsp;</td></tr>\r\n"
                //    , sumBodyInsurance, sumBodyInsuranceWage, sumBodyInsurance - sumBodyInsuranceWage
                //    , sumInsurance, sumInsuranceWage, sumInsurance - sumInsuranceWage);
                builder.AppendLine("</tbody></table>");
            }
            mPlaceHolder.Controls.Clear();
            mPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + ex.Message + "<br />StackTrace: " + ex.StackTrace));
        }

        Helper.Instance.Dispose();

    }
    private DataTable GetTripsDT()
    {
        //"fromDate={0}&toDate={1}&serviceID={2}&driverID={3}&carID={4}&series={5}&no={6}",
        string fromDate = Tool.GetString(Request.QueryString["fromDate"], "");
        string toDate = Tool.GetString(Request.QueryString["toDate"], "");
        int serviceID = Tool.GetInt(Request.QueryString["serviceID"], 0);
        int driverID = Tool.GetInt(Request.QueryString["driverID"], 0);
        int carID = Tool.GetInt(Request.QueryString["carID"], 0);
        string no = Tool.GetString(Request.QueryString["no"], "");
        string series = Tool.GetString(Request.QueryString["series"], "");

        // DateTime dt1 = DateTime.Now;
        #region old
        string qryOLD = string.Format(@"SELECT InValids.*
    , Services.DepartureTime
	, Services.ServiceType
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
FROM InValids
INNER JOIN Services ON Services.ID=InValids.ServiceID
INNER JOIN Paths ON Services.PathID=Paths.ID
LEFT JOIN Drivers ON InValids.DriverID1=Drivers.ID
LEFT JOIN Cars ON InValids.CarID=Cars.ID
INNER JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
WHERE InValids.Date>= '{0}' AND InValids.Date<='{1} 23:59:59'
"
            , fromDate, toDate);
        #endregion
        string qry = string.Format(@"SELECT * FROM 
(
SELECT InValids.*
    , Services.ServiceType, Services.DepartureTime
    , ISNULL(Exclusives.DepartureTime, Services.DepartureTime) AS DepartureTime2 
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
FROM InValids
LEFT outer JOIN Exclusives ON InValids.TripID=Exclusives.ID
LEFT outer JOIN Services ON Services.ID=InValids.ServiceID
left outer JOIN Paths ON Services.PathID=Paths.ID
LEFT outer JOIN Drivers ON InValids.DriverID1=Drivers.ID
LEFT outer JOIN Cars ON InValids.CarID=Cars.ID
left outer JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
WHERE InValids.Exclusive=1

UNION
SELECT InValids.*
    , Services.ServiceType, Services.DepartureTime
    , ISNULL(Trips.DepartureTime, Services.DepartureTime) AS DepartureTime2 
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.SrcCityID) AS SrcCity
	, (SELECT TOP 1 Cities.Title FROM Cities WHERE Cities.ID=Paths.DestCityID) AS DestCity
	, (CarTypes.Title + ' ' + Cars.Plate) AS CarTitle
	, (Drivers.[Name] + ' ' + Drivers.[Surname]) AS DriverTitle
FROM InValids
LEFT outer JOIN Trips ON InValids.TripID=Trips.ID
LEFT outer JOIN Services ON Services.ID=InValids.ServiceID
left outer JOIN Paths ON Services.PathID=Paths.ID
LEFT outer JOIN Drivers ON InValids.DriverID1=Drivers.ID
LEFT outer JOIN Cars ON InValids.CarID=Cars.ID
left outer JOIN CarTypes ON Cars.CarTypeID=CarTypes.ID
WHERE InValids.Exclusive IS NULL OR InValids.Exclusive=0
) t
WHERE t.Date>= '{0}' AND t.Date<='{1} 23:59:59'
"
            , fromDate, toDate);
        if (serviceID > 0)
            qry += " AND t.ServiceID=" + serviceID;
        if (driverID > 0)
            qry += " AND t.DriverID1=" + driverID;
        if (carID > 0)
            qry += " AND t.CarID=" + carID;
        if (!string.IsNullOrEmpty(series))
            qry += " AND t.Series LIKE N'%" + series + "%'";


        DataTable dt = Helper.Instance.FillDataTable(qry + " ORDER BY t.Date");
        if (dt != null && dt.Rows.Count > 0)
        {
            if (no.Trim().Length > 0)
            {
                string s = no.Trim();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int no1 = Tool.GetInt(dt.Rows[i]["No"], -1);
                    if (!no1.ToString().Contains(s))
                    {
                        dt.Rows.RemoveAt(i);
                        i--;
                    }
                }
            }
        }
        // mMsg.Text = msg;
        return dt;
    }
}
