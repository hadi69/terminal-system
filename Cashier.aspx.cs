﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class Cashier : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        //if (!IsPostBack)
        //    this.TicketID = Tool.GetInt(Request.QueryString["ticketid"], Null.NullInteger);
        if (!IsPostBack)
        {
            sDateStart.SelectedDate = DateTime.Now;
            sDateEnd.SelectedDate = DateTime.Now;
            BindData();
        }
    }
    protected int TicketID
    {
        get
        {
            return Tool.GetInt(ViewState["TicketID"], Null.NullInteger);
        }
        set
        {
            ViewState["TicketID"] = value;
        }
    }
    protected void list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Ticket info = e.Row.DataItem as Ticket;
            if (info != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                // e.Row.Cells[1].Text = (e.Row.RowIndex + 1 + list.PageIndex * list.PageCount).ToString();
                int num = info.GetNumTickets();
                e.Row.Cells[5].Text = num.ToString();
                e.Row.Cells[6].Text = (info.Price * num).ToString();
                e.Row.Cells[7].Text = info.City.Title;
                e.Row.Cells[8].Text = info.Trip.DepartureTime2;
                e.Row.Cells[9].Text = Helper.Instance.GetUserFullName(info.SaleUserID);
                e.Row.Cells[10].Text = info.Trip.No.ToString();
                e.Row.Cells[11].Text = info.Trip.Closed ? "بسته" : "باز";
                e.Row.Cells[12].Text = Tool.ToPersianDate(info.Trip.Date, "");
                if (info.Trip.Closed)
                    e.Row.Cells[13].Text = "";
                else if (info.PaidBack != 0)
                    e.Row.Cells[13].Text = "استرداد شده";
            }
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    void BindData()
    {
        var all = from c in Helper.Instance.DB.Tickets
                  where c.PaidBack == 0 && c.SaleType!=1
                  select c;

        DateTime fromDate = sDateStart.SelectedDate;//Tool.ParsePersianDate(sDateStart.Text, new DateTime());
        if (fromDate != new DateTime())
            all = all.Where(c => c.Trip.Date >= fromDate);
        DateTime toDate = sDateEnd.SelectedDate;// Tool.ParsePersianDate(sDateEnd.Text, new DateTime());
        if (toDate != new DateTime())
            all = all.Where(c => c.Trip.Date <= toDate);
        if (!string.IsNullOrEmpty(sFullname.Text))
        {
            all = all.Where(c => c.Fullname.Contains(sFullname.Text));
        }
        if (!string.IsNullOrEmpty(sNo.Text))
        {
            all = all.Where(c => c.No.Contains(sNo.Text));
        }
        if (!sPrinted.Checked)
        {
            all = all.Where(c => c.Printed == null || c.Printed == false);
        }
        try
        {
            list.DataSource = all;
        }
        catch
        {
            list.DataSource = Helper.Instance.GetTickets(sNo.Text);
        }
        list.DataBind();
    }
    protected void list_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (list.SelectedIndex < 0)
            return;
        int id = Tool.GetInt(list.Rows[list.SelectedIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;

        string open = @"
<script language='javascript' type='text/javascript'>
function openprint()
{
    window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=300,height=550,width=430');
}
window.open('Reports/Ticket.aspx?id=IDID','Surat', 'location=no,toolbar=no,scrollbars=no,menubar=no,status=no,top=50,left=50,height=550,width=900');
//window.onload=openprint;
</script>".Replace("IDID", id.ToString());
        Page.RegisterStartupScript("openprint", open);

        Ticket ticket = Helper.Instance.GetTicket(id);
        if (ticket.Printed!=true)
        {
            ticket.FundUserID = SiteSettings.UserID;
        }
        ticket.Printed = true;
        Helper.Instance.Update();
        BindData();
    }
}