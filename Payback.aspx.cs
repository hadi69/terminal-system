﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Payback : System.Web.UI.Page
{
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //mError.Text = "";
        if (!IsPostBack)
        {
            this.TicketID = Tool.GetInt(Request.QueryString["ticketid"], Null.NullInteger);
            doReturn.Visible = false;
            if (this.TicketID != Null.NullInteger)
            {
                Ticket t = Helper.Instance.GetTicket(this.TicketID);
                if (t != null)
                {
                    doReturn.Visible = true;
                    doReturn.NavigateUrl = "tickets.aspx?tripid=" + t.TripID;
                }
            }
        }
        if (!IsPostBack)
            BindData();
    }
    protected int TicketID
    {
        get
        {
            return Tool.GetInt(ViewState["TicketID"], Null.NullInteger);
        }
        set
        {
            ViewState["TicketID"] = value;
        }
    }
    void BindData()
    {
        Ticket t = Helper.Instance.GetTicket(this.TicketID);
        if (t == null)
        {
            doPayback.Enabled = doSubtract.Enabled = false;
            mClosed.Text = mDate.Text = mName.Text = mChairs.Text = mService.Text = "";
            mFromFund.Enabled = mFromSale.Enabled = false;
        }
        else
        {
            doPayback.Enabled = doSubtract.Enabled = true;
            mClosed.Text = t.Trip.Closed ? "بسته" : "باز";
            mDate.Text = Tool.ToPersianDate(t.Trip.Date, "");
            mName.Text = t.Fullname;
            mChairs.Text = t.Chairs == null ? "" : t.Chairs.Replace("f", "").Replace("m", "").Replace(";", "-").Trim('-'); ;
            mService.Text = t.Trip.Service.Title;
            mFromFund.Enabled = mFromSale.Enabled = t.SaleType == (int)SaleTypes.OfficeSale;
        }
        mMsg.Text = "";
    }
    protected void doSubtract_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(this.TicketID);
        if (t == null)
            return;
        if (t.Trip.Closed)
        {
            mMsg.Text = "سرویس مرتبط بسته شده است و نمیتوان این بلیط را استرداد کرد.";
            return;
        }
        if (t.PaidBack != 0)
        {
            mMsg.Text = "این بلیط قبلأ استرداد شده است.";
            return;
        }
        if (t.SaleType == (int)SaleTypes.Sale)
            t.PaidBack = (int)PaidBacks.FromFund;
        else
            t.PaidBack = mFromSale.Checked ? (int)PaidBacks.FromFund : (int)PaidBacks.FromBranch;
        t.PaidBackPercent = t.Trip.Closed ?(int) Helper.Instance.GetSettingValue("PaidBackPercentClosed", 0)
            : (int)Helper.Instance.GetSettingValue("PaidBackPercentOpen", 0);
        doPayback.Enabled = doSubtract.Enabled = true;
        t.SaleUserID = SiteSettings.UserID;
        if (Helper.Instance.Update())
        {
            mMsg.Text = "استرداد با کسر انجام شد";
        }
        else
        {
            mMsg.Text = "اشکال در استرداد";
        }
    }
    protected void doPayback_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(this.TicketID);
        if (t == null)
            return;
        if (t.Trip.Closed)
        {
            mMsg.Text = "سرویس مرتبط بسته شده است و نمیتوان این بلیط را استرداد کرد.";
            return;
        }
        if (t.PaidBack != 0)
        {
            mMsg.Text = "این بلیط قبلأ استرداد شده است.";
            return;
        }

        if (t.SaleType == (int)SaleTypes.Sale)
            t.PaidBack = (int)PaidBacks.FromFund;
        else
            t.PaidBack = mFromSale.Checked ? (int)PaidBacks.FromFund : (int)PaidBacks.FromBranch;
        t.PaidBackPercent = 0;
        doPayback.Enabled = doSubtract.Enabled = true;
        t.SaleUserID = SiteSettings.UserID;
        t.SaleDate = DateTime.Now;
        if (Helper.Instance.Update())
        {
            //Payment pay = new Payment();
            //pay.Type = (int)PaymentTypes.Payback;
            //pay.Amount 
            mMsg.Text = "استرداد بي کسر انجام شد";
        }
        else
        {
            mMsg.Text = "اشکال در استرداد";
        }
    }
    protected void doSearch_Click(object sender, EventArgs e)
    {
        Ticket t = Helper.Instance.GetTicket(sNo.Text);
        this.TicketID = t == null ? Null.NullInteger : t.ID;
        BindData();
    }
}
