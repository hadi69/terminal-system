﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;

public partial class EditExclusive : System.Web.UI.Page
{
    Exclusive mExclusive = null;
    bool enableClose = true;
    public static bool calcu = false;
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        Helper.Instance.Dispose();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mError.Text = "";
        if (!IsPostBack)
        {
            this.ExclusiveID = Tool.GetInt(Request.QueryString["id"], Null.NullInteger);
            mExclusive = Helper.Instance.GetExclusive(this.ExclusiveID);
            if (mExclusive == null)
            {
                mExclusiveTitle.Text = "جدید";
                //**doPrint.Enabled = doJustSave.Enabled = doInvalid.Enabled = doSave.Enabled = false;
            }
            else
            {
                mExclusiveTitle.Text = string.Format("{0} ({1}) شماره {2}", mExclusive.Path.Title, Tool.ToPersianDate(mExclusive.DepartureDate, ""), mExclusive.ID);
                calcu = true;
            }
        }
        else
            mExclusive = Helper.Instance.GetExclusive(ExclusiveID);
        if (!IsPostBack)
        {
            BindInitData();
            BindData();
        }

        if (!ButtonIsClicked(doJustSave))
            RegisterScripts();

        // mTotalRent.Attributes.Add("onKeyUp", "$get('sss').value=addCommas($get('sss').value);".Replace("sss", mTotalRent.ClientID));

    }
    void RegisterScripts()
    {
        doChangeSeries_NoCode.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این صورت را ابطال کنید؟"));
        doInvalid.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این صورت را ابطال کنید؟"));
        doInvalid2.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این شماره صورت را ابطال کنید؟"));
        doDelete.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "آیا مطمئن هستید که میخواهید این سرویس را حذف کنید؟"));

        #region Auto
        string reg = @"
        <script language='javascript'>
function CheckAuto(){
    document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRB.OmnikeyRB');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	 //کارت هوشمند 

    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseAuto.ClientID)
       .Replace("mSmartCard", mCar.ClientID);
        RegisterStartupScript("ReadClickScriptAuto", reg);
        doChooseAuto.Attributes.Add("OnClick", "CheckAuto()");
        #endregion

        #region Driver 1
        reg = @"
        <script language='javascript'>
function Check1(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseDriver1.ClientID)
       .Replace("mSmartCard", mDriver1.ClientID);
        RegisterStartupScript("ReadClickScript1", reg);
        doChooseDriver1.Attributes.Add("OnClick", "Check1()");
        #endregion

        #region Driver 2
        reg = @"
        <script language='javascript'>
function Check2(){
	document.getElementById('ReadCard').disabled = true;
	try{
	  var obj = new ActiveXObject('SmartRD.OmnikeyRD');
	}
	catch(e){
	  alert(e.message);
  	  document.getElementById('ReadCard').disabled = false;	  
	  return 0
	}
	
	 var flag = obj.SCEstablish;
     flag = obj.SCConnect;
	 if  (flag==false) {
	 	alert ('کارت اشکال دارد');
    	document.getElementById('ReadCard').disabled = false;		
		return 0;
	 } 
    var data = obj.SCGetNO_KART  // shomare kart
	document.getElementById('mSmartCard').value=data;	
    obj.SCDisconnect;
    obj.SCRelease;
	document.getElementById('ReadCard').disabled = false;	
}
</script>".Replace("ReadCard", doChooseDriver2.ClientID)
     .Replace("mSmartCard", mDriver2.ClientID);
        RegisterStartupScript("ReadClickScript2", reg);
        doChooseDriver2.Attributes.Add("OnClick", "Check2()");
        #endregion

    }

    private bool ButtonIsClicked(Button btn)
    {
        string clientID = btn.ClientID.Replace('_', '$');

        for (int i = 0; i < Request.Form.AllKeys.Length; i++)
        {
            if (Request.Form.AllKeys[i] == clientID)
                return true;
        }
        clientID = '$' + btn.ID;
        for (int i = 0; i < Request.Form.AllKeys.Length; i++)
        {
            if (Request.Form.AllKeys[i] != null && Request.Form.AllKeys[i].EndsWith(clientID))
                return true;
        }
        return false;
    }
    private void BindInitData()
    {
        {
            List<Car> all = Helper.Instance.GetCars();
            for (int i = 0; i < all.Count; i++)
            {
                if (all[i].Enabled == true)
                    continue;
                else
                {
                    all.RemoveAt(i);
                    i--;
                }
            }
            Car none = new Car();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            mCarID.DataValueField = "ID";
            mCarID.DataTextField = "Title";
            mCarID.DataSource = all;
            mCarID.DataBind();
        }
        {
            List<Driver> all = Helper.Instance.GetDrivers();
            for (int i = 0; i < all.Count; i++)
            {
                if (all[i].Enabled == true )
                    continue;
                else
                {
                    all.RemoveAt(i);
                    i--;
                }
            }
            Driver nonedriver = new Driver();
            nonedriver.ID = Null.NullInteger;
            all.Insert(0, nonedriver);

            mDriverID1.DataValueField = mDriverID2.DataValueField= mDriverID3.DataValueField = "ID";
            mDriverID1.DataTextField = mDriverID2.DataTextField =mDriverID3.DataTextField= "Title";
            mDriverID1.DataSource = mDriverID2.DataSource = mDriverID3.DataSource = all;
            mDriverID1.DataBind();

            Driver none = new Driver();
            none.ID = Null.NullInteger;
            all.Insert(0, none);

            mDriverID2.DataBind();
            mDriverID3.DataBind();
        }

        {
            List<Path> all = Helper.Instance.GetPaths();

            mPathID.DataValueField = "ID";
            mPathID.DataTextField = "Title";
            mPathID.DataSource = all;
            mPathID.DataBind();
        }
        {
            List<Fund> all = Helper.Instance.GetFunds();
            if (SiteSettings.User.IsAdmin == true)
                all = Helper.Instance.GetFunds();
            else
            {
                all = new List<Fund>();
                Fund f = Helper.Instance.GetFund(SiteSettings.User.FundID);
                if (f != null)
                    all.Add(f);
            }
            mFundID.DataValueField = "ID";
            mFundID.DataTextField = "Title";
            mFundID.DataSource = all;
            mFundID.DataBind();
        }
    }
    private void BindData()
    {
        BindDataInternal();
        int sFrom = -1, sTo = -1;
        sFrom = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartDFrom"), -1);
        sTo = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartDTo"), -1);

        if (sFrom == -1 || sTo == -1)
        {
            enableClose = false;
            mWarnings.Text += "در تنظیمات بازه سری صورت دربستی مشخص نشده است <br />";
        }
        int sCur = Tool.GetInt(mNo.Text, 0);
        if (sCur < sFrom || sCur > sTo)
        {
            mNo.BackColor = System.Drawing.Color.FromArgb(255, 10, 10);
            enableClose = false;
            mWarnings.Text += "سری صورت خارج از  بازه سری صورت در تنظیمات است <br />";
        }
        else
            mNo.BackColor = System.Drawing.Color.White;
        //**
        //if (mExclusive == null)
        //    enableClose = false;
        //**
        if (enableClose)
        {
            doPrint.Enabled = doJustSave.Enabled = enableClose;
        }
        else
        {
            doPrint.Enabled = doJustSave.Enabled = enableClose;
            mWarnings.Text += "-----------------> بستن و چاپ غيرفعال شده است";
        }

        if (mExclusive != null)
        {
            mTrackCode.Text = mExclusive.TrackCode;
            mTrackDate.Text = mExclusive.TrackDate.HasValue ? Tool.ToPersianDate(mExclusive.TrackDate.Value, "") : "";
        }

        doJustSave.Enabled = mExclusive != null;
        doPrint.Enabled = doPrint.Enabled && mExclusive != null && mExclusive.CloseDate.HasValue;

        if (mExclusive != null && !string.IsNullOrEmpty(mExclusive.TrackCode) && mExclusive.CloseDate.HasValue)
        {
            doCalc.Enabled = doChangeSeries_NoCode.Enabled = doJustSave.Enabled = EditMode;
        }

        doChangeSeries_NoCode.Enabled = doInvalid.Enabled = doEdit.Enabled = mExclusive != null;
        // ----------------------
        // Handle Invalidation + No
        if (mExclusive == null || mExclusive.No < 1) // it has no No yet
        {
            mNo.BackColor = System.Drawing.Color.Aqua;
            doChangeSeries_NoCode.Enabled = false;
            doInvalid.Enabled = false;
            doInvalid2.Enabled = true;
            doDelete.Enabled = true;
        }
        else if (string.IsNullOrEmpty(mExclusive.TrackCode))
        {
            doChangeSeries_NoCode.Enabled = true;
            doInvalid.Enabled = false;
            doInvalid2.Enabled = false;
            doDelete.Enabled = false;
        }
        else
        {
            doChangeSeries_NoCode.Enabled = false;
            doInvalid.Enabled = true;
            doInvalid2.Enabled = false;
            doDelete.Enabled = false;
        }
    }
    private void BindDataInternal()
    {
        if (mExclusive == null)
        {

            mSeriesNo1.Text = Helper.Instance.GetSettingValue("SeriesNoD1");
            mSeriesNo2.Text = Helper.Instance.GetSettingValue("SeriesNoD2");
            mSeriesNo3.Text = Helper.Instance.GetSettingValue("SeriesNoD3");
            string series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
            mNo.Text = Helper.Instance.GetNextSeriesStartD(series).ToString();
            //mDepartureTime.Text = Tool.FixTime(string.Format("{0}:{1}", DateTime.Now.Hour, DateTime.Now.Minute));
            string hm2 = Tool.FixTime(string.Format("{0}:{1}", DateTime.Now.Hour, DateTime.Now.Minute));
            mDepartureHour.Text = mDepartureMinute.Text = "";
            if (!string.IsNullOrEmpty(hm2))
                if (hm2.Contains(':'))
                {
                    mDepartureHour.Text = hm2.Split(':')[0];
                    mDepartureMinute.Text = hm2.Split(':')[1];
                }
            mDepartureDate.SelectedDate = DateTime.Now;
            mContractDate.SelectedDate = DateTime.Now;
            mToll.Text = "5";
            return;
        }
        Tool.SetSelected(mDriverID1, mExclusive.DriverID1);
        Tool.SetSelected(mDriverID2, mExclusive.DriverID2);
        Tool.SetSelected(mDriverID3, mExclusive.DriverID3);
        Tool.SetSelected(mCarID, mExclusive.CarID);
        Tool.SetSelected(mPathID, mExclusive.PathID);
        mPath2.Text = mExclusive.Path2;
        mViceVersa.Checked = mExclusive.ViceVersa;
        //mDepartureTime.Text = Tool.FixTime(mExclusive.DepartureTime);

        string hm = Tool.FixTime(mExclusive.DepartureTime);
        mDepartureHour.Text = mDepartureMinute.Text = "";
        if (!string.IsNullOrEmpty(hm))
            if (hm.Contains(':'))
            {
                mDepartureHour.Text = hm.Split(':')[0];
                mDepartureMinute.Text = hm.Split(':')[1];
            }

        mDepartureDate.SelectedDate = mExclusive.DepartureDate;
        mCarIsOwnerYes.Checked = mExclusive.CarIsOwner;
        mCarIsOwnerNo.Checked = !mExclusive.CarIsOwner;
        mContractNo.Text = mExclusive.ContractNo;

        mContractDate.SelectedDate = mExclusive.ContractDate.HasValue ? mExclusive.ContractDate.Value : Null.NullDate;

        mContractor.Text = mExclusive.Contractor.ToString();
        mDuration.Text = mExclusive.Duration.ToString();
        mNumPassangers.Text = mExclusive.NumPassangers.ToString();
        mTotalRent.Text = mExclusive.TotalRent.ToString("N0");
        mToll.Text = mExclusive.Toll.ToString();
        mInsurance.Text = mExclusive.Insurance.ToString();
        mBodyInsurance.Text = mExclusive.BodyInsurance.ToString();
        mCommission.Text = mExclusive.Commission.ToString();
        mPrice.Text = mExclusive.Price.ToString();
        mPecuniary.Text = mExclusive.Pecuniary.ToString();

        try
        {
            mBodyInsuranceWage.Text = Helper.Instance.GetWagePrice(0, int.Parse(mExclusive.Path.City1.Country.Code) > 1 ? true : false, mExclusive.Car.CarType.Layout.NumChairs > 30 ? 0 : mExclusive.Car.CarType.Layout.NumChairs > 5 ? 1 : 2, mExclusive.Path.KMs).ToString();
            mInsuranceWage.Text = Helper.Instance.GetWagePrice(1, int.Parse(mExclusive.Path.City1.Country.Code) > 1 ? true : false, mExclusive.Car.CarType.Layout.NumChairs > 30 ? 0 : mExclusive.Car.CarType.Layout.NumChairs > 5 ? 1 : 2, mExclusive.Path.KMs).ToString();
        }
        catch { }

        string series1 = mExclusive.Series;
        if (string.IsNullOrEmpty(mExclusive.Series) || mExclusive.Series.Replace(",", "").Trim().Length == 0)
            try
            {

                mSeriesNo1.Text = Helper.Instance.GetSettingValue("SeriesNoD1");
                mSeriesNo2.Text = Helper.Instance.GetSettingValue("SeriesNoD2");
                mSeriesNo3.Text = Helper.Instance.GetSettingValue("SeriesNoD3");
                series1 = string.Format("{0},{1},{2}"
                       , Helper.Instance.GetSettingValue("SeriesNoD1"), Helper.Instance.GetSettingValue("SeriesNoD2")
                       , Helper.Instance.GetSettingValue("SeriesNoD3"));

            }
            catch { }
        else
            try
            {
                string[] vals = mExclusive.Series.Split(',');
                mSeriesNo1.Text = vals[0];
                mSeriesNo2.Text = vals[1];
                mSeriesNo3.Text = vals[2];
            }
            catch { }
        mNo.Text = mExclusive.No.ToString();
        if (mExclusive.No < 1)
        {
            mNo.Text = Helper.Instance.GetNextSeriesStartD(series1).ToString();
            //mExclusive.No = Tool.GetInt(mNo.Text,0);
            //Helper.Instance.Update();
        }

        mProvince.Text = mExclusive.Province;
        mExtraPrice.Text = mExclusive.ExtraPrice.HasValue ? mExclusive.ExtraPrice.Value.ToString() : "0";
        mExtraPrice2.Text = mExclusive.ExtraPrice.HasValue ? (mExclusive.ExtraPrice.Value * mExclusive.NumPassangers).ToString() : "0";
        mToll2.Text = ((mExclusive.Toll * (mExclusive.TotalRent - mExclusive.Insurance - (mExclusive.ExtraPrice * mExclusive.NumPassangers) - Tool.GetInt(Helper.Instance.GetSettingValue("Stamp"), 0)) / 100) - (mExclusive.Toll * (mExclusive.TotalRent - mExclusive.Insurance - (mExclusive.ExtraPrice * mExclusive.NumPassangers) - Tool.GetInt(Helper.Instance.GetSettingValue("Stamp"), 0)) / 100 % 100)).ToString();

        //mSeries.Text = mExclusive.Series;


        Tool.SetSelected(mFundID, mExclusive.FundID);

        int sFrom = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartDFrom"), 0);
        int sTo = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartDTo"), int.MaxValue);
        int sCur = Tool.GetInt(mNo.Text, 0);
        if (sCur < sFrom || sCur > sTo)
            mNo.BackColor = System.Drawing.Color.FromArgb(255, 10, 10);
        else
            mNo.BackColor = System.Drawing.Color.White;

        mWarnings.Text = "";
        // bool enableClose = true;
        #region Check Dates - Drivers
        if (null != mExclusive.Driver)
        {
            int hDays = (int)(mExclusive.Driver.HourDate - DateTime.Now).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 1 اعتبار ندارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 1 فقط {0} روز اعتبار دارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(mExclusive.Driver.DrivingDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 1 اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 1 فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        Driver driver2 = mExclusive.DriverID2.HasValue ? Helper.Instance.GetDriver(mExclusive.DriverID2.Value) : null;
        if (null != driver2)
        {
            int hDays = (int) (driver2.HourDate - DateTime.Now).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 2 اعتبار ندارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 2 فقط {0} روز اعتبار دارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            int dDays = (int) (driver2.DrivingDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 2 اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 2 فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        Driver driver3 = mExclusive.DriverID3.HasValue ? Helper.Instance.GetDriver(mExclusive.DriverID3.Value) : null;
        if (null != driver3)
        {
            int hDays = (int)(driver3.HourDate - DateTime.Now).TotalDays;
            if (hDays < 0)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 3 اعتبار ندارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            else if (hDays <= 5)
            {
                mWarnings.Text += string.Format("دفترچه ساعت راننده 3 فقط {0} روز اعتبار دارد<br>", hDays);
                if (hDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(driver3.DrivingDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 3 اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("کارت سلامت راننده 3 فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
        }
        #endregion

        #region CheckDates - Auto
        if (null != mExclusive.Car)
        {
            int vDays = (int)(mExclusive.Car.VisitDate - DateTime.Now).TotalDays;
            if (vDays < 0)
            {
                mWarnings.Text += string.Format("معاینه فني اتوکار اعتبار ندارد<br>", vDays);
                if (vDays <= 1)
                    enableClose = false;
            }
            else if (vDays <= 5)
            {
                mWarnings.Text += string.Format("معاینه فني اتوکار فقط {0} روز اعتبار دارد<br>", vDays);
                if (vDays <= 1)
                    enableClose = false;
            }
            int dDays = (int)(mExclusive.Car.ThirdPersonDate - DateTime.Now).TotalDays;
            if (dDays < 0)
            {
                mWarnings.Text += string.Format("بيمه شخص ثالث اتوکار اعتبار ندارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            else if (dDays <= 5)
            {
                mWarnings.Text += string.Format("بيمه شخص ثالث اتوکار فقط {0} روز اعتبار دارد<br>", dDays);
                if (dDays <= 1)
                    enableClose = false;
            }
            int insurance = mExclusive.Insurance;
            int bodyinsurance = mExclusive.BodyInsurance;
            if (insurance == 0)
            {
                mWarnings.Text += "بیمه سرنشین 0 است.";
                enableClose = false;
            }
            if (bodyinsurance == 0 && mExclusive.Car.CarType.Layout.NumChairs > 25)
            {
                mWarnings.Text += "بیمه بدنه 0 است.";
                enableClose = false;
            }
            if (null != mExclusive && mExclusive.Price == 0)
            {
                mWarnings.Text += "ابتدا بر دکمه محاسبه کلیک کنید";
                enableClose = false;
            }
        }
        #endregion
        //mTrip.Driver mHourDate

        mCloseDate.Text = mExclusive.CloseDate.HasValue ? Tool.ToPersianDate(mExclusive.CloseDate.Value, "") : "";
        if (enableClose)
        {
            doPrint.Enabled = enableClose;
        }
        else
        {
            doPrint.Enabled = enableClose;
            mWarnings.Text += "----------------->  چاپ غيرفعال شده است";
        }
    }

    protected int ExclusiveID
    {
        get
        {
            return Tool.GetInt(ViewState["ExclusiveID"], Null.NullInteger);
        }
        set
        {
            ViewState["ExclusiveID"] = value;
        }
    }
    protected bool EditMode
    {
        get
        {
            return Tool.GetBool(ViewState["EditMode"], false);
        }
        set
        {
            ViewState["EditMode"] = value;
        }
    }
    bool SaveExclusive(bool close)
    {
        bool isNew = mExclusive.ID < 1;

        if (Helper.Instance.Update())
        {
            // Update Setting
            if (close && !isNew)
            {
                if (mExclusive.No > 0)
                {
                    int no = Tool.GetInt(Helper.Instance.GetSettingValue("SeriesStartD"), 1);
                    no = Math.Max(no, mExclusive.No);
                    Helper.Instance.UpdateSetting("SeriesStartD", no.ToString());
                }
                Helper.Instance.UpdateSetting("SeriesNoD1", mSeriesNo1.Text);
                Helper.Instance.UpdateSetting("SeriesNoD2", mSeriesNo2.Text);
                Helper.Instance.UpdateSetting("SeriesNoD3", mSeriesNo3.Text);
            }
            if (isNew && close)
            {
                enableClose = false;
                return false;
            }
            return true;
        }
        return false;
    }
    bool PrepareSave(bool close)
    {
        bool isNew = mExclusive == null;
        if (mExclusive == null)
            mExclusive = new Exclusive();

        mExclusive.DriverID1 = Tool.GetInt(mDriverID1.SelectedValue, Null.NullInteger);
        if (mDriverID2.SelectedIndex > 0)
            mExclusive.DriverID2 = Tool.GetInt(mDriverID2.SelectedValue, null);
        else
            mExclusive.DriverID2 = null;
        if (mDriverID3.SelectedIndex > 0)
            mExclusive.DriverID3 = Tool.GetInt(mDriverID3.SelectedValue, null);
        else
            mExclusive.DriverID3 = null;
        mExclusive.CarID = Tool.GetInt(mCarID.SelectedValue, Null.NullInteger);
        //mExclusive.Car = Helper.Instance.GetCar(mExclusive.CarID);
        mExclusive.PathID = Tool.GetInt(mPathID.SelectedValue, Null.NullInteger);
        mExclusive.Path2 = mPath2.Text;
        mExclusive.ViceVersa = mViceVersa.Checked;
        //mExclusive.DepartureTime = Tool.FixTime(mDepartureTime.Text);
        mExclusive.DepartureTime = Tool.FixTime(string.Format("{0}:{1}", mDepartureHour.Text.PadLeft(2, '0'), mDepartureMinute.Text.PadLeft(2, '0')));

        mExclusive.DepartureDate = mDepartureDate.SelectedDate;
        if (mExclusive.DepartureDate == Null.NullDate)
        {
            mError.Text += "تاریخ اشتباه است <br />";
            return false;
        }
        mExclusive.CarIsOwner = mCarIsOwnerYes.Checked;
        mExclusive.ContractNo = mContractNo.Text;
        mExclusive.ContractDate = mContractDate.SelectedDate;
        if (mExclusive.ContractDate == Null.NullDate)
            mExclusive.ContractDate = null;
        mExclusive.Contractor = mContractor.Text;
        mExclusive.Duration = mDuration.Text;
        mExclusive.NumPassangers = Tool.GetInt(mNumPassangers.Text, 0);
        mExclusive.TotalRent = Tool.GetInt(mTotalRent.Text, 0);
        mExclusive.Toll = Tool.GetInt(mToll.Text, 0);
        mExclusive.Insurance = Tool.GetInt(mInsurance.Text, 0);
        mExclusive.BodyInsurance = Tool.GetInt(mBodyInsurance.Text, mExclusive.BodyInsurance);
        mExclusive.Commission = Tool.GetInt(mCommission.Text, 0);
        mExclusive.Price = Tool.GetInt(mPrice.Text, 0);
        mExclusive.Pecuniary = Tool.GetInt(mPecuniary.Text, 0);
        //*mExclusive.No = Tool.GetInt(mNo.Text, 0);
        //mExclusive.Series = mSeries.Text;
        mExclusive.Series = string.Format("{0},{1},{2}", mSeriesNo1.Text, mSeriesNo2.Text, mSeriesNo3.Text);
        mExclusive.FundID = Tool.GetInt(mFundID.SelectedValue, Null.NullInteger);
        mExclusive.Province = mProvince.Text;
        mExclusive.ExtraPrice = Tool.GetInt(mExtraPrice.Text, 0);
        mExclusive.SentToInsurance = false;
        mExclusive.InsuranceWage = Tool.GetInt(mInsuranceWage.Text, 0);
        mExclusive.BodyInsuranceWage = Tool.GetInt(mBodyInsuranceWage.Text, 0);

        if (mExclusive.CarID < 1)
        {
            mError.Text += "اتوکار الزامی است. <br />";
            return false;
        }
        if (mExclusive.DriverID1 < 1)
        {
            mError.Text += "راننده اول الزامی است. <br />";
            return false;
        }
        if (mExclusive.PathID < 1)
        {
            mError.Text += "مسیر الزامی است.<br />";
            return false;
        }
        if (mExclusive.FundID < 1)
        {
            mError.Text += "صندوق الزامی است.<br />";
            return false;
        }
        //if (mExclusive.ID < 1)
        //{
        //    mExclusive.CloseDate = DateTime.Now;
        //    Helper.Instance.DB.Exclusives.InsertOnSubmit(mExclusive);
        //}
        if (mExclusive.No <= 0 && close)
        {
            mExclusive.No = Tool.GetInt(mNo.Text, 0);
            mExclusive.CloseDate = DateTime.Now;
            mExclusive.CloseUserID = SiteSettings.UserID;
        }
        if (mExclusive.ID < 1 && !close)
            Helper.Instance.DB.Exclusives.InsertOnSubmit(mExclusive);

        return true;
    }


    protected void doJustSave_Click(object sender, EventArgs e)
    {
        if (!PrepareSave(true))
            return;
        if (SaveExclusive(true))
        {
            mError.Text += "با موفقیت ذخیره شد";
            this.ExclusiveID = mExclusive.ID;
            try
            {
                string result = Helper.Instance.RequestInsurance(mExclusive);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(result);
                XmlNodeList msg = xml.GetElementsByTagName("Message");
                mError.Text = msg.Item(0).InnerText + "<br />";
                XmlNodeList status = xml.GetElementsByTagName("Status");
                XmlNodeList trackCode = xml.GetElementsByTagName("TrackingCode");
                XmlNodeList errorCode = xml.GetElementsByTagName("ErrorCode");
                if (status.Item(0).InnerText == "OK" || errorCode.Item(0).InnerText == "0" || errorCode.Item(0).InnerText == "-10")
                {
                    mExclusive.SentToInsurance = true;
                    mExclusive.InsuranceTrackCode = trackCode.Item(0).InnerText;
                    if (Helper.Instance.Update())
                        mError.Text += "صورت با موفقیت بروزرسانی شد" + ". شماره رهگیری بیمه: " + mExclusive.InsuranceTrackCode;
                }

            }
            catch (Exception ex)
            {
                mError.Text = "خطا در ارصال اطلاعات به بیمه :" + ex.Message;
                mError.Visible = true;
            }
            BindData();
        }
        else
        {
            if (!enableClose)
                mWarnings.Text += "ابتدا بر دکمه محاسبه کلیک کنید";
            mError.Text += "<br />خطا در بروز رساني: " + Helper.Instance.GetLastException();
        }
    }
    protected void doChooseAuto_Click(object sender, EventArgs e)
    {
        Car d = Helper.Instance.GetCar(mCar.Value);
        if (d == null)
        {
            mWarnings.Text = "کد اتوکار زیر از کارت خوانده شد که در لیست همکاران موجود نیست. لطفا به صورت دستی اضافه شود:<br />" + mCar.Value;
            return;
        }
        mWarnings.Text = "کد اتوکار از کارت خوانده شد.";
        Tool.SetSelected(mCarID, d.ID);
    }
    protected void doChooseDriver1_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver1.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver1.Value);
            }
            catch
            {
                mWarnings.Text = "کارت هوشمند راننده اول تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mWarnings.Text = "کارت هوشمند راننده اول تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                Tool.SetSelected(mDriverID1, d.ID);
            }
            return;
        }
        mWarnings.Text = "کد راننده اول از کارت خوانده شد.";
        Tool.SetSelected(mDriverID1, d.ID);
    }
    protected void doChooseDriver2_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver2.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver2.Value);
            }
            catch
            {
                mWarnings.Text = "کارت هوشمند راننده دوم تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mWarnings.Text = "کارت هوشمند راننده دوم تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                Tool.SetSelected(mDriverID2, d.ID);
            }
            return;
        }
        mWarnings.Text = "کد راننده دوم از کارت خوانده شد.";
        Tool.SetSelected(mDriverID2, d.ID);
    }

    protected void doPrint_Click(object sender, EventArgs e)
    {
        if (mExclusive == null)
            return;
        Response.Redirect(string.Format("Reports/Report.aspx?id={0}&repname={1}", ExclusiveID.ToString(), "SuratVaziatExclusive"), true);
    }
    protected void doCalc_Click(object sender, EventArgs e)
    {
        //doSave.Enabled = doJustSave.Enabled = true;
        //-5%toll
        //    -bimeser
        //    -ezafi*tedaf
        //    -15%CommitTransaction
        //    -nbimrebgedenb
        //int TAMBR = 2000; --->  250  le etalat paye 
        //  ebtal surert 
        int bodyIns = Tool.GetInt(mBodyInsurance.Text, 0);
        int price = Tool.GetInt(mTotalRent.Text, 0);
        int toll = Tool.GetInt(mToll.Text, 0);
        

        int ins = Tool.GetInt(mInsurance.Text, 0);
        int stamp = Tool.GetInt(Helper.Instance.GetSettingValue("Stamp"), 0);
        int extra = Tool.GetInt(mExtraPrice.Text, 0) * Tool.GetInt(mNumPassangers.Text, 0);
        extra = extra;// -(extra % 100);
        int y = price - extra - ins - stamp;
        int toll2 = toll * y / 100;
        toll2 = toll2 - (toll2 % 100);
        int commision = (y - toll2) * 15 / 100;
        commision = commision - (commision % 100);
        int busPay = price - toll2 - ins - extra - commision - bodyIns - stamp;

        mCommission.Text = commision.ToString();

        //if (mCarIsOwnerYes.Checked)
        //    busPay -= price;
        mPrice.Text = (busPay - (busPay % 100)).ToString("N0");
        mPecuniary.Text = (busPay - (busPay % 100)).ToString("N0");

        //
        if (busPay < 0)
            mPecuniary.Text = "0";

        mExtraPrice2.Text = extra.ToString("N0");
        mToll2.Text = toll2.ToString("N0");

        if (!PrepareSave(false))
            return;

        // Get Tracking Code
        if (string.IsNullOrEmpty(mExclusive.TrackCode) && enableClose)
            try
            {
                if (mExclusive.CarID > 0 && mExclusive.DriverID1 > 0 && mExclusive.NumPassangers > 0)
                {
                    if (!Helper.Instance.RequestTrackCode(mExclusive))
                        mError.Text += "<br />خطا در دریافت کد رهگیری صورت وضعیت. لطفا بعدا دوباره سعی کنید.";
                }
            }
            catch (Exception ex)
            {
                mError.Text += "<br />خطا در دریافت کد رهگیری صورت وضعیت: " + ex.Message;
            }
        if (SaveExclusive(false))
        {
            this.ExclusiveID = mExclusive.ID;
            BindData();
        }

    }
    protected void doChangeComission_Click(object sender, EventArgs e)
    {
        int BIMBADANEH = Tool.GetInt(mBodyInsurance.Text, 0);
        int TAX = Tool.GetInt(mToll.Text, 0);
        int TOTAL_PRICE = Tool.GetInt(mTotalRent.Text, 0);

        int INSURANCE_LIMIT = Tool.GetInt(mInsurance.Text, 0);
        int TAMBR = 2000;
        int COMMISION = Tool.GetInt(mCommission.Text, 0);
        COMMISION = (int)Math.Ceiling((decimal)COMMISION);
        int BUS_PAY = TOTAL_PRICE - TAX - INSURANCE_LIMIT - TAMBR - COMMISION - BIMBADANEH;
        if (mCarIsOwnerYes.Checked)
            BUS_PAY = BUS_PAY - TOTAL_PRICE;
        //document.getElementById('fld_Daraee').value=0;		
        BUS_PAY = BUS_PAY - 0;//document.getElementById('fld_Daraee').value;	
        BUS_PAY = (int)Math.Floor((decimal)BUS_PAY);
        //document.getElementById('BUS_PAY').value = $BUS_PAY;			
        mPrice.Text = BUS_PAY.ToString();
        mPecuniary.Text = BUS_PAY.ToString();

        if (BUS_PAY < 0)
            mPecuniary.Text = "0";

    }


    protected void doInvalid2_Click(object sender, EventArgs e)
    {
        if (mExclusive != null && mExclusive.No > 0)
        {
            // should not happen
            mError.Text = "این شماره صورت استفاده شده است و قابل ابطال نیست";
            return;
        }

        int no = Tool.GetInt(mNo.Text, 0);
        if (no == 0)
        {
            mError.Text = "شماره صورت اشتباه است";
            return;
        }
        //mExclusive.Series = "";
        if (mExclusive != null)
            mExclusive.No = 0;

        InValid v = new InValid();
        if (mExclusive != null)
            v.Date = mExclusive.DepartureDate;
        else
            v.Date = DateTime.Now;
        v.Ticketless = 0;
        v.TicketlessPrice = 0;
        v.TicketlessName = "";
        v.Stamp = 0;
        v.Comission = 0;
        v.BodyInsurance = 0;
        v.ExtraCost = 0;
        v.Closed = false;
        v.Price = 0;
        v.Toll = 0;
        v.Insurance = 0;
        v.Reception = 0;
        v.TotalPrice = 0;
        //v.Comission2 = mExclusive.Comission2;
        //v.OtherDeficits = mExclusive.OtherDeficits;
        //v.SumDeficits = mExclusive.SumDeficits;
        //v.AutoShare = mExclusive.AutoShare;
        //v.Toll2 = mExclusive.Toll2;
        //v.Locked = mExclusive.Locked;
        //v.Others = mExclusive.Others;
        v.TripID = 0;
        v.No = no;
        //v.Series = mExclusive.Series;
        v.Series = string.Format("{0},{1},{2}"
                          , Helper.Instance.GetSettingValue("SeriesNoD1"), Helper.Instance.GetSettingValue("SeriesNoD2")
                          , Helper.Instance.GetSettingValue("SeriesNoD3"));
        //v.CarID = mExclusive.CarID;
        //v.DriverID1 = mExclusive.DriverID1;
        //v.DriverID2 = mExclusive.DriverID2;
        if (mExclusive != null)
            v.ServiceID = mExclusive.PathID;
        v.Exclusive = true;
        Helper.Instance.DB.InValids.InsertOnSubmit(v);
        if (!Helper.Instance.Update())
        {
            mError.Text = "اشکال در ابطال : " + Helper.Instance.LastException.Message;
            return;
        }

        BindData();
        // no = Tool.GetInt(mNo.Text, 0);
        //doJustSave_Click(null, null);
        mError.Text = "شماره صورت وضعیت " + no + "باطل شد - تنظیمات بروزرسانی شد";
    }
    protected void puOk_Click(object sender, EventArgs e)
    {
        int no = mExclusive.No;
        try
        {
            if (!ChangeSeries(mIsWithCode.Text == true.ToString()))
                return;
        }
        catch (Exception ex)
        {
            mError.Text = ex.Message;
            return;
        }
        //ExclusiveID = -1;
        Response.Redirect("Exclusives.aspx");
        BindData();
        //**
        //doJustSave_Click(null, null);
        //**
        
        mWarnings.Text = "سری و شماره صورت وضعیت " + no + "باطل شد";
    }

    protected void doInvalid_Click(object sender, EventArgs e)
    {
        mIsWithCode.Text = true.ToString();
        puEx.Show();
    }
    protected void doChangeSeries_NoCode_Click(object sender, EventArgs e)
    {
        mIsWithCode.Text = false.ToString();
        puEx.Show();
    }
    bool ChangeSeries(bool withCode)
    {
        try
        {
            Helper.Instance.CancelExclusive(withCode, mExclusive);
        }
        catch (Exception ex)
        {
            mError.Text += ex.Message;
            return false;
        }

        //Response.Redirect("Exclusives.aspx");
        return true;
    }
    protected void doEdit_Click(object sender, EventArgs e)
    {
        if (mExclusive == null)
            return;
        if (mExclusive.CloseDate.HasValue)
        {
            TimeSpan ts = DateTime.Now - mExclusive.CloseDate.Value;
            if (ts.TotalMinutes > 86400)
            {
                mError.Text = "از بستن صورت بیش از 2 ماه گذشته است و نمیتوان آنرا ویرایش کرد.";
                return;
            }
        }
        EditMode = true;
        BindData();
    }
    protected void doDelete_Click(object sender, EventArgs e)
    {
        if (mExclusive == null || mExclusive.CloseDate.HasValue || mExclusive.No > 0)
        {
            // it should not happen
            mError.Text += "صورت وضعیت بسته یا دارای شماره صورت است و نمیتوان آنرا حذف کرد";
            return;
        }
        try
        {
            Helper.Instance.DB.Exclusives.DeleteOnSubmit(mExclusive);
            if (!Helper.Instance.Update())
                throw new Exception("اشکال در حذف سرویس");
        }
        catch (Exception ex)
        {
            BindData();
            mError.Text += ex.Message;
            return;
        }
        Response.Redirect("Exclusives.aspx");
    }
    protected void doRequestInsurance_Click(object sender, EventArgs e)
    {
        if (mExclusive == null)
        {
            mError.Text = "صورتی ایجاد نشده است <br />";
            return;
        }
        try
        {
            string result = Helper.Instance.RequestInsurance(mExclusive);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(result);
            XmlNodeList msg = xml.GetElementsByTagName("Message");
            mError.Text = msg.Item(0).InnerText+"<br />";
            XmlNodeList status = xml.GetElementsByTagName("Status");
            XmlNodeList trackCode = xml.GetElementsByTagName("TrackingCode");
            XmlNodeList errorCode = xml.GetElementsByTagName("ErrorCode");
            if (status.Item(0).InnerText == "OK" || errorCode.Item(0).InnerText == "0" || errorCode.Item(0).InnerText == "-10")
            {
                mExclusive.SentToInsurance = true;
                mExclusive.InsuranceTrackCode = trackCode.Item(0).InnerText;
                if (Helper.Instance.Update())
                    mError.Text += "صورت با موفقیت بروزرسانی شد";
            }
           
        }
        catch (Exception ex)
        {
            mError.Text = "خطا در ارصال اطلاعات به بیمه :" + ex.Message;
            mError.Visible = true;
        }

    }
    protected void doChooseDriver3_Click(object sender, EventArgs e)
    {
        Driver d = Helper.Instance.GetDriver(mDriver3.Value);
        if (d == null)
        {
            try
            {
                d = Helper.Instance.CreateDriver(mDriver3.Value);
            }
            catch
            {
                mWarnings.Text = "کارت هوشمند راننده سوم تشخیص داده نشد و اطلاعات آن از تهران دریافت نشد.";
            }
            if (d != null)
            {
                mWarnings.Text = "کارت هوشمند راننده سوم تشخیص داده شد و اطلاعات آن از تهران دریافت شد و به صورت خودکار به لیست همکاران اضافه شد..";
                Tool.SetSelected(mDriverID3, d.ID);
            }
            return;
        }
        mWarnings.Text = "کد راننده سوم از کارت خوانده شد.";
        Tool.SetSelected(mDriverID3, d.ID);
    }
}
